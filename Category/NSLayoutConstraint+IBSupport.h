//
//  NSLayoutConstraint+IBSupport.h
//  Welfare lottery
//
//  Created by 李楠 on 2019/11/25.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSLayoutConstraint (IBSupport)

@property (nonatomic, assign) IBInspectable BOOL mirror;

- (void)changeMultiplier:(CGFloat)multiplier;

@end

NS_ASSUME_NONNULL_END
