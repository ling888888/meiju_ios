//
//  UIButton+Additions.h
//  KaKa
//
//  Created by 李楠 on 2019/10/21.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (Additions)

- (void)fp_setImageWithURLString:(NSString *)URLString forState:(UIControlState)state;
- (void)fp_setBackgroundImageWithURLString:(NSString *)URLString forState:(UIControlState)state;

@property (nonatomic, assign) IBInspectable BOOL mirrorImage;
@property (nonatomic, assign) IBInspectable BOOL mirrorBacgroundImage;
@property (nonatomic, assign) IBInspectable BOOL mirrorEdgeInsets;
@property (nonatomic, strong) IBInspectable NSString *localizedKey;
@property (nonatomic, assign) IBInspectable BOOL localizeTitle;

@end

NS_ASSUME_NONNULL_END
