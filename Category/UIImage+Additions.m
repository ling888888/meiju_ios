//
//  UIImage+Additions.m
//  KaKa
//
//  Created by 李楠 on 2019/10/17.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "UIImage+Additions.h"

@implementation UIImage (Additions)

+ (UIImage *)mirroredImageNamed:(NSString *)name {
    UIImage *image = [UIImage imageNamed:name];
    if (kIsMirroredLayout) {
        return image.mirroredImage;;
    } else {
        return image;
    }
}

- (UIImage *)mirroredImage {
    return [UIImage imageWithCGImage:self.CGImage scale:self.scale orientation:UIImageOrientationUpMirrored];
}

+ (NSString *)typeForImageData:(NSData *)data {
    uint8_t c;
    [data getBytes:&c length:1];
    switch (c)
    {
        case 0xFF:
            return @"image/jpeg";
        case 0x89:
            return @"image/png";
        case 0x47:
            return @"image/gif";
        case 0x49:
        case 0x4D:
            return @"image/tiff";
    }
    return nil;
}

@end
