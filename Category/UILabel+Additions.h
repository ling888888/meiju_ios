//
//  UILabel+Additions.h
//  SimpleDate
//
//  Created by ling Hou on 2020/2/18.
//  Copyright © 2020 WYB. All rights reserved.
//


#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (Additions)

@property (nonatomic, strong) IBInspectable NSString *localizedKey;
@property (nonatomic, assign) IBInspectable BOOL localizeText;

@end

NS_ASSUME_NONNULL_END
