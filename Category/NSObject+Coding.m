//
//  NSObject+Coding.m
//  SimpleDate
//
//  Created by 侯玲 on 2018/7/6.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "NSObject+Coding.h"
#import <objc/runtime.h>

@implementation NSObject (Coding)

- (void)hl_decodeWithDeCoder:(NSCoder *)decoder{
    unsigned int count = 0;
    Ivar *ivars = class_copyIvarList([self class], &count);
    if (ivars) {
        for (int i = 0; i<count; i++) {
            Ivar ivar = ivars[i];
            const char *ivarname = ivar_getName(ivar);
            NSString * keyName = [[NSString alloc] initWithUTF8String:ivarname];
            id value = [decoder decodeObjectForKey:keyName];
            [self setValue:value?:@"" forKey:keyName];
        }
    }
    free(ivars);
}

//编码
- (void)hl_encodeWithCoder:(NSCoder *)coder {
    unsigned int count = 0;
    Ivar * vars = class_copyIvarList([self class], &count);
    if (vars) {
        for (int i =0 ; i < count; i++) {
            Ivar var = vars[i];
            const char *varname = ivar_getName(var);
            NSString * keyName = [NSString stringWithUTF8String:varname];
            id value = [self valueForKey:keyName];
            [coder encodeObject:value?:@"" forKey:keyName];
        }
    }
    free(vars);
}


@end
