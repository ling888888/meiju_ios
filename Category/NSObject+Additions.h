//
//  NSObject+Additions.h
//  Seeky
//
//  Created by 李楠 on 2019/8/8.
//  Copyright © 2019 简约互动. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (Additions)

@property (nonatomic, assign) BOOL notEmpty;
- (void)showTips:(NSString *)tips;

- (void)presentAlertWithTitle:(nullable NSString *)title
      message:(nullable NSString *)message
  cancelTitle:(nullable NSString *)cancelTitle
cancelHandler:(void (^ __nullable)(UIAlertAction *action))cancelHandler
  actionTitle:(nullable NSString *)actionTitle
                actionHandler:(void (^ __nullable)(UIAlertAction *action))actionHandler;

- (void)presentMessageAlertWithTitle:(nullable NSString *)title
      message:(nullable NSString *)message
  actionTitle:(nullable NSString *)actionTitle
                actionHandler:(void (^ __nullable)(UIAlertAction *action))actionHandler;
@end

NS_ASSUME_NONNULL_END
