//
//  UIButton+Additions.m
//  KaKa
//
//  Created by 李楠 on 2019/10/21.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "UIButton+Additions.h"
#import <SDWebImage/UIButton+WebCache.h>

@implementation UIButton (Additions)

@dynamic mirrorImage, mirrorBacgroundImage, mirrorEdgeInsets, localizedKey, localizeTitle;

- (void)fp_setImageWithURLString:(NSString *)URLString forState:(UIControlState)state {
    if (![URLString hasPrefix:@"http"]) {
        URLString = [NSString stringWithFormat:STATIC_DOMAIN_URL, URLString];
    }
    
    [self sd_setImageWithURL:[NSURL URLWithString:URLString] forState:state placeholderImage:[UIImage imageNamed:@"img_portrait_placeholder"]];
}

- (void)fp_setBackgroundImageWithURLString:(NSString *)URLString forState:(UIControlState)state {
    if (![URLString hasPrefix:@"http"]) {
        URLString = [NSString stringWithFormat:STATIC_DOMAIN_URL, URLString];
    }
    
    [self sd_setBackgroundImageWithURL:[NSURL URLWithString:URLString] forState:state placeholderImage:[UIImage imageNamed:@"img_portrait_placeholder"]];
}

- (void)setMirrorImage:(BOOL)mirrorImage {
    if (mirrorImage && kIsMirroredLayout) {
        UIImage *normalImage = [self imageForState:UIControlStateNormal];
        if (normalImage) {
            [self setImage:normalImage.mirroredImage forState:UIControlStateNormal];
        }
    }
}

- (void)setMirrorBacgroundImage:(BOOL)nmirrorBacgroundImage {
    if (nmirrorBacgroundImage && kIsMirroredLayout) {
        UIImage *normalBackgroundImage = [self backgroundImageForState:UIControlStateNormal];
        if (normalBackgroundImage) {
            [self setBackgroundImage:normalBackgroundImage.mirroredImage forState:UIControlStateNormal];
        }
    }
}

- (void)setMirrorEdgeInsets:(BOOL)mirrorEdgeInsets {
    if (mirrorEdgeInsets && kIsMirroredLayout) {
        self.contentEdgeInsets = UIEdgeInsetsMake(self.contentEdgeInsets.top, self.contentEdgeInsets.right, self.contentEdgeInsets.bottom, self.contentEdgeInsets.left);
        self.titleEdgeInsets = UIEdgeInsetsMake(self.titleEdgeInsets.top, self.titleEdgeInsets.right, self.titleEdgeInsets.bottom, self.titleEdgeInsets.left);
        self.imageEdgeInsets = UIEdgeInsetsMake(self.imageEdgeInsets.top, self.imageEdgeInsets.right, self.imageEdgeInsets.bottom, self.imageEdgeInsets.left);
    }
}

- (void)setLocalizedKey:(NSString *)localizedKey {
    [self setTitle:SPDLocalizedString(localizedKey) forState:UIControlStateNormal];
}

- (void)setLocalizeTitle:(BOOL)localizeTitle {
    if (localizeTitle) {
        NSString *normalTitle = [self titleForState:UIControlStateNormal];
        [self setTitle:SPDLocalizedString(normalTitle) forState:UIControlStateNormal];
        NSString *selectedTitle = [self titleForState:UIControlStateSelected];
        [self setTitle:SPDLocalizedString(selectedTitle) forState:UIControlStateSelected];
    }
}

@end
