//
//  UIImageView+Additions.h
//  SimpleDate
//
//  Created by ling Hou on 2020/1/7.
//  Copyright © 2020 WYB. All rights reserved.
//


#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImageView (Additions)

- (void)fp_setImageWithURLString:(NSString *)URLString;

@property (nonatomic, assign) IBInspectable BOOL mirrorImage;

@end

NS_ASSUME_NONNULL_END
