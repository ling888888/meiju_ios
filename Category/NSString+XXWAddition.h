//
//  NSString+XXWAddition.h
//  SimpleDate
//
//  Created by xu.juvenile on 2016/3/3.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (XXWAddition)

- (NSString *)md5String;

- (NSString *)stringByTrim;

- (NSString *)SHA256AndBase64 ;

+ (CGSize)sizeWithString:(NSString*)str andFont:(UIFont*)font andMaxSize:(CGSize)size;

+(CGFloat)getSpaceLabelHeightWithStr:(NSString *)str withSpeace:(CGFloat)lineSpeace withFont:(UIFont*)font withWidth:(CGFloat)width;

+(NSDictionary *)parseJSONStringToNSDictionary:(NSString *)JSONString;

@end
