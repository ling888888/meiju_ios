//
//  UIViewController+Additions.m
//  SimpleDate
//
//  Created by ling Hou on 2020/2/14.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "UIViewController+Additions.h"
#import <objc/runtime.h>
#import "LY_LiveAudienceHalfVC.h"

#import "LY_HalfViewController.h"
#import "LY_HalfNavigationController.h"

@implementation UIViewController (Additions)

+ (void)load {
    [super load];
    SEL exchangeSel = @selector(famy_presentViewController:animated:completion:);
    SEL originSel = @selector(presentViewController:animated:completion:);
    method_exchangeImplementations(class_getInstanceMethod(self.class, originSel), class_getInstanceMethod(self.class, exchangeSel));
}

#pragma mark - swizzled

- (void)famy_presentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion {
    if ([viewControllerToPresent class] == [LY_LiveAudienceHalfVC class]) {
        
    } else if ([viewControllerToPresent class] == [LY_HalfViewController class]) {
    
    } else if ([viewControllerToPresent class] == [LY_HalfNavigationController class]) {
        
        
    } else {
        viewControllerToPresent.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    [self famy_presentViewController:viewControllerToPresent animated:flag completion:completion];
}

@end
