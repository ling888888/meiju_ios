//
//  UIView+BorderWithCornerRadius.h
//  SimpleDate
//
//  Created by 周佳磊 on 16/2/26.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (BorderWithCornerRadius)

@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic) IBInspectable CGFloat cornerRadius;
@property (nonatomic) IBInspectable UIColor *shadowUIColor;
@property (nonatomic) IBInspectable CGFloat shadowUIOpacity;
@property (nonatomic) IBInspectable CGFloat shadowCornerRadius;

@end
