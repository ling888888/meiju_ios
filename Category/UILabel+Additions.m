//
//  UILabel+Additions.m
//  SimpleDate
//
//  Created by ling Hou on 2020/2/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "UILabel+Additions.h"


@implementation UILabel (Additions)

@dynamic localizedKey, localizeText;

- (void)setLocalizedKey:(NSString *)localizedKey {
    self.text = SPDLocalizedString(localizedKey);
}

- (void)setLocalizeText:(BOOL)localizeText {
    if (localizeText) {
        self.text = SPDStringWithKey(self.text, nil);
    }
}

@end
