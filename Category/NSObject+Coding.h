//
//  NSObject+Coding.h
//  SimpleDate
//
//  Created by 侯玲 on 2018/7/6.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Coding)

- (void)hl_decodeWithDeCoder:(NSCoder *)decoder;

- (void)hl_encodeWithCoder:(NSCoder *)coder ;

@end
