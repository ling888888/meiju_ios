//
//  UIImageView+WebCacheWithActivityIndicator.h
//  SimpleDate
//
//  Created by xu.juvenile on 16/3/16.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (WebCacheWithActivityIndicator)

- (void)sd_setImageShowingActivityIndicatorWithURL:(NSURL *)url;

@end
