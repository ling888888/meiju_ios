//
//  UIImage+Additions.h
//  KaKa
//
//  Created by 李楠 on 2019/10/17.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (Additions)

+ (UIImage *)mirroredImageNamed:(NSString *)name;

- (UIImage *)mirroredImage;

+ (NSString *)typeForImageData:(NSData *)data;

@end

NS_ASSUME_NONNULL_END
