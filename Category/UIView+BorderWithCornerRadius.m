//
//  UIView+BorderWithCornerRadius.m
//  SimpleDate
//
//  Created by 周佳磊 on 16/2/26.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "UIView+BorderWithCornerRadius.h"

@implementation UIView (BorderWithCornerRadius)

@dynamic borderColor, borderWidth, cornerRadius,shadowUIColor,shadowUIOpacity,shadowCornerRadius;

- (void) setBorderColor:(UIColor *)borderColor {
    [self.layer setBorderColor:borderColor.CGColor];
}

- (void) setBorderWidth:(CGFloat)borderWidth {
    [self.layer setBorderWidth:borderWidth];
}

- (void) setCornerRadius:(CGFloat)cornerRadius {
    [self.layer setCornerRadius:cornerRadius];
}

- (void)setShadowUIColor:(UIColor *)shadowUIColor {
    [self.layer setShadowColor:shadowUIColor.CGColor];
}

- (void)setShadowUIOpacity:(CGFloat)shadowUIOpacity {
    [self.layer setShadowOpacity:shadowUIOpacity];
}

- (void)setShadowCornerRadius:(CGFloat)shadowCornerRadius {
    [self.layer setShadowRadius:shadowCornerRadius];
}
@end
