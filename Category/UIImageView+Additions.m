//
//  UIImageView+Additions.m
//  SimpleDate
//
//  Created by ling Hou on 2020/1/7.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "UIImageView+Additions.h"

@implementation UIImageView (Additions)

@dynamic mirrorImage;

- (void)fp_setImageWithURLString:(NSString *)URLString {
    if (![URLString hasPrefix:@"http"]) {
        URLString = [NSString stringWithFormat:STATIC_DOMAIN_URL, URLString];
    }
    [self sd_setImageWithURL:[NSURL URLWithString:URLString] placeholderImage:[UIImage imageNamed:@"img_portrait_placeholder"]options:SDWebImageRefreshCached];
}

- (void)setMirrorImage:(BOOL)mirrorImage {
    if (mirrorImage && kIsMirroredLayout && self.image) {
        self.image = self.image.mirroredImage;
    }
}

@end
