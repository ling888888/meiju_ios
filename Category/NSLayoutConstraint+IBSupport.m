//
//  NSLayoutConstraint+IBSupport.m
//  Welfare lottery
//
//  Created by 李楠 on 2019/11/25.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "NSLayoutConstraint+IBSupport.h"

@implementation NSLayoutConstraint (IBSupport)

@dynamic mirror;

- (void)setMirror:(BOOL)mirror {
    if (mirror && kIsMirroredLayout) {
        self.constant = -self.constant;
        [self changeMultiplier:2 - self.multiplier];
    }
}

- (void)changeMultiplier:(CGFloat)multiplier {
    [NSLayoutConstraint deactivateConstraints:@[self]];
    
    NSLayoutConstraint *newConstraint = [NSLayoutConstraint constraintWithItem:self.firstItem attribute:self.firstAttribute relatedBy:self.relation toItem:self.secondItem attribute:self.secondAttribute multiplier:multiplier constant:self.constant];
    newConstraint.priority = self.priority;
    newConstraint.shouldBeArchived = self.shouldBeArchived;
    newConstraint.identifier = self.identifier;
    
    [NSLayoutConstraint activateConstraints:@[newConstraint]];
}

@end
