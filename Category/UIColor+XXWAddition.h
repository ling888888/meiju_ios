//
//  UIColor+XXWAddition.h
//  SimpleDate
//
//  Created by xu.juvenile on 2016/3/3.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


@interface UIColor (XXWAddition)

/**
 *  eg. @"0xF0F", @"66ccff", @"#66CCFF88"
 *
 *  @param hexStr value
 *
 *  @return UIColor object
 */
+ (nullable UIColor *)colorWithHexString:(NSString *)hexStr;


@end

NS_ASSUME_NONNULL_END
