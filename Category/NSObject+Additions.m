//
//  NSObject+Additions.m
//  Seeky
//
//  Created by 李楠 on 2019/8/8.
//  Copyright © 2019 简约互动. All rights reserved.
//

#import "NSObject+Additions.h"

@implementation NSObject (Additions)

- (void)setNotEmpty:(BOOL)notEmpty {
    
}

- (BOOL)notEmpty {
    if (!self || [self isEqual:[NSNull null]]) {
        return NO;
    } else if ([self isKindOfClass:[NSString class]]) {
        return [[(NSString *)self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0;
    } else if ([self isKindOfClass:[NSArray class]]) {
        return [(NSArray *)self count] > 0;
    } else if ([self isKindOfClass:[NSDictionary class]]) {
        return [(NSDictionary *)self count] > 0;
    } else {
        return YES;
    }
}

- (void)showTips:(NSString *)tips {
    [MBProgressHUD hideAllHUDsForView:[UIApplication sharedApplication].keyWindow animated:YES];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    hud.userInteractionEnabled = NO;
    hud.mode = MBProgressHUDModeText;
    hud.margin = 10.0f;
    hud.yOffset = 15.0f;
    hud.opacity = 0.5f;
    hud.removeFromSuperViewOnHide = YES;
    hud.detailsLabelText = tips;
    hud.detailsLabelFont = [UIFont systemFontOfSize:15];
    [hud hide:YES afterDelay:2.5];
}

- (void)presentAlertWithTitle:(nullable NSString *)title
                      message:(nullable NSString *)message
                  cancelTitle:(nullable NSString *)cancelTitle
                cancelHandler:(void (^ __nullable)(UIAlertAction *action))cancelHandler
                  actionTitle:(nullable NSString *)actionTitle
                actionHandler:(void (^ __nullable)(UIAlertAction *action))actionHandler {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    if (cancelTitle.notEmpty) {
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            if (cancelHandler) {
                cancelHandler(action);
            }
        }];
        [cancelAction setValue:[UIColor colorWithHexString:@"333333"] forKey:@"_titleTextColor"];
        [alertController addAction:cancelAction];
    }
    if (actionTitle.notEmpty) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:actionTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if (actionHandler) {
                actionHandler(action);
            }
        }];
        [action setValue:[UIColor colorWithHexString:@"333333"] forKey:@"_titleTextColor"];
        [alertController addAction:action];
    }
    
    if ([self isKindOfClass:[UIViewController class]]) {
        [(UIViewController *)self presentViewController:alertController animated:YES completion:nil];
    } else {
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)presentMessageAlertWithTitle:(nullable NSString *)title
    message:(nullable NSString *)message
actionTitle:(nullable NSString *)actionTitle
                       actionHandler:(void (^ __nullable)(UIAlertAction *action))actionHandler {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    if (actionTitle.notEmpty) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:actionTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if (actionHandler) {
                actionHandler(action);
            }
        }];
        [action setValue:[UIColor colorWithHexString:@"333333"] forKey:@"_titleTextColor"];
        [alertController addAction:action];
    }
    
    if ([self isKindOfClass:[UIViewController class]]) {
        [(UIViewController *)self presentViewController:alertController animated:YES completion:nil];
    } else {
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
    }
}

@end
