//
//  UIImageView+WebCacheWithActivityIndicator.m
//  SimpleDate
//
//  Created by xu.juvenile on 16/3/16.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "UIImageView+WebCacheWithActivityIndicator.h"

@implementation UIImageView (WebCacheWithActivityIndicator)


- (void)sd_setImageShowingActivityIndicatorWithURL:(NSURL *)url
{
//    [self sd_setImageWithPreviousCachedImageWithURL:url placeholderImage:[UIImage imageNamed:@"img_portrait_placeholder"] options:SDWebImageRefreshCached progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//        [self setShowActivityIndicatorView:YES];
//    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//        if (!image) {
//            [self setImage:[UIImage imageNamed:@"img_portrait_placeholder"]];
//        }
//    }];]
    [self sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"img_portrait_placeholder"]options:SDWebImageRefreshCached];
}

@end
