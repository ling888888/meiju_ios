//
//  NSData+XXWAddition.h
//  SimpleDate
//
//  Created by xu.juvenile on 2016/3/3.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (XXWAddition)

- (NSString *)md5String;

- (NSString *)SHA256String;

@end
