//
//  WMScrollView.h
//  WMPageController
//
//  Created by lh on 15/11/21.
//  Copyright (c) 2015年 yq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WMScrollViewDelegate <NSObject>

@optional
- (BOOL)customGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer;

@end

@interface WMScrollView : UIScrollView <UIGestureRecognizerDelegate>

@property(nonatomic, weak)id<WMScrollViewDelegate> customDelegate;

@end
