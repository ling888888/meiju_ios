//
//  LY_GradientView.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/27.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_GradientView : UIImageView

/// 颜色数组（必传）
@property(nonatomic, strong) NSArray<UIColor *> *colors;

/// 默认水平（可选，默认水平）
@property(nonatomic, assign) UIImageGradientColorsDirection direction;

/// 位置颜色（可选）
@property(nonatomic, assign) const CGFloat * _Nullable locations;

/// 主动更新渐变颜色（在未更新约束，同时修改颜色时使用。一般用不到）
- (void)updateColors;

@end

NS_ASSUME_NONNULL_END
