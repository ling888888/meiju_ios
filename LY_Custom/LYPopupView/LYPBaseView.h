//
//  LYPBaseView.h
//  LYPopupView
//
//  Created by jianyue on 2020/3/20.
//  Copyright © 2020 Famy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry.h>

NS_ASSUME_NONNULL_BEGIN

@interface LYPBaseView : UIView

typedef void (^DismissHandler)();

/// 是否关闭 在点击背景时
@property(nonatomic, assign) BOOL shouldDismissOnTouchBackgound;


/// 显示
- (void)present;

/// 显示在view上
- (void)presentOnView:(UIView *)view;

/// 移除消失
- (void)dismiss;

/// 弹窗消失回调
- (void)dismiss:(DismissHandler)handler;



- (void)willPresent;
- (void)animatePresent;
- (void)finishedPresent;

- (void)willDismiss;
- (void)animateDismiss;
- (void)finishedDismiss;

@end

NS_ASSUME_NONNULL_END
