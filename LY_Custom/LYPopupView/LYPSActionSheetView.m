//
//  LYPSActionSheetView.m
//  LYPopupView
//
//  Created by jianyue on 2020/3/23.
//  Copyright © 2020 Famy. All rights reserved.
//

#import "LYPSActionSheetView.h"

@interface LYPSActionSheetView () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

/// 取消、完成等按钮 - 按钮数量可变
@property(nonatomic, strong) UICollectionView *collectionView;

@property(nonatomic, strong) NSMutableArray *mutableActions;

@end

@implementation LYPSActionSheetView

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [_collectionView registerClass:[LYPSActionSheetViewCell class] forCellWithReuseIdentifier:@"ActionCellIdentifier"];
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterViewIdentifier"];
    }
    return _collectionView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self _setupUI];
        
        [self _setupUIFrame];
        
        _mutableActions = [NSMutableArray array];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
     
    NSLog(@"%@", self.actions);
    
    [self.containerView addRounded:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:20];
}

- (CGFloat)containerHeight {
    return self.collectionView.contentSize.height;
}

- (void)_setupUI {
    [self.containerView addSubview:self.collectionView];
}

- (void)_setupUIFrame {
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.containerView);
        make.leading.trailing.mas_equalTo(self.containerView);
        make.bottom.mas_equalTo(self.containerView);
        make.height.mas_equalTo(60);
    }];
}

- (void)addAction:(LYPSystemAlertAction *)action {
    [self.mutableActions addObject:action];
}

- (NSArray<LYPSystemAlertAction *> *)actions {
    return self.mutableActions;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.actions.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LYPSActionSheetViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ActionCellIdentifier" forIndexPath:indexPath];
    
    cell.action = self.actions[indexPath.row];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    LYPSystemAlertAction *action = self.actions[indexPath.row];
    action.handler(action);
    // 移除弹窗
    [self dismiss];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(collectionView.bounds.size.width, 60);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.5;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterViewIdentifier" forIndexPath:indexPath];
    footerView.backgroundColor = [UIColor whiteColor];
    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.2];
    
    
    UIButton *cancelBtn = [[UIButton alloc] init];
    [cancelBtn setTitle:@"取消".localized forState:UIControlStateNormal];
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [cancelBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelBtnAction) forControlEvents:UIControlEventTouchUpInside];
    
    [footerView addSubview:lineView];
    [footerView addSubview:cancelBtn];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(footerView);
        make.height.mas_equalTo(0.5);
        make.leading.trailing.mas_equalTo(footerView);
    }];
    
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lineView.mas_bottom);
        make.leading.trailing.mas_equalTo(footerView);
        make.height.mas_equalTo(49.5);
    }];
    
    return footerView;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    if ([UIDevice currentDevice].is_iPhoneX) {
        return CGSizeMake(self.bounds.size.width, 50 + 34);
    } else {
        return CGSizeMake(self.bounds.size.width, 50);
    }
}

- (void)cancelBtnAction {
    [self dismiss];
}

@end


@implementation LYPSActionSheetViewCell

- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] init];
        _textLabel.textAlignment = NSTextAlignmentCenter;
        _textLabel.font = [UIFont systemFontOfSize:15];
        _textLabel.textColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.8];
    }
    return _textLabel;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.textLabel];
        [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self.contentView);
        }];
    }
    return self;
}

- (void)setAction:(LYPSystemAlertAction *)action {
    _action = action;
    
    self.textLabel.text = action.title;
    
//    switch (action.style) {
//        case LYPSystemAlertActionStyleCancel: {
//            self.contentView.backgroundColor = [UIColor whiteColor];
//            self.textLabel.textColor = [UIColor blackColor];
//            break;
//        }
//        case LYPSystemAlertActionStyleDefault: {
//            self.contentView.backgroundColor = [UIColor whiteColor];
//            self.textLabel.textColor = [UIColor redColor];
//            break;
//        }
//        case LYPSystemAlertActionStyleDestructive: {
//            self.contentView.backgroundColor = [UIColor redColor];
//            self.textLabel.textColor = [UIColor whiteColor];
//            break;
//        }
//        default:
//            break;
//    }
    
}

@end
