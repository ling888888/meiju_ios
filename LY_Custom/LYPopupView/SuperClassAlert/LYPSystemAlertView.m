//
//  LYPSystemAlertView.m
//  LYPopupView
//
//  Created by jianyue on 2020/3/20.
//  Copyright © 2020 Famy. All rights reserved.
//

#import "LYPSystemAlertView.h"

@interface LYPSystemAlertAction ()

@end

@implementation LYPSystemAlertAction

+ (instancetype)actionWithTitle:(NSString *)title style:(LYPSystemAlertActionStyle)style handler:(void (^)(LYPSystemAlertAction * _Nonnull))handler {
    LYPSystemAlertAction *action = [[LYPSystemAlertAction alloc] initWithTitle:title style:style handler:handler];
    
    return action;
}

- (instancetype)initWithTitle:(NSString *)title style:(LYPSystemAlertActionStyle)style handler:(void (^)(LYPSystemAlertAction * _Nonnull))handler {
    self = [super init];
    if (self) {
        _title = title;
        _style = style;
        _handler = handler;
        _enabled = true;
    }
    return self;
}

- (nonnull id)copyWithZone:(nullable NSZone *)zone {
    return @"";
}

@end

@interface LYPSystemAlertActionView ()  <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

/// 取消、完成等按钮 - 按钮数量可变
@property(nonatomic, strong) UICollectionView *collectionView;

@end

@implementation LYPSystemAlertActionView

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.1];
        [_collectionView registerClass:[LYPSystemAlertActionCell class] forCellWithReuseIdentifier:@"ActionCellIdentifier"];
    }
    return _collectionView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.collectionView];
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self);
//            make.top.mas_equalTo(self.contentView.mas_bottom).offset(30);
//            make.leading.trailing.mas_equalTo(self.containerView);
//            make.bottom.mas_equalTo(self.containerView);
//            make.height.mas_equalTo(56);
        }];
    }
    return self;
}

- (void)setActions:(NSArray<LYPSystemAlertAction *> *)actions {
    _actions = actions;
    
    [self.collectionView reloadData];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.actions.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LYPSystemAlertActionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ActionCellIdentifier" forIndexPath:indexPath];
    
    cell.action = self.actions[indexPath.row];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    LYPSystemAlertAction *action = self.actions[indexPath.row];
    if (action.handler) {
        action.handler(action);
    }
    
    if (action.enabled) {
        // 移除弹窗
        if (indexPath.row == 0) {
            [self.delegate actionViewDidClickCancelAction];
        } else {
            [self.delegate actionViewDidClickDoneAction];
        }
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((collectionView.bounds.size.width - 0.5) / self.actions.count, collectionView.bounds.size.height - 0.5);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.5;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0.5, 0, 0, 0);
}

@end

@implementation LYPSystemAlertActionCell

- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] init];
        _textLabel.font = [UIFont mediumFontOfSize:18];
        _textLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _textLabel;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.textLabel];
        [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self.contentView);
        }];
    }
    return self;
}

- (void)setAction:(LYPSystemAlertAction *)action {
    _action = action;
    
    self.textLabel.text = action.title;
    
    switch (action.style) {
        case LYPSystemAlertActionStyleCancel: {
            self.contentView.backgroundColor = [UIColor whiteColor];
            self.textLabel.textColor = [UIColor blackColor];
            break;
        }
        case LYPSystemAlertActionStyleDefault: {
            self.contentView.backgroundColor = [UIColor whiteColor];
            self.textLabel.textColor = [UIColor colorWithHexString:@"#F8145B"];
            break;
        }
        case LYPSystemAlertActionStyleDestructive: {
            self.contentView.backgroundColor = [UIColor colorWithHexString:@"#F8145B"];
            self.textLabel.textColor = [UIColor whiteColor];
            break;
        }
        default:
            break;
    }
    
}

@end


@interface LYPSystemAlertView ()

/// 标题文本
@property(nonatomic, strong) UILabel *titleLabel;

///
@property(nonatomic, strong) LYPSystemAlertActionView *actionView;

///
@property(nonatomic, strong) NSMutableArray *mutableActions;

@end

@implementation LYPSystemAlertView

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = [UIFont boldSystemFontOfSize:18];
        _titleLabel.numberOfLines = 0;
    }
    return _titleLabel;
}

-(UIView *)contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] init];
        _contentView.backgroundColor = [UIColor whiteColor];
    }
    return _contentView;
}

- (LYPSystemAlertActionView *)actionView {
    if (!_actionView) {
        _actionView = [[LYPSystemAlertActionView alloc] init];
        _actionView.backgroundColor = [UIColor whiteColor];
        _actionView.delegate = self;
    }
    return _actionView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self _setupUI];
        
        [self _setupUIFrame];
        
        _mutableActions = [NSMutableArray array];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    NSLog(@"%@", self.actions);
    self.actionView.actions = self.actions;
//    [self.collectionView reloadData];
}

- (void)_setupUI {
    [self.containerView addSubview:self.titleLabel];
    [self.containerView addSubview:self.contentView];
    [self.containerView addSubview:self.actionView];
}

- (void)_setupUIFrame {
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.containerView).offset(28);
        make.leading.trailing.mas_equalTo(self.containerView);
        make.width.mas_equalTo(CONTAINERDEFAULTWIDTH);
    }];
    
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(30);
        make.leading.trailing.mas_equalTo(self.containerView);
//        make.height.mas_greaterThanOrEqualTo(40);
    }];
    
    [self.actionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView.mas_bottom).offset(30);
        make.leading.trailing.mas_equalTo(self.containerView);
        make.bottom.mas_equalTo(self.containerView);
        make.height.mas_equalTo(56);
    }];
}

- (void)setTitle:(NSString *)title {
    _title = title;
    
    self.titleLabel.text = title;
}

- (void)addAction:(LYPSystemAlertAction *)action {
    [self.mutableActions addObject:action];
}

- (NSArray<LYPSystemAlertAction *> *)actions {
    return self.mutableActions;
}

- (void)actionViewDidClickCancelAction {
    [self dismiss];
}

- (void)actionViewDidClickDoneAction {
    [self dismiss];
}

@end
