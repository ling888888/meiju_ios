//
//  LYPBaseAlertView.h
//  LYPopupView
//
//  Created by jianyue on 2020/3/20.
//  Copyright © 2020 Famy. All rights reserved.
//

#import "LYPBaseView.h"

NS_ASSUME_NONNULL_BEGIN

/// 容器视图宽度
#define CONTAINERDEFAULTWIDTH 275

@interface LYPBaseAlertView : LYPBaseView

/// 展示容器视图
@property(nonatomic, strong) UIView *containerView;

@end

NS_ASSUME_NONNULL_END
