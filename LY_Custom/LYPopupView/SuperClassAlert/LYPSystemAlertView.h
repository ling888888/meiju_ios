//
//  LYPSystemAlertView.h
//  LYPopupView
//
//  Created by jianyue on 2020/3/20.
//  Copyright © 2020 Famy. All rights reserved.
//

#import "LYPBaseAlertView.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, LYPSystemAlertActionStyle) {
    LYPSystemAlertActionStyleDefault = 0,
    LYPSystemAlertActionStyleCancel,
    LYPSystemAlertActionStyleDestructive
}; 

@interface LYPSystemAlertAction : NSObject <NSCopying>

+ (instancetype)actionWithTitle:(nullable NSString *)title style:(LYPSystemAlertActionStyle)style handler:(void (^ __nullable)(LYPSystemAlertAction *action))handler;

@property(nonatomic, copy) void (^handler)(LYPSystemAlertAction * handler);
@property (nullable, nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) LYPSystemAlertActionStyle style;

/// 默认True
@property (nonatomic, getter=isEnabled) BOOL enabled;

@end

@protocol LYPSystemAlertActionViewDelegate <NSObject>

- (void)actionViewDidClickCancelAction;

- (void)actionViewDidClickDoneAction;

@end

@interface LYPSystemAlertActionView : UIView

@property (nonatomic, weak) id<LYPSystemAlertActionViewDelegate> delegate;

@property (nonatomic, strong) NSArray<LYPSystemAlertAction *> *actions;

@end

@interface LYPSystemAlertActionCell : UICollectionViewCell

@property(nonatomic, strong) UILabel *textLabel;

@property(nonatomic, strong) LYPSystemAlertAction *action;

@end

@interface LYPSystemAlertView : LYPBaseAlertView <LYPSystemAlertActionViewDelegate>

@property(nonatomic, copy) NSString *title;

@property(nonatomic, strong) UIView *contentView;

- (void)addAction:(LYPSystemAlertAction *)action;

@property (nonatomic, readonly) NSArray<LYPSystemAlertAction *> *actions;

@end



NS_ASSUME_NONNULL_END
