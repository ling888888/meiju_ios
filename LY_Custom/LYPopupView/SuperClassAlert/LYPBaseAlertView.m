//
//  LYPBaseAlertView.m
//  LYPopupView
//
//  Created by jianyue on 2020/3/20.
//  Copyright © 2020 Famy. All rights reserved.
//

#import "LYPBaseAlertView.h"

@interface LYPBaseAlertView ()

@end

@implementation LYPBaseAlertView

- (UIView *)containerView {
    if (!_containerView) {
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor whiteColor];
        _containerView.layer.cornerRadius = 10;
        _containerView.layer.masksToBounds = true;
    }
    return _containerView;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.containerView];
        [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self);
            make.centerY.mas_equalTo(0);
            make.width.mas_equalTo(CONTAINERDEFAULTWIDTH);
        }];
    }
    return self;
}

- (void)willPresent {
    self.containerView.alpha = 0.5;
    [UIView animateWithDuration:0.05 animations:^{
        self.containerView.alpha = 1.0;
    }];
}

- (void)willDismiss {
    self.containerView.alpha = 1.0;
    [UIView animateWithDuration:0.05 animations:^{
        self.containerView.alpha = 0.0;
    }];
}



@end
