//
//  LYPSystemActionSheetView.h
//  LYPopupView
//
//  Created by jianyue on 2020/3/23.
//  Copyright © 2020 Famy. All rights reserved.
//

#import "LYPBaseActionSheetView.h"

NS_ASSUME_NONNULL_BEGIN


@interface LYPSystemActionSheetView : LYPBaseActionSheetView

@end

NS_ASSUME_NONNULL_END
