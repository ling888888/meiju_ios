//
//  LYPBaseActionSheetView.m
//  LYPopupView
//
//  Created by jianyue on 2020/3/23.
//  Copyright © 2020 Famy. All rights reserved.
//

#import "LYPBaseActionSheetView.h"

@interface LYPBaseActionSheetView ()



@end

@implementation LYPBaseActionSheetView

- (UIView *)containerView {
    if (!_containerView) {
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor whiteColor];
    }
    return _containerView;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.containerView];
        
        self.containerView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width, self.containerHeight);
    }
    return self;
}

- (void)willPresent {
    
}

- (void)animatePresent {
    self.containerView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height - self.containerHeight, [UIScreen mainScreen].bounds.size.width, self.containerHeight);
}

- (void)willDismiss {
    
}

- (void)animateDismiss {
    self.containerView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width, self.containerHeight);
}


@end
