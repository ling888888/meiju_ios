//
//  LYPBaseActionSheetView.h
//  LYPopupView
//
//  Created by jianyue on 2020/3/23.
//  Copyright © 2020 Famy. All rights reserved.
//

#import "LYPBaseView.h"

NS_ASSUME_NONNULL_BEGIN

/// 容器视图默认高度
#define CONTAINERDEFAULTHEIGHT 300

@interface LYPBaseActionSheetView : LYPBaseView

/// 展示容器视图
@property(nonatomic, strong) UIView *containerView;

/// 展示容器视图高度，子类必须实现，返回高度
@property(nonatomic, assign, readonly) CGFloat containerHeight;

@end

NS_ASSUME_NONNULL_END
