//
//  LYPSActionSheetView.h
//  LYPopupView
//
//  Created by jianyue on 2020/3/23.
//  Copyright © 2020 Famy. All rights reserved.
//

#import "LYPBaseActionSheetView.h"

#import "LYPSystemAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LYPSActionSheetView : LYPBaseActionSheetView

- (void)addAction:(LYPSystemAlertAction *)action;

@property (nonatomic, readonly) NSArray<LYPSystemAlertAction *> *actions;

@end


@interface LYPSActionSheetViewCell : UICollectionViewCell

@property(nonatomic, strong) UILabel *textLabel;

@property(nonatomic, strong) LYPSystemAlertAction *action;

@end

NS_ASSUME_NONNULL_END
