//
//  LYPBaseView.m
//  LYPopupView
//
//  Created by jianyue on 2020/3/20.
//  Copyright © 2020 Famy. All rights reserved.
//

#import "LYPBaseView.h"


@interface LYPBaseView ()

/// 黑色背景
@property(nonatomic, strong) UIButton *backgroundBtn;

@property(nonatomic, copy) DismissHandler dismissHandler;


@end

@implementation LYPBaseView

- (UIButton *)backgroundBtn {
    if (!_backgroundBtn) {
        _backgroundBtn = [[UIButton alloc] init];
        [_backgroundBtn addTarget:self action:@selector(backgroundBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backgroundBtn;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self __setupUI];
        
        [self __setupUIFrame];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    // 背景透明度渐变
    [self willPresent];
    self.backgroundBtn.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.25 animations:^{
        [weakSelf animatePresent];
        weakSelf.backgroundBtn.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    } completion:^(BOOL finished) {
        [weakSelf finishedPresent];
    }];
}
- (void)__setupUI {
    [self addSubview:self.backgroundBtn];
}

- (void)__setupUIFrame {
    [self.backgroundBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
}

/// 是否关闭 在点击背景时
- (void)setShouldDismissOnTouchBackgound:(BOOL)shouldDismissOnTouchBackgound {
    _shouldDismissOnTouchBackgound = shouldDismissOnTouchBackgound;
    
    [self.backgroundBtn setUserInteractionEnabled:shouldDismissOnTouchBackgound];
}

/// 显示
- (void)present {
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow addSubview:self];
    self.frame = keyWindow.bounds;
}

- (void)presentOnView:(UIView *)view {
    [view addSubview:self];
    self.frame = view.bounds;
}

/// 移除消失
- (void)dismiss {
    // 背景透明度渐变
    [self willDismiss];
    self.backgroundBtn.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.25 animations:^{
        [weakSelf animateDismiss];
        weakSelf.backgroundBtn.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
    } completion:^(BOOL finished) {
        [weakSelf finishedDismiss];
        if (weakSelf.dismissHandler != nil) {
            weakSelf.dismissHandler();
        }
        [weakSelf removeFromSuperview];
    }];
}

/// 弹窗消失回调
- (void)dismiss:(DismissHandler)handler {
    self.dismissHandler = handler;
//    self.dismi
}

- (void)willPresent{}
- (void)animatePresent{}
- (void)finishedPresent{}

- (void)willDismiss{}
- (void)animateDismiss{}
- (void)finishedDismiss{}

/// beijing
- (void)backgroundBtnAction {
    // 移除
    [self dismiss];
}














@end
