//
//  LYPSTextAlertView.h
//  LYPopupView
//
//  Created by jianyue on 2020/3/20.
//  Copyright © 2020 Famy. All rights reserved.
//

#import "LYPSystemAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LYPSTextAlertView : LYPSystemAlertView

/// 文本
@property(nonatomic, copy) NSString *message;



//message富文本

@end

NS_ASSUME_NONNULL_END
