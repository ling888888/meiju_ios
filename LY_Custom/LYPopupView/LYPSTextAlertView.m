//
//  LYPSTextAlertView.m
//  LYPopupView
//
//  Created by jianyue on 2020/3/20.
//  Copyright © 2020 Famy. All rights reserved.
//

#import "LYPSTextAlertView.h"

@interface LYPSTextAlertView ()

@property(nonatomic, strong) UILabel *messageLabel;



@end

@implementation LYPSTextAlertView

- (UILabel *)messageLabel {
    if (!_messageLabel) {
        _messageLabel = [[UILabel alloc] init];
        _messageLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _messageLabel.font = [UIFont mediumFontOfSize:15];
        _messageLabel.numberOfLines = 0;
    }
    return _messageLabel;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.messageLabel];
        [self.messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 28, 0, 28));
        }];
    }
    return self;
}

- (void)setMessage:(NSString *)message {
    _message = message;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:message];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    [paragraphStyle setLineSpacing:7];
//    [paragraphStyle setLineSpacing:(lineSpacing-(label.font.lineHeight - label.font.pointSize))];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [message length])];
//    [attributedString addAttribute:NSTextAlignment value:paragraphStyle range:NSMakeRange(0, [message length])];
    self.messageLabel.attributedText = attributedString;
    
//    self.messageLabel.text = message;
}


@end
