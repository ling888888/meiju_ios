//
//  LY_TabBarController.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/7.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_TabBarController.h"
#import "LY_BaseNavigationController.h"
#import "LY_HomeViewController.h"
#import "LY_ChatContainerController.h"
#import "MeViewController.h"
#import "HotClanHomeViewController.h"
#import "MagicBoxViewController.h"
#import "MagicBoxContainerViewController.h"
#import "OListHomeController.h"
@interface LY_TabBarController ()

@property (nonatomic ,strong) NSMutableArray * imagesArray;

@property (nonatomic, strong) UIView * lastSelectedView; // 上一次选择的view
@end

@implementation LY_TabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBar.backgroundColor = [UIColor whiteColor];
    
    //meiju
    OListHomeController *listController = [[OListHomeController alloc] init];
    LY_BaseNavigationController *listNaviC = [[LY_BaseNavigationController alloc] initWithRootViewController:listController];
    listNaviC.tabBarItem.image = [[UIImage imageNamed:@"mic_meiju_weixuan"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];;
    listNaviC.tabBarItem.selectedImage = [[UIImage imageNamed:@"mic_meiju_weixuan"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    listNaviC.tabBarItem.title = @"美局";
    
//    HotClanHomeViewController *clanViewController = [[HotClanHomeViewController alloc] init];
//    LY_BaseNavigationController *clanNaviC = [[LY_BaseNavigationController alloc] initWithRootViewController:clanViewController];
//    clanNaviC.tabBarItem.image = [[UIImage imageNamed:@"ic_tab_shouye"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];;
//    clanNaviC.tabBarItem.selectedImage = [[UIImage imageNamed:@"ic_shouye"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];;
//    clanNaviC.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
    
//    // 直播
//    LY_HomeViewController *homeViewController = [[LY_HomeViewController alloc] init];
//    LY_BaseNavigationController *homeNaviC = [[LY_BaseNavigationController alloc] initWithRootViewController:homeViewController];
//    homeNaviC.tabBarItem.image = [[UIImage imageNamed:@"ic_tab_live"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];;
//    homeNaviC.tabBarItem.selectedImage = [[UIImage imageNamed:@"ic_live"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];;
//    homeNaviC.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
    
    // pet
//    MagicBoxContainerViewController * magicVC = [[MagicBoxContainerViewController alloc]init];
//    LY_BaseNavigationController *magicNaviC = [[LY_BaseNavigationController alloc] initWithRootViewController:magicVC];
//    magicNaviC.tabBarItem.image = [[UIImage imageNamed:@"ic_tab_chongwu"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];;
//    magicNaviC.tabBarItem.selectedImage = [[UIImage imageNamed:@"ic_tab_chongwu1"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    magicNaviC.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
    
    // chat
    LY_ChatContainerController *chatContainerController = [[LY_ChatContainerController alloc] init];
    LY_BaseNavigationController *chatContainerNaviC = [[LY_BaseNavigationController alloc] initWithRootViewController:chatContainerController];
    chatContainerNaviC.tabBarItem.image = [[UIImage imageNamed:@"ic_xiaoxi_weixuan"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];;
    chatContainerNaviC.tabBarItem.selectedImage = [[UIImage imageNamed:@"ic_xiapxi_yixuan"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];;
    chatContainerNaviC.tabBarItem.title = @"消息";
//    chatContainerNaviC.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
    
    // 我的
    MeViewController *mineViewController = [[MeViewController alloc] init];
    LY_BaseNavigationController *mineNaviC = [[LY_BaseNavigationController alloc] initWithRootViewController:mineViewController];
    mineNaviC.tabBarItem.image = [[UIImage imageNamed:@"ic_tab_wode"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    mineNaviC.tabBarItem.selectedImage = [[UIImage imageNamed:@"ic_wode"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];;
    mineNaviC.tabBarItem.title = @"我的";
    
    self.viewControllers = @[listNaviC, chatContainerNaviC, mineNaviC];
    
    self.selectedIndex = 0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMessageBadgeValue:) name:@"UpdateMessageBadgeValueNotification" object:nil];
    
}


// 设置聊天未读消息数
- (void)updateMessageBadgeValue:(NSNotification *)noti {
    int unreadCount = [RCIMClient sharedRCIMClient].getTotalUnreadCount;
    dispatch_async(dispatch_get_main_queue(), ^{
        UITabBarItem *item = self.tabBar.items[unreadCountTabIndex];
        item.badgeValue = unreadCount > 0 ? [NSString stringWithFormat:@"%d", unreadCount] : nil;
    });
}

@end
