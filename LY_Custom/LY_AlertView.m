//
//  LY_AlertView.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/13.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_AlertView.h"

@interface LY_AlertView ()

/// 黑色背景视图
@property(nonatomic, strong) UIButton *backgroundBtn;

/// 容器视图（包含：topToolBar、contentView）
@property(nonatomic, strong) UIView *containerView;

/// 顶部工具栏（包含：标题，取消，完成）
@property(nonatomic, strong) UIView *topToolBar;

/// 标题文本
@property(nonatomic, strong) UILabel *titleLabel;

/// 取消按钮
@property(nonatomic, strong) UIButton *cancelBtn;

/// 完成按钮
@property(nonatomic, strong) UIButton *doneBtn;

/// 内容高度，包含
@property(nonatomic, assign) CGFloat containerSafeHeight;

@end

@implementation LY_AlertView


- (UIButton *)backgroundBtn {
    if (!_backgroundBtn) {
        _backgroundBtn = [[UIButton alloc] init];
        [_backgroundBtn addTarget:self action:@selector(backgroundBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backgroundBtn;
}

- (UIView *)containerView {
    if (!_containerView) {
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor whiteColor];
    }
    return _containerView;
}

- (UIView *)topToolBar {
    if (!_topToolBar) {
        _topToolBar = [[UIView alloc] init];
    }
    return _topToolBar;
}

- (UIView *)contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] init];
    }
    return _contentView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont mediumFontOfSize:18];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#333333"];
    }
    return _titleLabel;
}

- (UIButton *)cancelBtn {
    if (!_cancelBtn) {
        _cancelBtn = [[UIButton alloc] init];
        _cancelBtn.titleLabel.font = [UIFont mediumFontOfSize:15];
        [_cancelBtn setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateNormal];
        [_cancelBtn setTitle:@"取消".localized forState:UIControlStateNormal];
        _cancelBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 15);
        [_cancelBtn addTarget:self action:@selector(cancelEvent) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelBtn;
}

- (UIButton *)doneBtn {
    if (!_doneBtn) {
        _doneBtn = [[UIButton alloc] init];
        _doneBtn.titleLabel.font = [UIFont mediumFontOfSize:15];
        [_doneBtn setTitleColor:[UIColor colorWithHexString:@"#00D4A0"] forState:UIControlStateNormal];
        [_doneBtn setTitle:@"确定".localized forState:UIControlStateNormal];
        _doneBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 15);
        [_doneBtn addTarget:self action:@selector(doneEvent) forControlEvents:UIControlEventTouchUpInside];
    }
    return _doneBtn;
}

- (instancetype)initWithStyle:(LY_AlertViewStyle)style {
    if (self = [super initWithFrame:CGRectZero]) {
        self.style = style;
        
        [self _setupUI];
        
        [self _setupUIFrame];
        
        [self _setupDefaultData];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    [self.containerView addRounded:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:10];
}

- (void)_setupUI {
    [self addSubview:self.backgroundBtn];
    [self addSubview:self.containerView];
    [self.containerView addSubview:self.contentView];
}

- (void)_setupUIFrame {
    [self.backgroundBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
    
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.mas_equalTo(self.containerView);
//        make.height.mas_equalTo(self.containerHeight);
        if ([UIDevice currentDevice].is_iPhoneX && self.style == LY_AlertViewStyleActionSheet) {
            make.bottom.mas_equalTo(self.containerView).offset(-34);
        } else {
            make.bottom.mas_equalTo(self.containerView);
        }
    }];
}

- (void)_setupDefaultData {
    // 配置内容高度，适配安全区域
    if ([UIDevice currentDevice].is_iPhoneX && self.style == LY_AlertViewStyleActionSheet) {
        self.containerSafeHeight = self.containerHeight + 34;
    } else {
        self.containerSafeHeight = self.containerHeight;
    }
}

- (void)alertPresent {
    CGFloat contentViewW = 300.0;
    CGFloat contentViewH = self.containerSafeHeight;
    CGFloat contentViewX = (kScreenW - 300) / 2;
    CGFloat contentViewY = (kScreenH - self.containerSafeHeight) / 2;
    self.containerView.frame = CGRectMake(contentViewX, contentViewY, contentViewW, contentViewH);
    self.containerView.alpha = 0.0;
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        weakSelf.containerView.alpha = 1.0;
    } completion:^(BOOL finished) {
    }];
}

- (void)actionSheetPresent {
    self.containerView.frame = CGRectMake(0, kScreenH, kScreenW, self.containerSafeHeight);
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        weakSelf.containerView.frame = CGRectMake(0, kScreenH - weakSelf.containerSafeHeight, kScreenW, weakSelf.containerSafeHeight);
    } completion:^(BOOL finished) {
    }];
}

- (void)alertDismiss {
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        weakSelf.containerView.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (weakSelf.dismissHandler != nil) {
            weakSelf.dismissHandler();
        }
        [weakSelf removeFromSuperview];
    }];
}

- (void)actionSheetDismiss {
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        weakSelf.containerView.frame = CGRectMake(0, kScreenH, kScreenW, weakSelf.containerSafeHeight);
    } completion:^(BOOL finished) {
        if (weakSelf.dismissHandler != nil) {
            weakSelf.dismissHandler();
        }
        [weakSelf removeFromSuperview];
    }];
}

- (void)present {
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow addSubview:self];
    self.frame = keyWindow.bounds;
    
    // 背景透明度渐变
    self.backgroundBtn.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        weakSelf.backgroundBtn.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    }];
    
    // 判断样式
    if (self.style == LY_AlertViewStyleActionSheet) {
        // 底部内容视图从下而上划出
        [self actionSheetPresent];
    } else {
        // 底部内容视图从中心出现
        [self alertPresent];
    }
}

- (void)dismiss {
    // 背景透明度渐变
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        weakSelf.backgroundBtn.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
    } completion:^(BOOL finished) {
    }];
    
    // 判断样式
    if (self.style == LY_AlertViewStyleActionSheet) {
        // 底部内容视图从下而上划出
        [self actionSheetDismiss];
    } else {
        // 底部内容视图从中心出现
        [self alertDismiss];
    }
}

- (void)showTopToolBarWithTitle:(NSString *)title {
    [self.containerView addSubview:self.topToolBar];
    [self.topToolBar addSubview:self.titleLabel];
    [self.topToolBar addSubview:self.cancelBtn];
    [self.topToolBar addSubview:self.doneBtn];
    
//    self.contentView.frame = CGRectMake(0, 40, kScreenW, self.containerHeight - 40);
//    [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(UIEdgeInsetsMake(40, 0, 0, 0));
//    }];
    
    [self.topToolBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.mas_equalTo(self.containerView);
        make.height.mas_equalTo(40);
    }];
    
    [self.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topToolBar.mas_bottom);
        make.leading.trailing.mas_equalTo(self.containerView);
        if ([UIDevice currentDevice].is_iPhoneX && self.style == LY_AlertViewStyleActionSheet) {
            make.bottom.mas_equalTo(self.containerView).offset(-34);
        } else {
            make.bottom.mas_equalTo(self.containerView);
        }
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.topToolBar);
        make.top.bottom.mas_equalTo(self.topToolBar);
    }];
    
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.topToolBar);
        make.top.bottom.mas_equalTo(self.topToolBar);
    }];
    
    [self.doneBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(self.topToolBar);
        make.top.bottom.mas_equalTo(self.topToolBar);
    }];
    
    self.titleLabel.text = title;
}

- (void)done:(DoneHandler)handler {
    self.doneHandler = handler;
}

- (void)finishedDismiss:(DismissHandler)handler {
    self.dismissHandler = handler;
}

- (void)backgroundBtnAction {
    
    // 关闭弹窗
    [self dismiss];
}

/// 取消事件
- (void)cancelEvent {
    // 移除当前弹窗
    [self dismiss];
}

/// 完成事件
- (void)doneEvent {
    // 回调
    
    // 移除当前弹窗
    [self dismiss];
}

///// 取消按钮点击事件
//- (void)cancelBtnAction {
//
//    [self dismiss];
//}
//
///// 完成按钮点击事件
//- (void)doneBtnAction {
//    // 数据回调
////    if (self.selectedHandler) {
//        self.selectedHandler(self.selectedIndex, self.itemArray[self.selectedIndex]);
////    }
//    // 移除当前弹窗
//    [self dismiss];
//}

@end
