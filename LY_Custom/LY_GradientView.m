//
//  LY_GradientView.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/27.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_GradientView.h"

@implementation LY_GradientView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initialize];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self initialize];
}

- (void)initialize {
    self.direction = UIImageGradientColorsDirectionHorizontal;
    self.userInteractionEnabled = true;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.image = [[UIImage alloc] initWithGradient:self.colors locations:self.locations size:self.bounds.size direction:self.direction];
}

- (void)updateColors {
    [self layoutSubviews];
}

@end
