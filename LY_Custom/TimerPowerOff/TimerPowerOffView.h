//
//  TimerPowerOffView.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/12.
//  Copyright © 2020 WYB. All rights reserved.
//


/*  使用指南:------------------
 1. 添加通知监听
 [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(refreshTimer:) name:@"TimerPowerOffNotificationName" object:nil];
 
 2.实现监听方法
 - (void)refreshTimer:(NSNotification *)notification {
     // 根据 notification.userInfo[@"surplusTimeStr"] 字段 修改页面数据...
 
     // 判断 surplusTimeStr == "00:00" 时 执行关闭播放器动作
 }
 
notification.userInfo 参数如下:
 
cancel -> 取消定时关闭  1(true) 或 0(false)
 
totalSecond -> 设置定时关闭（总）秒数
 
surplusSecond -> 倒计时剩余秒数
 
surplusTimeStr -> 倒计时剩余时间字符串（格式化好的）
 
isgoing -> 是否正在进行  1(true) 或 0(false)
 
结果示例代码:
{
 isgoing = 1;
 surplusSecond = 294;
 surplusTimeStr = "04:54";
 totalSecond = 300;
}

{
 isgoing = 0;
}
{
 cancel = 1;
}
 */


#import "LY_AlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface TimerPowerOffView : LY_AlertView

@end

@interface TimerPowerOffTableViewCell : UITableViewCell

- (void)removeSelectedStyle;

- (void)addSelectedStyleWithTimeStr:(NSString *)timeStr;

- (void)setTitle:(NSString *)title;


@end

NS_ASSUME_NONNULL_END
