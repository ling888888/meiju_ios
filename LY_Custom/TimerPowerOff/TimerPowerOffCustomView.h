//
//  TimerPowerOffCustomView.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/12.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_AlertView.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^SelectionChangedHandler)(int row, int component, NSString * value);

@interface TimerPowerOffCustomView : LY_AlertView

@end

NS_ASSUME_NONNULL_END
