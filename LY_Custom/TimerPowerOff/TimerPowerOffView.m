//
//  TimerPowerOffView.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/12.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "TimerPowerOffView.h"
#import "TimerPowerOffMgr.h"
#import "TimerPowerOffCustomView.h"

@interface TimerPowerOffView () <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) UITableView *tableView;

@property(nonatomic, strong) UIView *lineView;

@property(nonatomic, strong) UIButton *closeBtn;

@property(nonatomic, strong) NSArray *titleArray;

@property(nonatomic, strong) TimerPowerOffTableViewCell *currentSelectedCell;

@end

@implementation TimerPowerOffView

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.rowHeight = 50;
        _tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15);
        _tableView.separatorColor = [UIColor colorWithHexString:@"#EFEFEF"];
        _tableView.scrollEnabled = false;
        [_tableView registerClass:[TimerPowerOffTableViewCell class] forCellReuseIdentifier:@"TimerPowerOffIdentifier"];
    }
    return _tableView;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.1];
    }
    return _lineView;
}

- (UIButton *)closeBtn {
    if (!_closeBtn) {
        _closeBtn = [[UIButton alloc] init];
        [_closeBtn setTitle:@"关闭".localized forState:UIControlStateNormal];
        [_closeBtn setTitleColor:[UIColor colorWithHexString:@"#1A1A1A"] forState:UIControlStateNormal];
        _closeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_closeBtn addTarget:self action:@selector(closeBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeBtn;
}

- (NSArray *)titleArray {
    if (!_titleArray) {
        _titleArray = @[@"不开启", @"10分钟后", @"20分钟后", @"30分钟后", @"60分钟后", @"90分钟后", @"自定义"];
    }
    return _titleArray;
}

- (CGFloat)containerHeight {
    return 415;
}

- (instancetype)initWithStyle:(LY_AlertViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
        [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(refreshTimer:) name:@"TimerPowerOffNotificationName" object:nil];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    [self setupDefaultData];
}

- (void)refreshTimer:(NSNotification *)notification {
    NSString *surplusTimeStr = notification.userInfo[@"surplusTimeStr"];
    [self.currentSelectedCell addSelectedStyleWithTimeStr:surplusTimeStr];
}

- (void)setupUI {
    [self.contentView addSubview:self.tableView];
    [self.contentView addSubview:self.lineView];
    [self.contentView addSubview:self.closeBtn];
}

- (void)setupUIFrame {
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(12);
        make.leading.trailing.mas_equalTo(self.contentView);
        make.height.mas_equalTo(350);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(1);
        make.leading.trailing.mas_equalTo(self.contentView);
        make.bottom.mas_equalTo(self.tableView);
    }];
    
    [self.closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.contentView);
        make.leading.trailing.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.lineView);
    }];
}

- (void)setupDefaultData {
    NSString *surplusTimeStr;
    if ([TimerPowerOffMgr isValid]) {
        surplusTimeStr = [TimerPowerOffMgr shareInstance].surplusTimeStr;
    }
    
    int index = [TimerPowerOffMgr shareInstance].index;
    
    TimerPowerOffTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    self.currentSelectedCell = cell;
    [cell addSelectedStyleWithTimeStr:surplusTimeStr];
}

- (void)closeBtnAction {
    [self dismiss];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.titleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TimerPowerOffTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TimerPowerOffIdentifier" forIndexPath:indexPath];
    
    [cell setTitle:self.titleArray[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *surplusTimeStr;
    switch (indexPath.row) {
        case 0: {
            // 不开启，清除定时关闭
            [TimerPowerOffMgr tearDown];
            //[MobClick event:@"FM1_9_57"];
            break;
        }
        case 1: {
            // 10分钟
            [[TimerPowerOffMgr shareInstance] startWithSecond:600 index:1];
            surplusTimeStr = [TimerPowerOffMgr shareInstance].surplusTimeStr;
            //[MobClick event:@"FM1_9_58"];
            break;
        }
        case 2: {
            // 20分钟
            [[TimerPowerOffMgr shareInstance] startWithSecond:1200 index:2];
            surplusTimeStr = [TimerPowerOffMgr shareInstance].surplusTimeStr;
            //[MobClick event:@"FM1_9_59"];
            break;
        }
        case 3: {
            // 30分钟
            [[TimerPowerOffMgr shareInstance] startWithSecond:1800 index:3];
            surplusTimeStr = [TimerPowerOffMgr shareInstance].surplusTimeStr;
            //[MobClick event:@"FM1_9_60"];
            break;
        }
        case 4: {
            // 60分钟
            [[TimerPowerOffMgr shareInstance] startWithSecond:3600 index:4];
            surplusTimeStr = [TimerPowerOffMgr shareInstance].surplusTimeStr;
            //[MobClick event:@"FM1_9_61"];
            break;
        }
        case 5: {
            // 90分钟
            [[TimerPowerOffMgr shareInstance] startWithSecond:5400 index:5];
            surplusTimeStr = [TimerPowerOffMgr shareInstance].surplusTimeStr;
            //[MobClick event:@"FM1_9_62"];
            break;
        }
        case 6: {
            // 自定义
            TimerPowerOffCustomView *customView = [[TimerPowerOffCustomView alloc] initWithStyle:self.style];
            [customView showTopToolBarWithTitle:@"选择时间".localized];
            [customView present];
            //[MobClick event:@"FM1_9_63"];
            [self dismiss];
            break;
        }
        default:
            break;
    }
    
    [self.currentSelectedCell removeSelectedStyle];
    TimerPowerOffTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    self.currentSelectedCell = cell;
    [cell addSelectedStyleWithTimeStr:surplusTimeStr];
    
    [self closeBtnAction];
}

@end


@interface TimerPowerOffTableViewCell ()

@property(nonatomic, strong) UILabel *titleLabel;

@property(nonatomic, strong) UILabel *countdownLabel;

@property(nonatomic, strong) UIImageView *selectedIconView;

@end

@implementation TimerPowerOffTableViewCell

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#333333"];
        _titleLabel.font = [UIFont mediumFontOfSize:15];
    }
    return _titleLabel;
}


- (UILabel *)countdownLabel {
    if (!_countdownLabel) {
        _countdownLabel = [[UILabel alloc] init];
        _countdownLabel.textColor = [UIColor colorWithHexString:@"#00D4A0"];
        _countdownLabel.font = [UIFont mediumFontOfSize:14];
    }
    return _countdownLabel;
}

- (UIImageView *)selectedIconView {
    if (!_selectedIconView) {
        _selectedIconView = [[UIImageView alloc] init];
        _selectedIconView.contentMode = UIViewContentModeScaleAspectFit;
        _selectedIconView.image = [UIImage imageNamed:@"ic_fenxiang_duoxuan"];
    }
    return _selectedIconView;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.titleLabel];
    [self addSubview:self.countdownLabel];
    [self addSubview:self.selectedIconView];
}

- (void)setupUIFrame {
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self);
        make.leading.mas_equalTo(15);
    }];
    
    [self.selectedIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self);
        make.trailing.mas_equalTo(-15);
    }];
    
    [self.countdownLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self);
        make.trailing.mas_equalTo(self.selectedIconView.mas_leading).offset(-11);
    }];
}

- (void)setTitle:(NSString *)title {
    self.titleLabel.text = title.localized;
}

- (void)removeSelectedStyle {
    self.selectedIconView.image = [UIImage imageNamed:@"ic_fenxiang_duoxuan"];
    [self.countdownLabel setHidden:true];
}

- (void)addSelectedStyleWithTimeStr:(NSString *)timeStr {
    self.selectedIconView.image = [UIImage imageNamed:@"ic_fenxiang_duoxuan_s"];
    [self.countdownLabel setHidden:false];
    if (timeStr != nil && ![timeStr isEqualToString:@""]) {
        self.countdownLabel.text = [NSString stringWithFormat:@"倒计时%@".localized, timeStr];
    }
    
}

@end
