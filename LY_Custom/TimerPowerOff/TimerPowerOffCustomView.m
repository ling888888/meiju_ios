//
//  TimerPowerOffCustomView.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/12.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "TimerPowerOffCustomView.h"
#import "TimerPowerOffMgr.h"

@interface TimerPowerOffCustomView () <UIPickerViewDataSource, UIPickerViewDelegate>

@property(nonatomic, strong) UIPickerView *pickerView;

@property(nonatomic, copy) NSArray *hourList;

@property(nonatomic, copy) NSArray *minuteList;

@property(nonatomic, copy) NSNumber *selectedHour;

@property(nonatomic, copy) NSNumber *selectedMinute;

@end

@implementation TimerPowerOffCustomView

- (UIPickerView *)pickerView {
    if (!_pickerView) {
        _pickerView = [[UIPickerView alloc] init];
        _pickerView.dataSource = self;
        _pickerView.delegate = self;
        _pickerView.backgroundColor = [UIColor whiteColor];
    }
    return _pickerView;
}

- (NSArray *)hourList {
    if (!_hourList) {
        NSMutableArray *hourList = [NSMutableArray array];
        for (int i = 0; i<24; i ++) {
            [hourList addObject:[NSNumber numberWithInt:i]];
        }
        _hourList = hourList;
    }
    return _hourList;
}

- (NSArray *)minuteList {
    if (!_minuteList) {
        NSMutableArray *minuteList = [NSMutableArray array];
        for (int i = 0; i<12; i ++) {
            [minuteList addObject:[NSNumber numberWithInt:i * 5]];
        }
        _minuteList = minuteList;
    }
    return _minuteList;
}

- (CGFloat)containerHeight {
    return 250;
}

- (instancetype)initWithStyle:(LY_AlertViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    // 设置默认
    [self.pickerView selectRow:1 inComponent:1 animated:true];
    self.selectedHour = self.hourList[0];
    self.selectedMinute = self.minuteList[1];
}

- (void)setupUI {
    [self.contentView addSubview:self.pickerView];
}

- (void)setupUIFrame {
    [self.pickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.contentView);
    }];
}

- (void)cancelEvetn {
    //[MobClick event:@"FM1_9_64"];
    
    [super cancelEvent];
}

- (void)doneEvent {
    //[MobClick event:@"FM1_9_65"];
    
    int second = self.selectedHour.intValue * 60 * 60 + self.selectedMinute.intValue * 60;
    [[TimerPowerOffMgr shareInstance] startWithSecond:second index:6];
    
    [super doneEvent];
}
///// 取消按钮点击事件
//- (void)cancelBtnAction {
//
//    // 移除当前弹窗
//    [self dismiss];
//}
//
///// 完成按钮点击事件
//- (void)doneBtnAction {
//
    
//    // 移除当前弹窗
//    [self dismiss];
//}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        return self.hourList.count;
    } else {
        return self.minuteList.count;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0) {
        NSNumber *hourNumber = self.hourList[row];
        NSString *hourStr = [NSString stringWithFormat:@"%@小时".localized, hourNumber.stringValue];
        return hourStr;
    } else {
        NSNumber *minuteNumber = self.minuteList[row];
        NSString *minuteStr = [NSString stringWithFormat:@"%@分钟".localized, minuteNumber.stringValue];
        return minuteStr;
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 50;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    //[MobClick event:@"FM1_9_66"];
    
    if (component == 0) {
        self.selectedHour = self.hourList[row];
    } else {
        self.selectedMinute = self.minuteList[row];
    }
    
    if ([self.selectedHour isEqualToNumber: @(0)] && [self.selectedMinute isEqualToNumber:@(0)]) {
        [pickerView selectRow:1 inComponent:1 animated:true];
        self.selectedHour = self.hourList[0];
        self.selectedMinute = self.minuteList[1];
        return;
    }
}


@end
