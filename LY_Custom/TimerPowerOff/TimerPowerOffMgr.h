//
//  TimerPowerOffMgr.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/12.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TimerPowerOffMgr : NSObject

/// 设置定时关闭（总）秒数
@property(nonatomic, assign) int totalSecond;

/// 倒计时剩余秒数
@property(nonatomic, assign) int surplusSecond;

/// 倒计时剩余时间字符串
@property(nonatomic, copy) NSString *surplusTimeStr;

/// 选择的索引
@property(nonatomic, assign) int index;

+ (instancetype)shareInstance;

/// 销毁单利对象，关闭定时器
+ (void)tearDown;

/// 判断定时器是否开启
+ (BOOL)isValid;



- (void)startWithSecond:(int)second index:(int)index;

@end

NS_ASSUME_NONNULL_END
