//
//  TimerPowerOffMgr.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/12.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "TimerPowerOffMgr.h"



@interface TimerPowerOffMgr ()

/// 定时器
@property(nonatomic, strong) NSTimer *timer;

@end

static TimerPowerOffMgr *_singleInstance = nil;
static dispatch_once_t onceToken;

@implementation TimerPowerOffMgr

+ (instancetype)shareInstance
{
    dispatch_once(&onceToken, ^{
        if (_singleInstance == nil) {
            _singleInstance = [[self alloc]init];
        }
    });
    return _singleInstance;
}

+ (void)tearDown {
    if (_singleInstance.timer != nil) {
        [_singleInstance.timer invalidate];
        _singleInstance.timer = nil;
    }
    // 发送关闭通知
    NSDictionary *dict = @{
        @"cancel": @(true)
    };
    [NSNotificationCenter.defaultCenter postNotificationName:@"TimerPowerOffNotificationName" object:nil userInfo:dict];
    
    _singleInstance = nil;
    onceToken = 0l;
}

+ (BOOL)isValid {
    if (_singleInstance == nil) {
        return false;
    }
    if (_singleInstance.timer == nil) {
        return false;
    }
    if (!_singleInstance.timer.valid) {
        return false;
    }
    return true;
}

- (void)startWithSecond:(int)second index:(int)index {
    self.totalSecond = second;
    self.surplusSecond = second;
    self.index = index;
    // 清除之前的定时器
    if (self.timer != nil) {
        [self.timer invalidate];
        self.timer = nil;
    }
    // 创建新定时器
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(refreshPlayTime) userInfo:nil repeats:true];
}


/// 定时器事件
- (void)refreshPlayTime {
    self.surplusSecond -= 1;
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    if (self.surplusSecond == 0) {
        self.index = 0;
        [dict setValue:@(false) forKey:@"isgoing"];
        [dict setValue:@(false) forKey:@"cancel"];
    } else {
        [dict setValue:@(true) forKey:@"isgoing"];
        [dict setValue:@(false) forKey:@"cancel"];
        [dict setValue:@(self.totalSecond) forKey:@"totalSecond"];
        [dict setValue:@(self.surplusSecond) forKey:@"surplusSecond"];
        [dict setValue:self.surplusTimeStr forKey:@"surplusTimeStr"];
    }
    
    [NSNotificationCenter.defaultCenter postNotificationName:@"TimerPowerOffNotificationName" object:nil userInfo:dict];
    
    if (self.surplusSecond == 0) {
        [self.timer invalidate];
    }
}

- (NSString *)surplusTimeStr {
    int minuteRemaining = floorf(self.surplusSecond / 60);
    int secondRemaining = self.surplusSecond % 60;
    
    NSString *timeStr = [NSString stringWithFormat:@"%02d:%02d", minuteRemaining, secondRemaining];
    
    return timeStr;
}


@end
