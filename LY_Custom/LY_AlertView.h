//
//  LY_AlertView.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/13.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, LY_AlertViewStyle) {
    LY_AlertViewStyleActionSheet = 0,
    LY_AlertViewStyleAlert
};

@interface LY_AlertView : UIView

typedef void (^DismissHandler)();
typedef void (^DoneHandler)(id _Nullable result);

/// 完成事件回调
@property(nonatomic, copy) DoneHandler doneHandler;

/// 移除事件回调
@property(nonatomic, copy) DismissHandler dismissHandler;

/// 内容视图
@property(nonatomic, strong) UIView *contentView;

/// 弹窗风格
@property(nonatomic, assign) LY_AlertViewStyle style;

/// 弹出窗高度，必须在子类中赋值（自动适配iPhoneX安全区域）
@property(nonatomic, assign, readonly) CGFloat containerHeight;


/// 初始化方法（initWithFrame失效）
- (instancetype)initWithStyle:(LY_AlertViewStyle)style;

/// 显示
- (void)present;

/// 移除消失
- (void)dismiss;

/// 设置显示顶部栏（默认不显示）
- (void)showTopToolBarWithTitle:(NSString *)title;

/// 完成回调
- (void)done:(DoneHandler)handler;

/// 弹窗消失回调
- (void)finishedDismiss:(DismissHandler)handler;

// 子类继承使用，不推荐外部调用
/// 取消事件
- (void)cancelEvent;

/// 完成事件
- (void)doneEvent;



@end

NS_ASSUME_NONNULL_END
