//
//  LY_PickerView.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/5.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_AlertView.h"

NS_ASSUME_NONNULL_BEGIN

//typedef void (^SelectionChangedHandler)(int index, NSString * value);

@interface LY_PickerView : LY_AlertView

@property(nonatomic, strong) NSArray *itemArray;

@property(nonatomic, assign) int defaultIndex;


//- (void)selected:(SelectionChangedHandler)selectedHandler;

@end

NS_ASSUME_NONNULL_END
