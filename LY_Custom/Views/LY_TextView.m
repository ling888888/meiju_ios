//
//  LY_TextView.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/5.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_TextView.h"

@interface LY_TextView ()

///
@property(nonatomic, assign) CGRect surfaceFrame;

@end

@implementation LY_TextView

- (instancetype)initWithFrame:(CGRect)frame textContainer:(NSTextContainer *)textContainer {
    self = [super initWithFrame:frame textContainer:textContainer];
    if (self) {
        [self addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
        
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
        
    }
    return self;
}

- (void)dealloc
{
    [self removeObserver:self forKeyPath:@"contentSize"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"contentSize"]) {
        if (CGRectIsEmpty(self.surfaceFrame)) {
            
            self.surfaceFrame = self.frame;
        }
        // 实际被设置的高度
        CGFloat frameH = self.surfaceFrame.size.height;
        // 文本内容的高度
        CGFloat contentH = self.contentSize.height;
        // 判断内容是否超出Frame区域
        if (contentH > frameH) {
            // 超出了 （重新设置Frame）
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, contentH);
        } else {
            // 未超出  (使用外部Frame)
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, frameH);
        }
        // 内边距
        UIEdgeInsets offset = UIEdgeInsetsZero;
        if (self.contentSize.height <= self.surfaceFrame.size.height) {
            CGFloat offsetTop = (self.surfaceFrame.size.height - self.contentSize.height) / 2;
            offset = UIEdgeInsetsMake(offsetTop, 0, 0, 0);
        } else {
            offset = UIEdgeInsetsZero;
        }
        self.contentInset = offset;
        
        self.changeHeightBlock(self.bounds.size.height);
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}


@end
