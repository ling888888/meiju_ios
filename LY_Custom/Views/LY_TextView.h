//
//  LY_TextView.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/5.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_TextView : UITextView
typedef void (^LYTextViewChangeHeightCompletionBlock)(CGFloat);

@property (copy, nonatomic) LYTextViewChangeHeightCompletionBlock changeHeightBlock;

@end

NS_ASSUME_NONNULL_END
