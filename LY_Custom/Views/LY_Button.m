//
//  LY_Button.m
//  SimpleDate
//
//  Created by 杨宝龙 on 2020/5/18.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_Button.h"

@interface LY_Button ()

@property(nonatomic, strong) UILabel *titleLabel;

@property(nonatomic, strong) UIImageView *imageView;

@property(nonatomic, strong) UIImageView *backgroundImageView;

@end

@implementation LY_Button

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.font = [UIFont mediumFontOfSize:16];
    }
    return _titleLabel;
}

- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _imageView;
}

- (UIImageView *)backgroundImageView {
    if (!_backgroundImageView) {
        _backgroundImageView = [[UIImageView alloc] init];
        _backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _backgroundImageView;
}

- (instancetype)initWithImageTitleSpace:(CGFloat)imageTitleSpace {
    self = [self initWithImagePosition:LY_ButtonImagePositionTrailing imageTitleSpace:imageTitleSpace];
    
    return self;
}

- (instancetype)initWithImagePosition:(LY_ButtonImagePosition)imagePosition imageTitleSpace:(CGFloat)imageTitleSpace {
    self = [super initWithFrame:CGRectZero];
    if (self) {
        _imagePosition = imagePosition;
        _imageTitleSpace = imageTitleSpace;
        
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setupUI];
    
    [self setupUIFrame];
}

- (void)setupUI {
    [self addSubview:self.backgroundImageView];
    [self.backgroundImageView addSubview:self.titleLabel];
    [self.backgroundImageView addSubview:self.imageView];
}

- (void)setupUIFrame {
    [self.backgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.leading.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.trailing.mas_equalTo(0);
    }];
    // 没有判断样式，需switch区分
    switch (self.imagePosition) {
        case LY_ButtonImagePositionTop: {
            [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(0);
                make.size.mas_equalTo(CGSizeMake(0, 0));
            }];
            [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.imageView.mas_bottom).offset(self.imageTitleSpace).priorityHigh();
                make.leading.mas_lessThanOrEqualTo(0);
                make.trailing.mas_lessThanOrEqualTo(0);
                make.centerX.mas_equalTo(0);
            }];
            break;
        }
        case LY_ButtonImagePositionLeading: {
            
            break;
        }
        case LY_ButtonImagePositionBottom: {
            
            break;
        }
        case LY_ButtonImagePositionTrailing: {
            [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.leading.mas_equalTo(0);
                make.top.bottom.mas_equalTo(0);
            }];
            [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.leading.mas_equalTo(self.titleLabel.mas_trailing).offset(self.imageTitleSpace);
                make.top.bottom.mas_equalTo(0);
                make.trailing.mas_equalTo(0);
            }];
            break;
        }
        default:
            break;
    }
}

- (void)setTitle:(NSString *)title {
    _title = title;
    
    self.titleLabel.text = title;
}

- (void)setImage:(UIImage *)image {
    _image = image;
    
    self.imageView.image = image;
    switch (self.imagePosition) {
        case LY_ButtonImagePositionTop: {
            [self.imageView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(image.size);
            }];
            [self.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(self.backgroundImageView).offset(image.size.height / 2 + self.imageTitleSpace / 2);
            }];
            break;
        }
        case LY_ButtonImagePositionLeading: {
            
        }
        case LY_ButtonImagePositionBottom: {
            
            break;
        }
        case LY_ButtonImagePositionTrailing: {
            
            break;
        }
        default:
            break;
    }
}

- (void)setBackgroundImage:(UIImage *)backgroundImage {
    _backgroundImage = backgroundImage;
    
    self.backgroundImageView.image = backgroundImage;
}

- (void)setTitleColor:(UIColor *)titleColor {
    _titleColor = titleColor;
    
    self.titleLabel.textColor = titleColor;
}

- (void)setTitleFont:(UIFont *)titleFont {
    _titleFont = titleFont;
    
    self.titleLabel.font = titleFont;
}

- (void)setContentEdgeInsets:(UIEdgeInsets)contentEdgeInsets {
    _contentEdgeInsets = contentEdgeInsets;
    
    [self.backgroundImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(contentEdgeInsets.top);
        make.leading.mas_equalTo(contentEdgeInsets.left);
        make.bottom.mas_equalTo(-contentEdgeInsets.bottom);
        make.trailing.mas_equalTo(-contentEdgeInsets.right);
    }];
}



@end
