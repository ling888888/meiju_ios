//
//  LY_PickerView.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/5.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_PickerView.h"

@interface LY_PickerView () <UIPickerViewDataSource, UIPickerViewDelegate>

@property(nonatomic, strong) UIPickerView *pickerView;

//@property(nonatomic, copy) SelectionChangedHandler selectedHandler;

@property(nonatomic, assign) int selectedIndex;

@end

@implementation LY_PickerView

- (UIPickerView *)pickerView {
    if (!_pickerView) {
        _pickerView = [[UIPickerView alloc] init];
        _pickerView.dataSource = self;
        _pickerView.delegate = self;
        _pickerView.backgroundColor = [UIColor whiteColor];
    }
    return _pickerView;
}

- (CGFloat)containerHeight {
    return 250;
}

- (instancetype)initWithStyle:(LY_AlertViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    self.selectedIndex = self.defaultIndex;
    [self.pickerView selectRow:self.defaultIndex inComponent:0 animated:true];
}

- (void)setupUI {
    [self.contentView addSubview:self.pickerView];
}

- (void)setupUIFrame {
    [self.pickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.contentView);
    }];
}

///// 取消事件
//- (void)cancelEvent {
//
//}

/// 完成事件
- (void)doneEvent {
    [super doneEvent];
//    self.doneHandler(self.selectedIndex);
    self.doneHandler(@(self.selectedIndex));
//    self.doneHandler =  //[[NSDictionary alloc] init];//@{@"value":[NSNumber numberWithInt:self.selectedIndex]}; <#^(NSDictionary * _Nonnull result)#>
//    self.selectedIndex
}

//- (void)selected:(SelectionChangedHandler)selectedHandler {
//    self.selectedHandler = selectedHandler;
//}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.itemArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return self.itemArray[row];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 50;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.selectedIndex = row;
}

@end
