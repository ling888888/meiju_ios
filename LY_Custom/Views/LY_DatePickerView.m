//
//  LY_DatePickerView.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_DatePickerView.h"

@interface LY_DatePickerView ()


@property(nonatomic, strong) UIDatePicker *datePickerView;


@property(nonatomic, assign) int selectedIndex;

@end

@implementation LY_DatePickerView

- (UIDatePicker *)datePickerView {
    if (!_datePickerView) {
        _datePickerView = [[UIDatePicker alloc] init];
        _datePickerView.datePickerMode = UIDatePickerModeDate;
        //得到当前的时间
        NSDate * mydate = [NSDate date];
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *comps = nil;
        comps = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitMonth fromDate:mydate];
        NSDateComponents *adcomps = [[NSDateComponents alloc] init];
        [adcomps setYear:-15];
        [adcomps setMonth:0];
        [adcomps setDay:0];
        NSDate *newdate = [calendar dateByAddingComponents:adcomps toDate:mydate options:0];
        
        // 设置最大时间
        [_datePickerView setMaximumDate:newdate];
        
        // 设置当前显示时间
        [_datePickerView setDate:newdate animated:true];
        _datePickerView.locale = [[NSLocale alloc] initWithLocaleIdentifier:[SPDLanguageTool sharedInstance].currentLanguage];
    }
    return _datePickerView;
}

- (CGFloat)containerHeight {
    return 250;
}

- (instancetype)initWithStyle:(LY_AlertViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.datePickerView];
}

- (void)setupUIFrame {
    [self.datePickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.contentView);
    }];
}

- (void)doneEvent {
    [super doneEvent];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setTimeZone:[NSTimeZone localTimeZone]];
    format.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [format setTimeZone:[NSTimeZone localTimeZone]];
    [format setDateFormat:@"yyyy-MM-dd"];
    
    NSString *timeString = [format stringFromDate:self.datePickerView.date];
    
    self.doneHandler(timeString);
}

@end
