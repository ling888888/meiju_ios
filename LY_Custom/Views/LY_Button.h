//
//  LY_Button.h
//  SimpleDate
//
//  Created by 杨宝龙 on 2020/5/18.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, LY_ButtonImagePosition) {
    LY_ButtonImagePositionLeading   = 0,
    LY_ButtonImagePositionTrailing  = 1,
    LY_ButtonImagePositionTop    = 2,
    LY_ButtonImagePositionBottom = 3
};

@interface LY_Button : UIControl

/// 图片位置
@property(nonatomic, assign) LY_ButtonImagePosition imagePosition;

/// 图片和标题间距
@property(nonatomic, assign) CGFloat imageTitleSpace;

/// 标题
@property(nonatomic, copy) NSString *title;

/// 图片
@property(nonatomic, strong) UIImage *image;

/// 背景图片
@property(nonatomic, strong) UIImage *backgroundImage;

/// 标题颜色
@property(nonatomic, strong) UIColor *titleColor;

/// 标题字号
@property(nonatomic, copy) UIFont *titleFont;

/// 内边距
@property(nonatomic, assign) UIEdgeInsets contentEdgeInsets;


/// 初始化方法（默认中文图片居右，阿语居左）
/// @param imageTitleSpace 图片和标题间距
- (instancetype)initWithImageTitleSpace:(CGFloat)imageTitleSpace;

/// 初始化方法
/// @param imagePosition 图片位置
/// @param imageTitleSpace 图片和标题间距
- (instancetype)initWithImagePosition:(LY_ButtonImagePosition)imagePosition imageTitleSpace:(CGFloat)imageTitleSpace;

@end

NS_ASSUME_NONNULL_END
