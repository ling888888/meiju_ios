//
//  LY_RechargeConchCell.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/31.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class MyWalletListModel;

@interface LY_RechargeConchCell : UICollectionViewCell

@property (nonatomic, strong) MyWalletListModel *model;

@end

NS_ASSUME_NONNULL_END
