//
//  LY_RechargeGoldCell.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/31.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_RechargeGoldCell.h"
#import "SPButton.h"

@interface LY_RechargeGoldCell()

// 背景视图
@property(nonatomic, strong) UIView *bgView;

// 顶部红色文本
@property(nonatomic, strong) UILabel *topRedLabel;

// 金币按钮
@property(nonatomic, strong) SPButton *goldCoinBtn;

// 钻石按钮
@property(nonatomic, strong) SPButton *diamondBtn;

// 美元文本
@property(nonatomic, strong) UILabel *dollarLabel;

// 原价文本
@property(nonatomic, strong) UILabel *originalPriceLabel;


@end

@implementation LY_RechargeGoldCell

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = [UIColor whiteColor];
        
//        let layerView = UIView()
//        layerView.frame = CGRect(x: 135.5, y: 279, width: 105, height: 110)
        // shadowCode
        
        _bgView.layer.shadowColor = [UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:0.09].CGColor;
        _bgView.layer.shadowOffset = CGSizeMake(0, 0);
        _bgView.layer.shadowOpacity = 0.09;
        _bgView.layer.shadowRadius = 6;
        // layerFillCode
//        let layer = CALayer()
//        layer.frame = layerView.bounds
//        layer.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1).cgColor
//        layerView.layer.addSublayer(layer)
//        layerView.layer.cornerRadius = 5;
//        [_bgView.layer addSublayer:layerView];
    }
    return _bgView;
}

- (UILabel *)topRedLabel {
    if (!_topRedLabel) {
        _topRedLabel = [[UILabel alloc] init];
        _topRedLabel.backgroundColor = [UIColor colorWithHexString:@"#F8145B"];
        _topRedLabel.textColor = [UIColor whiteColor];
        _topRedLabel.font = [UIFont mediumFontOfSize:15];
    }
    return _topRedLabel;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.bgView];
    [self addSubview:self.topRedLabel];
}

- (void)setupUIFrame {
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(15, 5, 5, 5));
    }];
}

@end
