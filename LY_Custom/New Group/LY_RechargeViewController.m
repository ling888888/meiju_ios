//
//  LY_RechargeViewController.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/30.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_RechargeViewController.h"
#import "LY_RechargeHeaderView.h"
#import "LY_RechargeGoldCell.h"

@interface LY_RechargeViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property(nonatomic, strong) UICollectionView *collectionView;

@property(nonatomic, strong) LY_RechargeHeaderView *headerView;



@end

@implementation LY_RechargeViewController

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        
        flowLayout.minimumLineSpacing = 5;
        flowLayout.minimumInteritemSpacing = 5;
        CGFloat itemW = (kScreenW - 20) / 3;
        CGFloat itemH = itemW / 105 * 110;
        flowLayout.itemSize = CGSizeMake(itemW, itemH);
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor colorWithHexString:@"#F7F7F7"];
        [_collectionView registerClass:[LY_RechargeGoldCell class] forCellWithReuseIdentifier:@"RechargeGoldIdentifier"];
    }
    return _collectionView;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 6;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LY_RechargeGoldCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RechargeGoldIdentifier" forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor redColor];
    
    return cell;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 5, 0, 5);
}

- (LY_RechargeHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[LY_RechargeHeaderView alloc] init];
        _headerView.backgroundColor = [UIColor colorWithHexString:@"#F7F7F7"];
    }
    return _headerView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavigation];
    
    [self setupUI];
    
    [self setupUIFrame];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    [self.navigationController.navigationBar ly_setBackgroundColor:[[UIColor colorWithHexString:@"#6b2cf7"] colorWithAlphaComponent:0]];
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
//    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
//    [self.navigationController.navigationBar ly_setBackgroundColor:[[UIColor colorWithHexString:@"#00D4A0"] colorWithAlphaComponent:1]];
//    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
//    [self.navigationController.navigationBar setShadowImage:nil];
}

- (void)setupNavigation {
    self.title = @"充值".localized;
}

- (void)setupUI {
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F7F7F7"];
    
    [self.view addSubview:self.headerView];
    self.headerView.frame = CGRectMake(0, 0, kScreenW, kScreenW / 375 * 182 + 60);
    
    [self.view addSubview:self.collectionView];
    
    self.view.backgroundColor = [UIColor yellowColor];
}

- (void)setupUIFrame {
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.headerView.mas_bottom);
        make.leading.trailing.bottom.mas_equalTo(self.view);
    }];
}



@end
