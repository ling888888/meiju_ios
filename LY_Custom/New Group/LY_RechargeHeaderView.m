//
//  LY_RechargeHeaderView.m
//  SimpleDate-dis
//
//  Created by jianyue on 2020/3/31.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_RechargeHeaderView.h"
#import "SPButton.h"

@interface LY_RechargeHeaderView()

/// 背景图片
@property(nonatomic, strong) UIImageView *imgView;

/// 金币按钮
@property(nonatomic, strong) SPButton *goldCoinBtn;

/// 钻石按钮
@property(nonatomic, strong) SPButton *diamondBtn;

/// 贝壳按钮
@property(nonatomic, strong) SPButton *conchBtn;

/// 充值金币/钻石
@property(nonatomic, strong) UILabel *goldCoinAndDiamondLabel;

/// 充值贝壳
@property(nonatomic, strong) UILabel *conchLabel;

/// 底部滑动线条
@property(nonatomic, strong) UIView *bottomLineView;

@end

@implementation LY_RechargeHeaderView

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = [[UIImageView alloc] init];
        _imgView.image = [UIImage imageNamed:@"img_wode_chongzhi_beijing"];
    }
    return _imgView;
}

- (SPButton *)goldCoinBtn {
    if (!_goldCoinBtn) {
        _goldCoinBtn = [[SPButton alloc] init];
        _goldCoinBtn.imageTitleSpace = 5;
        _goldCoinBtn.imagePosition = SPButtonImagePositionLeft;
        [_goldCoinBtn setImage:[UIImage imageNamed:@"ic_store_gold"] forState:UIControlStateNormal];
        _goldCoinBtn.layer.cornerRadius = 13;
        _goldCoinBtn.layer.masksToBounds = true;
        _goldCoinBtn.titleLabel.font = [UIFont mediumFontOfSize:16];
    }
    return _goldCoinBtn;
}

- (SPButton *)diamondBtn {
    if (!_diamondBtn) {
        _diamondBtn = [[SPButton alloc] init];
        _diamondBtn.imageTitleSpace = 5;
        _diamondBtn.imagePosition = SPButtonImagePositionLeft;
        [_diamondBtn setImage:[UIImage imageNamed:@"ic_store_diamonds"] forState:UIControlStateNormal];
        _diamondBtn.layer.cornerRadius = 13;
        _diamondBtn.layer.masksToBounds = true;
        _diamondBtn.titleLabel.font = [UIFont mediumFontOfSize:16];
    }
    return _diamondBtn;
}

- (SPButton *)conchBtn {
    if (!_conchBtn) {
        _conchBtn = [[SPButton alloc] init];
        _conchBtn.imageTitleSpace = 5;
        _conchBtn.imagePosition = SPButtonImagePositionLeft;
        [_conchBtn setImage:[UIImage imageNamed:@"ic_zhibo_liwuhe_beike_xiao"] forState:UIControlStateNormal];
        _conchBtn.layer.cornerRadius = 13;
        _conchBtn.layer.masksToBounds = true;
        _conchBtn.titleLabel.font = [UIFont mediumFontOfSize:16];
    }
    return _conchBtn;
}

- (UILabel *)goldCoinAndDiamondLabel {
    if (!_goldCoinAndDiamondLabel) {
        _goldCoinAndDiamondLabel = [[UILabel alloc] init];
        _goldCoinAndDiamondLabel.textColor = [[UIColor colorWithHexString:@"#00D4A0"] colorWithAlphaComponent:0.6];
        _goldCoinAndDiamondLabel.font = [UIFont mediumFontOfSize:18];
        _goldCoinAndDiamondLabel.text = @"充值金币/钻石".localized;
    }
    return _goldCoinAndDiamondLabel;
}

- (UILabel *)conchLabel {
    if (!_conchLabel) {
        _conchLabel = [[UILabel alloc] init];
        _conchLabel.textColor = [[UIColor colorWithHexString:@"#00D4A0"] colorWithAlphaComponent:0.6];
        _conchLabel.font = [UIFont mediumFontOfSize:18];
        _conchLabel.text = @"充值贝壳".localized;
    }
    return _conchLabel;
}

- (UIView *)bottomLineView {
    if (!_bottomLineView) {
        _bottomLineView = [[UIView alloc] init];
        _bottomLineView.backgroundColor = [UIColor colorWithHexString:@"#00D4A0"];
    }
    return _bottomLineView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
        [self setupDefaultData];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.imgView];
    [self.imgView addSubview:self.goldCoinBtn];
    [self.imgView addSubview:self.diamondBtn];
    [self.imgView addSubview:self.conchBtn];
    [self addSubview:self.goldCoinAndDiamondLabel];
    [self addSubview:self.conchLabel];
    [self addSubview:self.bottomLineView];
}

- (void)setupUIFrame {
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self);
        make.leading.mas_equalTo(self);
        make.trailing.mas_equalTo(self);
        make.bottom.mas_equalTo(-60);
    }];
    
    [self.conchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.bottom.mas_equalTo(-12);
        make.height.mas_equalTo(26);
    }];
    
    [self.diamondBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.bottom.mas_equalTo(self.conchBtn.mas_top).offset(-9);
        make.height.mas_equalTo(26);
    }];
    
    [self.goldCoinBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.bottom.mas_equalTo(self.diamondBtn.mas_top).offset(-9);
        make.height.mas_equalTo(26);
    }];
    
    [self.goldCoinAndDiamondLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(self.imgView.mas_bottom).offset(15);
    }];
    
    [self.conchLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.goldCoinAndDiamondLabel.mas_trailing).offset(10);
        make.top.mas_equalTo(self.imgView.mas_bottom).offset(15);
    }];
    
    [self.bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.goldCoinAndDiamondLabel);
        make.top.mas_equalTo(self.imgView.mas_bottom).offset(40);
        make.size.mas_equalTo(CGSizeMake(30, 2));
    }];
}

- (void)setupDefaultData {
    
    CGSize goldCoinSize = [@"0" sizeWithAttributes:@{NSFontAttributeName: [UIFont mediumFontOfSize:16]}];
    NSArray *colors = @[[[UIColor whiteColor] colorWithAlphaComponent:0.3], [[UIColor whiteColor] colorWithAlphaComponent:0.0]];
    CGFloat locations[2] = {0.0, 1.0};
    UIImage *goldCoinBtnBgImg = [[UIImage alloc] initWithGradient:colors locations:locations  size:CGSizeMake((goldCoinSize.width + 38) / 2, 13) direction:UIImageGradientColorsDirectionHorizontal];
    [self.goldCoinBtn setTitle:@"0" forState:UIControlStateNormal];
    [self.goldCoinBtn setBackgroundImage:goldCoinBtnBgImg forState:UIControlStateNormal];
    
    CGSize diamondSize = [@"023456" sizeWithAttributes:@{NSFontAttributeName: [UIFont mediumFontOfSize:16]}];
    UIImage *diamondBtnBgImg = [[UIImage alloc] initWithGradient:colors locations:locations size:CGSizeMake((diamondSize.width + 38) / 2, 13) direction:UIImageGradientColorsDirectionHorizontal];
    [self.diamondBtn setTitle:@"023456" forState:UIControlStateNormal];
    [self.diamondBtn setBackgroundImage:diamondBtnBgImg forState:UIControlStateNormal];
    
    CGSize conchSize = [@"1234567890" sizeWithAttributes:@{NSFontAttributeName: [UIFont mediumFontOfSize:16]}];
    UIImage *conchBtnBgImg = [[UIImage alloc] initWithGradient:colors locations:locations size:CGSizeMake((conchSize.width + 38) / 2, 13) direction:UIImageGradientColorsDirectionHorizontal];
    [self.conchBtn setTitle:@"1234567890" forState:UIControlStateNormal];
    [self.conchBtn setBackgroundImage:conchBtnBgImg forState:UIControlStateNormal];
}

@end
