//
//  LY_RechargeConchCell.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/31.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_RechargeConchCell.h"
#import "SPButton.h"
#import "MyWalletListModel.h"

@interface LY_RechargeConchCell ()

@property(nonatomic, strong) UIView *whiteBgView;

@property(nonatomic, strong) SPButton *numberBtn;

@property(nonatomic, strong) UILabel *moneyLabel;

@end

@implementation LY_RechargeConchCell

- (UIView *)whiteBgView {
    if (!_whiteBgView) {
        _whiteBgView = [[UIView alloc] init];
        _whiteBgView.backgroundColor = [UIColor whiteColor];
        _whiteBgView.layer.shadowColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:0.16].CGColor;
        _whiteBgView.layer.shadowOffset = CGSizeMake(0,1);
        _whiteBgView.layer.shadowOpacity = 1;
        _whiteBgView.layer.shadowRadius = 6;
        _whiteBgView.layer.cornerRadius = 5;
    }
    return _whiteBgView;
}

- (SPButton *)numberBtn {
    if (!_numberBtn) {
        _numberBtn = [[SPButton alloc] init];
        if ([SPDCommonTool currentVersionIsArbic]) {
            _numberBtn.imageTitleSpace = -5;
        } else {
            _numberBtn.imageTitleSpace = 5;
        }
        [_numberBtn setImage:[UIImage imageNamed:@"ic_zhibo_liwuhe_beike_xiao"] forState:UIControlStateNormal];
        [_numberBtn setTitleColor:[UIColor colorWithHexString:@"#1A1A1A"] forState:UIControlStateNormal];
        _numberBtn.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    }
    return _numberBtn;
}

- (UILabel *)moneyLabel {
    if (!_moneyLabel) {
        _moneyLabel = [[UILabel alloc] init];
        _moneyLabel.textColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.5];
        _moneyLabel.font = [UIFont systemFontOfSize:15];
        _moneyLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _moneyLabel;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.whiteBgView];
    [self.whiteBgView addSubview:self.numberBtn];
    [self.whiteBgView addSubview:self.moneyLabel];
}

- (void)setupUIFrame {
    [self.whiteBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(5, 5, 5, 5));
    }];
    
    [self.numberBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
//        make.centerX.mas_equalTo(self.whiteBgView);
        make.leading.trailing.mas_equalTo(self.whiteBgView);
    }];
    
    [self.moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.numberBtn.mas_bottom).offset(20);
        make.leading.trailing.mas_equalTo(self.whiteBgView);
    }];
}

- (void)setModel:(MyWalletListModel *)model {
    _model = model;

    [self.numberBtn setTitle:_model.number forState:UIControlStateNormal];
    self.moneyLabel.text = _model.price;
}

@end
