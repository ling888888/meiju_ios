//
//  LY_HalfViewController.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/17.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_HalfViewController.h"
#import "LY_Animator.h"

@interface LY_HalfViewController () <UIViewControllerTransitioningDelegate>

@end

@implementation LY_HalfViewController {
    LY_Animator * _animator;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.modalPresentationStyle = UIModalPresentationCustom;
        
        _animator = [[LY_Animator alloc] initWithControllerHeight:0];
        
        self.transitioningDelegate = _animator;
    }
    return self;
}

- (instancetype)initWithControllerHeight:(CGFloat)controllerHeight {
    self = [super init];
    if (self) {
        self.modalPresentationStyle = UIModalPresentationCustom;
        
        _animator = [[LY_Animator alloc] initWithControllerHeight:controllerHeight];
        
        self.transitioningDelegate = _animator;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blueColor];
}


@end
