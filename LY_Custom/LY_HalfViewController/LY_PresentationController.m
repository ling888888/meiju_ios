//
//  LY_PresentationController.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/17.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_PresentationController.h"

//要弹出的控制器的高度
#define LY_DEFAULTALFMODALHEIGHT 500

@interface LY_PresentationController ()

/// 黑色背景半透明蒙版
@property(nonatomic,strong)UIView *backgroundDismissView;

/// 控制器高度
@property (nonatomic, assign) CGFloat controllerHeight;

/// 黑色背景半透明蒙版点击手势
@property(nonatomic,strong)UITapGestureRecognizer * tapGesture;

@end

@implementation LY_PresentationController

- (UIView *)backgroundDismissView {
    if (!_backgroundDismissView) {
        _backgroundDismissView = [[UIView alloc] init];
        _backgroundDismissView.backgroundColor = [UIColor blackColor];
        [_backgroundDismissView addGestureRecognizer:self.tapGesture];
    }
    return _backgroundDismissView;
}

- (UITapGestureRecognizer *)tapGesture {
    if (!_tapGesture) {
        _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction)];
    }
    return _tapGesture;
}

- (instancetype)initWithPresentedViewController:(UIViewController *)presentedViewController presentingViewController:(UIViewController *)presentingViewController andControllerHeight:(CGFloat)controllerHeight {
    self = [super initWithPresentedViewController:presentedViewController presentingViewController:presentingViewController];
    if (self) {
        self.controllerHeight = controllerHeight;
    }
    return self;
}

- (void)presentationTransitionDidEnd:(BOOL)completed {
    [super presentationTransitionDidEnd:completed];
    
}

- (void)presentationTransitionWillBegin {
    [super presentationTransitionWillBegin];
    
    // presentedView 为我们主要内容显示的视图
    [self.containerView addSubview:self.presentedView];
    
    // 添加背景蒙版
    [self.containerView insertSubview:self.backgroundDismissView atIndex:0];
    
    // 背景透明度渐变
    self.backgroundDismissView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        weakSelf.backgroundDismissView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    }];
}

- (void)containerViewWillLayoutSubviews {
    [super containerViewWillLayoutSubviews];
    
    [self setupUIFrame];
}

- (void)setupUIFrame {
    // 控制器高度
    CGFloat cHeight = self.controllerHeight <= 0 ? LY_DEFAULTALFMODALHEIGHT : self.controllerHeight;
    // 设置背景遮罩蒙版视图Frame
    self.backgroundDismissView.frame = CGRectMake(0, 0, kScreenW, kScreenH);
    // 设置主要内容视图Frame
    self.presentedView.frame = CGRectMake(0, kScreenH - cHeight, kScreenW, cHeight);
    //
    [self.presentedView addRounded:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:10];
}

/// 黑色背景半透明蒙版点击事件
- (void)tapGestureAction {
    // 背景透明度渐变
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        weakSelf.backgroundDismissView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
    } completion:^(BOOL finished) {
    }];
    [self.presentedViewController dismissViewControllerAnimated:true completion:nil];
}

- (void)updateControllerHeight:(CGFloat)controllerHeight {
    self.controllerHeight = controllerHeight;
//    [self setupUIFrame];
    
    // 控制器高度
    CGFloat cHeight = self.controllerHeight <= 0 ? LY_DEFAULTALFMODALHEIGHT : self.controllerHeight;
    // 设置主要内容视图Frame
    self.presentedView.frame = CGRectMake(0, kScreenH - cHeight, kScreenW, cHeight);
}

@end
