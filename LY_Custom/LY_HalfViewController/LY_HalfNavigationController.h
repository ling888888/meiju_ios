//
//  LY_HalfNavigationController.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/20.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_HalfNavigationController : UINavigationController

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController controllerHeight:(CGFloat)controllerHeight;

@end

NS_ASSUME_NONNULL_END
