//
//  LY_Animator.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/17.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_Animator.h"
#import "LY_PresentationController.h"

@interface LY_Animator ()

/// 控制器高度
@property (nonatomic, assign) CGFloat controllerHeight;

@property (nonatomic, strong) LY_PresentationController *presentVC;

@end

@implementation LY_Animator

- (instancetype)initWithControllerHeight:(CGFloat)controllerHeight
{
    self = [super init];
    if (self) {
        self.controllerHeight = controllerHeight;
    }
    return self;
}


- (nullable UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(nullable UIViewController *)presenting sourceViewController:(UIViewController *)source {
    
    LY_PresentationController * vc = [[LY_PresentationController alloc] initWithPresentedViewController:presented presentingViewController:presenting andControllerHeight:self.controllerHeight];
    
    return vc;
}

- (void)updateControllerHeight:(CGFloat)controllerHeight {
    [self.presentVC updateControllerHeight:controllerHeight];
}

@end
