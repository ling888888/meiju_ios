//
//  LY_HalfViewController.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/17.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_HalfViewController : UIViewController

- (instancetype)initWithControllerHeight:(CGFloat)controllerHeight;

@end

NS_ASSUME_NONNULL_END
