//
//  LY_PresentationController.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/17.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_PresentationController : UIPresentationController

/**
 自定义initWithPresentedViewController: presentingViewController:方法

 @param presentedViewController 原本的控制器
 @param presentingViewController 要弹出的控制器
 @param controllerHeight 要弹出的控制器的高度
 @return 对象实例
 */
- (instancetype)initWithPresentedViewController:(UIViewController *)presentedViewController presentingViewController:(UIViewController *)presentingViewController andControllerHeight:(CGFloat)controllerHeight;

- (void)updateControllerHeight:(CGFloat)controllerHeight;

@end

NS_ASSUME_NONNULL_END
