//
//  LY_TestViewController.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/20.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_TestViewController.h"

@interface LY_TestViewController ()

@end

@implementation LY_TestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor redColor];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    
    LY_TestViewController *vc = [[LY_TestViewController alloc] init];
    [self.navigationController pushViewController:vc animated:true];
}

@end
