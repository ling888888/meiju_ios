//
//  LY_Animator.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/17.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_Animator : NSObject <UIViewControllerTransitioningDelegate>

- (instancetype)initWithControllerHeight:(CGFloat)controllerHeight;

- (void)updateControllerHeight:(CGFloat)controllerHeight;

@end

NS_ASSUME_NONNULL_END
