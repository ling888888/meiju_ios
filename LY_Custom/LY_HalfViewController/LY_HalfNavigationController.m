//
//  LY_HalfNavigationController.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/20.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_HalfNavigationController.h"
#import "LY_Animator.h"
#import "LY_HalfDataSource.h"
#import "UINavigationBar+LY.h"

@interface LY_HalfNavigationController ()

@end

@implementation LY_HalfNavigationController {
    LY_Animator * _animator;
}

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController {
    self = [super initWithRootViewController:rootViewController];
    if (self) {
        self.modalPresentationStyle = UIModalPresentationCustom;
        
        _animator = [[LY_Animator alloc] initWithControllerHeight:0];
        
        self.transitioningDelegate = _animator;
    }
    return self;
}

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController controllerHeight:(CGFloat)controllerHeight {
    self = [super initWithRootViewController:rootViewController];
    if (self) {
        self.modalPresentationStyle = UIModalPresentationCustom;
        
        _animator = [[LY_Animator alloc] initWithControllerHeight:controllerHeight];
        
        self.transitioningDelegate = _animator;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 设置导航栏前景色
    self.navigationBar.tintColor = [UIColor colorWithHexString:@"#1A1A1A"];
    // 设置导航栏背景颜色
    [self.navigationBar vhl_setBackgroundColor:[UIColor whiteColor]];
    // 设置标题文本样式
    self.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"#1A1A1A"],
      NSFontAttributeName: [UIFont mediumFontOfSize:18]
    };
    [self.navigationBar vhl_setBackgroundAlpha:0.0];
    [self.navigationBar vhl_setShadowImageHidden:true];
    [self.navigationBar setTranslucent:true];
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (self.viewControllers.count == 0) {
        [super pushViewController:viewController animated:animated];
        return;
    }
    viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ic_arrow_left"].adaptiveRtl style:UIBarButtonItemStylePlain target:self action:@selector(popViewControllerAnimated:)];
//    [_animator updateControllerHeight:300];
    
    [super pushViewController:viewController animated:animated];
}

- (UIViewController *)popViewControllerAnimated:(BOOL)animated {
    
//    [_animator updateControllerHeight:500];
    
    return [super popViewControllerAnimated:true];
}


@end
