//
//  LY_HalfDataSource.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/20.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol LY_HalfDataSource <NSObject>



@property (nonatomic, assign) CGFloat controllerHeight;

@end

NS_ASSUME_NONNULL_END
