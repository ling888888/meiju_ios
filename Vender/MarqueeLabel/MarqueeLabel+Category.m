//
//  MarqueeLabel+Category.m
//  Utility
//
//  Created by LHL on 16/6/8.
//
//

#import "MarqueeLabel+Category.h"

@implementation MarqueeLabel (Category)

+ (MarqueeLabel *)getNewMarqueeLabel{
    MarqueeLabel *marqueeLabel = [[MarqueeLabel alloc] init];
    marqueeLabel.backgroundColor = [UIColor clearColor];
    marqueeLabel.textAlignment = NSTextAlignmentLeft;
    marqueeLabel.fadeLength = 30;
    marqueeLabel.rate = 50;
    marqueeLabel.repeat = NO;
    marqueeLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    marqueeLabel.font = [UIFont systemFontOfSize:16.0f];
    marqueeLabel.textColor = [UIColor colorWithRed:237/255. green:237/255. blue:237/255. alpha:1.0];
    return marqueeLabel;
}

@end
