//
//  MarqueeLabel+Category.h
//  Utility
//
//  Created by LHL on 16/6/8.
//
//

#import "MarqueeLabel.h"

@interface MarqueeLabel (Category)

+ (MarqueeLabel *)getNewMarqueeLabel;

@end
