//
//  LY_BaseCollectionViewController.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/9.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    RequestNetDataTypeRefresh,
    RequestNetDataTypeLoadMore,
} RequestNetDataType;

@interface LY_BaseCollectionViewController : LY_BaseViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property(nonatomic, strong) UICollectionView *collectionView;

/// 数据源
@property(nonatomic, strong) NSMutableArray *dataList;

/// 网络请求页数
@property(nonatomic, assign) int pageNumber;

- (void)loadNetDataWith:(RequestNetDataType)type;

- (void)reloadData;

@end

NS_ASSUME_NONNULL_END
