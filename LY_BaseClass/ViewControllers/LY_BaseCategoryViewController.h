//
//  LY_BaseCategoryViewController.h
//  SimpleDate
//
//  Created by jianyue on 2020/5/12.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_BaseViewController.h"
#import "JXCategoryView.h"
#import "JXPagerView.h"
#import "JXPagerListRefreshView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_BaseCategoryViewController : LY_BaseViewController <JXCategoryViewDelegate, JXPagerViewDelegate, JXPagerMainTableViewGestureDelegate>

@property (nonatomic, strong) JXCategoryTitleView *categoryView;

@property (nonatomic, strong) JXCategoryIndicatorLineView *categoryIndicatorLineView;

@property (nonatomic, strong) JXPagerListRefreshView *pagerView;

@property (nonatomic, strong) NSMutableArray<NSString *> *titleArray;

@property (nonatomic, strong) NSMutableArray<JXPagerViewListViewDelegate> *VCArray;

@end

NS_ASSUME_NONNULL_END
