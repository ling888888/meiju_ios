//
//  LY_BaseNavigationController.h
//  SimpleDate
//
//  Created by jianyue on 2020/5/7.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_BaseNavigationController : UINavigationController


@end

NS_ASSUME_NONNULL_END
