//
//  LY_BaseNavigationController.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/7.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_BaseNavigationController.h"

@interface LY_BaseNavigationController ()

@end

@implementation LY_BaseNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationBar.tintColor = [UIColor colorWithHexString:@"#1A1A1A"];
    //    [navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationBar vhl_setBackgroundColor:[UIColor whiteColor]];
//    [self.navigationBar vhl_setBackgroundAlpha:1.0];
    [self.navigationBar vhl_setShadowImageHidden:true];
    
    //统一导航栏标题颜色
    self.navigationBar.titleTextAttributes = @{
        NSFontAttributeName:[UIFont mediumFontOfSize:18],
        NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#1A1A1A"]
    };
}

//- (UIStatusBarStyle)preferredStatusBarStyle {
//    return UIStatusBarStyleDefault;
//}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    // 隐藏TabBar
    viewController.hidesBottomBarWhenPushed = self.viewControllers.count > 0;
    
    if (viewController.hidesBottomBarWhenPushed) {
        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ic_arrow_left"].adaptiveRtl style:UIBarButtonItemStylePlain target:self action:@selector(popViewControllerAnimated:)];
    }
    
    [super pushViewController:viewController animated:animated];
}

@end
