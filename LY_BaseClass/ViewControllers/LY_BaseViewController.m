//
//  LY_BaseViewController.m
//  SimpleDate
//
//  Created by jianyue on 2020/2/27.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_BaseViewController.h"

@interface LY_BaseViewController ()

@end

@implementation LY_BaseViewController

- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = [[UIButton alloc] init];
        _backBtn.frame = CGRectMake(0, 0, 60, 35);
        UIImage *backBtnImg = [UIImage imageNamed:self.backBtnImgName].adaptiveRtl;
        [_backBtn setImage:backBtnImg forState:UIControlStateNormal];
        if (kIsMirroredLayout) {
            _backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 25, 0, -25);
        } else {
            _backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 25);
        }
        [_backBtn addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backBtn;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationController.interactivePopGestureRecognizer.delegate=(id)self;
    // 设置导航栏穿透
    [self.navigationController.navigationBar setTranslucent:true];
    
    if (self.hidesBottomBarWhenPushed) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.backBtn];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //TODO: 页面Disappear 启用
    // 使用智能键盘
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    // 控制整个功能是否启用
    manager.enable = true;
    // 控制是否显示键盘上的自动工具条,当需要支持内联编辑(Inline Editing), 这就需要隐藏键盘上的工具条(默认打开)
    manager.enableAutoToolbar = false;
    // 启用手势触摸:控制点击背景是否收起键盘。
    manager.shouldResignOnTouchOutside = true;
    // 是否显示提示文字
//    manager.shouldShowTextFieldPlaceholder = false;
    // 控制键盘上的工具条文字颜色是否用户自定义,(使用TextField的tintColor属性IQToolbar，否则色调的颜色是黑色 )
//    manager.shouldToolbarUsesTextFieldTintColor = YES;
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    //TODO: 页面appear 禁用
    [[IQKeyboardManager sharedManager] setEnable:false];
}

- (BOOL)prefersStatusBarHidden {
    return false;
}

// 由于当前项目统一管理状态栏，该方法失效
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (NSString *)backBtnImgName {
    return @"ic_tongyong_fanhui_hei";
}

- (void)updateBackBtnImage:(UIImage *)image {
    UIImage *backBtnImg = image.adaptiveRtl;
    [self.backBtn setImage:backBtnImg forState:UIControlStateNormal];
}

/// 返回按钮点击事件
- (void)backBtnAction {
    [self.navigationController popViewControllerAnimated:true];
}

@end
