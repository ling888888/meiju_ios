//
//  LY_WebViewController.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_BaseViewController.h"
#import "LY_WebView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_WebViewController : LY_BaseViewController

/// URL
@property(nonatomic, copy) NSString *URLString;

/// 是否隐藏进度条
@property(nonatomic, assign) BOOL isProgressHidden;

/// 网页视图
@property(nonatomic, strong) LY_WebView *webView;

- (instancetype)initWithURLString:(NSString *)URLString;

@end

NS_ASSUME_NONNULL_END
