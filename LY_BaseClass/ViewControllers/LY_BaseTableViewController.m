//
//  LY_BaseTableViewController.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_BaseTableViewController.h"

@interface LY_BaseTableViewController ()

@end

@implementation LY_BaseTableViewController

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15);
        _tableView.separatorColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.1];
        _tableView.tableFooterView = [[UIView alloc] init];
        __weak typeof(self) weakSelf = self;
        _tableView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            weakSelf.pageNumber = 1;
            [weakSelf loadNetDataWith:RequestNetDataTypeRefresh];
        }];
        
        _tableView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            weakSelf.pageNumber += 1;
            [weakSelf loadNetDataWith:RequestNetDataTypeLoadMore];
        }];
        
        _tableView.vOffset = -naviBarHeight;
        _tableView.tipImageName = @"img_kongyemian_haoyouliebiao";
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dataList = [NSMutableArray array];
    self.pageNumber = 1;
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (void)loadNetDataWith:(RequestNetDataType)type {
    
}

- (void)reloadData {
    [self.tableView reloadData];
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [[UITableViewCell alloc] init];
}

@end
