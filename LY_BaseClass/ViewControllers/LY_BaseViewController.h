//
//  LY_BaseViewController.h
//  SimpleDate
//
//  Created by jianyue on 2020/2/27.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LY_BaseViewController : UIViewController

@property (nonatomic, strong) UIButton *backBtn;

- (NSString *)backBtnImgName;

- (void)updateBackBtnImage:(UIImage *)image;

- (void)backBtnAction;

@end
