//
//  LY_BaseCategoryViewController.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/12.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_BaseCategoryViewController.h"

@interface LY_BaseCategoryViewController ()

@end

@implementation LY_BaseCategoryViewController

- (JXCategoryTitleView *)categoryView {
    if (!_categoryView) {
        _categoryView = [[JXCategoryTitleView alloc] initWithFrame:CGRectMake(200, 0, 200, 44)];
        _categoryView.titleColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.8];
        _categoryView.titleSelectedColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _categoryView.titleFont = [UIFont mediumFontOfSize:18];
        _categoryView.backgroundColor = [UIColor whiteColor];
        _categoryView.titleColorGradientEnabled = YES;
        _categoryView.titleLabelZoomEnabled = true;
        _categoryView.delegate = self;
        _categoryView.averageCellSpacingEnabled = false;
        _categoryView.cellWidthZoomEnabled = false;
        _categoryView.contentEdgeInsetLeft = 15;
        _categoryView.contentEdgeInsetRight = 15;
        _categoryView.cellWidthIncrement = 10;
        _categoryView.cellSpacing = 10;
        _categoryView.titleLabelAnchorPointStyle = JXCategoryTitleLabelAnchorPointStyleBottom;
        _categoryView.contentScrollViewClickTransitionAnimationEnabled = true;
        
        _categoryView.cellWidthZoomScrollGradientEnabled = false;
    }
    return _categoryView;
}

- (JXCategoryIndicatorLineView *)categoryIndicatorLineView {
    if (!_categoryIndicatorLineView) {
        _categoryIndicatorLineView = [[JXCategoryIndicatorLineView alloc] init];
        _categoryIndicatorLineView.indicatorColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _categoryIndicatorLineView.indicatorWidth = JXCategoryViewAutomaticDimension;
        _categoryIndicatorLineView.indicatorColor = [UIColor colorWithHexString:@"#00D4A0"];
        _categoryIndicatorLineView.indicatorHeight = 5;
        _categoryIndicatorLineView.indicatorCornerRadius = 0;
        _categoryIndicatorLineView.verticalMargin = 10;
    }
    return _categoryIndicatorLineView;
}

- (JXPagerListRefreshView *)pagerView {
    if (!_pagerView) {
        _pagerView = [[JXPagerListRefreshView alloc] initWithDelegate:self];
        _pagerView.mainTableView.gestureDelegate = self;
        _pagerView.frame = CGRectMake(0, 64, kScreenW, 500);
        _pagerView.listContainerView.categoryNestPagingEnabled = true;
    }
    return _pagerView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self __setupUI];
    
    [self __setupUIFrame];

    [self __setupData];
}

- (void)__setupUI {
    [self.view addSubview:self.categoryView];
    [self.view addSubview:self.pagerView];
}

- (void)__setupUIFrame {
    CGFloat categoryViewW = 0.0;
    for (NSString *title in self.titleArray) {
        CGFloat titleWidth = [title sizeWithFont:[UIFont mediumFontOfSize:18] andMaxSize:CGSizeMake(kScreenW, 44)].width;
        categoryViewW += titleWidth;
    }
    categoryViewW = categoryViewW + 15 + 2 * 10 + 3 * 10 + 15;
    
    [self.categoryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(44);
        make.leading.mas_equalTo(0);
        make.top.mas_equalTo(statusBarHeight);
        make.width.mas_equalTo(categoryViewW);
    }];
    [self.pagerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.categoryView.mas_bottom);
        make.leading.trailing.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view);
    }];
}

- (void)__setupData {
    if (kIsMirroredLayout) {
        self.categoryView.titles = [NSMutableArray arrayWithArray:[[self.titleArray reverseObjectEnumerator] allObjects]];
    } else {
        self.categoryView.titles = self.titleArray;
    }
    self.categoryView.indicators = @[self.categoryIndicatorLineView];
    self.categoryView.listContainer = (id<JXCategoryViewListContainer>)self.pagerView.listContainerView;
//    if (kIsMirroredLayout) {
//        self.categoryView.defaultSelectedIndex = 2;
//        self.pagerView.defaultSelectedIndex = 2;
//    } else {
//        self.categoryView.defaultSelectedIndex = 0;
//        self.pagerView.defaultSelectedIndex = 0;
//    }
}

#pragma mark - JXCategoryViewDelegate

- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
//    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}


#pragma mark - JXPagerViewDelegate

- (UIView *)tableHeaderViewInPagerView:(JXPagerView *)pagerView {
    return [UIView new];
}

- (NSUInteger)tableHeaderViewHeightInPagerView:(JXPagerView *)pagerView {
    return 0;
}

- (NSUInteger)heightForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    return 0;
}

- (UIView *)viewForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    return self.categoryView;
}

- (NSInteger)numberOfListsInPagerView:(JXPagerView *)pagerView {
    return self.VCArray.count;
}

        
- (id<JXPagerViewListViewDelegate>)pagerView:(JXPagerView *)pagerView initListAtIndex:(NSInteger)index {
    if (kIsMirroredLayout) {
        return self.VCArray[self.VCArray.count - 1 - index];
    } else {
        return self.VCArray[index];
    }
}


#pragma mark - JXPagerMainTableViewGestureDelegate

- (BOOL)mainTableViewGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    //禁止categoryView左右滑动的时候，上下和左右都可以滚动
    if (otherGestureRecognizer == self.categoryView.collectionView.panGestureRecognizer) {
        return NO;
    }
    return [gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]];
}

@end
