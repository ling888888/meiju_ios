//
//  LY_WebViewController.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_WebViewController.h"

@interface LY_WebViewController () <WKNavigationDelegate, WKUIDelegate, WKScriptMessageHandler>

@property(nonatomic, strong) WKPreferences *preferences;

@property(nonatomic, strong) WKWebViewConfiguration *configuration;
@end

@implementation LY_WebViewController

// 网页偏好设置
- (WKPreferences *)preferences {
    if (!_preferences) {
        // 最小的字体大小 当javaScriptEnabled = false时效果明显
        _preferences.minimumFontSize = 10;
        // 是否支持javaScript 默认是支持的
        _preferences.javaScriptEnabled = true;
        // 不允许通过js打开窗口，必须通过用户交互才能打开
        _preferences.javaScriptCanOpenWindowsAutomatically = false;
    }
    return _preferences;
}

// 网页视图配置项
- (WKWebViewConfiguration *)configuration {
    if (!_configuration) {
        _configuration = [[WKWebViewConfiguration alloc] init];
        // 偏好设置
        _configuration.preferences = self.preferences;
    }
    return _configuration;
}

- (LY_WebView *)webView {
    if (!_webView) {
        _webView = [[LY_WebView alloc] init];
        // 开启手势交互
        _webView.allowsBackForwardNavigationGestures = true;
        // 内容自适应
        [_webView sizeToFit];
        _webView.navigationDelegate = self;
        _webView.UIDelegate = self;
    }
    return _webView;
}

- (instancetype)initWithURLString:(NSString *)URLString {
    self = [super init];
    if (self) {
        self.URLString = URLString;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.webView];
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
//        if (@available(iOS 11.0, *)) {
//            make.edges.mas_equalTo(self.view.safeAreaLayoutGuide);
//        } else {
            make.edges.mas_equalTo(self.view);
//        }
    }];
}

- (void)setURLString:(NSString *)URLString {
    _URLString = URLString;
    
    NSURL *webURL = [NSURL URLWithString:URLString];
    NSURLRequest *webRequest = [NSURLRequest requestWithURL:webURL];
    [self.webView loadRequest:webRequest];
}

- (void)setIsProgressHidden:(BOOL)isProgressHidden {
    _isProgressHidden = isProgressHidden;
    
    [self.webView.progressView setHidden:isProgressHidden];
}

// 开始请求的时候调用 决定是否响应网页的某个动作，例如加载，回退，前进，刷新等 (WKNavigationAction)
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    // 执行允许操作
    decisionHandler(WKNavigationActionPolicyAllow);
}

// 接收到数据后是否允许执行渲染
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler {
    
    // 执行允许操作
    decisionHandler(WKNavigationResponsePolicyAllow);
}

// 需要响应身份验证时调用  在block中传入用户信息凭证
- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler {
    completionHandler(NSURLSessionAuthChallengeUseCredential, nil);
}

// JavaScript调用alert方法后回调的方法 message中为alert提示的信息 必须要在其中调用completionHandler()
- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler {
    completionHandler();
}

// JavaScript调用confirm方法后回调的方法 confirm是js中的确定框，需要在block中把用户选择的情况传递进去
- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(nonnull NSString *)message initiatedByFrame:(nonnull WKFrameInfo *)frame completionHandler:(nonnull void (^)(BOOL))completionHandler {
    completionHandler(true);
}

// JavaScript调用prompt方法后回调的方法 prompt是js中的输入框 需要在block中把用户输入的信息传入
- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * _Nullable))completionHandler {
    completionHandler(@"");
}

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    
}

@end
