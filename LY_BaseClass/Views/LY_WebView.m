//
//  LY_WebView.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_WebView.h"

@interface LY_WebView ()

@end

@implementation LY_WebView

- (UIProgressView *)progressView {
    if (!_progressView) {
        _progressView = [[UIProgressView alloc] init];
        _progressView.trackTintColor = [UIColor clearColor];
        _progressView.progressTintColor = [UIColor colorWithHexString:@"#00D4A0"];
    }
    return _progressView;
}

- (instancetype)initWithFrame:(CGRect)frame configuration:(WKWebViewConfiguration *)configuration {
    self = [super initWithFrame:frame configuration:configuration];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
        // 监听加载进度
        [self addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (void)dealloc
{
    [self removeObserver:self forKeyPath:@"estimatedProgress"];
}

- (void)setupUI {
    [self addSubview:self.progressView];
}
 
- (void)setupUIFrame {
    [self.progressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(3);
        make.leading.trailing.mas_equalTo(self);
        make.top.mas_equalTo(self);
    }];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (![keyPath isEqualToString:@"estimatedProgress"]) {
        return;
    } else {
        [self.progressView setProgress:self.estimatedProgress animated:true];
        if (self.estimatedProgress >= 1.0) {
            __weak typeof(self) weakSelf = self;
            [UIView animateWithDuration:1 delay:0.2 options:UIViewAnimationOptionCurveEaseOut animations:^{
                weakSelf.progressView.alpha = 0.0;
            } completion:^(BOOL finished) {
                if (finished) {
                    [weakSelf.progressView setProgress:0.0 animated:false];
                }
            }];
        }
    }
}

@end
