//
//  LY_TableViewCell.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/2.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_TableViewCell.h"

@implementation LY_TableViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIView *backgroundViews = [[UIView alloc] init];
        
        backgroundViews.backgroundColor = [UIColor colorWithHexString:@"#f5f5f5"];
        
        [self setSelectedBackgroundView:backgroundViews];
    }
    return self;
}

//- (instancetype)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//
//
//        backgroundViews.backgroundColor = [UIColor grayColor];
//        [cell setSelectedBackgroundView:backgroundViews];
//
//        self.selectedBackgroundView.backgroundColor = [UIColor redColor];//
//    }
//    return self;
//}
//- (void)awakeFromNib {
//    [super awakeFromNib];
//    // Initialization code
//}
//
//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
//
//    // Configure the view for the selected state
//}

@end
