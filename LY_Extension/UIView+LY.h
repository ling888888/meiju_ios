//
//  UIView+LY.h
//  SimpleDate-dis
//
//  Created by jianyue on 2020/3/3.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (LY)

@property(nonatomic, weak, readonly) UIViewController *VC;

/// 切圆角，使用此方法切圆角之前必须要设置Frame
- (void)addRounded:(UIRectCorner)corners withRadius:(CGFloat)radius;

@end

NS_ASSUME_NONNULL_END
