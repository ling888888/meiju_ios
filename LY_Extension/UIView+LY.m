
//
//  UIView+LY.m
//  SimpleDate-dis
//
//  Created by jianyue on 2020/3/3.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "UIView+LY.h"

@implementation UIView (LY)

-(UIViewController *)VC {
    for (UIView *next = [self superview]; next; next = next.superview) {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}

/// 切圆角，使用此方法切圆角之前必须要设置Frame
- (void)addRounded:(UIRectCorner)corners withRadius:(CGFloat)radius {
    UIBezierPath *rounded = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corners cornerRadii:CGSizeMake(radius, radius)];
    CAShapeLayer *shape = [[CAShapeLayer alloc] init];
    shape.frame = self.bounds;
    shape.path = rounded.CGPath;
    self.layer.mask = shape;
}

@end
