//
//  UIDevice+LY.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/2.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIDevice (LY)

/// 设备的唯一标示
@property(nonatomic, copy, readonly) NSString *identifier;

/// 是否是iPhoneX（带刘海儿、安全区域）
@property(nonatomic, assign, readonly) BOOL is_iPhoneX;

@end

NS_ASSUME_NONNULL_END
