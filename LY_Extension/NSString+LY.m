//
//  NSString+LY.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/6.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "NSString+LY.h"

@implementation NSString (LY)

- (NSString *)localized {
    return SPDStringWithKey(self, nil);
}

- (NSString *)filterEmojiString {
    //去除表情规则
    //  \u0020-\\u007E  标点符号，大小写字母，数字
    //  \u00A0-\\u00BE  特殊标点  (¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾)
    //  \u2E80-\\uA4CF  繁简中文,日文，韩文 彝族文字
    //  \uF900-\\uFAFF  部分汉字
    //  \uFE30-\\uFE4F  特殊标点(︴︵︶︷︸︹)
    //  \uFF00-\\uFFEF  日文  (ｵｶｷｸｹｺｻ)
    //  \u2000-\\u201f  特殊字符(‐‑‒–—―‖‗‘’‚‛“”„‟)
    //  \u0600-\\u06FF
    // 注：对照表 http://blog.csdn.net/hherima/article/details/9045765
    if (!self.length) return self;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[^\\u0020-\\u007E\\u00A0-\\u00BE\\u2E80-\\uA4CF\\uF900-\\uFAFF\\uFE30-\\uFE4F\\uFF00-\\uFFEF\\u0080-\\u009F\\u2000-\\u201f\\u0600-\\u06FF\r\n]" options:NSRegularExpressionCaseInsensitive error:nil];
    return [regex stringByReplacingMatchesInString:self options:0 range:NSMakeRange(0, [self length])withTemplate:@""];
}

- (CGSize)sizeWithFont:(UIFont*)font andMaxSize:(CGSize)size {
    NSDictionary*attrs =@{NSFontAttributeName: font};
    return [self boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
}

- (BOOL)containsEmoji {
    __block BOOL returnValue = NO;
    [self enumerateSubstringsInRange:NSMakeRange(0, [self length])
    options:NSStringEnumerationByComposedCharacterSequences
    usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
    const unichar hs = [substring characterAtIndex:0];
    if (0xd800 <= hs && hs <= 0xdbff) {
    if (substring.length > 1) {
    const unichar ls = [substring characterAtIndex:1];
    const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
    if (0x1d000 <= uc && uc <= 0x1f77f) {
    returnValue = YES;
    }
    }
    } else if (substring.length > 1) {
    const unichar ls = [substring characterAtIndex:1];
    if (ls == 0x20e3) {
    returnValue = YES;
    }
    } else {
    if (0x2100 <= hs && hs <= 0x27ff) {
    returnValue = YES;
    } else if (0x2B05 <= hs && hs <= 0x2b07) {
    returnValue = YES;
    } else if (0x2934 <= hs && hs <= 0x2935) {
    returnValue = YES;
    } else if (0x3297 <= hs && hs <= 0x3299) {
    returnValue = YES;
    } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50) {
    returnValue = YES;
    }
    }
    }];
    return returnValue;
}
@end
