//
//  UIScrollView+LY.h
//  SimpleDate
//

//  Created by jianyue on 2020/5/8.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIScrollView (LY)

/// 是否显示空页面提示（默认 true 显示）
@property (nonatomic, assign) BOOL showEmptyTipView;

/// 垂直偏移
@property (nonatomic, assign) CGFloat vOffset;

/// 提示文字
@property (nonatomic, copy) NSString *tipText;

/// 提示图片名
@property (nonatomic, copy) NSString *tipImageName;

/// 提示按钮
@property (nonatomic, copy) NSString *tipButtonTitle;

/// 提示按钮
@property (nonatomic, strong) UIButton *tipButton;

@end

NS_ASSUME_NONNULL_END
