//
//  UIImage+LY.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    /// 水平
    UIImageGradientColorsDirectionHorizontal = 0,
    /// 垂直
    UIImageGradientColorsDirectionVertical = 1,
} UIImageGradientColorsDirection;

@interface UIImage (LY)

///  RTL图片
@property(nonatomic, strong, readonly) UIImage *rtlImage;

///  自动转换RTL图片
@property(nonatomic, strong, readonly) UIImage *adaptiveRtl;


+ (UIImage *)imageWithUIColor:(UIColor *)color;

- (instancetype)initWithGradient:(NSArray<UIColor *> *)colors size:(CGSize)size direction:(UIImageGradientColorsDirection)direction;

- (instancetype)initWithGradient:(NSArray<UIColor *> *)colors locations:(const CGFloat * __nullable)locations size:(CGSize)size direction:(UIImageGradientColorsDirection)direction;
@end

NS_ASSUME_NONNULL_END
