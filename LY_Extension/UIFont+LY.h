//
//  UIFont+LY.h
//  SimpleDate
//
//  Created by jianyue on 2020/2/27.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIFont (LY)

+ (UIFont *)mediumFontOfSize:(CGFloat)fontSize;

@end

NS_ASSUME_NONNULL_END
