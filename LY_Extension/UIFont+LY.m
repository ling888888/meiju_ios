//
//  UIFont+LY.m
//  SimpleDate
//
//  Created by jianyue on 2020/2/27.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "UIFont+LY.h"

@implementation UIFont (LY)

+ (UIFont *)mediumFontOfSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"PingFangSC-Medium" size:fontSize];
}

@end
