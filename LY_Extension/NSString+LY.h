//
//  NSString+LY.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/6.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (LY)

/// 国际化后的字符串
@property(nonatomic, copy, readonly) NSString *localized;

/// 过滤表情后的字符串
@property(nonatomic, copy, readonly) NSString *filterEmojiString;

/// 获取文本Size
/// @param font 文本字体
/// @param size 最大Size
- (CGSize)sizeWithFont:(UIFont*)font andMaxSize:(CGSize)size;

///// 判断是否包含表情（不好用，待更改）
//- (BOOL)containsEmoji;

@end

NS_ASSUME_NONNULL_END
