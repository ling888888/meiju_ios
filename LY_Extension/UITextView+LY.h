//
//  UITextView+LY.h
//  SimpleDate
//
//  Created by 杨宝龙 on 2020/5/18.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextView (LY)

// 需要限制的字数(优先级高于lines)
@property (nonatomic,  copy) NSNumber *limitLength;

// 需要限制的行数
@property (nonatomic,  copy) NSNumber *limitLines;

@end

NS_ASSUME_NONNULL_END
