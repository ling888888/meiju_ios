//
//  UITextField+LY.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/5.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextField (LY)

- (NSString *)changeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

@end

NS_ASSUME_NONNULL_END
