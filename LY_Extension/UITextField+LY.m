//
//  UITextField+LY.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/5.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "UITextField+LY.h"

@implementation UITextField (LY)

- (NSString *)changeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // 当前完整内容
    NSString *content = self.text;
    
    if (range.length == 0) {
        // 当前是添加内容
        content = [content stringByAppendingString:string];
    } else {
        // 当前是删除内容
        content = [content substringToIndex:content.length - 1];
    }
    return content;
}

@end
