//
//  UIColor+LY.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/2.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "UIColor+LY.h"

@implementation UIColor (LY)

+ (instancetype)colorGradientChangeWithSize:(CGSize)size
                                  direction:(LYGradientChangeDirection)direction
                                 startColor:(UIColor *)startcolor
                                   endColor:(UIColor *)endColor {
    
    if (CGSizeEqualToSize(size, CGSizeZero) || !startcolor || !endColor) {
        return nil;
    }
    
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = CGRectMake(0, 0, size.width, size.height);

    CGPoint startPoint = CGPointZero;
    if (direction == LYGradientChangeDirectionDownDiagonalLine) {
        startPoint = CGPointMake(0.0, 1.0);
    }
    gradientLayer.startPoint = startPoint;
    
    CGPoint endPoint = CGPointZero;
    switch (direction) {
        case LYGradientChangeDirectionHorizontal:
            endPoint = CGPointMake(1.0, 0.0);
            break;
        case LYGradientChangeDirectionVertical:
            endPoint = CGPointMake(0.0, 1.0);
            break;
        case LYGradientChangeDirectionUpwardDiagonalLine:
            endPoint = CGPointMake(1.0, 1.0);
            break;
        case LYGradientChangeDirectionDownDiagonalLine:
            endPoint = CGPointMake(1.0, 0.0);
            break;
        default:
            break;
    }
    gradientLayer.endPoint = endPoint;
    
    gradientLayer.colors = @[(__bridge id)startcolor.CGColor, (__bridge id)endColor.CGColor];
    UIGraphicsBeginImageContext(size);
    [gradientLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage*image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return [UIColor colorWithPatternImage:image];
}

@end
