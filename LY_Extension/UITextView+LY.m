//
//  UITextView+LY.m
//  SimpleDate
//
//  Created by 杨宝龙 on 2020/5/18.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "UITextView+LY.h"
#import <objc/runtime.h>

@implementation UITextView (LY)

static NSString *LIMITLENGTH = @"limitLengthKey";
static NSString *LIMITLINES = @"limitLinesKey";

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextViewTextDidChangeNotification object:self];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.bounces = NO;
}

- (void)setLimitLength:(NSNumber *)limitLength {
    objc_setAssociatedObject(self, &LIMITLENGTH, limitLength, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewChanged:) name:UITextViewTextDidChangeNotification object:self];
}

- (void)setLimitLines:(NSNumber *)limitLines {
    objc_setAssociatedObject(self, &LIMITLINES, limitLines, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewChanged:) name:UITextViewTextDidChangeNotification object:self];
}

#pragma mark -- NSNotification

- (void)textViewChanged:(NSNotification *)notification {
    NSInteger wordCount = self.text.length;
    if (self.limitLength) {//字数限制
        if (wordCount > [self.limitLength integerValue]) {
            wordCount = [self.limitLength integerValue];
        }
        NSString *keyboardType = self.textInputMode.primaryLanguage;
        if ([keyboardType isEqualToString:@"zh-Hans"]) {//对简体中文做特殊处理>>>>高亮拼写问题
            UITextRange *range = [self markedTextRange];
            if (!range) {
                if (self.text.length > [self.limitLength intValue]) {
                    self.text = [self.text substringToIndex:[self.limitLength intValue]];
                    // 已经是最大字数
                }else {/*有高亮不做限制*/}
            }
        }else {
            if ([self.text length] > [self.limitLength intValue]) {
                self.text = [self.text substringToIndex:[self.limitLength intValue]];
                // 已经是最大字数
            }
        }
    }else {
        if (self.limitLines) {//行数限制
            float limitHeight = self.font.lineHeight * [self.limitLines intValue];
            if ([self getTextContentSize].height > limitHeight) {
                while ([self getTextContentSize].height > limitHeight) {
                    self.text = [self.text substringToIndex:self.text.length - 1];
                }
                // 已经是最大行数/n行数限制
            }
            /*行数的限制，没有做右下角提示*/
        }
    }
    self.scrollEnabled = YES;
}
- (CGSize)getTextContentSize {
    return [self getStringPlaceSize:self.text textFont:self.font bundingSize:CGSizeMake(self.contentSize.width-10, CGFLOAT_MAX)];
}
- (CGSize)getStringPlaceSize:(NSString *)string textFont:(UIFont *)font bundingSize:(CGSize)boundSize {
    //计算文本高度
    NSDictionary *attribute = @{NSFontAttributeName:font};
    NSStringDrawingOptions option = (NSStringDrawingOptions)(NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading);
    CGSize size = [string boundingRectWithSize:boundSize options:option attributes:attribute context:nil].size;
    return size;
}

- (NSNumber *)limitLength {
    return objc_getAssociatedObject(self, &LIMITLENGTH);
}
- (NSNumber *)limitLines {
    return objc_getAssociatedObject(self, &LIMITLINES);
}

@end
