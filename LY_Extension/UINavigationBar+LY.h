//
//  UINavigationBar+LY.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UINavigationBar (LY)

/// 背景颜色
- (void)vhl_setBackgroundColor:(UIColor *)color;

/// 背景透明度
- (void)vhl_setBackgroundAlpha:(CGFloat)alpha;

/// 底部分割线是否隐藏
- (void)vhl_setShadowImageHidden:(BOOL)hidden;

/*
 iOS 13:
 - UINavigationBar
 - - _UIBarBackground
 - - - _UIBarBackgroundShadowView
 - - - UIVisualEffectView
 
 iOS 12.4:
 - UINavigationBar
 - - _UIBarBackground
 - - - UIImageView
 - - - UIVisualEffectView
 
 当执行：[self setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault]; 后
 UIVisualEffectView及其子视图被移除，新增UIImageiView
*/
@end

NS_ASSUME_NONNULL_END
