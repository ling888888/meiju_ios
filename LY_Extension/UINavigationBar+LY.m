//
//  UINavigationBar+LY.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "UINavigationBar+LY.h"
#import <objc/runtime.h>

@interface UINavigationBar ()

@property(nonatomic, strong, readonly) UIView *backgroundView;

@property(nonatomic, strong, readonly) UIView *visualEffectView;

@end

@implementation UINavigationBar (LY)

static char UINavigationBarBackgroundView;
static char UINavigationBarVisualEffectView;

- (UIView *)backgroundView {
    UIView *_backgroundView = objc_getAssociatedObject(self, &UINavigationBarBackgroundView);
    if (!_backgroundView) {
        UIView *_UIBarBackground = nil;
        for (UIView *view in self.subviews) {
            NSString *viewClassName = NSStringFromClass([view class]);
            if ([viewClassName containsString:@"_UIBarBackground"]) {
                _UIBarBackground = view;
                break;
            }
        }
        if (!_UIBarBackground) {
            return nil;
        }
        _backgroundView = [[UIView alloc] init];
//        [_UIBarBackground addSubview:_backgroundView];
//        [_UIBarBackground insertSubview:_backgroundView atIndex:1];
        
        _backgroundView.frame = CGRectMake(0, -statusBarHeight, kScreenW, naviBarHeight);
        _backgroundView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin |
        UIViewAutoresizingFlexibleTopMargin |
        UIViewAutoresizingFlexibleWidth |
        UIViewAutoresizingFlexibleHeight;
        /** iOS11下导航栏不显示问题 */
//        if (self.subviews.count > 0) {
//            [self.subviews.firstObject insertSubview:self.backgroundView atIndex:0];
//        } else {
            [_UIBarBackground insertSubview:_backgroundView atIndex:1];
        objc_setAssociatedObject(self, &UINavigationBarBackgroundView, _backgroundView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return _backgroundView;
}

- (UIView *)visualEffectView {
    UIView *_visualEffectView = objc_getAssociatedObject(self, &UINavigationBarVisualEffectView);
    if (!_visualEffectView) {
        if (self.backgroundView.subviews.count > 1) {
            for (int i = 0; i < self.backgroundView.subviews.count; i ++) {
                UIView *view = self.backgroundView.subviews[i];
                NSString *viewClassName = NSStringFromClass([view class]);
                if ([viewClassName containsString:@"UIVisualEffectView"]) {
                    _visualEffectView = view;
                    break;
                }
            }
//            _visualEffectView.alpha = 0;
//            [_visualEffectView removeFromSuperview];
        }
    }
    return _visualEffectView;
}

#pragma mark - public method

/** 设置当前 NavigationBar 背景透明度*/
- (void)vhl_setBackgroundAlpha:(CGFloat)alpha {
    self.backgroundView.alpha = alpha;
}

// -> 设置导航栏背景颜色
- (void)vhl_setBackgroundColor:(UIColor *)color {
    [self setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    self.backgroundView.backgroundColor = color;
}

/** 设置当前 NavigationBar 底部分割线是否隐藏*/
- (void)vhl_setShadowImageHidden:(BOOL)hidden {
    self.shadowImage = hidden ? [UIImage new] : nil;
}

@end
