//
//  UIImage+LY.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "UIImage+LY.h"

@implementation UIImage (LY)

- (UIImage *)rtlImage {
    return [UIImage imageWithCGImage:self.CGImage scale:self.scale orientation:UIImageOrientationUpMirrored];
}

- (UIImage *)adaptiveRtl {
    if (kIsMirroredLayout) {
        return self.rtlImage;;
    } else {
        return self;
    }
}

+ (UIImage *)imageWithUIColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}

/// 生成渐变色图片
/// @param colors 颜色数组
/// @param size 图片尺寸
/// @param direction 方向
- (instancetype)initWithGradient:(NSArray<UIColor *> *)colors size:(CGSize)size direction:(UIImageGradientColorsDirection)direction
{
    self = [super init];
    if (self) {
        self = [self initWithGradient:colors locations:nil size:size direction:direction];
//        UIGraphicsBeginImageContextWithOptions(size, false, [UIScreen mainScreen].scale);
//        CGContextRef context = UIGraphicsGetCurrentContext();
//        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
//
//        NSMutableArray *cgColors = [NSMutableArray array];
//        for (UIColor *color in colors) {
//            [cgColors addObject:(id)color.CGColor];
//        }
//
//        CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (CFArrayRef)cgColors, nil);
//        if (direction == UIImageGradientColorsDirectionVertical) {
//            CGContextDrawLinearGradient(context, gradient, CGPointZero, CGPointMake(0, size.height), kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation);
//        } else {
//            CGContextDrawLinearGradient(context, gradient, CGPointZero, CGPointMake(size.width, 0), kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation);
//        }
//        UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
//        self = [self initWithCGImage:img.CGImage];
//        UIGraphicsEndImageContext();
    }
    return self;
}

- (instancetype)initWithGradient:(NSArray<UIColor *> *)colors locations:(const CGFloat * __nullable)locations size:(CGSize)size direction:(UIImageGradientColorsDirection)direction
{
    self = [super init];
    if (self) {
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(size.width / 2, size.height / 2), false, [UIScreen mainScreen].scale);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        
        NSMutableArray *cgColors = [NSMutableArray array];
        for (UIColor *color in colors) {
            [cgColors addObject:(id)color.CGColor];
        }
        CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (CFArrayRef)cgColors, locations);
        if (direction == UIImageGradientColorsDirectionVertical) {
            CGContextDrawLinearGradient(context, gradient, CGPointZero, CGPointMake(0, size.height / 2), kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation);
        } else {
            CGContextDrawLinearGradient(context, gradient, CGPointZero, CGPointMake(size.width / 2, 0), kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation);
        }
        UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
        self = [self initWithCGImage:img.CGImage];
        UIGraphicsEndImageContext();
    }
    return self;
}

@end
