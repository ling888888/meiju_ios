//
//  UIScrollView+LY.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/8.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "UIScrollView+LY.h"
#import <objc/runtime.h>

static char *vOffsetKey = "vOffsetKey";
static char *showEmptyTipViewKey = "showEmptyTipViewKey";
static char *tipTextKey = "tipTextKey";
static char *tipImageNameKey = "tipImageNameKey";
static char *tipButtonTitleKey = "tipButtonTitleKey";

static char *bgViewKey = "bgViewKey";
static char *tipImgViewKey = "tipImgViewKey";
static char *tipTextLabelKey = "tipTextLabelKey";
static char *tipButtonKey = "tipButtonKey";

@interface UIScrollView ()

// 提示背景视图
@property(nonatomic, strong) UIView *bgView;

// 提示图片
@property(nonatomic, strong) UIImageView *tipImgView;

// 提示文本
@property(nonatomic, strong) UILabel *tipTextLabel;

@end

@implementation UIScrollView (LY)

- (UIView *)bgView {
    UIView *_bgView = objc_getAssociatedObject(self, &bgViewKey);
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        
        objc_setAssociatedObject(self, &bgViewKey, _bgView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return _bgView;
}

- (UIImageView *)tipImgView {
    UIImageView *_tipImgView = objc_getAssociatedObject(self, &tipImgViewKey);
    if (!_tipImgView) {
        _tipImgView = [[UIImageView alloc] init];
        _tipImgView.contentMode = UIViewContentModeScaleAspectFit;
        
        objc_setAssociatedObject(self, &tipImgViewKey, _tipImgView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return _tipImgView;
}

- (UILabel *)tipTextLabel {
    UILabel *_tipTextLabel = objc_getAssociatedObject(self, &tipTextLabelKey);
    if (!_tipTextLabel) {
        _tipTextLabel = [[UILabel alloc] init];
        _tipTextLabel.textColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.6];
        _tipTextLabel.font = [UIFont mediumFontOfSize:15];
        _tipTextLabel.textAlignment = NSTextAlignmentCenter;
        _tipTextLabel.numberOfLines = 0;
        _tipTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
        
        objc_setAssociatedObject(self, &tipTextLabelKey, _tipTextLabel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return _tipTextLabel;
}

- (UIButton *)tipButton {
    UIButton *_tipButton = objc_getAssociatedObject(self, &tipButtonKey);
    if (!_tipButton) {
        _tipButton = [[UIButton alloc] init];
        [_tipButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _tipButton.titleLabel.font = [UIFont mediumFontOfSize:15];
        _tipButton.layer.cornerRadius = 15;
        _tipButton.layer.masksToBounds = true;
        _tipButton.contentEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 15);
        objc_setAssociatedObject(self, &tipButtonKey, _tipButton, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return _tipButton;
}


- (void)setShowEmptyTipView:(BOOL)showEmptyTipView {
//    objc_setAssociatedObject(self, &showEmptyTipViewKey, [NSNumber numberWithBool:showEmptyTipView], OBJC_ASSOCIATION_ASSIGN);
    
    if (showEmptyTipView) {
        [self addSubview:self.bgView];
        [self.bgView addSubview:self.tipImgView];
        [self.bgView addSubview:self.tipTextLabel];
        [self.bgView addSubview:self.tipButton];
        
        [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self);
            make.centerY.mas_equalTo(self.vOffset);
            make.width.mas_equalTo(210);
        }];
        
        [self.tipImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.bgView);
            make.centerX.mas_equalTo(self.bgView);
        }];
        
        [self.tipTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.tipImgView.mas_bottom).offset(10);
            make.leading.trailing.mas_equalTo(self.bgView);
        }];
        
        [self.tipButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.tipTextLabel.mas_bottom).offset(40);
    //        make.leading.trailing.mas_equalTo(_bgView);
            make.centerX.mas_equalTo(self.tipTextLabel);
            make.bottom.mas_equalTo(self.bgView);
            make.height.mas_equalTo(30);
        }];
    } else {
        if (objc_getAssociatedObject(self, &bgViewKey) != nil) {
            [objc_getAssociatedObject(self, &bgViewKey) removeFromSuperview];
            objc_setAssociatedObject(self, &bgViewKey, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        }
    }
}

/// 设置垂直偏移
- (void)setVOffset:(CGFloat)vOffset {
    objc_setAssociatedObject(self, &vOffsetKey, [NSNumber numberWithFloat:vOffset], OBJC_ASSOCIATION_ASSIGN);
    
    if (self.bgView.superview != nil) {
        [self.bgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self).offset(vOffset);
        }];
    }
}

/// 设置提示文字
- (void)setTipText:(NSString *)tipText {
    objc_setAssociatedObject(self, &tipTextKey, tipText, OBJC_ASSOCIATION_ASSIGN);
    
    self.tipTextLabel.text = tipText;
}

/// 设置提示图片名
- (void)setTipImageName:(NSString *)tipImageName {
    objc_setAssociatedObject(self, &tipImageNameKey, tipImageName, OBJC_ASSOCIATION_ASSIGN);
    
    self.tipImgView.image = [UIImage imageNamed:tipImageName];
}

- (void)setTipButtonTitle:(NSString *)tipButtonTitle {
    objc_setAssociatedObject(self, &tipButtonTitleKey, tipButtonTitle, OBJC_ASSOCIATION_ASSIGN);
    
    if (tipButtonTitle.notEmpty) {
        [self.tipButton setTitle:tipButtonTitle forState:UIControlStateNormal];
        [self.tipButton setHidden:false];
    } else {
        [self.tipButton setHidden:true];
    }
}

- (void)setTipButton:(UIButton *)tipButton {
    objc_setAssociatedObject(self, &tipButtonKey, tipButton, OBJC_ASSOCIATION_ASSIGN);
}


- (BOOL)showEmptyTipView {
    return [objc_getAssociatedObject(self, &showEmptyTipViewKey) boolValue];
}
- (CGFloat)vOffset {
    return [objc_getAssociatedObject(self, &vOffsetKey) floatValue];
}
- (NSString *)tipText {
    return objc_getAssociatedObject(self, &tipTextKey);
}
- (NSString *)tipImageName {
    return objc_getAssociatedObject(self, &tipImageNameKey);
}
- (NSString *)tipButtonTitle {
    return objc_getAssociatedObject(self, &tipButtonTitleKey);
}

@end
