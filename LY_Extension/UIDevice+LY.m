//
//  UIDevice+LY.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/2.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "UIDevice+LY.h"
#import "AppManager.h"

@implementation UIDevice (LY)

- (NSString *)identifier {
    return [[AppManager sharedInstance] getDeviceId];
}

- (BOOL)is_iPhoneX {
    return [UIApplication sharedApplication].statusBarFrame.size.height >= 44;
}

@end
