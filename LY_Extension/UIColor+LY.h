//
//  UIColor+LY.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/2.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 渐变方式

 - LYGradientChangeDirectionLevel:              水平渐变
 - LYGradientChangeDirectionVertical:           竖直渐变
 - LYGradientChangeDirectionUpwardDiagonalLine: 向下对角线渐变
 - LYGradientChangeDirectionDownDiagonalLine:   向上对角线渐变
 */
typedef NS_ENUM(NSInteger, LYGradientChangeDirection) {
    LYGradientChangeDirectionHorizontal,
    LYGradientChangeDirectionVertical,
    LYGradientChangeDirectionUpwardDiagonalLine,
    LYGradientChangeDirectionDownDiagonalLine,
};

@interface UIColor (LY)

/**
 创建渐变颜色

 @param size       渐变的size
 @param direction  渐变方式
 @param startcolor 开始颜色
 @param endColor   结束颜色

 @return 创建的渐变颜色
 */
+ (instancetype)colorGradientChangeWithSize:(CGSize)size
                                  direction:(LYGradientChangeDirection)direction
                                 startColor:(UIColor *)startcolor
                                   endColor:(UIColor *)endColor;

@end

NS_ASSUME_NONNULL_END
