//
//  SPDCommonDefine.h
//  SimpleDate
//
//  Created by 侯玲 on 2019/4/27.
//  Copyright © 2019 WYB. All rights reserved.
//

#ifndef SPDCommonDefine_h
#define SPDCommonDefine_h

static NSString *const Headwear = @"headwear";
static NSString *const Bubble = @"bubble";
static NSString *const HomePage_Falling = @"homepage_falling";
static NSString *const Medal = @"medal";

#endif /* SPDCommonDefine_h */
