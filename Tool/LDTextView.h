//
//  LDTextView.h
//  Easyto
//
//  Created by 段乾磊 on 16/6/14.
//  Copyright © 2016年 cherry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LDTextView : UITextView
@property(nullable, nonatomic,copy) NSString *placeholder;
@property(nullable, nonatomic,copy) UIColor *placeholderColor;
@property(nullable, nonatomic,strong) UIFont *placeholderFont;
@property(nonatomic,assign) BOOL placeholderAlignCenter;
@end
