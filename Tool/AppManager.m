//
//  AppManager.m
//  SimpleDate
//
//  Created by xu.juvenile on 16/7/8.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "AppManager.h"
#import "SSKeychain.h"

@implementation AppManager

+ (AppManager *)sharedInstance{
    static dispatch_once_t onceToken;
    static AppManager *shared = nil;

    dispatch_once(&onceToken, ^{
        shared = [[AppManager alloc] init];
    });
    
    return shared;
}


- (NSString *)getDeviceId
{
    NSString *serviceName = @"com.SimpleDate.JianYue";
    
    NSString * currentDeviceUUIDStr = [SSKeychain passwordForService:serviceName account:@"user"];
    
    if (currentDeviceUUIDStr == nil || [currentDeviceUUIDStr isEqualToString:@""])
    {
        NSUUID * currentDeviceUUID  = [UIDevice currentDevice].identifierForVendor;
        currentDeviceUUIDStr = currentDeviceUUID.UUIDString;
        currentDeviceUUIDStr = [currentDeviceUUIDStr stringByReplacingOccurrencesOfString:@"-" withString:@""];
        currentDeviceUUIDStr = [currentDeviceUUIDStr lowercaseString];
        [SSKeychain setPassword: currentDeviceUUIDStr forService:serviceName account:@"user"];

    }
    return currentDeviceUUIDStr;
}
@end
