//
//  WXManager.m
//  SimpleDate
//
//  Created by ling Hou on 2020/10/19.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "WXManager.h"

@interface WXManager ()

@end

@implementation WXManager

+ (instancetype)sharedInstance {
    static WXManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[WXManager alloc] init];
    });
    return manager;
}

// 发送登录请求

- (void)sendLoginReq {
    SendAuthReq* req = [[SendAuthReq alloc]init];
    req.scope = @"snsapi_userinfo";
    req.state = [SPDApiUser currentUser].userId;
    [WXApi sendReq:req completion:^(BOOL success) {
                
    }];
}

#pragma mark - WXApiDelegate

- (void)onReq:(BaseReq*)req {
    
}

- (void)onResp:(BaseResp*)resp {
    NSLog(@"BaseReq ---- %@",@(resp.errCode));
    if ([resp isKindOfClass:[SendAuthResp class]] && resp.errCode == 0) {
        SendAuthResp * loginResponse = (SendAuthResp *)resp;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"WXAuthSuccess" object:nil userInfo:@{@"code":loginResponse.code}];
    }
}


@end
