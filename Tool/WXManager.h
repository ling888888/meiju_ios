//
//  WXManager.h
//  SimpleDate
//
//  Created by ling Hou on 2020/10/19.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WXApi.h"
NS_ASSUME_NONNULL_BEGIN

@interface WXManager : NSObject<WXApiDelegate>

+ (instancetype)sharedInstance;

- (void)sendLoginReq; // 发送登录请求

@end

NS_ASSUME_NONNULL_END
