//
//  DBUtil.m
//  SimpleDate
//
//  Created by xu.juvenile on 2016/4/29.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "DBUtil.h"
#import "FMDB.h"

#define DB_FILE_NAME @"data.sqlite"

@implementation DBUtil
    
+ (FMDatabase *)openDB:(NSString *)dbFileName {
    NSString *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;
    FMDatabase *database = [[FMDatabase alloc] initWithPath:[documentPath stringByAppendingPathComponent:dbFileName]];
    [database open];
        
    [database executeUpdate:@"create table if not exists clanHistory (id text primary key, user_id text, clan_id text, time integer)"];
    
    [database executeUpdate:@"create table if not exists searchHistory (id text primary key, user_id text, search_id text, time integer)"];
    
    [database executeUpdate:@"create table if not exists relationMsgStatus (messageUId text primary key, state integer)"]; // 0代表没有点击 1代表点击过了

    [database executeUpdate:@"create table if not exists shareLimit (user_id text primary key, count integer, time text)"];
    
    [database executeUpdate:@"create table if not exists APServedOrder (orderId text primary key)"];
    
    [database executeUpdate:@"create table if not exists radioHistory (id text primary key, userId text, radioId text, time integer)"];
    
    [database executeUpdate:@"create table if not exists radioSearchHistory (id text primary key, user_id text, search_text text, time integer)"];
    
    [database executeUpdate:@"create table if not exists IAP (transactionId text primary key, userId text, billId text, receipt text, type integer)"];
    
    return database;
}

#pragma mark - ClanHistory
    
+ (void)recordClanHistory:(NSString *)clan_id {
    FMDatabase *database = [DBUtil openDB:DB_FILE_NAME];
    NSString *sql = @"REPLACE INTO clanHistory ('id', 'user_id', 'clan_id', 'time') VALUES (?, ?, ?, ?)";
    NSString *user_id = [SPDApiUser currentUser].userId;
    NSString *key = [NSString stringWithFormat:@"%@%@", user_id, clan_id];
    NSInteger time = [[NSDate date] timeIntervalSince1970];;
    [database executeUpdate:sql, key, user_id, clan_id, @(time)];
    [database close];
}
    
+ (NSMutableArray *)queryClanHistory {
    FMDatabase *database = [DBUtil openDB:DB_FILE_NAME];
    NSString *sql = [NSString stringWithFormat:@"select * from clanHistory where user_id = '%@' order by time desc limit 20", [SPDApiUser currentUser].userId];
    FMResultSet *result = [database executeQuery:sql];
    NSMutableArray *array = [NSMutableArray new];
    while ([result next]) {
        [array addObject:[result stringForColumn:@"clan_id"]?:@""];
    }
    [database close];
    return array;
}
    
+ (void)deleteClanHistory:(NSString *)clan_id {
    FMDatabase *database = [DBUtil openDB:DB_FILE_NAME];
    NSString *sql = [NSString stringWithFormat:@"delete from clanHistory where id = '%@%@'", [SPDApiUser currentUser].userId, clan_id];
    [database executeUpdate:sql];
    [database close];
}
    
+ (void)deleteClanHistory {
    FMDatabase *database = [DBUtil openDB:DB_FILE_NAME];
    NSString *sql = [NSString stringWithFormat:@"delete from clanHistory where user_id = '%@'", [SPDApiUser currentUser].userId];
    [database executeUpdate:sql];
    [database close];
}
    
#pragma mark - SearchHistory
    
+ (void)recordSearchHistory:(NSString *)search_id {
    FMDatabase *database = [DBUtil openDB:DB_FILE_NAME];
    NSString *sql = @"REPLACE INTO searchHistory ('id', 'user_id', 'search_id', 'time') VALUES (?, ?, ?, ?)";
    NSString *user_id = [SPDApiUser currentUser].userId;
    NSString *key = [NSString stringWithFormat:@"%@%@", user_id, search_id];
    NSInteger time = [[NSDate date] timeIntervalSince1970];
    [database executeUpdate:sql, key, user_id, search_id, @(time)];
    [database close];
}
    
+ (NSMutableArray *)querySearchHistory {
    FMDatabase *database = [DBUtil openDB:DB_FILE_NAME];
    NSString *sql = [NSString stringWithFormat:@"select * from searchHistory where user_id = '%@' order by time desc limit 20", [SPDApiUser currentUser].userId];
    FMResultSet *result = [database executeQuery:sql];
    NSMutableArray *array = [NSMutableArray array];
    while ([result next]) {
        [array addObject:[result stringForColumn:@"search_id"]];
    }
    [database close];
    return array;
}
    
+ (void)deleteSearchHistory {
    FMDatabase *database = [DBUtil openDB:DB_FILE_NAME];
    NSString *sql = [NSString stringWithFormat:@"delete from searchHistory where user_id = '%@'", [SPDApiUser currentUser].userId];
    [database executeUpdate:sql];
    [database close];
}

#pragma mark - MessageState

+ (void)recordMessage:(NSString *)messageUId state:(NSInteger)state {
    FMDatabase *database = [DBUtil openDB:DB_FILE_NAME];
    NSString *sql = @"REPLACE INTO relationMsgStatus ('messageUId', 'state') VALUES (?, ?)";
    [database executeUpdate:sql, messageUId, @(state)];
    [database close];
}

+ (BOOL)queryMessageState:(NSString *)messageUId {
    FMDatabase *database = [DBUtil openDB:DB_FILE_NAME];
    NSString *sql = [NSString stringWithFormat:@"select * from relationMsgStatus where messageUId = '%@' and state = '%@'", messageUId, @(1)];
    FMResultSet *result = [database executeQuery:sql];
    [database close];
    return [result next];
}

#pragma mark - ShareLimit

+ (void)recordShareLimit:(NSInteger)count {
    FMDatabase *database = [DBUtil openDB:DB_FILE_NAME];
    NSString *sql = @"REPLACE INTO shareLimit ('user_id', 'count', 'time') VALUES (?, ?, ?)";
    NSString *user_id = [SPDApiUser currentUser].userId;
    NSString *time = [SPDCommonTool getCurrentTimes];
    [database executeUpdate:sql, user_id, @(count), time];
    [database close];
}

+ (NSInteger)queryShareLimit {
    FMDatabase *database = [DBUtil openDB:DB_FILE_NAME];
    NSString *sql = [NSString stringWithFormat:@"select * from shareLimit where user_id = '%@'", [SPDApiUser currentUser].userId];
    FMResultSet *result = [database executeQuery:sql];
    while ([result next]) {
        NSString *time = [result stringForColumn:@"time"];
        if (![time isEqualToString:[SPDCommonTool getCurrentTimes]]) {
            [database close];
            [self deleteShareLimit];
            return 0;
        } else {
            NSInteger limit = [result intForColumn:@"count"];
            [database close];
            return limit;
        }
    }
    [database close];
    return 0;
}

+ (void)deleteShareLimit {
    FMDatabase *database = [DBUtil openDB:DB_FILE_NAME];
    NSString *sql = [NSString stringWithFormat:@"delete from shareLimit where user_id = '%@'", [SPDApiUser currentUser].userId];
    [database executeUpdate:sql];
    [database close];
}

#pragma mark - APServedOrder

+ (void)recordAPServedOrderWithOrderId:(NSString *)orderId {
    FMDatabase *database = [DBUtil openDB:DB_FILE_NAME];
    NSString *sql = @"REPLACE INTO APServedOrder ('orderId') VALUES (?)";
    [database executeUpdate:sql, orderId];
    [database close];
}

+ (BOOL)queryAPServedOrderWithOrderId:(NSString *)orderId {
    FMDatabase *database = [DBUtil openDB:DB_FILE_NAME];
    NSString *sql = [NSString stringWithFormat:@"select * from APServedOrder where orderId = '%@'", orderId];
    FMResultSet *resultSet = [database executeQuery:sql];
    [database close];
    return [resultSet next];
}

#pragma mark - RadioHistory

+ (void)recordRadioHistory:(NSString *)radioId {
    FMDatabase *database = [DBUtil openDB:DB_FILE_NAME];
    NSString *sql = @"REPLACE INTO radioHistory ('id', 'userId', 'radioId', 'time') VALUES (?, ?, ?, ?)";
    NSString *user_id = [SPDApiUser currentUser].userId;
    NSString *key = [NSString stringWithFormat:@"%@%@", user_id, radioId];
    NSInteger time = [[NSDate date] timeIntervalSince1970];;
    [database executeUpdate:sql, key, user_id, radioId, @(time)];
    [database close];
}
    
+ (NSMutableArray *)queryRadioHistory {
    NSMutableArray *array = [NSMutableArray array];
    FMDatabase *database = [DBUtil openDB:DB_FILE_NAME];
    NSString *sql = [NSString stringWithFormat:@"select * from radioHistory where userId = '%@' order by time desc", [SPDApiUser currentUser].userId];
    FMResultSet *result = [database executeQuery:sql];
    while ([result next]) {
        [array addObject:[result stringForColumn:@"radioId"] ? : @""];
    }
    [database close];
    return array;
}

+ (void)deleteRadioHistory:(NSString *)radioId {
    FMDatabase *database = [DBUtil openDB:DB_FILE_NAME];
    NSString *sql = [NSString stringWithFormat:@"delete from radioHistory where id = '%@%@'", [SPDApiUser currentUser].userId, radioId];
    [database executeUpdate:sql];
    [database close];
}

+ (void)deleteRadioHistory {
    FMDatabase *database = [DBUtil openDB:DB_FILE_NAME];
    NSString *sql = [NSString stringWithFormat:@"delete from radioHistory where userId = '%@'", [SPDApiUser currentUser].userId];
    [database executeUpdate:sql];
    [database close];
}

#pragma mark - RadioSearchHistory

+ (void)recordRadioSearchHistory:(NSString *)search_text {
    FMDatabase *database = [DBUtil openDB:DB_FILE_NAME];
    NSString *sql = @"REPLACE INTO radioSearchHistory ('id', 'user_id', 'search_text', 'time') VALUES (?, ?, ?, ?)";
    NSString *user_id = [SPDApiUser currentUser].userId;
    NSString *key = [NSString stringWithFormat:@"%@%@", user_id, search_text];
    NSInteger time = [[NSDate date] timeIntervalSince1970];
    [database executeUpdate:sql, key, user_id, search_text, @(time)];
    [database close];
}
    
+ (NSMutableArray *)queryRadioSearchHistory {
    NSMutableArray *array = [NSMutableArray array];
    FMDatabase *database = [DBUtil openDB:DB_FILE_NAME];
    NSString *sql = [NSString stringWithFormat:@"select * from radioSearchHistory where user_id = '%@' order by time desc limit 10", [SPDApiUser currentUser].userId];
    FMResultSet *result = [database executeQuery:sql];
    while ([result next]) {
        [array addObject:[result stringForColumn:@"search_text"]];
    }
    [database close];
    return array;
}
    
+ (void)deleteRadioSearchHistory {
    FMDatabase *database = [DBUtil openDB:DB_FILE_NAME];
    NSString *sql = [NSString stringWithFormat:@"delete from radioSearchHistory where user_id = '%@'", [SPDApiUser currentUser].userId];
    [database executeUpdate:sql];
    [database close];
}

#pragma mark - IAP

+ (void)recordIAPWithTransactionId:(NSString *)transactionId
                            userId:(NSString *)userId
                            billId:(NSString *)billId
                           receipt:(NSString *)receipt
                              type:(NSInteger)type {
    
    FMDatabase *database = [self openDB:DB_FILE_NAME];
    NSString *update = @"replace into IAP ('transactionId', 'userId', 'billId', 'receipt', 'type') values (?, ?, ?, ?, ?)";
    [database executeUpdate:update, transactionId, userId, billId, receipt, @(type)];
    [database close];
}

+ (NSMutableArray *)queryIAPWithType:(NSInteger)type {
    FMDatabase *database = [self openDB:DB_FILE_NAME];
    NSString *query = [NSString stringWithFormat:@"select * from IAP where userId = '%@' and type = '%@'", [SPDApiUser currentUser].userId, @(type)];
    FMResultSet *result = [database executeQuery:query];
    
    NSMutableArray *array = [NSMutableArray new];
    while ([result next]) {
        NSMutableDictionary *params = [NSMutableDictionary new];
        [params setValue:[result stringForColumn:@"billId"] forKey:@"bill_id"];
        [params setValue:[result stringForColumn:@"transactionId"] forKey:@"transaction_id"];
        [params setValue:[result stringForColumn:@"receipt"] forKey:@"receipt_data"];
        [array addObject:params];
    }
    [database close];
    return array;
}

+ (void)deleteIAPWithTransactionId:(NSString *)transactionId {
    FMDatabase *database = [self openDB:DB_FILE_NAME];
    NSString *update = [NSString stringWithFormat:@"delete from IAP where transactionId = '%@'", transactionId];
    [database executeUpdate:update];
    [database close];
}

@end
