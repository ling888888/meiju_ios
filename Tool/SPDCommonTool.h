//
//  SPDCommonTool.h
//  SimpleDate
//
//  Created by Monkey on 2017/4/1.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewController.h"
#import "SPDPrivateMagicMessage.h"
#import "SPDMagicModel.h"

typedef enum {
    ExpGiftType,
    ExpVehicle,
    EXpmsg_chatroom
}SPDExpStyleType;

typedef NS_ENUM(NSInteger, RelationType) {
    SPDRelationIWantApprentice, //我要收徒
    SPDRelationIWantMaster, //我要拜师
    SPDRelationLetHimBeMaster, //拜他为师
    SPDRelationLetHimBeApprentice //收他为徒
};

typedef void (^SPDCommonToolJudgePic)(BOOL isYellowPicture);
typedef void (^SPDCommonToolAlertBind)(id alertFailed);

extern BOOL isAlreadyDownloadGiftImagesEffect;
extern BOOL isAlreadyDownloadVehicleImagesEffect;
extern BOOL isAlreadyDownloadMagicImagesEffect;

typedef void(^SPDRequestSuccessBlock)(id _Nullable suceess); //请求成功写一个blcok
typedef void(^SPDRequestFailureBlock)(id _Nullable failure); //请求成功写一个blcok

@interface SPDCommonTool : NSObject

+(instancetype)shareTool ;
//处理公共方法的

//检查字符串中是否有emoji
+(BOOL)stringContainEmoji:(NSString *)str;

+(BOOL) isEmpty:(NSString *) str ;

+(BOOL)isContainEmptyStr:(NSString *) str ; //str 包不包含空格
+ (BOOL)isLegalPhoneNumber:(NSString *)str ;
//检查当前系统设置的语言环境
+ (NSString*)getPreferredLanguage;
//当前famy 的语言
+ (NSString*)getFamyLanguage;

//获得传递给服务器的ar
+ (NSString *)getLanguageStrPostToServer:(NSString *)language ;

+ (BOOL)currentVersionIsOversea ;

+ (BOOL)currentVersionIsArbic;

+(void)beginCheakImageWithImage: (UIImage *)image andIsYellowPicture:(SPDCommonToolJudgePic) isYellowPictue ;

+(NSString *)digitUppercase:(NSString *)numstr ;

+(BOOL)setupGiftEffectsFolder:(NSString *)type;

+(void)postiOSMethodToRecoredRCall:(NSString *)methodName;

//vc - > vip
+ (void)pushToVipViewController:(UIViewController *)viewController WithIsVip:(BOOL) isVip ;

+ (void)pushToRechageController:(UIViewController *)viewController ;

+(void)presentAutoController:(UIViewController *)viewController
                       title:(NSString *)title
                  titleColor:(NSString *)titleColor
               titleFontSize:(CGFloat)titleFontSize
               messageString:(NSString *)message
                messageColor:(NSString *)messageColor
             messageFontSize:(CGFloat)messageFontSize
                 cancelTitle:(NSString * )cancelTitle
                cancelAction:(ActionHandler)cancelAction
                confirmTitle:(NSString *)confirmTitle
               confirmAction:(ActionHandler)confirmAction;

+(void)presentCommonController :(UIViewController *)vc
                          title:(NSString *)title
                  messageString:(NSString *)message
                    cancelTitle:(NSString * _Nullable)cancelTitle
                   cancelAction:(_Nullable ActionHandler)cancelAction
                   confirmTitle:(NSString *)confirmTitle
                  confirmAction:(ActionHandler)confirmAction ;


//高斯模糊
+ (UIImage *)coreBlurImage:(UIImage *)image withBlurNumber:(CGFloat)blur;

+(NSString *)getCharmRankNumber:(int)charmRank;

+(NSString *)getRichRankNumber:(int)charmRank;

+(NSString *)getTextMsgClanMarkWithPosition:(NSString *)position;

+ (void)pushToSecurityViewControllerWithController :(UIViewController *)vc ;

+(void)manageTagWithImageView:(UIImageView *)imageView andLevel:(int) level ;

+ (void)configDataForLevelBtnWithLevel:(int) level andUIButton:(UIButton *)level_btn ;

+(NSString*)getCurrentTimes;

- (void)checkExpWithGlodNum:(NSInteger )goldNum WithType:(SPDExpStyleType )expType;

- (void)checkExpToWhetherToUpgrage ;

+(NSString *)getArImageNameWithImage:(NSString *)name ;

+ (void)requestAccountPropertyAPIWithType:(NSString * )type
                         viewController  :(UIViewController *)vc
                          alertBindFailed:(SPDCommonToolAlertBind) failed;

+(SPDPrivateMagicMessage *)getPrivateMagicMessageWithDict:(NSDictionary *)suceess andMagicModel:(SPDMagicModel *)magicModel andReceiveId:(NSString *)receive_id;

+ (UIViewController *)getWindowTopViewController ;

// 请求动效图片
+(void)requestAnimationImagesWithType:(NSString *)type;

+ (void)showWindowToast:(NSString *)str ;

//+ (UIViewController *)getWindowTopViewController;

+ (NSString *)getStringFromCityWholeText:(NSString *)wholeText;

+ (UIImage *)getLanchImage;



+ (NSString *)getInvitationCodeWithUserId:(NSString *)userId;
+(CGSize) getAttributeSizeWithText:(NSString *)text fontSize:(int)fontSize;
+(void)loadWebViewController:(UIViewController *)vc title:(NSString *)title url:(NSString *)url ;

+(void)pushToDetailTableViewController:(UIViewController *)viewController
                                userId:(NSString *)userId;
+(NSString *)getDateStringWithDate:(NSDate *)date
                        DateFormat:(NSString *)formatString;
+ (NSMutableAttributedString *)AttributedString:(NSString *)content andImageNameArray:(NSMutableArray *)array;


+ (void)requestFriendPostWithUserId:(NSString *)userId type:(NSString *)type success:(SPDRequestSuccessBlock)success failure:(SPDRequestFailureBlock)failure;

+ (NSString *)transformIntegerToBriefStr:(NSInteger)integer;

+ (BOOL)getGlobalMessageSwitch;
+ (void)setGlobalMessageSwitch:(BOOL)on;

+ (UIImage *)createImageWithColor:(UIColor *)color size:(CGSize)size;

+ (NSMutableArray *)allRangesOfString:(NSString *)searchString withinString:(NSString *)targetString;

+ (void)presentAlertWithTitle:(nullable NSString *)title
                      message:(nullable NSString *)message
                  cancelTitle:(nullable NSString *)cancelTitle
                cancelHandler:(void (^ __nullable)(UIAlertAction *action))cancelHandler
                  actionTitle:(nullable NSString *)actionTitle
                actionHandler:(void (^ __nullable)(UIAlertAction *action))actionHandler;

+ (NSString *)orderTimeStingWithTimeStamp:(long long)timeStamp;
+ (NSString *)getNobleNameColor:(NSString *)noble;

@end
_Nullable
