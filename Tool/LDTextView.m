//
//  LDTextView.m
//  Easyto
//
//  Created by 段乾磊 on 16/6/14.
//  Copyright © 2016年 cherry. All rights reserved.
//

#import "LDTextView.h"
@interface LDTextView ()

- (void)refreshPlaceholder;

@end

@implementation LDTextView{
    UILabel *placeHolderLabel;
}

@synthesize placeholder = _placeholder;

- (void)initialize {
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(refreshPlaceholder)
     name:UITextViewTextDidChangeNotification
     object:self];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initialize];
}

- (void)refreshPlaceholder {
    if ([[self text] length]) {
        [placeHolderLabel setAlpha:0];
    } else {
        [placeHolderLabel setAlpha:1];
    }
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (void)setText:(NSString *)text {
    [super setText:text];
    [self refreshPlaceholder];
}

- (void)setFont:(UIFont *)font {
    [super setFont:font];
    placeHolderLabel.font = self.placeholderFont;
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [placeHolderLabel sizeToFit];
    CGFloat orightY = self.textContainerInset.top;
    if (self.placeholderAlignCenter) {
        orightY = (self.frame.size.height -CGRectGetHeight(placeHolderLabel.frame))/2;
    }
    placeHolderLabel.frame = CGRectMake(4,  orightY, CGRectGetWidth(self.frame) - 8,
                                        CGRectGetHeight(placeHolderLabel.frame));
    
}

- (void)setPlaceholder:(NSString *)placeholder {
    _placeholder = placeholder;
    
    if (placeHolderLabel == nil) {
        placeHolderLabel = [[UILabel alloc] init];
        placeHolderLabel.autoresizingMask =
        (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
        placeHolderLabel.lineBreakMode = NSLineBreakByWordWrapping;
        placeHolderLabel.numberOfLines = 0;
        placeHolderLabel.font = self.placeholderFont;
        placeHolderLabel.backgroundColor = [UIColor clearColor];
        placeHolderLabel.textColor = self.placeholderColor;
        placeHolderLabel.alpha = 0;
        placeHolderLabel.textAlignment = self.textAlignment;
        [self addSubview:placeHolderLabel];
    }
    
    placeHolderLabel.text = self.placeholder;
    [self refreshPlaceholder];
}

- (void)setPlaceholderColor:(UIColor *)placeholderColor {
    _placeholderColor = placeholderColor;
    placeHolderLabel.textColor = _placeholderColor;
}

- (void)setPlaceholderFont:(UIFont *)placeholderFont {
    _placeholderFont = placeholderFont;
    placeHolderLabel.font = _placeholderFont;
}


// When any text changes on textField, the delegate getter is called. At this
// time we refresh the textView's placeholder
- (id<UITextViewDelegate>)delegate {
    [self refreshPlaceholder];
    return [super delegate];
}

@end
