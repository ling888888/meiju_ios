//
//  SPDUMShareUtils.h
//  SimpleDate
//
//  Created by 侯玲 on 17/4/16.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

typedef  void (^SPDUMShareSuccess) (id shareSuccess);
typedef  void (^SPDUMShareFailure) (id shareFailure);

@interface SPDUMShareUtils : NSObject<FBSDKSharingDelegate>

+(instancetype)shareInstance;

//分享到第三方平台
-(void)shareToPlatform:(SPDUMShareType )platform WithShareUrl:(NSString *)shareUrl andshareTitle:(NSString *)shareTitle andshareContent:(NSString *)shareContent andImageData:(UIImage *)image
      andShareSourece :(NSString *)shareSource
  andSPDUMShareSuccess:(SPDUMShareSuccess) shareSuccess
   presentedController:(BaseViewController *)presentedController andSPDUMShareFailure:(SPDUMShareFailure) shareFailure ;


//授权
//+ (void)authWithPlatform:(SPDUMShareType)platform andSPDUMShareSuccess:(SPDUMShareSuccess) shareSuccess andSPDUMShareFailure:(SPDUMShareFailure) shareFailure ;


//获取用户信息
+ (void)getUserInfoWithPlatform:(SPDUMShareType)platform andSPDUMShareSuccess:(SPDUMShareSuccess) shareSuccess andSPDUMShareFailure:(SPDUMShareFailure) shareFailure ;



@end
