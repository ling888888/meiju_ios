//
//  SPDLaunageTool.h
//  SimpleDate
//
//  Created by Monkey on 2017/11/4.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPDLanguageTool : NSObject

@property (nonatomic, copy) NSString *currentLanguage; // 当前语言
@property (nonatomic, copy) NSDictionary *allLanguages; // 所有语言

+ (instancetype)sharedInstance;

/**
 *  返回table中指定的key的值
 *
 *  @param key   key
 *  @param table table
 *
 *  @return 返回table中指定的key的值
 */
- (NSString *)getStringForKey:(NSString *)key;
- (NSString *)getStringForKey:(NSString *)key withTable:(NSString *)table;

/**
 *  设置新的语言
 *
 *  @param language 新语言
 */
- (void)setNewLanguage:(NSString *)language;

@end
