//
//  SPDCommonTool.m
//  SimpleDate
//
//  Created by Monkey on 2017/4/1.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDCommonTool.h"
#import "VipViewController.h"
#import "MyWalletViewController.h"
#import "FDAlertView.h"
#import "SPDUpGradeView.h"
#import "securityViewController.h"
#import "ZegoKitManager.h"
#import "SPDConversationViewController.h"
#import "SPDHelpController.h"
#import "LY_HomePageViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

BOOL isAlreadyDownloadGiftImagesEffect = YES;
BOOL isAlreadyDownloadVehicleImagesEffect = YES;
BOOL isAlreadyDownloadMagicImagesEffect = YES;

@interface SPDCommonTool ()<SPDUpGradeViewDelegate>

@property (nonatomic,strong)FDAlertView *upgradeSuperView;
@property (nonatomic,strong)SPDUpGradeView * upgradeView;
@end

@implementation SPDCommonTool

+(instancetype)shareTool {
    static SPDCommonTool * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance= [[self alloc]init];
    });
    return instance;
}

+(BOOL)stringContainEmoji:(NSString *)str{
    
    __block BOOL returnValue = NO;
    [str enumerateSubstringsInRange:NSMakeRange(0, [str length])
                               options:NSStringEnumerationByComposedCharacterSequences
                            usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                                const unichar high = [substring characterAtIndex: 0];
                                // Surrogate pair (U+1D000-1F9FF)
                                if (0xD800 <= high && high <= 0xDBFF) {
                                    const unichar low = [substring characterAtIndex: 1];
                                    const int codepoint = ((high - 0xD800) * 0x400) + (low - 0xDC00) + 0x10000;
                                    
                                    if (0x1D000 <= codepoint && codepoint <= 0x1F9FF){
                                        returnValue = YES;
                                    }
                                    // Not surrogate pair (U+2100-27BF)
                                } else {
                                    if (0x2100 <= high && high <= 0x27BF){
                                        returnValue = YES;
                                    }
                                }
                            }];
    
    return returnValue;
    
}

//是不是空格
+(BOOL) isEmpty:(NSString *) str {
    if (!str) {
        return true;
    } else {
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        NSString *trimedString = [str stringByTrimmingCharactersInSet:set];
        if ([trimedString length] == 0) {
            return true;
        } else {
            return false;
        }
    }
}

//是不是包含空字符串
+(BOOL)isContainEmptyStr:(NSString *) str {
    NSRange range = [str rangeOfString:@" "];
    if (range.location != NSNotFound) {
        return YES; //yes代表包含空格
    }else {
        return NO; //反之
    }
    
}



//是不是纯数字 包含空格 全是空
+ (BOOL)isLegalPhoneNumber:(NSString *)str {
    if (!str || [str isEqualToString:@""] ) {
        return NO;
    }
    NSString *  str1 = [str stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
    if(str1.length > 0){
        return NO;
    }//不是纯数字
    NSRange range = [str rangeOfString:@" "];
    if (range.location != NSNotFound) {
        return NO; //yes代表包含空格
    }else {
        return YES;
    }
}
//鉴黄
+(void)beginCheakImageWithImage: (UIImage *)image andIsYellowPicture:(SPDCommonToolJudgePic) isYellowPictue {
    
    //image 转换成base64 格式
    NSData *data=UIImageJPEGRepresentation(image,1.0f);
    
    NSString * encodeImageStr=[data base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn];
    
    NSLog(@"===Encoded image:\n%@", encodeImageStr);
    
    //获取当前时间戳
    int timestamp=(int)[[NSDate date] timeIntervalSince1970];
    NSNumber *timeNu=@(timestamp);
    NSLog(@"timeinter==%@",timeNu);
    
    //nonce
    int nonce =arc4random() % 5000;
    
    //签名
    
    NSMutableDictionary *parameters=[NSMutableDictionary new];
    //1.设置公共参数
    [parameters setValue:WYsecretId forKey:@"secretId"];
    [parameters setValue:WYbusinessId forKey:@"businessId"];
    [parameters setValue:@"v3" forKey:@"version"];
    [parameters setValue:timeNu forKey:@"timestamp"];
    [parameters setValue:@(nonce) forKey:@"nonce"];
    
    //2.设置私有参数
    NSDictionary *dict=@{@"name":[SPDApiUser currentUser].userId ?: @"hello",
                         @"type":@(2),
                         @"data":encodeImageStr ? :@"famy"};
    NSArray *images=@[dict];
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:images options:0 error:nil];
    NSString * imagesString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    if (!imagesString) {
        return;
    }
    [parameters setValue:imagesString forKey:@"images"];
    
    //生成签名
    BaseViewController *baseVC = [BaseViewController new];
    NSString *signStr=[baseVC genSignature:parameters andSecretKey:WYsecretKey];
    
    [parameters setValue:signStr forKey:@"signature"];
    
    [RequestUtils SpecialPostRequestUtils:parameters bURL:@"https://api.aq.163.com/v3/image/check" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable responseObject) {
        
        NSMutableArray *judge=[NSMutableArray new];
        
        NSArray *result=responseObject[@"result"];
        
        NSDictionary *dict=result[0];
        
        NSArray *labelsArray=dict[@"labels"];
        
        for (NSDictionary *dic in labelsArray) {
            
            [judge addObject:dic[@"level"]];
        }
        
        BOOL isYePicture ;
        
        if ([judge containsObject:@(1)] || [judge containsObject:@(2)]) {
            NSLog(@"确定是黄图---强制挂断");
            isYePicture = YES;
        }else{
            isYePicture = NO;
            NSLog(@"一切正常");
        }
        
        if (isYellowPictue) {
            isYellowPictue(isYePicture);
        }
        
    } bFailure:nil];
    
}

// @"5" -@"五"
+(NSString *)digitUppercase:(NSString *)numstr{
    double numberals=[numstr doubleValue];
    NSArray *numberchar = @[@"零",@"壹",@"贰",@"叁",@"肆",@"伍",@"陆",@"柒",@"捌",@"玖"];
    NSArray *inunitchar = @[@"",@"拾",@"佰",@"仟"];
    NSArray *unitname = @[@"",@"万",@"亿",@"万亿"];
    //金额乘以100转换成字符串（去除圆角分数值）
    NSString *valstr=[NSString stringWithFormat:@"%.2f",numberals];
    NSString *prefix;
    NSString *suffix;
    if (valstr.length<=2) {
        prefix=@"零元";
        if (valstr.length==0) {
            suffix=@"零角零分";
        }
        else if (valstr.length==1)
        {
            suffix=[NSString stringWithFormat:@"%@分",[numberchar objectAtIndex:[valstr intValue]]];
        }
        else
        {
            NSString *head=[valstr substringToIndex:1];
            NSString *foot=[valstr substringFromIndex:1];
            suffix = [NSString stringWithFormat:@"%@角%@分",[numberchar objectAtIndex:[head intValue]],[numberchar  objectAtIndex:[foot intValue]]];
        }
    }
    else
    {
        prefix=@"";
        suffix=@"";
        NSInteger flag = valstr.length - 2;
        NSString *head=[valstr substringToIndex:flag - 1];
        NSString *foot=[valstr substringFromIndex:flag];
        if (head.length>13) {
            return@"数值太大（最大支持13位整数），无法处理";
        }
        //处理整数部分
        NSMutableArray *ch=[[NSMutableArray alloc]init];
        for (int i = 0; i < head.length; i++) {
            NSString * str=[NSString stringWithFormat:@"%x",[head characterAtIndex:i]-'0'];
            [ch addObject:str];
        }
        int zeronum=0;
        
        for (int i=0; i<ch.count; i++) {
            int index=(ch.count -i-1)%4;//取段内位置
            NSInteger indexloc=(ch.count -i-1)/4;//取段位置
            if ([[ch objectAtIndex:i]isEqualToString:@"0"]) {
                zeronum++;
            }
            else
            {
                if (zeronum!=0) {
                    if (index!=3) {
                        prefix=[prefix stringByAppendingString:@"零"];
                    }
                    zeronum=0;
                }
                prefix=[prefix stringByAppendingString:[numberchar objectAtIndex:[[ch objectAtIndex:i]intValue]]];
                prefix=[prefix stringByAppendingString:[inunitchar objectAtIndex:index]];
            }
            if (index ==0 && zeronum<4) {
                prefix=[prefix stringByAppendingString:[unitname objectAtIndex:indexloc]];
            }
        }
        prefix =[prefix stringByAppendingString:@"元"];
        //处理小数位
        if ([foot isEqualToString:@"00"]) {
            suffix =[suffix stringByAppendingString:@"整"];
        }
        else if ([foot hasPrefix:@"0"])
        {
            NSString *footch=[NSString stringWithFormat:@"%x",[foot characterAtIndex:1]-'0'];
            suffix=[NSString stringWithFormat:@"%@分",[numberchar objectAtIndex:[footch intValue] ]];
        }
        else
        {
            NSString *headch=[NSString stringWithFormat:@"%x",[foot characterAtIndex:0]-'0'];
            NSString *footch=[NSString stringWithFormat:@"%x",[foot characterAtIndex:1]-'0'];
            suffix=[NSString stringWithFormat:@"%@角%@分",[numberchar objectAtIndex:[headch intValue]],[numberchar  objectAtIndex:[footch intValue]]];
        }
    }
    return [prefix stringByAppendingString:suffix];
}

+(BOOL)setupGiftEffectsFolder:(NSString *)type {
    
    NSString * giftImagePath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"/Library/Caches/%@",type]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:giftImagePath]) {
        BOOL create_result =  [[NSFileManager defaultManager] createDirectoryAtPath:giftImagePath withIntermediateDirectories:YES attributes:nil error:nil];
        if (create_result) {
            NSLog(@"创建/Library/Caches/成功");
        }else{
            NSLog(@"创建/Library/Caches/失败");
        }
        return create_result;
    }else{
        NSLog(@"已经存在这个文件夹了");
        return YES;
    }
}

+(void)postiOSMethodToRecoredRCall:(NSString *)methodName{
    NSString *msg=methodName;
    NSDictionary *dict=@{@"msg":msg};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dict] bURL:@"ry.record" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        NSLog(@"发送成功--%@",methodName);
    } bFailure:nil];
}

+ (void)pushToVipViewController:(UIViewController *)viewController WithIsVip:(BOOL) isVip {
    
    UIStoryboard *home=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VipViewController *vc=[home instantiateViewControllerWithIdentifier:@"VipViewController"];
    vc.hidesBottomBarWhenPushed=YES;
    vc.is_vip=isVip;
    [viewController.navigationController pushViewController:vc animated:YES];
    
}

// 跳转到充值页面
+ (void)pushToRechageController:(UIViewController *)viewController{
    [viewController.navigationController pushViewController:[MyWalletViewController new] animated:YES];
}

//带颜色的弹框 比如说金币不足
 +(void)presentCommonController :(UIViewController *)vc
                           title:(NSString *)title
                  messageString:(NSString *)message
                    cancelTitle:(NSString * _Nullable)cancelTitle
                   cancelAction:(_Nullable ActionHandler)cancelAction
                   confirmTitle:(NSString *)confirmTitle
                  confirmAction:(ActionHandler)confirmAction {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    
    if (message) {
        NSMutableAttributedString *messageStr = [[NSMutableAttributedString alloc] initWithString:message];
        [messageStr addAttribute:NSForegroundColorAttributeName value:SPD_HEXCOlOR(@"f56986") range:NSMakeRange(0, message.length)];
        [messageStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(0, message.length)];
        [alertController setValue:messageStr forKey:@"attributedMessage"];
    }
    
    
    if (cancelTitle.length > 0) {
        UIAlertAction *cancelAlertAction =
        [UIAlertAction actionWithTitle:cancelTitle
                                 style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action) {
                                   if (cancelAction) {
                                       cancelAction();
                                   }
                               }];
        
        if ([[UIDevice currentDevice].systemVersion floatValue] > 8.3) {
            [cancelAlertAction setValue:SPD_HEXCOlOR(@"bbbbbb") forKey:@"_titleTextColor"];
        }
        
        [alertController addAction:cancelAlertAction];
    }
    if (confirmTitle.length > 0) {
        UIAlertAction *confirmAlertAction =
        [UIAlertAction actionWithTitle:confirmTitle
                                 style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action) {
                                   if (confirmAction) {
                                       confirmAction();
                                   }
                               }];
        if ([[UIDevice currentDevice].systemVersion floatValue] > 8.3) {
            [confirmAlertAction setValue:SPD_HEXCOlOR(@"3782fe") forKey:@"_titleTextColor"];
        }
        
        [alertController addAction:confirmAlertAction];
    }
    [vc presentViewController:alertController animated:YES completion:nil];
}

+(void)presentAutoController:(UIViewController *)viewController
                    title:(NSString *)title
                   titleColor:(NSString *)titleColor
                titleFontSize:(CGFloat)titleFontSize
                messageString:(NSString *)message
                 messageColor:(NSString *)messageColor
              messageFontSize:(CGFloat)messageFontSize
                  cancelTitle:(NSString * _Nullable)cancelTitle
                 cancelAction:(_Nullable ActionHandler)cancelAction
                 confirmTitle:(NSString *)confirmTitle
                confirmAction:(ActionHandler)confirmAction {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    if (message) {
        NSMutableAttributedString *messageStr = [[NSMutableAttributedString alloc] initWithString:message];
        if (messageColor) {
            [messageStr addAttribute:NSForegroundColorAttributeName value:SPD_HEXCOlOR(messageColor) range:NSMakeRange(0, message.length)];
        }
        if (messageFontSize !=0) {
            [messageStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:messageFontSize] range:NSMakeRange(0, message.length)];
        }
        [alertController setValue:messageStr forKey:@"attributedMessage"];
    }
    
    
    if (cancelTitle.length > 0) {
        UIAlertAction *cancelAlertAction =
        [UIAlertAction actionWithTitle:cancelTitle
                                 style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action) {
                                   if (cancelAction) {
                                       cancelAction();
                                   }
                               }];
        
        if ([[UIDevice currentDevice].systemVersion floatValue] > 8.3) {
            [cancelAlertAction setValue:SPD_HEXCOlOR(@"bbbbbb") forKey:@"_titleTextColor"];
        }
        
        [alertController addAction:cancelAlertAction];
    }
    if (confirmTitle.length > 0) {
        UIAlertAction *confirmAlertAction =
        [UIAlertAction actionWithTitle:confirmTitle
                                 style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action) {
                                   if (confirmAction) {
                                       confirmAction();
                                   }
                               }];
        if ([[UIDevice currentDevice].systemVersion floatValue] > 8.3) {
            [confirmAlertAction setValue:SPD_HEXCOlOR(@"3782fe") forKey:@"_titleTextColor"];
        }
        
        [alertController addAction:confirmAlertAction];
    }
    [viewController presentViewController:alertController animated:YES completion:nil];
}

+ (NSString*)getPreferredLanguage

{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    
    NSArray * allLanguages = [defaults objectForKey:@"AppleLanguages"];
    
    NSString * preferredLang = [allLanguages objectAtIndex:0];
    
        NSLog(@"当前语言:%@", preferredLang);
    [[NSUserDefaults standardUserDefaults] setObject:preferredLang forKey:CURRENTSYSTEMLANGUAGE];
    
    return preferredLang;
    
}

+ (NSString*)getFamyLanguage
{
    NSString * language ;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:CURRENTLANGUAGE]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:CURRENTLANGUAGE];
    }else {
        return [SPDCommonTool getPreferredLanguage];
    }
    return language;
}

+ (NSString *)getLanguageStrPostToServer:(NSString *)language {
    NSString * langStr ;
    NSArray * dataArray = [language componentsSeparatedByString:@"-"];
    langStr = dataArray[0];
    return langStr;
}

+ (BOOL)currentVersionIsOversea {
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    
    NSArray * allLanguages = [defaults objectForKey:@"AppleLanguages"];
    
    NSString * preferredLang = [allLanguages objectAtIndex:0];
    
//    NSLog(@"当前语言:%@", preferredLang);
    if (![preferredLang containsString:@"zh-Hans"]) {
        return YES;
    }else{
        return NO;
    }
}

+ (BOOL)currentVersionIsArbic {
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    
    NSArray * allLanguages = [defaults objectForKey:@"AppleLanguages"];
    
    NSString * preferredLang = [allLanguages objectAtIndex:0];
    
//    NSLog(@"当前语言:%@", preferredLang);
    if ([preferredLang containsString:@"ar"]) {
        return YES;
    }else{
        return NO;
    }
}
+ (UIImage *_Nullable)coreBlurImage:(UIImage *_Nullable)image withBlurNumber:(CGFloat)blur{
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage= [CIImage imageWithCGImage:image.CGImage];
    //设置filter
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:inputImage forKey:kCIInputImageKey]; [filter setValue:@(blur) forKey: @"inputRadius"];
    //模糊图片
    CIImage *result=[filter valueForKey:kCIOutputImageKey];
    CGImageRef outImage=[context createCGImage:result fromRect:[result extent]];
    UIImage *blurImage=[UIImage imageWithCGImage:outImage];
    CGImageRelease(outImage);
    return blurImage;
}


//获得 土豪 美丽的 代表权重～
+(NSString *)getCharmRankNumber:(int)charmRank{
    NSString * rank_numberStr = @"1000";
    if (charmRank == 0) {
        rank_numberStr = @"120";
    }else if (charmRank == 1){
        rank_numberStr = @"118";
    }else if (charmRank == 2){
        rank_numberStr = @"110";
    }else if (charmRank >= 3 && charmRank <= 9){
        rank_numberStr = @"102";
    }
    return rank_numberStr;
}

+(NSString *)getRichRankNumber:(int)charmRank{
    NSString * rank_numberStr = @"1000";
    if (charmRank == 0) {
        rank_numberStr = @"121";
    }else if (charmRank == 1){
        rank_numberStr = @"119";
    }else if (charmRank == 2){
        rank_numberStr = @"115";
    }else if (charmRank >= 3 && charmRank <= 9){
        rank_numberStr = @"106";
    }
    return rank_numberStr;
}

+(NSString *)getTextMsgClanMarkWithPosition:(NSString *)position{
    NSString * mark ;
    if ([position isEqualToString:@"chief"]) {
        mark = @"117";
    }else if ([position isEqualToString:@"deputychief"]){
        mark = @"116";
    }else if ([position isEqualToString:@"member"]){
        mark = @"11";
    }else{
        mark = @"10";
    }
    return mark;
}

+(void)manageTagWithImageView:(UIImageView *)imageView andLevel:(int) level {
    
    switch (level) {
        case Clan_Chief_Level:
            imageView.image = [UIImage imageNamed:@"clan_chief_tag"];
            break;
        case Clan_DeputyChief_Level:
            imageView.image = [UIImage imageNamed:@"clan_deputychief_tag"];
            break;
        case Clan_Member_Level:
            imageView.image = [UIImage imageNamed:@"clan_member_tag"];
            break;
        case Clan_Tourist_Level:
            imageView.image = [UIImage imageNamed:@""];
            break;
        case Rich_first_Level:
            imageView.image = [UIImage imageNamed:@"rich_first_tag"];
            break;
        case Rich_second_Level:
            imageView.image = [UIImage imageNamed:@"rich_second_tag"];
            break;
        case Rich_third_Level:
            imageView.image = [UIImage imageNamed:@"rich_third_tag"];
            break;
        case Rich_fourth_Level:
            imageView.image = [UIImage imageNamed:@"rich_fourth_tag"];
            break;
        case Charm_first_Level:
            imageView.image = [UIImage imageNamed:@"charm_first_tag"];
            break;
        case Charm_second_Level:
            imageView.image = [UIImage imageNamed:@"charm_second_tag"];
            break;
        case Charm_third_Level:
            imageView.image = [UIImage imageNamed:@"charm_third_tag"];
            break;
        case Charm_fourth_Level:
            imageView.image = [UIImage imageNamed:@"charm_fourth_tag"];
            break;
        default:
            break;
    }
}

+ (void)configDataForLevelBtnWithLevel:(int) level andUIButton:(UIButton *)level_btn {
    
    NSString * imageName = @"1-10level";
    if (level>=0 && level <=10) {
        imageName = @"1-10level";
    }else if (level >=11 && level <=20){
        imageName = @"11-20level";
    }else if(level >=21 && level <= 30){
        imageName = @"21-30level";
    }else if (level >=31 && level <= 40){
        imageName = @"31-40level";
    }else if (level >=41 && level <= 50){
        imageName = @"41-50level";
    }else if (level >=51 && level <= 60){
        imageName = @"51-60level";
    }else if (level > 60 && level < 71){
        imageName = @"61-70level";
    }
    else {
        imageName = @"ic_dengji_71-80";
    }
    [level_btn setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [level_btn setTitle:[NSString stringWithFormat:@"%d",level] forState:UIControlStateNormal];
    [level_btn setTitleEdgeInsets:UIEdgeInsetsMake(0, level_btn.bounds.size.width * 0.3, 0, 0)];
}

//获取当前的时间

+(NSString*)getCurrentTimes{
    NSCalendar * calendar = [NSCalendar currentCalendar];
    NSDateComponents * dateCompontents = [calendar components:(NSCalendarUnitYear |NSCalendarUnitMonth | NSCalendarUnitDay |NSCalendarUnitHour |NSCalendarUnitMinute |NSCalendarUnitSecond) fromDate:[NSDate date]];
//    dateCompontents.year-=20;
    NSString *month=@"";
    if (dateCompontents.month<10) {
        month=[NSString stringWithFormat:@"0%ld",dateCompontents.month];
    }else{
        month=[NSString stringWithFormat:@"%ld",dateCompontents.month];
    }
    NSString *day=@"";
    if (dateCompontents.day<10) {
        day=[NSString stringWithFormat:@"0%ld",dateCompontents.day];
    }else{
        day=[NSString stringWithFormat:@"%ld",dateCompontents.day];
    }
    NSString *currentTimeString=[NSString stringWithFormat:@"%ld-%@-%@",dateCompontents.year,month,day];
    
    return currentTimeString;
    
}


//送出接受礼物 座驾 用level 来判断
- (void)checkExpToWhetherToUpgrage {
    
    NSMutableDictionary * dict = [NSMutableDictionary new];
    [dict setObject:[SPDApiUser currentUser].userId forKey:@"user_id"];
    [RequestUtils commonGetUrlRequestUtils:dict bURL:@"userlevel" RequestType:SPDRankUrl bAnimated:NO bAnimatedViewController:nil bSuccessDoSomething:^(id  _Nullable suceess) {
        
        NSDictionary * dict = suceess[@"user_level"];
        NSInteger  curr_level = [dict[@"curr_level"] integerValue];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@%@",MYCURRENTLEVEL,[SPDApiUser currentUser].userId]]) {
            NSInteger myCurrentLevel = [[[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"%@%@",MYCURRENTLEVEL,[SPDApiUser currentUser].userId]] integerValue];
            if (myCurrentLevel < curr_level) {
                //给弹框
                [[NSUserDefaults standardUserDefaults] setObject:dict[@"curr_level"]  forKey:[NSString stringWithFormat:@"%@%@",MYCURRENTLEVEL,[SPDApiUser currentUser].userId]];
                self.upgradeView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([SPDUpGradeView class]) owner:self options:nil] lastObject];
                self.upgradeView.current_level = curr_level;
                self.upgradeView .delegate = self;
                self.upgradeSuperView.contentView = self.upgradeView;
                [self.upgradeSuperView show];
            }
            
            if (myCurrentLevel < 10 && curr_level >= 10) {
                [FBSDKAppEvents logEvent:@"tenLevel"];
            }
        }else{
            if([dict[@"curr_level"] integerValue] == 0){
                [[NSUserDefaults standardUserDefaults] setObject:@(1)  forKey:[NSString stringWithFormat:@"%@%@",MYCURRENTLEVEL,[SPDApiUser currentUser].userId]];
            }else{
                [[NSUserDefaults standardUserDefaults] setObject:dict[@"curr_level"]  forKey:[NSString stringWithFormat:@"%@%@",MYCURRENTLEVEL,[SPDApiUser currentUser].userId]];
            }
            
            if (curr_level >= 10) {
                [FBSDKAppEvents logEvent:@"tenLevel"];
            }
        }
    } bFailure:^(id  _Nullable failure) {
        
    }];
    
}


#warning  废弃
//送出接受礼物 座驾 用 来判断
- (void)checkExpWithGlodNum:(NSInteger )goldNum WithType:(SPDExpStyleType )expType {
    
    NSMutableDictionary * dict = [NSMutableDictionary new];
    [dict setObject:[SPDApiUser currentUser].userId forKey:@"user_id"];
    [RequestUtils commonGetUrlRequestUtils:dict bURL:@"userlevel" RequestType:SPDRankUrl bAnimated:NO bAnimatedViewController:nil bSuccessDoSomething:^(id  _Nullable suceess) {
        
        NSDictionary * dict = suceess[@"user_level"];
        NSInteger  curr_level = [dict[@"curr_level"] integerValue];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@%@",MYCURRENTLEVEL,[SPDApiUser currentUser].userId]]) {
            NSInteger myCurrentLevel = [[[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"%@%@",MYCURRENTLEVEL,[SPDApiUser currentUser].userId]] integerValue];
            if (myCurrentLevel < curr_level) {
                //给弹框
                [[NSUserDefaults standardUserDefaults] setObject:dict[@"curr_level"]  forKey:[NSString stringWithFormat:@"%@%@",MYCURRENTLEVEL,[SPDApiUser currentUser].userId]];
                self.upgradeView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([SPDUpGradeView class]) owner:self options:nil] lastObject];
                self.upgradeView.current_level = curr_level;
                self.upgradeView .delegate = self;
                self.upgradeSuperView.contentView = self.upgradeView;
                [self.upgradeSuperView show];
            }
            
            if (myCurrentLevel < 10 && curr_level >= 10) {
                [FBSDKAppEvents logEvent:@"tenLevel"];
            }
        }else{
            [[NSUserDefaults standardUserDefaults] setObject:dict[@"curr_level"]  forKey:[NSString stringWithFormat:@"%@%@",MYCURRENTLEVEL,[SPDApiUser currentUser].userId]];
            
            if (curr_level >= 10) {
                [FBSDKAppEvents logEvent:@"tenLevel"];
            }
        }
    } bFailure:^(id  _Nullable failure) {
        
    }];
    
}

- (NSInteger)getExpMultipleWithType:(SPDExpStyleType )expType andDict:(NSDictionary *)dict {
    NSInteger expMultiple = 1;
    switch (expType) {
        case ExpGiftType:{
            expMultiple = [dict[@"gift_exp_multiple"] integerValue];
            break;
        }
        case ExpVehicle:{
            expMultiple = [dict[@"vehicle_exp_multiple"] integerValue];
            break;
        }
        case EXpmsg_chatroom:{
            expMultiple = [dict[@"msg_chatroom_exp_multiple"] integerValue];
            break;
        }
            
        default:
            break;
    }
    return expMultiple;
    
}

+(NSString *)getArImageNameWithImage:(NSString *)name {
    NSString * imgName;
    if ([SPDCommonTool currentVersionIsArbic]) {
        imgName = [NSString stringWithFormat:@"%@_ar",name];
    }else{
        imgName = [NSString stringWithString:name];
    }
    return imgName;
}

-(FDAlertView *)upgradeSuperView{
    if (!_upgradeSuperView) {
        _upgradeSuperView = [[FDAlertView alloc] init];
    }
    return _upgradeSuperView;
}

+ (void)requestAccountPropertyAPIWithType:(NSString * )type
                         viewController  :(UIViewController *)vc
                         alertBindFailed:(SPDCommonToolAlertBind) failed{

        [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"account.property" bAnimated:NO bAnimatedViewController:nil bSuccessDoSomething:^(id  _Nullable suceess) {
            BOOL is_visitors = [suceess[@"is_visitors"] boolValue];
            if (is_visitors) {
                NSInteger register_day_num = [suceess[@"register_day_num"] integerValue] +1;
                if ([[NSUserDefaults standardUserDefaults]  boolForKey:SIGNUP_ONETAP([SPDApiUser currentUser].userId)]) {
                    //是一键注册的
                    if (register_day_num <= 3) {
                        //取点击的存储时候 与当前时候对比
                        NSInteger day =[[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"register_day_num%@",[SPDApiUser currentUser].userId]];
                        if (day==0 || day ==1 ) {
                            //等于的话 比对有没有点击
                            BOOL isClicked = [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"alertClick%@%@",type,[SPDApiUser currentUser].userId]];
                            if (isClicked) {
                                failed(false);
                            }else{
                                [self presentBindAlertController:vc andType:type];
                            }
                            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"alertClick%@%@",type,[SPDApiUser currentUser].userId]];
                            
                            [[NSUserDefaults standardUserDefaults] setInteger:register_day_num forKey:[NSString stringWithFormat:@"register_day_num%@",[SPDApiUser currentUser].userId]];
                            
                        }else{
                            //compare
                            NSInteger day=  [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"register_day_num%@",[SPDApiUser currentUser].userId]];
                            if (day !=register_day_num) {
                                //设置没有点击
                                [self presentBindAlertController:vc andType:type];
                                
                                [[NSUserDefaults standardUserDefaults] setBool:false forKey:[NSString stringWithFormat:@"alertClick%@%@",type,[SPDApiUser currentUser].userId]];
                                
                                [[NSUserDefaults standardUserDefaults] setInteger:register_day_num forKey:[NSString stringWithFormat:@"register_day_num%@",[SPDApiUser currentUser].userId]];
                            }else{
                                //等于的话 比对有没有点击
                                BOOL isClicked = [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"alertClick%@%@",type,[SPDApiUser currentUser].userId]];
                                if (isClicked) {
                                    failed(false);
                                }else{
                                    [SPDCommonTool presentBindAlertController:vc andType:type];
                                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"alertClick%@%@",type,[SPDApiUser currentUser].userId]];
                                    
                                    [[NSUserDefaults standardUserDefaults] setInteger:register_day_num forKey:[NSString stringWithFormat:@"register_day_num%@",[SPDApiUser currentUser].userId]];
                                }
                                
                                
                            }
                            //存储一下day
                            
                        }
                    }else {
                        failed(false);
                    }
                }else{
                    failed(false);
                }
            }else{
                failed(false);
            }
            
        } bFailure:^(id  _Nullable failure) {
    
        }];
}

+ (void)presentBindAlertController:(UIViewController *)vc andType:(NSString *)type {
    if ([type isEqualToString:@"personal"]) {
        [SPDCommonTool presentAutoController:vc title:@"" titleColor:@"" titleFontSize:0 messageString:SPDStringWithKey(@"为了保障您的账户安全，请绑定账号完成个人认证", nil) messageColor:nil messageFontSize:0 cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
            
        } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
            [SPDCommonTool pushToSecurityViewControllerWithController:vc];
        }];
    }else{
        [SPDCommonTool presentAutoController:vc title:@"" titleColor:@"" titleFontSize:0 messageString:SPDStringWithKey(@"进行账号绑定，开启无限畅聊", nil) messageColor:nil messageFontSize:0 cancelTitle:SPDStringWithKey(@"稍后", nil) cancelAction:^{
        } confirmTitle:SPDStringWithKey(@"好哒", nil) confirmAction:^{
            [SPDCommonTool pushToSecurityViewControllerWithController:vc];
        }];
    }
}

+ (void)pushToSecurityViewControllerWithController :(UIViewController *)vc {
    securityViewController * svc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"securityViewController"];
    [vc.navigationController pushViewController:svc animated:YES];
}

#pragma mark - upgradeview delegate

- (void)upgradeViewHiddenBtnClicked {
    [self.upgradeSuperView hide];
}
- (void)upgradeViewSureBtnClicked{
    [self.upgradeSuperView hide];
}

#pragma mark --
+(SPDPrivateMagicMessage *)getPrivateMagicMessageWithDict:(NSDictionary *)suceess andMagicModel:(SPDMagicModel *)magicModel andReceiveId:(NSString *)receive_id{
    //成功
    BOOL  is_buff ;
    if ([suceess[@"effect"] isEqualToString:@"inc"]) {
        is_buff = YES;
    }else{
        is_buff = FALSE;
    }

    NSString * cover = [NSString stringWithFormat:STATIC_DOMAIN_URL,magicModel.cover];
    SPDPrivateMagicMessage * magicMsg = [SPDPrivateMagicMessage privateMagicMessageWith:magicModel._id magic_name:magicModel.name magic_level:[NSString stringWithFormat:@"%@",magicModel.magic_level] magic_effect:suceess[@"effect"] magic_result:suceess[@"result"] power:is_buff receive_id:receive_id magic_cover:cover magic_type:suceess[@"type"] send_gender:[SPDApiUser currentUser].gender magic_effect_value:[NSString stringWithFormat:@"%@",suceess[@"value"]]] ;
    
    magicMsg.senderUserInfo = [RCIM sharedRCIM].currentUserInfo;
    
    if (isVideoStatus && isAlreadyDownloadMagicImagesEffect) {
        [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:@{@"user_id": receive_id}] bURL:@"user.detail" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable dataDict) {
            magicMsg.receive_avatar = dataDict[@"avatar"];
            magicMsg.receive_gender = dataDict[@"gender"];
            magicMsg.receive_nickname = dataDict[@"nick_name"];
            [[SPDRCIMDataSource shareInstance] showMagicAnimationWithMessage:magicMsg];
        } bFailure:^(id  _Nullable failure) {
            
        }];
    }
    
    return magicMsg;
}

+ (void)showWindowToast:(NSString *)text {
    [MBProgressHUD hideAllHUDsForView:[UIApplication sharedApplication].keyWindow animated:YES];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    hud.userInteractionEnabled = NO;
    hud.mode = MBProgressHUDModeText;
    hud.margin = 10.0f;
    hud.yOffset = 15.0f;
    hud.opacity = 0.5f;
    hud.removeFromSuperViewOnHide = YES;
    hud.detailsLabelText = text;
    hud.detailsLabelFont = [UIFont systemFontOfSize:15];
    [hud hide:YES afterDelay:2.5];
}

+ (UIViewController *)getWindowTopViewController {
    UIViewController *resultVC;
    resultVC = [SPDCommonTool _topViewController:[[UIApplication sharedApplication].keyWindow rootViewController]];
    while (resultVC.presentedViewController) {
        resultVC = [SPDCommonTool _topViewController:resultVC.presentedViewController];
    }
    return resultVC;
}

+(UIViewController *)_topViewController:(UIViewController *)vc {
    if ([vc isKindOfClass:[UINavigationController class]]) {
        return [self _topViewController:[(UINavigationController *)vc topViewController]];
    } else if ([vc isKindOfClass:[UITabBarController class]]) {
        return [self _topViewController:[(UITabBarController *)vc selectedViewController]];
    } else {
        return vc;
    }
    return nil;
}

// 请求动效图片
+ (void)requestAnimationImagesWithType:(NSString *)type {
    
    NSString * sandboxPath = [NSHomeDirectory()stringByAppendingPathComponent:[NSString stringWithFormat:@"/Library/Caches/%@", type]];
    
    NSInteger local_last_update = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"%@_lastupdate",type]];
    
//    NSLog(@"last_update = %ld", local_last_update);
    
    NSDictionary * dict = @{
                            @"type": type,
                            @"last_update": @(local_last_update)
                            };
    
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dict] bURL:@"effect.images" bAnimated:YES bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        if ([type isEqualToString:@"launch"]) {
            NSInteger last_update = [suceess[@"update_time"] integerValue];
            if (!last_update || local_last_update != last_update) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    
                    NSArray *fileNameArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:sandboxPath error:nil];
                    for (int i = 0 ;i < fileNameArray.count; i++) {
                        NSString *fileName = fileNameArray[i];
                        NSString *deletePath = [sandboxPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", fileName]];
                        BOOL is_delete = [[NSFileManager defaultManager] removeItemAtPath:deletePath error:nil];
                        if (is_delete) {
                            NSLog(@"删除%d个成功", i);
                        }
                    }
                    
                    NSArray *array = suceess[@"launch"];
                    BOOL finished = !array.count;
                    for (int i = 0; i < array.count; i++) {
                        NSString *imageUrl = array[i];
                        NSString *imageFilePath = [sandboxPath stringByAppendingPathComponent:imageUrl];
                        NSString *url = [NSString stringWithFormat:STATIC_DOMAIN_URL, imageUrl];
                        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
                        if ([data writeToFile:imageFilePath  atomically:YES]) {
                            finished = YES;
                            NSLog(@"%@第%d个图片写入成功！", type, i);
                        } else {
                            NSLog(@"第%d个图片写入失败！", i);
                        }
                    }
                    
                    if (finished) {
                        [[NSUserDefaults standardUserDefaults] setInteger:last_update forKey:@"launch_lastupdate"];
                    }
                });
            }
            
        } else {
            
            NSInteger last_update = [suceess[@"last_update"] integerValue];
            if (local_last_update == 0) {
                if ([type isEqualToString:@"gift"]) {
                    isAlreadyDownloadGiftImagesEffect = NO;
                } else if ([type isEqualToString:@"vehicle"]) {
                    isAlreadyDownloadVehicleImagesEffect = NO;
                } else {
                    isAlreadyDownloadMagicImagesEffect = NO;
                }
                dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_group_t group = dispatch_group_create();
                NSDictionary * prresent_Dict = suceess;
                NSArray * keyArray = [prresent_Dict allKeys];
                for (int i = 0; i< keyArray.count; i++) {
                    NSString * key = keyArray[i];
                    if ([key isEqualToString:@"last_update"]) {
                        [[NSUserDefaults standardUserDefaults] setInteger:[prresent_Dict[@"last_update"] integerValue ]forKey:[NSString stringWithFormat:@"%@_lastupdate",type]];
                        continue;
                    }else if ([key isEqualToString:@"update_time"]){
                        //容错
                    }else{
                        if (![prresent_Dict[key] isKindOfClass:[NSArray class]]) {return ;}
                        NSArray * array = prresent_Dict[key];
                        dispatch_group_async(group, queue, ^{
                            for (int i = 0; i< array.count; i++) {
                                //                        dispatch_group_async(group, queue, ^{
                                NSString * imageUrl = array[i];
                                NSString * url = [NSString stringWithFormat:STATIC_DOMAIN_URL,imageUrl];
                                NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
                                NSString *imageFilePath = [sandboxPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_%d.png",key,i]];
                                BOOL is_exit =  [[NSFileManager defaultManager] fileExistsAtPath:imageFilePath];
                                NSLog(@"current thread count %@",[NSThread currentThread].name);
                                if (is_exit) {
                                    NSLog(@"第二次进入会有重复下载了");
                                }else{
                                    if ([data writeToFile:imageFilePath  atomically:YES]) {
                                        NSLog(@"%@第%d个图片写入成功！",type,i);
                                    }else{
                                        NSLog(@"第%d个图片写入失败！",i);
                                    }
                                }
                                //                        });
                            }
                        });
                    }
                }
                dispatch_group_notify(group, queue, ^{
                    //判断是不是last_update
                    if ([type isEqualToString:@"gift"]) {
                        isAlreadyDownloadGiftImagesEffect = YES;
                    }else if ([type isEqualToString:@"vehicle"]){
                        isAlreadyDownloadVehicleImagesEffect = YES;
                    }else {
                        isAlreadyDownloadMagicImagesEffect = YES;
                    }
                    NSLog(@"图片写入完成～～～");
                });
                
            }else if(local_last_update < last_update){
                //有内容更新
                if ([type isEqualToString:@"gift"]) {
                    isAlreadyDownloadGiftImagesEffect = NO;
                }else if ([type isEqualToString:@"vehicle"]){
                    isAlreadyDownloadVehicleImagesEffect = NO;
                }else {
                    isAlreadyDownloadMagicImagesEffect = NO;
                }
                dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_group_t groupOne = dispatch_group_create();
                NSDictionary * prresent_Dict = suceess;
                NSArray * keyArray = [prresent_Dict allKeys];
                for (int i = 0; i< keyArray.count; i++) {
                    NSString * key = keyArray[i];
                    if ([key isEqualToString:@"last_update"]) {
                        [[NSUserDefaults standardUserDefaults] setInteger:[prresent_Dict[@"last_update"] integerValue ]forKey:[NSString stringWithFormat:@"%@_lastupdate",type]];
                        continue;
                    }else{
                        NSArray * array = prresent_Dict[key];
                        dispatch_group_async(groupOne, queue, ^{
                            
                            for (int i = 0; i< array.count; i++) {
                                //                        dispatch_group_async(groupOne, queue, ^{
                                NSString * imageUrl = array[i];
                                NSString * url = [NSString stringWithFormat:STATIC_DOMAIN_URL,imageUrl];
                                NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
                                NSString *imageFilePath = [sandboxPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_%d.png",key,i]];
                                BOOL is_exit =  [[NSFileManager defaultManager] fileExistsAtPath:imageFilePath];
                                if (is_exit) {
                                    NSLog(@"已经存在这个图片");
                                    //把所有的原先的图片都删除 - 适应数组长度作出改变
                                    NSArray * file_NameArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:sandboxPath error:nil];
                                    for (int i =0 ;i < file_NameArray.count ; i++) {
                                        NSString * name = file_NameArray[i];
                                        if ([name containsString:key]) {
                                            NSString * delete_str =[sandboxPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
                                            NSLog(@"delete path - %@",name);
                                            BOOL is_delete = [[NSFileManager defaultManager] removeItemAtPath:delete_str error:nil];
                                            if (is_delete) {
                                                NSLog(@"删除%@个成功",[NSString stringWithFormat:@"%d",i]);}
                                        }
                                    }
                                }
                                if ([data writeToFile:imageFilePath  atomically:YES]) {
                                    NSLog(@"图片写入成功！%@",[NSString stringWithFormat:@"%d",i]);
                                }else{
                                    NSLog(@"第%d个图片写入失败！",i);
                                }
                                //                        });
                            }
                        });
                        
                    }
                }//判断是不是last_update
                dispatch_group_notify(groupOne, queue, ^{
                    if ([type isEqualToString:@"gift"]) {
                        isAlreadyDownloadGiftImagesEffect = YES;
                    }else if ([type isEqualToString:@"vehicle"]){
                        isAlreadyDownloadVehicleImagesEffect = YES;
                    }else {
                        isAlreadyDownloadMagicImagesEffect = YES;
                    }
                    NSLog(@"图片写入完成～～～");
                });
                
            }else if(local_last_update >= last_update){
                //没有内容更新
                NSLog(@"没有内容更新");
                if ([type isEqualToString:@"gift"]) {
                    isAlreadyDownloadGiftImagesEffect = YES;
                }else if ([type isEqualToString:@"vehicle"]){
                    isAlreadyDownloadVehicleImagesEffect = YES;
                }else {
                    isAlreadyDownloadMagicImagesEffect = YES;
                }
            }
            
        }
        
    } bFailure:^(id  _Nullable failure) {
        NSLog(@"failure = %@",failure);
    }];
}

//获取地区
+ (NSString *)getStringFromCityWholeText:(NSString *)wholeText
{
    NSArray *array=[wholeText componentsSeparatedByString:@" "];
    if (array.count > 1) {
        NSString *lastString = array[1];
        return ([SPDCommonTool isEmpty:lastString] || [lastString isEqualToString:@""])?SPDStringWithKey(@"火星", nil) :SPDStringWithKey(lastString, nil);
    }else{
        return SPDStringWithKey(@"火星", nil);
    }
}

+ (UIImage *)getLanchImage {
    NSString *launchImagePath = [NSHomeDirectory() stringByAppendingPathComponent:@"/Library/Caches/launch"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:launchImagePath]) {
        NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:launchImagePath error:nil];
        if (files.count) {
            UIImage *image = [UIImage imageWithContentsOfFile:[launchImagePath stringByAppendingPathComponent:files[arc4random() % files.count]]];
            return image;
        }
    }
    return nil;
}


+ (NSString *)getInvitationCodeWithUserId:(NSString *)userId {
    if (userId.length >= 5) {
        return [NSString stringWithFormat:@"9%@", userId];
    } else {
        return [NSString stringWithFormat:@"%ld", 900000 + userId.integerValue];
    }
}

+(CGSize) getAttributeSizeWithText:(NSString *)text fontSize:(int)fontSize
{
    CGSize size=[text sizeWithAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:fontSize]}];
    NSAttributedString *attributeSting = [[NSAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:fontSize]}];
    size = [attributeSting size];
    return size;
}

+(void)loadWebViewController:(UIViewController *)vc title:(NSString *)title url:(NSString *)url {
    SPDHelpController *helpController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SPDHelpController"];
    helpController.url = url;
    helpController.titleText = title;
    [vc.navigationController pushViewController:helpController animated:YES];
}

+(void)pushToDetailTableViewController:(UIViewController *)viewController
                                userId:(NSString *)userId{
    LY_HomePageViewController * vc = [[LY_HomePageViewController alloc] initWithUserId:userId];
    [viewController.navigationController pushViewController:vc animated:YES];
}

+(NSString *)getDateStringWithDate:(NSDate *)date
                        DateFormat:(NSString *)formatString
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:formatString];
    NSString *dateString = [dateFormat stringFromDate:date];
    NSLog(@"date: %@", dateString);
    return dateString;
}

+ (NSMutableAttributedString *)AttributedString:(NSString *)content andImageNameArray:(NSMutableArray *)array{
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",content]];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#ffffff"] range:NSMakeRange(0, content.length)];
    for (int i =0 ; i< array.count;i++) {
        NSTextAttachment *attach = [[NSTextAttachment alloc] init];
        attach.image = [UIImage imageNamed:array[i]];
        attach.bounds = CGRectMake(0, -3, 12, 17);
        NSAttributedString *attachString = [NSAttributedString attributedStringWithAttachment:attach];
        [string insertAttributedString:attachString atIndex:string.length];
        
    }
    return string;
}


+ (void)requestFriendPostWithUserId:(NSString *)userId type:(NSString *)type success:(SPDRequestSuccessBlock)success failure:(SPDRequestFailureBlock)failure {
    if ([self isEmpty:type]) {
        return;
    }
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:type forKey:@"type"];
    if (![self isEmpty:userId]) {
        [dic setObject:userId forKey:@"to"];
    }
    [RequestUtils commonPostRequestUtils:dic bURL:@"friend" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        if (suceess) {
            success(suceess);
        }
    } bFailure:^(id  _Nullable error) {
        [self showWindowToast:error[@"msg"]];
        if (failure) {
            failure(error);
        }
    }];
}

+ (NSString *)transformIntegerToBriefStr:(NSInteger)integer {
    if (integer >= 1000000) {
        return [NSString stringWithFormat:@"%.1lfM", floor(integer / 1000000.0 * 10) / 10];
    } else if (integer >= 1000) {
        return [NSString stringWithFormat:@"%.1lfK", floor(integer / 1000.0 * 10) / 10];
    } else {
        return [NSString stringWithFormat:@"%ld", integer];
    }
}

+ (BOOL)getGlobalMessageSwitch {
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"GlobalMessageSwitch"];
}

+ (void)setGlobalMessageSwitch:(BOOL)on {
    [[NSUserDefaults standardUserDefaults] setBool:on forKey:@"GlobalMessageSwitch"];
}

+ (UIImage *)createImageWithColor:(UIColor *)color size:(CGSize)size {
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (NSMutableArray *)allRangesOfString:(NSString *)searchString withinString:(NSString *)targetString {
    NSMutableArray *rangeArray = [NSMutableArray array];
    NSRange targetRange = NSMakeRange(0, targetString.length);
    while (YES) {
        NSRange range = [targetString rangeOfString:searchString options:NSLiteralSearch range:targetRange];
        if (range.location != NSNotFound) {
            [rangeArray addObject:[NSValue valueWithRange:range]];
            targetRange = NSMakeRange(range.location + 1, targetString.length - (range.location + 1));
        } else {
            break;
        }
    }
    return rangeArray;
}

+ (void)presentAlertWithTitle:(nullable NSString *)title
                      message:(nullable NSString *)message
                  cancelTitle:(nullable NSString *)cancelTitle
                cancelHandler:(void (^ __nullable)(UIAlertAction *action))cancelHandler
                  actionTitle:(nullable NSString *)actionTitle
                actionHandler:(void (^ __nullable)(UIAlertAction *action))actionHandler {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:SPDStringWithKey(title, nil) message:SPDStringWithKey(message, nil) preferredStyle:UIAlertControllerStyleAlert];
    if (cancelTitle.notEmpty) {
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:SPDStringWithKey(cancelTitle, nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if (cancelHandler) {
                cancelHandler(action);
            }
        }];
        [alertController addAction:cancelAction];
    }
    if (actionTitle.notEmpty) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:SPDStringWithKey(actionTitle, nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if (actionHandler) {
                actionHandler(action);
            }
        }];
        [alertController addAction:action];
    }
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
}

+ (NSString *)orderTimeStingWithTimeStamp:(long long)timeStamp {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.dateFormat = @"yyyy-MM-dd HH:mm";
    dateFormat.timeZone = [NSTimeZone localTimeZone];
    
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:timeStamp];
    NSString *orderTime = [dateFormat stringFromDate:date];
    
    NSDate *todayDate = [NSDate date];
    NSDate *tomorrowDate = [todayDate dateByAddingTimeInterval:24 * 60 * 60];
    NSDate *theDayAfterTomorrowDate = [todayDate dateByAddingTimeInterval:24 * 60 * 60 * 2];
    
    NSString *todayString = [[dateFormat stringFromDate:todayDate] substringToIndex:10];
    NSString *tomorrowString = [[dateFormat stringFromDate:tomorrowDate] substringToIndex:10];
    NSString *theDayAfterTomorrowString = [[dateFormat stringFromDate:theDayAfterTomorrowDate] substringToIndex:10];
    
    NSString *dateString = [orderTime substringToIndex:10];
    if ([dateString isEqualToString:todayString]) {
        return [NSString stringWithFormat:@"%@%@", SPDStringWithKey(@"今天", nil), [orderTime substringFromIndex:10]];
    } else if ([dateString isEqualToString:theDayAfterTomorrowString]) {
        return [NSString stringWithFormat:@"%@%@", SPDStringWithKey(@"明天", nil), [orderTime substringFromIndex:10]];
    } else if ([dateString isEqualToString:tomorrowString])  {
        return [NSString stringWithFormat:@"%@%@", SPDStringWithKey(@"后天", nil), [orderTime substringFromIndex:10]];
    } else {
        return orderTime;
    }
}

+ (NSString *)getNobleNameColor:(NSString *)noble {
    NSDictionary * nobleColorDict = @{
        @"baron": @"8d9ac3",
        @"viscount": @"90b36b",
        @"count": @"61b3f9",
        @"marquis": @"a86df0",
        @"duke": @"24c2d0",
        @"king": @"fed467",
        @"emperor": @"eab971"
    };
    return nobleColorDict[noble]?:@"#eab971";
}


@end
