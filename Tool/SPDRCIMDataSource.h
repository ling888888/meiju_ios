//
//  SPDRCIMDataSource.h
//  SimpleDate
//
//  Created by 侯玲 on 17/1/4.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RongCallKit/RongCallKit.h>
#import <RongIMKit/RongIMKit.h>
#import "SPDPrivateMagicMessage.h"

/*!
 @const 收到消息的Notification
 
 @discussion 接收到消息后，SDK会分发此通知。
 
 Notification的object为RCMessage消息对象。
 userInfo为NSDictionary对象，其中key值为@"left"，value为还剩余未接收的消息数的NSNumber对象。
 */
FOUNDATION_EXPORT NSString *const SPDLiveKitChatRoomMessageNotification;
FOUNDATION_EXPORT NSString *const SPDLiveCallWillConnect;
FOUNDATION_EXPORT NSString *const SPDLiveCallWillDisConnect;
FOUNDATION_EXPORT NSString *const SPDLivedidJYReceiveCall;
FOUNDATION_EXPORT NSString *const SPDLiveKitChatRoomGlobalNotification;
@interface SPDRCIMDataSource : NSObject<RCIMUserInfoDataSource,RCIMReceiveMessageDelegate,JYCallSessionDelegate,RCIMReceiveMessageDelegate>

@property (nonatomic, strong) RCMessageContent *lastGlobalMessage;
@property (nonatomic, assign) NSInteger heartbeatInterval;

+(SPDRCIMDataSource *)shareInstance;

- (void)logIn; // 登录im

- (void)showMagicAnimationWithMessage:(SPDPrivateMagicMessage *)magicMessage;

@end
