//
//  SPDHashGenerator.h
//  SimpleDate
//
//  Created by Leoc O'Cear on 1/28/16.
//  Copyright © 2016 Jianyue. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>

@interface SPDHashGenerator : NSObject

+ (NSString*)md5:(NSString*)str;
+ (NSString*)sha1:(NSString*)str;

@end
