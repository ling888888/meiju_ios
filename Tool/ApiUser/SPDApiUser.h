//
//  SPDApiUser.h
//  SimpleDate
//
//  Created by Leoc O'Cear on 2/2/16.
//  Copyright © 2016 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//#define USER_DATA_IDENTIFIER (@"self.userId")

@interface SPDApiUser : NSObject

@property (atomic, strong) NSString* userId;
@property (atomic, strong) NSString* ryToken;
@property (atomic, strong) NSString* nickName;
@property (atomic,strong)  NSString* gender;
@property (atomic, strong) NSString* avatar;
@property (atomic, strong) NSString* headwearWebp;
@property (atomic, strong) NSNumber* if_special;
@property (atomic,strong)  NSNumber * age;
@property (atomic,strong)  NSString * lang; //语言区

@property (nonatomic,assign)  NSInteger gift_lastupdate_time;
@property (nonatomic,assign)  NSInteger vehicle_lastupdate_time;


+ (SPDApiUser*)currentUser;

/**
 *  init current user with local default data
 *
 *  @return True if data exists else False
 */
- (BOOL)initWithDefaultsData;

/**
 *  save user info from dict
 *
 *  @param userInfo   user info dict
 */
- (void)saveUserInfoToDefault:(NSDictionary*)userInfo;

- (void)saveToDefault;

/**
 *  check if login
 */
- (BOOL)isLoginToLocal;
/**
 *  logOut
 */
//- (void)logOut;

@end
