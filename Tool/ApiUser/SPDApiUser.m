//
//  SPDApiUser.m
//  SimpleDate
//
//  Created by Leoc O'Cear on 2/2/16.
//  Copyright © 2016 WYB. All rights reserved.
//

#import "SPDApiUser.h"

//static NSString *const kUserDataIdentifier = @"self.userId";

@implementation SPDApiUser

+ (SPDApiUser*)currentUser {
    static SPDApiUser* shareInstance = nil;
    static dispatch_once_t onePredicate;
    dispatch_once(&onePredicate, ^{
        shareInstance = [[self alloc] init];
        [shareInstance initWithDefaultsData];
    });
    return shareInstance;
}

- (BOOL)initWithDefaultsData {
    NSDictionary* userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDataIdentifier];
    if (userInfo == nil) {
        return NO;
    }
//    NSLog(@"jianyue---%@--",userInfo);
    self.userId   = userInfo[@"_id"];
    self.ryToken  = userInfo[@"ry_token"];
    self.nickName = userInfo[@"nick_name"];
    self.avatar   = userInfo[@"avatar"];
    self.headwearWebp = userInfo[@"headwearWebp"];
    self.if_special= userInfo[@"if_special"];
    self.gender = userInfo[@"gender"];
    self.age   = userInfo[@"age"];
    self.lang = userInfo[@"lang"];
    return YES;
}

- (void)saveUserInfoToDefault:(NSDictionary*)userInfo {
    
    self.userId   = userInfo[@"_id"];
    self.ryToken  = userInfo[@"ry_token"];
    self.nickName = userInfo[@"nick_name"];
    self.avatar   = userInfo[@"avatar"];
    self.headwearWebp = userInfo[@"headwearWebp"];
    self.if_special = userInfo[@"if_special"];
    self.gender = userInfo [@"gender"];
    self.age = userInfo[@"age"];
    self.lang = userInfo[@"lang"];
    [[NSUserDefaults standardUserDefaults] setObject:userInfo forKey:kUserDataIdentifier];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)saveToDefault {
    
    NSDictionary* userInfo=[NSDictionary new];
    NSString* gender = self.gender == nil? @"male":self.gender;
    NSNumber* if_special = self.if_special == nil? @(0):self.if_special;
    NSNumber* age = self.age == nil?@(20):self.age;
    userInfo = @{
                 @"_id":       self.userId,
                 @"ry_token"  :self.ryToken,
                 @"nick_name" :self.nickName,
                 @"avatar"    :self.avatar,
                 @"headwearWebp" :self.headwearWebp?:@"", // fix crash
                 @"if_special":if_special,
                 @"gender"    :gender,
                 @"age"       :age,
                 @"lang"      :self.lang
                 };
    
    [[NSUserDefaults standardUserDefaults] setObject:userInfo forKey:kUserDataIdentifier];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)isLoginToLocal {
    
    NSDictionary* userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDataIdentifier];
    if (userInfo == nil) {
         NSArray* cookieStorage = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:[NSURL URLWithString:SERVER_URL]];
        if (cookieStorage) {
            for (NSHTTPCookie* cookie in cookieStorage) {
                [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
            }
        }
    }
    if (userInfo) {
        return YES;
    }
    return NO;
}


//- (void)logOut {
//    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kUserDataIdentifier];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}

@end
