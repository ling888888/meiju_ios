//
//  SPDUMShareUtils.m
//  SimpleDate
//
//  Created by 侯玲 on 17/4/16.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDUMShareUtils.h"
//#import <UMSocialCore/UMSocialCore.h>
//#import <UMSocialCore/UMSocialManager.h>
//#import "UMCCommon/u"
#import "AppManager.h"

@interface SPDUMShareUtils ()

@property(nonatomic,copy)NSString *shareType; // ["wc_session","wc_timeline","qq","azone","facebook","twitter","sina"]
@property(nonatomic,copy)NSString *shareSource; //分享的来源 video ....
@property(nonatomic,copy)NSString *shareUrl; //分享的链接.


@end

@implementation SPDUMShareUtils


+(instancetype)shareInstance {
    static SPDUMShareUtils * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc]init];
    });
    return instance;
}

////分享的网页
//-(void)shareToPlatform:(SPDUMShareType )platform WithShareUrl:(NSString *)shareUrl andshareTitle:(NSString *)shareTitle andshareContent:(NSString *)shareContent andImageData:(UIImage *)image
//      andShareSourece :(NSString *)shareSource
//    andSPDUMShareSuccess:(SPDUMShareSuccess) shareSuccess
//    presentedController:(BaseViewController *)presentedController andSPDUMShareFailure:(SPDUMShareFailure) shareFailure {
//
//    if ([DBUtil queryShareLimit] >= 5) {
//        [SPDCommonTool showWindowToast:SPDStringWithKey(@"今日分享次数已用完，明天再来分享吧", nil)];
//        return;
//    }
//
//    self.shareSource = shareSource;
//    self.shareUrl = shareUrl;
//
//    UMSocialPlatformType  share_Platform ;
//    if (platform == SPDShareToWechatSession ) {
//
//        share_Platform = UMSocialPlatformType_WechatSession;
//        self.shareType = @"wc_session";
//
//    }else if (platform == SPDShareToWechatTimeline){
//
//        share_Platform = UMSocialPlatformType_WechatTimeLine;
//        self.shareType = @"wc_timeline";
//
//    }else if (platform == SPDShareToQQ){
//        share_Platform = UMSocialPlatformType_FaceBookMessenger;
//        self.shareType = @"qq";
//
//    }else if (platform == SPDShareToQQZone){
//        share_Platform = UMSocialPlatformType_Qzone;
//        self.shareType = @"qzone";
//
//    }else if (platform == SPDShareToFacebook){
//        share_Platform = UMSocialPlatformType_Facebook;
//        self.shareType = @"facebook";
//        [self shareToFacebookWithViewController:presentedController];
//        return;
//    }else if (platform == SPDShareToTwitter){
//        share_Platform = UMSocialPlatformType_Twitter;
//        self.shareType = @"twitter";
//    }
//    else {
//        share_Platform = UMSocialPlatformType_Sina;
//        self.shareType = @"sina";
//    }
//    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
//    //创建网页内容对象
//    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:shareTitle descr:shareContent thumImage:image];
//    //设置网页地址
//    shareObject.webpageUrl = shareUrl;
//
//    //分享消息对象设置分享内容对象
//    messageObject.shareObject = shareObject;
//
//    [[UMSocialManager defaultManager] shareToPlatform:share_Platform messageObject:messageObject currentViewController:nil completion:^(id data, NSError *error) {
//
//        if (error) {
//            UMSocialLogInfo(@"************Share fail with error %@*********",error);
//            if (shareFailure) {
//                shareFailure(error);
//            }
//        }else{
//            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
//                UMSocialShareResponse *resp = data;
//                //分享结果消息
//                UMSocialLogInfo(@"response message is %@",resp.message);
//                //第三方原始返回的数据
//                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
//                if (shareSuccess) {
//                    shareSuccess(resp);
//                    //post
//                    [self postShareDataWithType:self.shareType andSource:self.shareSource];
//                    //check level
//                    [[SPDCommonTool shareTool] checkExpToWhetherToUpgrage];
//                }
//
//            }else{
//                UMSocialLogInfo(@"response data is %@",data);
//                if (shareSuccess) {
//                    shareSuccess(data);
//                }
//            }
//
//            NSInteger shareLimit = [DBUtil queryShareLimit];
//            [DBUtil recordShareLimit:shareLimit + 1];
//        }
//    }];
//}
//
//- (void)shareToFacebookWithViewController:(BaseViewController *)baseViewController {
//
//    FBSDKShareLinkContent * linkContent = [[FBSDKShareLinkContent alloc] init];
//    linkContent.contentURL = [NSURL URLWithString:self.shareUrl];
//    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
//    dialog.fromViewController = baseViewController;
//    dialog.shareContent = linkContent;
//    dialog.delegate = self;
//    dialog.mode = FBSDKShareDialogModeNative;
//    [dialog show];
//
//}
//
////type 	分享渠道,
////source 分享页面来源
//- (void)postShareDataWithType:(NSString *)type andSource:(NSString *)source {
//
//    NSMutableDictionary * dict = [NSMutableDictionary new];
//    [dict setObject:type forKey:@"type"];
//    [dict setObject:source forKey:@"source"];
//    [dict setObject:[[AppManager sharedInstance] getDeviceId] forKey:@"udid"];
//
//    [RequestUtils commonPostRequestUtils:dict bURL:@"share" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
//
//    } bFailure:^(id  _Nullable failure) {
//
//    }];
//}
//
//
//
//
//+ (void)getUserInfoWithPlatform:(SPDUMShareType)platform andSPDUMShareSuccess:(SPDUMShareSuccess) shareSuccess andSPDUMShareFailure:(SPDUMShareFailure) shareFailure  {
//
//    UMSocialPlatformType  share_Platform ;
//    if (platform == SPDShareToWechatSession ) {
//        share_Platform = UMSocialPlatformType_WechatSession;
//    }else if (platform == SPDShareToWechatTimeline){
//        share_Platform = UMSocialPlatformType_WechatTimeLine;
//    }else if (platform == SPDShareToQQ){
//        share_Platform = UMSocialPlatformType_QQ;
//    }else if (platform == SPDShareToQQZone){
//        share_Platform = UMSocialPlatformType_Qzone;
//    }else if (platform == SPDShareToFacebook){
//        share_Platform = UMSocialPlatformType_Facebook;
//    }else if(platform == SPDShareToSina){
//        share_Platform = UMSocialPlatformType_Sina;
//    }else if (platform == SPDShareToTwitter){
//        share_Platform = UMSocialPlatformType_Twitter;
//    }else {
//        share_Platform = UMSocialPlatformType_Sina;
//    }
//
//    [[UMSocialManager defaultManager] getUserInfoWithPlatform:share_Platform currentViewController:nil completion:^(id result, NSError *error) {
//        if (error) {
//            if (shareFailure) {
//                shareFailure(error);
//            }
//        } else {
//            UMSocialUserInfoResponse *resp = result;
//            if (shareSuccess) {
//                shareSuccess(resp);
//            }
//        }
//    }];
//}
//
//#pragma mark - facebook share delegate
//- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results {
//
//    [SPDCommonTool showWindowToast:SPDStringWithKey(@"分享成功", nil)];
//    [self postShareDataWithType:self.shareType andSource:self.shareSource];
//
//    NSInteger shareLimit = [DBUtil queryShareLimit];
//    [DBUtil recordShareLimit:shareLimit + 1];
//}
//
///**
// Sent to the delegate when the sharer encounters an error.
// - Parameter sharer: The FBSDKSharing that completed.
// - Parameter error: The error.
// */
//- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error {
//
//}
//
///**
// Sent to the delegate when the sharer is cancelled.
// - Parameter sharer: The FBSDKSharing that completed.
// */
//- (void)sharerDidCancel:(id<FBSDKSharing>)sharer {
//
//}

@end
