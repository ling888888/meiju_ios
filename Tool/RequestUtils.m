//
//  RequestUtils.m
//  SimpleDate
//
//  Created by xu.juvenile on 2016/3/3.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "PrefixHeader.pch"
#import "RequestUtils.h"
#import "SPDApiUser.h"
#import "AppManager.h"
#import "NSString+XXWAddition.h"

static AFHTTPSessionManager *sessionManager;
//全局错误
static const NSInteger SUCCESSCODE = 0;
static const NSInteger PARAMETER_ERROR = 1;
static const NSInteger LOGININVALID_ERROR = 2;
static const NSInteger LOWVERSION_ERROR = 3;   //客户端版本过低
static const NSInteger SERVER_MAINTENANCE = 4; //服务器正在维护，请稍后重试
static const NSInteger SERVER_BUG = 5;         //	系统错误，请稍候重试
static const NSInteger BANNED = 7;         //黑名單鎖定

static  BOOL  networkError =NO;

@implementation RequestUtils

+ (NSString *)getRequestURL:(NSString *)url
{
    return [NSString stringWithFormat:@"%@?_v=%@", URLStringWithPath(url), API_VERSION];
}

+ (NSString *)getRelationURL:(NSString *)url {
    return [NSString stringWithFormat:@"%@?_v=%@", URLStringWithRelation(url), API_VERSION];
}

+ (NSString *)getRankURL:(NSString *)url
{
    return [NSString stringWithFormat:@"%@?_v=%@", URLStringWithRank(url), API_VERSION];
}

+ (NSString *)getClanURL:(NSString *)url
{
    return [NSString stringWithFormat:@"%@?_v=%@", URLStringWithClan(url), API_VERSION];
}

+(NSString *)getCommonUrl:(NSString *)url {
    return [NSString stringWithFormat:@"%@?_v=%@", URLStringWithCommon(url), API_VERSION];}

+(AFHTTPSessionManager *) setupManager{
    if (!sessionManager) {
        sessionManager = [AFHTTPSessionManager manager];
        sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"image/png", @"application/x-www-form-urlencoded", @"text/html", nil];
        sessionManager.requestSerializer.HTTPMethodsEncodingParametersInURI = [NSSet setWithObjects:@"GET", @"HEAD", nil];
        sessionManager.securityPolicy.allowInvalidCertificates = YES;
        sessionManager.securityPolicy = [AFSecurityPolicy defaultPolicy];
        NSString *userAgentName = USER_AGENT_NAME;
        [sessionManager.requestSerializer setValue:userAgentName forHTTPHeaderField:@"User-Agent"];
        //deal code = 4 之后请求某个api 一直code = 4
        [sessionManager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    }
    return sessionManager;
}

+ (NSMutableDictionary *)commonRequestParameter
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    return params;
}

+ (void)handleError:(NSError *)error
{
//    [self alertViewWithMessage:SPDStringWithKey(@"网络连接错误，请稍后重试。", nil)];
}
+ (void)handleError404:(NSError *)error
{
    [SPDCommonTool showWindowToast:SPDStringWithKey(@"此功能暂未开放", nil)];
    
}
#pragma - mark - 共通處理的請求

+ (void)setRequestHeaderLanguage:(AFHTTPSessionManager *)manager {
    if ([SPDCommonTool getFamyLanguage]) {
        NSString * str = [SPDCommonTool getFamyLanguage];
        NSArray * array = [str componentsSeparatedByString:@"-"];
        if (array.count >0 ) {
            [manager.requestSerializer setValue:array[0] forHTTPHeaderField:@"Accept-Language"];
        }
    }
}

+ (NSURLSessionDataTask *)POST:(NSString *)URL
                    parameters:(NSDictionary *)parameters
                       success:(XXWResponseSuccess)success
                       failure:(XXWResponseFailure)failure {
    
    AFHTTPSessionManager *manager = [self setupManager];
    NSString *cookie = ([[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"]);
    if (cookie.notEmpty) {
        [manager.requestSerializer setValue:cookie forHTTPHeaderField:@"Cookie"];
    }
    
    NSMutableDictionary *params = [parameters mutableCopy];
    [params setValue:[[AppManager sharedInstance] getDeviceId] forKey:@"UDID"];
    NSURLSessionDataTask *task = [manager POST:URL parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        NSInteger code = [responseObject[@"code"] integerValue];
        if (code == SUCCESSCODE) {
            success(responseObject[@"data"]);
            networkError = NO;
        } else {
            if (failure) {
                failure(responseObject);
            }
            if (code > SUCCESSCODE && code <= BANNED) {
                [self handleResponse:responseObject presentAlertController:nil bURL:URL];
            }
        }
    } failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
        if (error.code != NSURLErrorCancelled) {
            [RequestUtils handleError:error];
        }
        if (failure) {
            failure(@{@"msg": error.localizedDescription});
        }
    }];
    return task;
}

+ (NSURLSessionDataTask *)GET:(NSString *)URL
                   parameters:(NSDictionary *)parameters
                      success:(XXWResponseSuccess)success
                      failure:(XXWResponseFailure)failure {
    
    AFHTTPSessionManager *manager = [self setupManager];
    [self setRequestHeaderLanguage:manager];
    NSString *cookie = ([[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"]);
    if (cookie.notEmpty) {
        [manager.requestSerializer setValue:cookie forHTTPHeaderField:@"Cookie"];
    }
    NSMutableDictionary *params = [parameters mutableCopy];
    [params setValue:[[AppManager sharedInstance] getDeviceId] forKey:@"UDID"];
    NSURLSessionDataTask *task = [manager GET:URL parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        NSInteger code = [responseObject[@"code"] integerValue];
        if (code == SUCCESSCODE) {
            success(responseObject[@"data"]);
            networkError = NO;
        } else {
            if (failure) {
                failure(responseObject);
            }
            if (code > SUCCESSCODE && code <= BANNED) {
                [self handleResponse:responseObject presentAlertController:nil bURL:URL];
            }
        }
    } failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
        if (error.code != NSURLErrorCancelled) {
            [RequestUtils handleError:error];
        }
        if (failure) {
            failure(@{@"msg": error.localizedDescription});
        }
    }];
    return task;
}

+ (NSURLSessionDataTask *)PostMultipartFormDataUtils:(NSMutableDictionary *)params
                                    bURL:(NSString *)bURL
               constructingBodyWithBlock:(XXWConstructingBody)constructingBody
                                 success:(XXWResponseSuccess)success
                                 failure:(nullable XXWResponseFailure)failure
{
    [params setObject:[[AppManager sharedInstance] getDeviceId] forKey:@"UDID"];
    AFHTTPSessionManager * manager = [self setupManager];
    [self setRequestHeaderLanguage:manager];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"]) {
         NSString * cookie_strings = [[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"];
        [manager.requestSerializer setValue:cookie_strings forHTTPHeaderField:@"Cookie"];
    }
    NSURLSessionDataTask *task =  [manager POST:bURL parameters:params headers:nil constructingBodyWithBlock:constructingBody progress:^(NSProgress *_Nonnull uploadProgress) {
        NSLog(@"%@", uploadProgress);
    }success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
    
        NSInteger code = [responseObject[@"code"] integerValue];
        if (code == SUCCESSCODE) {
            success(responseObject[@"data"]);
            networkError = NO;
        } else {
            if (failure) {
                failure(responseObject);
            }
            if (code > SUCCESSCODE && code <= BANNED) {
                [self handleResponse:responseObject presentAlertController:nil bURL:bURL];
            }
        }
        
    } failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
        [RequestUtils handleError:error];
        if (failure) {
            failure(@{@"msg": error.localizedDescription});
        }
    }];
    return task;
}


+ (void)commonPostRequestUtils:(NSMutableDictionary *)params
                          bURL:(NSString *)bURL
                     bAnimated:(BOOL)bAnimated
       bAnimatedViewController:(UIViewController *)viewController
           bSuccessDoSomething:(XXWResponseSuccess)bSuccess
                      bFailure:(nullable XXWResponseFailure)bFailure
{
    
    if (bAnimated) {
        [MBProgressHUD showHUDAddedTo:viewController.view animated:YES];
    }
    AFHTTPSessionManager * manager = [self setupManager];
    [self setRequestHeaderLanguage:manager];
    //判断所有登录的都是 不带这个cookie 的
    if ([bURL isEqualToString:@"login"] ||[bURL isEqualToString:@"signup/onetap"]|| [bURL isEqualToString:@"signup/onetap.pre"]||[bURL isEqualToString:@"platform.login"]) {
        
        [manager.requestSerializer setValue:nil forHTTPHeaderField:@"Cookie"];
        
    }else{
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"]) {
            NSString * cookie_strings =[[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"];
            [manager.requestSerializer setValue:cookie_strings forHTTPHeaderField:@"Cookie"];
        }
    }
    
    bURL = [self getRequestURL:bURL];
    [manager POST:bURL parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        
        NSHTTPURLResponse * response = (NSHTTPURLResponse *)task.response;
        NSString * set_cookies = response.allHeaderFields[@"Set-Cookie"];
        if ([set_cookies containsString:@"server_token"]) {
            [[NSUserDefaults standardUserDefaults] setObject:set_cookies forKey:@"Set-Cookie"];
        }else{
            NSLog(@"verify url -- %@",bURL);
        }
        
      if (bAnimated) {
          [MBProgressHUD hideHUDForView:viewController.view animated:YES];
          MBProgressHUD *HUD = [MBProgressHUD HUDForView:viewController.view];
          if (HUD != nil) {
              [HUD removeFromSuperview];
              HUD = nil;
          }
      }

      if ([responseObject[@"code"] integerValue] != SUCCESSCODE) {
          if ([responseObject[@"code"] integerValue] > SUCCESSCODE && [responseObject[@"code"] integerValue] <= BANNED) {
              //处理全局错误变量 1-5,7
              [RequestUtils handleResponse:responseObject presentAlertController:viewController bURL:bURL];
          }
          if (bFailure) {
              bFailure(responseObject);
          }
      }
      else {
          bSuccess(responseObject[@"data"]);

          networkError =NO;

      }

    } failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
        if (bAnimated) {
            [MBProgressHUD hideHUDForView:viewController.view animated:YES];
        }
        [RequestUtils handleError:error];
        if (bFailure) {
            bFailure(@{@"msg": error.localizedDescription});
        }
    }];
}

+(void)SpecialGetRequestUtils:(NSMutableDictionary *)params
                          bURL:(NSString *)bURL
                     bAnimated:(BOOL)bAnimated
       bAnimatedViewController:(UIViewController *)viewController
           bSuccessDoSomething:(XXWResponseSuccess)bSuccess
                      bFailure:(nullable XXWResponseFailure)bFailure
{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"image/png", @"application/x-www-form-urlencoded", @"text/html",@"text/plain", nil];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.securityPolicy = [AFSecurityPolicy defaultPolicy];

    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"]) {
        NSString * cookie_strings =[[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"];
        [manager.requestSerializer setValue:cookie_strings forHTTPHeaderField:@"Cookie"];
    }
    [manager GET:bURL parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        
        
        NSHTTPURLResponse * response = (NSHTTPURLResponse *)task.response;
        NSString * set_cookies = response.allHeaderFields[@"Set-Cookie"];
        [[NSUserDefaults standardUserDefaults] setObject:set_cookies forKey:@"Set-Cookie"];
        
            bSuccess(responseObject);
            networkError =NO;

        
    } failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
        if (bFailure) {
            bFailure(@{@"msg": error.localizedDescription});
        }
    }];
}


+(void)SpecialPostRequestUtils:(NSMutableDictionary *)params
                          bURL:(NSString *)bURL
                     bAnimated:(BOOL)bAnimated
       bAnimatedViewController:(UIViewController *)viewController
           bSuccessDoSomething:(XXWResponseSuccess)bSuccess
                      bFailure:(nullable XXWResponseFailure)bFailure
{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"image/png", @"application/x-www-form-urlencoded", @"text/html", nil];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.securityPolicy = [AFSecurityPolicy defaultPolicy];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"]) {
        NSString * cookie_strings =[[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"];
        [manager.requestSerializer setValue:cookie_strings forHTTPHeaderField:@"Cookie"];
    }
    [manager POST:bURL parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        
        
        NSHTTPURLResponse * response = (NSHTTPURLResponse *)task.response;
        NSString * set_cookies = response.allHeaderFields[@"Set-Cookie"];
        [[NSUserDefaults standardUserDefaults] setObject:set_cookies forKey:@"Set-Cookie"];

        
        if ([responseObject[@"code"] integerValue] == 200 ) {
            
            bSuccess(responseObject);
            networkError =NO;
        }
    
    } failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
        if (bFailure) {
            bFailure(@{@"msg": error.localizedDescription});
        }
    }];
}

//有type

+ (void)commonPostUrlRequestUtils:(NSMutableDictionary *)params
                            bURL:(NSString *)bURL
                     RequestType:(SPDUrlRequestType) requestType
                       bAnimated:(BOOL)bAnimated
         bAnimatedViewController:(UIViewController *)viewController
             bSuccessDoSomething:(XXWResponseSuccess)bSuccess
                         bFailure:(nullable XXWResponseFailure)bFailure{
    if (bAnimated) {
        [MBProgressHUD showHUDAddedTo:viewController.view animated:YES];
    }
    if (requestType ==SPDAPIUrl ) {
        bURL = [self getRequestURL:bURL];
    }else if (requestType == SPDClanUrl){
        bURL=[self getClanURL:bURL];
    }else if (requestType == SPDRankUrl){
        bURL=[self getRankURL:bURL];
    }else if (requestType == SPDRelationUrl){
        bURL = [self getRelationURL:bURL];
    }
    else{
        bURL=[self getCommonUrl:bURL];
    }
    AFHTTPSessionManager * manager = [self setupManager];
    [self setRequestHeaderLanguage:manager];

    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"]) {
        NSString * cookie_strings =[[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"];
        [manager.requestSerializer setValue:cookie_strings forHTTPHeaderField:@"Cookie"];
    }
    [manager POST:bURL parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        
        NSHTTPURLResponse * response = (NSHTTPURLResponse *)task.response;
        NSString * set_cookies = response.allHeaderFields[@"Set-Cookie"];
        [[NSUserDefaults standardUserDefaults] setObject:set_cookies forKey:@"Set-Cookie"];
        
        if (bAnimated) {
            [MBProgressHUD hideHUDForView:viewController.view animated:YES];
            
            MBProgressHUD *HUD = [MBProgressHUD HUDForView:viewController.view];
            if (HUD != nil) {
                [HUD removeFromSuperview];
                HUD = nil;
            }
        }
        
        if ([responseObject[@"code"] integerValue] != SUCCESSCODE) {
            if (bFailure) {
                bFailure(responseObject);
            }
            
            if ([responseObject[@"code"] integerValue] > SUCCESSCODE && [responseObject[@"code"] integerValue] <= BANNED) {
                //处理全局错误变量 1-5,7
                [RequestUtils handleResponse:responseObject presentAlertController:viewController bURL:bURL];
                
            }
        }
        else {
            bSuccess(responseObject[@"data"]);
            networkError =NO;
        }
        
    } failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
        if (bAnimated) {
            [MBProgressHUD hideHUDForView:viewController.view animated:YES];
        }
        [RequestUtils handleError:error];
        if (bFailure) {
            bFailure(@{@"msg": error.localizedDescription});
        }
    }];
}

+ (void)commonGetUrlRequestUtils:(NSMutableDictionary *)params
                            bURL:(NSString *)bURL
                     RequestType:(SPDUrlRequestType) requestType
                       bAnimated:(BOOL)bAnimated
         bAnimatedViewController:(UIViewController *)viewController
             bSuccessDoSomething:(XXWResponseSuccess)bSuccess
                        bFailure:(nullable XXWResponseFailure)bFailure
{
    
    if (bAnimated) {
        [MBProgressHUD showHUDAddedTo:viewController.view animated:YES];
    }
    
    if (requestType ==SPDAPIUrl ) {
        bURL = [self getRequestURL:bURL];
    }else if (requestType == SPDClanUrl){
        bURL=[self getClanURL:bURL];
    }else if (requestType == SPDRankUrl){
        bURL=[self getRankURL:bURL];
    }else if (requestType == SPDRelationUrl){
        bURL = [self getRelationURL:bURL];
    }else{
        bURL=[self getCommonUrl:bURL];
    }
    [params setObject:[[AppManager sharedInstance] getDeviceId] forKey:@"UDID"];

    AFHTTPSessionManager * manager = [self setupManager];
    [self setRequestHeaderLanguage:manager];

    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"]) {
        NSString * cookie_strings =[[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"];
        [manager.requestSerializer setValue:cookie_strings forHTTPHeaderField:@"Cookie"];
    }
    [manager GET:bURL parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        
        NSHTTPURLResponse * response = (NSHTTPURLResponse *)task.response;
        NSString * set_cookies = response.allHeaderFields[@"Set-Cookie"];
        if ([set_cookies containsString:@"server_token"]) {
            [[NSUserDefaults standardUserDefaults] setObject:set_cookies forKey:@"Set-Cookie"];
        }else{
            NSLog(@"verify url--- %@",bURL);
        }
        
        if (bAnimated) {
            [MBProgressHUD hideHUDForView:viewController.view animated:YES];
            MBProgressHUD *HUD = [MBProgressHUD HUDForView:viewController.view];
            if (HUD != nil) {
                [HUD removeFromSuperview];
                HUD = nil;
            }
        }
        
        if ([responseObject[@"code"] integerValue] != SUCCESSCODE) {
            if (bFailure) {
                bFailure(responseObject);
            }
            
            if ([responseObject[@"code"] integerValue] > SUCCESSCODE && [responseObject[@"code"] integerValue] <= BANNED) {
                //处理全局错误变量 1-5
                [RequestUtils handleResponse:responseObject presentAlertController:viewController bURL:bURL];
            }
        }
        else {
            bSuccess(responseObject[@"data"]);
            networkError =NO;
            
        }
        
    } failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
        if (bAnimated) {
            [MBProgressHUD hideHUDForView:viewController.view animated:YES];
        }
        if (error.code == 3840) {
//            [RequestUtils handleError404:error];
        } else {
            [RequestUtils handleError:error];
        }
        if (bFailure) {
            bFailure(@{@"msg": error.localizedDescription});
        }
    }];
}


//普通的url
+ (void)commonGetRequestUtils:(NSMutableDictionary *)params
                         bURL:(NSString *)bURL
                       isCommon:(BOOL )isCommon
                    bAnimated:(BOOL)bAnimated
      bAnimatedViewController:(UIViewController *)viewController
          bSuccessDoSomething:(XXWResponseSuccess)bSuccess
                     bFailure:(nullable XXWResponseFailure)bFailure
{
    
    if (bAnimated) {
        [MBProgressHUD showHUDAddedTo:viewController.view animated:YES];
    }
    
    if (isCommon) {
        bURL=[self getCommonUrl:bURL];
    }else{
        bURL = [self getRequestURL:bURL];
    }
    [params setObject:[[AppManager sharedInstance] getDeviceId] forKey:@"UDID"];
    
    AFHTTPSessionManager * manager = [self setupManager];
    [self setRequestHeaderLanguage:manager];

    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"]) {
        NSString * cookie_strings =[[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"];
        [manager.requestSerializer setValue:cookie_strings forHTTPHeaderField:@"Cookie"];
    }
    [manager GET:bURL parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        
        NSHTTPURLResponse * response = (NSHTTPURLResponse *)task.response;
        NSString * set_cookies = response.allHeaderFields[@"Set-Cookie"];
        [[NSUserDefaults standardUserDefaults] setObject:set_cookies forKey:@"Set-Cookie"];

        
        if (bAnimated) {
            [MBProgressHUD hideHUDForView:viewController.view animated:YES];
            MBProgressHUD *HUD = [MBProgressHUD HUDForView:viewController.view];
            if (HUD != nil) {
                [HUD removeFromSuperview];
                HUD = nil;
            }
        }
        
        if ([responseObject[@"code"] integerValue] != SUCCESSCODE) {
            if (bFailure) {
                bFailure(responseObject);
            }
            
            if ([responseObject[@"code"] integerValue] > SUCCESSCODE && [responseObject[@"code"] integerValue] <= BANNED) {
                //处理全局错误变量 1-5
                [RequestUtils handleResponse:responseObject presentAlertController:viewController bURL:bURL];
            }
        }
        else {
            bSuccess(responseObject[@"data"]);
            networkError =NO;
            
        }
        
    } failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
        if (bAnimated) {
            [MBProgressHUD hideHUDForView:viewController.view animated:YES];
        }
        if (error.code == 3840) {
//            [RequestUtils handleError404:error];
        } else {
            [RequestUtils handleError:error];
        }
        if (bFailure) {
            bFailure(@{@"msg": error.localizedDescription});
        }
    }];
}



//家族
+ (void)commonGetRequestUtils:(NSMutableDictionary *)params
                         bURL:(NSString *)bURL
                       isClan:(BOOL )isclan
                    bAnimated:(BOOL)bAnimated
      bAnimatedViewController:(UIViewController *)viewController
          bSuccessDoSomething:(XXWResponseSuccess)bSuccess
                     bFailure:(nullable XXWResponseFailure)bFailure
{
    
    if (bAnimated) {
        [MBProgressHUD showHUDAddedTo:viewController.view animated:YES];
    }
    
    if (isclan) {
        bURL=[self getClanURL:bURL];
    }else{
        bURL = [self getRequestURL:bURL];
    }
    [params setObject:[[AppManager sharedInstance] getDeviceId] forKey:@"UDID"];
    AFHTTPSessionManager * manager = [self setupManager];
    [self setRequestHeaderLanguage:manager];

    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"]) {
        NSString * cookie_strings =[[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"];
        [manager.requestSerializer setValue:cookie_strings forHTTPHeaderField:@"Cookie"];
    }
    [manager GET:bURL parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        
        NSHTTPURLResponse * response = (NSHTTPURLResponse *)task.response;
        NSString * set_cookies = response.allHeaderFields[@"Set-Cookie"];
        [[NSUserDefaults standardUserDefaults] setObject:set_cookies forKey:@"Set-Cookie"];
        
        if (bAnimated) {
            [MBProgressHUD hideHUDForView:viewController.view animated:YES];
            MBProgressHUD *HUD = [MBProgressHUD HUDForView:viewController.view];
            if (HUD != nil) {
                [HUD removeFromSuperview];
                HUD = nil;
            }
        }
        
        if ([responseObject[@"code"] integerValue] != SUCCESSCODE) {
            if (bFailure) {
                bFailure(responseObject);
            }
            
            if ([responseObject[@"code"] integerValue] > SUCCESSCODE && [responseObject[@"code"] integerValue] <= BANNED) {
                //处理全局错误变量 1-5
                [RequestUtils handleResponse:responseObject presentAlertController:viewController bURL:bURL];
            }
        }
        else {
            bSuccess(responseObject[@"data"]);
            networkError =NO;

        }
        
    } failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
        if (bAnimated) {
            [MBProgressHUD hideHUDForView:viewController.view animated:YES];
        }
        if (error.code == 3840) {
//            [RequestUtils handleError404:error];
        } else {
            [RequestUtils handleError:error];
        }
        if (bFailure) {
            bFailure(@{@"msg": error.localizedDescription});
        }
    }];
}

+ (void)commonPostRequestUtils:(NSMutableDictionary *)params
                          bURL:(NSString *)bURL
                        isClan:(BOOL )isclan
                     bAnimated:(BOOL)bAnimated
       bAnimatedViewController:(UIViewController *)viewController
           bSuccessDoSomething:(XXWResponseSuccess)bSuccess
                      bFailure:(nullable XXWResponseFailure)bFailure
{
    
    if (bAnimated) {
        [MBProgressHUD showHUDAddedTo:viewController.view animated:YES];
    }
    if (isclan) {
        bURL=[self getClanURL:bURL];
    }else{
        bURL = [self getRequestURL:bURL];
    }
    //
    
    AFHTTPSessionManager * manager = [self setupManager];
    [self setRequestHeaderLanguage:manager];

    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"]) {
        NSString * cookie_strings =[[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"];
        [manager.requestSerializer setValue:cookie_strings forHTTPHeaderField:@"Cookie"];
    }
    
    [manager POST:bURL parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        
        NSHTTPURLResponse * response = (NSHTTPURLResponse *)task.response;
        NSString * set_cookies = response.allHeaderFields[@"Set-Cookie"];
        [[NSUserDefaults standardUserDefaults] setObject:set_cookies forKey:@"Set-Cookie"];
        
        if (bAnimated) {
            [MBProgressHUD hideHUDForView:viewController.view animated:YES];
            
            MBProgressHUD *HUD = [MBProgressHUD HUDForView:viewController.view];
            if (HUD != nil) {
                [HUD removeFromSuperview];
                HUD = nil;
            }
        }
        
        if ([responseObject[@"code"] integerValue] != SUCCESSCODE) {
            if (bFailure) {
                bFailure(responseObject);
            }
            
            if ([responseObject[@"code"] integerValue] > SUCCESSCODE && [responseObject[@"code"] integerValue] <= BANNED) {
                //处理全局错误变量 1-5,7
                [RequestUtils handleResponse:responseObject presentAlertController:viewController bURL:bURL];

            }
        }
        else {
            bSuccess(responseObject[@"data"]);
            networkError =NO;
            
        }
        
    } failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
        if (bAnimated) {
            [MBProgressHUD hideHUDForView:viewController.view animated:YES];
        }
        if (bFailure) {
            bFailure(@{@"msg": error.localizedDescription});
        }
    }];
}


//涉及网络请求失败处理
+ (NSURLSessionDataTask *)commonGetRequestUtils:(NSMutableDictionary *)params
                                           bURL:(NSString *)bURL
                                      bAnimated:(BOOL)bAnimated
                        bAnimatedViewController:(UIViewController *)viewController
                            bSuccessDoSomething:(XXWResponseSuccess)bSuccess
                                       bFailure:(nullable XXWResponseFailure)bFailure
{

    if (bAnimated) {
        [MBProgressHUD showHUDAddedTo:viewController.view animated:YES];
    }
    if (!bURL) {
        return nil;
    }
    //fix 注册-重新登陆之后 账号会注销
    AFHTTPSessionManager * manager = [self setupManager];
    [self setRequestHeaderLanguage:manager];
    
    if ([bURL isEqualToString:@"login"] ||[bURL isEqualToString:@"signup/onetap"]|| [bURL isEqualToString:@"signup/onetap.pre"]||[bURL isEqualToString:@"platform.login"]) {
        [manager.requestSerializer setValue:nil forHTTPHeaderField:@"Cookie"];
        
    }else{
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"]) {
            NSString * cookie_strings =[[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"];
            [manager.requestSerializer setValue:cookie_strings forHTTPHeaderField:@"Cookie"];
        }
    }
    
    bURL = [self getRequestURL:bURL];
//    NSLog(@"my url --- %@",bURL);
    [params setObject:[[AppManager sharedInstance] getDeviceId] forKey:@"UDID"];

    NSURLSessionDataTask *task = [manager GET:bURL parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        
        NSHTTPURLResponse * response = (NSHTTPURLResponse *)task.response;
        NSString * set_cookies = response.allHeaderFields[@"Set-Cookie"];
        [[NSUserDefaults standardUserDefaults] setObject:set_cookies forKey:@"Set-Cookie"];
        

//        NSHTTPURLResponse * response = (NSHTTPURLResponse *)task.response;
//        NSLog(@"banner - remark  - %@",response.allHeaderFields);
//        if ([bURL containsString:@"banner"]) {
//            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"Login—Set-Cookie"];
//        }
        if (bAnimated) {
          [MBProgressHUD hideHUDForView:viewController.view animated:YES];
          MBProgressHUD *HUD = [MBProgressHUD HUDForView:viewController.view];
          if (HUD != nil) {
              [HUD removeFromSuperview];
              HUD = nil;
          }
        }

      if ([responseObject[@"code"] integerValue] != SUCCESSCODE) {
          if (bFailure) {
              bFailure(responseObject);
          }

          if ([responseObject[@"code"] integerValue] > SUCCESSCODE && [responseObject[@"code"] integerValue] <= BANNED) {
              //处理全局错误变量 1-5
              NSLog(@"error url----- %@",bURL);
              [RequestUtils handleResponse:responseObject presentAlertController:viewController bURL:bURL];
          }
      }
      else {
          
          //判断是不是my.banlance
          if ([bURL isEqualToString:[self getRequestURL:@"my.balance"]]) {
              
              NSString *goldBalance = responseObject[@"data"][@"balance"];
              NSString *jewelBalance = responseObject[@"data"][@"jewelBalance"];
              NSString *conchBalance = responseObject[@"data"][@"conch"];
              NSString *secretStr = [NSString stringWithFormat:@"balance=%@&conch=%@&jewelBalance=%@&key=%@", goldBalance, conchBalance, jewelBalance, jianyue_secretKey];
              
              //校验
              if ([secretStr.SHA256AndBase64 isEqualToString:responseObject[@"data"][@"sign"]]) {
                  bSuccess(responseObject[@"data"]);
                  networkError =NO;
              }else{
                  [RequestUtils handleError404:nil];
              }
              
          }else{
              bSuccess(responseObject[@"data"]);
              networkError =NO;
          }
          

      }

    } failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
        if (bAnimated) {
            [MBProgressHUD hideHUDForView:viewController.view animated:YES];
        }
        if (error.code != NSURLErrorCancelled) {
            [RequestUtils handleError:error];
        }
        if (bFailure) {
            bFailure(@{@"msg": error.localizedDescription});
        }
    }];
    return task;
}

+ (void)commonPostMultipartFormDataUtils:(NSMutableDictionary *)params
                                    bURL:(NSString *)bURL
                 bAnimatedViewController:(UIViewController *)viewController
               constructingBodyWithBlock:(XXWConstructingBody)constructingBody
                                 success:(XXWResponseSuccess)success
                                 failure:(nullable XXWResponseFailure)failure
{
//    AFHTTPSessionManager *manager = [self newSessionManager];
    [params setObject:[[AppManager sharedInstance] getDeviceId] forKey:@"UDID"];

    AFHTTPSessionManager * manager = [self setupManager];
    [self setRequestHeaderLanguage:manager];

//    if ([bURL containsString:@"signup.info"]) {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"]) {
            NSString * cookie_strings = [[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"];
            [manager.requestSerializer setValue:cookie_strings forHTTPHeaderField:@"Cookie"];
        }
//    }
    
    [manager POST:[self getRequestURL:bURL] parameters:params headers:nil constructingBodyWithBlock:constructingBody progress:^(NSProgress *_Nonnull uploadProgress) {
        NSLog(@"%@", uploadProgress);
    }success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        
        NSHTTPURLResponse * response = (NSHTTPURLResponse *)task.response;
        NSString * set_cookies = response.allHeaderFields[@"Set-Cookie"];
        [[NSUserDefaults standardUserDefaults] setObject:set_cookies forKey:@"Set-Cookie"];
        
//        
        if ([responseObject[@"code"] integerValue] != SUCCESSCODE) {
            if (failure) {
                failure(responseObject);
            }
            
            if ([responseObject[@"code"] integerValue] > SUCCESSCODE && [responseObject[@"code"] integerValue] <= BANNED) {
                //处理全局错误变量 1-5
                [RequestUtils handleResponse:responseObject presentAlertController:viewController bURL:bURL];
            }
        }
        else {
            success(responseObject[@"data"]);
            networkError =NO;

        }
        
    } failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
        [RequestUtils handleError:error];
        if (failure) {
            failure(@{@"msg": error.localizedDescription});
        }
    }];
}


+ (void)commonPostMultipartFormDataUtils:(NSMutableDictionary *)params
                                    bURL:(NSString *)bURL
                                  isClan:(BOOL)isclan
                 bAnimatedViewController:(UIViewController *)viewController
               constructingBodyWithBlock:(XXWConstructingBody)constructingBody
                                 success:(XXWResponseSuccess)success
                                 failure:(nullable XXWResponseFailure)failure
{
    //    AFHTTPSessionManager *manager = [self newSessionManager];
    [params setObject:[[AppManager sharedInstance] getDeviceId] forKey:@"UDID"];
    
    AFHTTPSessionManager * manager = [self setupManager];
    [self setRequestHeaderLanguage:manager];
    
    //    if ([bURL containsString:@"signup.info"]) {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"]) {
        NSString * cookie_strings = [[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"];
        [manager.requestSerializer setValue:cookie_strings forHTTPHeaderField:@"Cookie"];
    }
    //    }
    
    if (isclan) {
        bURL=[self getClanURL:bURL];
    }else{
        bURL = [self getRequestURL:bURL];
    }
    
    [manager POST:bURL parameters:params headers:nil constructingBodyWithBlock:constructingBody progress:^(NSProgress *_Nonnull uploadProgress) {
    }success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        
        NSHTTPURLResponse * response = (NSHTTPURLResponse *)task.response;
        NSString * set_cookies = response.allHeaderFields[@"Set-Cookie"];
        [[NSUserDefaults standardUserDefaults] setObject:set_cookies forKey:@"Set-Cookie"];
        
        //
        if ([responseObject[@"code"] integerValue] != SUCCESSCODE) {
            if (failure) {
                failure(responseObject);
            }
            
            if ([responseObject[@"code"] integerValue] > SUCCESSCODE && [responseObject[@"code"] integerValue] <= BANNED) {
                //处理全局错误变量 1-5
                [RequestUtils handleResponse:responseObject presentAlertController:viewController bURL:bURL];
            }
        }
        else {
            success(responseObject[@"data"]);
            networkError =NO;
            
        }
        
    } failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
        [RequestUtils handleError:error];
        if (failure) {
            failure(@{@"msg": error.localizedDescription});
        }
    }];
}


//progress
+ (void)commonPostMultipartFormDataUtils:(NSMutableDictionary *)params
                                    bURL:(NSString *)bURL
                 bAnimatedViewController:(UIViewController *)viewController
               constructingBodyWithBlock:(XXWConstructingBody)constructingBody
                                progress:(Progress)progress
                                 success:(XXWResponseSuccess)success
                                 failure:(nullable XXWResponseFailure)failure
{
//    AFHTTPSessionManager *manager = [self newSessionManager];
    [params setObject:[[AppManager sharedInstance] getDeviceId] forKey:@"UDID"];
    AFHTTPSessionManager *manager = [self  setupManager];
    [self setRequestHeaderLanguage:manager];

    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"]) {
        NSString * cookie_strings = [[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"];
        [manager.requestSerializer setValue:cookie_strings forHTTPHeaderField:@"Cookie"];
    }
    [manager POST:[self getRequestURL:bURL] parameters:params headers:nil constructingBodyWithBlock:constructingBody progress:^(NSProgress *_Nonnull uploadProgress) {
        progress(uploadProgress);
    }success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        
        NSHTTPURLResponse * response = (NSHTTPURLResponse *)task.response;
        NSString * set_cookies = response.allHeaderFields[@"Set-Cookie"];
        [[NSUserDefaults standardUserDefaults] setObject:set_cookies forKey:@"Set-Cookie"];
        
        if ([responseObject[@"code"] integerValue] != SUCCESSCODE) {
            if (failure) {
                failure(responseObject);
            }
            
            if ([responseObject[@"code"] integerValue] > SUCCESSCODE && [responseObject[@"code"] integerValue] <= BANNED) {
                //处理全局错误变量 1-5
                [RequestUtils handleResponse:responseObject presentAlertController:viewController bURL:bURL];
            }
        }
        else {
            success(responseObject[@"data"]);
            networkError =NO;

        }
        
    } failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
        [RequestUtils handleError:error];
        if (failure) {
            failure(@{@"msg": error.localizedDescription});
        }
    }];
}

+ (void)commonDeleteRequestUtils:(NSMutableDictionary *)params
                            bURL:(NSString *)bURL
                       bAnimated:(BOOL)bAnimated
         bAnimatedViewController:(UIViewController *)viewController
             bSuccessDoSomething:(XXWResponseSuccess)bSuccess
                        bFailure:(nullable XXWResponseFailure)bFailure
{
    
    if (bAnimated) {
        [MBProgressHUD showHUDAddedTo:viewController.view animated:YES];
    }
    
    bURL = [self getRequestURL:bURL];
    [params setObject:[[AppManager sharedInstance] getDeviceId] forKey:@"UDID"];
    
    AFHTTPSessionManager * manager = [self setupManager];
    [self setRequestHeaderLanguage:manager];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"]) {
        NSString * cookie_strings =[[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"];
        [manager.requestSerializer setValue:cookie_strings forHTTPHeaderField:@"Cookie"];
    }

    [manager DELETE:bURL parameters:params headers:nil success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        NSString *set_cookies = response.allHeaderFields[@"Set-Cookie"];
        if (set_cookies.notEmpty) {
            [[NSUserDefaults standardUserDefaults] setObject:set_cookies forKey:@"Set-Cookie"];
        }
        if (bAnimated) {
            [MBProgressHUD hideHUDForView:viewController.view animated:YES];
            MBProgressHUD *HUD = [MBProgressHUD HUDForView:viewController.view];
            if (HUD != nil) {
                [HUD removeFromSuperview];
                HUD = nil;
            }
        }
        
        if ([responseObject[@"code"] integerValue] != SUCCESSCODE) {
            if (bFailure) {
                bFailure(responseObject);
            }
            
            if ([responseObject[@"code"] integerValue] > SUCCESSCODE && [responseObject[@"code"] integerValue] <= BANNED) {
                //处理全局错误变量 1-5
                [RequestUtils handleResponse:responseObject presentAlertController:viewController bURL:bURL];
            }
        } else {
            bSuccess(responseObject[@"data"]);
            networkError = NO;
        }
        
    } failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
        if (bAnimated) {
            [MBProgressHUD hideHUDForView:viewController.view animated:YES];
        }
        if (error.code == 3840) {
//            [RequestUtils handleError404:error];
        } else {
            [RequestUtils handleError:error];
        }
        if (bFailure) {
            bFailure(@{@"msg": error.localizedDescription});
        }
    }];
}


//处理全局错误弹框
+ (BOOL)handleResponse:(NSDictionary *)responseObject presentAlertController:(UIViewController *)presentAlertController
                  bURL:(NSString *)URL{

    BOOL result = NO;
    if ([responseObject isKindOfClass:[NSDictionary class]]) {
        NSInteger errorCode = [responseObject[@"code"] integerValue];
        NSString *errormsg = responseObject[@"msg"];
        if (errorCode != 0) {

            switch (errorCode) {
                case LOGININVALID_ERROR: {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginInvalid" object:nil];
                    break;
                }

                case LOWVERSION_ERROR: {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"LOWVERSION_ERROR" object:nil];
                    break;
                }

                case SERVER_MAINTENANCE: {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"SERVER_MAINTENANCE" object:nil];
                    break;
                }
                    
                case BANNED:{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"BANNED" object:nil userInfo:responseObject];
                    break;
                }
                    
                case SERVER_BUG:{
                    NSLog(@"SERVER_BUGSERVER_BUG== %@",URL);
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"EXIT" object:nil];
                    break;
                }

                case 6:
                    break;

                default:
                    [self alertViewWithMessage:responseObject[@"msg"]];
                    break;
            }
            result = NO;
        }
        else {
            result = YES;
        }
    }
    return result;
}

//弹框
+ (void)alertViewWithMessage:(NSString *)message
{
    if (!networkError) {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@""
                                  message:message
                                  delegate:nil
                                  cancelButtonTitle:nil
                                  otherButtonTitles:@"OK", nil];
        [alertView show];
        networkError =YES;
    }
    
}

+ (void)setUserStauesOnline:(BOOL)online
{

    NSMutableDictionary *dict = [NSMutableDictionary new];
    if (online) {
        [dict setObject:@"online" forKey:@"status"];
    }
    else {
        [dict setObject:@"offline" forKey:@"status"];
    }
    
    if ([[SPDApiUser currentUser] isLoginToLocal]) {
        [RequestUtils commonPostRequestUtils:dict bURL:@"my.status" bAnimated:YES bAnimatedViewController:[UIViewController new] bSuccessDoSomething:^(id _Nullable suceess) {
        }bFailure:^(id  _Nullable failure) {
            
        }];
    }

   
}

@end
