//
//  HL_GIFRefreshHeader.m
//  SimpleDate
//
//  Created by Monkey on 2018/1/2.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "HL_GIFRefreshHeader.h"

@implementation HL_GIFRefreshHeader

#pragma mark - 重写父类的方法
- (void)prepare{
    
    [super prepare];
    
    // 创建一个数组来存放普通状态下的gif图片（就是在下拉过程中，还没有达到松手即刷新界限的时刻）
    NSMutableArray *normalImages = [NSMutableArray array];
    for (NSUInteger i = 50; i<= 85; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"loading_000%ld", i]];
        [normalImages addObject:image];
    }
    
    // 创建一个数组来存放将要刷新时需要展示的gif图片数组（就是松手即刷新的时刻）
    NSMutableArray *refreshingImages = [NSMutableArray array];
    for (NSUInteger i = 50; i<= 85; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"loading_000%ld", i]];
        [refreshingImages addObject:image];
    }
    
    // 设置下拉过程中的图片数组
//    [self setImages:normalImages forState:MJRefreshStateIdle];
    [self setImages:normalImages duration:35*0.04 forState:MJRefreshStateIdle];
    // 设置松手即刷新时的图片数组
//    [self setImages:normalImages forState:MJRefreshStatePulling];
    [self setImages:normalImages duration:35*0.04 forState:MJRefreshStatePulling];

    // 设置正在刷新状态时的动画图片
//    [self setImages:refreshingImages forState:MJRefreshStateRefreshing];
    [self setImages:refreshingImages duration:35*0.04 forState:MJRefreshStateRefreshing];

    
    //隐藏时间文字
    self.lastUpdatedTimeLabel.hidden = YES;
    //隐藏状态文字
    self.stateLabel.hidden = YES;
}


@end
