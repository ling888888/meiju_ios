//
//  ZegoKitManager.h
//  ZegoSdkDemo
//
//  Created by Strong on 2016/12/15.
//  Copyright © 2016年 Zego. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ZegoAudioRoom/ZegoAudioRoom.h>
#import "SPDChatroomPasswordView.h"
#import <ZegoAudioRoom/zego-api-mediaplayer-oc.h>
#import <ZegoAudioRoom/zego-api-audio-processing-oc.h>

#define ZegoManager [ZegoKitManager sharedInstance]
#define MicCount 9
#define PublicMicCount 4

typedef NS_ENUM(NSUInteger, SceneType) {
    SceneTypeNone,
    SceneTypeClanRoom,
    SceneTypeRadioRoom,
    SceneTypeListenRadio,
    SceneTypeLiveRoom
};

typedef NS_ENUM(NSUInteger, LinkMicState) {
    LinkMicStateNormal,
    LinkMicStateDialing,
    LinkMicStateIncoming,
    LinkMicStateLinking
};

UIKIT_EXTERN NSNotificationName const LinkMicSwitchDidChangeNotification;
UIKIT_EXTERN NSNotificationName const LinkMicStateDidChangeNotification;
UIKIT_EXTERN NSNotificationName const LinkMicUserDidChangeNotification;

@class RadioRoomBackgroundRunView, LiveModel;

@protocol ZegoKitManagerDelegate <NSObject>

@optional

- (void)micStatusDidChanged:(id)data;
- (void)kickOut;
- (void)didLeaveRoom;
- (void)liveDidStart;

@end

@interface ZegoKitManager : NSObject<ZegoAudioRoomDelegate, ZegoAudioIMDelegate, ZegoAudioLivePublisherDelegate>

+ (instancetype)sharedInstance;

@property (nonatomic, strong, readonly) ZegoAudioRoomApi *api;
@property (nonatomic, assign) SceneType sceneType;

@property (nonatomic, weak) id<ZegoKitManagerDelegate> delegate;

@property (nonatomic, copy) NSString *clan_id;
@property (nonatomic, copy) NSString *clan_name;
@property (nonatomic, copy) NSString *clan_cover;
@property (nonatomic, copy) NSString *selfMicIndex;
@property (nonatomic, copy) NSString *selfEndStatus;
@property (nonatomic, assign) NSInteger popIndex;

@property (nonatomic, assign) BOOL isBackground;

@property (nonatomic, assign) BOOL micEnabled;
@property (nonatomic, assign) BOOL micSelected;
@property (nonatomic, assign) BOOL speakerEnabled;
@property (nonatomic, assign) BOOL chatroomLock;
@property (nonatomic, assign) BOOL isLock;
@property (nonatomic, assign) BOOL canLock;
@property (nonatomic, assign) BOOL uploadedCount;

@property (nonatomic, strong) NSMutableDictionary *micStatus;
@property (nonatomic, strong) NSMutableDictionary *micUsers;
@property (nonatomic, strong) NSMutableDictionary *streams;
@property (nonatomic, strong) UIImageView *clanBackgroundView;
@property (nonatomic, strong) RadioRoomBackgroundRunView *radioBackgroundRunView;

- (void)startPublish;
- (void)startPublishWithUserId:(NSString *)userId;
- (void)stopPublish;
- (void)enableMic:(BOOL)bEnable;
- (void)enableSpeaker:(BOOL)bEnable;
- (void)pauseAudio;
- (void)resumeAudio;
- (void)sendCustomCommandToUser:(NSString *)userId content:(NSString *)content;
- (void)sendBigRoomMessage:(NSString *)message;

- (void)joinRoomFrom:(UIViewController *)vc clanId:(NSString *)clanId;
- (void)leaveRoom;
- (void)showPasswordViewWithType:(PasswordViewType)type;
- (void)requestChatroomLockUseWithType:(NSString *)type password:(NSString *)password;

- (NSString *)getUserMicIndex:(NSString *)userId;
- (void)startCheckStreams;
- (void)kickOutUser:(NSString *)userId;

#pragma mark - RadioRoom & ListenRadio

@property (nonatomic, strong) ZegoMediaPlayer *radioPlayer;

- (void)startPlayRadio:(NSString *)radio;
- (void)muteRadio:(BOOL)mute;
- (void)stopPlayRadio;
- (void)exitListenRadio;

#pragma mark - LiveRoom

@property (nonatomic, strong) LiveModel *liveInfo;
@property (nonatomic, assign) BOOL isAnchor;
@property (nonatomic, assign) BOOL linkMicSwitch;
@property (nonatomic, assign) BOOL linkMicFreeUpMicSwitch;

@property (nonatomic, strong) NSMutableDictionary * linkMicUserInfoDic;

@property (nonatomic, assign) LinkMicState linkMicState;
@property (nonatomic, strong) NSMutableDictionary *linkMicLinkingDic; // old
@property (nonatomic, strong) NSMutableDictionary *linkMicApplyDic;
@property (nonatomic, strong) NSMutableDictionary *linkMicUnreadDic;
@property (nonatomic, strong) NSMutableDictionary *linkMicInvitingDic;
@property (nonatomic, assign) NSTimeInterval linkMicStartTimeStamp;

- (void)joinLiveRoomWithliveInfo:(LiveModel *)liveInfo isAnchor:(BOOL)isAnchor;
- (void)playBackgroundMusic:(NSString *)music;
- (void)pauseBackgroundMusic;
- (void)resumeBackgroundMusic;
- (void)stopBackgroundMusic;
- (void)playAudioEffect:(NSString *)effect;
- (void)enableLoopback:(BOOL)bEnable;

- (void)startLinkMic;
- (void)stopLinkMic;

- (void)requestLiveUpMicChange:(NSDictionary *)dict; // 用户上麦
- (void)requestDownMic:(NSDictionary *)dict;

@end
