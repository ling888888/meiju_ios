//
//  IAPManager.m
//  SimpleDate
//
//  Created by 李楠 on 2020/4/15.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "IAPManager.h"
#import <StoreKit/StoreKit.h>
#import "DBUtil.h"

#define PreorderInfoKey @"PreorderInfo"

@interface IAPManager ()<SKPaymentTransactionObserver, SKProductsRequestDelegate>

@property (nonatomic, copy) void (^completion)(BOOL succeeded);

@end

@implementation IAPManager

+ (IAPManager *)sharedInstance {
    static IAPManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

#pragma mark - Public methods

- (void)startObserve {
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
}

- (void)checkUnfinishedPaymentsOfType:(IAPType)type {
    NSMutableArray *unfinishedPayments = [DBUtil queryIAPWithType:type];
    if (unfinishedPayments.count > 0) {
        [self verifyPaymentOfType:type params:unfinishedPayments.firstObject];
    }
}

- (void)startPaymentWithProductId:(NSString *)productId completion:(nullable void (^)(BOOL succeeded))completion {
    [MBProgressHUD showHUDAddedTo:UIApplication.sharedApplication.delegate.window animated:YES];
    self.completion = completion;
    if ([SKPaymentQueue canMakePayments]) {
        switch ([self typeOfProduct:productId]) {
            case IAPTypeConch:
                [self requestProductWithProductId:productId billId:nil];
                break;
            default:
                [self requestBillRechargeIAPWithProductId:productId];
                break;
        }
    } else {
        [self paymentEndedWitSucceeded:NO message:@"You can't make payments now"];
    }
}

#pragma mark - Private methods

- (void)requestBillRechargeIAPWithProductId:(NSString *)productId {
    NSDictionary *params = @{@"product_id": productId};
    [RequestUtils commonGetRequestUtils:[params mutableCopy] bURL:@"bill.recharge.iap.live" bAnimated:NO bAnimatedViewController:[UIViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        [self requestProductWithProductId:productId billId:suceess[@"_id"]];
    } bFailure:^(id  _Nullable failure) {
        [self paymentEndedWitSucceeded:NO message:failure[@"msg"]];
    }];
}

- (void)requestProductWithProductId:(NSString *)productId billId:(nullable NSString *)billId {
    [[NSUserDefaults standardUserDefaults] setObject:@{@"userId": [SPDApiUser currentUser].userId, @"billId": billId ? : @""} forKey:PreorderInfoKey];

    NSSet *productIdentifiers = [NSSet setWithObject:productId];
    SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
    request.delegate = self;
    [request start];
}

- (void)verifyPaymentOfType:(IAPType)type params:(NSDictionary *)params {
    switch (type) {
        case IAPTypeConch:
            [self requestPayAppleVerifyWithParams:params];
            break;
        default:
            [self requestBillRechargeCheckIAPWithParams:params];
            break;
    }
}

- (void)requestBillRechargeCheckIAPWithParams:(NSDictionary *)params {
    [RequestUtils commonPostRequestUtils:[params mutableCopy] bURL:@"bill.recharge.check.iap.live" bAnimated:YES bAnimatedViewController:[UIViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        [DBUtil deleteIAPWithTransactionId:params[@"transaction_id"]];
        [self paymentEndedWitSucceeded:YES message:SPDLocalizedString(@"支付成功")];
    } bFailure:^(id  _Nullable failure) {
        if ([failure[@"code"] integerValue] == 3010){
            [DBUtil deleteIAPWithTransactionId:params[@"transaction_id"]];
        }
        [self paymentEndedWitSucceeded:NO message:failure[@"msg"]];
    }];
}

- (void)requestPayAppleVerifyWithParams:(NSDictionary *)params {
    [RequestUtils POST:URL_NewServer(@"sys/pay.apple.verify") parameters:params success:^(id  _Nullable suceess) {
        [DBUtil deleteIAPWithTransactionId:params[@"transaction_id"]];
        [self paymentEndedWitSucceeded:YES message:SPDLocalizedString(@"支付成功")];
    } failure:^(id  _Nullable failure) {
        if ([failure[@"code"] integerValue] == 3010){
            [DBUtil deleteIAPWithTransactionId:params[@"transaction_id"]];
        }
        [self paymentEndedWitSucceeded:NO message:failure[@"msg"]];
    }];
}

- (void)paymentEndedWitSucceeded:(BOOL)succeeded message:(nullable NSString *)message {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.completion) {
            self.completion(succeeded);
        }
        self.completion = nil;
        [MBProgressHUD hideHUDForView:UIApplication.sharedApplication.delegate.window animated:YES];
        
        if (message.notEmpty) {
            [self showTips:message];
        }
    });
}

- (IAPType)typeOfProduct:(NSString *)productId {
    if ([productId containsString:@"VIP"]) {
        return IAPTypeVIP;
    } else if ([productId containsString:@"shell"]) {
        return IAPTypeConch;
    } else {
        return IAPTypeGold;
    }
}

#pragma mark - SKPaymentTransactionObserver

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray<SKPaymentTransaction *> *)transactions {
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                break;
            case SKPaymentTransactionStatePurchased: {
                NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
                NSData *receiptData = [NSData dataWithContentsOfURL:receiptURL];
                NSString *receiptString = [receiptData base64EncodedStringWithOptions:0];
                
                NSDictionary *preorderInfo = [[NSUserDefaults standardUserDefaults] dictionaryForKey:PreorderInfoKey];
                NSString *preorderBillId = preorderInfo[@"billId"];
                NSString *preorderUserId = preorderInfo[@"userId"];
                IAPType type = [self typeOfProduct:transaction.payment.productIdentifier];
                
                [DBUtil recordIAPWithTransactionId:transaction.transactionIdentifier userId:preorderUserId billId:preorderBillId receipt:receiptString type:type];
                
                if ([[SPDApiUser currentUser].userId isEqualToString:preorderUserId]) {
                    NSMutableDictionary *params = [NSMutableDictionary new];
                    [params setValue:preorderBillId forKey:@"bill_id"];
                    [params setValue:transaction.transactionIdentifier forKey:@"transaction_id"];
                    [params setValue:receiptString forKey:@"receipt_data"];
                    [self verifyPaymentOfType:type params:params];
                } else {
                    [self paymentEndedWitSucceeded:NO message:nil];
                }
                
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:PreorderInfoKey];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            }
            case SKPaymentTransactionStateFailed:
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                [self paymentEndedWitSucceeded:NO message:transaction.error.localizedDescription];
                break;
            case SKPaymentTransactionStateRestored:
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                [self paymentEndedWitSucceeded:NO message:nil];
                break;
            default:
                break;
        }
    }
}

#pragma mark - SKProductsRequestDelegate

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    if (response.products.count > 0) {
        SKProduct *product = response.products.firstObject;
        SKPayment *payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    } else {
        [self paymentEndedWitSucceeded:NO message:@"Product is invalid"];
    }
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    [self paymentEndedWitSucceeded:NO message:error.localizedDescription];
}

@end
