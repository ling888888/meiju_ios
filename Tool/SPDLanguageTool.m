//
//  SPDLaunageTool.m
//  SimpleDate
//
//  Created by Monkey on 2017/11/4.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDLanguageTool.h"
#import "AppDelegate.h"
#import "ZegoKitManager.h"

@interface SPDLanguageTool ()

@property (nonatomic, strong) NSBundle *bundle;

@end

@implementation SPDLanguageTool

+ (instancetype)sharedInstance {
    static SPDLanguageTool *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[SPDLanguageTool alloc] init];
    });
    return instance;
}

- (instancetype)init {
    if (self = [super init]) {
        [self initCurrentLanguage];
    }
    return self;
}

- (void)initCurrentLanguage {
    NSString *language = [[NSUserDefaults standardUserDefaults] objectForKey:CURRENTLANGUAGE];
    if (!language) {
        // 第一次给系统的语言
        NSString *systemLanguage = [SPDCommonTool getPreferredLanguage];
        NSArray *array = [systemLanguage componentsSeparatedByString:@"-"];
        if (array.count == 3) {
            language = [NSString stringWithFormat:@"%@-%@", array[0], array[1]];
        } else {
            language = [NSString stringWithFormat:@"%@", array[0]];
        }
    }
    self.currentLanguage = language;
}

- (NSString *)getStringForKey:(NSString *)key withTable:(NSString *)table {
    return NSLocalizedStringFromTableInBundle(key, @"SPD", self.bundle, @"");
}

- (NSString *)getStringForKey:(NSString *)key {
    return NSLocalizedStringFromTableInBundle(key, @"SPD", self.bundle, @"");
}

- (void)setNewLanguage:(NSString *)language {
    if (![language isEqualToString:self.currentLanguage]) {
        // 语言区切换这里中文用zh, 但是app语言选择是zh-hans, 所以这里做一下兼容
        if ([language containsString:@"zh"]) {
            self.currentLanguage = @"zh-Hans";
        } else {
            self.currentLanguage = language;
        }
    };
    
    [SPDCommonTool showWindowToast:SPDStringWithKey(@"设置成功", nil)];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self resetRootViewController];
    });
}

- (void)resetRootViewController {    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([SPDApiUser currentUser].isLoginToLocal) {
        LY_TabBarController *tabBarController = [[LY_TabBarController alloc] init];
        appDelegate.window.rootViewController = tabBarController;
    } else {
        UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        appDelegate.window.rootViewController = [loginStoryboard instantiateInitialViewController];
    }
    if ([ZegoKitManager sharedInstance].isBackground) {
       
        [appDelegate.window bringSubviewToFront:[ZegoKitManager sharedInstance].radioBackgroundRunView];
    }
}

- (void)setCurrentLanguage:(NSString *)currentLanguage {
    if ([[NSBundle mainBundle] pathForResource:currentLanguage ofType:@"lproj"]) {
        _currentLanguage = currentLanguage;
    } else {
        _currentLanguage = @"en";
    }
    NSString *path = [[NSBundle mainBundle] pathForResource:_currentLanguage ofType:@"lproj"];
    self.bundle = [NSBundle bundleWithPath:path];
    
    [[NSUserDefaults standardUserDefaults] setObject:_currentLanguage forKey:CURRENTLANGUAGE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
    
- (NSDictionary *)allLanguages {
    if (!_allLanguages) {
        _allLanguages = @{@"zh-Hans": @"中文", // 中文
                          @"ar": @"اللغة العربية", // 阿语
                          @"en": @"English", // 英语
                          };
        // @"de": @"Deutsch", // 德语
        // @"ru": @"Русский язык" // 俄语
        // @"fr": @"Français" // 法语
    }
    return _allLanguages;
}

@end
