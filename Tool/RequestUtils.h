//
//  RequestUtils.h
//  SimpleDate
//
//  Created by xu.juvenile on 2016/3/3.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFURLRequestSerialization.h"



NS_ASSUME_NONNULL_BEGIN

#define URL_NewServer(name) [NSString stringWithFormat:@"%@%@?_v=%@", NEW_SERVER_URL, name, API_VERSION]
#define URL_Server(name) [NSString stringWithFormat:@"%@%@?_v=%@", SERVER_URL, name, API_VERSION]

typedef void (^XXWConstructingBody)(id <AFMultipartFormData> formData);
typedef void (^XXWResponseSuccess)(id _Nullable suceess);
typedef void (^XXWResponseFailure)(id _Nullable failure);
typedef void (^Progress) (NSProgress * _Nonnull uploadProgress);

 typedef enum {
    SPDAPIUrl,
    SPDClanUrl,
    SPDRankUrl,
    SPDRelationUrl,
     
}SPDUrlRequestType;

@interface RequestUtils : NSObject



+ (NSMutableDictionary*)commonRequestParameter;
+ (void)handleError:(NSError*)error;

 //处理全局错误弹框
+ (BOOL)handleResponse:(NSDictionary *)responseObject presentAlertController:(UIViewController *)presentAlertController;

+ (NSURLSessionDataTask *)POST:(NSString *)URL
                    parameters:(NSDictionary *)parameters
                       success:(XXWResponseSuccess)success
                       failure:(XXWResponseFailure)failure;

+ (NSURLSessionDataTask *)GET:(NSString *)URL
                   parameters:(NSDictionary *)parameters
                      success:(XXWResponseSuccess)success
                      failure:(XXWResponseFailure)failure;

//弹框
//+ (void)alertViewWithMessage:(NSString *)message;
+ (void)commonGetRequestUtils:(NSMutableDictionary *)params
                         bURL:(NSString *)bURL
                     isCommon:(BOOL )isCommon
                    bAnimated:(BOOL)bAnimated
      bAnimatedViewController:(UIViewController *)viewController
          bSuccessDoSomething:(XXWResponseSuccess)bSuccess
                     bFailure:(nullable XXWResponseFailure)bFailure;

+ (void)commonPostRequestUtils:(NSMutableDictionary *)params
                          bURL:(NSString *)bURL
                     bAnimated:(BOOL)bAnimated
       bAnimatedViewController:(UIViewController *)viewController
           bSuccessDoSomething:(XXWResponseSuccess)bSuccess
                      bFailure:(nullable XXWResponseFailure)bFailure;
//带类型的
+ (void)commonGetUrlRequestUtils:(NSMutableDictionary *)params
                         bURL:(NSString *)bURL
                RequestType:(SPDUrlRequestType) requestType
                    bAnimated:(BOOL)bAnimated
      bAnimatedViewController:(UIViewController *)viewController
          bSuccessDoSomething:(XXWResponseSuccess)bSuccess
                     bFailure:(nullable XXWResponseFailure)bFailure;

+ (void)commonPostUrlRequestUtils:(NSMutableDictionary *)params
                             bURL:(NSString *)bURL
                      RequestType:(SPDUrlRequestType) requestType
                        bAnimated:(BOOL)bAnimated
          bAnimatedViewController:(UIViewController *)viewController
              bSuccessDoSomething:(XXWResponseSuccess)bSuccess
                         bFailure:(nullable XXWResponseFailure)bFailure;

+ (NSURLSessionDataTask *)commonGetRequestUtils:(NSMutableDictionary *)params
                                           bURL:(NSString *)bURL
                                      bAnimated:(BOOL)bAnimated
                        bAnimatedViewController:(UIViewController *)viewController
                            bSuccessDoSomething:(XXWResponseSuccess)bSuccess
                                       bFailure:(nullable XXWResponseFailure)bFailure;

+ (void)commonGetRequestUtils:(NSMutableDictionary *)params
                         bURL:(NSString *)bURL
                       isClan:(BOOL )isclan
                    bAnimated:(BOOL)bAnimated
      bAnimatedViewController:(UIViewController *)viewController
          bSuccessDoSomething:(XXWResponseSuccess)bSuccess
                     bFailure:(nullable XXWResponseFailure)bFailure;

+ (void)commonPostRequestUtils:(NSMutableDictionary *)params
                          bURL:(NSString *)bURL
                        isClan:(BOOL )isclan
                     bAnimated:(BOOL)bAnimated
       bAnimatedViewController:(UIViewController *)viewController
           bSuccessDoSomething:(XXWResponseSuccess)bSuccess
                      bFailure:(nullable XXWResponseFailure)bFailure;

+ (void)commonPostMultipartFormDataUtils:(NSMutableDictionary *)params
                    bURL:(NSString *)bURL
                 bAnimatedViewController:(UIViewController *)viewController
     constructingBodyWithBlock:(XXWConstructingBody)constructingBody
                       success:(XXWResponseSuccess)success
                       failure:(nullable XXWResponseFailure)failure;

+(void)SpecialPostRequestUtils:(NSMutableDictionary *)params
                          bURL:(NSString *)bURL
                     bAnimated:(BOOL)bAnimated
       bAnimatedViewController:(UIViewController *)viewController
           bSuccessDoSomething:(XXWResponseSuccess)bSuccess
                      bFailure:(nullable XXWResponseFailure)bFailure;
+(void)SpecialGetRequestUtils:(NSMutableDictionary *)params
                         bURL:(NSString *)bURL
                    bAnimated:(BOOL)bAnimated
      bAnimatedViewController:(UIViewController *)viewController
          bSuccessDoSomething:(XXWResponseSuccess)bSuccess
                     bFailure:(nullable XXWResponseFailure)bFailure;
//上传文件 下载进度
+ (void)commonPostMultipartFormDataUtils:(NSMutableDictionary *)params
                                    bURL:(NSString *)bURL
                 bAnimatedViewController:(UIViewController *)viewController
               constructingBodyWithBlock:(XXWConstructingBody)constructingBody
                                progress:(Progress)progress
                                 success:(XXWResponseSuccess)success
                                 failure:(nullable XXWResponseFailure)failure;

+ (void)commonDeleteRequestUtils:(NSMutableDictionary *)params
                            bURL:(NSString *)bURL
                       bAnimated:(BOOL)bAnimated
         bAnimatedViewController:(UIViewController *)viewController
             bSuccessDoSomething:(XXWResponseSuccess)bSuccess
                        bFailure:(nullable XXWResponseFailure)bFailure;

+ (void)commonPostMultipartFormDataUtils:(NSMutableDictionary *)params
                                    bURL:(NSString *)bURL
                                  isClan:(BOOL)isclan
                 bAnimatedViewController:(UIViewController *)viewController
               constructingBodyWithBlock:(XXWConstructingBody)constructingBody
                                 success:(XXWResponseSuccess)success
                                 failure:(nullable XXWResponseFailure)failure;

+ (void)setUserStauesOnline:(BOOL )online;

+ (NSURLSessionDataTask *)PostMultipartFormDataUtils:(NSMutableDictionary *)params
                     bURL:(NSString *)bURL
constructingBodyWithBlock:(XXWConstructingBody)constructingBody
                  success:(XXWResponseSuccess)success
                                             failure:(nullable XXWResponseFailure)failure;
@end

NS_ASSUME_NONNULL_END
