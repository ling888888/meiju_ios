//
//  AppManager.h
//  SimpleDate
//
//  Created by xu.juvenile on 16/7/8.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppManager : NSObject

+ (AppManager *)sharedInstance;
- (NSString *)getDeviceId;

@end
