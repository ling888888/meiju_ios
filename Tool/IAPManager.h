//
//  IAPManager.h
//  SimpleDate
//
//  Created by 李楠 on 2020/4/15.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#define kIAPManager [IAPManager sharedInstance]

typedef NS_ENUM(NSUInteger, IAPType) {
    IAPTypeGold,
    IAPTypeVIP,
    IAPTypeConch
};

@interface IAPManager : NSObject

+ (IAPManager *)sharedInstance;

- (void)startObserve;
- (void)startPaymentWithProductId:(NSString *)productId completion:(nullable void (^)(BOOL succeeded))completion;
- (void)checkUnfinishedPaymentsOfType:(IAPType)type;

@end

NS_ASSUME_NONNULL_END
