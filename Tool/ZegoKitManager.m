//
//  ZegoKitManager.m
//  ZegoSdkDemo
//
//  Created by Strong on 2016/12/15.
//  Copyright © 2016年 Zego. All rights reserved.
//

#import "ZegoKitManager.h"
#import "SPDVoiceLiveUserModel.h"
#import "SPDVoiceLiveOnlineUserCell.h"
#import "ChatRoomLiveViewController.h"
#import "ChatroomVoiceViewController.h"
#import "RadioRoomBackgroundRunView.h"
#import "TimerPowerOffMgr.h"
#import "LiveModel.h"
#import "LiveRoomViewController.h"
#import "LinkMicModel.h"
#import "LinkMicAudienceIncomingView.h"
#import "LinkMicReceiveApplyMsgView.h"
#import "LinkMicNewModel.h"

NSNotificationName const LinkMicStateDidChangeNotification = @"LinkMicStateDidChangeNotification";
NSNotificationName const LinkMicUserDidChangeNotification = @"LinkMicUserDidChangeNotification";
NSNotificationName const LinkMicSwitchDidChangeNotification = @"LinkMicSwitchDidChangeNotification";

@interface ZegoKitManager ()<SPDChatroomPasswordViewDelegate>

@property (nonatomic, strong) NSTimer *streamsTimer;
@property (nonatomic, strong) NSTimer *linkMicStreamTimer; // 跟主播连麦的时候 30s 心跳一次
@property (nonatomic, strong) SPDChatroomPasswordView *passwordView;
@property (nonatomic, weak) UIViewController *fromViewController;
@property (nonatomic, copy) NSString *toClanId;
@property (nonatomic, assign) BOOL joining;
@property (nonatomic, assign) BOOL enterNewChatroom;

@property (nonatomic, strong) ZegoMediaPlayer *livePlayer;
@property (nonatomic, strong) ZegoMediaPlayer *audioEffectPlayer;

@end

@implementation ZegoKitManager

+ (instancetype)sharedInstance {
    static ZegoKitManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ZegoKitManager alloc] init];
    });
    return manager;
}

- (instancetype)init {
    if (self = [super init]) {
        [ZegoAudioRoomApi setUseTestEnv:NO];
        
#ifdef DEBUG
        [ZegoAudioRoomApi setVerbose:YES];
#endif
        
        Byte signkey[] = {0x18, 0x19, 0x41, 0x2a, 0xa5, 0xcd, 0x97, 0x06, 0xb8, 0x0e, 0x94, 0x9d, 0x5e, 0x50, 0x1f, 0x2d, 0xbb, 0x82, 0x21, 0xe5, 0xd9, 0xf4, 0xcd, 0x23, 0xe7, 0xd6, 0x9b, 0x02, 0x4a, 0xf1, 0xf5, 0x48};
        NSData *appSign = [NSData dataWithBytes:signkey length:32];
        _api = [[ZegoAudioRoomApi alloc] initWithAppID:ZEGOAPPID appSignature:appSign];
        
        [self.api setManualPublish:YES];
        [self.api setAudioIMDelegate:self];
        [self initProperties];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMagicEffect:) name:@"CHATROOM_MAGIC_Effect" object:nil];
    }
    return self;
}

- (void)initProperties {
    self.clan_id = nil;
    self.clan_name = nil;
    self.clan_cover = nil;
    self.selfMicIndex = nil;
    self.isBackground = NO;
    self.speakerEnabled = YES;
    self.micEnabled = NO;
    self.micSelected = NO;
    self.isLock = NO;
    self.canLock = NO;
    self.uploadedCount = NO;
    [self.streamsTimer invalidate];
    self.streamsTimer = nil;
    self.sceneType = SceneTypeNone;
    [self resetLiveRoomProperties];
}

- (void)initMicStatus {
    for (NSInteger i = 0; i < MicCount; i++) {
        [_micStatus setValue:@(Freeze) forKey:[NSString stringWithFormat:@"%zd", i]];
    }
}

#pragma mark - Join room & Leave room

- (void)joinRoomFrom:(UIViewController *)viewController clanId:(NSString *)clanId {
    if (self.sceneType == SceneTypeLiveRoom && self.isAnchor) {
        return;
    }
    if (clanId && clanId.length && !self.joining) {
        self.joining = YES;
        self.fromViewController = viewController;
        self.toClanId = clanId;
        if ([self.clan_id isEqualToString:clanId]) {
            [self joinRoom];
        } else {
            [self requestChatroomEnter];
        }
    }
}

- (void)requestChatroomEnter {
    NSDictionary *dic = @{@"clanId": self.toClanId};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"chatroom.enter" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        self.enterNewChatroom = [suceess[@"enterNewChatRoom"] isEqualToString:@"on"];
        [self joinRoom];
    } bFailure:^(id  _Nullable failure) {
        self.joining = NO;
        if ([failure isKindOfClass:[NSDictionary class]]) {
            if ([failure[@"code"] integerValue] == 3000) {
                self.joining = YES;
                [self showPasswordViewWithType:PasswordViewTypeUnlock];
            } else {
                [SPDCommonTool showWindowToast:failure[@"msg"]];
            }
        }
    }];
}

- (void)showPasswordViewWithType:(PasswordViewType)type {
    [[UIApplication sharedApplication].keyWindow addSubview:self.passwordView];
    self.passwordView.type = type;
    [self.passwordView show];
}

- (void)requestChatroomLockVerifyWithPassword:(NSString *)password {
    NSDictionary *dic = @{@"clanId": self.toClanId, @"password": password};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"chatroom.lock.verify" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        self.enterNewChatroom = [suceess[@"enterNewChatRoom"] isEqualToString:@"on"];
        [self joinRoom];
    } bFailure:^(id  _Nullable failure) {
        self.joining = NO;
        if ([failure isKindOfClass:[NSDictionary class]]) {
            if ([failure[@"code"] integerValue] == 2114) {
                [SPDCommonTool showWindowToast:SPDStringWithKey(@"密码错误，请再试一次", nil)];
            } else {
                [SPDCommonTool showWindowToast:failure[@"msg"]];
            }
        }
    }];
}

- (void)requestChatroomLockUseWithType:(NSString *)type password:(NSString *)password {
    NSDictionary *dic = @{@"clanId": self.clan_id, @"type": type, @"password": password};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"chatroom.lock.use" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        if ([type isEqualToString:@"lock"]) {
            self.isLock = YES;
            [SPDCommonTool showWindowToast:SPDStringWithKey(@"房间设置密码成功", nil)];
        } else {
            self.isLock = NO;
            [SPDCommonTool showWindowToast:SPDStringWithKey(@"房间解锁成功", nil)];
        }
    } bFailure:^(id  _Nullable failure) {
        if ([failure isKindOfClass:[NSDictionary class]]) {
            [SPDCommonTool showWindowToast:failure[@"msg"]];
            if ([failure[@"code"] integerValue] == 2111) {
                self.canLock = NO;
                self.isLock = NO;
            }
        }
    }];
}

- (void)joinRoom {
    [[RCIMClient sharedRCIMClient] joinChatRoom:self.toClanId messageCount:CHATROOM_MESSAGECOUNT success:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([self.fromViewController isKindOfClass:[ChatRoomLiveViewController class]]) {
                [(ChatRoomLiveViewController *)self.fromViewController reloadDataWithClanId:self.toClanId];
            } else if ([self.fromViewController isKindOfClass:[ChatroomVoiceViewController class]]) {
                [(ChatroomVoiceViewController *)self.fromViewController reloadDataWithClanId:self.toClanId];
            } else {
                if (self.enterNewChatroom) {
                    ChatroomVoiceViewController *vc = [[ChatroomVoiceViewController alloc] init];
                    vc.conversationType = ConversationType_CHATROOM;
                    vc.clan_id = self.toClanId;
                    if ([self.fromViewController isKindOfClass:[UINavigationController class]]) {
                        [(UINavigationController *)self.fromViewController pushViewController:vc animated:YES];
                    } else {
                        [self.fromViewController.navigationController pushViewController:vc animated:YES];
                    }
                } else {
                    ChatRoomLiveViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([ChatRoomLiveViewController class])];
                    vc.conversationType = ConversationType_CHATROOM;
                    vc.clan_id = self.toClanId;
                    if ([self.fromViewController isKindOfClass:[UINavigationController class]]) {
                        [(UINavigationController *)self.fromViewController pushViewController:vc animated:YES];
                    } else {
                        [self.fromViewController.navigationController pushViewController:vc animated:YES];
                    }
                }

            }
            
            NSDictionary *dic = @{@"clanId": self.toClanId};
            [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"chatroom.join" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
                
            } bFailure:^(id  _Nullable failure) {
                
            }];
        });
        self.joining = NO;
    } error:^(RCErrorCode status) {
        self.joining = NO;
        if (status == KICKED_FROM_CHATROOM) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SPDCommonTool showWindowToast:SPDStringWithKey(@"你已被踢出房间，暂时无法进入此房间", nil)];
            });
        }
    }];
}

- (void)leaveRoom {
    if (self.clan_id && self.clan_id.length) {
        [[RCIMClient sharedRCIMClient] quitChatRoom:self.clan_id success:^{

        } error:^(RCErrorCode status) {
            
        }];
        [[RCIM sharedRCIM] setDisableMessageAlertSound:NO];
        
        [self.api logoutRoom];
        
        switch (self.sceneType) {
            case SceneTypeClanRoom: {
                if (self.selfMicIndex && self.selfMicIndex.length) {
                    NSMutableDictionary *dic = [@{@"chatroom_id": self.clan_id, @"index": self.selfMicIndex, @"change": self.selfEndStatus} mutableCopy];
                    [RequestUtils commonPostRequestUtils:dic bURL:@"chatroom.change" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
                        
                    } bFailure:^(id  _Nullable failure) {
                        
                    }];
                }
                
                NSDictionary *params = @{@"clanId": self.clan_id};
                [RequestUtils commonPostRequestUtils:[params mutableCopy] bURL:@"chatroom.signout" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
                    
                } bFailure:^(id  _Nullable failure) {
                    
                }];
                break;
            }
            case SceneTypeRadioRoom: {
                [self.radioPlayer stop];
                
                NSDictionary *params = @{@"radioId": self.clan_id};
                [RequestUtils commonPostRequestUtils:[params mutableCopy] bURL:@"radio.leave" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {

                } bFailure:^(id  _Nullable failure) {

                }];
                break;
            }
            case SceneTypeLiveRoom: {
                [self.livePlayer stop];
                [self enableLoopback:NO];
                [ZegoAudioProcessing enableReverb:NO mode:0];
                [[RCIMClient sharedRCIMClient] quitChatRoom:DEV?@"liveWorldRoom_dev":@"liveWorldRoom" success:^{
                } error:^(RCErrorCode status) {
                    
                }];
                NSDictionary *params = @{@"liveId": self.clan_id};
                [RequestUtils POST:URL_NewServer(@"live/live.outto") parameters:params success:^(id  _Nullable suceess) {
                    
                } failure:^(id  _Nullable failure) {
                    
                }];
                // 麦上有我
                if (self.linkMicUserInfoDic[[SPDApiUser currentUser].userId]) {
                    LinkMicNewModel * model = self.linkMicUserInfoDic[[SPDApiUser currentUser].userId];
                    [self requestDownMic:@{@"broadcaster":self.liveInfo.userId,@"number":model.site ,@"type":@"-1",@"liveId":self.liveInfo.liveId,@"siteUserId":[SPDApiUser currentUser].userId}];
                }
                break;
            }
            default:
                break;
        }
        
        [self initProperties];
        [self initMicStatus];
        [self.micUsers removeAllObjects];
        [self.streams removeAllObjects];
        self.clanBackgroundView.hidden = YES;
    } else if (self.sceneType == SceneTypeListenRadio) {
        [self exitListenRadio];
    }
}

- (void)pauseAudio {
    if (self.clan_id && self.clan_id.length) {
        [self.api pauseAudioModule];
    }
}

- (void)resumeAudio {
    if (self.clan_id && self.clan_id.length) {
        [self.api resumeAudioModule];
    }
}

- (void)handleMagicEffect:(NSNotification *)noti {
    SPDPrivateMagicMessage *magicMessage = (SPDPrivateMagicMessage *)noti.object;
    if ([magicMessage.magic_type isEqualToString:@"kicked"]) {
        [SPDCommonTool showWindowToast:[NSString stringWithFormat:SPDStringWithKey(@"%@将您踢出房间%@s", nil), magicMessage.senderUserInfo.name, magicMessage.magic_effect_value]];
        [self leaveRoom];
        if (self.delegate && [self.delegate respondsToSelector:@selector(kickOut)]) {
            [self.delegate kickOut];
        }
    } else if ([magicMessage.magic_type isEqualToString:@"banned_mic"]) {
        if (self.selfMicIndex && self.selfMicIndex.length) {
            [self uploadAdminMessage:@"下麦" userId:[SPDApiUser currentUser].userId index:self.selfMicIndex status:self.selfEndStatus];
        }
    }
}

- (void)upMicConfirmation:(NSString *)index {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:SPDStringWithKey(@"家族管理员想邀请你上麦语音聊天", nil) preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:SPDStringWithKey(@"拒绝", nil) style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:cancleAction];
    UIAlertAction *upMicAction = [UIAlertAction actionWithTitle:SPDStringWithKey(@"接受", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self beginUseMic:[SPDApiUser currentUser].userId index:index];
    }];
    [alertController addAction:upMicAction];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
}

- (void)beginUseMic:(NSString *)userId index:(NSString *)index {
    if (!self.micUsers[index] && [self.micStatus[index] intValue] != CannotUse && [self.micStatus[index] intValue] != Freeze) {
        NSString *status;
        if ([self.micStatus[index] intValue] != Forbidden) {
            status = @"1";
        } else {
            status = @"3";
        }
        [self uploadAdminMessage:@"上麦" userId:userId index:index status:status];
    } else {
        [SPDCommonTool showWindowToast:SPDStringWithKey(@"此麦位不可用或已经有人", nil)];
    }
}

- (void)uploadAdminMessage:(NSString *)type userId:(NSString *)userId index:(NSString *)index status:(NSString *)status {
    NSMutableDictionary *dic = [@{@"chatroom_id": self.clan_id, @"index": index, @"change": status} mutableCopy];
    if (![userId isEqualToString:@"n"] && ![type isEqualToString:@"下麦"] && ![type isEqualToString:@"封麦"]) {
        [dic setValue:userId forKey:@"user_id"];
    }
    [RequestUtils commonPostRequestUtils:dic bURL:@"chatroom.change" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        [self requestMicStatus];
        [self sendAdminMessage:type userId:userId index:index];
    } bFailure:^(id  _Nullable failure) {
        [SPDCommonTool showWindowToast:SPDStringWithKey(@"操作失败，请稍后重试", nil)];
    }];
}

- (void)sendAdminMessage:(NSString *)type userId:(NSString *)userId index:(NSString *)index {
    NSString *msg = [NSString stringWithFormat:@"%@,%@,%@", type, index, userId];
    [self.api sendRoomMessage:msg type:ZEGO_TEXT category:ZEGO_SYSTEM completion:^(int errorCode, NSString *roomId, unsigned long long messageId) {
        if (errorCode != 0) {
            [SPDCommonTool showWindowToast:SPDStringWithKey(@"操作失败，请稍后重试", nil)];
        }
    }];
}

- (void)requestMicStatus {
    if (!self.clan_id) {
        return;
    }
    NSDictionary *dic = @{@"chatroom_id": self.clan_id};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"chatroom.status" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        if (self.isBackground) {
            NSDictionary *oldMicUsers = [self.micUsers copy];
            [self.micUsers removeAllObjects];
            for (NSDictionary *dic in suceess[@"micstatus"]) {
                NSString *index = [NSString stringWithFormat:@"%@", dic[@"index"]];
                if (!dic[@"status"]) {
                    [SPDCommonTool showWindowToast:@"chatroom.status return null"];
                    return ;
                } // deal crash
                [self.micStatus setValue:dic[@"status"] forKey:index];
                NSDictionary *userDic = dic[@"user"];
                if ([dic[@"status"] intValue] != 0 && [dic[@"status"] intValue] != 4 && userDic.count) {
                    SPDVoiceLiveUserModel *model = [SPDVoiceLiveUserModel initWithDictionary:userDic];
                    [self.micUsers setObject:model forKey:index];
                    if ([model._id isEqualToString:[SPDApiUser currentUser].userId]) {
                        [self.api startPublish];
                        if (!oldMicUsers[index]) {
                            [SPDCommonTool showWindowToast:SPDStringWithKey(@"您已经上麦，可以开始讲话", nil)];
                        }
                        self.selfMicIndex = index;
                        if ([_micStatus[index] intValue] == 3) {
                            self.selfEndStatus = @"3";
                            [self enableMic:NO];
                        } else {
                            self.selfEndStatus = @"0";
                            if ([_micStatus[index] intValue] == 2) {
                                [self.api enableMic:NO];
                                self.micEnabled = YES;
                                self.micSelected = NO;
                            } else {
                                [self enableMic:YES];
                            }
                        }
                    }
                } else {
                    SPDVoiceLiveUserModel *model = oldMicUsers[index];
                    if (model && [model._id isEqualToString:[SPDApiUser currentUser].userId] && ![self getUserMicIndex:model._id]) {
                        [self.api stopPublish];
                        [self enableMic:NO];
                        self.selfMicIndex = nil;
                        [self.streams removeObjectForKey:[SPDApiUser currentUser].userId];
                    }
                }
            }
        } else {
            if (self.delegate && [self.delegate respondsToSelector:@selector(micStatusDidChanged:)]) {
                [self.delegate micStatusDidChanged:suceess];
            }
        }
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

// 判断在不在麦上
- (NSString *)getUserMicIndex:(NSString *)userId {
    for (SPDVoiceLiveUserModel *micUser in self.micUsers.allValues) {
        if ([userId isEqualToString:micUser._id]) {
            NSString *index = [[self.micUsers allKeysForObject:micUser] firstObject];
            return index;
        }
    }
    return nil;
}

- (void)enableMic:(BOOL)bEnable {
    [self.api enableMic:bEnable];
    self.micEnabled = bEnable;
    self.micSelected = bEnable;
}

- (void)enableSpeaker:(BOOL)bEnable {
    [self.api enableSpeaker:bEnable];
    self.speakerEnabled = bEnable;
}

- (void)startPublish {
    [self.api startPublish];
}

- (void)startPublishWithUserId:(NSString *)userId {
    [self.api startPlayStream:[NSString stringWithFormat:@"s-%@-Famy",userId]];
}

- (void)stopPublish {
    [self.api stopPublish];
}

#pragma mark - ZegoAudioIMDelegate

- (void)onRecvAudioRoomMessage:(NSString *)roomId messageList:(NSArray<ZegoRoomMessage *> *)messageList {
    BOOL isMicStatusChanged = NO;
    for (ZegoRoomMessage *message in messageList) {
        NSString *msg = message.content;
        NSArray *array = [msg componentsSeparatedByString:@","];
        NSString *type = array[0];
        NSString *index = array[1];
        NSString *userId = array[2];
        if ([userId isEqualToString:[SPDApiUser currentUser].userId]) {
            if ([type isEqualToString:@"踢人"]) {
                [self leaveRoom];
                if (self.delegate && [self.delegate respondsToSelector:@selector(kickOut)]) {
                    [self.delegate kickOut];
                }
            } else if ([type isEqualToString:@"帮访客上麦"]) {
                [self upMicConfirmation:index];
            } else {
                isMicStatusChanged = YES;
            }
        } else {
            if (![type isEqualToString:@"踢人"] && ![type isEqualToString:@"帮访客上麦"]) {
                isMicStatusChanged = YES;
            }
        }
    }
    if (isMicStatusChanged) {
        [self requestMicStatus];
    }
}

- (void)onUpdateOnlineCount:(int)onlineCount room:(NSString *)roomId {
    if (self.sceneType == SceneTypeClanRoom && !self.uploadedCount && [roomId isEqualToString:self.clan_id]) {
        NSDictionary *params = @{@"radioId": self.clan_id, @"members": @(onlineCount)};
        [RequestUtils commonPostRequestUtils:[params mutableCopy] bURL:@"radio.enter" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
            self.uploadedCount = YES;
        } bFailure:^(id  _Nullable failure) {
            
        }];
    }
}

- (void)onRecvBigAudioRoomMessage:(NSString *)roomId messageList:(NSArray<ZegoBigRoomMessage *> *)messageList {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([roomId isEqualToString:self.clan_id]) {
            for (ZegoBigRoomMessage *message in messageList) {
                if ([message.content hasPrefix:@"isOpenConnMic:"]) {
                    self.linkMicSwitch = [message.content substringFromIndex:14].boolValue;
                    [self stopLinkMic];
                }
            }
        }
    });
}

#pragma mark - ZegoAudioRoomDelegate

- (void)onKickOut:(int)reason roomID:(NSString *)roomID customReason:(NSString *)customReason {
    if ([roomID isEqualToString:self.clan_id]) {
        [self leaveRoom];
        [self showTips:SPDLocalizedString(@"你已被请出房间")];
        if ([self.delegate respondsToSelector:@selector(didLeaveRoom)]) {
            [self.delegate didLeaveRoom];
        }
    }
}

- (void)onDisconnect:(int)errorCode roomID:(NSString *)roomID {
    if ([roomID isEqualToString:self.clan_id]) {
        [self leaveRoom];
        [self showTips:SPDLocalizedString(@"网络连接已断开,请重新进入")];
        if ([self.delegate respondsToSelector:@selector(didLeaveRoom)]) {
            [self.delegate didLeaveRoom];
        }
    }
}

- (void)onStreamUpdated:(ZegoAudioStreamType)type stream:(ZegoAudioStream*)stream {
    if (type == ZEGO_AUDIO_STREAM_ADD) {
        [self.streams setObject:stream forKey:stream.userID];
        switch (self.sceneType) {
            case SceneTypeLiveRoom:
                [self.api startPlayStream:stream.streamID];
                if (self.isAnchor) {
                    if (!self.linkMicLinkingDic[stream.userID]) {
                        [self requestUserBatchWithUserId:stream.userID isApply:NO];
                    }
                } else {
                    if ([stream.userID isEqualToString:self.liveInfo.userId]) {
                        if ([self.delegate respondsToSelector:@selector(liveDidStart)]) {
                            [self.delegate liveDidStart];
                        }
                    } else {
                        [self requestUserBatchWithUserId:stream.userID isApply:NO];
                    }
                }
                break;
            default:
                break;
        }
    } else {
        [self.streams removeObjectForKey:stream.userID];
        switch (self.sceneType) {
            case SceneTypeLiveRoom:
                [self.linkMicLinkingDic removeObjectForKey:stream.userID];
                [[NSNotificationCenter defaultCenter] postNotificationName:LinkMicUserDidChangeNotification object:nil];
                break;
            default:
                break;
        }
    }
}

- (void)onReceiveCustomCommand:(NSString *)fromUserID userName:(NSString *)fromUserName content:(NSString*)content roomID:(NSString *)roomID {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([roomID isEqualToString:self.clan_id]) {
            switch (self.sceneType) {
                case SceneTypeLiveRoom:
                    if (self.isAnchor) {
                        if (self.linkMicSwitch) {
                            if ([content isEqualToString:@"apply"]) {
//                                [self requestUserBatchWithUserId:fromUserID  isApply:YES];
                                // 兼容老版本 先走老版本同意的代码
                                [ZegoManager sendCustomCommandToUser:fromUserID content:@"agree"];
                                // 新版本主播 帮他上麦
                                [self requestLiveUpMicChange:@{@"broadcaster":self.liveInfo.userId,@"number":@"-1",@"type":@"2",@"liveId":self.liveInfo.liveId,@"siteUserId":fromUserID}];
                                
                            } else {
                                if ([content isEqualToString:@"cancel"]) {
                                    [self.linkMicApplyDic removeObjectForKey:fromUserID];
                                    [self.linkMicUnreadDic removeObjectForKey:fromUserID];
                                } else if ([content isEqualToString:@"agree"]) {
                                    if (self.linkMicLinkingDic.count < 5) {
                                        LinkMicModel *model = self.linkMicInvitingDic[fromUserID];
                                        model.startTimeStamp = [[NSDate date] timeIntervalSince1970];
                                        [self.linkMicLinkingDic setValue:model forKey:fromUserID];
                                        [self.linkMicInvitingDic removeObjectForKey:fromUserID];
                                    } else {
                                        [self sendCustomCommandToUser:fromUserID content:@"handUp"];
                                    }
                                } else if ([content isEqualToString:@"handUp"]) {
                                    [self.linkMicLinkingDic removeObjectForKey:fromUserID];
                                    // 老版本的会发hangup消息 主播帮他请求下麦接口
#warning to do
                                } else if ([content isEqualToString:@"refuse"]) {
                                    [self showTips:SPDLocalizedString(@"已拒绝")];
                                }  else {
                                    return;
                                }
                                [[NSNotificationCenter defaultCenter] postNotificationName:LinkMicUserDidChangeNotification object:nil];
                            }
                        }
                    } else {
                        if ([content isEqualToString:@"invite"]) {
                            // 先判断我在不在麦上 以及有没有人已经先x邀请我了
                            if (!self.linkMicUserInfoDic[[SPDApiUser currentUser].userId] && self.linkMicState != LinkMicStateIncoming) {
                                self.linkMicState = LinkMicStateIncoming;
                                [self requestUserInfo:fromUserID];
                            }
                        } else if ([content isEqualToString:@"agree"]) {
                            [self startLinkMic];
                        } else if ([content isEqualToString:@"refuse"]) {
                            self.linkMicState = LinkMicStateNormal;
                            [self showTips:SPDLocalizedString(@"已拒绝")];
                        } else if ([content isEqualToString:@"handUp"]) {
                            [self stopLinkMic];
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    });
}

- (void)requestUserInfo:(NSString *)uId {
    [RequestUtils GET:URL_Server(@"user.info") parameters:@{@"user_id":uId} success:^(id  _Nullable suceess) {
        __weak typeof(self) ws = self;
        LinkMicReceiveApplyMsgView * alert = [LinkMicReceiveApplyMsgView new];
        alert.name = suceess[@"nick_name"];
        alert.avatarUrl = suceess[@"avatar"];
        [alert addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"取消") style:LYPSystemAlertActionStyleCancel handler:^(LYPSystemAlertAction * _Nonnull action) {
            
        }]];
        [alert addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"确定") style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
            [ws requestLiveUpMicChange:@{@"broadcaster":self.liveInfo.userId,@"number":@"-1",@"type":@"0",@"liveId":self.liveInfo.liveId}];
        }]];
        [alert present];
    } failure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestUserBatchWithUserId:(NSString *)userId isApply:(BOOL)isApply {
    NSDictionary *params = @{@"user_ids": [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:@[userId] options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding]};
    [RequestUtils commonGetRequestUtils:[params mutableCopy] bURL:@"user.batch" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        NSArray *user = suceess[@"users"];
        if (user.count) {
            LinkMicModel *model = [LinkMicModel initWithDictionary:user.firstObject];
            if (self.isAnchor && isApply) {
                [self.linkMicApplyDic setValue:model forKey:userId];
                [self.linkMicUnreadDic setValue:model forKey:userId];
            } else {
                model.startTimeStamp = [[NSDate date] timeIntervalSince1970];
                [self.linkMicLinkingDic setValue:model forKey:userId];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:LinkMicUserDidChangeNotification object:nil];
        }
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

#pragma mark - ZegoAudioLivePublisherDelegate

- (void)onPublishStateUpdate:(int)stateCode streamID:(NSString *)streamID streamInfo:(NSDictionary *)info {
    if (stateCode == 0) {
        ZegoAudioStream *stream = [[ZegoAudioStream alloc] init];
        stream.streamID = streamID;
        stream.userID = [SPDApiUser currentUser].userId;
        [self.streams setObject:stream forKey:stream.userID];
        
        switch (self.sceneType) {
            case SceneTypeLiveRoom:
                if (!self.isAnchor) {
                    LinkMicModel *model = [LinkMicModel new];
                    model._id = [SPDApiUser currentUser].userId;
                    model.avatar = [SPDApiUser currentUser].avatar;
                    LY_Portrait *portrait = [[LY_Portrait alloc] init];
                    portrait.url = [LY_User currentUser].portrait;
                    portrait.headwearUrl = [LY_User currentUser].headwearWebp;
                    model.portrait = portrait;
                    model.nick_name = [SPDApiUser currentUser].nickName;
                    model.startTimeStamp = [[NSDate date] timeIntervalSince1970];
                    [self.linkMicLinkingDic setValue:model forKey:model._id];
                    [[NSNotificationCenter defaultCenter] postNotificationName:LinkMicUserDidChangeNotification object:nil];
                }
                break;
            default:
                break;
        }
    }
}

#pragma mark - Check Streams

- (void)startCheckStreams {
    if (!self.streamsTimer && self.clan_id) {
        self.streamsTimer = [NSTimer timerWithTimeInterval:90 target:self selector:@selector(checkStreams) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:self.streamsTimer forMode:NSRunLoopCommonModes];
        [self.streamsTimer fire];
    }
}

- (void)checkStreams {
    for (NSString *userId in self.streams.allKeys) {
        if (![self getUserMicIndex:userId]) {
            [self kickOutUser:userId];
        }
    }
}

- (void)kickOutUser:(NSString *)userId {
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObject:self.clan_id forKey:@"chatroom_id"];
    NSMutableArray *userIds = [NSMutableArray arrayWithObject:userId];
    [dict setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:userIds options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding] forKey:@"user_ids"];
    [RequestUtils commonPostRequestUtils:dict bURL:@"chatroom.zego.kickout" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {

    } bFailure:^(id  _Nullable failure) {
        
    }];
}

#pragma mark - SPDChatroomPasswordViewDelegate

- (void)didClickConfirmBtnWithType:(PasswordViewType)type password:(NSString *)password {
    if (type == PasswordViewTypeUnlock) {
        [self requestChatroomLockVerifyWithPassword:password];
    } else {
        [self requestChatroomLockUseWithType:@"lock" password:password];
    }
}

- (void)didClickCancelBtnWithType:(PasswordViewType)type {
    if (type == PasswordViewTypeUnlock) {
        self.joining = NO;
    }
}

#pragma mark - RadioRoom & ListenRadio

- (void)startPlayRadio:(NSString *)radio {
    [self.radioPlayer start:radio repeat:NO];
}

- (void)muteRadio:(BOOL)mute {
    [self.radioPlayer muteLocal:mute];
    self.speakerEnabled = !mute;
}

- (void)stopPlayRadio {
    [self.radioPlayer stop];
}

- (void)exitListenRadio {
    self.isBackground = NO;
    self.sceneType = SceneTypeNone;
    [self stopPlayRadio];
    [TimerPowerOffMgr tearDown];
}

#pragma mark - LiveRoom

- (void)joinLiveRoomWithliveInfo:(LiveModel *)liveInfo isAnchor:(BOOL)isAnchor {
    if (!liveInfo.liveId.notEmpty || self.joining || (self.sceneType == SceneTypeLiveRoom && self.isAnchor)) {
        return;
    }
    self.joining = YES;
    
    int messageCount = -1;
    if (self.sceneType != SceneTypeLiveRoom || ![self.clan_id isEqualToString:liveInfo.liveId]) {
        [self leaveRoom];
    } else {
        [[RCIMClient sharedRCIMClient] quitChatRoom:self.clan_id success:^{
            
        } error:^(RCErrorCode status) {
                
        }];
        messageCount = 20;
    }
    
    self.liveInfo = liveInfo;
    self.isAnchor = isAnchor;
    [[RCIMClient sharedRCIMClient] joinChatRoom:self.liveInfo.liveId messageCount:messageCount success:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (![[SPDCommonTool getWindowTopViewController] isKindOfClass:[LiveRoomViewController class]]) {
                LiveRoomViewController *vc = [LiveRoomViewController new];
                vc.liveInfo = self.liveInfo;
                vc.isAnchor = self.isAnchor;
                UITabBarController *tabBarController = (UITabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
                [(UINavigationController *)tabBarController.selectedViewController pushViewController:vc animated:YES];
            }
            
            if (self.sceneType != SceneTypeLiveRoom || ![self.clan_id isEqualToString:self.liveInfo.liveId] || [[SPDCommonTool getWindowTopViewController] isKindOfClass:[LiveRoomViewController class]]) {
                // [[SPDCommonTool getWindowTopViewController] isKindOfClass:[LiveRoomViewController class]] 代表在直播间跳转直播间
                NSDictionary *params = @{@"liveId": self.liveInfo.liveId};
                [RequestUtils POST:URL_NewServer(@"live/live.into") parameters:params success:^(id  _Nullable suceess) {
                    
                } failure:^(id  _Nullable failure) {
                    
                }];
            }
            self.joining = NO;
        });
    } error:^(RCErrorCode status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (status == KICKED_FROM_CHATROOM) {
                [self showTips:SPDLocalizedString(@"你已被踢出房间，暂时无法进入此房间")];
            }
            self.joining = NO;
        });
    }];
    
    // join 直播间世界
    [[RCIMClient sharedRCIMClient] joinChatRoom:DEV?@"liveWorldRoom_dev":@"liveWorldRoom" messageCount:0 success:^{
        NSLog(@"加入世界房间成功");
    } error:^(RCErrorCode status) {
        
    }];
}

- (void)playBackgroundMusic:(NSString *)music {
    [self.livePlayer start:music repeat:YES];
}

- (void)pauseBackgroundMusic {
    [self.livePlayer pause];
}

- (void)resumeBackgroundMusic {
    [self.livePlayer resume];
}

- (void)stopBackgroundMusic {
    [self.livePlayer stop];
}

- (void)playAudioEffect:(NSString *)effect {
    [self.audioEffectPlayer stop];
    [self.audioEffectPlayer start:effect repeat:NO];
}

- (void)enableLoopback:(BOOL)bEnable {
    [self.api setLoopbackVolume:100];
    [self.api enableLoopback:bEnable];
}

- (void)sendCustomCommandToUser:(NSString *)userId content:(NSString *)content {
    ZegoUser *user = [[ZegoUser alloc] init];
    user.userId = userId;
    user.userName = userId;
    [self.api sendCustomCommand:@[user] content:content completion:^(int errorCode, NSString *roomID) {
        
    }];
}

- (void)sendBigRoomMessage:(NSString *)message {
    [self.api sendBigRoomMessage:message type:ZEGO_TEXT category:ZEGO_SYSTEM completion:^(int errorCode, NSString *roomId, NSString *messageId) {
        
    }];
}

- (void)startLinkMic {
    [self startPublish];
    [self enableMic:YES];
    self.linkMicStartTimeStamp = [[NSDate date] timeIntervalSince1970];
    self.linkMicState = LinkMicStateLinking;
    [self startLinkMicTimer];
}

- (void)stopLinkMic {
    [self stopPublish];
    [self enableMic:NO];
    [self enableSpeaker:YES];
    [self.linkMicLinkingDic removeObjectForKey:[SPDApiUser currentUser].userId];
    [[NSNotificationCenter defaultCenter] postNotificationName:LinkMicUserDidChangeNotification object:nil];
    self.linkMicState = LinkMicStateNormal;
    [self.linkMicStreamTimer invalidate];
    self.linkMicStreamTimer = nil;
}

// 上麦
- (void)requestLiveUpMicChange:(NSDictionary *)dict {
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    [RequestUtils POST:URL_NewServer(@"live/live.site.save") parameters:dict success:^(id  _Nullable suceess) {
        [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
        [self startLinkMic];
    } failure:^(id  _Nullable failure) {
        [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
    }];
}

- (void)requestDownMic:(NSDictionary *)dict {
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    [RequestUtils POST:URL_NewServer(@"live/live.site.save") parameters:dict success:^(id  _Nullable suceess) {
        [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
        if (self.isAnchor) {
            [self sendCustomCommandToUser:dict[@"siteUserId"] content:@"handUp"];
            [self.linkMicLinkingDic removeObjectForKey:dict[@"siteUserId"]];
            [self.linkMicUserInfoDic removeObjectForKey:dict[@"siteUserId"]];
            [[NSNotificationCenter defaultCenter] postNotificationName:LinkMicUserDidChangeNotification object:nil];
        }else{
            [self stopLinkMic];
        }
    } failure:^(id  _Nullable failure) {
        [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
    }];
}

- (void)startLinkMicTimer {
    if (!self.linkMicStreamTimer) {
        self.linkMicStreamTimer = [NSTimer timerWithTimeInterval:30 target:self selector:@selector(handleLinkMicStreamTimer:) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:self.linkMicStreamTimer forMode:NSRunLoopCommonModes];
        [self.linkMicStreamTimer fire];
    }
}

- (void)handleLinkMicStreamTimer:(NSTimer *)timer {
    [RequestUtils GET:URL_NewServer(@"live/live.user.heartbeat") parameters:@{@"broadcaster":self.liveInfo.userId,@"steamId":[SPDApiUser currentUser].userId,@"liveId":self.liveInfo.liveId} success:^(id  _Nullable suceess) {
    } failure:^(id  _Nullable failure) {
        [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
    }];
}

- (void)resetLiveRoomProperties {
    self.isAnchor = NO;
    self.liveInfo = nil;
    self.linkMicSwitch = NO;
    self.linkMicFreeUpMicSwitch = NO;
    self.linkMicState = LinkMicStateNormal;
    [self.linkMicLinkingDic removeAllObjects];
    [self.linkMicApplyDic removeAllObjects];
    [self.linkMicInvitingDic removeAllObjects];
    if (self.linkMicStreamTimer) {
        [self.linkMicStreamTimer invalidate];
        self.linkMicStreamTimer = nil;
    }
}

- (void)setLinkMicSwitch:(BOOL)linkMicSwitch {
    if (_linkMicSwitch != linkMicSwitch) {
        _linkMicSwitch = linkMicSwitch;
        [[NSNotificationCenter defaultCenter] postNotificationName:LinkMicSwitchDidChangeNotification object:nil];
    }
}

- (void)setLinkMicFreeUpMicSwitch:(BOOL)linkMicFreeUpMicSwitch {
    if (_linkMicFreeUpMicSwitch != linkMicFreeUpMicSwitch) {
        _linkMicFreeUpMicSwitch = linkMicFreeUpMicSwitch;
    }
}

- (void)setLinkMicState:(LinkMicState)linkMicState {
    if (_linkMicState != linkMicState) {
        _linkMicState = linkMicState;
        [[NSNotificationCenter defaultCenter] postNotificationName:LinkMicStateDidChangeNotification object:@(_linkMicState)];
    }
}

- (NSMutableDictionary *)linkMicLinkingDic {
    if (!_linkMicLinkingDic) {
        _linkMicLinkingDic = [NSMutableDictionary new];
    }
    return _linkMicLinkingDic;
}

- (NSMutableDictionary *)linkMicApplyDic {
    if (!_linkMicApplyDic) {
        _linkMicApplyDic = [NSMutableDictionary new];
    }
    return _linkMicApplyDic;
}

- (NSMutableDictionary *)linkMicUnreadDic{
    if (!_linkMicUnreadDic) {
        _linkMicUnreadDic = [NSMutableDictionary new];
    }
    return _linkMicUnreadDic;
}

- (NSMutableDictionary *)linkMicInvitingDic {
    if (!_linkMicInvitingDic) {
        _linkMicInvitingDic = [NSMutableDictionary new];
    }
    return _linkMicInvitingDic;
}

#pragma mark - Setters & Getters

- (void)setPopIndex:(NSInteger)popIndex {
    if (!_popIndex || popIndex == 0) {
        _popIndex = popIndex;
    }
}

- (void)setCanLock:(BOOL)canLock {
    if (self.clan_id && self.clan_id.length) {
        _canLock = canLock;
    }
}

- (void)setIsBackground:(BOOL)isBackground {
    _isBackground = isBackground;
    
    switch (self.sceneType) {
        case SceneTypeRadioRoom:
        case SceneTypeListenRadio:
            if (_isBackground) {
                self.radioBackgroundRunView.radioId = self.clan_id;
                self.radioBackgroundRunView.radioAvatar = self.clan_cover;
                [[UIApplication sharedApplication].keyWindow addSubview:self.radioBackgroundRunView];
            } else {
                [self.radioBackgroundRunView removeFromSuperview];
            }
            break;
        case SceneTypeLiveRoom:
            if (_isBackground) {
                self.radioBackgroundRunView.liveInfo = self.liveInfo;
                [[UIApplication sharedApplication].keyWindow addSubview:self.radioBackgroundRunView];
            } else {
                [self.radioBackgroundRunView removeFromSuperview];
            }
            break;
        default:
            break;
    }
}

- (NSMutableDictionary *)micStatus {
    if (!_micStatus) {
        _micStatus = [NSMutableDictionary dictionary];
        [self initMicStatus];
    }
    return _micStatus;
}

- (NSMutableDictionary *)micUsers {
    if (!_micUsers) {
        _micUsers = [NSMutableDictionary dictionary];
    }
    return _micUsers;
}

- (NSMutableDictionary *)streams {
    if (!_streams) {
        _streams = [NSMutableDictionary dictionary];
    }
    return _streams;
}

- (SPDChatroomPasswordView *)passwordView {
    if (!_passwordView) {
        _passwordView = [[[NSBundle mainBundle] loadNibNamed:@"SPDChatroomPasswordView" owner:self options:nil] firstObject];
        _passwordView.frame = CGRectMake(0, 0, kScreenW, kScreenH);
        _passwordView.delegate = self;
    }
    return _passwordView;
}

- (ZegoMediaPlayer *)radioPlayer {
    if (!_radioPlayer) {
        _radioPlayer = [[ZegoMediaPlayer alloc] initWithPlayerType:MediaPlayerTypePlayer];
    }
    return _radioPlayer;
}

- (RadioRoomBackgroundRunView *)radioBackgroundRunView {
    if (!_radioBackgroundRunView) {
        _radioBackgroundRunView = [[NSBundle mainBundle] loadNibNamed:@"RadioRoomBackgroundRunView" owner:self options:nil].firstObject;
        _radioBackgroundRunView.frame = CGRectMake(kScreenW - 15.5 - 67, kScreenH - TabBarHeight - 16 - 67, 67, 67);
    }
    return _radioBackgroundRunView;
}

- (ZegoMediaPlayer *)livePlayer {
    if (!_livePlayer) {
        _livePlayer = [[ZegoMediaPlayer alloc] initWithPlayerType:MediaPlayerTypeAux];
    }
    return _livePlayer;
}

- (ZegoMediaPlayer *)audioEffectPlayer {
    if (!_audioEffectPlayer) {
        _audioEffectPlayer = [[ZegoMediaPlayer alloc] initWithPlayerType:MediaPlayerTypeAux playerIndex:ZegoMediaPlayerIndexSecond];
        [_audioEffectPlayer setVolume:65];
    }
    return _audioEffectPlayer;
}

- (NSMutableDictionary *)linkMicUserInfoDic {
    if (!_linkMicUserInfoDic) {
        _linkMicUserInfoDic = [NSMutableDictionary new];
    }
    return _linkMicUserInfoDic;
}

@end
