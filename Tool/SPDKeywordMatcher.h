//
//  SPDKeywordMatcher.h
//  SimpleDate
//
//  Created by Monkey on 2017/8/15.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KeywordMap : NSObject
@property (assign, nonatomic) UInt8 value;
@property (strong, nonatomic) NSMutableArray *subMaps;
@end


@interface SPDKeywordMatcher : NSObject

- (BOOL)match:(NSString *)text withKeywordMap:(KeywordMap *)keywordMap;

- (KeywordMap *)convert:(NSArray *)keywordArray;

- (NSArray *)backToArray:(KeywordMap *)keywordMap;

@end
