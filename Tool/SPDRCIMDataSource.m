//
//  SPDRCIMDataSource.m
//  SimpleDate
//
//  Created by 侯玲 on 17/1/4.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDRCIMDataSource.h"
#import "SPDApiUser.h"
#import "PresentAnimView.h"
#import "PresentMessage.h"
#import "SPDConversationViewController.h"
#import "ZegoKitManager.h"
#import "SPDPrivateMagicMessage.h"
#import "SPDChatRoomMagicEffectView.h"
#import "SPDTopBarView.h"
#import "SPDWorldChatMessage.h"
#import "SPDChatRoomSendPresentMessage.h"
#import "SPDCRSendMagicMessage.h"
#import "SPDTextMessage.h"
#import "SPDJoinChatRoomMessage.h"
#import "SPDBannedSpeakMessage.h"
#import "SPDFriendMessage.h"
#import "KKGiftGivingView.h"
#import "HomeModel.h"
#import "PresentAnimView.h"
#import "ZegoKitManager.h"
#import "SPDJoinChatRoomMessage.h"
#import "SPDBannedSpeakMessage.h"
#import "SPDWorldChatMessage.h"
#import "SPDChatRoomSendPresentMessage.h"
#import "SPDCRSendMagicMessage.h"
#import "SPDTextMessage.h"
#import "SPDClanCallMessage.h"
#import "SPDSignInView.h"
#import "FDAlertView.h"
#import "SPDPrivateMagicMessage.h"
#import "SPDLaunchImageView.h"
#import "SPDTipsMessage.h"
#import "SPDFriendMessage.h"
#import "SPDLotteryMessage.h"
#import "SPDGiftDoubleRewardMessage.h"
#import "SPDChatroomRedPacketMessage.h"
#import "SPDNobleGreetingMessage.h"
#import "SPDClanInviteMessage.h"
#import "SPDLockExpireMessage.h"
#import "SPDChatRoomEmojiMessage.h"
#import "ChatroomGiftComboMessage.h"
#import "LiveRoomTextMessage.h"
#import "LiveGiftMessage.h"
#import "LiveJoinMessage.h"
#import "LiveBeginNotificationMessage.h"
#import "LiveUserUpdateMessage.h"
#import "LiveFollowMessage.h"
#import "LiveImageMessage.h"
#import "LiveRoomViewController.h"
#import "LY_LiveFansInviteMessage.h"
#import "LY_LiveFansLevelMessage.h"
#import "LY_LiveFollowedMessage.h"
#import "LiveNormalBarrageMessage.h"
#import "LiveFansBarrageMessage.h"
#import "LiveStateMessage.h"
#import "LiveWealthLevelMessage.h"
#import "LivePKInviteMessage.h"
#import "LivePKInviteRefuseMessage.h"
#import "LivePKInviteBusyMessage.h"
#import "LivePKBeginMessage.h"
#import "LivePKTimeMessage.h"
#import "LivePKScoreMessage.h"
#import "PKCheckBeheadingMsg.h"
#import "PKResultMessage.h"
#import "LiveWorldMessage.h"
#import "LiveApplyMicMessage.h"
#import "LiveLinkMicAgreeMessage.h"
#import "LiveLinkMicSilenceMsg.h"
#import "LiveCancelInvitePk.h"
#import "LiveChangeManagerMessage.h"
#import "LiveLuckyNumberMessage.h"
#import "LiveRedPackMessage.h"

//聊天室消息
NSString *const SPDLiveKitChatRoomMessageNotification = @"SPDLiveKitChatRoomMessageNotification";
NSString *const SPDLiveKitChatRoomGlobalNotification = @"SPDLiveKitChatRoomGlobalNotification";

NSString *const SPDLiveCallWillConnect = @"SPDLiveCallWillConnect";
NSString *const SPDLiveCallWillDisConnect = @"SPDLiveCallWillDisConnect";
NSString *const SPDLivedidJYReceiveCall = @"SPDdidJYReceiveCall";

@interface SPDRCIMDataSource ()<SPDTopBarViewDelegate,KKGiftGivingViewDelegate>

@property (nonatomic, strong) PresentAnimView *annimationView;
@property (nonatomic, strong) SPDChatRoomMagicEffectView *magicAnimationView; //魔法的动效
@property (nonatomic, strong) BaseViewController *baseView;
@property (nonatomic, strong) NSTimer *timer;

@end

@implementation SPDRCIMDataSource

+(SPDRCIMDataSource *)shareInstance{
    
    static SPDRCIMDataSource *instance=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance=[[[self class] alloc]init];
    });
    return instance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [[RCIMClient sharedRCIMClient] setServerInfo:NaviServer fileServer:FileServer];
        [[RCIM sharedRCIM] initWithAppKey:RONGIMAPPKEY];
        // 注册自定义测试消息
        [[RCIM sharedRCIM] registerMessageType:[PresentMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[SPDTipsMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[SPDJoinChatRoomMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[SPDBannedSpeakMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[SPDWorldChatMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[SPDChatRoomSendPresentMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[SPDTextMessage  class]];
        [[RCIM sharedRCIM] registerMessageType:[SPDClanCallMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[SPDCRSendMagicMessage class]]; //magic 的消息
        [[RCIM sharedRCIM] registerMessageType:[SPDPrivateMagicMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[SPDFriendMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[SPDLotteryMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[SPDGiftDoubleRewardMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[SPDChatroomRedPacketMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[SPDNobleGreetingMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[SPDClanInviteMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[SPDLockExpireMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[SPDChatRoomEmojiMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[ChatroomGiftComboMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LiveRoomTextMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LiveJoinMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LiveGiftMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LiveBeginNotificationMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LiveUserUpdateMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LiveFollowMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LiveImageMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LY_LiveFansInviteMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LY_LiveFansLevelMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LY_LiveFollowedMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LiveFansBarrageMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LiveNormalBarrageMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LiveStateMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LiveWealthLevelMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LivePKInviteMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LivePKInviteRefuseMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LivePKInviteBusyMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LivePKBeginMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LivePKTimeMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LivePKScoreMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[PKCheckBeheadingMsg class]];
        [[RCIM sharedRCIM] registerMessageType:[PKResultMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LiveWorldMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LiveApplyMicMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LiveLinkMicAgreeMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LiveLinkMicSilenceMsg class]];
        [[RCIM sharedRCIM] registerMessageType:[LiveCancelInvitePk class]];
        [[RCIM sharedRCIM] registerMessageType:[LiveChangeManagerMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LiveLuckyNumberMessage class]];
        [[RCIM sharedRCIM] registerMessageType:[LiveRedPackMessage class]];
        //设置信息代理
        [[RCIM sharedRCIM] setUserInfoDataSource:self];
        [[RCIM sharedRCIM] setReceiveMessageDelegate:self];
        [[RCIM sharedRCIM] registerMessageType:[RCTextMessage class]];

        [[RCCall sharedRCCall] setJyCallSessionDelegate:self];
        //设置RCIM消息监听
        [[RCIM sharedRCIM] setReceiveMessageDelegate:self];
        [RCIM sharedRCIM].enableTypingStatus          = YES;
        [RCIM sharedRCIM].enablePersistentUserInfoCache = YES;
        [RCIM sharedRCIM].enableMessageAttachUserInfo = YES;
        //开启已读回执
        [RCIM sharedRCIM].enabledReadReceiptConversationTypeList = @[@(ConversationType_PRIVATE),@(ConversationType_DISCUSSION),@(ConversationType_GROUP)];
        [RCIM sharedRCIM].showUnkownMessage = YES;
        [[RCCallClient sharedRCCallClient] setVideoProfile:RC_VIDEO_PROFILE_480P];
    }
    return self;
}

#pragma mark - login

- (void)logIn {
//    NSString * token = [[SPDApiUser currentUser].userId isEqualToString:@"664"] ? @"zFl8LJsTQVstwplcHZiFqpHz++0ZTupHfoG3hLJB+WE=@d94m.sg.rongnav.com;d94m.sg.rongcfg.com":@"dT3E/FuPo8GhnV47z/0kDKwUTxDcbHIpEzJs2fx/8Kk=@d94m.sg.rongnav.com;d94m.sg.rongcfg.com";
    [[RCIM sharedRCIM] connectWithToken:[SPDApiUser currentUser].ryToken timeLimit:60 dbOpened:^(RCDBErrorCode code) {
    } success:^(NSString *userId) {
        [[RCIM sharedRCIM] setCurrentUserInfo:[[RCUserInfo alloc] initWithUserId:[SPDApiUser currentUser].userId name:[SPDApiUser currentUser].nickName portrait:[NSString stringWithFormat:STATIC_DOMAIN_URL, [SPDApiUser currentUser].avatar]]];
        [[RCIMClient sharedRCIMClient] joinChatRoom:GLOBALCHATROOM messageCount:1 success:^{
            //关闭世界消息的提示音
            [[RCIMClient sharedRCIMClient] setConversationNotificationStatus:ConversationType_CHATROOM targetId:GLOBALCHATROOM isBlocked:YES success:^(RCConversationNotificationStatus nStatus) {
            } error:^(RCErrorCode status) {

            }];

        } error:^(RCErrorCode status) {

        }];
    } error:^(RCConnectErrorCode errorCode) {
        
    }];
    
}

- (BaseViewController *)baseView {
    if (!_baseView) {
        _baseView = [BaseViewController new];
    }
    return _baseView;
}

#pragma mark - RCIMUserInfoDataSourceDelegate
/*!
 获取用户信息
 
 @param userId      用户ID
 @param completion  获取用户信息完成之后需要执行的Block [userInfo:该用户ID对应的用户信息]
 
 @discussion SDK通过此方法获取用户信息并显示，请在completion中返回该用户ID对应的用户信息。
 在您设置了用户信息提供者之后，SDK在需要显示用户信息的时候，会调用此方法，向您请求用户信息用于显示。
 */
- (void)getUserInfoWithUserId:(NSString *)userId
                   completion:(void (^)(RCUserInfo *userInfo))completion{
//    
    RCUserInfo *user = [RCUserInfo new];
    if (userId == nil || [userId length] == 0) {
        user.userId = userId;
        user.portraitUri = @"";
        user.name = @"";
        completion(user);
        return;
    }
    
    if ([userId isEqualToString:[RCIM sharedRCIM].currentUserInfo.userId]) {
        
        SPDApiUser *userSelf=[SPDApiUser currentUser];
        user.name=userSelf.nickName;
        user.userId=userId;
        user.portraitUri=[NSString stringWithFormat:STATIC_DOMAIN_URL,userSelf.avatar];
        completion(user);
        
    }else{
        NSDictionary *dict=@{@"user_id":userId};

        [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dict] bURL:@"user.detail" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
            
            RCUserInfo *user = [[RCUserInfo alloc] init];
            user.userId = suceess[@"_id"];
            user.name = suceess[@"nick_name"];
            user.portraitUri = [NSString stringWithFormat:STATIC_DOMAIN_URL, suceess[@"avatar"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(user);
            });
            
        } bFailure:nil];
        
    }
    
}

#pragma mark -RCIM监听接受消息的方法

- (void)onRCIMReceiveMessage:(RCMessage *)message left:(int)left {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (message.conversationType == ConversationType_PRIVATE) {
            [self handlePrivateMessage:message];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateMessageBadgeValueNotification" object:nil];
        } else if (message.conversationType == ConversationType_CHATROOM) {
            if ([message.targetId  isEqualToString:GLOBALCHATROOM]) {
                self.lastGlobalMessage = message.content;
                [[NSNotificationCenter defaultCenter] postNotificationName:SPDLiveKitChatRoomGlobalNotification object:message userInfo:@{@"left": @(left)}];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:SPDLiveKitChatRoomMessageNotification object:message userInfo:@{ @"left": @(left)}];
            }
        }
    });
}

-(void)handlePrivateMessage:(RCMessage *)message {
    
    if ([message.content isKindOfClass:[PresentMessage class]]) {
        if (isVideoStatus && [[RCCall sharedRCCall].currentCallSession.targetId isEqualToString:message.content.senderUserInfo.userId]) {
            [self popAnimationViewWithMessage:message];
        }
        //每次受到礼物 或者是座驾 都要check 一下有没有升级
        [[SPDCommonTool shareTool] checkExpToWhetherToUpgrage];
        
    }else if ([message.content isKindOfClass:[RCImageMessage class]]){
        
        RCImageMessage *imgMessage = (RCImageMessage *)message.content;
        NSData *data=[[NSData alloc]initWithContentsOfURL:[NSURL URLWithString:imgMessage.imageUrl]];
        UIImage *msgImage = [UIImage imageWithData:data];
        [SPDCommonTool beginCheakImageWithImage:msgImage andIsYellowPicture:^(BOOL isYellowPicture) {
            if (isYellowPicture) {
                [[RCIMClient sharedRCIMClient] deleteMessages:@[@(message.messageId)]];
                NSDictionary * dict = @{@"msg":message};
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:Delete_YELLOW_PIC object:nil userInfo:dict];
                });
            }
        }];
        
    }else if ([message.content isKindOfClass:[SPDPrivateMagicMessage class]]){
        
        SPDPrivateMagicMessage *magicMessage = (SPDPrivateMagicMessage *)message.content;
        if ([magicMessage.magic_result isEqualToString:@"success"]
            && ([magicMessage.magic_type isEqualToString:@"banned_mic"] || [magicMessage.magic_type isEqualToString:@"kicked"])
            && ZegoManager.clan_id
            && ZegoManager.clan_id.length) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"CHATROOM_MAGIC_Effect" object:magicMessage userInfo:nil];
            });
        }
        if (isVideoStatus && isAlreadyDownloadMagicImagesEffect && [[RCCall sharedRCCall].currentCallSession.targetId isEqualToString:magicMessage.senderUserInfo.userId]) {
            SPDApiUser *user = [SPDApiUser currentUser];
            magicMessage.receive_avatar = user.avatar;
            magicMessage.receive_gender = user.gender;
            magicMessage.receive_nickname = user.nickName;
            [self showMagicAnimationWithMessage:magicMessage];
        }

    }else if ([message.content isKindOfClass:[RCTextMessage class]]){
        if (!isVideoStatus && !isInPrivateConversation && !ZegoManager.isAnchor) {
            dispatch_async(dispatch_get_main_queue(), ^{
                RCTextMessage *msg= (RCTextMessage *)message.content;
                if (![msg.senderUserInfo.userId isEqualToString:[SPDApiUser currentUser].userId]) {
                    SPDTopBarView *banner = [SPDTopBarView sharedInstance];
                    banner.delegate = self;
                    banner.msg = msg;
                }
            });
        }
    } else if ([message.content isKindOfClass:[SPDFriendMessage class]]) {
        SPDFriendMessage * msg = (SPDFriendMessage *)message.content;
        if (isInPrivateConversation && [msg.type isEqualToString:@"accept"]) {
            //发送一个通知
            [[NSNotificationCenter defaultCenter] postNotificationName:@"HAVEBECOMEFRIEND" object:message.senderUserId];
        }
    }
}

- (void)showMagicAnimationWithMessage:(SPDPrivateMagicMessage *)magicMessage {
    SPDCRSendMagicMessage *message = [[SPDCRSendMagicMessage alloc] init];
    message.senderUserInfo = magicMessage.senderUserInfo;
    message.send_gender = magicMessage.send_gender;
    message.receive_id = magicMessage.receive_id;
    message.receive_avatar = magicMessage.receive_avatar;
    message.receive_gender = magicMessage.receive_gender;
    message.receive_nickname = magicMessage.receive_nickname;
    message.magic_id = magicMessage.magic_id;
    message.magic_url = magicMessage.magic_cover;
    message.magic_effect = magicMessage.magic_effect;
    message.result = magicMessage.magic_result;
    message.magic_effect_value = magicMessage.magic_effect_value;
    message.type = magicMessage.magic_type ? : @"charm";
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!self.magicAnimationView) {
            self.magicAnimationView = [[SPDChatRoomMagicEffectView alloc] initWithFrame:CGRectMake(19, 175 + 12, kScreenW - 38, (115 + 460) / 2)];
        }
        [[UIApplication sharedApplication].keyWindow addSubview:self.magicAnimationView];
        self.magicAnimationView.magicMsg = message;
    });
}

-(void)popAnimationViewWithMessage:(RCMessage *)message{
    NSString *messageName;
    NSString *messgeImageName;
    NSString *num;
    if ([message.content isKindOfClass:[PresentMessage  class]]) {
        PresentMessage *receiveMessage=(PresentMessage*)message.content;
        messgeImageName = [NSString stringWithFormat:STATIC_DOMAIN_URL,receiveMessage.imageName];
        messageName = receiveMessage.giftName;
        num = receiveMessage.num;
    }else if ([message.content isKindOfClass:[SPDPrivateMagicMessage class]]){
        SPDPrivateMagicMessage *receiveMessage=(SPDPrivateMagicMessage*)message.content;
        messageName = receiveMessage.magic_name;
        messgeImageName = receiveMessage.magic_cover;
        num = @"1";
    }
    
    __weak typeof(self) __weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication].keyWindow addSubview:__weakSelf.annimationView];
        [__weakSelf.annimationView configureDataWithGiftName:messageName giftImageName:messgeImageName avatar:message.content.senderUserInfo.portraitUri nick_name:message.content.senderUserInfo.name num:num];
        [__weakSelf.annimationView startAnimationViewAction];
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [__weakSelf.annimationView stopAnimationViewAction];
        
    });
}

-(PresentAnimView *)annimationView{
    if (!_annimationView) {
        _annimationView=[[PresentAnimView alloc]init];
    }
    return _annimationView;
}


#pragma mark - jysession
//被叫方发送礼物主要是通过baseViewController来实现
-(void)clickedVideoPresentButtonWithTargetID:(NSString *)targrtID{
    // 不是一个语言区的不能送魔法
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:targrtID forKey:@"user_id"];
    [RequestUtils commonGetRequestUtils:dict bURL:@"user.detail" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable userDict) {
        
        HomeModel * model = [HomeModel new];
        model._id = targrtID;
        model.nick_name = userDict[@"nick_name"];
        model.avatar = userDict[@"avatar"];
        model.lang = userDict[@"lang"];
        KKGiftGivingView * givingView = [[[NSBundle mainBundle]loadNibNamed:@"KKGiftGivingView" owner:self options:nil]firstObject];
        givingView.delegate = self;
        givingView.frame = CGRectMake(0, 0, kScreenW, kScreenH);
        givingView.receiveGiftUserModel = model;
        [[UIApplication sharedApplication].keyWindow addSubview:givingView];
    
    } bFailure:^(id  _Nullable failure) {

    }];
    if ([RCCall sharedRCCall].currentCallSession.mediaType == RCCallMediaVideo) {
        //[MobClick event:@"clickVideoGifts"];
    } else {
        //[MobClick event:@"clickVoiceGifts"];
    };
}

- (void)callWillConnect{
    isVideoStatus=YES;
//    //给视频页面发送礼物开关
//    BaseViewController *base=[BaseViewController new];
//    [base postGiftConfigToSingelCallVC];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SPDLiveCallWillConnect object:nil];
    
}

- (void)callWillDisconnect{
    isVideoStatus=NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:SPDLiveCallWillDisConnect object:nil    ];
    
    if ([[SPDApiUser currentUser].userId isEqualToString:[RCCall sharedRCCall].currentCallSession.caller]) {
        NSInteger sec = 0;
        if ([RCCall sharedRCCall].currentCallSession.connectedTime) {
            sec = [[NSDate date] timeIntervalSince1970] - [RCCall sharedRCCall].currentCallSession.connectedTime / 1000;
        }
        
        if ([[RCCall sharedRCCall] currentCallSession].mediaType == RCCallMediaVideo) {
            [self.baseView postCallActiveToUserId:[RCCall sharedRCCall].currentCallSession.targetId type:@"video" sec:sec];
        } else {
            [self.baseView postCallActiveToUserId:[RCCall sharedRCCall].currentCallSession.targetId type:@"audio" sec:sec];
        }
    }
}

- (BOOL)shouldJYReceiveCall {
    [ZegoManager pauseAudio];
    if (ZegoManager.isAnchor) {
        [[RCCall sharedRCCall].currentCallSession hangup];
        [ZegoManager resumeAudio];
        return NO;
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:SPDLivedidJYReceiveCall object:nil];
        return YES;
    }
}

- (void)didJYFinishCall {
    [ZegoManager resumeAudio];
}

#pragma mark - SPDTopBarView

- (void)clickedTopBarView:(RCUserInfo * )userinfo {
    if (userinfo.name && userinfo.userId) {
        UIViewController * vc = [SPDCommonTool getWindowTopViewController];
        SPDConversationViewController* chat = [[SPDConversationViewController alloc] initWithConversationType:ConversationType_PRIVATE targetId:userinfo.userId];
        [chat setHidesBottomBarWhenPushed:YES];
        [vc.navigationController pushViewController:chat animated:YES];
        [[SPDTopBarView sharedInstance] animatingStop];
    }
}

#pragma mark - rcim receive delegate

- (BOOL)onRCIMCustomAlertSound:(RCMessage*)message {
    return YES;
}



@end
