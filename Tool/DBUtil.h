//
//  DBUtil.h
//  SimpleDate
//
//  Created by xu.juvenile on 2016/4/29.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBUtil : NSObject

// ClanHistory
+ (void)recordClanHistory:(NSString *)clan_id;
+ (NSMutableArray *)queryClanHistory;
+ (void)deleteClanHistory:(NSString *)clan_id;
+ (void)deleteClanHistory;

// SearchHistory
+ (void)recordSearchHistory:(NSString *)search_id;
+ (NSMutableArray *)querySearchHistory;
+ (void)deleteSearchHistory;

// MessageState
+ (void)recordMessage:(NSString *)messageUId state:(NSInteger) state ;
+ (BOOL)queryMessageState:(NSString *)messageUId ;

// ShareLimit
+ (void)recordShareLimit:(NSInteger)count;
+ (NSInteger)queryShareLimit;
+ (void)deleteShareLimit;

// APServedOrder
+ (void)recordAPServedOrderWithOrderId:(NSString *)orderId;
+ (BOOL)queryAPServedOrderWithOrderId:(NSString *)orderId;

// RadioHistory
+ (void)recordRadioHistory:(NSString *)radioId;
+ (NSMutableArray *)queryRadioHistory;
+ (void)deleteRadioHistory:(NSString *)radioId;
+ (void)deleteRadioHistory;

// RadioSearchHistory
+ (void)recordRadioSearchHistory:(NSString *)search_text;
+ (NSMutableArray *)queryRadioSearchHistory;
+ (void)deleteRadioSearchHistory;

+ (void)recordIAPWithTransactionId:(NSString *)transactionId
                            userId:(NSString *)userId
                            billId:(NSString *)billId
                           receipt:(NSString *)receipt
                              type:(NSInteger)type;
+ (NSMutableArray *)queryIAPWithType:(NSInteger)type;
+ (void)deleteIAPWithTransactionId:(NSString *)transactionId;

@end
