//
//  ZJTechViewController.m
//  SimpleDate
//
//  Created by houling on 2021/11/21.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "ZJTechViewController.h"

@interface ZJTechBottomView : UIView

@property (nonatomic, strong) UIButton * xiadan;
@property (nonatomic, strong) UIButton * chat;

@end

@implementation ZJTechBottomView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.xiadan];
        [self addSubview:self.chat];
        [self.xiadan mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.top.mas_equalTo(10);
            make.width.mas_equalTo((kScreenW - 30)/3);
            make.height.mas_equalTo(40);
        }];
        [self.chat mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(10);
            make.leading.equalTo(self.xiadan.mas_trailing).offset(10);
            make.width.mas_equalTo((kScreenW - 30)/3);
            make.height.mas_equalTo(40);
        }];
    }
    return self;
}

- (UIButton *)xiadan {
    if (!_xiadan) {
        _xiadan = [UIButton buttonWithType:UIButtonTypeCustom];
        [_xiadan setTitle:@"立即下单" forState:UIControlStateNormal];
        [_xiadan setImage:[UIImage imageNamed:@"ic_xiadan"] forState:UIControlStateNormal];
        [_xiadan setBackgroundColor:[UIColor colorWithHexString:@"#F4F4F4"]];
        _xiadan.titleLabel.font = [UIFont boldSystemFontOfSize:15];
        _xiadan.layer.cornerRadius = 5;
    }
    return _xiadan;
}

- (UIButton *)chat {
    if (!_chat) {
        _chat = [UIButton buttonWithType:UIButtonTypeCustom];
        [_chat setTitle:@"聊一聊" forState:UIControlStateNormal];
        [_chat setImage:[UIImage imageNamed:@"ic_liaoyiliao"] forState:UIControlStateNormal];
        [_chat setBackgroundColor:[UIColor colorWithHexString:@"#00F568"]];
        _chat.titleLabel.font = [UIFont boldSystemFontOfSize:15];
        _chat.layer.cornerRadius = 5;
    }
    return _chat;
}

@end

@interface ZJTechViewController ()
@property(nonatomic, strong)ZJTechBottomView * bottomView;
@end

@implementation ZJTechViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setupFrame];
}

- (void)setupUI {
    [self.view addSubview:self.bottomView];
}

- (void)setupFrame {
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_equalTo(92);
    }];
}
- (ZJTechBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [ZJTechBottomView new];
    }
    return _bottomView;
}
@end
