//
//  ZJTechBaseInfoView.m
//  SimpleDate
//
//  Created by houling on 2021/11/21.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "ZJTechBaseInfoView.h"
#import "LY_IDView.h"

@interface ZJTechBaseInfoView ()

@property(nonatomic, strong)UIImageView * icon;

@property(nonatomic, strong)UIImageView * typeBG;

@property(nonatomic, strong)UIImageView * haopingIcon;

@property(nonatomic, strong)UILabel * typeLabel;

@property(nonatomic, strong) UILabel *nicknameLabel;

@property(nonatomic, strong) LY_IDView *idView;

@property(nonatomic, strong) UILabel * priceLabel;
@property(nonatomic, strong) UILabel * priceDesc;

// 左侧引号视图
@property(nonatomic, strong) UIImageView *leadingSymbolView;

// 简介
@property(nonatomic, strong) UILabel *synopsisLabel;

// 右侧引号视图
@property(nonatomic, strong) UIImageView *trailingSymbolView;

@end

@implementation ZJTechBaseInfoView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (void)setupUI {
    
}

- (void)setupFrames {
    [self.icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(17);
        make.size.mas_equalTo(50);
        make.top.mas_equalTo(15);
    }];
    [self.nicknameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(80);
        make.top.mas_equalTo(19);
    }];
    [self.idView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(80);
        make.top.equalTo(self.nicknameLabel.mas_bottom).offset(10);
        make.height.mas_equalTo(14);
    }];
    [self.leadingSymbolView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(self.haopingIcon.mas_bottom).offset(15);
        make.size.mas_equalTo(CGSizeMake(11, 10));
    }];
    [self.synopsisLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.leadingSymbolView);
        make.leading.mas_equalTo(self.leadingSymbolView.mas_trailing).offset(15);
        make.bottom.mas_equalTo(-10);
    }];
    [self.trailingSymbolView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.synopsisLabel);
        make.trailing.mas_lessThanOrEqualTo(-15);
        make.leading.mas_equalTo(self.synopsisLabel.mas_trailing).offset(15);
        make.size.mas_equalTo(CGSizeMake(11, 10));
    }];
}
- (UIImageView *)icon {
    if (!_icon) {
        _icon = [UIImageView new];
    }
    return _icon;
}

- (UIImageView *)typeBG {
    if (!_typeBG) {
        _typeBG = [UIImageView new];
    }
    return _typeBG;
}

- (UILabel *)typeLabel {
    if (!_typeLabel) {
        _typeLabel = [[UILabel alloc] init];
        _typeLabel.textColor = [UIColor colorWithHexString:@"#263E5B"];
        _typeLabel.font = [UIFont mediumFontOfSize:20];
    }
    return _typeLabel;
}

- (UILabel *)nicknameLabel {
    if (!_nicknameLabel) {
        _nicknameLabel = [[UILabel alloc] init];
        _nicknameLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _nicknameLabel.font = [UIFont mediumFontOfSize:18];
    }
    return _nicknameLabel;
}

- (LY_IDView *)idView {
    if (!_idView) {
        _idView = [[LY_IDView alloc] init];
        _idView.normalIDColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _idView.normalIDFont = [UIFont mediumFontOfSize:12];
        _idView.specialIDFont = [UIFont mediumFontOfSize:12];
        //        [_idView addTarget:self action:@selector(copyIDAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _idView;
}

- (UIImageView *)haopingIcon {
    if (!_haopingIcon) {
        _haopingIcon = [UIImageView new];
    }
    return _haopingIcon;
}

- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [UILabel new];
        _priceLabel.textColor = [UIColor colorWithHexString:@"#FF3369"];
        _priceLabel.font = [UIFont boldSystemFontOfSize:18];
        _priceLabel.text = @"999";
    }
    return _priceLabel;
}

- (UILabel *)priceDesc {
    if (!_priceDesc) {
        _priceDesc = [UILabel new];
        _priceDesc.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _priceDesc.font = [UIFont boldSystemFontOfSize:10];
        _priceDesc.text = @"金币/1小时";
    }
    return _priceDesc;
}

- (UIImageView *)leadingSymbolView {
    if (!_leadingSymbolView) {
        _leadingSymbolView = [[UIImageView alloc] init];
        _leadingSymbolView.image = [UIImage imageNamed:@"ic_gerenzhuye_gerenjianjie_zuo"].adaptiveRtl;
    }
    return _leadingSymbolView;
}

- (UILabel *)synopsisLabel {
    if (!_synopsisLabel) {
        _synopsisLabel = [[UILabel alloc] init];
        _synopsisLabel.textColor = [UIColor colorWithHexString:@"#232426"];
        _synopsisLabel.font = [UIFont systemFontOfSize:14];
        _synopsisLabel.numberOfLines = 0;
    }
    return _synopsisLabel;
}

- (UIImageView *)trailingSymbolView {
    if (!_trailingSymbolView) {
        _trailingSymbolView = [[UIImageView alloc] init];
        _trailingSymbolView.image = [UIImage imageNamed:@"ic_gerenzhuye_gerenjianjie_you"].adaptiveRtl;
    }
    return _trailingSymbolView;
}


@end
