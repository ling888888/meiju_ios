//
//  ZJTechBaseInfoView.h
//  SimpleDate
//
//  Created by houling on 2021/11/21.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZJTechBaseInfoView : UIView

@end

NS_ASSUME_NONNULL_END
