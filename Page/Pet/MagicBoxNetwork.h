//
//  MagicBoxNetwork.h
//  SimpleDate
//
//  Created by houling on 2021/8/1.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MagicBoxNetwork : NSObject

- (void)requOpenBox:(NSString *)type num:(NSInteger)num;

@end

NS_ASSUME_NONNULL_END
