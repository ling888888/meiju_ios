//
//  PetRankListCell.m
//  SimpleDate
//
//  Created by houling on 2021/10/16.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PetRankListCell.h"
#import "PetLabelView.h"
#import "PetView.h"
#import "PetModel.h"

@interface PetRankListCell()

@property (nonatomic, strong) UILabel * indexLabel;
@property (nonatomic, strong) PetView * petView;
@property (nonatomic, strong) PetLabelView * numLabel;
@property (nonatomic, strong) UIImageView * avatarImageView;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) NSDictionary * dict;

@end

@implementation PetRankListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.accessoryType = UITableViewCellAccessoryNone;
        [self setupUI];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self setupFrame];
}

- (void)setupUI {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.contentView addSubview:self.indexLabel];
    [self.contentView addSubview:self.petView];
    [self.contentView addSubview:self.numLabel];
    [self.contentView addSubview:self.avatarImageView];
    [self.contentView addSubview:self.nameLabel];
}

- (void)update:(NSDictionary *)dict type:(NSString *)type {
    self.dict = dict;
    NSInteger index = [dict[@"rank"] integerValue];
    self.indexLabel.text = [NSString stringWithFormat:@"%ld", index];
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:dict[@"avatar"]]]];
    self.nameLabel.text = dict[@"nickName"];
    if ([type isEqualToString:@"bill"]) {
        NSInteger gold = [dict[@"bill"] integerValue];
        [self.numLabel update:type num:gold];
    } if ([type isEqualToString:@"exp"]) {
        NSInteger gold = [dict[@"exp"] integerValue];
        [self.numLabel update:type num:gold];
    } if ([type isEqualToString:@"charm"]) {
        NSInteger gold = [dict[@"charm"] integerValue];
        [self.numLabel update:type num:gold];
    }
    PetModel * model = [[PetModel alloc]init];
    model.level = [dict[@"level"] integerValue];
    model.image = dict[@"image"];
    model.quality = dict[@"quality"];
    self.petView.pet = model;
    [self setNeedsLayout];
}

- (void)setupFrame {
    [self.indexLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.leading.mas_equalTo(0);
        make.width.mas_equalTo(52);
        make.height.equalTo(self);
    }];
    [self.petView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self).offset(52);
        make.centerY.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(65, 65));
    }];
    [self.numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.petView.mas_trailing).offset(19);
        make.centerY.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(75, 26));
    }];
    [self.avatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.numLabel.mas_trailing).offset(15);
        make.centerY.equalTo(self.numLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.avatarImageView.mas_trailing).offset(15);
        make.centerY.equalTo(self.numLabel.mas_centerY);
        make.trailing.mas_equalTo(-15);
    }];
}

- (void)handle:(UITapGestureRecognizer *)tap {
    if (self.didTapdBlock) {
        self.didTapdBlock(self.dict[@""]);
    }
}

- (UILabel *)indexLabel {
    if (!_indexLabel) {
        _indexLabel = [UILabel new];
        _indexLabel.font = [UIFont systemFontOfSize:15];
        _indexLabel.textColor = [UIColor colorWithHexString:@"1A1A1A"];
        _indexLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _indexLabel;
}

- (PetView *)petView {
    if (!_petView) {
        _petView = [PetView new];
        _petView.userInteractionEnabled = YES;
        UITapGestureRecognizer * TAP = [[UITapGestureRecognizer alloc]initWithTarget:self  action:@selector(handle:)];
        [_petView addGestureRecognizer:TAP];
    }
    return _petView;
}

- (UIImageView *)avatarImageView {
    if (!_avatarImageView) {
        _avatarImageView = [UIImageView new];
        _avatarImageView.layer.cornerRadius = 15;
        _avatarImageView.clipsToBounds = true;
    }
    return _avatarImageView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.font = [UIFont systemFontOfSize:15];
        _nameLabel.textColor = [UIColor colorWithHexString:@"000000"];
    }
    return _nameLabel;
}

- (PetLabelView *)numLabel {
    if (!_numLabel) {
        _numLabel = [PetLabelView new];
        _numLabel.layer.cornerRadius = 13;
        _numLabel.clipsToBounds = true;
        _numLabel.numLabel.font = [UIFont systemFontOfSize:12];
        _numLabel.numLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
    }
    return _numLabel;
}


@end
