//
//  PetLabelView.m
//  SimpleDate
//
//  Created by houling on 2021/10/16.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PetLabelView.h"

@implementation PetLabelView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [[UIColor colorWithHexString:@"#000000"] colorWithAlphaComponent:0.12];
        [self addSubview:self.goldImageView];
        [self addSubview:self.numLabel];
    }
    return self;
}

- (void)update:(NSString *)type num:(NSInteger)num {
    if ([type isEqualToString:@"bill"]) {
        self.goldImageView.image = [UIImage imageNamed:@"ic_jinbi"];
    } else if ([type isEqualToString:@"exp"]) {
        self.goldImageView.image = [UIImage imageNamed:@"ic_sb_exp"];
    } else if ([type isEqualToString:@"charm"]) {
        self.goldImageView.image = [UIImage imageNamed:@"ic_sb_charm"];
    }
    self.numLabel.text = [SPDCommonTool transformIntegerToBriefStr:num];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.goldImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(10);
        make.size.mas_equalTo(16);
        make.centerY.mas_equalTo(0);
    }];
    [self.numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.leading.equalTo(self.goldImageView.mas_trailing).offset(6);
    }];
}

- (UIImageView *)goldImageView {
    if (!_goldImageView) {
        _goldImageView = [UIImageView new];
        _goldImageView.image = [UIImage imageNamed:@"ic_jinbi"];
    }
    return _goldImageView;
}
- (UILabel *)numLabel {
    if (!_numLabel) {
        _numLabel = [UILabel new];
        _numLabel.textColor = [UIColor whiteColor];
        _numLabel.font = [UIFont systemFontOfSize:13];
    }
    return _numLabel;
}


@end
