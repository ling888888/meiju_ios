//
//  PetDetailAlertView.m
//  SimpleDate
//
//  Created by houling on 2021/8/21.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PetDetailAlertView.h"
#import "LY_GradientView.h"
#import "PetModel.h"
#import "UIView+LY.h"
#import "PetView.h"

@interface PetDetailAlertView ()<UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIView * containerView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * descLabel;
@property (nonatomic, strong) UIButton * changeBtn;
@property (nonatomic, strong) UIButton * leftBtn;
@property (nonatomic, strong) UIButton * rightBtn;

@property (nonatomic, strong) PetView * petView;

@property (nonatomic, strong) UIButton * equipBtn;
@property (nonatomic, strong) UIButton * upgradeBtn;
@property (nonatomic, strong) UIButton * breakupBtn;
@property (nonatomic, strong) LY_GradientView * gradient;

@property (nonatomic, strong) UIView * lineView;
@property (nonatomic, strong) UIView * greenView;
@property (nonatomic, strong) UILabel * shuxingLabel;
@property (nonatomic, strong) UILabel * goldlbl;
@property (nonatomic, strong) UILabel * charmlbl;
@property (nonatomic, strong) UILabel * levellbl;
@property (nonatomic, strong) UIImageView * leftdouhaoImage;
@property (nonatomic, strong) UIImageView * rightdouhaoImage;

@property (nonatomic, strong) PetModel * pet;

@end

@implementation PetDetailAlertView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(diss)];
        tap.delegate = self;
        [self addGestureRecognizer:tap];
        self.backgroundColor = [UIColor clearColor];
        [self addSubview:self.containerView];
        [self.containerView addSubview:self.gradient];
        [self.containerView addSubview:self.titleLabel];
        [self.containerView addSubview:self.petView];

        [self.containerView addSubview:self.equipBtn];
        [self.containerView addSubview:self.upgradeBtn];
        [self.containerView addSubview:self.breakupBtn];
        [self.containerView addSubview:self.changeBtn];
        [self.containerView addSubview:self.descLabel];
        [self.containerView addSubview:self.leftdouhaoImage];
        [self.containerView addSubview:self.rightdouhaoImage];
        [self.containerView addSubview:self.lineView];
        [self.containerView addSubview:self.greenView];
        [self.containerView addSubview:self.shuxingLabel];
        [self.containerView addSubview:self.goldlbl];
        [self.containerView addSubview:self.charmlbl];
        [self.containerView addSubview:self.levellbl];
        [self.containerView addSubview:self.leftBtn];
        [self.containerView addSubview:self.rightBtn];
        [self updateFrames];
    }
    return self;
}

- (void)updateFrames {
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kScreenH);
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(kScreenW);
        make.height.mas_equalTo(480);
    }];
    [self.gradient mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(18);
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(20);
    }];
    [self.petView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(45);
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(160);
        make.height.mas_equalTo(160 * 102.0/92);
    }];
    [self.rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.width.mas_equalTo(24);
        make.centerY.equalTo(self.petView.mas_centerY);
        make.leading.equalTo(self.petView.mas_trailing).offset(40);
    }];
    [self.leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.width.mas_equalTo(24);
        make.centerY.equalTo(self.petView.mas_centerY);
        make.trailing.equalTo(self.petView.mas_leading).offset(-40);
    }];
    [self.descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.petView.mas_bottom).offset(22);
        make.centerX.mas_equalTo(0);
        make.height.mas_greaterThanOrEqualTo(14);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.descLabel.mas_bottom).offset(15);
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(kScreenW - 30);
        make.height.mas_equalTo(1);
    }];
    [self.changeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(16);
        make.trailing.mas_equalTo(0);
        make.height.mas_equalTo(31);
        make.width.mas_greaterThanOrEqualTo(74);
    }];
    [self.greenView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineView.mas_bottom).offset(14);
        make.width.mas_equalTo(2);
        make.height.mas_equalTo(13);
        make.leading.mas_equalTo(26);
    }];
    [self.shuxingLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineView.mas_bottom).offset(14);
        make.leading.equalTo(self.greenView.mas_trailing).offset(6);
    }];
    [self.goldlbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.shuxingLabel.mas_bottom).offset(13);
        make.leading.equalTo(self.shuxingLabel.mas_leading).offset(0);
    }];
    
    [self.levellbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goldlbl.mas_bottom).offset(13);
        make.leading.equalTo(self.shuxingLabel.mas_leading).offset(0);
    }];
    
    [self.charmlbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.levellbl.mas_bottom).offset(13);
        make.leading.equalTo(self.shuxingLabel.mas_leading).offset(0);
    }];
    
    [self.leftdouhaoImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(11);
        make.trailing.equalTo(self.descLabel.mas_leading).offset(-6);
        make.top.equalTo(self.descLabel.mas_top).offset(2);
    }];
    [self.rightdouhaoImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(11);
        make.leading.equalTo(self.descLabel.mas_trailing).offset(6);
        make.top.equalTo(self.descLabel.mas_top).offset(2);
    }];

    [self.upgradeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(105, 50));
        make.top.equalTo(self.charmlbl.mas_bottom).offset(22);
    }];
    [self.equipBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(105, 50));
        make.top.equalTo(self.charmlbl.mas_bottom).offset(22);
        make.trailing.equalTo(self.upgradeBtn.mas_leading).offset(-16);
    }];
    [self.breakupBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(105, 50));
        make.top.equalTo(self.charmlbl.mas_bottom).offset(22);
        make.leading.equalTo(self.upgradeBtn.mas_trailing).offset(16);
    }];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.containerView addRounded:UIRectCornerTopLeft| UIRectCornerTopRight withRadius:10];
    if (kIsMirroredLayout) {
        [self.changeBtn addRounded:UIRectCornerTopRight| UIRectCornerBottomRight withRadius:15];
    } else {
        [self.changeBtn addRounded:UIRectCornerTopLeft| UIRectCornerBottomLeft withRadius:15];

    }

}

- (void)update:(PetModel *)pet {
    self.pet = pet;
    [self.petView setPet:pet];
    [SPDCommonTool getFamyLanguage];
    self.titleLabel.text = pet.nameStr;
    self.descLabel.text = pet.descStr;
    [self.equipBtn setTitle:pet.equip ? @"卸下".localized :@"装备".localized forState:UIControlStateNormal];
    self.goldlbl.hidden = YES;
    self.charmlbl.hidden = YES;
    self.levellbl.hidden = YES;
    UILabel * lastl = self.goldlbl;
    for (int i = 0; i < pet.attr.count; i++) {
        NSDictionary * dict = pet.attr[i];
        NSString * text = @"";
        if ([dict[@"name"] isEqualToString:@"bill"]) {
            text = [NSString stringWithFormat:@"每天可领取%@金币。".localized,dict[@"value"]];
            lastl.hidden = NO;
            lastl.text = text;
            lastl = self.levellbl;
        } else if ([dict[@"name"] isEqualToString:@"charm"]) {
            text = [NSString stringWithFormat:@"每天可领取%@魅力值。".localized,dict[@"value"]];
            lastl.hidden = NO;
            lastl.text = text;
            lastl = self.charmlbl;
        } else if ([dict[@"name"] isEqualToString:@"exp"]) {
            text = [NSString stringWithFormat:@"每天可领取%@个人等级经验值。".localized,dict[@"value"]];
            lastl.hidden = NO;
            lastl.text = text;
        }
    }
}

#pragma mark - publics

- (void)showIn:(UIView *)targetView pet:(PetModel *)pet {
    [self update:pet];
    [targetView addSubview:self];
    [UIView animateWithDuration:0.25 animations:^{
        [self.containerView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(kScreenH - 480);
        }];
    } completion:^(BOOL finished) {
        [self layoutIfNeeded];
        [self setNeedsLayout];
    }];
}

- (void)diss {
    [self removeFromSuperview];
}

- (void)updateCover:(NSString *)cover {
    [self.petView updatecover:cover];
}

- (void)hanleChangeCover:(UIButton *)sender {
    if (self.didTapChangeCoverBlock) {
        self.didTapChangeCoverBlock(self.pet);
    }
}

- (void)hanleBreakup:(UIButton *)sender {
    [self diss];
    if (self.didTapBreakupBlock) {
        self.didTapBreakupBlock(self.pet);
    }
}

- (void)hanleUpgrade:(UIButton *)sender {
    [self diss];
    if (self.didTapUpgradeBlock) {
        self.didTapUpgradeBlock(self.pet);
    }
}

- (void)hanleEquip:(UIButton *)sender {
    [self diss];
    if (self.didTapEquipBlock) {
        self.didTapEquipBlock(self.pet);
    }
}

- (void)hanleleft:(UIButton *)sender {
    if (self.didTapleftBlock) {
        self.didTapleftBlock(self.pet);
    }
}

- (void)handleright:(UIButton *)sender {
    if (self.didTaprightBlock) {
        self.didTaprightBlock(self.pet);
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return touch.view == self;
}

#pragma mark - getters

- (UIView *)containerView {
    if (!_containerView) {
        _containerView = [UIView new];
        _containerView.backgroundColor = [UIColor whiteColor];
    }
    return _containerView;
}

- (PetView *)petView {
    if (!_petView) {
        _petView = [PetView new];
        _petView.backgroundColor = [UIColor clearColor];
        [_petView setLevelBGColor:[UIColor colorWithHexString:@"00D8C9"]];
    }
    return _petView;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.1];
    }
    return _lineView;
}

- (UIButton *)equipBtn {
    if (!_equipBtn) {
        _equipBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _equipBtn.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [_equipBtn setBackgroundColor:[UIColor colorWithHexString:@"#E845F2"]];
        _equipBtn.layer.cornerRadius = 25;
        [_equipBtn addTarget:self action:@selector(hanleEquip:) forControlEvents:UIControlEventTouchUpInside];
        [_equipBtn setTitle:@"装备".localized forState:UIControlStateNormal];
        [_equipBtn setBackgroundImage:[UIImage imageNamed:@"ic_chongwushuxing_zhuangbei"] forState:UIControlStateNormal];
    }
    return _equipBtn;
}

- (UIButton *)upgradeBtn {
    if (!_upgradeBtn) {
        _upgradeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _upgradeBtn.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [_upgradeBtn setTitle:@"升级".localized forState:UIControlStateNormal];
        [_upgradeBtn setBackgroundColor:[UIColor colorWithHexString:@"#E845F2"]];
        _upgradeBtn.layer.cornerRadius = 25;
        [_upgradeBtn addTarget:self action:@selector(hanleUpgrade:) forControlEvents:UIControlEventTouchUpInside];
        [_upgradeBtn setBackgroundImage:[UIImage imageNamed:@"ic_chongwushuxing_zhuangbei"] forState:UIControlStateNormal];

    }
    return _upgradeBtn;
}

- (UIButton *)breakupBtn {
    if (!_breakupBtn) {
        _breakupBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _breakupBtn.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [_breakupBtn setTitle:@"分解".localized forState:UIControlStateNormal];
        [_breakupBtn setBackgroundColor:[UIColor colorWithHexString:@"#E845F2"]];
        _breakupBtn.layer.cornerRadius = 25;
        [_breakupBtn addTarget:self action:@selector(hanleBreakup:) forControlEvents:UIControlEventTouchUpInside];
        [_breakupBtn setBackgroundImage:[UIImage imageNamed:@"ic_chongwushuxing_zhuangbei"] forState:UIControlStateNormal];

    }
    return _breakupBtn;
}

- (UIButton *)changeBtn {
    if (!_changeBtn) {
        _changeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _changeBtn.titleLabel.font = [UIFont boldSystemFontOfSize:13];
        [_changeBtn setTitle:@"更换形象".localized forState:UIControlStateNormal];
        [_changeBtn setBackgroundColor:[UIColor colorWithHexString:@"#00D39F"]];
        [_changeBtn addTarget:self action:@selector(hanleChangeCover:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _changeBtn;
}

- (LY_GradientView *)gradient {
    if (!_gradient) {
        _gradient = [LY_GradientView new];
        _gradient.colors = @[[UIColor colorWithHexString:@"#FFFFFF"],[UIColor colorWithHexString:@"#88CBFF"]];
        _gradient.direction = UIImageGradientColorsDirectionVertical;
    }
    return _gradient;
}

- (UILabel *)descLabel {
    if (!_descLabel) {
        _descLabel = [UILabel new];
        _descLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _descLabel.font = [UIFont boldSystemFontOfSize:14];
    }
    return _descLabel;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _titleLabel.font = [UIFont boldSystemFontOfSize:18];
        _titleLabel.text = @"宠物名字".localized;
    }
    return _titleLabel;
}

- (UILabel *)shuxingLabel {
    if (!_shuxingLabel) {
        _shuxingLabel = [UILabel new];
        _shuxingLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _shuxingLabel.font = [UIFont boldSystemFontOfSize:15];
        _shuxingLabel.text = @"属性".localized;
    }
    return _shuxingLabel;
}

- (UIView *)greenView {
    if (!_greenView) {
        _greenView = [UIView new];
        _greenView.backgroundColor = [UIColor colorWithHexString:@"#00D4A0"];
    }
    return _greenView;
}

- (UILabel *)goldlbl {
    if (!_goldlbl) {
        _goldlbl = [UILabel new];
        _goldlbl.textColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.8];
        _goldlbl.font = [UIFont boldSystemFontOfSize:14];
        _goldlbl.hidden = true;
    }
    return _goldlbl;
}

- (UILabel *)charmlbl {
    if (!_charmlbl) {
        _charmlbl = [UILabel new];
        _charmlbl.textColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.8];
        _charmlbl.font = [UIFont boldSystemFontOfSize:14];
        _charmlbl.hidden = true;
    }
    return _charmlbl;
}

- (UILabel *)levellbl {
    if (!_levellbl) {
        _levellbl = [UILabel new];
        _levellbl.textColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.8];
        _levellbl.font = [UIFont boldSystemFontOfSize:14];
        _levellbl.hidden = true;
    }
    return _levellbl;
}

- (UIImageView *)leftdouhaoImage {
    if (!_leftdouhaoImage) {
        _leftdouhaoImage = [UIImageView new];
        _leftdouhaoImage.image = [UIImage imageNamed:@"ic_gerenzhuye_gerenjianjie_zuo"];
    }
    return _leftdouhaoImage;
}

- (UIImageView *)rightdouhaoImage {
    if (!_rightdouhaoImage) {
        _rightdouhaoImage = [UIImageView new];
        _rightdouhaoImage.image = [UIImage imageNamed:@"ic_gerenzhuye_gerenjianjie_you"];
    }
    return _rightdouhaoImage;
}

- (UIButton *)leftBtn {
    if (!_leftBtn) {
        _leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        if (kIsMirroredLayout) {
            [_leftBtn setBackgroundImage:[UIImage imageNamed:@"ic_youhua"] forState:UIControlStateNormal];
        } else {
            [_leftBtn setBackgroundImage:[UIImage imageNamed:@"ic_zuohua"] forState:UIControlStateNormal];
        }
        [_leftBtn addTarget:self action:@selector(hanleleft:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _leftBtn;
}

- (UIButton *)rightBtn {
    if (!_rightBtn) {
        _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        if (kIsMirroredLayout) {
            [_rightBtn setBackgroundImage:[UIImage imageNamed:@"ic_zuohua"] forState:UIControlStateNormal];
        } else {
            [_rightBtn setBackgroundImage:[UIImage imageNamed:@"ic_youhua"] forState:UIControlStateNormal];
        }
        [_rightBtn addTarget:self action:@selector(handleright:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rightBtn;
}

@end
