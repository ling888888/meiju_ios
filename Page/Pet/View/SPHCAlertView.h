//
//  SPHCAlertView.h
//  SimpleDate
//
//  Created by houling on 2021/9/12.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LYPCustomAlertView.h"

NS_ASSUME_NONNULL_BEGIN
@class PetModel,DebrisModel;

@interface SPHCAlertView : LYPCustomAlertView
@property (copy, nonatomic) void (^hvClicked)(void);
@property (nonatomic, copy) NSDictionary * config;
@property (nonatomic, strong) PetModel * pet;
@property (nonatomic, strong) DebrisModel * debris;

@end

NS_ASSUME_NONNULL_END
