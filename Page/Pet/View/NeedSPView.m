//
//  NeedSPView.m
//  SimpleDate
//
//  Created by houling on 2021/8/27.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "NeedSPView.h"

@interface NeedSPView ()
@property (nonatomic, strong) UIImageView * light;
@property (nonatomic, strong) UIImageView * dian;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIImageView * titleBg;
@property (nonatomic, strong) UIButton * cButton;
@property (nonatomic, strong) UIImageView * box;

@end

@implementation NeedSPView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.containerView addSubview:self.titleBg];
        [self.containerView addSubview:self.titleLabel];
        [self.containerView addSubview:self.cButton];
        [self.containerView addSubview:self.light];
        [self.containerView insertSubview:self.dian belowSubview:self.light];
        [self.containerView addSubview:self.box];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.containerView addRounded:UIRectCornerTopLeft | UIRectCornerBottomRight withRadius:30];

    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30);
        make.leading.mas_equalTo(36);
        make.trailing.mas_equalTo(-36);
    }];
    
    [self.titleBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.top.trailing.mas_equalTo(0);
        make.height.mas_equalTo(109);
        make.trailing.mas_equalTo(0);
        make.width.mas_equalTo(295);
    }];
    
    [self.box mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleBg.mas_bottom).offset(57);
        make.height.mas_equalTo(123);
        make.centerX.equalTo(self).offset(0);
        make.width.mas_equalTo(134);
    }];
    
    [self.cButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.box.mas_bottom).offset(43);
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(180);
        make.height.mas_equalTo(44);
        make.bottom.mas_equalTo(-25);
    }];
    
    [self.light mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(199);
        make.centerX.equalTo(self);
        make.centerY.equalTo(self.box.mas_centerY);
    }];
    [self.dian mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(255);
        make.height.mas_equalTo(185);
        make.centerX.equalTo(self);
        make.centerY.equalTo(self.box.mas_centerY);
    }];
    [self.containerView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(295);
    }];
}

- (UIImageView *)titleBg {
    if (!_titleBg) {
        _titleBg = [UIImageView new];
        _titleBg.mirrorImage = YES;
        _titleBg.image = [UIImage imageNamed:@"img_tanchuangdingtu"];
    }
    return _titleBg;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#392912"];
        _titleLabel.font = [UIFont boldSystemFontOfSize:18];
        _titleLabel.numberOfLines = 0;
        _titleLabel.text = @"宠物碎片不足，先去开宝箱获取碎片吧".localized;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLabel;
}

- (UIButton *)cButton {
    if (!_cButton) {
        _cButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _cButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [_cButton setTitle:@"开宝箱".localized forState:UIControlStateNormal];
        [_cButton setBackgroundColor:[UIColor colorWithHexString:@"#FBA22A"]];
        _cButton.layer.cornerRadius = 22;
        [_cButton addTarget:self action:@selector(handleCC:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cButton;
}

- (void)handleCC:(UIButton *)BTN {
    [self dismiss];
    if (self.didTapUpgradeBlock) {
        self.didTapUpgradeBlock();
    }
}

- (UIImageView *)light {
    if (!_light) {
        _light = [UIImageView new];
        _light.image = [UIImage imageNamed:@"img_guang"];
    }
    return _light;
}

- (UIImageView *)box {
    if (!_box) {
        _box = [UIImageView new];
        _box.image = [UIImage imageNamed:@"ic_baoxiang"];
    }
    return _box;
}

- (UIImageView *)dian {
    if (!_dian) {
        _dian = [UIImageView new];
        _dian.image = [UIImage imageNamed:@"bg_tanchuangyuansu"];
    }
    return _dian;
}

@end
