//
//  AttributeTitleCell.m
//  SimpleDate
//
//  Created by houling on 2021/8/29.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "AttributeTitleCell.h"

@interface AttributeTitleCell ()

@property (nonatomic, strong) UIView *greenView;
@property (nonatomic, strong) UILabel *titleLbl;

@end

@implementation AttributeTitleCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.titleLbl];
        [self.contentView addSubview:self.greenView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.greenView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(21);
        make.centerY.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(2, 13));
    }];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.greenView.mas_trailing).offset(5);
        make.centerY.mas_equalTo(0);
    }];
}

- (void)setTitle:(NSString *)title {
    self.titleLbl.text = title;
}

- (UIView *)greenView {
    if (!_greenView) {
        _greenView = [UIView new];
        _greenView.backgroundColor = [UIColor colorWithHexString:@"#00D4A0"];
    }
    return _greenView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [UILabel new];
        _titleLbl.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _titleLbl.font = [UIFont mediumFontOfSize:15];
    }
    return _titleLbl;
}

@end
