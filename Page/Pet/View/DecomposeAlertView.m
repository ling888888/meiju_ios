//
//  DecomposeAlertView.m
//  SimpleDate
//
//  Created by houling on 2021/8/29.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "DecomposeAlertView.h"
#import "PetView.h"
#import "PetModel.h"
#import "MagicBoxRewardsCell.h"
#import "LY_GradientView.h"
#import "PetSliceModel.h"

@interface DecomposeAlertView ()
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) PetView * petView;
@property (nonatomic, strong) MagicBoxRewardsCell * rewardcell;
@property (nonatomic, strong) UIButton * okButton;
@property (nonatomic, strong) UIButton * cButton;
@property (nonatomic, strong) UIImageView * arrow;
@property (nonatomic, strong) LY_GradientView * gradient;

@end

@implementation DecomposeAlertView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setui];
        [self setupframe];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

- (void)setui {
    [self.containerView insertSubview:self.gradient atIndex:0];
    self.gradient.frame = CGRectMake(0, 0, 295, 368);
    [self.containerView addSubview:self.titleLabel];
    [self.containerView addSubview:self.petView];
    [self.containerView addSubview:self.rewardcell];
    [self.containerView addSubview:self.okButton];
    [self.containerView addSubview:self.cButton];
    [self.containerView addSubview:self.arrow];
}

- (void)setupframe {
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(24);
        make.leading.mas_equalTo(36);
        make.trailing.mas_equalTo(-36);
        make.width.mas_equalTo(224);
        make.height.mas_greaterThanOrEqualTo(40);
    }];
        
    [self.arrow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(55);
        make.size.mas_equalTo(CGSizeMake(35, 26));
        make.centerX.mas_equalTo(0);
    }];
    
    [self.petView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(16);
        make.size.mas_equalTo(97);
        make.trailing.equalTo(self.arrow.mas_leading).offset(-5);
    }];
    
    [self.rewardcell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(33);
        make.size.mas_equalTo(81);
        make.leading.equalTo(self.arrow.mas_trailing).offset(17);
    }];
    
    [self.okButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.petView.mas_bottom).offset(33);
        make.size.mas_equalTo(CGSizeMake(180, 44));
        make.centerX.mas_equalTo(0);
    }];
    
    [self.cButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.okButton.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(180, 44));
        make.centerX.mas_equalTo(0);
        make.bottom.mas_equalTo(-20);
    }];
}

- (void)handlefenjie:(UIButton *)sender {
    sender.userInteractionEnabled = NO;
    [self dismiss];
    if (self.didTapDecomposeBlock) {
        self.didTapDecomposeBlock(self.pet);
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        sender.userInteractionEnabled = YES;
    });
}

- (void)setPet:(PetModel *)pet {
    _pet = pet;
    self.petView.pet = pet;
    NSString * str = [NSString stringWithFormat:@"当前宠物品质为%@LV.%@，分解 后将随机得到%@个%@宠物碎片。".localized,[self getcolorstr:pet.quality].localized,@(pet.level),@(pet.resolveToNextLevelNum),[self getcolorstr:pet.resolveToNextLevel].localized];
    self.titleLabel.text = str;
    self.rewardcell.icon.hidden = NO;
    self.rewardcell.qicon.hidden = NO;
    self.rewardcell.qicon.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_suipian_%@_weizhi",pet.resolveToNextLevel]];
    self.rewardcell.num.text = [NSString stringWithFormat:@"x%@",@(pet.resolveToNextLevelNum)];

    [self setNeedsLayout];
}

- (NSString *)getcolorstr:(NSString *)color {
    if (color.length <= 0) {
        return  @"白色";
    }
    NSDictionary * dic = @{
        @"white": @"白色",
        @"green": @"绿色",
        @"blue": @"蓝色",
        @"red": @"红色",
        @"purple": @"紫色",
        @"gold": @"金色",
        @"colorful": @"彩色"
    };
    return dic[color] ?: @"白色";
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#392912"];
        _titleLabel.font = [UIFont boldSystemFontOfSize:15];
        _titleLabel.numberOfLines = 0;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLabel;
}
- (PetView *)petView {
    if (!_petView) {
        _petView = [[PetView alloc]init];
        _petView.layer.cornerRadius = 5;
    }
    return _petView;
}

- (UIButton *)cButton {
    if (!_cButton) {
        _cButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _cButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [_cButton setTitle:@"取消".localized forState:UIControlStateNormal];
        [_cButton setTitleColor:[UIColor colorWithHexString:@"#392912"] forState:UIControlStateNormal];
        _cButton.layer.cornerRadius = 22;
        [_cButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cButton;
}

- (UIButton *)okButton {
    if (!_okButton) {
        _okButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _okButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [_okButton setTitle:@"分解".localized forState:UIControlStateNormal];
        [_okButton setBackgroundColor:[UIColor colorWithHexString:@"#FBA22A"]];
        _okButton.layer.cornerRadius = 22;
        [_okButton addTarget:self action:@selector(handlefenjie:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _okButton;
}

- (UIImageView *)arrow {
    if (!_arrow) {
        _arrow = [UIImageView new];
        _arrow.image = [UIImage imageNamed:@"ic_hecheng"];
        _arrow.mirrorImage = YES;
    }
    return _arrow;
}

- (LY_GradientView *)gradient {
    if (!_gradient) {
        _gradient = [LY_GradientView new];
        _gradient.colors = @[SPD_HEXCOlOR(@"#FFFFFF"),SPD_HEXCOlOR(@"#FBE7D4")];
        _gradient.direction = UIImageGradientColorsDirectionVertical;
    }
    return _gradient;
}

- (MagicBoxRewardsCell *)rewardcell {
    if (!_rewardcell) {
        _rewardcell = [[MagicBoxRewardsCell alloc]init];
    }
    return _rewardcell;
}

@end
