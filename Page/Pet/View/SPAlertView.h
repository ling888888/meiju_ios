//
//  SPAlertView.h
//  SimpleDate
//
//  Created by houling on 2021/9/12.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LYPCustomAlertView.h"


@interface SPAlertView : LYPCustomAlertView
@property (copy, nonatomic) void (^hvClicked)(void);
@property (copy, nonatomic) void (^dhClicked)(void);

@end
