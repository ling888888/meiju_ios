//
//  PetRankListCell.h
//  SimpleDate
//
//  Created by houling on 2021/10/16.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PetRankListCell : UITableViewCell

- (void)update:(NSDictionary *)dict type:(NSString *)type;
@property (nonatomic, copy) void (^didTapdBlock)(NSString *ID);

@end

NS_ASSUME_NONNULL_END
