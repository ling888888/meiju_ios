//
//  MagicBoxIntroCell.m
//  SimpleDate
//
//  Created by houling on 2021/8/8.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "MagicBoxIntroCell.h"

@interface MagicBoxIntroCell ()
@property (weak, nonatomic) IBOutlet UILabel *LBL;

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end

@implementation MagicBoxIntroCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.LBL.text = @"说明".localized;
    NSString * t1 = @"1、开宝箱可以获得宠物碎片，贵族值宝箱大概率开出宠物碎片、钻石宝箱中概率，金币宝箱小概率。".localized;
    NSString * t2 = @"2、宠物碎片可以合成宠物，宠物和碎片有不同的品质，品质由高到低依次为：七彩、金色、紫色、红色、蓝色、绿色、白色。".localized;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:t1 attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"1A1A1A"],NSFontAttributeName: [UIFont boldSystemFontOfSize:12]}];
    NSMutableAttributedString * tt = [[NSMutableAttributedString alloc]initWithString:t2 attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"1A1A1A"],NSFontAttributeName: [UIFont boldSystemFontOfSize:12]}];
    NSRange  range = [t2 rangeOfString:@"："];
    [tt addAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"#F8145B"],NSFontAttributeName: [UIFont boldSystemFontOfSize:12]} range:NSMakeRange(range.location, t2.length - range.location)];
    [attributedString appendAttributedString:[[NSAttributedString alloc]initWithString:@"\n"]];
    [attributedString appendAttributedString:tt];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = kIsMirroredLayout ? NSTextAlignmentRight : NSTextAlignmentLeft;
    [paragraphStyle setLineSpacing:1];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attributedString.length)];
    self.contentLabel.attributedText = attributedString;

}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.bgView addRounded:UIRectCornerTopRight | UIRectCornerTopLeft withRadius:25];
}

@end
