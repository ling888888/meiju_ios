//
//  DHSPView.m
//  SimpleDate
//
//  Created by houling on 2021/9/27.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "DHSPView.h"
#import "MagicBoxRewardsCell.h"
#import "LY_GradientView.h"
#import "DebrisModel.h"

@interface DHSPView()

@property (nonatomic, strong) LY_GradientView * gradient;
@property (nonatomic, strong) UIButton * dhButton;
@property (nonatomic, strong) UIImageView * arrow;
@property (nonatomic, strong) MagicBoxRewardsCell * petcell;
@property (nonatomic, strong) MagicBoxRewardsCell * goldcell;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIButton * cButton;
@property (nonatomic, strong) UIButton * jianButton;
@property (nonatomic, strong) UIButton * jiaButton;
@property (nonatomic, strong) UIView * bgJinduView;
@property (nonatomic, strong) UIImageView * lightJinduView;
@property (nonatomic, strong) UIImageView * luotuoView;
@property (nonatomic, strong) UILabel * jlbl;
@property (nonatomic, strong) UILabel * jialbl;
@property (nonatomic, strong) UILabel * currlbl;
@property (nonatomic, assign) NSInteger total;
@property (nonatomic, assign) NSInteger progress;

@end

@implementation DHSPView
- (UILabel *)jlbl {
    if (!_jlbl) {
        _jlbl = [UILabel new];
        _jlbl.textColor = [UIColor colorWithHexString:@"#5E3317"];
        _jlbl.font = [UIFont systemFontOfSize:18];
    }
    return _jlbl;
}

- (UILabel *)jialbl {
    if (!_jialbl) {
        _jialbl = [UILabel new];
        _jialbl.textColor = [UIColor colorWithHexString:@"#5E3317"];
        _jialbl.font = [UIFont systemFontOfSize:18];
    }
    return _jialbl;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setui];
        [self setupframes];
    }
    return self;
}

- (void)setui {
    [self.containerView addSubview:self.gradient];
    [self.containerView addSubview:self.titleLabel];
    [self.containerView addSubview:self.arrow];
    [self.containerView addSubview:self.petcell];
    [self.containerView addSubview:self.goldcell];
    [self.containerView addSubview:self.dhButton];
    [self.containerView addSubview:self.cButton];
    [self.containerView addSubview:self.bgJinduView];
    [self.containerView addSubview:self.lightJinduView];
    [self.containerView addSubview:self.jianButton];
    [self.containerView addSubview:self.jiaButton];
    [self.containerView addSubview:self.luotuoView];
    [self.containerView addSubview:self.jlbl];
    [self.containerView addSubview:self.jialbl];
    [self.containerView addSubview:self.currlbl];
}

- (void)setupframes {
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(33);
        make.leading.mas_equalTo(36);
        make.trailing.mas_equalTo(-36);
        make.height.mas_equalTo(50);
    }];
    [self.arrow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(64);
        make.width.mas_equalTo(35);
        make.height.mas_equalTo(26);
    }];
    [self.petcell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(30);
        make.size.mas_equalTo(81);
        make.trailing.equalTo(self.arrow.mas_leading).offset(-12);
    }];
    [self.goldcell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(30);
        make.leading.equalTo(self.arrow.mas_trailing).offset(12);
        make.size.mas_equalTo(CGSizeMake(81, 81));
    }];
    
    [self.bgJinduView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.arrow.mas_bottom).offset(60);
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(175);
        make.height.mas_equalTo(12);
    }];
    
    [self.lightJinduView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.arrow.mas_bottom).offset(60);
        make.leading.equalTo(self.bgJinduView.mas_leading).offset(0);
        make.width.mas_equalTo(0);
        make.height.mas_equalTo(12);
    }];
    
    [self.jianButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bgJinduView.mas_top).offset(-8);
        make.trailing.equalTo(self.bgJinduView.mas_leading).offset(-14);
    }];
    
    [self.jiaButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bgJinduView.mas_top).offset(-8);
        make.leading.equalTo(self.bgJinduView.mas_trailing).offset(14);
    }];
    
    [self.luotuoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.lightJinduView.mas_trailing).offset(-13.5);
        make.size.mas_equalTo(CGSizeMake(27, 20));
        make.centerY.equalTo(self.bgJinduView.mas_centerY).offset(0);
    }];
    [self.jialbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.jiaButton.mas_bottom).offset(3);
        make.centerX.equalTo(self.jiaButton.mas_centerX).offset(0);
    }];
    [self.jlbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.jianButton.mas_bottom).offset(3);
        make.centerX.equalTo(self.jianButton.mas_centerX).offset(0);
    }];
    [self.dhButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.petcell.mas_bottom).offset(96);
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(180);
        make.height.mas_equalTo(44);
    }];
    [self.cButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.dhButton.mas_bottom).offset(8);
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(180);
        make.height.mas_equalTo(44);
    }];
    [self.containerView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(295);
        make.height.mas_equalTo(400);
    }];
    [self.gradient mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.containerView);
    }];
    [self.currlbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.luotuoView.mas_centerX);
        make.bottom.equalTo(self.luotuoView.mas_top).offset(-4);
    }];
}

- (void)setModel:(DebrisModel *)model {
    _model = model;
    self.total = model.num > 500 ? 500 : model.num;
    NSDictionary * dict = @{
            @"white": @"白色".localized,
            @"green": @"绿色".localized,
            @"blue": @"蓝色".localized,
            @"red": @"红色".localized,
            @"purple": @"紫色".localized,
            @"gold": @"金色".localized,
            @"colorful": @"七彩".localized
    };
    NSString * n = @"green";
    if ([model.quality isEqualToString:@"white"]) {
        n = @"green";
    } else if ([model.quality isEqualToString:@"green"]) {
        n = @"blue";
    } else if ([model.quality isEqualToString:@"blue"]) {
        n = @"red";
    } else if ([model.quality isEqualToString:@"red"]) {
        n = @"purple";
    } else if ([model.quality isEqualToString:@"purple"]) {
        n = @"gold";
    } else if ([model.quality isEqualToString:@"gold"]) {
        n = @"colorful";
    }
    self.petcell.debris = model;
    self.titleLabel.text = [NSString stringWithFormat:@"使用10个同种类%@宠物碎片可兑换1个%@宠物碎片。".localized,dict[model.quality],dict[n]];
    self.goldcell.icon.hidden = YES;
    self.goldcell.num.text = [NSString stringWithFormat:@"x%@",@(model.num/10)];
    self.goldcell.qicon.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_suipian_%@_weizhi",n]];
    self.jlbl.text = [NSString stringWithFormat:@"%@",@(model.num)] ;
    if (model.num < 10) {
        self.jianButton.selected = YES;
        self.jiaButton.selected = YES;
        self.jianButton.userInteractionEnabled = NO;
        self.jiaButton.userInteractionEnabled = NO;
        self.goldcell.num.text = [NSString stringWithFormat:@"x%@",@(0)];
        self.jlbl.text = [NSString stringWithFormat:@"%@",@(model.num)] ;
        self.jialbl.text = [NSString stringWithFormat:@"%@",@(model.num)];
        self.progress = model.num;
        self.currlbl.text = [NSString stringWithFormat:@"%@",@(model.num)];
    } else {
        self.jianButton.selected = YES;
        self.jiaButton.selected = NO;
        self.jianButton.userInteractionEnabled = YES;
        self.jiaButton.userInteractionEnabled = YES;
        self.jlbl.text = [NSString stringWithFormat:@"%@",@(10)] ;
        self.jialbl.text = [NSString stringWithFormat:@"%@",model.num > 500 ? @(500):@(model.num)];
        self.titleLabel.text = [NSString stringWithFormat:@"使用10个同种类%@宠物碎片可兑换1个%@宠物碎片。".localized,dict[model.quality],dict[n]];
        self.goldcell.icon.hidden = YES;
        model.num = 10;
        self.petcell.debris = model;
        self.goldcell.num.text = [NSString stringWithFormat:@"x%@",@(1)];
        self.goldcell.qicon.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_suipian_%@_weizhi",n]];
        self.progress = 10;
    }

}

- (void)handlejian:(UIButton*)sender {
    if (self.progress <= 10) {
        return;
    }
    self.progress -= 10;
}

- (void)handlejia:(UIButton *)sender {
    if (self.progress >= self.total || self.progress + 10 > self.total) {
        return;
    }
    self.progress += 10;
}

- (void)setProgress:(NSInteger)progress {
    _progress = progress;
    if (_progress <= 10) {
        self.jianButton.selected = YES;
    } else {
        self.jianButton.selected = NO;
    }
    if (_progress >= self.total) {
        self.jiaButton.selected = YES;
    } else {
        self.jiaButton.selected = NO;
    }
    double ratio = _progress * 1.0/ self.total;
    self.currlbl.text = [NSString stringWithFormat:@"%@",@(_progress)];
    DebrisModel * d = self.petcell.debris;
    d.num = _progress;
    self.petcell.debris = d;
    self.goldcell.num.text = [NSString stringWithFormat:@"x%@",@(_progress/10)];
    [self.lightJinduView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.bgJinduView.mas_leading).offset(0);
        make.centerY.equalTo(self.bgJinduView.mas_centerY).offset(1);
        make.width.mas_equalTo(175 * ratio);
        make.height.mas_equalTo(12);
    }];
    [self.luotuoView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.lightJinduView.mas_trailing).offset(-13.5);
        make.size.mas_equalTo(CGSizeMake(27, 20));
        make.centerY.equalTo(self.bgJinduView.mas_centerY).offset(0);
    }];
}

#pragma mark - getter

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#392912"];
        _titleLabel.font = [UIFont boldSystemFontOfSize:14];
        _titleLabel.numberOfLines = 0;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLabel;
}

- (MagicBoxRewardsCell *)petcell {
    if (!_petcell) {
        _petcell = [[MagicBoxRewardsCell alloc]init];
        _petcell.layer.cornerRadius = 5;
    }
    return _petcell;
}

- (MagicBoxRewardsCell *)goldcell {
    if (!_goldcell) {
        _goldcell = [[MagicBoxRewardsCell alloc]init];
        _goldcell.layer.cornerRadius = 5;
    }
    return _goldcell;
}

- (UIButton *)cButton {
    if (!_cButton) {
        _cButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cButton setTitleColor:[UIColor colorWithHexString:@"392912"] forState:UIControlStateNormal];
        _cButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
        [_cButton setTitle:@"取消".localized forState:UIControlStateNormal];
        [_cButton setBackgroundColor:[UIColor clearColor]];
        [_cButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cButton;
}

- (UIButton *)dhButton {
    if (!_dhButton) {
        _dhButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _dhButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [_dhButton setTitle:@"兑换".localized forState:UIControlStateNormal];
        [_dhButton setBackgroundColor:[UIColor colorWithHexString:@"#FBA22A"]];
        _dhButton.layer.cornerRadius = 22;
        [_dhButton addTarget:self action:@selector(hanleUpgrade:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _dhButton;
}

- (UIImageView *)arrow {
    if (!_arrow) {
        _arrow = [UIImageView new];
        if (kIsMirroredLayout) {
            _arrow.image = [UIImage imageNamed:@"ic_hecheng_jingxiang"].mirroredImage;
        } else {
            _arrow.image = [UIImage imageNamed:@"ic_hecheng"];
        }
    }
    return _arrow;
}

- (UIImageView *)luotuoView {
    if (!_luotuoView) {
        _luotuoView = [UIImageView new];
        _luotuoView.image = [UIImage imageNamed:@"ic_luotuo"];
    }
    return _luotuoView;
}
- (UIView *)bgJinduView {
    if (!_bgJinduView) {
        _bgJinduView = [UIView new];
        _bgJinduView.backgroundColor = [[UIColor colorWithHexString:@"#000000"] colorWithAlphaComponent:0.1];
        _bgJinduView.layer.cornerRadius = 6;
    }
    return _bgJinduView;
}

- (UIImageView *)lightJinduView {
    if (!_lightJinduView) {
        _lightJinduView = [UIImageView new];
        _lightJinduView.image = [UIImage imageNamed:@"img_jindutiao"];
        _lightJinduView.contentMode = UIViewContentModeScaleAspectFill;
        _lightJinduView.layer.cornerRadius = 6;
        _lightJinduView.clipsToBounds = YES;
    }
    return _lightJinduView;
}

- (UIButton *)jianButton {
    if (!_jianButton) {
        _jianButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_jianButton setImage:[UIImage imageNamed:@"ic_morenjian"] forState:UIControlStateSelected];
        [_jianButton setImage:[UIImage imageNamed:@"ic_jian"] forState:UIControlStateNormal];
        [_jianButton addTarget:self action:@selector(handlejian:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _jianButton;
}
- (UIButton *)jiaButton {
    if (!_jiaButton) {
        _jiaButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_jiaButton setImage:[UIImage imageNamed:@"ic_jia_zhihui"] forState:UIControlStateSelected];
        [_jiaButton setImage:[UIImage imageNamed:@"ic_jia"] forState:UIControlStateNormal];
        [_jiaButton addTarget:self action:@selector(handlejia:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _jiaButton;
}


- (LY_GradientView *)gradient {
    if (!_gradient) {
        _gradient = [LY_GradientView new];
        _gradient.colors = @[SPD_HEXCOlOR(@"#FFFFFF"),SPD_HEXCOlOR(@"#FBE7D4")];
        _gradient.direction = UIImageGradientColorsDirectionVertical;
    }
    return _gradient;
}

#pragma mark - action

- (void)hanleUpgrade:(UIButton *)sender {
    [self dismiss];
    if (self.didTapdhBlock) {
        self.didTapdhBlock(self.model.good_id,[self.currlbl.text integerValue]);
    }
}

- (UILabel *)currlbl {
    if (!_currlbl) {
        _currlbl = [UILabel new];
        _currlbl.textColor = [UIColor colorWithHexString:@"#E86D2C"];
        _currlbl.font = [UIFont systemFontOfSize:12];
    }
    return  _currlbl;
}


@end
