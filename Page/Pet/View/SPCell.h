//
//  SPCell.h
//  SimpleDate
//
//  Created by houling on 2021/8/21.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class DebrisModel;
@interface SPCell : UICollectionViewCell

@property(nonatomic, strong) DebrisModel * debris;

@end

NS_ASSUME_NONNULL_END
