//
//  NeedSPView.h
//  SimpleDate
//
//  Created by houling on 2021/8/27.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LYPCustomAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface NeedSPView : LYPCustomAlertView

@property (nonatomic, copy) void (^didTapUpgradeBlock)();

@end

NS_ASSUME_NONNULL_END
