//
//  OpenMagicBoxOneResultView.m
//  SimpleDate
//
//  Created by houling on 2021/8/1.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "OpenMagicBoxOneResultView.h"
#import "PetSliceModel.h"

@interface OpenMagicBoxOneResultView ()

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIImageView * iconImageView;
@property (nonatomic, strong) UIImageView * startImageView;
@property (nonatomic, strong) UILabel * numlabel;
@property (nonatomic, strong) UIButton * okBtn;
@property (nonatomic, strong) UIImageView * qicon; // zhiliang
@property (nonatomic, strong) CAGradientLayer *gl;
@end

@implementation OpenMagicBoxOneResultView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setui];
        [self setupframes];
    }
    return self;
}

- (void)setui {
    [self.containerView.layer addSublayer:self.gl];
    [self.containerView addSubview:self.startImageView];
    [self.containerView addSubview:self.titleLabel];
    [self.containerView addSubview:self.qicon];
    [self.containerView addSubview:self.iconImageView];
    [self.containerView addSubview:self.numlabel];
    [self.containerView addSubview:self.okBtn];
}

- (void)setupframes {
    [self.startImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(48);
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(255);
        make.height.mas_equalTo(186);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(29);
        make.centerX.mas_equalTo(0);
        make.leading.mas_equalTo(5);
        make.trailing.mas_equalTo(-5);
    }];
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(40);
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(123);
        make.height.mas_equalTo(106);
    }];
    [self.qicon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.centerY.equalTo(self.iconImageView.mas_centerY).offset(0);
        make.width.mas_equalTo(96);
        make.height.mas_equalTo(96);
    }];
    [self.numlabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.iconImageView.mas_bottom).offset(0);
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(30);
    }];
    [self.okBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.numlabel.mas_bottom).offset(20);
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(180);
        make.height.mas_equalTo(44);
        make.bottom.mas_equalTo(-18);
    }];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.gl.frame = self.containerView.bounds;
}

- (void)handleDismiss:(UIButton *)sender {
    [self dismiss];
}

- (void)update:(PetSliceModel *)sliceModel {
    switch (sliceModel.rewardType) {
        case PetSliceRewardTypeCoins: {
            self.qicon.hidden = YES;
            self.iconImageView.image = [UIImage imageNamed:@"ic_jinbimormor"];
            self.numlabel.text = [NSString stringWithFormat:@"x%@",sliceModel.num];
            break;
        }
        case PetSliceRewardTypeDebris: {
            self.qicon.hidden = NO;
            self.qicon.image = [UIImage imageNamed:sliceModel.imageName];
            [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:sliceModel.cover]]];
            [self.iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.titleLabel.mas_bottom).offset(61);
                make.centerX.mas_equalTo(0);
                make.width.mas_equalTo(68);
                make.height.mas_equalTo(60);
            }];
            break;
        }
        default:
            break;
    }
}

#pragma mark - getter

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _titleLabel.font = [UIFont boldSystemFontOfSize:16];
        _titleLabel.text = SPDLocalizedString(@"恭喜获得以下奖励");
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.numberOfLines = 0;
    }
    return _titleLabel;
}

- (UIButton *)okBtn {
    if (!_okBtn) {
        _okBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_okBtn setBackgroundColor:[UIColor colorWithHexString:@"#FBA22A"]];
        [_okBtn setTitle:SPDLocalizedString(@"好的") forState:UIControlStateNormal];
        _okBtn.layer.cornerRadius = 22;
        [_okBtn addTarget:self action:@selector(handleDismiss:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _okBtn;
}

- (UILabel *)numlabel {
    if (!_numlabel) {
        _numlabel = [UILabel new];
        _numlabel.font = [UIFont boldSystemFontOfSize:18];
        _numlabel.textColor = [UIColor colorWithHexString:@"#824A01"];
        _numlabel.textAlignment = NSTextAlignmentCenter;
    }
    return _numlabel;
}

- (UIImageView *)iconImageView {
    if (!_iconImageView) {
        _iconImageView = [UIImageView new];
    }
    return _iconImageView;
}

- (UIImageView *)startImageView {
    if (!_startImageView) {
        _startImageView = [UIImageView new];
        _startImageView.image = [UIImage imageNamed:@"bg_tanchuangyuansu"];
    }
    return _startImageView;
}

- (UIImageView *)qicon {
    if (!_qicon) {
        _qicon = [UIImageView new];
        _qicon.hidden = YES;
    }
    return _qicon;
}

- (CAGradientLayer *)gl {
    if (!_gl) {
        _gl = [CAGradientLayer new];
        _gl.startPoint = CGPointMake(0, 0);
        _gl.endPoint = CGPointMake(1, 1);
        _gl.colors = @[(__bridge id)[UIColor colorWithRed:251/255.0 green:231/255.0 blue:212/255.0 alpha:1.0].CGColor,(__bridge id)[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor];
        _gl.locations = @[@(0.0),@(1.0f)];
    }
    return _gl;
}

@end
