//
//  SPGetPetRewardView.m
//  SimpleDate
//
//  Created by ty on 2021/9/3.
//  Copyright © 2021
//

#import "SPGetPetRewardView.h"

@interface SPGetPetRewardView ()

@property (nonatomic, strong)UILabel * titleLbl;

@end

@implementation SPGetPetRewardView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

#pragma mark - title

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [UILabel new];
        _titleLbl.font = [UIFont systemFontOfSize:16];
        _titleLbl.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _titleLbl.text = @"恭喜获得以下奖励".localized;
    }
    return _titleLbl;
}

@end
