//
//  MagicBoxView.m
//  SimpleDate
//
//  Created by houling on 2021/7/24.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "MagicBoxView.h"
#import "MagicBoxListCell.h"
#import "MagicBoxViewModel.h"
#import "MagicBoxIntroCell.h"

@interface MagicBoxView ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic, strong) UITableView * tableView;
@property(nonatomic, strong) NSMutableArray * dataArr;
@property(nonatomic, strong) MagicBoxViewModel * viewModel;

@end

@implementation MagicBoxView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self setupFrames];
}

- (void)setupUI {
    [self addSubview:self.tableView];
}

- (void)setupFrames {
    self.tableView.frame = self.bounds;
}

- (void)updatedic:(NSDictionary *)dict {
    if (dict[@"noble"]) {
        NSInteger dd = [dict[@"noble"] integerValue];
        NSString * str = [NSString stringWithFormat:@"贵族值宝箱，可以开出高级宠物碎片，%@贵族/次。".localized,@(dd)];
        NSDictionary * dc = @{@"type": @"noble",@"price": @(dd),@"img": @"img_guizukapian",@"bg":@"img_guizukapian",
                              @"title":str ?:@""
        };
        [self.dataArr addObject:dc];
    }
    
    if (dict[@"diamond"]) {
        NSInteger dd = [dict[@"diamond"] integerValue];
        NSString * str = [NSString stringWithFormat:@"钻石宝箱，可以开出中级宠物碎片，%@钻石/次。".localized,@(dd)];
        NSDictionary * dc = @{@"type": @"diamond",@"price": @(dd),@"img": @"img_zuanshikapian",@"bg":@"img_guizukapian",
                              @"title":str ?:@""
        };
        [self.dataArr addObject:dc];
    }
    
    if (dict[@"bill"]) {
        NSInteger dd = [dict[@"bill"] integerValue];
        NSString * str = [NSString stringWithFormat:@"金币宝箱，可以开出初级宠物碎片，%@金币/次。".localized,@(dd)];
        NSDictionary * dc = @{@"type": @"bill",@"price": @(dd),@"img": @"img_jibinkapian",@"bg":@"img_jibinkapian",
                              @"title":str ?:@""
        };
        [self.dataArr addObject:dc];
    }

    [self.tableView reloadData];
}

#pragma mark - delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  self.dataArr.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < self.dataArr.count) {
        MagicBoxListCell * cell = [tableView dequeueReusableCellWithIdentifier:@"MagicBoxListCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (!cell) {
            cell = [[MagicBoxListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MagicBoxListCell"];
            cell.clipsToBounds = NO;
        }
        __weak typeof(self) wself = self;
        cell.openBoxClicked = ^(NSDictionary * _Nonnull param, NSInteger num) {
            wself.openBoxClicked(param, num);
        };
        [cell update:self.dataArr[indexPath.item]];
        return cell;
    } else {
        MagicBoxIntroCell * cell = [MagicBoxIntroCell cellWithTableView:tableView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 200;
}

#pragma mark - getters

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[MagicBoxListCell class] forCellReuseIdentifier:@"MagicBoxListCell"];
        _tableView.backgroundColor = [UIColor clearColor];
    }
    return _tableView;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray new];
    }
    return _dataArr;
}

@end
