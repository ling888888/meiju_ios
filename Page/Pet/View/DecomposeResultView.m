//
//  DecomposeResultView.m
//  SimpleDate
//
//  Created by houling on 2021/8/29.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "DecomposeResultView.h"
#import "MagicBoxRewardsCell.h"
#import "DebrisModel.h"
#import "LY_GradientView.h"

@interface DecomposeResultView ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UILabel * titleLbl;
@property (nonatomic, strong) UICollectionView * collectionView;
@property (nonatomic, strong) UIButton * okBtn;
@property (nonatomic, strong) LY_GradientView * gradient;
@property (nonatomic, strong) UIImageView * titleBg;

@end

@implementation DecomposeResultView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setui];
        [self setupframes];
    }
    return self;
}

- (void)setui {
    [self.containerView insertSubview:self.gradient atIndex:0];
    [self.containerView addSubview:self.titleBg];
    [self.containerView addSubview:self.titleLbl];
    [self.containerView addSubview:self.collectionView];
    [self.containerView addSubview:self.okBtn];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.gradient updateColors];
}

- (void)setupframes {
    [self.containerView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300);
    }];
    [self setNeedsLayout];
    [self layoutIfNeeded];
    [self.gradient mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    [self.titleBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(300, 109));
    }];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(29);
        make.centerX.mas_equalTo(0);
        make.leading.mas_equalTo(33);
        make.trailing.mas_equalTo(-35);
    }];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleBg.mas_bottom).offset(22);
        make.leading.mas_equalTo(15);
        make.trailing.mas_equalTo(-15);
        make.height.mas_equalTo(182);
        make.width.mas_equalTo(265);
    }];
    [self.okBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.collectionView.mas_bottom).offset(20);
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(180);
        make.height.mas_equalTo(44);
        make.bottom.mas_equalTo(-18);
    }];
}

- (void)handleDismiss:(UIButton *)sender {
    [self dismiss];
}

- (void)setData:(NSMutableArray *)data {
    _data = data;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (data.count <= 3) {
            [self.collectionView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(105);
            }];
        }
        [self.collectionView reloadData];
    });
}

#pragma mark - data

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.data.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MagicBoxRewardsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MagicBoxRewardsCell" forIndexPath:indexPath];
    if (indexPath.row < self.data.count) {
        cell.debris = (DebrisModel*)self.data[indexPath.row];
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(81, 81);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return  10;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return  10;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    CGFloat intt = 0;
    if (self.data.count == 1) {
        intt = (self.collectionView.bounds.size.width - 81) / (self.data.count + 1);
    } else if(self.data.count == 2) {
        intt = (self.collectionView.bounds.size.width - 81 * 2 - 10) / 2;
    } else {
        intt = (self.collectionView.bounds.size.width - 81 * 3 - 10 * 2) / 2;
    }
    return UIEdgeInsetsMake(0, intt, 0, intt);
}

#pragma mark - getter

- (UIButton *)okBtn {
    if (!_okBtn) {
        _okBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_okBtn setBackgroundColor:[UIColor colorWithHexString:@"#FBA22A"]];
        [_okBtn setTitle:SPDLocalizedString(@"好的") forState:UIControlStateNormal];
        _okBtn.layer.cornerRadius = 22;
        [_okBtn addTarget:self action:@selector(handleDismiss:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _okBtn;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.itemSize = CGSizeMake(81, 81);
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = 10;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor clearColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerClass:[MagicBoxRewardsCell class] forCellWithReuseIdentifier:@"MagicBoxRewardsCell"];
    }
    return _collectionView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [UILabel new];
        _titleLbl.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _titleLbl.font = [UIFont boldSystemFontOfSize:18];
        _titleLbl.text = @"恭喜你，获得以下宠物碎片， 已放进背包中。".localized;
        _titleLbl.textAlignment = NSTextAlignmentCenter;
        _titleLbl.numberOfLines = 2;
    }
    return _titleLbl;
}

- (LY_GradientView *)gradient {
    if (!_gradient) {
        _gradient = [LY_GradientView new];
        _gradient.colors = @[SPD_HEXCOlOR(@"#FFFFFF"),SPD_HEXCOlOR(@"#FBE7D4")];
        _gradient.direction = UIImageGradientColorsDirectionVertical;
    }
    return _gradient;
}

- (UIImageView *)titleBg {
    if (!_titleBg) {
        _titleBg = [UIImageView new];
        _titleBg.mirrorImage = YES;
        _titleBg.image = [UIImage imageNamed:@"img_tanchuangdingtu-2"];
    }
    return _titleBg;
}

@end
