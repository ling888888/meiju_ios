//
//  AttributeReceiveRewardCell.m
//  SimpleDate
//
//  Created by houling on 2021/8/29.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "AttributeReceiveRewardCell.h"

@interface AttributeReceiveRewardCell ()

@property(nonatomic, strong) UIView *bgView;
@property(nonatomic, strong) UIButton *actionButton;
@property(nonatomic, strong) UILabel *goldlbl;
@property(nonatomic, strong) UILabel *charmlbl;
@property(nonatomic, strong) UILabel *explbl;

@end

@implementation AttributeReceiveRewardCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self p_ui];
    }
    return self;
}

- (void)p_ui {
    [self addSubview:self.bgView];
    [self.bgView addSubview:self.goldlbl];
    [self.bgView addSubview:self.charmlbl];
    [self.bgView addSubview:self.explbl];
    [self.bgView addSubview:self.actionButton];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self p_frame];
}

- (void)p_frame {
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(20);
        make.trailing.mas_equalTo(-20);
        make.top.mas_equalTo(0);
    }];
    
    [self.goldlbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(25);
        make.trailing.mas_equalTo(-25);
        make.top.mas_equalTo(20);
        make.height.mas_equalTo(17);
    }];
    
    [self.explbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(25);
        make.trailing.mas_equalTo(-25);
        make.height.mas_equalTo(17);
        make.top.equalTo(self.goldlbl.mas_bottom).offset(12);
    }];
    [self.charmlbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(25);
        make.trailing.mas_equalTo(-25);
        make.height.mas_equalTo(17);
        make.top.equalTo(self.explbl.mas_bottom).offset(12);
    }];
    
    [self.actionButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(self.explbl.mas_bottom).offset(23);
        make.size.mas_equalTo(CGSizeMake(290, 44));
        make.bottom.mas_equalTo(-10);
    }];
}

- (void)hanle:(UIButton *)sender {
    if (self.didTapReceiveBlock) {
        self.didTapReceiveBlock();
    }
}

- (void)updateday:(NSDictionary *)dic {
    self.actionButton.hidden = NO;
    NSInteger gold = [dic[@"todayBill"] integerValue];
    NSInteger charm = [dic[@"todayCharm"] integerValue];
    NSInteger exp = [dic[@"todayExp"] integerValue];

    if (gold == 0 && charm == 0 && exp == 0) {
        self.goldlbl.attributedText = [[NSAttributedString alloc]initWithString:@"暂无宠物，先去开宝箱获取宠物碎片吧".localized attributes:[self attr]];
        [self.actionButton setBackgroundImage:nil forState:UIControlStateNormal];
        self.actionButton.hidden = NO;
        [self.actionButton setTitle:@"领取".localized forState:UIControlStateNormal];
        [self.actionButton setBackgroundColor:[[UIColor colorWithHexString:@"#000000"] colorWithAlphaComponent:0.15]];
        self.actionButton.userInteractionEnabled = NO;
        self.explbl.hidden = YES;
        self.charmlbl.hidden = YES;
        return;
    }
    self.goldlbl.hidden = YES;
    self.explbl.hidden = YES;
    self.charmlbl.hidden = YES;
    UILabel *lastLbl = self.goldlbl;
    if (gold != 0) {
        NSString * goldssss = [NSString stringWithFormat:@"每天可领取%@金币。".localized,@(gold)];
        NSMutableAttributedString * goldmutable = [[NSMutableAttributedString alloc]initWithString:goldssss attributes:[self attr]];
        [goldmutable addAttributes:[self numAttr] range:[goldssss rangeOfString:[NSString stringWithFormat:@"%ld",gold]]];
        lastLbl.attributedText = goldmutable;
        lastLbl.hidden = NO;
        lastLbl = self.explbl;
    }
    if (exp != 0) {
        NSString * expssss = [NSString stringWithFormat:@"每天可领取%@个人等级经验值。".localized, @(exp)];
        NSMutableAttributedString * expmutable = [[NSMutableAttributedString alloc]initWithString:expssss attributes:[self attr]];
        [expmutable addAttributes:[self numAttr] range:[expssss rangeOfString:[NSString stringWithFormat:@"%ld",exp]]];
        lastLbl.hidden = NO;
        lastLbl.attributedText = expmutable;
        lastLbl = self.charmlbl;
    }
    
    if (charm != 0) {
        NSString * charmssss = [NSString stringWithFormat:@"每天可领取%@魅力值。".localized, @(charm)];
        NSMutableAttributedString * charemmutable = [[NSMutableAttributedString alloc]initWithString:charmssss attributes:[self attr]];
        [charemmutable addAttributes:[self numAttr] range:[charmssss rangeOfString:[NSString stringWithFormat:@"%ld",charm]]];
        lastLbl.attributedText = charemmutable;
        lastLbl.hidden = NO;
    }
    
    [self.actionButton mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.charmlbl.mas_bottom).offset(23);
    }];
    
    if ([dic[@"isRecive"] boolValue]) {
        [self.actionButton setBackgroundImage:nil forState:UIControlStateNormal];
        [self.actionButton setBackgroundColor:[[UIColor colorWithHexString:@"#000000"] colorWithAlphaComponent:0.15]];
        [self.actionButton setTitle:@"已领取".localized forState:UIControlStateNormal];
        self.actionButton.userInteractionEnabled = NO;
    } else {
        [self.actionButton setBackgroundImage:[UIImage imageNamed:@"ic_shuixng_lingqu"] forState:UIControlStateNormal];
        [self.actionButton setTitle:@"领取".localized forState:UIControlStateNormal];
        self.actionButton.userInteractionEnabled = YES;
    }
}

+ (CGFloat)cellheight {
    return 185;
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor colorWithHexString:@"#F2F3FB"];
        _bgView.layer.cornerRadius = 10;
        _bgView.clipsToBounds = YES;
    }
    return _bgView;
}

- (UIButton *)actionButton {
    if (!_actionButton) {
        _actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _actionButton.layer.cornerRadius = 5;
        [_actionButton addTarget:self action:@selector(hanle:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _actionButton;
}

- (NSDictionary *)attr {
    NSDictionary * dc = @{
        NSFontAttributeName: [UIFont systemFontOfSize:15],
        NSForegroundColorAttributeName: [UIColor colorWithHexString:@"#1A1A1A"]
    };
    return dc;
}

- (NSDictionary *)numAttr {
    NSDictionary * dc = @{
        NSFontAttributeName: [UIFont systemFontOfSize:15],
        NSForegroundColorAttributeName: [UIColor colorWithHexString:@"#F83877"]
    };
    return dc;
}

- (UILabel *)goldlbl {
    if (!_goldlbl) {
        _goldlbl = [[UILabel alloc]init];
    }
    return _goldlbl;
}

- (UILabel *)charmlbl {
    if (!_charmlbl) {
        _charmlbl = [[UILabel alloc]init];
    }
    return _charmlbl;
}

- (UILabel *)explbl {
    if (!_explbl) {
        _explbl = [[UILabel alloc]init];
    }
    return _explbl;
}

@end
