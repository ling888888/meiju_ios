//
//  PetCell.m
//  SimpleDate
//
//  Created by houling on 2021/8/14.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PetCell.h"
#import "PetModel.h"

@interface PetCell ()

@property (nonatomic, strong) UIImageView * lightImageView;
@property (nonatomic, strong) UIImageView * petImageView;
@property (nonatomic, strong) UIImageView * bottom1;
@property (nonatomic, strong) UIImageView * bottom2;
@property (nonatomic, strong) UILabel * levelLabel;
@property (nonatomic, strong) UIImageView * equipedImageView;

@end

@implementation PetCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.layer.cornerRadius = 10;
        [self.contentView addSubview:self.lightImageView];
        [self.contentView addSubview:self.bottom2];
        [self.contentView addSubview:self.bottom1];
        [self.contentView addSubview:self.petImageView];
        [self.contentView addSubview:self.levelLabel];
        [self.contentView addSubview:self.equipedImageView];

    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.lightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(90, 62));
    }];
    [self.petImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(2);
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(68, 68));
    }];
    [self.bottom1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.petImageView.mas_bottom).offset(-9);
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(68, 18));
    }];
    [self.bottom2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bottom1.mas_bottom).offset(-9);
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(68, 29));
    }];
    [self.levelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bottom2.mas_bottom).offset(-9);
        make.leading.mas_equalTo(12);
        make.size.mas_equalTo(CGSizeMake(68, 18));
    }];
    [self.equipedImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(2);
        make.trailing.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(22, 22));
    }];
}

- (void)setPet:(PetModel *)pet {
    _pet = pet;
    self.levelLabel.text = [NSString stringWithFormat:@"LV.%ld",pet.level];
    self.bottom1.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_%@_tuoyuan",pet.quality]];
    self.bottom2.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_%@_dizuo",pet.quality]];
    [self.petImageView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:pet.image]] placeholderImage:[UIImage imageNamed:@"img_portrait_placeholder"]];
    self.equipedImageView.hidden = !pet.equip;
    if (pet.selec) {
        self.contentView.backgroundColor = [[UIColor colorWithHexString:@"#000000"] colorWithAlphaComponent:0.1];
    } else {
        self.contentView.backgroundColor = [UIColor clearColor];
    }
}

#pragma mark - getter

- (UIImageView *)lightImageView {
    if (!_lightImageView) {
        _lightImageView = [UIImageView new];
        _lightImageView.image = [UIImage imageNamed:@"bg_guang"];
    }
    return _lightImageView;
}

- (UIImageView *)petImageView {
    if (!_petImageView) {
        _petImageView = [UIImageView new];
    }
    return _petImageView;
}

- (UIImageView *)bottom1 {
    if (!_bottom1) {
        _bottom1 = [UIImageView new];
    }
    return _bottom1;
}

- (UIImageView *)bottom2 {
    if (!_bottom2) {
        _bottom2 = [UIImageView new];
    }
    return _bottom2;
}

- (UILabel *)levelLabel {
    if (!_levelLabel) {
        _levelLabel = [UILabel new];
        _levelLabel.backgroundColor = [[UIColor colorWithHexString:@"000000"] colorWithAlphaComponent:0.3];
        _levelLabel.textColor = [UIColor whiteColor];
        _levelLabel.font = [UIFont mediumFontOfSize:12];
        _levelLabel.layer.cornerRadius = 9;
        _levelLabel.clipsToBounds = YES;
        _levelLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _levelLabel;
}

- (UIImageView *)equipedImageView {
    if (!_equipedImageView) {
        _equipedImageView = [UIImageView new];
        _equipedImageView.image = [UIImage imageNamed:@"ic_yizhuangbei"];
        _equipedImageView.mirrorImage = YES;
        _equipedImageView.hidden = true;
    }
    return _equipedImageView;
}

@end
