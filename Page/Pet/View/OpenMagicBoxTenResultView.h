//
//  OpenMagicBoxTenResultView.h
//  SimpleDate
//
//  Created by houling on 2021/8/1.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LYPCustomAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface OpenMagicBoxTenResultView : LYPCustomAlertView

@property (nonatomic, strong)NSMutableArray * data;

@end

NS_ASSUME_NONNULL_END
