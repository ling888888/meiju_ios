//
//  AttributeHeaderCell.m
//  SimpleDate
//
//  Created by houling on 2021/8/29.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "AttributeHeaderCell.h"
#import "PetView.h"
#import "LookPetView.h"

@interface AttributeHeaderButtonView : UIView

@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) UILabel *lbl;

@end

@implementation AttributeHeaderButtonView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.icon];
        [self addSubview:self.lbl];
        [self.icon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(0);
            make.centerY.mas_equalTo(0);
            make.size.mas_equalTo(32);
        }];
        [self.lbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(0);
            make.top.equalTo(self.icon.mas_bottom).offset(6);
        }];
    }
    return self;
}

- (UIImageView *)icon {
    if(!_icon) {
        _icon = [UIImageView new];
    }
    return _icon;
}

- (UILabel *)lbl {
    if (!_lbl) {
        _lbl = [UILabel new];
        _lbl.font = [UIFont systemFontOfSize:13];
        _lbl.textColor = [UIColor whiteColor];
        _lbl.textAlignment = NSTextAlignmentCenter;
    }
    return _lbl;
}

@end

@interface AttributeHeaderCell ()

@property (nonatomic, strong) UIImageView *personImageView;
@property (nonatomic, strong) PetView *petView;
@property (nonatomic, strong) UIImageView *noPetImageView;
@property (nonatomic, strong) UIImageView *openBoxImageView;
@property (nonatomic, strong) AttributeHeaderButtonView *petButton;
@property (nonatomic, strong) AttributeHeaderButtonView *helpButton;

@end

@implementation AttributeHeaderCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUI];
        [self setFrame];
    }
    return self;
}

- (void)setUI {
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.personImageView];
    [self.contentView addSubview:self.petView];
    [self.contentView addSubview:self.noPetImageView];
    [self.contentView addSubview:self.helpButton];
    [self.contentView addSubview:self.petButton];
    [self.contentView addSubview:self.openBoxImageView];
}

- (void)setFrame {
    [self.personImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(8);
        make.bottom.mas_equalTo(-10);
        make.size.mas_equalTo(CGSizeMake(135, 267));
    }];
    
    [self.petView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-25);
        make.leading.mas_equalTo(102);
        make.size.mas_equalTo(CGSizeMake(129, 102/92.0 * 129));
    }];
    [self.noPetImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-25);
        make.leading.mas_equalTo(102);
        make.size.mas_equalTo(CGSizeMake(129, 142));
    }];
    [self.helpButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.personImageView.mas_top).offset(7);
        make.trailing.mas_equalTo(-20);
        make.size.mas_equalTo(CGSizeMake(32, 50));
    }];
    [self.petButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.helpButton.mas_bottom).offset(15);
        make.trailing.mas_equalTo(-20);
        make.size.mas_equalTo(CGSizeMake(32, 50));
    }];
    [self.openBoxImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(63, 58));
        make.bottom.mas_equalTo(-20);
        make.trailing.mas_equalTo(-16);
    }];
}

- (void)setPet:(PetModel *)pet {
    _pet = pet;
    self.personImageView.image = [[SPDApiUser currentUser].gender isEqualToString:@"male"] ? [UIImage imageNamed:@"img_nanshneg"] : [UIImage imageNamed:@"img_nvsheng"];
    if (pet) {
        self.petView.pet = pet;
        self.noPetImageView.hidden = YES;
        self.petView.hidden = NO;
    } else{
        self.noPetImageView.hidden = NO;
        self.petView.hidden = YES;
    }
    [self BeginWobble];
}

- (void)BeginWobble {
    
    srand([[NSDate date] timeIntervalSince1970]);
    float rand=(float)random();
    CFTimeInterval t=rand*0.0000000001;
    
    [UIView animateWithDuration:0.1 delay:t options:0  animations:^
     {
        self.openBoxImageView.transform=CGAffineTransformMakeRotation(-0.05);
    } completion:^(BOOL finished)
     {
        [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionRepeat|UIViewAnimationOptionAutoreverse|UIViewAnimationOptionAllowUserInteraction  animations:^
         {
            self.openBoxImageView.transform=CGAffineTransformMakeRotation(0.05);
        } completion:^(BOOL finished) {}];
    }];
}

- (void)handleOPenbox:(UITapGestureRecognizer *)tap {
    if (self.didOpenButtonBlock) {
        self.didOpenButtonBlock();
    }
}

- (void)handlePetTap:(UITapGestureRecognizer *)tap {
    [LY_Network getRequestWithURL:LY_Api.shuxing parameters:@{@"user_id":[SPDApiUser currentUser].userId} success:^(id  _Nullable response) {
        if (response[@"decorate"]) {
            NSArray * arr = response[@"decorate"];
            for (NSDictionary * dic in arr) {
                if ([dic[@"position"] isEqualToString:@"pet"]) {
                    LookPetView * fuc = [LookPetView new];
                    [fuc setDict:[NSMutableDictionary dictionaryWithDictionary:dic]];
                    [fuc present];
                }
            }
        }
    } failture:^(NSError * _Nullable error) {
    }];
}

- (void)handleNoPetTap:(UITapGestureRecognizer *)tap {
    if (self.didPetButtonBlock) {
        self.didPetButtonBlock();
    }
}

- (void)handlePetButtonTap:(UITapGestureRecognizer *)tap {
    if (self.didPetButtonBlock) {
        self.didPetButtonBlock();
    }
}

- (void)handleHelpButtonTap:(UITapGestureRecognizer *)tap {
    if (self.didHelpBlock) {
        self.didHelpBlock();
    }
}

#pragma mark - getter

- (UIImageView *)personImageView {
    if (!_personImageView) {
        _personImageView = [UIImageView new];
        _personImageView.contentMode = UIViewContentModeCenter;
    }
    return _personImageView;
}

- (PetView *)petView {
    if (!_petView) {
        _petView = [PetView new];
        _petView.hidden = YES;
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handlePetTap:)];
        [_petView addGestureRecognizer:tap];
    }
    return _petView;
}

- (UIImageView *)noPetImageView {
    if (!_noPetImageView) {
        _noPetImageView = [UIImageView new];
        _noPetImageView.image = [UIImage imageNamed:@"ic_zanwuchongwu"];
        _noPetImageView.userInteractionEnabled = YES;
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleNoPetTap:)];
        [_noPetImageView addGestureRecognizer:tap];
    }
    return _noPetImageView;
}

- (AttributeHeaderButtonView *)petButton {
    if (!_petButton) {
        _petButton = [AttributeHeaderButtonView new];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handlePetButtonTap:)];
        [_petButton addGestureRecognizer:tap];
        _petButton.lbl.text = @"宠物".localized;
        _petButton.icon.image = [UIImage imageNamed:@"ic_zhongwu"];
    }
    return _petButton;
}
- (AttributeHeaderButtonView *)helpButton {
    if (!_helpButton) {
        _helpButton = [AttributeHeaderButtonView new];
        _helpButton.lbl.text = @"帮助".localized;
        _helpButton.icon.image = [UIImage imageNamed:@"ic_bangzhu"];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleHelpButtonTap:)];
        [_helpButton addGestureRecognizer:tap];
    }
    return _helpButton;
}

- (UIImageView *)openBoxImageView {
    if (!_openBoxImageView) {
        _openBoxImageView = [UIImageView new];
        _openBoxImageView.image = [UIImage imageNamed:@"ic_baoxiang"];
        _openBoxImageView.userInteractionEnabled = YES;
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleOPenbox:)];
        [_openBoxImageView addGestureRecognizer:tap];
    }
    return _openBoxImageView;
}

@end
