//
//  SPHCAlertView.m
//  SimpleDate
//
//  Created by houling on 2021/9/12.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "SPHCAlertView.h"
#import "PetView.h"
#import "MagicBoxRewardsCell.h"
#import "PetModel.h"
#import "DebrisModel.h"
#import "PetSliceModel.h"

@interface SPHCAlertView ()

@property (nonatomic, strong) UIButton * hcButton;
@property (nonatomic, strong) UIButton * cancelButton;
@property (nonatomic, strong) UIImageView * arrow;
@property (nonatomic, strong) MagicBoxRewardsCell * petcell;
@property (nonatomic, strong) MagicBoxRewardsCell * goldcell;

@property (nonatomic, strong) UIImageView * light;
@property (nonatomic, strong) UIImageView * dian;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) PetView * petView;
@end

@implementation SPHCAlertView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUI];
        [self setFrames];
    }
    return self;
}

- (void)setUI {
    [self.containerView addSubview:self.titleLabel];
    [self.containerView addSubview:self.dian];
    [self.containerView addSubview:self.light];
    [self.containerView addSubview:self.petView];
    [self.containerView addSubview:self.arrow];
    [self.containerView addSubview:self.petcell];
    [self.containerView addSubview:self.goldcell];
    [self.containerView addSubview:self.hcButton];
    [self.containerView addSubview:self.cancelButton];
}

- (void)setFrames {
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.leading.mas_equalTo(36);
        make.trailing.mas_equalTo(-36);
    }];
    
    [self.petView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(30);
        make.size.mas_equalTo(134);
        make.height.mas_equalTo(134 * 102.0/92);
        make.centerX.equalTo(self);
    }];
    [self.arrow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(self.petView.mas_bottom);
        make.width.mas_equalTo(26);
        make.height.mas_equalTo(35);
    }];
    [self.light mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(199);
        make.centerX.equalTo(self);
        make.centerY.equalTo(self.petView.mas_centerY);
    }];
    [self.dian mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(255);
        make.height.mas_equalTo(185);
        make.centerX.equalTo(self);
        make.centerY.equalTo(self.petView.mas_centerY);
    }];
    [self.petcell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.arrow.mas_bottom).offset(4);
        make.leading.mas_equalTo(62);
        make.size.mas_equalTo(81);
    }];
    [self.goldcell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.arrow.mas_bottom).offset(4);
        make.leading.equalTo(self.petcell.mas_trailing).offset(11);
        make.size.mas_equalTo(81);
        make.trailing.mas_equalTo(-62);
    }];
    [self.hcButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.petcell.mas_bottom).offset(34);
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(180);
        make.height.mas_equalTo(44);
        make.bottom.mas_equalTo(-25);
    }];
    [self.containerView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(295);
        make.height.mas_equalTo(515);
    }];
}

- (void)handleHC:(UIButton *)sender {
    [self dismiss];
    if (self.hvClicked) {
        self.hvClicked();
    }
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#392912"];
        _titleLabel.font = [UIFont boldSystemFontOfSize:18];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.numberOfLines = 0;
    }
    return _titleLabel;
}
- (PetView *)petView {
    if (!_petView) {
        _petView = [[PetView alloc]init];
        _petView.layer.cornerRadius = 5;
        [_petView setLevelBGColor:[UIColor colorWithHexString:@"#00D8C9"]];
    }
    return _petView;
}

- (UIImageView *)light {
    if (!_light) {
        _light = [UIImageView new];
        _light.image = [UIImage imageNamed:@"img_guang"];
    }
    return _light;
}

- (UIImageView *)dian {
    if (!_dian) {
        _dian = [UIImageView new];
        _dian.image = [UIImage imageNamed:@"bg_tanchuangyuansu"];
    }
    return _dian;
}
- (UIButton *)hcButton {
    if (!_hcButton) {
        _hcButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _hcButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [_hcButton setTitle:@"合成".localized forState:UIControlStateNormal];
        [_hcButton setBackgroundColor:[UIColor colorWithHexString:@"#FBA22A"]];
        _hcButton.layer.cornerRadius = 22;
        [_hcButton addTarget:self action:@selector(handleHC:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _hcButton;
}

- (UIImageView *)arrow {
    if (!_arrow) {
        _arrow = [UIImageView new];
        _arrow.image = [UIImage imageNamed:@"ic_hecheng-1"];
    }
    return _arrow;
}

- (MagicBoxRewardsCell *)goldcell {
    if (!_goldcell) {
        _goldcell = [[MagicBoxRewardsCell alloc]init];
    }
    return _goldcell;
}

- (MagicBoxRewardsCell *)petcell {
    if (!_petcell) {
        _petcell = [[MagicBoxRewardsCell alloc]init];
    }
    return _petcell;
}

- (void)setDebris:(DebrisModel *)debris {
    _debris = debris;
    self.petcell.qicon.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_suipian_%@",debris.quality]];
    self.petcell.num.text = [NSString stringWithFormat:@"x%@",@(10)];
    [self.petcell.icon sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:debris.cover]]];
    PetModel * pet = [PetModel new];
    pet.image = debris.cover;
    pet.level = 0;
    pet.quality = debris.quality;
    self.petView.pet = pet;
    if ([debris.quality isEqualToString:@"colorful"] || [debris.quality isEqualToString:@"purple"] || [debris.quality isEqualToString:@"gold"]) {
        self.goldcell.num.font = [UIFont systemFontOfSize:10];
    }
}

- (void)setConfig:(NSDictionary *)config {
    _config = config;
    self.goldcell.sliceModel = nil;
    self.goldcell.icon.hidden = YES;
    self.goldcell.qicon.image = [UIImage imageNamed:@"ic_jinbimormor"];
    NSDictionary * dict = config[@"compoundWholeOutgo"];
    if (dict[self.debris.quality]) {
        self.goldcell.num.text = [NSString stringWithFormat:@"x%@",dict[self.debris.quality]];
    } else {
        self.goldcell.num.text = @"x100";
    }
    self.titleLabel.text = [NSString stringWithFormat:@"使用%@个宠物碎片和%@金币可以合成1个宠物。".localized,@(10),dict[self.debris.quality]];
}

@end
