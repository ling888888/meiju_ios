//
//  MagicBoxRewardsCell.m
//  SimpleDate
//
//  Created by houling on 2021/8/1.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "MagicBoxRewardsCell.h"
#import "PetSliceModel.h"
#import "DebrisModel.h"

@interface MagicBoxRewardsCell ()
@property (nonatomic, strong) UIImageView * numbg;
@end

@implementation MagicBoxRewardsCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.bg];
        [self.contentView addSubview:self.qicon];
        [self.contentView addSubview:self.icon];
        [self.contentView addSubview:self.numbg];
        [self.contentView addSubview:self.num];
        [self.bg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
        [self.qicon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(9);
            make.centerX.mas_equalTo(0);
            make.width.mas_equalTo(45.4);
            make.height.mas_equalTo(45.4);
        }];
        [self.icon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(17);
            make.centerX.mas_equalTo(0);
            make.width.mas_equalTo(32);
            make.height.mas_equalTo(28);
        }];
        [self.numbg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.leading.trailing.mas_equalTo(0);
            make.height.mas_equalTo(22);
        }];
        [self.num mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView.mas_bottom).offset(-22);
            make.centerX.mas_equalTo(0);
            make.height.mas_equalTo(22);
        }];
    }
    return self;
}

#pragma mark - public

- (void)setSliceModel:(PetSliceModel *)sliceModel {
    _sliceModel = sliceModel;
    if (sliceModel) {
        self.num.text = [NSString stringWithFormat:@"x%@",sliceModel.num];
        if (_sliceModel.rewardType == PetSliceRewardTypeDebris) {
            self.icon.hidden = NO;
            self.qicon.image = [UIImage imageNamed:sliceModel.imageName];
            [self.icon sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:sliceModel.cover]]];
        } else {
            self.icon.hidden = YES;
            self.qicon.image = [UIImage imageNamed:@"ic_jinbimormor"];
        }
    }
}

- (void)setDebris:(DebrisModel *)debris {
    _debris = debris;
    self.icon.hidden = NO;
    self.num.text = [NSString stringWithFormat:@"x%@",@(debris.num)];
    self.qicon.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_suipian_%@",debris.quality]];
    [self.icon sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:debris.cover]]];
}

#pragma mark - getter

- (UIImageView *)bg {
    if (!_bg) {
        _bg = [UIImageView new];
        _bg.image = [UIImage imageNamed:@"ic_jianglibiankuang"];
    }
    return _bg;
}

- (UIImageView *)icon {
    if (!_icon) {
        _icon = [UIImageView new];
    }
    return _icon;
}

- (UIImageView *)qicon {
    if (!_qicon) {
        _qicon = [UIImageView new];
    }
    return _qicon;
}

- (UIImageView *)numbg {
    if (!_numbg) {
        _numbg = [UIImageView new];
        _numbg.contentMode = UIViewContentModeScaleAspectFill;
        _numbg.clipsToBounds = YES;
        _numbg.image = [UIImage imageNamed:@"ic_shuzhi"];
    }
    return _numbg;
}

- (UILabel *)num {
    if (!_num) {
        _num = [UILabel new];
        _num.textColor = [UIColor colorWithHexString:@"#5E3317"];
        _num.font = [UIFont systemFontOfSize:17];
    }
    return _num;
}

@end
