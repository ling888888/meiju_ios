//
//  AttributeReceiveRewardCell.h
//  SimpleDate
//
//  Created by houling on 2021/8/29.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AttributeReceiveRewardCell : UICollectionViewCell

@property (nonatomic, copy) void (^didTapReceiveBlock)(void);

- (void)updateday:(NSDictionary *)dic;
+ (CGFloat)cellheight;

@end

NS_ASSUME_NONNULL_END
