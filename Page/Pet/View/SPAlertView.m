//
//  SPAlertView.m
//  SimpleDate
//
//  Created by houling on 2021/9/12.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "SPAlertView.h"

@interface SPAlertView ()
@property(nonatomic, strong) UIButton * hecheng;
@property(nonatomic, strong) UIButton * duihuan;

@end

@implementation SPAlertView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setui];
        [self setupframes];
    }
    return self;
}

- (void)setui {
    [self.containerView addSubview:self.hecheng];
    [self.containerView addSubview:self.duihuan];
}
- (void)setupframes {
    [self.hecheng mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.height.mas_equalTo(60);
        make.leading.mas_equalTo(0);
        make.trailing.mas_equalTo(0);
    }];
    [self.duihuan mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(0);
        make.trailing.mas_equalTo(0);
        make.height.mas_equalTo(60);
        make.top.mas_equalTo(70);
        make.bottom.mas_equalTo(-10);
    }];
}

- (void)hanldehecheng:(UIButton *)sender {
    [self dismiss];
    if (self.hvClicked) {
        self.hvClicked();
    }
}

- (void)handleduihuan:(UIButton *)sender {
    [self dismiss];
    if (self.dhClicked) {
        self.dhClicked();
    }
}

- (void)handleDismiss:(UIButton *)sender {
    [self dismiss];
}

- (UIButton *)hecheng {
    if (!_hecheng) {
        _hecheng = [UIButton buttonWithType:UIButtonTypeCustom];
        [_hecheng setTitle:@"合成宠物".localized forState:UIControlStateNormal];
        [_hecheng setTitleColor:[UIColor colorWithHexString:@"1A1A1A"] forState:UIControlStateNormal];
        [_hecheng addTarget:self action:@selector(hanldehecheng:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _hecheng;
}

- (UIButton *)duihuan {
    if (!_duihuan) {
        _duihuan = [UIButton buttonWithType:UIButtonTypeCustom];
        [_duihuan setTitle:@"兑换碎片".localized forState:UIControlStateNormal];
        [_duihuan setTitleColor:[UIColor colorWithHexString:@"1A1A1A"] forState:UIControlStateNormal];
        [_duihuan addTarget:self action:@selector(handleduihuan:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _duihuan;
}

@end
