//
//  LookPetView.h
//  SimpleDate
//
//  Created by houling on 2021/9/19.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LYPCustomAlertView.h"

NS_ASSUME_NONNULL_BEGIN
@class PetModel;
@interface LookPetView : LYPCustomAlertView

@property(nonatomic, strong) NSMutableDictionary * dict;

@end

NS_ASSUME_NONNULL_END
