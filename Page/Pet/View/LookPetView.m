//
//  LookPetView.m
//  SimpleDate
//
//  Created by houling on 2021/9/19.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LookPetView.h"
#import "PetView.h"
#import "PetModel.h"

@interface LookPetView ()

@property (nonatomic, strong) UIImageView * light;
@property (nonatomic, strong) UIImageView * dian;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIImageView * titleBg;
@property (nonatomic, strong) PetView * petView;
@property (nonatomic, strong) UIView * greenView;

@property (nonatomic, strong) UILabel * namelBl;
@property (nonatomic, strong) UILabel * shuxingLabel;
@property (nonatomic, strong) UILabel * goldlbl;
@property (nonatomic, strong) UILabel * charmlbl;
@property (nonatomic, strong) UILabel * levellbl;
@property (nonatomic, strong) UIImageView * leftdouhaoImage;
@property (nonatomic, strong) UIImageView * rightdouhaoImage;
@property (nonatomic, strong) UILabel * descLabel;

@end

@implementation LookPetView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self p_setui];
    }
    return self;
}

- (void)p_setui {
    [self.containerView addSubview:self.titleBg];
    [self.containerView addSubview:self.titleLabel];
    [self.containerView addSubview:self.petView];
    [self.containerView addSubview:self.namelBl];
    [self.containerView addSubview:self.leftdouhaoImage];
    [self.containerView addSubview:self.rightdouhaoImage];
    [self.containerView addSubview:self.shuxingLabel];
    [self.containerView addSubview:self.goldlbl];
    [self.containerView addSubview:self.levellbl];
    [self.containerView addSubview:self.charmlbl];
    [self.containerView addSubview:self.greenView];
    [self.containerView addSubview:self.descLabel];

    [self.containerView insertSubview:self.light belowSubview:self.petView];
    [self.containerView insertSubview:self.dian belowSubview:self.light];

    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(24);
        make.leading.mas_equalTo(36);
        make.trailing.mas_equalTo(-36);
    }];
    
    [self.titleBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.top.trailing.mas_equalTo(0);
        make.height.mas_equalTo(74);
        make.trailing.mas_equalTo(0);
        make.width.mas_equalTo(295);
    }];
    
    [self.namelBl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleBg.mas_bottom).offset(6);
        make.centerX.mas_equalTo(0);
    }];
    
    [self.petView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.namelBl.mas_bottom).offset(8);
        make.size.mas_equalTo(134);
        make.centerX.equalTo(self);
    }];
    
    [self.light mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(199);
        make.centerX.equalTo(self);
        make.centerY.equalTo(self.petView.mas_centerY);
    }];
    [self.dian mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(255);
        make.height.mas_equalTo(185);
        make.centerX.equalTo(self);
        make.centerY.equalTo(self.petView.mas_centerY);
    }];
    
    [self.descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.petView.mas_bottom).offset(31);
        make.centerX.mas_equalTo(0);
        make.height.mas_greaterThanOrEqualTo(14);
        make.width.mas_lessThanOrEqualTo(199);
    }];
    
    [self.greenView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.descLabel.mas_bottom).offset(23);
        make.width.mas_equalTo(2);
        make.height.mas_equalTo(13);
        make.leading.mas_equalTo(26);
    }];
    [self.shuxingLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.descLabel.mas_bottom).offset(21);
        make.leading.equalTo(self.greenView.mas_trailing).offset(6);
    }];
    [self.goldlbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.shuxingLabel.mas_bottom).offset(13);
        make.leading.equalTo(self.shuxingLabel.mas_leading).offset(0);
    }];
    [self.charmlbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goldlbl.mas_bottom).offset(13);
        make.leading.equalTo(self.shuxingLabel.mas_leading).offset(0);
    }];
    [self.levellbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.charmlbl.mas_bottom).offset(13);
        make.leading.equalTo(self.shuxingLabel.mas_leading).offset(0);
    }];
    [self.leftdouhaoImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(11);
        make.trailing.equalTo(self.descLabel.mas_leading).offset(-6);
        make.top.equalTo(self.descLabel.mas_top).offset(2);
    }];
    [self.rightdouhaoImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(11);
        make.leading.equalTo(self.descLabel.mas_trailing).offset(6);
        make.top.equalTo(self.descLabel.mas_top).offset(2);
    }];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.containerView addRounded:UIRectCornerTopLeft | UIRectCornerBottomRight withRadius:30];
}

- (void)setDict:(NSMutableDictionary *)dict {
    _dict = dict;
    PetModel * pet = [PetModel new];
    pet.level = [dict[@"level"] integerValue];
    pet.quality = dict[@"quality"];
    pet.image = dict[@"image"];
    [self.petView setPet:pet];
    self.titleLabel.text = @"查看宠物属性".localized;
    self.namelBl.text = dict[@"name"];
    self.descLabel.text = dict[@"desc"];
    UILabel * lastlbl = self.goldlbl;
    if ([dict[@"bill"] integerValue] != 0) {
        lastlbl.text = [NSString stringWithFormat:@"每天可领取%@金币。".localized, dict[@"bill"]];
        lastlbl.hidden = NO;
        lastlbl = self.charmlbl;
    }
    if ([dict[@"charm"] integerValue] != 0) {
        lastlbl.text = [NSString stringWithFormat:@"每天可领取%@魅力值。".localized, dict[@"charm"]];
        lastlbl.hidden = NO;
        lastlbl = self.levellbl;
    }
    if ([dict[@"exp"] integerValue] != 0) {
        lastlbl.text = [NSString stringWithFormat:@"每天可领取%@个人等级经验值。".localized, dict[@"exp"]];
        lastlbl.hidden = NO;
    }
    [self layoutIfNeeded];
    
    [self.containerView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(295);
        make.height.mas_equalTo(CGRectGetMaxY(lastlbl.frame) + 25);
    }];
    [self layoutIfNeeded];
    [self setNeedsLayout];
}

- (UIImageView *)titleBg {
    if (!_titleBg) {
        _titleBg = [UIImageView new];
        _titleBg.mirrorImage = YES;
        _titleBg.image = [UIImage imageNamed:@"img_tanchuangdingtu"];
    }
    return _titleBg;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#392912"];
        _titleLabel.font = [UIFont boldSystemFontOfSize:18];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLabel;
}
- (PetView *)petView {
    if (!_petView) {
        _petView = [[PetView alloc]init];
        _petView.layer.cornerRadius = 5;
        [_petView setLevelBGColor:[UIColor colorWithHexString:@"#00D8C9"]];
    }
    return _petView;
}

- (UILabel *)shuxingLabel {
    if (!_shuxingLabel) {
        _shuxingLabel = [UILabel new];
        _shuxingLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _shuxingLabel.font = [UIFont boldSystemFontOfSize:15];
        _shuxingLabel.text = @"属性".localized;
    }
    return _shuxingLabel;
}

- (UIView *)greenView {
    if (!_greenView) {
        _greenView = [UIView new];
        _greenView.backgroundColor = [UIColor colorWithHexString:@"#00D4A0"];
    }
    return _greenView;
}

- (UILabel *)goldlbl {
    if (!_goldlbl) {
        _goldlbl = [UILabel new];
        _goldlbl.textColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.8];
        _goldlbl.font = [UIFont boldSystemFontOfSize:12];
        _goldlbl.hidden = true;
    }
    return _goldlbl;
}

- (UILabel *)charmlbl {
    if (!_charmlbl) {
        _charmlbl = [UILabel new];
        _charmlbl.textColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.8];
        _charmlbl.font = [UIFont boldSystemFontOfSize:12];
        _charmlbl.hidden = true;
    }
    return _charmlbl;
}

- (UILabel *)levellbl {
    if (!_levellbl) {
        _levellbl = [UILabel new];
        _levellbl.textColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.8];
        _levellbl.font = [UIFont boldSystemFontOfSize:12];
        _levellbl.hidden = true;
    }
    return _levellbl;
}

- (UIImageView *)leftdouhaoImage {
    if (!_leftdouhaoImage) {
        _leftdouhaoImage = [UIImageView new];
        _leftdouhaoImage.image = [UIImage imageNamed:@"ic_gerenzhuye_gerenjianjie_zuo"];
    }
    return _leftdouhaoImage;
}

- (UIImageView *)rightdouhaoImage {
    if (!_rightdouhaoImage) {
        _rightdouhaoImage = [UIImageView new];
        _rightdouhaoImage.image = [UIImage imageNamed:@"ic_gerenzhuye_gerenjianjie_you"];
    }
    return _rightdouhaoImage;
}

- (UILabel *)namelBl {
    if (!_namelBl) {
        _namelBl = [UILabel new];
        _namelBl.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _namelBl.font = [UIFont boldSystemFontOfSize:18];
        _namelBl.text = @"宠物名字".localized;
    }
    return _namelBl;
}

- (UILabel *)descLabel {
    if (!_descLabel) {
        _descLabel = [UILabel new];
        _descLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _descLabel.font = [UIFont boldSystemFontOfSize:14];
    }
    return _descLabel;
}

- (UIImageView *)light {
    if (!_light) {
        _light = [UIImageView new];
        _light.image = [UIImage imageNamed:@"img_guang"];
    }
    return _light;
}

- (UIImageView *)dian {
    if (!_dian) {
        _dian = [UIImageView new];
        _dian.image = [UIImage imageNamed:@"bg_tanchuangyuansu"];
    }
    return _dian;
}

@end
