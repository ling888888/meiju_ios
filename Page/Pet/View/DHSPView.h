//
//  DHSPView.h
//  SimpleDate
//
//  Created by houling on 2021/9/27.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LYPCustomAlertView.h"

NS_ASSUME_NONNULL_BEGIN
@class DebrisModel;
@interface DHSPView : LYPCustomAlertView

@property (nonatomic, strong) DebrisModel * model;
@property (nonatomic, copy) void (^didTapdhBlock)(NSString *goodID, NSInteger num);

@end

NS_ASSUME_NONNULL_END
