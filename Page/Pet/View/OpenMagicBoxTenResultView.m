//
//  OpenMagicBoxTenResultView.m
//  SimpleDate
//
//  Created by houling on 2021/8/1.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "OpenMagicBoxTenResultView.h"
#import "MagicBoxRewardsCell.h"
#import "PetSliceModel.h"
#import "LY_GradientView.h"

@interface OpenMagicBoxTenResultView ()<UICollectionViewDataSource,UICollectionViewDelegate>

@property (nonatomic, strong) UILabel * titleLbl;
@property (nonatomic, strong) UICollectionView * collectionView;
@property (nonatomic, strong) UIButton * okBtn;
@property (nonatomic, strong) LY_GradientView * gradient;

@end

@implementation OpenMagicBoxTenResultView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setui];
        [self setupframes];
    }
    return self;
}

- (void)setui {
    [self.containerView insertSubview:self.gradient atIndex:0];
    [self.containerView addSubview:self.titleLbl];
    [self.containerView addSubview:self.collectionView];
    [self.containerView addSubview:self.okBtn];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.gradient updateColors];
}

- (void)setupframes {
    [self.containerView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300);
    }];
    [self setNeedsLayout];
    [self layoutIfNeeded];
    [self.gradient mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(29);
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(20);
        make.leading.mas_equalTo(5);
        make.trailing.mas_equalTo(-5);
    }];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLbl.mas_bottom).offset(20);
        make.leading.mas_equalTo(15);
        make.trailing.mas_equalTo(-15);
        make.height.mas_equalTo(354);
        make.width.mas_equalTo(265);
    }];
    [self.okBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.collectionView.mas_bottom).offset(20);
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(180);
        make.height.mas_equalTo(44);
        make.bottom.mas_equalTo(-18);
    }];
}

- (void)handleDismiss:(UIButton *)sender {
    [self dismiss];
}

- (void)setData:(NSMutableArray *)data {
    _data = data;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
}

#pragma mark - data

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.data.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MagicBoxRewardsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MagicBoxRewardsCell" forIndexPath:indexPath];
    if (indexPath.row < self.data.count) {
        cell.sliceModel = (PetSliceModel*)self.data[indexPath.row];
    }
    return  cell;
}

#pragma mark - getter

- (UIButton *)okBtn {
    if (!_okBtn) {
        _okBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_okBtn setBackgroundColor:[UIColor colorWithHexString:@"#FBA22A"]];
        [_okBtn setTitle:SPDLocalizedString(@"好的") forState:UIControlStateNormal];
        _okBtn.layer.cornerRadius = 22;
        [_okBtn addTarget:self action:@selector(handleDismiss:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _okBtn;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.itemSize = CGSizeMake(81, 81);
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = 10;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor clearColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerClass:[MagicBoxRewardsCell class] forCellWithReuseIdentifier:@"MagicBoxRewardsCell"];
    }
    return _collectionView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [UILabel new];
        _titleLbl.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _titleLbl.font = [UIFont boldSystemFontOfSize:16];
        _titleLbl.text = SPDLocalizedString(@"恭喜获得以下奖励");
        _titleLbl.textAlignment = NSTextAlignmentCenter;
        _titleLbl.numberOfLines = 0;
        _titleLbl.minimumScaleFactor = 3;
    }
    return _titleLbl;
}

- (LY_GradientView *)gradient {
    if (!_gradient) {
        _gradient = [LY_GradientView new];
        _gradient.colors = @[SPD_HEXCOlOR(@"#FFFFFF"),SPD_HEXCOlOR(@"#FBE7D4")];
        _gradient.direction = UIImageGradientColorsDirectionVertical;
    }
    return _gradient;
}

@end
