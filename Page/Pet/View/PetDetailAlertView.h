//
//  PetDetailAlertView.h
//  SimpleDate
//
//  Created by houling on 2021/8/21.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class PetModel;

@interface PetDetailAlertView : UIView

- (void)showIn:(UIView *)targetView pet:(PetModel *)pet;
- (void)update:(PetModel *)pet;

@property (nonatomic, copy) void (^didTapChangeCoverBlock)(PetModel *pet);
@property (nonatomic, copy) void (^didTapUpgradeBlock)(PetModel *pet);
@property (nonatomic, copy) void (^didTapBreakupBlock)(PetModel *pet);
@property (nonatomic, copy) void (^didTapEquipBlock)(PetModel *pet);
@property (nonatomic, copy) void (^didTapleftBlock)(PetModel *pet);
@property (nonatomic, copy) void (^didTaprightBlock)(PetModel *pet);

- (void)updateCover:(NSString *)cover;
@end

NS_ASSUME_NONNULL_END
