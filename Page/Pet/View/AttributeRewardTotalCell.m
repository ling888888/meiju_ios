//
//  AttributeRewardTotalCell.m
//  SimpleDate
//
//  Created by houling on 2021/8/29.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "AttributeRewardTotalCell.h"

@interface AttributeRewardTotalCell ()

@property(nonatomic, strong) UIView *bgView;
@property(nonatomic, strong) UILabel *goldlbl;
@property(nonatomic, strong) UILabel *charmlbl;
@property(nonatomic, strong) UILabel *explbl;

@end

@implementation AttributeRewardTotalCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self p_ui];
    }
    return self;
}

- (void)p_ui {
    [self addSubview:self.bgView];
    [self.bgView addSubview:self.goldlbl];
    [self.bgView addSubview:self.charmlbl];
    [self.bgView addSubview:self.explbl];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self p_frame];
}

- (void)p_frame {
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(20);
        make.trailing.mas_equalTo(-20);
        make.top.mas_equalTo(0);
    }];
    
    [self.goldlbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(25);
        make.trailing.mas_equalTo(-25);
        make.top.mas_equalTo(20);
    }];
    
    [self.explbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(25);
        make.trailing.mas_equalTo(-25);
        make.top.equalTo(self.goldlbl.mas_bottom).offset(12);
    }];
    
    [self.charmlbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(25);
        make.trailing.mas_equalTo(-25);
        make.top.equalTo(self.explbl.mas_bottom).offset(12);
        make.bottom.mas_equalTo(-20);
    }];
}

- (void)updateto:(NSDictionary *)dic {
    NSInteger gold = [dic[@"allBill"] integerValue];
    NSString* gg = [SPDCommonTool transformIntegerToBriefStr:gold];
    NSString * goldssss = [NSString stringWithFormat:@"已累积领取%@金币".localized,gg];
    NSMutableAttributedString * goldmutable = [[NSMutableAttributedString alloc]initWithString:goldssss attributes:[self attr]];
    [goldmutable addAttributes:[self numAttr] range:[goldssss rangeOfString:gg]];
    self.goldlbl.attributedText = goldmutable;
    
    NSInteger charm = [dic[@"allCharm"] integerValue];
    NSString* cc = [SPDCommonTool transformIntegerToBriefStr:charm];
    NSString * charmssss = [NSString stringWithFormat:@"已累积领取%@个人等级经验值".localized, cc];
    NSMutableAttributedString * charemmutable = [[NSMutableAttributedString alloc]initWithString:charmssss attributes:[self attr]];
    [charemmutable addAttributes:[self numAttr] range:[charmssss rangeOfString:cc]];
    self.charmlbl.attributedText = charemmutable;
    
    NSInteger exp = [dic[@"allExp"] integerValue];
    NSString* ee = [SPDCommonTool transformIntegerToBriefStr:[dic[@"allExp"] integerValue]];
    NSString * expssss = [NSString stringWithFormat:@"已累积领取%@魅力值".localized, ee];
    NSMutableAttributedString * expmutable = [[NSMutableAttributedString alloc]initWithString:expssss attributes:[self attr]];
    [expmutable addAttributes:[self numAttr] range:[expssss rangeOfString:ee]];
    self.explbl.attributedText = expmutable;
}

+ (CGFloat)cellheight {
    return 140;
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor colorWithHexString:@"#F2F3FB"];
        _bgView.layer.cornerRadius = 10;
        _bgView.clipsToBounds = YES;
    }
    return _bgView;
}

- (NSDictionary *)attr {
    NSDictionary * dc = @{
        NSFontAttributeName: [UIFont systemFontOfSize:15],
        NSForegroundColorAttributeName: [UIColor colorWithHexString:@"#1A1A1A"]
    };
    return dc;
}

- (NSDictionary *)numAttr {
    NSDictionary * dc = @{
        NSFontAttributeName: [UIFont systemFontOfSize:15],
        NSForegroundColorAttributeName: [UIColor colorWithHexString:@"#F83877"]
    };
    return dc;
}

- (UILabel *)goldlbl {
    if (!_goldlbl) {
        _goldlbl = [[UILabel alloc]init];
    }
    return _goldlbl;
}

- (UILabel *)charmlbl {
    if (!_charmlbl) {
        _charmlbl = [[UILabel alloc]init];
    }
    return _charmlbl;
}

- (UILabel *)explbl {
    if (!_explbl) {
        _explbl = [[UILabel alloc]init];
    }
    return _explbl;
}

@end
