//
//  AttributeIntroCell.m
//  SimpleDate
//
//  Created by houling on 2021/8/29.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "AttributeIntroCell.h"

@implementation AttributeIntroCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self p_ui];
    }
    return self;
}

- (void)p_ui {
    [self addSubview:self.bgView];
    [self.bgView addSubview:self.goldlbl];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self p_frame];
}

- (void)p_frame {
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(20);
        make.trailing.mas_equalTo(-20);
        make.top.mas_equalTo(0);
    }];
    
    [self.goldlbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(25);
        make.trailing.mas_equalTo(-25);
        make.top.mas_equalTo(20);
        make.bottom.mas_equalTo(-20);
    }];
}

- (UILabel *)goldlbl {
    if (!_goldlbl) {
        _goldlbl = [[UILabel alloc]init];
        _goldlbl.numberOfLines = 2;
        _goldlbl.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _goldlbl.font = [UIFont systemFontOfSize:13];
    }
    return _goldlbl;
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor colorWithHexString:@"#F2F3FB"];
        _bgView.layer.cornerRadius = 10;
        _bgView.clipsToBounds = YES;
    }
    return _bgView;
}

+(CGFloat)cellheight {
    return 100;
}

@end
