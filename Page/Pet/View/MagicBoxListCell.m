//
//  MagicBoxListCell.m
//  SimpleDate
//
//  Created by houling on 2021/7/24.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "MagicBoxListCell.h"

@interface MagicBoxListCell ()

@property(nonatomic, strong) UIView *bgView;
@property(nonatomic, strong) UILabel *titlelbl;
@property(nonatomic, strong) UIButton *oneBtn;
@property(nonatomic, strong) UIButton *tenBtn;
@property(nonatomic, strong) UIImageView *iconImageView;
@property(nonatomic, strong) UIImageView *drawImageView;
@property(nonatomic, strong) UIImageView *lajiimageview;
@property(nonatomic, strong) NSDictionary *dic;
@property(nonatomic, strong) NSDictionary *price;

@end

@implementation MagicBoxListCell

- (UIImageView *)lajiimageview {
    if (!_lajiimageview) {
        _lajiimageview = [UIImageView new];
    }
    return _lajiimageview;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupUI];
    }
    return  self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self setupFrames];
}

- (void)setupUI {
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.bgView];
    [self.bgView addSubview:self.lajiimageview];
    [self.bgView addSubview:self.iconImageView];
    [self.bgView addSubview:self.titlelbl];
    [self.bgView addSubview:self.oneBtn];
    [self.bgView addSubview:self.tenBtn];
  
}

- (void)setupFrames {
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.leading.mas_equalTo(20);
        make.trailing.mas_equalTo(-20);
        make.height.mas_equalTo(179);
    }];
    [self.titlelbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(18);
        make.top.mas_equalTo(20);
        make.width.mas_lessThanOrEqualTo(250);
    }];
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(20);
        make.trailing.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(154, 189));
    }];
    [self.oneBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titlelbl.mas_bottom).offset(10);
        make.leading.mas_equalTo(18);
        make.size.mas_equalTo(CGSizeMake(114, 34));
    }];
    [self.tenBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.oneBtn.mas_bottom).offset(10);
        make.leading.mas_equalTo(18);
        make.size.mas_equalTo(CGSizeMake(114, 34));
    }];
    [self.lajiimageview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.and.bottom.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(155, 115));
    }];
}

-(void)update:(NSDictionary *)dic {
    self.dic = [dic copy];
    self.titlelbl.text = dic[@"title"];
    self.drawImageView.image = [UIImage imageNamed:dic[@"drawer"]];
    self.iconImageView.image = [UIImage imageNamed:dic[@"img"]];
    if ([dic[@"type"] isEqualToString:@"noble"]) {
        self.lajiimageview.image = kIsMirroredLayout ? [UIImage imageNamed:@"bg_guizu"].mirroredImage : [UIImage imageNamed:@"bg_guizu"];
    } else if([dic[@"type"] isEqualToString:@"diamond"]) {
        self.lajiimageview.image = kIsMirroredLayout ? [UIImage imageNamed:@"bd_zuanshi"].mirroredImage : [UIImage imageNamed:@"bd_zuanshi"];
    } else {
        self.lajiimageview.image = kIsMirroredLayout ? [UIImage imageNamed:@"bg_jinbi"].mirroredImage : [UIImage imageNamed:@"bg_jinbi"];
    }
}

#pragma mark - handler

- (void)handlerOne:(UIButton *)sender {
    sender.userInteractionEnabled = NO;
    if (self.openBoxClicked) {
        self.openBoxClicked(self.dic, 1);
    }
    [self performSelector:@selector(handd) withObject:nil afterDelay:3];
}

- (void)handlerTen:(UIButton *)sender {
    sender.userInteractionEnabled = NO;
    if (self.openBoxClicked) {
        self.openBoxClicked(self.dic, 10);
    }
    [self performSelector:@selector(handd) withObject:nil afterDelay:3];
}

- (void)handd{
    self.tenBtn.userInteractionEnabled = YES;
    self.oneBtn.userInteractionEnabled = YES;
}

#pragma mark - getters

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 15;
    }
    return _bgView;
}

- (UIImageView *)iconImageView {
    if (!_iconImageView) {
        _iconImageView = [UIImageView new];
        _iconImageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _iconImageView;
}

- (UIImageView *)drawImageView {
    if (!_drawImageView) {
        _drawImageView = [UIImageView new];
    }
    return _drawImageView;
}

- (UILabel *)titlelbl {
    if (!_titlelbl) {
        _titlelbl = [UILabel new];
        _titlelbl.font = [UIFont fontWithName:@"PingFangSC-Medium" size:13];
        _titlelbl.textColor = [UIColor colorWithHexString:@"#4A4166"];
        _titlelbl.numberOfLines = 0;
    }
    return _titlelbl;
}

- (UIButton *)oneBtn {
    if (!_oneBtn) {
        _oneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _oneBtn.titleLabel.font = [UIFont mediumFontOfSize:15];
        [_oneBtn setTitle:SPDLocalizedString(@"开1次") forState:UIControlStateNormal];
        [_oneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _oneBtn.layer.cornerRadius = 17;
        [_oneBtn addTarget:self action:@selector(handlerOne:) forControlEvents:UIControlEventTouchUpInside];
        [_oneBtn setBackgroundImage:[UIImage imageNamed:@"ic_kaiyici"] forState:UIControlStateNormal];
    }
    return _oneBtn;
}

- (UIButton *)tenBtn {
    if (!_tenBtn) {
        _tenBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _tenBtn.titleLabel.font = [UIFont mediumFontOfSize:15];
        [_tenBtn setTitle:SPDLocalizedString(@"开10次") forState:UIControlStateNormal];
        [_tenBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _tenBtn.layer.cornerRadius = 17;
        [_tenBtn addTarget:self action:@selector(handlerTen:) forControlEvents:UIControlEventTouchUpInside];
        [_tenBtn setBackgroundImage:[UIImage imageNamed:@"ic_kaishici"] forState:UIControlStateNormal];
    }
    return _tenBtn;
}
@end
