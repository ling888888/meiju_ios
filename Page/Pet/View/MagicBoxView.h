//
//  MagicBoxView.h
//  SimpleDate
//
//  Created by houling on 2021/7/24.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class MagicBoxViewModel;

@interface MagicBoxView : UIView

@property (copy, nonatomic) void (^openBoxClicked)(NSDictionary * param, NSInteger num);

- (void)updatedic:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
