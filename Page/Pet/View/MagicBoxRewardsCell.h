//
//  MagicBoxRewardsCell.h
//  SimpleDate
//
//  Created by houling on 2021/8/1.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class PetSliceModel,DebrisModel;

@interface MagicBoxRewardsCell : UICollectionViewCell

@property (nonatomic, strong) PetSliceModel* sliceModel;

@property (nonatomic, strong) DebrisModel* debris;

@property (nonatomic, strong) UIImageView * bg;
@property (nonatomic, strong) UIImageView * icon;
@property (nonatomic, strong) UIImageView * qicon; // zhiliang
@property (nonatomic, strong) UILabel * num;

@end

NS_ASSUME_NONNULL_END
