//
//  PetUpgradeFuncView.h
//  SimpleDate
//
//  Created by houling on 2021/8/26.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LYPCustomAlertView.h"

NS_ASSUME_NONNULL_BEGIN
@class PetModel;
@interface PetUpgradeFuncView : LYPCustomAlertView

@property(nonatomic, assign) BOOL ishc;
@property(nonatomic, assign) BOOL iscommon;
- (void)updatePet:(PetModel *)pet status:(NSInteger)status;

@end

NS_ASSUME_NONNULL_END
