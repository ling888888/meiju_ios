//
//  PetRankTopCell.m
//  SimpleDate
//
//  Created by houling on 2021/10/16.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PetRankTopCell.h"
#import "PetLabelView.h"
#import "PetView.h"
#import "PetModel.h"

@interface PetRankTopCell ()

@property (nonatomic, strong) UIImageView * indexImageView;
@property (nonatomic, strong) PetView * petView;
@property (nonatomic, strong) PetLabelView * numLabel;
@property (nonatomic, strong) UIImageView * avatarImageView;
@property (nonatomic, strong) UILabel * nameLabel;

@end

@implementation PetRankTopCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.accessoryType = UITableViewCellAccessoryNone;
        [self setupUI];
    }
    return self;
}
- (void)handle:(UITapGestureRecognizer *)tap {
//    if (self.didTapdBlock) {
//        self.didTapdBlock(self.dict[@""]);
//    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self setupFrame];
}

- (void)setupUI {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.indexImageView];
    [self.contentView addSubview:self.petView];
    [self.contentView addSubview:self.numLabel];
    [self.contentView addSubview:self.avatarImageView];
    [self.contentView addSubview:self.nameLabel];
}

- (void)update:(NSDictionary *)dict type:(NSString *)type {
    NSInteger index = [dict[@"rank"] integerValue];
    if (index == 1) {
        self.indexImageView.image = [UIImage imageNamed:@"ic_paihangbang_diyi"];
    } else if (index == 2) {
        self.indexImageView.image = [UIImage imageNamed:@"ic_paihangbang_dier"];
    } else if (index == 3) {
        self.indexImageView.image = [UIImage imageNamed:@"ic_paihangbang_disan"];
    }
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:dict[@"avatar"]]]];
    self.nameLabel.text = dict[@"nickName"];
    if ([type isEqualToString:@"bill"]) {
        NSInteger gold = [dict[@"bill"] integerValue];
        [self.numLabel update:type num:gold];
    } if ([type isEqualToString:@"exp"]) {
        NSInteger gold = [dict[@"exp"] integerValue];
        [self.numLabel update:type num:gold];
    } if ([type isEqualToString:@"charm"]) {
        NSInteger gold = [dict[@"charm"] integerValue];
        [self.numLabel update:type num:gold];
    }
    PetModel * model = [[PetModel alloc]init];
    model.level = [dict[@"level"] integerValue];
    model.image = dict[@"image"];
    model.quality = dict[@"quality"];
    self.petView.pet = model;
}



- (void)setupFrame {
    [self.indexImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(31);
        make.leading.mas_equalTo(5);
        make.size.mas_equalTo(CGSizeMake(44, 32));
    }];
    [self.petView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.indexImageView.mas_trailing).offset(3);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(78, 84));
    }];
    [self.numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.petView.mas_trailing).offset(9);
        make.top.mas_equalTo(22 + 9);
        make.size.mas_equalTo(CGSizeMake(75, 26));
    }];
    [self.avatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.numLabel.mas_trailing).offset(15);
        make.centerY.equalTo(self.numLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(34, 34));
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.avatarImageView.mas_trailing).offset(15);
        make.centerY.equalTo(self.numLabel.mas_centerY);
        make.trailing.mas_equalTo(-15);
    }];
}

- (UIImageView *)indexImageView {
    if (!_indexImageView) {
        _indexImageView = [UIImageView new];
    }
    return _indexImageView;
}

- (PetView *)petView {
    if (!_petView) {
        _petView = [PetView new];
        _petView.userInteractionEnabled = YES;
        UITapGestureRecognizer * TAP = [[UITapGestureRecognizer alloc]initWithTarget:self  action:@selector(handle:)];
        [_petView addGestureRecognizer:TAP];
    }
    return _petView;
}

- (UIImageView *)avatarImageView {
    if (!_avatarImageView) {
        _avatarImageView = [UIImageView new];
        _avatarImageView.layer.cornerRadius = 17;
        _avatarImageView.clipsToBounds = true;
    }
    return _avatarImageView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.font = [UIFont systemFontOfSize:15];
        _nameLabel.textColor = [UIColor whiteColor];
    }
    return _nameLabel;
}

- (PetLabelView *)numLabel {
    if (!_numLabel) {
        _numLabel = [PetLabelView new];
        _numLabel.layer.cornerRadius = 13;
        _numLabel.clipsToBounds = true;
    }
    return _numLabel;
}

@end
