//
//  PetUpgradeFuncView.m
//  SimpleDate
//
//  Created by houling on 2021/8/26.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PetUpgradeFuncView.h"
#import "PetView.h"
#import "PetModel.h"

@interface PetUpgradeFuncView()
@property (nonatomic, strong) UIImageView * light;
@property (nonatomic, strong) UIImageView * dian;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIImageView * titleBg;
@property (nonatomic, strong) PetView * petView;
@property (nonatomic, strong) UIButton * cButton;
@property (nonatomic, strong) UIView * greenView;

@property (nonatomic, strong) UILabel * namelBl;
@property (nonatomic, strong) UILabel * shuxingLabel;
@property (nonatomic, strong) UILabel * goldlbl;
@property (nonatomic, strong) UILabel * charmlbl;
@property (nonatomic, strong) UILabel * levellbl;
@property (nonatomic, strong) UIImageView * leftdouhaoImage;
@property (nonatomic, strong) UIImageView * rightdouhaoImage;
@property (nonatomic, strong) UILabel * descLabel;

@end

@implementation PetUpgradeFuncView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self p_setui];
    }
    return self;
}

- (void)p_setui {
    [self.containerView addSubview:self.titleBg];
    [self.containerView addSubview:self.titleLabel];
    [self.containerView addSubview:self.petView];
    [self.containerView addSubview:self.namelBl];
    [self.containerView addSubview:self.leftdouhaoImage];
    [self.containerView addSubview:self.rightdouhaoImage];
    [self.containerView addSubview:self.shuxingLabel];
    [self.containerView addSubview:self.goldlbl];
    [self.containerView addSubview:self.levellbl];
    [self.containerView addSubview:self.charmlbl];
    [self.containerView addSubview:self.greenView];
    [self.containerView addSubview:self.descLabel];

    [self.containerView addSubview:self.cButton];
    [self.containerView insertSubview:self.light belowSubview:self.petView];
    [self.containerView insertSubview:self.dian belowSubview:self.light];

    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(24);
        make.leading.mas_equalTo(36);
        make.trailing.mas_equalTo(-36);
        make.height.mas_equalTo(80);
    }];
    
    [self.titleBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.top.trailing.mas_equalTo(0);
        make.height.mas_equalTo(109);
        make.trailing.mas_equalTo(0);
        make.width.mas_equalTo(295);
    }];
    
    [self.namelBl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleBg.mas_bottom).offset(12);
        make.centerX.mas_equalTo(0);
    }];
    
    [self.petView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.namelBl.mas_bottom).offset(8);
        make.size.mas_equalTo(134);
        make.centerX.equalTo(self);
    }];
    
    [self.light mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(199);
        make.centerX.equalTo(self);
        make.centerY.equalTo(self.petView.mas_centerY);
    }];
    [self.dian mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(255);
        make.height.mas_equalTo(185);
        make.centerX.equalTo(self);
        make.centerY.equalTo(self.petView.mas_centerY);
    }];
    
    [self.descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.petView.mas_bottom).offset(31);
        make.centerX.mas_equalTo(0);
        make.height.mas_greaterThanOrEqualTo(14);
        make.width.mas_lessThanOrEqualTo(199);
    }];
    
    [self.greenView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.descLabel.mas_bottom).offset(23);
        make.width.mas_equalTo(2);
        make.height.mas_equalTo(13);
        make.leading.mas_equalTo(26);
    }];
    [self.shuxingLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.descLabel.mas_bottom).offset(21);
        make.leading.equalTo(self.greenView.mas_trailing).offset(6);
    }];
    [self.goldlbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.shuxingLabel.mas_bottom).offset(13);
        make.leading.equalTo(self.shuxingLabel.mas_leading).offset(0);
    }];
    [self.charmlbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goldlbl.mas_bottom).offset(13);
        make.leading.equalTo(self.shuxingLabel.mas_leading).offset(0);
    }];
    [self.levellbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.charmlbl.mas_bottom).offset(13);
        make.leading.equalTo(self.shuxingLabel.mas_leading).offset(0);
    }];
    [self.leftdouhaoImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(11);
        make.trailing.equalTo(self.descLabel.mas_leading).offset(-6);
        make.top.equalTo(self.descLabel.mas_top).offset(2);
    }];
    [self.rightdouhaoImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(11);
        make.leading.equalTo(self.descLabel.mas_trailing).offset(6);
        make.top.equalTo(self.descLabel.mas_top).offset(2);
    }];

    [self.cButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.levellbl.mas_bottom).offset(25);
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(180);
        make.height.mas_equalTo(44);
        make.bottom.mas_equalTo(-25);
    }];
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.containerView addRounded:UIRectCornerTopLeft | UIRectCornerBottomRight withRadius:30];
}

- (NSMutableDictionary<NSAttributedStringKey, id> *)getDetailTextAttributes {
    // 设置行高为17
    CGFloat lineHeight = 24;
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.maximumLineHeight = lineHeight;
    paragraphStyle.minimumLineHeight = lineHeight;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    [attributes setObject:paragraphStyle forKey:NSParagraphStyleAttributeName];
    [attributes setObject:[UIFont boldSystemFontOfSize:15] forKey:NSFontAttributeName];
    CGFloat baselineOffset = (lineHeight - [UIFont boldSystemFontOfSize:15].lineHeight) / 4;
    [attributes setObject:[UIColor colorWithHexString:@"#1A1A1A"] forKey:NSForegroundColorAttributeName];
    [attributes setObject:@(baselineOffset) forKey:NSBaselineOffsetAttributeName];
    return attributes;
}

- (void)updatePet:(PetModel *)pet status:(NSInteger)status {
    [self.petView setPet:pet];
    if (!self.ishc && !self.iscommon) {
        if (status == 0) {
            NSString * title = [NSString stringWithFormat:@"很遗憾，升级失败，宠物已回到LV.%@".localized,@(pet.level)];
            self.titleLabel.attributedText = [[NSAttributedString alloc]initWithString:title attributes:[self getDetailTextAttributes]];
            self.titleBg.image = [UIImage mirroredImageNamed:@"img_tanchuangdingtu-1"];
            self.light.hidden = YES;
            self.dian.hidden = YES;
        } else {
            NSString * title = [NSString stringWithFormat:@"恭喜你，升级成功，宠物已升级到LV.%@".localized,@(pet.level)];
            self.titleLabel.attributedText = [[NSAttributedString alloc]initWithString:title attributes:[self getDetailTextAttributes]];
        }
    } else if(_iscommon) {
        self.titleLabel.attributedText = [[NSAttributedString alloc]initWithString:@"查看宠物属性".localized attributes:[self getDetailTextAttributes]];
    } else if(_ishc) {
        self.titleLabel.attributedText = [[NSAttributedString alloc]initWithString:@"恭喜你，获得以下宠物碎片， 已放进背包中。".localized attributes:[self getDetailTextAttributes]];
    }
    [SPDCommonTool getFamyLanguage];
    self.namelBl.text = pet.type;
    NSString *currentLanguage = [[SPDLanguageTool sharedInstance].currentLanguage substringToIndex:2];
    if ([currentLanguage isEqualToString:@"zh"]) {
        self.descLabel.text = pet.desc_zh;
    } else if ([currentLanguage isEqualToString:@"en"]) {
        self.descLabel.text = pet.desc_en;
    } else {
        self.descLabel.text = pet.desc_ar;
    }
    for (int i = 0; i < pet.attr.count; i++) {
        NSDictionary * dict = pet.attr[i];
        NSString * text = @"";
        if ([dict[@"name"] isEqualToString:@"bill"]) {
            text = [NSString stringWithFormat:@"每天可领取%@金币。".localized,dict[@"value"]];
        } else if ([dict[@"name"] isEqualToString:@"charm"]) {
            text = [NSString stringWithFormat:@"每天可领取%@魅力值。".localized,dict[@"value"]];
        } else if ([dict[@"name"] isEqualToString:@"exp"]) {
            text = [NSString stringWithFormat:@"每天可领取%@个人等级经验值。".localized,dict[@"value"]];
        }
        if (i == 0) {
            self.goldlbl.hidden = NO;
            self.goldlbl.text = text;
        } else if(i == 1) {
            self.charmlbl.hidden = NO;
            self.charmlbl.text = text;
        }else if(i == 2) {
            self.levellbl.hidden = NO;
            self.levellbl.text = text;
        }
    }
    [self.containerView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(295);
    }];
}

- (UIImageView *)titleBg {
    if (!_titleBg) {
        _titleBg = [UIImageView new];
        _titleBg.mirrorImage = YES;
        _titleBg.image = [UIImage imageNamed:@"img_tanchuangdingtu"];
    }
    return _titleBg;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#392912"];
        _titleLabel.font = [UIFont boldSystemFontOfSize:15];
        _titleLabel.numberOfLines = 0;
        _titleLabel.minimumScaleFactor = 2;
    }
    return _titleLabel;
}
- (PetView *)petView {
    if (!_petView) {
        _petView = [[PetView alloc]init];
        _petView.layer.cornerRadius = 5;
        [_petView setLevelBGColor:[UIColor colorWithHexString:@"#00D8C9"]];
    }
    return _petView;
}

- (UIButton *)cButton {
    if (!_cButton) {
        _cButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _cButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [_cButton setTitle:@"好的".localized forState:UIControlStateNormal];
        [_cButton setBackgroundColor:[UIColor colorWithHexString:@"#FBA22A"]];
        _cButton.layer.cornerRadius = 22;
        [_cButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cButton;
}

- (UILabel *)shuxingLabel {
    if (!_shuxingLabel) {
        _shuxingLabel = [UILabel new];
        _shuxingLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _shuxingLabel.font = [UIFont boldSystemFontOfSize:15];
        _shuxingLabel.text = @"属性".localized;
    }
    return _shuxingLabel;
}

- (UIView *)greenView {
    if (!_greenView) {
        _greenView = [UIView new];
        _greenView.backgroundColor = [UIColor colorWithHexString:@"#00D4A0"];
    }
    return _greenView;
}

- (UILabel *)goldlbl {
    if (!_goldlbl) {
        _goldlbl = [UILabel new];
        _goldlbl.textColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.8];
        _goldlbl.font = [UIFont boldSystemFontOfSize:12];
        _goldlbl.hidden = true;
    }
    return _goldlbl;
}

- (UILabel *)charmlbl {
    if (!_charmlbl) {
        _charmlbl = [UILabel new];
        _charmlbl.textColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.8];
        _charmlbl.font = [UIFont boldSystemFontOfSize:12];
        _charmlbl.hidden = true;
    }
    return _charmlbl;
}

- (UILabel *)levellbl {
    if (!_levellbl) {
        _levellbl = [UILabel new];
        _levellbl.textColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.8];
        _levellbl.font = [UIFont boldSystemFontOfSize:12];
        _levellbl.hidden = true;
    }
    return _levellbl;
}

- (UIImageView *)leftdouhaoImage {
    if (!_leftdouhaoImage) {
        _leftdouhaoImage = [UIImageView new];
        _leftdouhaoImage.image = [UIImage imageNamed:@"ic_gerenzhuye_gerenjianjie_zuo"];
    }
    return _leftdouhaoImage;
}

- (UIImageView *)rightdouhaoImage {
    if (!_rightdouhaoImage) {
        _rightdouhaoImage = [UIImageView new];
        _rightdouhaoImage.image = [UIImage imageNamed:@"ic_gerenzhuye_gerenjianjie_you"];
    }
    return _rightdouhaoImage;
}

- (UILabel *)namelBl {
    if (!_namelBl) {
        _namelBl = [UILabel new];
        _namelBl.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _namelBl.font = [UIFont boldSystemFontOfSize:18];
        _namelBl.text = @"宠物名字".localized;
    }
    return _namelBl;
}

- (UILabel *)descLabel {
    if (!_descLabel) {
        _descLabel = [UILabel new];
        _descLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _descLabel.font = [UIFont boldSystemFontOfSize:14];
    }
    return _descLabel;
}

- (UIImageView *)light {
    if (!_light) {
        _light = [UIImageView new];
        _light.image = [UIImage imageNamed:@"img_guang"];
    }
    return _light;
}

- (UIImageView *)dian {
    if (!_dian) {
        _dian = [UIImageView new];
        _dian.image = [UIImage imageNamed:@"bg_tanchuangyuansu"];
    }
    return _dian;
}
@end
