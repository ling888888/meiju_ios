//
//  SPCell.m
//  SimpleDate
//
//  Created by houling on 2021/8/21.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "SPCell.h"
#import "DebrisModel.h"

@interface SPCell ()

@property (nonatomic, strong) UIImageView * pintuImageView;
@property (nonatomic, strong) UIImageView * lightImageView;
@property (nonatomic, strong) UIImageView * petImageView;
@property (nonatomic, strong) UIImageView * bottom1;
@property (nonatomic, strong) UIImageView * bottom2;
@property (nonatomic, strong) UILabel * levelLabel;

@end

@implementation SPCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.lightImageView];
        [self.contentView addSubview:self.bottom2];
        [self.contentView addSubview:self.bottom1];
        [self.contentView addSubview:self.pintuImageView];
        [self.contentView addSubview:self.petImageView];
        [self.contentView addSubview:self.levelLabel];

    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.lightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(15);
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(90, 62));
    }];
    [self.pintuImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(5);
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(68, 64));
    }];
    [self.petImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(11);
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(44, 44));
    }];
    [self.bottom1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.pintuImageView.mas_bottom).offset(-8);
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(68, 18));
    }];
    [self.bottom2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.pintuImageView.mas_bottom).offset(0);
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(68, 29));
    }];
    [self.levelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bottom2.mas_bottom).offset(-9);
        make.leading.mas_equalTo(12);
        make.size.mas_equalTo(CGSizeMake(68, 18));
    }];
}

- (void)setDebris:(DebrisModel *)debris {
    _debris = debris;
    self.levelLabel.text = [NSString stringWithFormat:@"x%ld",debris.num];
    self.bottom1.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_%@_tuoyuan",debris.quality]];
    self.bottom2.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_%@_dizuo",debris.quality]];
    self.pintuImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_suipian_%@",debris.quality]];
    [self.petImageView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:debris.cover]] placeholderImage:[UIImage imageNamed:@"img_portrait_placeholder"]];
}


#pragma mark - getter

- (UIImageView *)lightImageView {
    if (!_lightImageView) {
        _lightImageView = [UIImageView new];
        _lightImageView.image = [UIImage imageNamed:@"bg_guang"];
    }
    return _lightImageView;
}

- (UIImageView *)petImageView {
    if (!_petImageView) {
        _petImageView = [UIImageView new];
    }
    return _petImageView;
}

- (UIImageView *)bottom1 {
    if (!_bottom1) {
        _bottom1 = [UIImageView new];
    }
    return _bottom1;
}

- (UIImageView *)bottom2 {
    if (!_bottom2) {
        _bottom2 = [UIImageView new];
    }
    return _bottom2;
}

- (UILabel *)levelLabel {
    if (!_levelLabel) {
        _levelLabel = [UILabel new];
        _levelLabel.backgroundColor = [[UIColor colorWithHexString:@"000000"] colorWithAlphaComponent:0.3];
        _levelLabel.textColor = [UIColor whiteColor];
        _levelLabel.font = [UIFont mediumFontOfSize:12];
        _levelLabel.layer.cornerRadius = 9;
        _levelLabel.clipsToBounds = YES;
        _levelLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _levelLabel;
}

- (UIImageView *)pintuImageView {
    if (!_pintuImageView) {
        _pintuImageView = [UIImageView new];
    }
    return _pintuImageView;
}

@end
