//
//  PetView.h
//  SimpleDate
//
//  Created by houling on 2021/8/24.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class PetModel;
@interface PetView : UIView

@property(nonatomic, strong) PetModel * pet;

- (void)updatecover:(NSString*)url;

- (void)setLevelBGColor:(UIColor *)color;

@end

NS_ASSUME_NONNULL_END
