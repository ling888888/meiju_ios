//
//  DecomposeAlertView.h
//  SimpleDate
//
//  Created by houling on 2021/8/29.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LYPCustomAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@class  PetModel;
@interface DecomposeAlertView : LYPCustomAlertView

@property (nonatomic, copy) void (^didTapDecomposeBlock)(PetModel *pet);

@property (nonatomic, strong) PetModel * pet;

@end

NS_ASSUME_NONNULL_END
