//
//  PetUpgradeView.h
//  SimpleDate
//
//  Created by houling on 2021/8/25.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LYPCustomAlertView.h"

NS_ASSUME_NONNULL_BEGIN
@class PetModel;
@interface PetUpgradeView : LYPCustomAlertView

@property (nonatomic, copy) void (^didTapUpgradeBlock)(PetModel *pet);

- (void)updatePet:(PetModel *)pet config:(NSDictionary *)config;

@end

NS_ASSUME_NONNULL_END
