//
//  PetView.m
//  SimpleDate
//
//  Created by houling on 2021/8/24.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PetView.h"
#import "PetModel.h"

@interface PetView ()

@property (nonatomic, strong) UIImageView * lightImageView;
@property (nonatomic, strong) UIImageView * petImageView;
@property (nonatomic, strong) UIImageView * bottom1;
@property (nonatomic, strong) UIImageView * bottom2;
@property (nonatomic, strong) UILabel * levelLabel;

@end

@implementation PetView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.lightImageView];
        [self addSubview:self.bottom2];
        [self addSubview:self.bottom1];
        [self addSubview:self.petImageView];
        [self addSubview:self.levelLabel];
        [self.lightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.centerX.mas_equalTo(0);
            make.width.equalTo(self);
            make.height.mas_equalTo(self.lightImageView.mas_width).multipliedBy(67.0/92);
        }];
        [self.petImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(2);
            make.centerX.mas_equalTo(0);
            make.width.equalTo(self).multipliedBy(68.0/92);
            make.height.equalTo(self.petImageView.mas_width).multipliedBy(1.0);
        }];
        [self.bottom1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(0);
            make.width.equalTo(self.petImageView.mas_width);
            make.height.equalTo(self.bottom1.mas_width).multipliedBy(18.0/68);
            make.bottom.equalTo(self.petImageView.mas_bottom).offset(5);
        }];
        [self.bottom2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(0);
            make.top.equalTo(self.bottom1.mas_bottom).offset(0);
            make.width.equalTo(self.petImageView.mas_width);
            make.height.equalTo(self.bottom2.mas_width).multipliedBy(18.0/68);
        }];
        [self.levelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.mas_bottom).offset(0);
            make.width.equalTo(self).multipliedBy(67.0/92);
            make.centerX.mas_equalTo(0);
            make.height.equalTo(self.levelLabel.mas_width).multipliedBy(18.0/68);
        }];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.levelLabel.layer.cornerRadius = CGRectGetHeight(self.levelLabel.frame) / 2.0;
    self.levelLabel.clipsToBounds = YES;
    [self.bottom2 mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bottom1.mas_bottom).offset(-CGRectGetHeight(self.bottom1.frame) / 2.0);
    }];
}

- (void)setPet:(PetModel *)pet {
    _pet = pet;
    self.levelLabel.text = [NSString stringWithFormat:@"LV.%ld",pet.level];
    self.bottom1.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_%@_tuoyuan",pet.quality]];
    self.bottom2.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_%@_dizuo",pet.quality]];
    [self.petImageView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:pet.image]] placeholderImage:[UIImage imageNamed:@"img_portrait_placeholder"]];
}

- (void)updatecover:(NSString*)url {
    [self.petImageView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:url]] placeholderImage:[UIImage imageNamed:@"img_portrait_placeholder"]];
}

- (void)setLevelBGColor:(UIColor *)color {
    [self.levelLabel setBackgroundColor:color];
}

#pragma mark - getter

- (UIImageView *)lightImageView {
    if (!_lightImageView) {
        _lightImageView = [UIImageView new];
        _lightImageView.image = [UIImage imageNamed:@"bg_guang"];
        _lightImageView.contentMode = UIViewContentModeScaleAspectFill;
        _lightImageView.clipsToBounds = YES;
    }
    return _lightImageView;
}

- (UIImageView *)petImageView {
    if (!_petImageView) {
        _petImageView = [UIImageView new];
        _petImageView.contentMode = UIViewContentModeScaleAspectFill;
        _petImageView.clipsToBounds = YES;
    }
    return _petImageView;
}

- (UIImageView *)bottom1 {
    if (!_bottom1) {
        _bottom1 = [UIImageView new];
        _bottom1.contentMode = UIViewContentModeScaleAspectFit;
        _bottom1.clipsToBounds = YES;
    }
    return _bottom1;
}

- (UIImageView *)bottom2 {
    if (!_bottom2) {
        _bottom2 = [UIImageView new];
        _bottom2.contentMode = UIViewContentModeScaleAspectFill;
        _bottom2.clipsToBounds = YES;
    }
    return _bottom2;
}

- (UILabel *)levelLabel {
    if (!_levelLabel) {
        _levelLabel = [UILabel new];
        _levelLabel.backgroundColor = [[UIColor colorWithHexString:@"000000"] colorWithAlphaComponent:0.3];
        _levelLabel.textColor = [UIColor whiteColor];
        _levelLabel.font = [UIFont mediumFontOfSize:12];
        _levelLabel.layer.cornerRadius = 9;
        _levelLabel.clipsToBounds = YES;
        _levelLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _levelLabel;
}

@end
