//
//  PetUpgradeView.m
//  SimpleDate
//
//  Created by houling on 2021/8/25.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PetUpgradeView.h"
#import "UIView+LY.h"
#import "LY_GradientView.h"
#import "MagicBoxRewardsCell.h"
#import "PetModel.h"
#import "PetSliceModel.h"

@interface PetUpgradeView ()

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIImageView * titleBg;

@property (nonatomic, strong) MagicBoxRewardsCell * goldView;
@property (nonatomic, strong) MagicBoxRewardsCell * petView;
@property (nonatomic, strong) UILabel * descLabel;
@property (nonatomic, strong) UIButton * upgradeBtn;
@property (nonatomic, strong) UIButton * cButton;
@property (nonatomic, strong) PetModel * currPet;

@end

@implementation PetUpgradeView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self p_setui];
    }
    return self;
}

- (void)p_setui {
    [self.containerView addSubview:self.titleBg];
    [self.containerView addSubview:self.titleLabel];
    [self.containerView addSubview:self.goldView];
    [self.containerView addSubview:self.petView];
    [self.containerView addSubview:self.descLabel];
    [self.containerView addSubview:self.upgradeBtn];
    [self.containerView addSubview:self.cButton];

    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(24);
        make.leading.mas_equalTo(36);
        make.trailing.mas_equalTo(-36);
        make.height.mas_equalTo(80);
    }];
    [self.titleBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.top.trailing.mas_equalTo(0);
        make.height.mas_equalTo(109);
        make.trailing.mas_equalTo(0);
        make.width.mas_equalTo(295);
    }];
    
    [self.petView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleBg.mas_bottom).offset(22);
        make.size.mas_equalTo(81);
        make.centerX.equalTo(self).offset(-81/2-13);
    }];
    
    
    [self.goldView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleBg.mas_bottom).offset(22);
        make.centerX.equalTo(self).offset(81/2+13);
        make.size.mas_equalTo(81);
    }];
    
    [self.descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.petView.mas_bottom).offset(26);
        make.leading.mas_equalTo(38);
        make.trailing.mas_equalTo(-38);
    }];
    
    [self.upgradeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.descLabel.mas_bottom).offset(30);
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(180);
        make.height.mas_equalTo(44);
        make.bottom.mas_equalTo(-68);
    }];
    
    [self.cButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.upgradeBtn.mas_bottom).offset(8);
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(180);
        make.height.mas_equalTo(44);
    }];
    
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.containerView addRounded:UIRectCornerTopLeft | UIRectCornerBottomRight withRadius:30];
}

- (NSMutableDictionary<NSAttributedStringKey, id> *)getDetailTextAttributes {
    // 设置行高为17
    CGFloat lineHeight = 24;
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.maximumLineHeight = lineHeight;
    paragraphStyle.minimumLineHeight = lineHeight;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    [attributes setObject:paragraphStyle forKey:NSParagraphStyleAttributeName];
    [attributes setObject:[UIFont boldSystemFontOfSize:13] forKey:NSFontAttributeName];
    CGFloat baselineOffset = (lineHeight - [UIFont boldSystemFontOfSize:13].lineHeight) / 4;
    [attributes setObject:[UIColor colorWithHexString:@"#1A1A1A"] forKey:NSForegroundColorAttributeName];
    [attributes setObject:@(baselineOffset) forKey:NSBaselineOffsetAttributeName];
    return attributes;
}


#pragma mark - action

- (void)hanleUpgrade:(UIButton *)sender {
    [self dismiss];
    if (self.didTapUpgradeBlock) {
        self.didTapUpgradeBlock(self.currPet);
    }
}

- (void)updatePet:(PetModel *)pet config:(NSDictionary *)config {
    self.currPet = pet;
    if (pet.level > 0) {
        self.descLabel.attributedText = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"你有 %@ %%的概率升级成功，若升级失败，宠物将会回到LV.%@。".localized, config[@"probability"],@(pet.level > 0 ? pet.level - 1 : pet.level)] attributes:[self getDetailTextAttributes]];
    } else {
        self.descLabel.attributedText = [[NSAttributedString alloc]initWithString:@"你有100%的概率升级成功".localized attributes:[self getDetailTextAttributes]];
    }
    self.titleLabel.attributedText = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"宠物升级后，属性带来的奖励将会增加，当前宠物等级为LV.%@，升到 LV.%@需要以下物品".localized, @(pet.level),@(pet.level + 1)] attributes:[self getDetailTextAttributes]];
    PetSliceModel * sp = [PetSliceModel new];
    sp.rewardType =  PetSliceRewardTypeDebris;
    sp.cover = pet.cover;
    sp.quality = pet.quality;
    sp.imageName = [NSString stringWithFormat:@"bg_suipian_%@",pet.quality];
    sp.num = config[@"debris"];
    self.petView.sliceModel = sp;
    
    PetSliceModel * gold = [PetSliceModel new];
    gold.rewardType =  PetSliceRewardTypeCoins;
    gold.cover = @"";
    gold.num = config[@"bill"];
    self.goldView.sliceModel = gold;
}

#pragma mark - getter

- (UIImageView *)titleBg {
    if (!_titleBg) {
        _titleBg = [UIImageView new];
        _titleBg.mirrorImage = YES;
        _titleBg.image = [UIImage imageNamed:@"img_tanchuangdingtu"];
    }
    return _titleBg;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#392912"];
        _titleLabel.font = [UIFont boldSystemFontOfSize:13];
        _titleLabel.numberOfLines = 0;
        _titleLabel.minimumScaleFactor = 2;
    }
    return _titleLabel;
}

- (UILabel *)descLabel {
    if (!_descLabel) {
        _descLabel = [UILabel new];
        _descLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _descLabel.font = [UIFont boldSystemFontOfSize:15];
        _descLabel.numberOfLines = 0;
    }
    return _descLabel;
}

- (MagicBoxRewardsCell *)petView {
    if (!_petView) {
        _petView = [[MagicBoxRewardsCell alloc]init];
        _petView.layer.cornerRadius = 5;
    }
    return _petView;
}

- (MagicBoxRewardsCell *)goldView {
    if (!_goldView) {
        _goldView = [[MagicBoxRewardsCell alloc]init];
        _goldView.layer.cornerRadius = 5;
    }
    return _goldView;
}

- (UIButton *)cButton {
    if (!_cButton) {
        _cButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cButton setTitleColor:[UIColor colorWithHexString:@"392912"] forState:UIControlStateNormal];
        _cButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [_cButton setTitle:@"取消".localized forState:UIControlStateNormal];
        [_cButton setBackgroundColor:[UIColor clearColor]];
        [_cButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cButton;
}

- (UIButton *)upgradeBtn {
    if (!_upgradeBtn) {
        _upgradeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _upgradeBtn.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [_upgradeBtn setTitle:@"升级".localized forState:UIControlStateNormal];
        [_upgradeBtn setBackgroundColor:[UIColor colorWithHexString:@"#FBA22A"]];
        _upgradeBtn.layer.cornerRadius = 22;
        [_upgradeBtn addTarget:self action:@selector(hanleUpgrade:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _upgradeBtn;
}


@end
