//
//  MagicBoxViewModel.h
//  SimpleDate
//
//  Created by houling on 2021/7/24.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MagicBoxViewModel : NSObject

@property (nonatomic, strong) NSDictionary * dict;

@end

NS_ASSUME_NONNULL_END
