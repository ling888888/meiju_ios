//
//  PetSliceModel.h
//  SimpleDate
//
//  Created by houling on 2021/8/1.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "CommonModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, PetSliceRewardType) {
    PetSliceRewardTypeDebris,
    PetSliceRewardTypeCoins
};
// 碎片的model
@interface PetSliceModel : CommonModel

@property (nonatomic, assign) PetSliceRewardType rewardType;
@property (nonatomic, copy) NSString * type;
@property (nonatomic, copy) NSString * postion;//pet
@property (nonatomic, copy) NSString * cover;//cover
@property (nonatomic, copy) NSString * good_id;
@property (nonatomic, copy) NSNumber * num;
@property (nonatomic, copy) NSString * imageName;
@property (nonatomic, copy) NSString * quality;// green

@end

NS_ASSUME_NONNULL_END
