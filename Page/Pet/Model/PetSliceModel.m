//
//  PetSliceModel.m
//  SimpleDate
//
//  Created by houling on 2021/8/1.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PetSliceModel.h"

@implementation PetSliceModel

- (void)setValue:(id)value forKey:(NSString *)key {
    [super setValue:value forKey:key];
    
    if ([key isEqualToString:@"quality"]) {
        self.imageName = [NSString stringWithFormat:@"bg_suipian_%@",value];
    }
    if ([key isEqualToString:@"type"]) {
        if ([value isEqualToString:@"debris"]) {
            self.rewardType = PetSliceRewardTypeDebris;
        } else {
            self.rewardType = PetSliceRewardTypeCoins;
        }
    }

}

@end
