//
//  PetModel.h
//  SimpleDate
//
//  Created by houling on 2021/8/15.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
/**
 宠物的model
 */

@interface PetModel : CommonModel

@property (nonatomic, copy) NSString * pid; // 背包里饰品安全id
@property(nonatomic,assign) BOOL selec;
@property (nonatomic, copy) NSString * user_id;
@property (nonatomic, copy) NSString * postion;//pet
@property (nonatomic, assign) NSInteger level;
@property (nonatomic, copy) NSString * quality;// green
@property (nonatomic, assign) NSInteger  equip;// 0/1 未装备/装备中
@property (nonatomic, copy) NSString * type;//cat
@property (nonatomic, copy) NSString * good_id;
@property (nonatomic, copy) NSString * image;
@property (nonatomic, copy) NSString * cover;
@property (nonatomic, copy) NSString * desc_en;
@property (nonatomic, copy) NSString * desc_ar;
@property (nonatomic, copy) NSString * desc_zh;
@property (nonatomic, assign) BOOL  is_delete;
@property (nonatomic, copy) NSArray * attr;
@property (nonatomic, copy) NSDictionary * desc;//{zh en ar}
@property (nonatomic, copy) NSDictionary * name;//{zh en ar}
@property (nonatomic, copy) NSString * nameStr;
@property (nonatomic, copy) NSString * descStr;

@property (nonatomic, assign) NSInteger resolveToNextLevelNum; // 分解到高等级获得的碎片数量
@property (nonatomic, copy) NSString* resolveToNextLevel; // "green" 分解到高等级获得的碎片品质


@end

NS_ASSUME_NONNULL_END
