//
//  DebrisModel.h
//  SimpleDate
//
//  Created by houling on 2021/8/21.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "CommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface DebrisModel : CommonModel

@property (nonatomic, copy) NSString * sid;

@property (nonatomic, copy) NSString * user_id;
@property (nonatomic, copy) NSString * postion;//pet
@property (nonatomic, copy) NSString * good_id;
@property (nonatomic, copy) NSString * quality;// green
@property (nonatomic, copy) NSString * type;//cat
@property (nonatomic, assign) NSInteger num;

@property (nonatomic, copy) NSString * image;
@property (nonatomic, copy) NSString * cover;
@property (nonatomic, assign) NSInteger resolveToNextLevelNum; // 分解到高等级获得的碎片数量



@end

NS_ASSUME_NONNULL_END
