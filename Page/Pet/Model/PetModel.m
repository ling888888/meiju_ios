//
//  PetModel.m
//  SimpleDate
//
//  Created by houling on 2021/8/15.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PetModel.h"

@implementation PetModel


+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"pid": @"id"
             };
}

- (NSString *)nameStr {
    if ([self.name isKindOfClass:[NSDictionary class]] && self.name.allKeys >0) {
        NSString *currentLanguage = [[SPDLanguageTool sharedInstance].currentLanguage substringToIndex:2];
        return self.name[currentLanguage];
    }
    return  nil;
}

- (NSString *)descStr {
    if ([self.desc isKindOfClass:[NSDictionary class]] && self.desc.allKeys >0) {
        NSString *currentLanguage = [[SPDLanguageTool sharedInstance].currentLanguage substringToIndex:2];
        return self.desc[currentLanguage];
    }
    return  nil;
}

@end
