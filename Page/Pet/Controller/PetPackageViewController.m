//
//  PetPackageViewController.m
//  SimpleDate
//
//  Created by houling on 2021/8/14.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PetPackageViewController.h"
#import "PetCell.h"
#import "PetModel.h"
#import "DebrisModel.h"
#import "SPCell.h"
#import "LY_HalfNavigationController.h"
#import "SPDHelpController.h"
#import "PetDetailAlertView.h"
#import "PetUpgradeView.h"
#import "PetUpgradeFuncView.h"
#import "MyWalletViewController.h"
#import "NeedSPView.h"
#import "DecomposeAlertView.h"
#import "PetSliceModel.h"
#import "DecomposeResultView.h"
#import "SPAlertView.h"
#import "SPHCAlertView.h"
#import "DHSPView.h"

@interface PetPackageViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UIImageView * bg;
@property (nonatomic, strong) UILabel * numLbl;
@property (nonatomic, copy) NSString * type;
@property (nonatomic, copy) NSDictionary * allConfig;
@end

@implementation PetPackageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"背包".localized;
    self.type = @"whole";
    [self setupUI];
    [self setupUIFrame];
    [self loadNetDataWith:RequestNetDataTypeRefresh];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self requestAllConfig];
}

- (void)setupUI {
    self.navigationController.navigationBar.tintColor = [[UIColor colorWithHexString:@"#000000"] colorWithAlphaComponent:0.2];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"ffffff"],
                                                                    NSFontAttributeName: [UIFont mediumFontOfSize:18]
    };
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:0];
    [self.navigationController.navigationBar vhl_setShadowImageHidden:true];
    [self.navigationController.navigationBar setTranslucent:true];
    if (@available(iOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ic_zhubo_guize_bai"] style:UIBarButtonItemStylePlain target:self action:@selector(handlSettings:)];
    
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.mj_footer = nil;
    [self.collectionView registerClass:[PetCell class] forCellWithReuseIdentifier:@"PetCell"];
    [self.collectionView registerClass:[SPCell class] forCellWithReuseIdentifier:@"SPCell"];
    self.collectionView.mj_header = nil;
    
    self.collectionView.mj_footer = nil;

    [self.view insertSubview:self.bg atIndex:0];
    [self.view addSubview:self.numLbl];
}

- (void)setupUIFrame {
    [self.bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    [self.numLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(naviBarHeight + 20);
        make.height.mas_equalTo(20);
    }];
    [self.collectionView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(naviBarHeight + 20 + 20);
    }];
}

- (void)handlSettings:(UIButton *)sender {
    NSString * str = [NSString stringWithFormat:@"https://share.famy.ly/view/dist/#/pet_rule?language=%@",[SPDCommonTool getFamyLanguage]];
    SPDHelpController *helpController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SPDHelpController"];
    helpController.titleText = @"规则".localized;
    helpController.url = str;
    [self.navigationController pushViewController:helpController animated:true];
}

- (void)loadNetDataWith:(RequestNetDataType)type {
    if (type == RequestNetDataTypeRefresh ) {
        self.pageNumber = 1;
    } else {
        self.pageNumber ++;
    }
    NSDictionary *params = @{@"position": @"pet",
                             @"page":@(self.pageNumber),
                             @"type":self.type ?:@"whole"
    };
    [LY_Network getRequestWithURL:LY_Api.petBag parameters:params success:^(id  _Nullable response) {
        if (type == RequestNetDataTypeRefresh && [self.type isEqualToString:@"whole"]) {
            [self.dataList removeAllObjects];
        }
        self.numLbl.text = [NSString stringWithFormat:@"%@(%@/%@)",@"宠物".localized,response[@"bagNum"],response[@"bagNumMax"]];
        if (response[@"wholes"]) {
            NSArray * array = response[@"wholes"];
            NSMutableArray * models = NSMutableArray.new;
            for (NSDictionary * dic in array) {
                PetModel * md = [PetModel yy_modelWithDictionary:dic];
                [models addObject:md];
            }
            [self.dataList addObjectsFromArray:models];
            if (array.count < 50) {
                self.type = @"debris";
                self.pageNumber = 1;
                [self loadNetDataWith:RequestNetDataTypeRefresh];
            }
            else {
                [self loadNetDataWith:RequestNetDataTypeLoadMore];
            }
        }
        if (response[@"debris"]) {
            NSArray * array = response[@"debris"];
            NSMutableArray * models = NSMutableArray.new;
            for (NSDictionary * dic in array) {
                DebrisModel * md = [DebrisModel yy_modelWithDictionary:dic];
                [models addObject:md];
            }
            [self.dataList addObjectsFromArray:models];
            if (array.count >= 50) {
                [self loadNetDataWith:RequestNetDataTypeLoadMore];
            }
        }
        [self reloadData];
    } failture:^(NSError * _Nullable error) {
        [self reloadData];
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

- (void)requestAllConfig {
    [LY_Network getRequestWithURL:LY_Api.decorateConfig parameters:@{} success:^(id  _Nullable response) {
        if ([response isKindOfClass:[NSDictionary class]]) {
            self.allConfig = [response copy];
        }
    } failture:^(NSError * _Nullable error) {
        [self showTips:error.localizedDescription];
    }];
}

- (void)refreshStatus {
    self.type = @"whole";
    [self loadNetDataWith:RequestNetDataTypeRefresh];
}

- (void)changeCover:(PetModel *)pet complete:(nullable void(^)(id  _Nullable response, NSError *error))complete {
    [LY_Network postRequestWithURL:LY_Api.transformation parameters:@{@"id": pet.pid} success:^(id  _Nullable response) {
        [self refreshStatus];
        complete(response, nil);
    } failture:^(NSError * _Nullable error) {
        if (error.code == 2001) {
            [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
                
            } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                [self pushToRechageController];
            }];
        }
        complete(nil, error);
    }];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < self.dataList.count) {
        if ([self.dataList[indexPath.row] isKindOfClass:[PetModel class]]) {
            PetCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PetCell" forIndexPath:indexPath];
            cell.backgroundColor = [UIColor clearColor];
            cell.contentView.backgroundColor = [UIColor clearColor];
            cell.pet = self.dataList[indexPath.row];
            return cell;
        } else if([self.dataList[indexPath.row] isKindOfClass:[DebrisModel class]]) {
            SPCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SPCell" forIndexPath:indexPath];
            cell.backgroundColor = [UIColor clearColor];
            cell.contentView.backgroundColor = [UIColor clearColor];
            cell.debris = self.dataList[indexPath.row];
            return cell;
        }
    }
    return [UICollectionViewCell new];
}

#pragma mark - hc

- (void)requesetHC:(NSString *)gd {
    [LY_Network postRequestWithURL:LY_Api.compoundwhole parameters:@{@"good_id":gd?:@""} success:^(id  _Nullable response) {
        PetModel * pet = [PetModel yy_modelWithDictionary:response];
        PetUpgradeFuncView * func = [[PetUpgradeFuncView alloc]init];
        func.ishc = true;
        [func updatePet:pet status:0];
        [func present];
        [self refreshStatus];
//        [self loadNetDataWith:RequestNetDataTypeRefresh];
    } failture:^(NSError * _Nullable error) {
        if (error.code == 2004) {
            [self showSPNotEn];
        } else if (error.code == 2001) {
            [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
                
            } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                [self pushToRechageController];
            }];
        } else {
            [self showTips:error.localizedDescription];
        }
    }];
}

- (void)showSPNotEn {
    NeedSPView * view = [[NeedSPView alloc]initWithFrame:self.view.bounds];
    __weak typeof(self) wself = self;
    [view setDidTapUpgradeBlock:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kaibaoxiang" object:nil];
        [wself.navigationController popViewControllerAnimated:YES];
    }];
    [view present];
}

- (void)didselect:(DebrisModel *)model {
    SPAlertView * aa = [[SPAlertView alloc]init];
    __weak typeof(self) wself = self;
    aa.hvClicked = ^{
        SPHCAlertView * view = [[SPHCAlertView alloc]init];
        view.debris = model;
        view.config = self.allConfig;
        view.hvClicked = ^{
            [wself requesetHC:model.good_id];
        };
        [view present];
    };
    aa.dhClicked = ^{
        if ([model.quality isEqualToString:@"colorful"]) {
            [self showTips:@"宠物碎片已为最高品质，不可兑换更高等级".localized];
        } else {
            DHSPView * v = [DHSPView new];
            v.model = model;
            v.didTapdhBlock = ^(NSString * _Nonnull goodID, NSInteger num) {
                [wself requestDH:goodID and:num];
            };
            [v present];
        }
    };
    [aa present];
}

// 兑换

#pragma mark - 兑换

- (void)requestDH:(NSString * )gd and:(NSInteger)num {
    if (num < 10) {
        [self showSPNotEn];
        return;
    }
    [LY_Network postRequestWithURL:LY_Api.compounddebris parameters:@{@"good_id":gd?:@"",@"num":@(num)} success:^(id  _Nullable response) {
        NSArray * data = response;
        NSMutableArray * arrs = [NSMutableArray new];
        for (NSDictionary * dic in data) {
            DebrisModel * model = [DebrisModel yy_modelWithDictionary:dic];
            [arrs addObject:model];
        }
        DecomposeResultView * result = [[DecomposeResultView alloc]initWithFrame:self.view.bounds];
        result.data = arrs;
        [result present];
        [self refreshStatus];
//        [self loadNetDataWith:RequestNetDataTypeRefresh];
    } failture:^(NSError * _Nullable error) {
        if (error.code == 2004) {
            [self showSPNotEn];
        } else if (error.code == 2001) {
            [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{

            } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                [self pushToRechageController];
            }];
        } else {
            [self showTips:error.localizedDescription];
        }
    }];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (![self.dataList[indexPath.row] isKindOfClass:[PetModel class]]) {
        [self didselect:self.dataList[indexPath.row]];
        return;
    }
    if (indexPath.row >= self.dataList.count) {
        return;
    }
    
    PetModel * model = self.dataList[indexPath.row];
    for (PetModel *m in self.dataList) {
        if ([m isKindOfClass:[PetModel class]]) {
            if ([m.pid isEqualToString:model.pid]) {
                m.selec = true;
            } else {
                m.selec = false;
            }
        }
    }
    [self.collectionView reloadData];
    PetDetailAlertView * detail = [[PetDetailAlertView alloc]initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH)];
    [detail showIn:self.view pet:model];
    __weak typeof(self) wself = self;
    __weak typeof(detail) wdetail = detail;
    [detail setDidTapChangeCoverBlock:^(PetModel * _Nonnull pet) {
        LYPSTextAlertView *view = [[LYPSTextAlertView alloc] init];
        NSDictionary *outgoDict = self.allConfig[@"transformationOutgo"];
        if ([outgoDict valueForKey:pet.quality]) {
            view.title = SPDLocalizedString(@"提示");
            view.message = [NSString stringWithFormat:@"确认要花费%@金币随机更换该宠物的形象吗？".localized, [outgoDict valueForKey:pet.quality]];
            [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"取消") style:LYPSystemAlertActionStyleCancel handler:^(LYPSystemAlertAction * _Nonnull action) {
                
            }]];
            [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"更换") style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
                [wself changeCover:pet complete:^(id  _Nullable response, NSError *error) {
                    [wdetail updateCover:response[@"image"]];
                }];
            }]];
            [view present];
        }
        
    }];
    [detail setDidTapEquipBlock:^(PetModel * _Nonnull pet) {
        [wself equip:pet];
    }];
    [detail setDidTapBreakupBlock:^(PetModel * _Nonnull pet) {
        LYPSTextAlertView *view = [[LYPSTextAlertView alloc] init];
        view.title = SPDLocalizedString(@"提示");
        view.message = @"确认要分解该宠物吗？".localized;
        [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"取消") style:LYPSystemAlertActionStyleCancel handler:^(LYPSystemAlertAction * _Nonnull action) {
            
        }]];
        [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"确认") style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
            [wself showDecompose:pet];
        }]];
        [view present];
    }];
    [detail setDidTapUpgradeBlock:^(PetModel * _Nonnull pet) {
        [wself settleUpgrade:pet];
    }];
    [detail setDidTapleftBlock:^(PetModel * _Nonnull pet) {
//        NSInteger index = [wself.dataList indexOfObject:pet];
        int index = 0;
        for (int i = 0 ; i< wself.dataList.count ; i++) {
            if ([wself.dataList[i] isKindOfClass:[PetModel class]]) {
                PetModel * p = (PetModel *)wself.dataList[i];
                if ([p.pid isEqualToString:pet.pid]) {
                    index = i;
                }
            }
        }
        int next = index - 1;
        if (next >= 0 && next < wself.dataList.count) {
            if ([wself.dataList[next] isKindOfClass:[PetModel class]]) {
                for (int i = 0; i< wself.dataList.count; i++) {
                    id mm = wself.dataList[i];
                    if ([mm isKindOfClass:[PetModel class]]) {
                        PetModel * m0 = (PetModel *)mm;
                        m0.selec = false;
                    }
                }
                PetModel * model = wself.dataList[next];
                model.selec = true;
                [wself.collectionView reloadData];
                [wdetail update:model];
            }
        }
    }];
    [detail setDidTaprightBlock:^(PetModel * _Nonnull pet) {
        int index = 0;
        for (int i = 0 ; i< wself.dataList.count ; i++) {
            if ([wself.dataList[i] isKindOfClass:[PetModel class]]) {
                PetModel * p = (PetModel *)wself.dataList[i];
                if ([p.pid isEqualToString:pet.pid]) {
                    index = i;
                }
            }
        }
        int next = index + 1;
        if (next < wself.dataList.count) {
            if ([wself.dataList[next] isKindOfClass:[PetModel class]]) {
                for (int i = 0; i< wself.dataList.count; i++) {
                    id mm = wself.dataList[i];
                    if ([mm isKindOfClass:[PetModel class]]) {
                        PetModel * m0 = (PetModel *)mm;
                        m0.selec = false;
                    }
                }
                PetModel * model = wself.dataList[next];
                model.selec = true;
                [wself.collectionView reloadData];
                [wdetail update:model];
            }
        }
    }];
}

#pragma mark - upgrade

- (void)settleUpgrade:(PetModel *)pet {
    [LY_Network getRequestWithURL:LY_Api.decorateConfig parameters:@{} success:^(id  _Nullable response) {
        NSDictionary * dict = response[@"upgradeConfig"];
        NSString * lvl = [NSString stringWithFormat:@"%@%@",pet.quality,@(pet.level + 1)];
        if (dict[lvl]) {
            PetUpgradeView * upgradeV = [[PetUpgradeView alloc]initWithFrame:self.view.bounds];
            [upgradeV present];
            [upgradeV updatePet:pet config:dict[lvl]];
            __weak typeof(self) wself = self;
            [upgradeV setDidTapUpgradeBlock:^(PetModel * _Nonnull pet) {
                [wself requestUpgrade:pet];
            }];
        }
    } failture:^(NSError * _Nullable error) {
        if (error.code == 2001) {
            [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
                
            } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                [self pushToRechageController];
            }];
        } else {
            [self showTips:error.localizedDescription];
        }
    }];
}

- (void)requestUpgrade:(PetModel *)pet {
    [LY_Network postRequestWithURL:LY_Api.dupgrade parameters:@{@"id":pet.pid?:@""} success:^(id  _Nullable response) {
        if ([response[@"result"] isEqualToString:@"success"]) {
            PetUpgradeFuncView * func = [[PetUpgradeFuncView alloc]initWithFrame:self.view.bounds];
            PetModel * newp = [PetModel yy_modelWithDictionary:response[@"nowWhole"]];
            if (newp) {
                [func updatePet:newp status:1];
                [func present];
            } else {
                [func updatePet:pet status:0];
                [func present];
            }
            [self refreshStatus];
        } else {
            PetUpgradeFuncView * func = [[PetUpgradeFuncView alloc]initWithFrame:self.view.bounds];
            PetModel * newp = [PetModel yy_modelWithDictionary:response[@"nowWhole"]];
            [func updatePet:newp status:0];
            [func present];
            [self refreshStatus];
        }
        // 根据条件判断升级view
    } failture:^(NSError * _Nullable error) {
        if (error.code == 2001) {
            [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
                
            } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                [self pushToRechageController];
            }];
        } else if (error.code ==2004) {
            [self showSPNotEn];
        }
        else {
            [self showTips:error.localizedDescription];
        }
    }];
}

// 跳转到充值页面
- (void)pushToRechageController {
    [self.navigationController pushViewController:[MyWalletViewController new] animated:YES];
}

#pragma mark - zhuangbei

- (void)equip:(PetModel *)pet {
    NSMutableDictionary * dic = [NSMutableDictionary new];
    [dic setValue:pet.pid forKey:@"id"];
    if (pet.equip) {
        [dic setValue:@(0) forKey:@"equip"];
    } else {
        [dic setValue:@(1) forKey:@"equip"];
    }
    __weak typeof(self) wself = self;
    [RequestUtils POST:URL_Server(@"decorate/equip.decorate") parameters:dic success:^(id  _Nullable suceess) {
        if (pet.equip) {
            [wself showTips:@"已卸下".localized];
        } else {
            [wself showTips:@"装备成功".localized];
        }
        [self refreshStatus];
    } failure:^(id  _Nullable failure) {
        [wself showTips:failure[@"msg"]];
    }];
}

#pragma mark - fenjie

- (void)showDecompose:(PetModel *)pet {
    DecomposeAlertView * view = [[DecomposeAlertView alloc]initWithFrame:self.view.bounds];
    view.pet = pet;
    [view present];
    __weak typeof(self) wself = self;
    [view setDidTapDecomposeBlock:^(PetModel * _Nonnull pet) {
        [wself requestDecompose:pet];
    }];
}

- (void)requestDecompose:(PetModel *)model {
    NSMutableDictionary * dic = [NSMutableDictionary new];
    [dic setValue:model.pid forKey:@"id"];
    __weak typeof(self) wself = self;
    [RequestUtils POST:URL_Server(@"/decorate/resolve") parameters:dic success:^(id  _Nullable suceess) {
        NSArray * data = suceess;
        NSMutableArray * arrs = [NSMutableArray new];
        for (NSDictionary * dic in data) {
            DebrisModel * model = [DebrisModel yy_modelWithDictionary:dic];
            [arrs addObject:model];
        }
        DecomposeResultView * result = [[DecomposeResultView alloc]initWithFrame:wself.view.bounds];
        result.data = arrs;
        [result present];
        [self refreshStatus];
//        [wself loadNetDataWith:RequestNetDataTypeRefresh];
    } failure:^(id  _Nullable failure) {
        [wself showTips:failure[@"msg"]];
    }];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(92, 108);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 8;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return (kScreenW - 4 *92) /3;
}

#pragma mark - getters

- (UIImageView *)bg {
    if (!_bg) {
        _bg = [UIImageView new];
        _bg.image = [UIImage imageNamed:@"bg_beibao"];
    }
    return _bg;
}

- (UILabel *)numLbl {
    if (!_numLbl) {
        _numLbl = [UILabel new];
        _numLbl.textColor = [UIColor whiteColor];
        _numLbl.font = [UIFont boldSystemFontOfSize:18];
        _numLbl.text = @"宠物(0/100)".localized;
    }
    return _numLbl;
}

//不能自定义颜色
-(void)presentCommonController :(NSString *)title
                  messageString:(NSString *)message
                    cancelTitle:(NSString * _Nullable)cancelTitle
                   cancelAction:(_Nullable ActionHandler)cancelAction
                   confirmTitle:(NSString *)confirmTitle
                  confirmAction:(ActionHandler)confirmAction {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    
    if (message) {
        NSMutableAttributedString *messageStr = [[NSMutableAttributedString alloc] initWithString:message];
        [messageStr addAttribute:NSForegroundColorAttributeName value:SPD_HEXCOlOR(@"f56986") range:NSMakeRange(0, message.length)];
        [messageStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(0, message.length)];
        [alertController setValue:messageStr forKey:@"attributedMessage"];
    }
    
    
    if (cancelTitle.length > 0) {
        UIAlertAction *cancelAlertAction =
        [UIAlertAction actionWithTitle:cancelTitle
                                 style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action) {
                                   if (cancelAction) {
                                       cancelAction();
                                   }
                               }];
        
        if ([[UIDevice currentDevice].systemVersion floatValue] > 8.3) {
            [cancelAlertAction setValue:SPD_HEXCOlOR(@"bbbbbb") forKey:@"_titleTextColor"];
        }
        
        [alertController addAction:cancelAlertAction];
    }
    if (confirmTitle.length > 0) {
        UIAlertAction *confirmAlertAction =
        [UIAlertAction actionWithTitle:confirmTitle
                                 style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action) {
                                   if (confirmAction) {
                                       confirmAction();
                                   }
                               }];
        if ([[UIDevice currentDevice].systemVersion floatValue] > 8.3) {
            [confirmAlertAction setValue:SPD_HEXCOlOR(@"3782fe") forKey:@"_titleTextColor"];
        }

        [alertController addAction:confirmAlertAction];
    }
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
