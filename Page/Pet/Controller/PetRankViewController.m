//
//  PetRankViewController.m
//  SimpleDate-dis
//
//  Created by houling on 2021/10/13.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PetRankViewController.h"
#import "JXPagerView.h"
#import "RankOptionCollectionViewCell.h"
#import "PetRankTopCell.h"
#import "PetRankListCell.h"
#import "PetLabelView.h"
#import "PetView.h"
#import "PetModel.h"
#import "LY_HomePageViewController.h"
#import "LYPCustomAlertView.h"

@interface RuleView : LYPCustomAlertView

@property (nonatomic, strong)UILabel * uu;
@property (nonatomic, strong)UILabel * r1;
@property (nonatomic, strong)UILabel * r2;
@property (nonatomic, strong)UILabel * r3;
@end

@implementation RuleView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.containerView addSubview:self.uu];
        [self.containerView addSubview:self.r1];
        [self.containerView addSubview:self.r2];
        [self.containerView addSubview:self.r3];
    }
    return self;
}


- (void)layoutSubviews {
    [super layoutSubviews];
    [self.uu mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.centerX.mas_equalTo(0);
    }];
    [self.r1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.uu.mas_bottom).offset(20);
        make.centerX.mas_equalTo(0);
        make.leading.mas_equalTo(23);
        make.trailing.mas_equalTo(-23);
        make.bottom.mas_equalTo(-20);
    }];
}

- (UILabel *)uu {
    if (!_uu) {
        _uu = [UILabel new];
        _uu.font = [UIFont systemFontOfSize:15];
        _uu.text = @"规则".localized;
        _uu.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
    }
    return _uu;
}

- (UILabel *)r1 {
    if (!_r1) {
        _r1 = [UILabel new];
        _r1.font = [UIFont systemFontOfSize:15];
        _r1
            .textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _r1.text = @"1、金币排行榜按照用户宠物携带的每天可领取的金币数量进行排行，背包中的所有宠物都可以上榜。\n2、经验排行榜按照用户宠物携带的每天可领取的经验值数量进行排行。\n3、魅力排行榜按照用户宠物携带的每天可领取的魅力值数量进行排行。".localized;
        _r1.numberOfLines = 0;
    }
    return _r1;
}

@end

@interface MyView: UIView

@property (nonatomic, strong) UIImageView * myViewBack;
@property (nonatomic, strong) UILabel * indexLabel;
@property (nonatomic, strong) PetView * petView;
@property (nonatomic, strong) PetLabelView * numLabel;
@property (nonatomic, strong) UIImageView * avatarImageView;
@property (nonatomic, strong) UILabel * nameLabel;
@end

@implementation MyView
- (UIImageView *)myViewBack {
    if (!_myViewBack) {
        _myViewBack = [UIImageView new];
        _myViewBack.image = [UIImage imageNamed:@"bg_wodepaihangbang"];
    }
    return _myViewBack;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self setupFrame];
}

- (void)setupUI {
    [self addSubview:self.indexLabel];
    [self addSubview:self.petView];
    [self addSubview:self.numLabel];
    [self addSubview:self.avatarImageView];
    [self addSubview:self.nameLabel];
}

- (void)update:(NSDictionary *)dict type:(NSString *)type {
    NSInteger index = [dict[@"rank"] integerValue];
    self.indexLabel.text = [NSString stringWithFormat:@"%ld", index];
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:dict[@"avatar"]]]];
    self.nameLabel.text = dict[@"nickName"];
    if ([type isEqualToString:@"bill"]) {
        NSInteger gold = [dict[@"bill"] integerValue];
        [self.numLabel update:type num:gold];
    } if ([type isEqualToString:@"exp"]) {
        NSInteger gold = [dict[@"exp"] integerValue];
        [self.numLabel update:type num:gold];
    } if ([type isEqualToString:@"charm"]) {
        NSInteger gold = [dict[@"charm"] integerValue];
        [self.numLabel update:type num:gold];
    }
    PetModel * model = [[PetModel alloc]init];
    model.level = [dict[@"level"] integerValue];
    model.image = dict[@"image"];
    model.quality = dict[@"quality"];
    self.petView.pet = model;
    [self setNeedsLayout];
}

- (void)setupFrame {
    [self.myViewBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(-10, -10, -10, -10));
    }];
    [self.indexLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.leading.mas_equalTo(0);
        make.width.mas_equalTo(52);
        make.height.equalTo(self);
    }];
    [self.petView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self).offset(52);
        make.centerY.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(65, 65));
    }];
    [self.numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.petView.mas_trailing).offset(19);
        make.centerY.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(75, 26));
    }];
    [self.avatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.numLabel.mas_trailing).offset(15);
        make.centerY.equalTo(self.numLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.avatarImageView.mas_trailing).offset(15);
        make.centerY.equalTo(self.numLabel.mas_centerY);
        make.trailing.mas_equalTo(-15);
    }];
}

- (UILabel *)indexLabel {
    if (!_indexLabel) {
        _indexLabel = [UILabel new];
        _indexLabel.font = [UIFont systemFontOfSize:15];
        _indexLabel.textColor = [UIColor colorWithHexString:@"1A1A1A"];
        _indexLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _indexLabel;
}

- (PetView *)petView {
    if (!_petView) {
        _petView = [PetView new];
    }
    return _petView;
}

- (UIImageView *)avatarImageView {
    if (!_avatarImageView) {
        _avatarImageView = [UIImageView new];
        _avatarImageView.layer.cornerRadius = 15;
        _avatarImageView.clipsToBounds = true;
    }
    return _avatarImageView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.font = [UIFont systemFontOfSize:15];
        _nameLabel.textColor = [UIColor colorWithHexString:@"000000"];
    }
    return _nameLabel;
}

- (PetLabelView *)numLabel {
    if (!_numLabel) {
        _numLabel = [PetLabelView new];
        _numLabel.layer.cornerRadius = 13;
        _numLabel.clipsToBounds = true;
        _numLabel.numLabel.font = [UIFont systemFontOfSize:12];
        _numLabel.numLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
    }
    return _numLabel;
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.myViewBack];
        self.backgroundColor = [UIColor whiteColor];
        [self setupUI];
    }
    return self;
}


@end


@interface PetRankViewController ()<JXPagerViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property(nonatomic, strong) UIScrollView * scroll;
@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);
@property(nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) NSArray *optionArr;
@property (nonatomic, strong) UIButton *goldBtn;
@property (nonatomic, strong) UIButton *expBtn;
@property (nonatomic, strong) UIButton *charmBtn;
@property (nonatomic, assign) NSInteger selectIndex;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, strong) MyView * myView;
@property (nonatomic, strong) UIButton * ruleBTn;


//@property (nonatomic, strong) UIImageView * myViewBack;

//PetRankTopCell
@end

@implementation PetRankViewController


//- (UIImageView *)myViewBack {
//    if (!_myViewBack) {
//        _myViewBack = [UIImageView new];
//        _myViewBack.image = [UIImage imageNamed:@"bg_wodepaihangbang"];
//    }
//    return _myViewBack;
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.goldBtn];
    [self.view addSubview:self.expBtn];
    [self.view addSubview:self.charmBtn];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.myView];
    self.optionArr = @[self.goldBtn, self.expBtn, self.charmBtn];
    self.ruleBTn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:self.ruleBTn];
    [self.ruleBTn setImage:[UIImage imageNamed:@"family_help"] forState:UIControlStateNormal];
    [self.ruleBTn addTarget:self action:@selector(ja) forControlEvents:UIControlEventTouchUpInside];
}

- (void)ja {
    RuleView * rule = [[RuleView alloc]initWithFrame:self.view.bounds];
    rule.r1.text = @"1、金币排行榜按照用户宠物携带的每天可领取的金币数量进行排行，背包中的所有宠物都可以上榜。\n2、经验排行榜按照用户宠物携带的每天可领取的经验值数量进行排行。\n3、魅力排行榜按照用户宠物携带的每天可领取的魅力值数量进行排行。".localized;
    [rule present];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.type = @"bill";
    self.selectIndex = 0;
    [self request];
}


- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self.expBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(110, 26));
    }];
    [self.goldBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.trailing.equalTo(self.expBtn.mas_leading).offset(-15);
        make.size.mas_equalTo(CGSizeMake(110, 26));
    }];
    [self.charmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.leading.equalTo(self.expBtn.mas_trailing).offset(15);
        make.size.mas_equalTo(CGSizeMake(110, 26));
    }];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30);
        make.leading.trailing.bottom.mas_equalTo(0);
    }];

    [self.myView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-TabBarHeight - 10);
        make.leading.mas_equalTo(10);
        make.trailing.mas_equalTo(-10);
        make.height.mas_equalTo(65);
    }];
    
    [self.ruleBTn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-6);
        make.size.mas_equalTo(16);
        make.centerY.equalTo(self.expBtn.mas_centerY);
    }];
//    [self.myView layoutIfNeeded];
//    [self.myViewBack mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.leading.trailing.mas_equalTo(0);
//        make.bottom.mas_equalTo(-TabBarHeight);
//        make.top.equalTo(self.myView.mas_top).offset(-10);
//    }];
}

- (void)setSelectIndex:(NSInteger)selectIndex {
    _selectIndex = selectIndex;
    if (_selectIndex == 0) {
        [self.goldBtn setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.2]];
        [self.expBtn setBackgroundColor:[UIColor clearColor]];
        [self.charmBtn setBackgroundColor:[UIColor clearColor]];
    } else if (_selectIndex == 1) {
        [self.expBtn setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.2]];
        [self.goldBtn setBackgroundColor:[UIColor clearColor]];
        [self.charmBtn setBackgroundColor:[UIColor clearColor]];
    } else {
        [self.charmBtn setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.2]];
        [self.expBtn setBackgroundColor:[UIColor clearColor]];
        [self.goldBtn setBackgroundColor:[UIColor clearColor]];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CHANGEui" object:nil userInfo:@{@"index":@(selectIndex)}];
    });
}

- (void)request {
    if (self.selectIndex == 0) {
        self.type = @"bill";
    } else if (self.selectIndex == 1) {
        self.type = @"exp";
    } else {
        self.type = @"charm";
    }
    [RequestUtils GET:URL_Server(@"pets.rank.list") parameters:@{@"type": self.type ?: @""} success:^(id  _Nullable suceess) {
        [self.dataArr removeAllObjects];
        self.dataArr = [suceess[@"list"] mutableCopy];
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        if (suceess[@"myData"]) {
            self.myView.hidden = NO;
            [self.myView update:suceess[@"myData"] type:self.type];
        } else {
            self.myView.hidden = YES;
        }
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)handleGoldTapped:(UIButton *)sender {
    self.type = @"bill";
    self.selectIndex = 0;
    [self.tableView.mj_header beginRefreshing];
}

- (void)handleExpTapped:(UIButton *)sender {
    self.type = @"exp";
    self.selectIndex = 1;
    [self.tableView.mj_header beginRefreshing];
}

- (void)handleCharmTapped:(UIButton *)sender {
    self.type = @"charm";
    self.selectIndex = 2;
    [self.tableView.mj_header beginRefreshing];
}

#pragma mark - tableview

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < 3) {
        PetRankTopCell * cell = [tableView dequeueReusableCellWithIdentifier:@"PetRankTopCell"];
        [cell update:self.dataArr[indexPath.row] type:self.type];
//        cell.didTapdBlock = ^(NSString * _Nonnull ID) {
//            NSString * userID = self.dataArr[indexPath.row][@"userId"];
//
//        };
        return cell;
    } else {
        PetRankListCell* cell = [tableView dequeueReusableCellWithIdentifier:@"PetRankListCell"];
        [cell update:self.dataArr[indexPath.row] type:self.type];
        if (indexPath.row == 4) {
            cell.contentView.layer.cornerRadius = 8;
        }
//        cell.didTapdBlock = ^(NSString * _Nonnull ID) {
//            NSString * userID = self.dataArr[indexPath.row][@"userId"];
//            
//        };
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < 3) {
        return 90;
    }
    return 65;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString * userID = self.dataArr[indexPath.row][@"userId"];
    LY_HomePageViewController * vc = [[LY_HomePageViewController alloc] initWithUserId:userID];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - Getters

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray new];
    }
    return _dataArr;
}

- (UIButton *)goldBtn {
    if (!_goldBtn) {
        _goldBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_goldBtn setTitle:@"金币榜".localized forState:UIControlStateNormal];
        _goldBtn.layer.cornerRadius = 13;
        _goldBtn.clipsToBounds = true;
        [_goldBtn addTarget:self action:@selector(handleGoldTapped:) forControlEvents:UIControlEventTouchUpInside];
        _goldBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    }
    return _goldBtn;
}

- (UIButton *)expBtn {
    if (!_expBtn) {
        _expBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_expBtn setTitle:@"经验榜".localized forState:UIControlStateNormal];
        _expBtn.layer.cornerRadius = 13;
        _expBtn.clipsToBounds = true;
        _expBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_expBtn addTarget:self action:@selector(handleExpTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _expBtn;
}

- (UIButton *)charmBtn {
    if (!_charmBtn) {
        _charmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_charmBtn setTitle:@"魅力榜".localized forState:UIControlStateNormal];
        _charmBtn.layer.cornerRadius = 13;
        _charmBtn.clipsToBounds = true;
        [_charmBtn addTarget:self action:@selector(handleCharmTapped:) forControlEvents:UIControlEventTouchUpInside];
        _charmBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    }
    return _charmBtn;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[PetRankListCell class] forCellReuseIdentifier:@"PetRankListCell"];
        [_tableView registerClass:[PetRankTopCell class] forCellReuseIdentifier:@"PetRankTopCell"];
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorColor = [UIColor clearColor];
        __weak typeof(self) wself = self;
        _tableView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            [wself request];
        }];
    }
    return _tableView;
}

- (MyView *)myView {
    if (!_myView) {
        _myView = [[MyView alloc]initWithFrame:CGRectZero];
        _myView.hidden = true;
        _myView.layer.cornerRadius = 65/2.0f;
        _myView.clipsToBounds = true;
    }
    return _myView;
}


// MARK: - JXPagerViewListViewDelegate

- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return self.scroll;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback ?: self.scrollCallback(scrollView);
    CGPoint translation = [scrollView.panGestureRecognizer translationInView:scrollView];
    self.myView.hidden = translation.y < 0;
}

@end
