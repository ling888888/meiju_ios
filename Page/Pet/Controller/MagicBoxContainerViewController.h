//
//  MagicBoxContainerViewController.h
//  SimpleDate-dis
//
//  Created by houling on 2021/8/1.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_BaseCategoryViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MagicBoxContainerViewController : LY_BaseCategoryViewController

@end

NS_ASSUME_NONNULL_END
