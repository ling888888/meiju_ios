//
//  AttributeViewController.m
//  SimpleDate
//
//  Created by houling on 2021/8/29.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "AttributeViewController.h"
#import "JXPagerView.h"
#import "AttributeHeaderCell.h"
#import "AttributeReceiveRewardCell.h"
#import "AttributeTitleCell.h"
#import "AttributeRewardTotalCell.h"
#import "AttributeIntroCell.h"
#import "PetModel.h"
#import "SPDHelpController.h"
#import "PetDetailAlertView.h"
#import "LYPSTextAlertView.h"
#import "MyWalletViewController.h"
#import "DecomposeAlertView.h"
#import "DecomposeResultView.h"
#import "PetUpgradeFuncView.h"
#import "PetUpgradeView.h"
#import "DebrisModel.h"
#import "NeedSPView.h"
#import "PetPackageViewController.h"

@interface AttributeViewController ()<JXPagerViewListViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property(nonatomic, strong) UIScrollView * scroll;
@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);
@property (nonatomic, strong) UICollectionView * collectionView;
@property (nonatomic, copy) NSDictionary * dict;
@property(nonatomic, strong) PetModel * pet;
@property (nonatomic, copy) NSMutableArray * datas;
@property (nonatomic, copy) NSDictionary * allConfig;

@end

@implementation AttributeViewController

- (NSMutableArray *)datas {
    if (!_datas) {
        _datas = [NSMutableArray new];
    }
    return _datas;
}

- (NSDictionary *)allConfig {
    if (!_allConfig) {
        _allConfig = [NSDictionary new];
    }
    return _allConfig;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.collectionView];
    self.collectionView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - naviBarHeight - TabBarHeight);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self request:1];
    [self requestAllConfig];
    [self requestData];
}

#pragma mark - request

- (void)requestData {
    [LY_Network getRequestWithURL:LY_Api.shuxing parameters:@{} success:^(id  _Nullable response) {
        self.dict = response;
        if (response[@"decorate"]) {
            self.pet = nil;
            NSArray * arr = response[@"decorate"];
            for (NSDictionary * dic in arr) {
                if ([dic[@"position"] isEqualToString:@"pet"]) {
                    PetModel * model = [PetModel initWithDictionary:dic];
                    self.pet = model;
                }
            }
        }
        [self.collectionView reloadData];
    } failture:^(NSError * _Nullable error) {
        [self showTips:error.localizedDescription];
    }];
}

- (void)receive {
    [LY_Network getRequestWithURL:LY_Api.receivejiangli parameters:@{@"user_id": [SPDApiUser currentUser].userId ?: @""} success:^(id  _Nullable response) {
        [self requestData];
    } failture:^(NSError * _Nullable error) {
        [self showTips:error.localizedDescription];
    }];
}

#pragma mark - pet

- (void)request:(NSInteger)pageNumber{
    NSDictionary *params = @{@"position": @"pet",
                             @"page":@(pageNumber),
                             @"type":@"whole"
    };
    if (pageNumber == 1){
        [self.datas removeAllObjects];
    }
    [LY_Network getRequestWithURL:LY_Api.petBag parameters:params success:^(id  _Nullable response) {
        if (response[@"wholes"]) {
            NSArray * array = response[@"wholes"];
            NSMutableArray * models = NSMutableArray.new;
            for (NSDictionary * dic in array) {
                PetModel * md = [PetModel yy_modelWithDictionary:dic];
                [models addObject:md];
            }
            [self.datas addObjectsFromArray:models];
            if (array.count >= 50) {
                NSInteger ha = pageNumber + 1;
                [self request:ha];
            }
        }
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}


- (void)requestAllConfig {
    [LY_Network getRequestWithURL:LY_Api.decorateConfig parameters:@{} success:^(id  _Nullable response) {
        
        if ([response isKindOfClass:[NSDictionary class]]) {
            self.allConfig = [response copy];
        }
        
    } failture:^(NSError * _Nullable error) {
        [self showTips:error.localizedDescription];
    }];
}

- (void)changeCover:(PetModel *)pet complete:(nullable void(^)(id  _Nullable response, NSError *error))complete {
    [LY_Network postRequestWithURL:LY_Api.transformation parameters:@{@"id": pet.pid?:@""} success:^(id  _Nullable response) {
        [self requestData];
        [self request:1];
        complete(response, nil);
    } failture:^(NSError * _Nullable error) {
        if (error.code == 2001) {
            [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
                
            } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                [self pushToRechageController];
            }];
        }
        complete(nil, error);
    }];
}

// 跳转到充值页面
- (void)pushToRechageController {
    [self.navigationController pushViewController:[MyWalletViewController new] animated:YES];
}

- (void)showPet {
    PetModel * pp = nil;
    for (PetModel *p in self.datas) {
        if (p.equip) {
            pp = p;
            break;
        }
    }
    if (!pp) {
        LYPSTextAlertView *view = [[LYPSTextAlertView alloc] init];
        view.title = SPDLocalizedString(@"提示");
        view.message = @"你还没有装备宠物，快去背包中装备宠物或去开宝箱中获取宠物碎片吧".localized;
        __weak typeof(self) wself = self ;
        [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"去装备") style:LYPSystemAlertActionStyleCancel handler:^(LYPSystemAlertAction * _Nonnull action) {
            [self.navigationController pushViewController:[PetPackageViewController new] animated:YES];
        }]];
        [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"开宝箱") style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"kaibaoxiang" object:nil];
            [wself.navigationController popViewControllerAnimated:YES];
        }]];
        [view present];
        return;
    }
    PetDetailAlertView * detail = [[PetDetailAlertView alloc]initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH)];
    [detail showIn:[UIApplication sharedApplication].keyWindow pet:pp];
    __weak typeof(self) wself = self;
    __weak typeof(detail) wdetail = detail;
    [detail setDidTapChangeCoverBlock:^(PetModel * _Nonnull pet) {
        LYPSTextAlertView *view = [[LYPSTextAlertView alloc] init];
        NSDictionary *outgoDict = self.allConfig[@"transformationOutgo"];
        if ([outgoDict valueForKey:pet.quality]) {
            view.title = SPDLocalizedString(@"提示");
            view.message = [NSString stringWithFormat:@"确认要花费%@金币随机更换该宠物的形象吗？".localized, [outgoDict valueForKey:pet.quality]];
            [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"取消") style:LYPSystemAlertActionStyleCancel handler:^(LYPSystemAlertAction * _Nonnull action) {
            }]];
            [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"更换") style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
                [wself changeCover:pet complete:^(id  _Nullable response, NSError *error) {
                    [wdetail updateCover:response[@"image"]];
                }];
            }]];
            [view present];
        }
        
    }];
    [detail setDidTapEquipBlock:^(PetModel * _Nonnull pet) {
        [wself equip:pet];
    }];
    [detail setDidTapBreakupBlock:^(PetModel * _Nonnull pet) {
        LYPSTextAlertView *view = [[LYPSTextAlertView alloc] init];
        view.title = SPDLocalizedString(@"提示");
        view.message = @"确认要分解该宠物吗？".localized;
        [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"取消") style:LYPSystemAlertActionStyleCancel handler:^(LYPSystemAlertAction * _Nonnull action) {
            
        }]];
        [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"确认") style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
            [wself showDecompose:pet];
        }]];
        [view present];
    }];
    [detail setDidTapUpgradeBlock:^(PetModel * _Nonnull pet) {
        [wself settleUpgrade:pet];
    }];
    [detail setDidTapleftBlock:^(PetModel * _Nonnull pet) {
        int index = 0;
        for (int i = 0 ; i< wself.datas.count ; i++) {
            if ([wself.datas[i] isKindOfClass:[PetModel class]]) {
                PetModel * p = (PetModel *)wself.datas[i];
                if ([p.pid isEqualToString:pet.pid]) {
                    index = i;
                }
            }
        }
        int next = index - 1;
        if (next >= 0 && next < wself.datas.count) {
            if ([wself.datas[next] isKindOfClass:[PetModel class]]) {
                PetModel * model = wself.datas[next];
                [wdetail update:model];
            }
        }
    }];
    [detail setDidTaprightBlock:^(PetModel * _Nonnull pet) {
        int index = 0;
        for (int i = 0 ; i< wself.datas.count ; i++) {
            if ([wself.datas[i] isKindOfClass:[PetModel class]]) {
                PetModel * p = (PetModel *)wself.datas[i];
                if ([p.pid isEqualToString:pet.pid]) {
                    index = i;
                }
            }
        }
        int next = index + 1;
        if (next < wself.datas.count) {
            if ([wself.datas[next] isKindOfClass:[PetModel class]]) {
                PetModel * model = wself.datas[next];
                [wdetail update:model];
            }
        }
    }];
}

#pragma mark - upgrade

- (void)settleUpgrade:(PetModel *)pet {
    [LY_Network getRequestWithURL:LY_Api.decorateConfig parameters:@{} success:^(id  _Nullable response) {
        NSDictionary * dict = response[@"upgradeConfig"];
        NSString * lvl = [NSString stringWithFormat:@"%@%@",pet.quality,@(pet.level + 1)];
        if (dict[lvl]) {
            PetUpgradeView * upgradeV = [[PetUpgradeView alloc]initWithFrame:self.view.bounds];
            [upgradeV present];
            [upgradeV updatePet:pet config:dict[lvl]];
            __weak typeof(self) wself = self;
            [upgradeV setDidTapUpgradeBlock:^(PetModel * _Nonnull pet) {
                [wself requestUpgrade:pet];
            }];
        }
    } failture:^(NSError * _Nullable error) {
        if (error.code == 2001) {
            [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
                
            } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                [self pushToRechageController];
            }];
        } else {
            [self showTips:error.localizedDescription];
        }
    }];
}

- (void)requestUpgrade:(PetModel *)pet {
    [LY_Network postRequestWithURL:LY_Api.dupgrade parameters:@{@"id":pet.pid?:@""} success:^(id  _Nullable response) {
        if ([response[@"result"] isEqualToString:@"success"]) {
            PetUpgradeFuncView * func = [[PetUpgradeFuncView alloc]initWithFrame:self.view.bounds];
            PetModel * newp = [PetModel yy_modelWithDictionary:response[@"nowWhole"]];
            if (newp) {
                [func updatePet:newp status:1];
                [func present];
            } else {
                [func updatePet:pet status:0];
                [func present];
            }
            [self request:1];
            [self requestData];
//            [self loadNetDataWith:RequestNetDataTypeRefresh];
        } else {
            PetUpgradeFuncView * func = [[PetUpgradeFuncView alloc]initWithFrame:self.view.bounds];
            [func updatePet:pet status:0];
            [func present];
        }
        // 根据条件判断升级view
    } failture:^(NSError * _Nullable error) {
        if (error.code == 2001) {
            [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
                
            } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                [self pushToRechageController];
            }];
        } else if (error.code ==2004) {
            [self showSPNotEn];
        }
        else {
            [self showTips:error.localizedDescription];
        }
    }];
}

- (void)showSPNotEn {
    NeedSPView * view = [[NeedSPView alloc]initWithFrame:self.view.bounds];
    __weak typeof(self) wself = self;
    [view setDidTapUpgradeBlock:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kaibaoxiang" object:nil];
        [wself.navigationController popViewControllerAnimated:YES];
    }];
    [view present];
}

#pragma mark - fenjie

- (void)showDecompose:(PetModel *)pet {
    DecomposeAlertView * view = [[DecomposeAlertView alloc]initWithFrame:self.view.bounds];
    view.pet = pet;
    [view present];
    __weak typeof(self) wself = self;
    [view setDidTapDecomposeBlock:^(PetModel * _Nonnull pet) {
        [wself requestDecompose:pet];
    }];
}

- (void)requestDecompose:(PetModel *)model {
    NSMutableDictionary * dic = [NSMutableDictionary new];
    [dic setValue:model.pid forKey:@"id"];
    __weak typeof(self) wself = self;
    [RequestUtils POST:URL_Server(@"/decorate/resolve") parameters:dic success:^(id  _Nullable suceess) {
        NSArray * data = suceess;
        NSMutableArray * arrs = [NSMutableArray new];
        for (NSDictionary * dic in data) {
            DebrisModel * model = [DebrisModel yy_modelWithDictionary:dic];
            [arrs addObject:model];
        }
        DecomposeResultView * result = [[DecomposeResultView alloc]initWithFrame:wself.view.bounds];
        result.data = arrs;
        [result present];
        [self request:1];
        [self requestData];
    } failure:^(id  _Nullable failure) {
        [wself showTips:failure[@"msg"]];
    }];
}

#pragma mark - zhuangbei

- (void)equip:(PetModel *)pet {
    NSMutableDictionary * dic = [NSMutableDictionary new];
    [dic setValue:pet.pid forKey:@"id"];
    if (pet.equip) {
        [dic setValue:@(0) forKey:@"equip"];
    } else {
        [dic setValue:@(1) forKey:@"equip"];
    }
    __weak typeof(self) wself = self;
    [RequestUtils POST:URL_Server(@"decorate/equip.decorate") parameters:dic success:^(id  _Nullable suceess) {
        if (pet.equip) {
            [wself showTips:@"已卸下".localized];
        } else {
            [wself showTips:@"装备成功".localized];
        }
        [self requestData];
        [self request:1];
    } failure:^(id  _Nullable failure) {
        [wself showTips:failure[@"msg"]];
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 7;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        AttributeHeaderCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AttributeHeaderCell" forIndexPath:indexPath];
        __weak typeof(self) wself = self;
        cell.didHelpBlock = ^{
            NSString * str = [NSString stringWithFormat:@"https://share.famy.ly/view/dist/#/pet_rule?language=%@",[SPDCommonTool getFamyLanguage]];
            SPDHelpController *helpController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SPDHelpController"];
            helpController.url = str;
            [wself.navigationController pushViewController:helpController animated:true];
        };
        cell.didPetButtonBlock = ^{
            [wself showPet];
        };
        cell.didOpenButtonBlock = ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"kaibaoxiang" object:nil];
        };
        cell.pet = self.pet;
        return cell;
    } else if(indexPath.row == 1) {
        AttributeTitleCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AttributeTitleCell" forIndexPath:indexPath];
        cell.title = @"奖励属性".localized;
        return cell;
    } else if(indexPath.row == 2) {
        AttributeReceiveRewardCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AttributeReceiveRewardCell" forIndexPath:indexPath];
        [cell updateday:self.dict];
        __weak typeof(self) wself = self;
        [cell setDidTapReceiveBlock:^{
            [wself receive];
        }];
        return cell;
    } else if(indexPath.row == 3) {
        AttributeTitleCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AttributeTitleCell" forIndexPath:indexPath];
        cell.title = @"累计领取".localized;
        return cell;
    }  else if(indexPath.row == 4) {
        AttributeRewardTotalCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AttributeRewardTotalCell" forIndexPath:indexPath];
        [cell updateto:self.dict];
        return cell;
    } else if(indexPath.row == 5) {
        AttributeTitleCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AttributeTitleCell" forIndexPath:indexPath];
        cell.title = @"规则".localized;
        return cell;
    } else if(indexPath.row == 6) {
        AttributeIntroCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AttributeIntroCell" forIndexPath:indexPath];
        cell.goldlbl.text = @"可领取当前装备中宠物的奖励，每天可领1次，GMT+8 00:00更新。".localized;
        [cell.goldlbl sizeToFit];
        return cell;
    }
    return  [UICollectionViewCell new];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
            return CGSizeMake(kScreenW, 300);
            break;
        case 2:
            return CGSizeMake(kScreenW, [AttributeReceiveRewardCell cellheight]);
        case 4:
            return CGSizeMake(kScreenW, [AttributeRewardTotalCell cellheight]);
        case 6:
            return CGSizeMake(kScreenW, [AttributeIntroCell cellheight]);
        default:
            return CGSizeMake(kScreenW, 50);
            break;
    }
    return CGSizeMake(kScreenW, 300);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}


// MARK: - JXPagerViewListViewDelegate

- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return self.scroll;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback ?: self.scrollCallback(scrollView);
}

#pragma mark - getter

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor clearColor];
        [_collectionView registerClass:[AttributeHeaderCell class] forCellWithReuseIdentifier:@"AttributeHeaderCell"];
        [_collectionView registerClass:[AttributeReceiveRewardCell class] forCellWithReuseIdentifier:@"AttributeReceiveRewardCell"];
        [_collectionView registerClass:[AttributeTitleCell class] forCellWithReuseIdentifier:@"AttributeTitleCell"];
        [_collectionView registerClass:[AttributeRewardTotalCell class] forCellWithReuseIdentifier:@"AttributeRewardTotalCell"];
        [_collectionView registerClass:[AttributeIntroCell class] forCellWithReuseIdentifier:@"AttributeIntroCell"];
    }
    return _collectionView;
}

- (UIScrollView *)scroll {
    if (!_scroll) {
        _scroll = [UIScrollView new];
    }
    return _scroll;
}

- (NSDictionary *)dict {
    if (!_dict) {
        _dict = [NSDictionary new];
    }
    return _dict;
}

//不能自定义颜色
-(void)presentCommonController :(NSString *)title
                  messageString:(NSString *)message
                    cancelTitle:(NSString * _Nullable)cancelTitle
                   cancelAction:(_Nullable ActionHandler)cancelAction
                   confirmTitle:(NSString *)confirmTitle
                  confirmAction:(ActionHandler)confirmAction {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    
    if (message) {
        NSMutableAttributedString *messageStr = [[NSMutableAttributedString alloc] initWithString:message];
        [messageStr addAttribute:NSForegroundColorAttributeName value:SPD_HEXCOlOR(@"f56986") range:NSMakeRange(0, message.length)];
        [messageStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(0, message.length)];
        [alertController setValue:messageStr forKey:@"attributedMessage"];
    }
    
    
    if (cancelTitle.length > 0) {
        UIAlertAction *cancelAlertAction =
        [UIAlertAction actionWithTitle:cancelTitle
                                 style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action) {
                                   if (cancelAction) {
                                       cancelAction();
                                   }
                               }];
        
        if ([[UIDevice currentDevice].systemVersion floatValue] > 8.3) {
            [cancelAlertAction setValue:SPD_HEXCOlOR(@"bbbbbb") forKey:@"_titleTextColor"];
        }
        
        [alertController addAction:cancelAlertAction];
    }
    if (confirmTitle.length > 0) {
        UIAlertAction *confirmAlertAction =
        [UIAlertAction actionWithTitle:confirmTitle
                                 style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action) {
                                   if (confirmAction) {
                                       confirmAction();
                                   }
                               }];
        if ([[UIDevice currentDevice].systemVersion floatValue] > 8.3) {
            [confirmAlertAction setValue:SPD_HEXCOlOR(@"3782fe") forKey:@"_titleTextColor"];
        }

        [alertController addAction:confirmAlertAction];
    }
    [self presentViewController:alertController animated:YES completion:nil];
}
@end
