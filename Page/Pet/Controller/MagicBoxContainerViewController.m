//
//  MagicBoxContainerViewController.m
//  SimpleDate-dis
//
//  Created by houling on 2021/8/1.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "MagicBoxContainerViewController.h"
#import "MagicBoxViewController.h"
#import "PetPackageViewController.h"
#import "AttributeViewController.h"
#import "PetRankViewController.h"

@interface MagicBoxContainerViewController ()

@property (nonatomic, strong) AttributeViewController *attributeViewController;
@property (nonatomic, strong) MagicBoxViewController *boxViewController;
@property (nonatomic, strong) PetRankViewController *petRankViewController;

@property(nonatomic, strong) UIImageView * bgImageView;

@end

@implementation MagicBoxContainerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view insertSubview:self.bgImageView atIndex:0];
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(0);
    }];
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:0.0];
    self.categoryView.defaultSelectedIndex = 0;
    self.categoryView.backgroundColor = [UIColor clearColor];
    self.pagerView.defaultSelectedIndex = 0;
    self.pagerView.mainTableView.backgroundColor = [UIColor clearColor];
    self.pagerView.backgroundColor = [UIColor clearColor];
    self.pagerView.listContainerView.listCellBackgroundColor = [UIColor clearColor];
    UIButton * bag = [UIButton buttonWithType:UIButtonTypeCustom];
    [bag setTitle:@"背包".localized forState:UIControlStateNormal];
    [bag setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [bag addTarget:self action:@selector(GOTO) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:bag];
    [bag mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.centerY.equalTo(self.categoryView.mas_centerY);
        make.trailing.mas_equalTo(-15);;
    }];
    self.categoryView.titleColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    self.categoryView.titleSelectedColor = [UIColor whiteColor];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handle:) name:@"kaibaoxiang" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleChange:) name:@"CHANGEui" object:nil];

}

- (void)handle:(NSNotification *)noti {
    NSInteger index = [self.titleArray indexOfObject:@"开宝箱".localized];
    if (kIsMirroredLayout) {
        index = self.titleArray.count - index - 1;
    }
    [self.categoryView selectItemAtIndex:index];
    [self.categoryView.listContainer didClickSelectedItemAtIndex:index];
}

- (void)handleChange:(NSNotification *)noti {
    NSInteger dd = [noti.userInfo[@"index"] integerValue];
    if (dd == 0) {
        self.bgImageView.image = [UIImage imageNamed:@"pet_jinbi_bg"];
    }else if (dd == 1) {
        self.bgImageView.image = [UIImage imageNamed:@"pet_exp_bg"];
    }else if (dd == 2) {
        self.bgImageView.image = [UIImage imageNamed:@"pet_charm_bg"];
    }

}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:0.0];
    [self.navigationController.navigationBar setHidden:true];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:1.0];
    [self.navigationController.navigationBar setHidden:false];
}

- (void)GOTO {
    [self.navigationController pushViewController:[PetPackageViewController new] animated:YES];
}

- (NSMutableArray<NSString *> *)titleArray {
    return [NSMutableArray arrayWithArray:@[@"属性".localized, @"开宝箱".localized,@"排行榜".localized]];
}

- (NSMutableArray<JXPagerViewListViewDelegate> *)VCArray {
    return @[self.attributeViewController,
             self.boxViewController,
             self.petRankViewController];
}

#pragma mark - JXCategoryViewDelegate

- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    if (index == 0 || index == 1) {
        self.bgImageView.image = [UIImage imageNamed:@"pet_home_bg"];
    }
}

- (MagicBoxViewController *)boxViewController {
    if (!_boxViewController) {
        _boxViewController = [MagicBoxViewController new];
    }
    return _boxViewController;
}

- (UIImageView *)bgImageView {
    if (!_bgImageView) {
        _bgImageView = [UIImageView new];
        _bgImageView.image = [UIImage imageNamed:@"pet_home_bg"];
        _bgImageView.contentMode = UIViewContentModeScaleAspectFill;
        _bgImageView.clipsToBounds = YES;
    }
    return _bgImageView;
}

- (AttributeViewController *)attributeViewController {
    if (!_attributeViewController) {
        _attributeViewController = [AttributeViewController new];
    }
    return _attributeViewController;
}
    
- (PetRankViewController *)petRankViewController {
   if (!_petRankViewController) {
       _petRankViewController = [PetRankViewController new];
   }
   return _petRankViewController;
}
    
@end
