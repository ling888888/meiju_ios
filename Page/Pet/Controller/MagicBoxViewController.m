//
//  MagicBoxViewController.m
//  SimpleDate
//
//  Created by houling on 2021/7/24.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "MagicBoxViewController.h"
#import "MagicBoxView.h"
#import "JXPagerView.h"
#import "MagicBoxViewModel.h"
#import "OpenMagicBoxOneResultView.h"
#import "OpenMagicBoxTenResultView.h"
#import "PetSliceModel.h"
#import "MyWalletViewController.h"

@interface MagicBoxViewController ()<JXPagerViewListViewDelegate>

@property(nonatomic, strong) MagicBoxView * boxView;
@property(nonatomic, strong) UIScrollView * scroll;
@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);

@end

@implementation MagicBoxViewController

- (UIScrollView *)scroll {
    if (!_scroll) {
        _scroll = [UIScrollView new];
    }
    return _scroll;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.boxView];
    [self requestprice];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.boxView.frame = CGRectMake(0,26, self.view.bounds.size.width, self.view.bounds.size.height - 113);
}

// MARK: - JXPagerViewListViewDelegate
- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return self.scroll;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback ?: self.scrollCallback(scrollView);
}

- (void)requestOpenBoxWithNum:(NSInteger )num type:(NSString *)type {
    NSMutableDictionary * dic = [NSMutableDictionary new];
    [dic setValue:@(num) forKey:@"num"];
    [dic setValue:type forKey:@"type"];
    [RequestUtils POST:URL_Server(@"decorate/buy") parameters:dic success:^(id  _Nullable suceess) {
        NSArray * data = suceess;
        NSMutableArray * arrs = [NSMutableArray new];
        for (NSDictionary * dic in data) {
            PetSliceModel *model = [PetSliceModel initWithDictionary:dic];
            [arrs addObject:model];
        }
        if (arrs.count > 1) {
            OpenMagicBoxTenResultView * tem = [[OpenMagicBoxTenResultView alloc]initWithFrame:self.view.bounds];
            tem.data = [arrs copy];
            [tem present];
        } else {
            OpenMagicBoxOneResultView * result = [[OpenMagicBoxOneResultView alloc]initWithFrame:self.view.bounds];
            PetSliceModel *model = arrs.firstObject;
            [result update:model];
            [result present];
        }
    } failure:^(id  _Nullable failure) {
        if ([failure[@"code"] integerValue] == 2001) {
            [SPDCommonTool presentCommonController:self title:SPDStringWithKey(@"余额不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
                
            } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                [self pushToRechageController];
            }];
        }
        else {
            [self showTips:failure[@"msg"]];
        }
    }];
}

// 跳转到充值页面
- (void)pushToRechageController {
    [self.navigationController pushViewController:[MyWalletViewController new] animated:YES];
}

- (void)requestprice {
    [LY_Network getRequestWithURL:LY_Api.boxprice parameters:@{} success:^(id  _Nullable response) {
        if ([response isKindOfClass:[NSDictionary class]]) {
            [self.boxView updatedic:response];
        }
    } failture:^(NSError * _Nullable error) {
        [self showTips:error.localizedDescription];
    }];
}

#pragma mark - getters

- (MagicBoxView *)boxView {
    if (!_boxView) {
        _boxView = [MagicBoxView new];
        _boxView.backgroundColor = [UIColor clearColor];
        __weak typeof(self) wself = self;
        _boxView.openBoxClicked = ^(NSDictionary * _Nonnull param, NSInteger num) {
            [wself requestOpenBoxWithNum:num type:param[@"type"]];
        };
    }
    return _boxView;
}

@end
