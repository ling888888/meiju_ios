//
//  PetPackageViewController.h
//  SimpleDate
//
//  Created by houling on 2021/8/14.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LY_BaseCollectionViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PetPackageViewController : LY_BaseCollectionViewController

@end

NS_ASSUME_NONNULL_END
