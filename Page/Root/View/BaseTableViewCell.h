//
//  BaseTableViewCell.h
//  SimpleDate
//
//  Created by xu.juvenile on 2016/3/3.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableViewCell : UITableViewCell

- (NSURL *)getImageUrlStringFromPath:(NSString *)avatarImgURL;

- (NSString *)getVideoUrlStringFromPath:(NSString *)avatarImgURL;

+ (id)cellWithTableView:(UITableView*)tableView ;



@end
