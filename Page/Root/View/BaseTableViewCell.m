//
//  BaseTableViewCell.m
//  SimpleDate
//
//  Created by xu.juvenile on 2016/3/3.
//  Copyright © 2016年 WYB. All rights reserved.
//


#import "BaseTableViewCell.h"


@implementation BaseTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

+ (id)cellWithTableView:(UITableView*)tableView {
    NSString* className = NSStringFromClass([self class]);
    UINib* nib          = [UINib nibWithNibName:className bundle:nil] ;
    [tableView registerNib:nib forCellReuseIdentifier:className];
    return [tableView dequeueReusableCellWithIdentifier:className];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (NSURL *)getImageUrlStringFromPath:(NSString *)avatarImgURL{
    return [NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,avatarImgURL]];
}

- (NSString *)getVideoUrlStringFromPath:(NSString *)avatarImgURL{
    return [NSString stringWithFormat:STATIC_DOMAIN_VIDEO_URL,avatarImgURL];
}

- (NSURL *)getThumbnailImageURL:(NSString *)videoURL{
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@?vframe/jpg/offset/0",videoURL]];
}

@end
