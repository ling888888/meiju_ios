//
//  BaseViewController.m
//  SimpleDate
//
//  Created by xu.juvenile on 2016/3/3.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "BaseViewController.h"
#import "UIColor+XXWAddition.h"
#import "SPDConversationViewController.h"
#import "MyWalletViewController.h"
#import "PresentView.h"
#import "PresentMessage.h"
#import "SPDHashGenerator.h"
#import "VipViewController.h"
#import "ChatRoomLiveViewController.h"
#import "SPDClanListViewController.h"
#import "SPDHelpController.h"
#import "ZegoKitManager.h"
#import "SPDPrivateMagicMessage.h"
#import "PresentView.h"
#import "ChatroomVoiceViewController.h"
#import "KKGiftGivingView.h"
#import "HomeModel.h"
//#import "RadioRoomContainerController.h"
#import "DBUtil.h"
#import "LY_HomePageViewController.h"

@interface BaseViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,PresentViewDelegate>

@property(nonatomic,strong) PresentView *presentView;

@property (nonatomic,strong) NSMutableArray *giftArray;

@property (nonatomic,copy)__block NSString *goldNum;

@property (nonatomic,strong) NSMutableArray *vehicleArray;

@property (nonatomic,strong) NSMutableArray *magicArray;

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    [self configLocationManager];
    
    //设置导航栏的右侧滑动手势 --因为自定义了导航栏baseNav 为了统一返回按钮
    self.navigationController.interactivePopGestureRecognizer.delegate=(id)self;
}

- (void)jumpToSafari:(NSURL *)myURL{
    [[UIApplication sharedApplication] openURL:myURL];
}



#pragma mark - 提示框
- (void)showMessageToast:(NSString *)text {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
    [hud setMode:MBProgressHUDModeText];
    [hud setLabelText:text];
    [hud setRemoveFromSuperViewOnHide:YES];
    [hud hide:YES afterDelay:0.8];
}


#pragma mark - UIImagePickerController

- (void)presentImagePickerController:(NSString *)title
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:SPDStringWithKey(@"拍照", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
            [imagePickerController setDelegate:self];
            [imagePickerController setAllowsEditing:YES];
            [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
            [self presentViewController:imagePickerController animated:YES completion:nil];
        }
    }];
    UIAlertAction *albumAction = [UIAlertAction actionWithTitle:SPDStringWithKey(@"相册", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        [imagePickerController setDelegate:self];
        [imagePickerController setAllowsEditing:YES];
        [imagePickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:SPDStringWithKey(@"取消", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alertController addAction:cameraAction];
    [alertController addAction:albumAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
//    UIImage *image = info[UIImagePickerControllerEditedImage];
//    [picker dismissViewControllerAnimated:YES completion:^{
//        UIImage *resizedImage = [image resizedImageToFitInSize:CGSizeMake(1024.0, 1024.0) scaleIfSmaller:NO];
//        self.guidebook.localImage = [resizedImage saveToTmpPath];
//        NSIndexPath *headerIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//        [self.tableView reloadRowsAtIndexPaths:@[headerIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
//    }];
}


/**
 *  用UIBezierPath创建矢量圆弧
 *
 *  @param view   cell
 *  @param direct top | bottom
 *
 *  @return CALayer
 */
- (CALayer *)makeCorner:(UIView *)view withDirect:(UITableViewCellCornerDirectString)direct
{
    
    UIRectCorner rectCorner;
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];

    if (direct == UITableViewCellCornerTop) {
        rectCorner = UIRectCornerTopLeft | UIRectCornerTopRight;
    }
    else {
        rectCorner = UIRectCornerBottomLeft | UIRectCornerBottomRight;
    }
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:rectCorner cornerRadii:CGSizeMake(10, 10)];
    maskLayer.frame = view.bounds;
    maskLayer.path = maskPath.CGPath;
    
    return maskLayer;
}


- (NSURL *)getCoverImageUrlStringFromPath:(NSString *)coverImgURL{
    return [NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,coverImgURL]];
}



- (void)presentAlertController:(NSString *)message
                   cancelTitle:(NSString * _Nullable)cancelTitle
                  cancelAction:(_Nullable ActionHandler)cancelAction
                  confirmTitle:(NSString *)confirmTitle
                 confirmAction:(ActionHandler)confirmAction {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@""
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    if (cancelTitle.length > 0) {
        UIAlertAction *cancelAlertAction =
        [UIAlertAction actionWithTitle:cancelTitle
                                 style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action) {
                                   if (cancelAction) {
                                       cancelAction();
                                   }
                               }];
        [alertController addAction:cancelAlertAction];
    }
    if (confirmTitle.length > 0) {
        UIAlertAction *confirmAlertAction =
        [UIAlertAction actionWithTitle:confirmTitle
                                 style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action) {
                                   if (confirmAction) {
                                       confirmAction();
                                   }
                               }];
        [alertController addAction:confirmAlertAction];
    }
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)presentAlertWithTitle:(nullable NSString *)title
                      message:(nullable NSString *)message
                  cancelTitle:(nullable NSString *)cancelTitle
                cancelHandler:(void (^ __nullable)(UIAlertAction *action))cancelHandler
                  actionTitle:(nullable NSString *)actionTitle
                actionHandler:(void (^ __nullable)(UIAlertAction *action))actionHandler {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:SPDStringWithKey(title, nil) message:SPDStringWithKey(message, nil) preferredStyle:UIAlertControllerStyleAlert];
    if (cancelTitle.notEmpty) {
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:SPDStringWithKey(cancelTitle, nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if (cancelHandler) {
                cancelHandler(action);
            }
        }];
        [alertController addAction:cancelAction];
    }
    if (actionTitle.notEmpty) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:SPDStringWithKey(actionTitle, nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if (actionHandler) {
                actionHandler(action);
            }
        }];
        [alertController addAction:action];
    }
    [self presentViewController:alertController animated:YES completion:nil];
}

- (NSString *)getStringFromCityWholeText:(NSString *)wholeText{
    NSRange range = [wholeText rangeOfString:@" "];
    if (range.location == NSNotFound) {
        return wholeText;
    }
    return [wholeText substringFromIndex:range.location+1];
}

- (void)pushToConversationViewControllerWithTargetId:(NSString *)targetId {
    SPDConversationViewController *vc = [[SPDConversationViewController alloc] initWithConversationType:ConversationType_PRIVATE targetId:targetId];
    [self.navigationController pushViewController:vc animated:YES];
}


-(void)pushToDetailTableViewController:(UIViewController *)viewController
                                userId:(NSString *)userId{
    LY_HomePageViewController * vc = [[LY_HomePageViewController alloc] initWithUserId:userId];
    [viewController.navigationController pushViewController:vc animated:YES];
}

// 跳转到充值页面
- (void)pushToRechageController {
    [self.navigationController pushViewController:[MyWalletViewController new] animated:YES];
}


- (void)joinChatroomWithClanId:(NSString *)clanId {
    [ZegoManager joinRoomFrom:self clanId:clanId];
}


#warning 有问题
- (void)saveUserDefaultFormRequestAPI:(NSDictionary *)dataDic{
    NSString *ry_token = dataDic[@"ry_token"];
    NSString *avatar = dataDic[@"avatar"];
    NSString *headwearWebp = dataDic[@"headwearWebp"];
    NSString *_ID = dataDic[@"_id"];
//    NSDictionary *userInfo = @{
//                               @"_id" : dataDic[@"_id"],
//                               @"avatar" : dataDic[@"avatar"],
//                               @"ry_token" : dataDic[@"ry_token"],
//                               @"nick_name" : dataDic[@"nick_name"],
//                               @"if_special" :@([dataDic[@"if_special"] boolValue]),
//                               @"gender":dataDic[@"gender"],
//                               @"age": @([dataDic[@"age"] intValue]),
//                               };
    [[SPDApiUser currentUser] saveUserInfoToDefault:dataDic];
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    if (userDef) {
        [userDef setObject:_ID forKey:@"_id"];
        [userDef setObject:avatar forKey:@"avatar"];
        [userDef setObject:headwearWebp forKey:@"headwearWebp"];
        [userDef setObject:ry_token forKey:@"ry_token"];
        [userDef setObject:dataDic[@"is_anchor"] forKey:@"is_anchor"];
        [userDef synchronize];
    }
    LY_User *user = [LY_User yy_modelWithDictionary:dataDic];
    [user save];
}

- (NSURL *)getThumbnailImageURL:(NSString *)videoURL{
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@?vframe/jpg/offset/0",videoURL]];
}

- (void)videoChatWithUserID:(NSString *)userID{
    [self postCallDialingToUserId:userID type:@"video"];

    [[RCCall sharedRCCall] startSingleCall:userID mediaType:RCCallMediaVideo];
    
}


- (void)voiceChatWithUserId:(NSString *)userID{
    [self postCallDialingToUserId:userID type:@"audio"];

    [[RCCall sharedRCCall] startSingleCall:userID mediaType:RCCallMediaAudio];
}


- (void)postCallDialingToUserId:(NSString *)toUserId type:(NSString *)type{
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:toUserId forKey:@"to_user_id"];
    [dict setObject:type forKey:@"type"];
    [dict setObject:@"dialing" forKey:@"status"];
    
    [RequestUtils commonPostRequestUtils:dict bURL:@"call.count" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        NSLog(@"发送主叫方API成功");
    } bFailure:nil];
}

- (void)postCallActiveToUserId:(NSString *)toUserId type:(NSString *)type sec:(NSInteger)sec{
    if (!toUserId) {return;}
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:toUserId forKey:@"to_user_id"];
    [dict setObject:type forKey:@"type"];
    [dict setObject:@"active" forKey:@"status"];
    [dict setObject:@(sec) forKey:@"sec"];
    
    [RequestUtils commonPostRequestUtils:dict bURL:@"call.count" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        NSLog(@"发送主叫方拨打记录 success ");
    } bFailure:nil];
}
- (void)showPresentViewWithTargetId:(NSString *)targetId type:(NSString *)type {
    if ([type isEqualToString:@"magic"]) {
        self.presentView.selectedIndex = SPDSelectedMagic;
    }
    [self showPresentViewWithTargetId:targetId];
}

//点击视频界面的
-(void)showPresentViewWithTargetId:(NSString *)targetId{
    HomeModel * model = [HomeModel new];
    model._id = targetId;
    model.avatar = @"";
    model.nick_name = @"";
    KKGiftGivingView *view = [[[NSBundle mainBundle] loadNibNamed:@"KKGiftGivingView" owner:self options:nil]firstObject];
    view.receiveGiftUserModel = model;
    view.frame = self.view.bounds;
    view.delegate = self;
    [[UIApplication sharedApplication].keyWindow addSubview:view];
}

- (void)requestVehicleListApi {
    
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL: @"vehicle.list" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        [self.vehicleArray removeAllObjects];
        for (NSDictionary *dic in suceess[@"list"]) {
            SPDVehicleModel *model = [SPDVehicleModel initWithDictionary:dic];
            [self.vehicleArray addObject:model];
        }
        
        self.presentView.vehicleArray = self.vehicleArray;
        
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

-(PresentView *)presentView{
    if (!_presentView) {
        _presentView=[PresentView presentView];
        _presentView.delegate=self;
        
    }
    return _presentView;
}

#pragma mark - present View Delegate

- (void)clickedBillRechargeButton {
    [self pushToRechageController];
}

//点击赠送按钮
- (void)clickedSendPresentButtonWithModel:(GiftListModel *)model num:(NSString *)num {
    SPDApiUser *user = [SPDApiUser currentUser];
    BOOL if_special = [user.if_special boolValue];
    if (if_special) {
        [self showToast:@"无法赠送"];
    }else{
        [self.presentView dismiss];
        model.num = num;
        [self sendPresentToUserWith:model withToUser_id:self.presentToId];
    }
}

//点击赠送----送的是座驾
- (void)clickedSendVehicleButtonWithVehicleModel:(SPDVehicleModel * )vehicleModel {
    
    double errorTime = [[[NSUserDefaults standardUserDefaults] objectForKey:@"errorTime"] integerValue];
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    
    SPDApiUser *user = [SPDApiUser currentUser];
    BOOL  if_special = [user.if_special boolValue];
    
    if (!if_special) {
        //可以正常购买
        if (errorTime == 0 || currentTime - errorTime > 90) {
            [self requestVehicleBuy:vehicleModel];
        }else{
            [self showToast:SPDStringWithKey(@"请耐心等候足够90s中哦！", nil)];
        }
    }else{
        [self showToast:SPDStringWithKey(@"无法赠送", nil)];
    }
}

//点击魔法
-(void)clickedSendMagicButtonWithSPDMagicModel:(SPDMagicModel * )SPDMagicModel {
    //判断当前model 的道具个数是否 >0 : 大于0 的话 在执行 present view 的一个方法 （减少数组里面 model 的 num）
    if ([SPDMagicModel.num integerValue] > 0) {
        [self requestMagicBuy:SPDMagicModel];
    }else {
        //弹框
        NSString*title = [NSString stringWithFormat:SPDStringWithKey(@"确定花费%@金币使用此魔法？", nil),SPDMagicModel.cost];
        [SPDCommonTool presentAutoController:[SPDCommonTool getWindowTopViewController] title:title titleColor:@"" titleFontSize:0 messageString:@"" messageColor:@"" messageFontSize:0 cancelTitle:SPDStringWithKey(@"取消",nil) cancelAction:^{
        } confirmTitle:SPDStringWithKey(@"确定",nil) confirmAction:^{
            [self requestMagicBuy:SPDMagicModel];
        }];
    }
}

- (void)requestMagicBuy:(SPDMagicModel *)magicModel {
    //先请求一个发送api
    NSLog(@"点击了赠送magic");
    if ([self.presentToId isEqualToString:[SPDApiUser currentUser].userId]) {
        [self showToast:SPDStringWithKey(@"不可以对自己使用魔法", nil)];
        return;
    }
    NSMutableDictionary * dict = [NSMutableDictionary new];
    [dict setObject:self.presentToId forKey:@"user_id"];
    [dict setObject:magicModel._id forKey:@"magic_id"];
//是不是在聊天室内
    if (ZegoManager.clan_id) {
        [dict setObject:ZegoManager.clan_id forKey:@"chatroom_id"];
    }
    [RequestUtils commonPostRequestUtils:dict bURL:@"magic.send.to" bAnimated:YES bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        dispatch_async(dispatch_get_main_queue(), ^{
//            if ([magicModel.num integerValue] > 0 && self.magicArray.count) {
//                NSInteger num = [magicModel.num integerValue] -1;
//                NSInteger index = [self.magicArray indexOfObject:magicModel];
//                SPDMagicModel * model = self.magicArray[index];
//                [model setNum:@(num)];
//            }
            SPDPrivateMagicMessage *magicMsg = [SPDCommonTool getPrivateMagicMessageWithDict:suceess andMagicModel:magicModel andReceiveId:self.presentToId] ;
            [[RCIM sharedRCIM] sendMessage:ConversationType_PRIVATE targetId:self.presentToId content:magicMsg pushContent:magicMsg.conversationDigest pushData:magicMsg.conversationDigest success:^(long messageId) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self showToast:SPDStringWithKey(@"魔法使用成功", nil)];
                    //发送通知
                    if (!isVideoStatus) {
                        NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithDictionary:suceess];
                        if ([magicModel.num integerValue] > 0) {
                            [dict setObject:@(0) forKey:@"is_cost"];//标志次魔法是否使用道具 yes
                        }else{
                            [dict setObject:@(1) forKey:@"is_cost"];//标志次魔法是否使用道具 yes
                        }
                        [[NSNotificationCenter defaultCenter] postNotificationName:CHATROOM_SENDPRESENT object:magicModel userInfo:dict];
                    }
                });
            } error:^(RCErrorCode nErrorCode, long messageId) {
                NSLog(@"释放魔法失败-----");
            }];
        });
    } bFailure:^(id  _Nullable failure) {
        
        if ([failure isKindOfClass:[NSDictionary class]]) {
            if ([failure[@"code"] integerValue] == 2002) {
                if (isVideoStatus) {
                    [self showToast:SPDStringWithKey(@"金币不足，请充值", nil)];
                }else{
                    [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
                    } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                        [self pushToRechageController];
                    }];
                }
            }else{
                [self showToast:failure[@"msg"]];
            }
        }
        
        
    }];
        
}



//扣费成功发送 私聊魔法消息 -》 success：（chatroom 给通知 给聊天室发消息 使chatroom的 发送魔法效果）
//result - success fail 施加魔法效果的成功失败 给聊天室的人展示不同的效果
- (void)sendPrivateMagicMessageWith:(SPDMagicModel *)SPDMagicModel  andResult:(NSString *)result {
    //发送私聊消息成功 如果在聊天室里面就给聊天室发送通知
    //发送通知 告知相关页面 已经扣费成功
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!isVideoStatus) {
            [[NSNotificationCenter defaultCenter] postNotificationName:CHATROOM_SENDPRESENT object:SPDMagicModel userInfo:@{@"reslut":result}];
        }
    });
}

//送座驾扣费------
- (void)requestVehicleBuy:(SPDVehicleModel *)vehicle_Model {
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setValue:self.presentToId forKey:@"user_id"];
    [dic setValue:vehicle_Model.vehicle_id forKey:@"vehicle_id"];
    if ([ZegoKitManager sharedInstance].clan_id) {
        if (isVideoStatus) {// 在聊天室中 私聊某人在视频中
            [dic setValue:@"person" forKey:@"scene_type"];
        }else{
            [dic setValue:@"chatroom" forKey:@"scene_type"];
            [dic setValue:[ZegoKitManager sharedInstance].clan_id forKey:@"scene_id"];
        }
    }else{
        [dic setValue:@"person" forKey:@"scene_type"];
    }
    [RequestUtils commonPostRequestUtils:dic bURL:@"vehicle.buy" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        [vehicle_Model setExpire_time:suceess[@"expire_time"]];
        NSLog(@"送座驾扣费成功----");
        [self sendVehicleMessage:vehicle_Model];
    } bFailure:^(id  _Nullable failure) {
        
        if ([failure[@"code"] integerValue] == 203) {
            if (isVideoStatus) {
                [self showToast:SPDStringWithKey(@"金币不足，请充值", nil)];
            }else{
                [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:@"取消" cancelAction:^{
                } confirmTitle:@"确定" confirmAction:^{
                    [self pushToRechageController];
                }];
            }
        }else {
            [self showToast:failure[@"msg"]];
        }
    }];
}

//发送座驾消息---
- (void)sendVehicleMessage:(SPDVehicleModel *)vehicle_Model {
    //发送自定义消息 ---以及发送送礼物减少时间通知
    PresentMessage * msg = [PresentMessage messageWithContent:@"送你一辆座驾，快去车库装备吧" andVehicle_id:vehicle_Model.vehicle_id andGiftName:vehicle_Model.name andImageName:vehicle_Model.image andGoldNum:@"0" andCharamScore:@"0" andType:@"vehicle" andExpire_time:vehicle_Model.expire_time_str];
    
    [[RCIM sharedRCIM] sendMessage:ConversationType_PRIVATE targetId:self.presentToId content:msg pushContent:@"送你一辆座驾，快去车库装备吧" pushData:@"送你一辆座驾，快去车库装备吧" success:^(long messageId) {
        NSLog(@"送座驾成功-----");
        [[SPDCommonTool shareTool] checkExpWithGlodNum:[vehicle_Model.price integerValue] WithType:ExpVehicle];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showToast:SPDStringWithKey(@"送座驾成功!", nil)];
            //不是在视频中 就是在家族聊天室
            if (!isVideoStatus) {
                [[NSNotificationCenter defaultCenter] postNotificationName:CHATROOM_SENDPRESENT object:vehicle_Model];
            }
        });
    } error:^(RCErrorCode nErrorCode, long messageId) {
        NSLog(@"送座驾失败-----");
    }];
    
}

//检查余额够不够
-(void)checkBalanceWithModel:(GiftListModel *)model wihtToUser_id:(NSString *)user_id{
    
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.balance" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        
        NSString * goldNum=[suceess[@"balance"] stringValue];
        NSUInteger goldNumInteger=[goldNum integerValue];
        if (goldNumInteger < model.cost.integerValue * model.num.integerValue) {
            NSLog(@"余额不足请充值");
            if (isVideoStatus) {
                [self showToast:SPDStringWithKey(@"金币不足，请充值", nil)];
            }else{
                [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
                    
                } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                    [self pushToRechageController];
                }];
            }
        }else{
            [self sendPresentToUserWith:model withToUser_id:user_id];
        }
        
    } bFailure:nil];
    
}

//发送自己自定义消息
-(void)sendMyMessageWithModel:(GiftListModel *)model withToUser_id:(NSString *)user_id{
    
    NSString *goldNum = [NSString stringWithFormat:@"%ld", model.cost.integerValue * model.num.integerValue];
    NSString *charamScore = [NSString stringWithFormat:@"%ld", model.charm.integerValue * model.num.integerValue];
    PresentMessage *message=[PresentMessage  messageWithContent:SPDStringWithKey(@"送给你一个礼物希望你会喜欢", nil) andGiftName:model.name andImageName:model.cover andGoldNum:goldNum andCharamScore:charamScore];
    message._id = model._id;
    message.num = model.num;
    message.currencyType = [model.currencyType isEqualToString:@"gold"] ? 0 : 1;
    [[RCIM sharedRCIM] sendMessage:ConversationType_PRIVATE targetId:user_id  content:message pushContent:SPDStringWithKey(@"送给你一个礼物希望你会喜欢", nil) pushData:SPDStringWithKey(@"送给你一个礼物希望你会喜欢", nil) success:^(long messageId) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showToast:SPDStringWithKey(@"礼物发送成功", nil)];
            [[SPDCommonTool shareTool] checkExpWithGlodNum:[model.cost integerValue] WithType:ExpGiftType];
            //此时只有  一对一视频 和 聊天室送 用这里的方法 以后可以改
            NSDictionary * dict = @{@"user_id":user_id}; //送给谁user_ID
            if (!isVideoStatus) {
                [[NSNotificationCenter defaultCenter] postNotificationName:CHATROOM_SENDPRESENT object:model userInfo:dict];

            }
        });
    } error:nil];
    
}

//向服务器-发送礼物凭证
-(void)sendPresentToUserWith:(GiftListModel *)model withToUser_id:(NSString *)user_id{
    NSMutableDictionary * dict = [NSMutableDictionary new];
    [dict setValue:user_id forKey:@"user_id"];
    [dict setValue:model._id forKey:@"gift_id"];
    [dict setValue:model.num forKey:@"num"];
    if (isVideoStatus) {
        if ([RCCall sharedRCCall].currentCallSession.mediaType == RCCallMediaVideo) {
            [dict setValue:@"video" forKey:@"scene_type"];
        } else {
            [dict setValue:@"audio" forKey:@"scene_type"];
        };
    } else if (ZegoManager.clan_id) {
        [dict setValue:@"chatroom" forKey:@"scene_type"];
        [dict setValue:ZegoManager.clan_id forKey:@"scene_id"];
    }
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dict] bURL:@"gift.send.to" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        NSLog(@"向服务器发送礼物凭证成功--");
        [self sendMyMessageWithModel:model withToUser_id:user_id];
    } bFailure:^(id  _Nullable failure) {
        if ([failure isKindOfClass:[NSDictionary class]]) {
            if ([failure[@"code"] integerValue] == 2002) {
                NSString *title = SPDStringWithKey([model.currencyType isEqualToString:@"gold"] ? @"金币不足，请充值" : @"钻石不足，充金币得钻石", nil);
                [self presentCommonController:title messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
                    
                } confirmTitle:SPDStringWithKey(@"充值", nil) confirmAction:^{
                    [self pushToRechageController];
                }];
            } else {
                [self showToast:failure[@"msg"]];
            }
        }
    }];
    
}

-(void)requestGiftListApi{
    //礼物列表
    NSDictionary *dict=@{ @"type":@"chatlive"};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dict] bURL:@"gift.list" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        
        if (self.giftArray.count!=0) {
            [self.giftArray removeAllObjects];
        }
        NSArray *giftsArray = suceess[@"gift"];
        for (NSDictionary *dict in giftsArray) {
            GiftListModel *model = [GiftListModel initWithDictionary:dict];
            [self.giftArray addObject:model];
        }
    } bFailure:nil];
    
    
}

- (void)showToast:(NSString *)text {
    [MBProgressHUD hideAllHUDsForView:[UIApplication sharedApplication].keyWindow animated:YES];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    hud.userInteractionEnabled = NO;
    hud.mode = MBProgressHUDModeText;
    hud.margin = 10.0f;
    hud.yOffset = 15.0f;
    hud.opacity = 0.5f;
    hud.removeFromSuperViewOnHide = YES;
    hud.detailsLabelText = text;
    hud.detailsLabelFont = [UIFont systemFontOfSize:15];
    [hud hide:YES afterDelay:2.5];
}

#pragma mark - take screen shot

-(UIImage *)takeScreenShot{
    
    NSLog(@"------takeScreenShot----");
    // 将要被截图的view,即窗口的根控制器的view(必须不含状态栏,默认ios7中控制器是包含了状态栏的)
    UIViewController *beyondVC = self.navigationController.view.window.rootViewController;
    // 背景图片 总的大小
    CGSize size = beyondVC.view.frame.size;
    // 开启上下文,使用参数之后,截出来的是原图（YES  0.0 质量高）
    UIGraphicsBeginImageContextWithOptions(size, YES, 0.0);
    // 要裁剪的矩形范围
    CGRect rect = CGRectMake(0, 0, kScreenW, kScreenH);
    //注：iOS7以后renderInContext：由drawViewHierarchyInRect：afterScreenUpdates：替代
    
    UIWindow *window= [UIApplication sharedApplication].keyWindow;
    [window drawViewHierarchyInRect:rect afterScreenUpdates:NO];
    
    UIImage *snapshot = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return snapshot;
}

-(NSString *) genSignature:(NSDictionary *)params andSecretKey:(NSString *) secretkey{
    
    NSArray *keyArray = [params allKeys];
    NSArray *sortArray = [keyArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    NSMutableArray *valueArray = [NSMutableArray array];
    for (NSString *sortString in sortArray) {
        [valueArray addObject:[params objectForKey:sortString]];
    }
    NSMutableArray *signArray = [NSMutableArray array];
    for (int i = 0; i < sortArray.count; i++) {
        NSString *keyValueStr = [NSString stringWithFormat:@"%@%@",sortArray[i],valueArray[i]];
        [signArray addObject:keyValueStr];
    }
    NSString *sign = [signArray componentsJoinedByString:@""];
    NSLog(@"sign==%@",sign);
    
    NSString *generateSignStr=[NSString stringWithFormat:@"%@%@",sign,secretkey];
    
    return [SPDHashGenerator md5:generateSignStr];
    
}

-(UIImage *)scaleImage:(UIImage *)image toKb:(NSInteger)kb{
    
    if (!image) {
        return image;
    }
    if (kb<1) {
        return image;
    }
    
    kb*=1024;
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    NSData *imageData = UIImageJPEGRepresentation(image, compression);
    while ([imageData length] > kb && compression > maxCompression) {
        compression -= 0.1;
        imageData = UIImageJPEGRepresentation(image, compression);
    }
    NSLog(@"当前大小:%fkb",(float)[imageData length]/1024.0f);
    UIImage *compressedImage = [UIImage imageWithData:imageData];
    return compressedImage;
  
}


#pragma mark - 礼物金币不组提示消息

//不能自定义颜色
-(void)presentCommonController :(NSString *)title
                  messageString:(NSString *)message
                    cancelTitle:(NSString * _Nullable)cancelTitle
                   cancelAction:(_Nullable ActionHandler)cancelAction
                   confirmTitle:(NSString *)confirmTitle
                  confirmAction:(ActionHandler)confirmAction {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    
    if (message) {
        NSMutableAttributedString *messageStr = [[NSMutableAttributedString alloc] initWithString:message];
        [messageStr addAttribute:NSForegroundColorAttributeName value:SPD_HEXCOlOR(@"f56986") range:NSMakeRange(0, message.length)];
        [messageStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(0, message.length)];
        [alertController setValue:messageStr forKey:@"attributedMessage"];
    }
    
    
    if (cancelTitle.length > 0) {
        UIAlertAction *cancelAlertAction =
        [UIAlertAction actionWithTitle:cancelTitle
                                 style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action) {
                                   if (cancelAction) {
                                       cancelAction();
                                   }
                               }];
        
        if ([[UIDevice currentDevice].systemVersion floatValue] > 8.3) {
            [cancelAlertAction setValue:SPD_HEXCOlOR(@"bbbbbb") forKey:@"_titleTextColor"];
        }
        
        [alertController addAction:cancelAlertAction];
    }
    if (confirmTitle.length > 0) {
        UIAlertAction *confirmAlertAction =
        [UIAlertAction actionWithTitle:confirmTitle
                                 style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action) {
                                   if (confirmAction) {
                                       confirmAction();
                                   }
                               }];
        if ([[UIDevice currentDevice].systemVersion floatValue] > 8.3) {
            [confirmAlertAction setValue:SPD_HEXCOlOR(@"3782fe") forKey:@"_titleTextColor"];
        }

        [alertController addAction:confirmAlertAction];
    }
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)presentAutoController :(NSString *)title
                   titleColor:(NSString *)titleColor
                   titleFontSize:(CGFloat)titleFontSize
                  messageString:(NSString *)message
                   messageColor:(NSString *)messageColor
                messageFontSize:(CGFloat)messageFontSize
                    cancelTitle:(NSString * _Nullable)cancelTitle
                   cancelAction:(_Nullable ActionHandler)cancelAction
                   confirmTitle:(NSString *)confirmTitle
                  confirmAction:(ActionHandler)confirmAction {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    if (message) {
        NSMutableAttributedString *messageStr = [[NSMutableAttributedString alloc] initWithString:message];
        [messageStr addAttribute:NSForegroundColorAttributeName value:SPD_HEXCOlOR(messageColor) range:NSMakeRange(0, message.length)];
        [messageStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:messageFontSize] range:NSMakeRange(0, message.length)];
        [alertController setValue:messageStr forKey:@"attributedMessage"];
    }
    
    
    if (cancelTitle.length > 0) {
        UIAlertAction *cancelAlertAction =
        [UIAlertAction actionWithTitle:cancelTitle
                                 style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action) {
                                   if (cancelAction) {
                                       cancelAction();
                                   }
                               }];
        
        if ([[UIDevice currentDevice].systemVersion floatValue] > 8.3) {
            [cancelAlertAction setValue:SPD_HEXCOlOR(@"bbbbbb") forKey:@"_titleTextColor"];
        }
        
        [alertController addAction:cancelAlertAction];
    }
    if (confirmTitle.length > 0) {
        UIAlertAction *confirmAlertAction =
        [UIAlertAction actionWithTitle:confirmTitle
                                 style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action) {
                                   if (confirmAction) {
                                       confirmAction();
                                   }
                               }];
        if ([[UIDevice currentDevice].systemVersion floatValue] > 8.3) {
            [confirmAlertAction setValue:SPD_HEXCOlOR(@"3782fe") forKey:@"_titleTextColor"];
        }
        
        [alertController addAction:confirmAlertAction];
    }
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)pushToVipViewController:(UIViewController *)viewController WithIsVip:(BOOL) isVip {
    
    UIStoryboard *home=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VipViewController *vc=[home instantiateViewControllerWithIdentifier:@"VipViewController"];
    vc.hidesBottomBarWhenPushed=YES;
    vc.is_vip=isVip;
    [viewController.navigationController pushViewController:vc animated:YES];
    
}

- (void)pushToAllClanList {
    SPDClanListViewController *vc = [SPDClanListViewController new];
    [self.navigationController pushViewController:vc animated:YES];
}


- (NSMutableArray *)giftArray {
    if (!_giftArray) {
        _giftArray = [NSMutableArray array];
    }
    return _giftArray;
}

-(NSMutableArray *)vehicleArray {
    if (!_vehicleArray) {
        _vehicleArray = [NSMutableArray array];
    }
    return _vehicleArray;
}

- (NSMutableArray *)magicArray {
    if (!_magicArray) {
        _magicArray = [NSMutableArray array];
    }
    return _magicArray;
}

- (void)loadWebViewController:(UIViewController *)vc title:(NSString *)title url:(NSString *)url {
    SPDHelpController *helpController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SPDHelpController"];
    helpController.url = url;
    helpController.titleText = title;
    [vc.navigationController pushViewController:helpController animated:YES];
}


@end
