//
//  BaseViewController.h
//  SimpleDate
//
//  Created by xu.juvenile on 2016/3/3.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "GiftListModel.h"

NS_ASSUME_NONNULL_BEGIN

@class SPDMagicModel;

typedef enum {
    UITableViewCellCornerTop,
    UITableViewCellCornerBottom
}UITableViewCellCornerDirectString;

typedef void (^ActionHandler)(void);

typedef void (^XXWLocationResponseSuccess)(NSString *latitude, NSString *longitude, NSString *province);




typedef void (^XXWLocationResponseFailure)(NSError *error);

typedef void (^ProviceAndCityResponseSuccess)(NSString *latitude, NSString *longitude, NSString *province,NSString *city,NSString *district);

static int  unreadCountTabIndex = 1;

@interface BaseViewController : UIViewController


- (void)showMessageToast:(NSString *)text ;

- (void)presentAlertController:(NSString *)message
                   cancelTitle:(NSString * _Nullable )cancelTitle
                  cancelAction:(_Nullable ActionHandler)cancelAction
                  confirmTitle:(NSString *)confirmTitle
                 confirmAction:(ActionHandler)confirmAction;

- (void)presentAlertWithTitle:(nullable NSString *)title
                      message:(nullable NSString *)message
                  cancelTitle:(nullable NSString *)cancelTitle
                cancelHandler:(void (^ __nullable)(UIAlertAction *action))cancelHandler
                  actionTitle:(nullable NSString *)actionTitle
                actionHandler:(void (^ __nullable)(UIAlertAction *action))actionHandler;

- (void)jumpToSafari:(NSURL*)myURL;


- (void)presentImagePickerController:(NSString *)title;
- (CALayer *)makeCorner:(UIView *)view withDirect:(UITableViewCellCornerDirectString)direct;
- (NSURL *)getCoverImageUrlStringFromPath:(NSString *)coverImgURL;





/**
 *  @author xu.juvenile, 16-03-14 19:03:23
 *
 *  @brief 城市只需要显示二三级目录
 *
 *  @param wholeText API或者PickerView返回的数据
 *
 *  @return 截取二三级字符串
 */

- (NSString *)getStringFromCityWholeText:(NSString *)wholeText;


- (void)pushToConversationViewControllerWithTargetId:(NSString *)targetId;


/**
 *  @author xu.juvenile, 16-03-21 14:03:13
 *
 *  @brief 去个人详情
 */

-(void)pushToDetailTableViewController:(UIViewController *)viewController
                                userId:(NSString *)userId;


//跳转到充值页面
-(void)pushToRechageController;

-(void)pushToVipViewController:(UIViewController *)viewController WithIsVip:(BOOL) isVip ;

// 加入聊天室
- (void)joinChatroomWithClanId:(NSString *)clanId;



/**
 *  @author xu.juvenile, 16-03-22 18:03:25
 *
 *  @brief 处理登录/注册成功后保存用户融云token等一系列问题
 *
 *  @param response 服务器返回data
 */
- (void)saveUserDefaultFormRequestAPI:(NSDictionary *)response;



//视频缩略图
//- (UIImage *)getThumbnailImage:(NSString *)videoURL;

- (NSURL *)getThumbnailImageURL:(NSString *)videoURL;

- (void)pushToAllClanList ;

//表白我
- (void)pushToInvitationToMe:(UIViewController *)vc atId:(NSString*)invitationId;

//开启视频聊天
- (void)videoChatWithUserID:(NSString *)userID;

//开启音频聊天
- (void)voiceChatWithUserId:(NSString *)userID;


- (void)postCallDialingToUserId:(NSString *)toUserId type:(NSString *)type;

- (void)postCallActiveToUserId:(NSString *)toUserId type:(NSString *)type sec:(NSInteger)sec;

//关注某人
-(void)attentionUserWith:(NSString *)user_id andIsFavorite:(BOOL )isFavorite;

//显示presentView
- (void)showPresentViewWithTargetId:(NSString *)targetId type:(NSString *)type;
- (void)showPresentViewWithTargetId:(NSString *)targetId;

//发送魔法
@property (nonatomic,copy)NSString *presentToId;

-(void)clickedSendMagicButtonWithSPDMagicModel:(SPDMagicModel * )SPDMagicModel;



-(UIImage *)takeScreenShot;//基于window 截屏
-(NSString *) genSignature:(NSDictionary *)params andSecretKey:(NSString *) secretkey;

-(void)compressVideoWithUrl:(NSURL *)movieUrl andParameters:(NSDictionary *)dict;

//message
-(void)showToast:(NSString *)str;

//礼物
//检查余额够不够
-(void)checkBalanceWithModel:(GiftListModel *)model wihtToUser_id:(NSString *)user_id;

//压缩图片到指定大小
-(UIImage *)scaleImage:(UIImage *)image toKb:(NSInteger)kb;

-(void)presentCommonController :(NSString *)title
                  messageString:(NSString *)message
                    cancelTitle:(NSString * _Nullable)cancelTitle
                   cancelAction:(_Nullable ActionHandler)cancelAction
                   confirmTitle:(NSString *)confirmTitle
                  confirmAction:(ActionHandler)confirmAction;

-(void)presentAutoController :(NSString *)title
                   titleColor:(NSString *)titleColor
                titleFontSize:(CGFloat)titleFontSize
                messageString:(NSString *)message
                 messageColor:(NSString *)messageColor
              messageFontSize:(CGFloat)messageFontSize
                  cancelTitle:(NSString * _Nullable)cancelTitle
                 cancelAction:(_Nullable ActionHandler)cancelAction
                 confirmTitle:(NSString *)confirmTitle
                confirmAction:(ActionHandler)confirmAction;

- (int)requestClanInviteCountAPI ;

//加载一个webview
- (void)loadWebViewController:(UIViewController *)vc title:(NSString *)title url:(NSString *)url;


@end

NS_ASSUME_NONNULL_END
