//
//  GiftListModel.m
//  SimpleDate
//
//  Created by 程龙军 on 16/2/1.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "GiftListModel.h"

@implementation GiftListModel


-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
 
    if ([key isEqualToString:@"description"]) {
        self.desc=value;
    }
}

@end
