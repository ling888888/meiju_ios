//
//  CommonModel.h
//  SimpleDate
//
//  Created by 程龙军 on 15/12/7.
//  Copyright © 2015年 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CommonModel : NSObject

+(id)initWithDictionary:(NSDictionary *)dic;

@end
