//
//  CommonModel.m
//  SimpleDate
//
//  Created by 程龙军 on 15/12/7.
//  Copyright © 2015年 WYB. All rights reserved.
//

#import "CommonModel.h"

@implementation CommonModel

- (id)initWithDict:(NSDictionary *)dic
{
    if (self=[super init]) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

// kvc容错
- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
}

+(id)initWithDictionary:(NSDictionary *)dic
{
    return [[self alloc] initWithDict:dic];
}


@end
