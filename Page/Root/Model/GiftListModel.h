//
//  GiftListModel.h
//  SimpleDate
//
//  Created by 程龙军 on 16/2/1.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "CommonModel.h"

@interface GiftListModel : CommonModel
@property(nonatomic, copy) NSString* _id;
@property(nonatomic, copy) NSNumber* cost;
@property(nonatomic, copy) NSString* cover;
@property(nonatomic, copy) NSString* desc;
@property(nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSString *popdesc;// topbar描述

//聊天送礼物的描述
@property(nonatomic, copy) NSNumber* charm;

@property(nonatomic,assign) NSNumber *count;

@property(nonatomic,copy)  NSString  *position; //礼物特效显示的位置

@property(nonatomic,assign) BOOL   send_msg_world; //礼物特效之后是否发送世界消息

@property(nonatomic,assign) BOOL  send_all_chatroom; //通知全世界

@property (nonatomic, copy) NSString *num;
@property (nonatomic, copy) NSString *limit_value;
@property (nonatomic, copy) NSString *currencyType;
@property (nonatomic, copy) NSString *rewardType;

@end
