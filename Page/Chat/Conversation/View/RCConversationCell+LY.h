//
//  RCConversationCell+LY.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/17.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <RongIMKit/RongIMKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RCConversationCell (LY)

- (void)showLivingTag;

- (void)hiddenLivingTag;

@end

NS_ASSUME_NONNULL_END
