//
//  PresentAnimView.h
//  RongCallKit
//
//  Created by 侯玲 on 16/11/21.
//  Copyright © 2016年 Rong Cloud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShakeLabel.h"

typedef void(^completeBlock)(BOOL finished,NSInteger finishCount);

@interface PresentAnimView : UIView

//@property (nonatomic,strong) GiftModel *model;
@property (nonatomic,strong) UIImageView *headImageView; // 头像
@property (nonatomic,strong) UIImageView *giftImageView; // 礼物
@property (nonatomic,strong) UILabel *nameLabel; // 送礼物者
@property (nonatomic,strong) UILabel *giftLabel; // 礼物名称
@property (nonatomic,assign) NSInteger giftCount; // 礼物个数

@property (nonatomic,strong) ShakeLabel *skLabel;
@property (nonatomic,assign) NSInteger animCount; // 动画执行到了第几次
@property (nonatomic,assign) CGRect originFrame; // 记录原始坐标

- (void)shakeNumberLabel;
-(void)configureDataWithGiftName:(NSString *)giftName giftImageName:(NSString *)giftImageName avatar:(NSString *)avatar nick_name:(NSString *)nick_name num:(NSString *)num;
-(void)startAnimationViewAction;
-(void)stopAnimationViewAction;
@end
