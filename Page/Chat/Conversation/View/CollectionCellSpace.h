//
//  CollectionCellSpace.h
//  SimpleDate
//
//  Created by Monkey on 2017/7/26.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionCellSpace : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *moreLabel;

@end
