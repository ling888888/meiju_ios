//
//  SPDPresentSelectCell.h
//  SimpleDate
//
//  Created by Monkey on 2018/11/26.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPDPresentSelectCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
