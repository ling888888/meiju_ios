//
//  SPDTopBarView.m
//  SimpleDate
//
//  Created by Monkey on 2017/11/14.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDTopBarView.h"

#define Top_Bar_beforeY -72

#define Top_Bar_Y 28
#define Top_Bar_X 10

@interface SPDTopBarView()

@property (nonatomic,assign)BOOL animating;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *msgContentLabel;
@property(nonatomic,strong)NSMutableArray * msgArray;
@end

@implementation SPDTopBarView

- (NSMutableArray *)msgArray{
    if (!_msgArray) {
        _msgArray = [NSMutableArray array];
    }
    return _msgArray;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius = 13;
    self.clipsToBounds = YES;
    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.85];
    self.avatar.layer.cornerRadius = 21;
    self.avatar.clipsToBounds= YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    [self addGestureRecognizer:tap];
    
    UISwipeGestureRecognizer * swipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(animatingStop)];
    swipe.direction = UISwipeGestureRecognizerDirectionUp;
    
    [self addGestureRecognizer:swipe];
}

- (void)handleTap:(UITapGestureRecognizer*)gesture {
    NSLog(@"handleTap");
    if (self.msg.senderUserInfo) {
        if (self.delegate  && [self.delegate respondsToSelector:@selector(clickedTopBarView:)]) {
            [self.delegate clickedTopBarView:self.msg.senderUserInfo];
        }
    }
    
}

+ (id)sharedInstance {
    static SPDTopBarView * view ;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        view = [[[NSBundle mainBundle] loadNibNamed:@"SPDTopBarView" owner:self options:nil]lastObject];
        view.frame = CGRectMake(Top_Bar_X, Top_Bar_beforeY, kScreenW-20, 72);
        UIWindow * window = [UIApplication sharedApplication].keyWindow;
        [window addSubview:view];
    });
    return view;
}

// 来了下一个 如果当前的正在展示那么直接替换 没有的话再坐动画
- (void)setMsg:(RCTextMessage *)msg{
    _msg = msg;
    //給值
    [self.superview bringSubviewToFront:self];
    if (msg.senderUserInfo) {
        [self.avatar sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:msg.senderUserInfo.portraitUri]];
        self.nameLabel.text = msg.senderUserInfo.name;
        self.msgContentLabel.text = msg.content;
        if (!self.animating) {
            [self animatingStart];
        }else{
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(animatingStop) object:nil];
            [self performSelector:@selector(animatingStop) withObject:nil afterDelay:3];
        }
    }
    
    
}

- (void)animatingStart {
    
    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.animating = YES;
        CGRect yframe = self.frame;
        yframe.origin.y = Top_Bar_Y;
        self.frame = yframe;
    }completion:^(BOOL finished) {
        [self performSelector:@selector(animatingStop) withObject:nil afterDelay:3];
    }];
}

- (void)animatingStop{
    [UIView animateWithDuration:0.5 animations:^{
        self.animating = NO;
        CGRect yframe = self.frame;
        yframe.origin.y = Top_Bar_beforeY;
        self.frame = yframe;
    }];
}

@end
