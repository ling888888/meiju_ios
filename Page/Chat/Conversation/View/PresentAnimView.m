//
//  PresentAnimView.m
//  RongCallKit
//
//  Created by 侯玲 on 16/11/21.
//  Copyright © 2016年 Rong Cloud. All rights reserved.
//

#import "PresentAnimView.h"

@interface PresentAnimView ()
@property (nonatomic,strong) UIImageView *bgImageView;
@property (nonatomic,strong) NSTimer *timer;
@end


@implementation PresentAnimView

// 重置
- (void)reset {
    
    self.frame = CGRectMake(-1, 250, 205, 40);
    self.alpha = 1;
    self.animCount = 0;
    self.skLabel.text = @"";
}

-(void)startAnimationViewAction{
    
    NSLog(@"START！！");
    NSLog(@"%@",NSStringFromCGRect(self.frame));
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        self.frame=CGRectMake(20, 250, 205, 50);
        NSLog(@"AnimationView----%@",NSStringFromCGRect(self.frame));

    } completion:^(BOOL finished) {
    }];

}

-(void)stopAnimationViewAction{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            
            CGRect fram=self.frame;
            fram.origin.x=-205-60;
            self.frame=fram;
            
        } completion:^(BOOL finished) {
            
        }];
        
    });
}


- (instancetype)init {
    if (self = [super init]) {
        
        self.frame=CGRectMake(-205-10, 250, 205, 40);
        [self setUI];
        
    }
    return self;
}

#pragma mark 布局 UI
- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    _headImageView.frame = CGRectMake(0, 0, self.frame.size.height, self.frame.size.height);
    _headImageView.layer.borderWidth = 1;
    _headImageView.layer.borderColor = [([UIColor colorWithHexString:COMMON_PINK])CGColor];
    _headImageView.layer.cornerRadius = _headImageView.frame.size.height / 2;
    _headImageView.layer.masksToBounds = YES;
    
    _giftImageView.frame = CGRectMake(self.frame.size.width - 50, self.frame.size.height - 50-10, 60, 60);
    _giftImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    _nameLabel.frame = CGRectMake(_headImageView.frame.size.width + 5, 5, _headImageView.frame.size.width * 3, 10);
    
    _giftLabel.frame = CGRectMake(_nameLabel.frame.origin.x, CGRectGetMaxY(_headImageView.frame) - 10 - 5, _nameLabel.frame.size.width, 10);
    
    _bgImageView.frame = self.bounds;
    _bgImageView.layer.cornerRadius = self.frame.size.height / 2;
    _bgImageView.layer.masksToBounds = YES;
    
    _skLabel.frame = CGRectMake(205 + 5, -20, 100, 30);
    
}

#pragma mark 初始化 UI
- (void)setUI {
    
    _bgImageView = [[UIImageView alloc] init];
    _bgImageView.backgroundColor = [UIColor blackColor];
    _bgImageView.alpha = 0.3;
    _headImageView = [[UIImageView alloc] init];
    _giftImageView = [[UIImageView alloc] init];
    _nameLabel = [[UILabel alloc] init];
    _giftLabel = [[UILabel alloc] init];
    _nameLabel.textColor  = [UIColor whiteColor];
    _nameLabel.font = [UIFont systemFontOfSize:15];
    _nameLabel.textAlignment = NSTextAlignmentLeft;
    _giftLabel.textColor  = [UIColor yellowColor];
    _giftLabel.font = [UIFont systemFontOfSize:15];
    _giftLabel.textAlignment = NSTextAlignmentLeft;
    
     //初始化动画label
    _skLabel =  [[ShakeLabel alloc] init];
    _skLabel.font = [UIFont systemFontOfSize:25];
    _skLabel.borderColor = [UIColor colorWithHexString:COMMON_PINK];
    _skLabel.textColor = [UIColor colorWithHexString:@"F0B23A"];
    _skLabel.textAlignment = NSTextAlignmentLeft;
    
    [self addSubview:_bgImageView];
    [self addSubview:_headImageView];
    [self addSubview:_giftImageView];
    [self addSubview:_nameLabel];
    [self addSubview:_giftLabel];
    [self addSubview:_skLabel];
    
}

-(void)configureDataWithGiftName:(NSString *)giftName giftImageName:(NSString *)giftImageName avatar:(NSString *)avatar nick_name:(NSString *)nick_name num:(NSString *)num {
    
    _nameLabel.text=nick_name;
    _giftLabel.text=[NSString stringWithFormat:@"%@%@", SPDStringWithKey(@"送您一个", nil), giftName];
    [_giftImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:giftImageName]];
    [_headImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:avatar]];
    self.skLabel.text = [NSString stringWithFormat:@"x%@", num];
}


@end
