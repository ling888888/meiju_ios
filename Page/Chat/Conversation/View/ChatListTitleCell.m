//
//  ChatListTitleCell.m
//  SimpleDate
//
//  Created by ling Hou on 2019/12/10.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "ChatListTitleCell.h"

@interface ChatListTitleCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;


@end

@implementation ChatListTitleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setDict:(NSDictionary *)dict {
    _dict = dict;
    self.coverImageView.image = [UIImage imageNamed:_dict[@"image"]];
    self.titleLabel.text = SPDLocalizedString(dict[@"title"]);
}

- (void)updateUnreadCount {
    NSString * userId = self.dict[@"userId"];
    int count = [[RCIMClient sharedRCIMClient] getUnreadCount:ConversationType_PRIVATE targetId:userId];
    self.unreadLabel.hidden = (count == 0);
    if (count !=0) {
        self.unreadLabel.text = [NSString stringWithFormat:@"%d",count];
    }
}

@end
