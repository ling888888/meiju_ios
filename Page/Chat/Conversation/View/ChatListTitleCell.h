//
//  ChatListTitleCell.h
//  SimpleDate
//
//  Created by ling Hou on 2019/12/10.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatListTitleCell : UICollectionViewCell

@property (nonatomic, copy) NSDictionary * dict;

@property (weak, nonatomic) IBOutlet UILabel *unreadLabel;

- (void)updateUnreadCount;

@end

NS_ASSUME_NONNULL_END
