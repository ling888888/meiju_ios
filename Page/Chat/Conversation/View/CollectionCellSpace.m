//
//  CollectionCellSpace.m
//  SimpleDate
//
//  Created by Monkey on 2017/7/26.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "CollectionCellSpace.h"

@interface CollectionCellSpace ()


@end

@implementation CollectionCellSpace

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.userInteractionEnabled = NO;
    self.moreLabel.text = @"المراكب الاكثر \n انتظروا";
}

@end
