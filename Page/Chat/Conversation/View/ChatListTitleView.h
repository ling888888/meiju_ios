//
//  ChatListTitleView.h
//  SimpleDate
//
//  Created by ling Hou on 2019/12/10.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class ChatListTitleView;

@protocol ChatListTitleViewDelegate <NSObject>

- (void)chatListTitleView:(ChatListTitleView *)titleView andSelectedItemIndex:(NSInteger )index;

@end

@interface ChatListTitleView : UIView

@property (nonatomic, weak)id<ChatListTitleViewDelegate> delegate;
@property (nonatomic, copy) NSArray * dataArray;
@property(nonatomic, copy) NSString *entryType;

- (void)updateUnreadMessage;

@end

NS_ASSUME_NONNULL_END
