//
//  GiftView.m
//  TsetGift
//
//  Created by 侯玲 on 16/9/18.
//  Copyright © 2016年 侯玲. All rights reserved.
//

#import "PresentView.h"
#import "GiftCVCell.h"
#import "GiftListModel.h"
#import "VehicleCVCell.h"
#import "CollectionCellSpace.h"
#import "MagicCVCell.h"
#import "SPDPresentSelectCell.h"

@interface PresentView ()<UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet UIView *selfView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *giftView;
@property (weak, nonatomic) IBOutlet UICollectionView *selectCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIButton *selectNumBtn;
@property (weak, nonatomic) IBOutlet UIView *selectNumView;
@property (weak, nonatomic) IBOutlet UITableView *numTableView;
@property (weak, nonatomic) IBOutlet UIImageView *currencyImageView;
@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;
@property (weak, nonatomic) IBOutlet UIButton *rechargeBtn;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;

@property (nonatomic, copy) NSNumber *index;
@property (nonatomic, assign) int vehicle_spaceCount;
@property (nonatomic, assign) int magic_spaceCount;
@property (nonatomic, copy) NSArray *numArray;
@property (nonatomic, copy) NSString *num;
@property (nonatomic, strong) NSNumber *goldBalance;
@property (nonatomic, strong) NSNumber *diamondBalance;
@property (nonatomic, strong) NSMutableArray *selectTitleArray;

@end

@implementation PresentView

+ (instancetype)presentView {
    return [[self alloc] initWithFrame:[UIScreen mainScreen].bounds];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self loadSelfView];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self loadSelfView];
    }
    return self;
}

- (void)loadSelfView {
    [[NSBundle mainBundle] loadNibNamed:@"PresentView" owner:self options:nil];
    [self addSubview:self.selfView];
    
    UITapGestureRecognizer *tapTopView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
    [self.topView addGestureRecognizer:tapTopView];
    
    self.giftView.frame = CGRectMake(0, kScreenH, kScreenW, 295);
    [self.selectCollectionView registerNib:[UINib nibWithNibName:@"SPDPresentSelectCell" bundle:nil] forCellWithReuseIdentifier:@"SPDPresentSelectCell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"CollectionCellSpace" bundle:nil] forCellWithReuseIdentifier:@"CollectionCellSpace"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"GiftCVCell" bundle:nil] forCellWithReuseIdentifier:@"GiftCVCell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"VehicleCVCell" bundle:nil] forCellWithReuseIdentifier:@"VehicleCVCell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"MagicCVCell" bundle:nil] forCellWithReuseIdentifier:@"MagicCVCell"];
    
    [self.rechargeBtn setTitle:SPDStringWithKey(@"充值", nil) forState:UIControlStateNormal];
    self.arrowImageView.image = [UIImage imageNamed:@"gift_recharge_arrow"].adaptiveRtl;
    UITapGestureRecognizer *tapSelectNumView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapSelectNumView:)];
    tapSelectNumView.delegate = self;
    [self.selectNumView addGestureRecognizer:tapSelectNumView];
    [self.sendBtn setTitle:SPDStringWithKey(@"赠送", nil) forState:UIControlStateNormal];
}

- (void)setDataArray:(NSMutableArray *)dataArray {
    while (dataArray.count % 8 != 0) {
        [dataArray addObject:@"goldGift"];
    }
    _dataArray = dataArray;
}

- (void)setVehicleArray:(NSMutableArray *)vehicleArray {
    self.vehicle_spaceCount = 0;
    while (vehicleArray.count % 8 != 0) {
        self.vehicle_spaceCount++;
        [vehicleArray addObject:@"vehicle"];
    }
    _vehicleArray = vehicleArray;
}

- (void)setMagicArray:(NSMutableArray *)magicArray {
    self.magic_spaceCount = 0;
    while (magicArray.count % 8 != 0) {
        self.magic_spaceCount++;
        [magicArray addObject:@"magic"];
    }
    _magicArray = magicArray;
}

- (void)setNobleGiftArray:(NSMutableArray *)nobleGiftArray {
    while (nobleGiftArray.count % 8 != 0) {
        [nobleGiftArray addObject:@"nobleGift"];
    }
    _nobleGiftArray = nobleGiftArray;
}

- (void)setDiamondGiftArray:(NSMutableArray *)diamondGiftArray {
    while (diamondGiftArray.count % 8 != 0) {
        [diamondGiftArray addObject:@"diamondGift"];
    }
    _diamondGiftArray = diamondGiftArray;
}

- (void)show {
    [self requestMyBalance];

    self.selectedIndex = self.selectedIndex;
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.giftView.frame = CGRectMake(0, kScreenH - 295, kScreenW, 295);
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismiss {
    self.index = nil;
    self.selectedIndex = SPDSelectedGift;
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.giftView.frame = CGRectMake(0, kScreenH, kScreenW, 295);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(presentViewAlreadyHidden)]) {
        [self.delegate presentViewAlreadyHidden];
    }
}

- (void)layoutSubviews {
    self.selfView.frame = self.bounds;
}

- (void)setSelectedIndex:(PresentViewSelectIndex)selectedIndex {
    _selectedIndex = selectedIndex;
    
    switch (_selectedIndex) {
        case SPDSelectedGift:
            self.pageControl.numberOfPages = self.dataArray.count == 0 ? : (self.dataArray.count - 1) / 8 + 1;
            self.pageControl.currentPage = 0;
            self.selectNumBtn.hidden = NO;
            self.num = @"1";
            [self.selectNumBtn setTitle:self.num forState:UIControlStateNormal];
            
            self.currencyImageView.image = [UIImage imageNamed:@"ic_invitation_gold"];
            self.balanceLabel.textColor = [UIColor colorWithHexString:@"F7BD1F"];
            self.balanceLabel.text = self.goldBalance.stringValue;
            self.rechargeBtn.hidden = NO;
            self.arrowImageView.hidden = NO;
            break;
        case SPDSelectedDiamondGift:
            self.pageControl.numberOfPages = self.diamondGiftArray.count == 0 ? : (self.diamondGiftArray.count - 1) / 8 + 1;
            self.pageControl.currentPage = 0;
            self.selectNumBtn.hidden = NO;
            self.num = @"1";
            [self.selectNumBtn setTitle:self.num forState:UIControlStateNormal];
            
            self.currencyImageView.image = [UIImage imageNamed:@"ic_diamond"];
            self.balanceLabel.textColor = [UIColor colorWithHexString:@"50B3FF"];
            self.balanceLabel.text = self.diamondBalance.stringValue;
            self.rechargeBtn.hidden = YES;
            self.arrowImageView.hidden = YES;
            break;
        case SPDSelectedVehicle:
            self.pageControl.numberOfPages = self.vehicleArray.count == 0 ? : (self.vehicleArray.count - 1) / 8 + 1;
            self.pageControl.currentPage = 0;
            self.selectNumBtn.hidden = YES;
            
            self.currencyImageView.image = [UIImage imageNamed:@"ic_invitation_gold"];
            self.balanceLabel.textColor = [UIColor colorWithHexString:@"F7BD1F"];
            self.balanceLabel.text = self.goldBalance.stringValue;
            self.rechargeBtn.hidden = NO;
            self.arrowImageView.hidden = NO;
            break;
        case SPDSelectedMagic:
            self.pageControl.numberOfPages = self.magicArray.count == 0 ? : (self.magicArray.count - 1) / 8 + 1;
            self.pageControl.currentPage = 0;
            self.selectNumBtn.hidden = YES;
            
            self.currencyImageView.image = [UIImage imageNamed:@"ic_invitation_gold"];
            self.balanceLabel.textColor = [UIColor colorWithHexString:@"F7BD1F"];
            self.balanceLabel.text = self.goldBalance.stringValue;
            self.rechargeBtn.hidden = NO;
            self.arrowImageView.hidden = NO;
            break;
        case SPDSelectedNobleGift:
            self.pageControl.numberOfPages = self.nobleGiftArray.count == 0 ? : (self.nobleGiftArray.count - 1) / 8 + 1;
            self.pageControl.currentPage = 0;
            self.selectNumBtn.hidden = NO;
            self.num = @"1";
            [self.selectNumBtn setTitle:self.num forState:UIControlStateNormal];
            
            self.currencyImageView.image = [UIImage imageNamed:@"ic_invitation_gold"];
            self.balanceLabel.textColor = [UIColor colorWithHexString:@"F7BD1F"];
            self.balanceLabel.text = self.goldBalance.stringValue;
            self.rechargeBtn.hidden = NO;
            self.arrowImageView.hidden = NO;
            break;
        default:
            break;
    }

    self.index = nil;
    [self.selectCollectionView reloadData];
    [self.collectionView reloadData];
    if ([SPDCommonTool currentVersionIsArbic]) {
        [self.collectionView setContentOffset:CGPointMake(kScreenW * (self.pageControl.numberOfPages - 1), 0)];
    } else {
        [self.collectionView setContentOffset:CGPointZero];
    }
}

- (IBAction)sendButtonClicked:(id)sender {
    if (self.index) {
        switch (self.selectedIndex) {
            case SPDSelectedGift:{
                if (self.delegate && [self.delegate respondsToSelector:@selector(clickedSendPresentButtonWithModel:num:)]) {
                    [self.delegate clickedSendPresentButtonWithModel:self.dataArray[self.index.integerValue] num:self.num];
                }
                break;
            }
            case SPDSelectedVehicle:{
                if (self.delegate && [self.delegate respondsToSelector:@selector(clickedSendVehicleButtonWithVehicleModel:)]) {
                    SPDVehicleModel * model = self.vehicleArray[[self.index integerValue]];
                    [self.delegate clickedSendVehicleButtonWithVehicleModel:model];
                    [self dismiss];
                }
                break;
            }
            case SPDSelectedMagic:{
                if ([[SPDApiUser currentUser].lang isEqualToString:self.lang]) {
                    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedSendMagicButtonWithSPDMagicModel:)]) {
                        SPDMagicModel * model = self.magicArray[[self.index integerValue]];
                        [self.delegate clickedSendMagicButtonWithSPDMagicModel:model];
                        [self dismiss];
                    }
                } else {
                    [SPDCommonTool showWindowToast:SPDStringWithKey(@"对方和你不在同一语言区，不能跨区使用魔法", nil)];
                }
                break;
            }
            case SPDSelectedNobleGift: {
                if (self.delegate && [self.delegate respondsToSelector:@selector(clickedSendPresentButtonWithModel:num:)]) {
                    [self.delegate clickedSendPresentButtonWithModel:self.nobleGiftArray[self.index.integerValue] num:self.num];
                }
                break;
            }
            case SPDSelectedDiamondGift: {
                if (self.delegate && [self.delegate respondsToSelector:@selector(clickedSendPresentButtonWithModel:num:)]) {
                    [self.delegate clickedSendPresentButtonWithModel:self.diamondGiftArray[self.index.integerValue] num:self.num];
                }
                break;
            }
            default:
                break;
        }
    }
}

- (IBAction)billRechargeButtonClicked:(id)sender {
    if ([self.delegate respondsToSelector:@selector(clickedBillRechargeButton)]) {
        [self dismiss];
        [self.delegate clickedBillRechargeButton];
    }
}

- (IBAction)clickSelectNumBtn:(UIButton *)sender {
    self.selectNumView.hidden = NO;
}

- (void)tapSelectNumView:(UITapGestureRecognizer *)sender {
    self.selectNumView.hidden = YES;
}

- (void)requestMyBalance {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.balance" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        self.goldBalance = suceess[@"balance"];
        self.diamondBalance = suceess[@"jewelBalance"];
        self.balanceLabel.text = self.selectedIndex == SPDSelectedDiamondGift ? self.diamondBalance.stringValue : self.goldBalance.stringValue;
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if (collectionView == self.selectCollectionView) {
        return self.selectTitleArray.count;
    } else {
        return 1;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == self.selectCollectionView) {
        return 1;
    } else {
        switch (self.selectedIndex) {
            case SPDSelectedGift:
                return self.dataArray.count;
                break;
            case SPDSelectedDiamondGift:
                return self.diamondGiftArray.count;
                break;
            case SPDSelectedVehicle:
                return self.vehicleArray.count;
                break;
            case SPDSelectedMagic:
                return self.magicArray.count;
                break;
            case SPDSelectedNobleGift:
                return self.nobleGiftArray.count;
                break;
            default:
                return 0;
                break;
        }
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.selectCollectionView) {
        SPDPresentSelectCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SPDPresentSelectCell" forIndexPath:indexPath];
        cell.titleLabel.text = SPDStringWithKey(self.selectTitleArray[indexPath.section], nil);
        cell.titleLabel.textColor = [UIColor colorWithHexString:self.selectedIndex == indexPath.section ? COMMON_PINK :@"CCCCCC"];
        return cell;
    } else  {
        if (self.selectedIndex == SPDSelectedGift) {
            id goldGift = self.dataArray[indexPath.row];
            if ([goldGift isKindOfClass:[GiftListModel class]]) {
                GiftCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([GiftCVCell class]) forIndexPath:indexPath];
                cell.model = self.dataArray[indexPath.row];
                cell.lightImg.hidden = YES;
                if (self.index != nil && [self.index integerValue] == indexPath.row) {
                    cell.lightImg.hidden = NO;
                }
                return cell;
            } else {
                CollectionCellSpace *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([CollectionCellSpace class]) forIndexPath:indexPath];
                cell.moreLabel.hidden = YES;
                return cell;
            }
        }else if (self.selectedIndex == SPDSelectedVehicle) {
            id choose = self.vehicleArray [indexPath.row];
            if ([choose isKindOfClass:[SPDVehicleModel class]]) {
                VehicleCVCell * cell = [_collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([VehicleCVCell class]) forIndexPath:indexPath];
                SPDVehicleModel *model = self.vehicleArray[indexPath.row];
                cell.lightImg.hidden = YES;
                cell.vehicleModel = model;
                if (self.index != nil&& [self.index integerValue] == indexPath.row) {
                    cell.lightImg.hidden = NO;
                }
                return cell;
            }else {
                if (indexPath.row == self.vehicleArray.count- self.vehicle_spaceCount) {
                    CollectionCellSpace * cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([CollectionCellSpace class]) forIndexPath:indexPath];
                    cell.moreLabel.text = SPDStringWithKey(@"更多座驾\n敬请期待", nil);
                    cell.moreLabel.hidden = NO;
                    return cell;
                    
                }else{
                    CollectionCellSpace * cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([CollectionCellSpace class]) forIndexPath:indexPath];
                    cell.moreLabel.hidden = YES;
                    return cell;
                }
                
            }
        }else if (self.selectedIndex == SPDSelectedMagic) {
            id choose = self.magicArray [indexPath.row];
            if ([choose isKindOfClass:[SPDMagicModel class]]) {
                MagicCVCell * cell = [_collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([MagicCVCell class]) forIndexPath:indexPath];
                SPDMagicModel *model = self.magicArray[indexPath.row];
                cell.lightImg.hidden = YES;
                cell.magicModel = model;
                if (self.index != nil&& [self.index integerValue] == indexPath.row) {
                    cell.lightImg.hidden = NO;
                }
                return cell;
            }else {
                if (indexPath.row == self.magicArray.count- self.magic_spaceCount) {
                    CollectionCellSpace * cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([CollectionCellSpace class]) forIndexPath:indexPath];
                    cell.moreLabel.text = SPDStringWithKey(@"更多魔法\n敬请期待", nil);
                    cell.moreLabel.hidden = NO;
                    return cell;
                    
                }else{
                    CollectionCellSpace * cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([CollectionCellSpace class]) forIndexPath:indexPath];
                    cell.moreLabel.hidden = YES;
                    return cell;
                }
                
            }
        } else if (self.selectedIndex == SPDSelectedNobleGift) {
            id nobleGift = self.nobleGiftArray[indexPath.row];
            if ([nobleGift isKindOfClass:[GiftListModel class]]) {
                GiftCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([GiftCVCell class]) forIndexPath:indexPath];
                cell.model = self.nobleGiftArray[indexPath.row];
                cell.lightImg.hidden = YES;
                if (self.index != nil && [self.index integerValue] == indexPath.row) {
                    cell.lightImg.hidden = NO;
                }
                return cell;
            } else {
                CollectionCellSpace *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([CollectionCellSpace class]) forIndexPath:indexPath];
                cell.moreLabel.hidden = YES;
                return cell;
            }
        } else if (self.selectedIndex == SPDSelectedDiamondGift) {
            id diamondGift = self.diamondGiftArray[indexPath.row];
            if ([diamondGift isKindOfClass:[GiftListModel class]]) {
                GiftCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([GiftCVCell class]) forIndexPath:indexPath];
                cell.model = self.diamondGiftArray[indexPath.row];
                cell.lightImg.hidden = YES;
                if (self.index != nil && [self.index integerValue] == indexPath.row) {
                    cell.lightImg.hidden = NO;
                }
                return cell;
            } else {
                CollectionCellSpace *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([CollectionCellSpace class]) forIndexPath:indexPath];
                cell.moreLabel.hidden = YES;
                return cell;
            }
        } else {
            return [[UICollectionViewCell alloc]init];
        }
    }
}

#pragma mark - UICollectionViewDataSource

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath*)indexPath {
    if (collectionView == self.selectCollectionView) {
        CGFloat width = [SPDStringWithKey(self.selectTitleArray[indexPath.section], nil) boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 45)
                                                                                                      options:NSStringDrawingUsesLineFragmentOrigin
                                                                                                   attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15]}
                                                                                                      context:nil].size.width;
        return CGSizeMake(ceilf(width) + 25, 45);
    } else {
        return CGSizeMake(self.selfView.bounds.size.width / 4, 107);
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.selectCollectionView) {
        self.selectedIndex = indexPath.section;
        [self.selectCollectionView reloadData];
    } else {
        if ((self.selectedIndex == SPDSelectedGift || self.selectedIndex == SPDSelectedNobleGift || self.selectedIndex == SPDSelectedDiamondGift) && self.index.integerValue != indexPath.row) {
            self.num = @"1";
            [self.selectNumBtn setTitle:self.num forState:UIControlStateNormal];
        }
        self.index = @(indexPath.row);
        [self.collectionView reloadData];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    if (scrollView == self.collectionView) {
        if ([SPDCommonTool currentVersionIsArbic]) {
            self.pageControl.currentPage = self.pageControl.numberOfPages - 1 - targetContentOffset->x / kScreenW;
        } else {
            self.pageControl.currentPage = targetContentOffset->x / kScreenW;
        }
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.numArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"NumCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.textLabel.textColor = [UIColor colorWithHexString:@"000000"];
        cell.separatorInset = UIEdgeInsetsMake(0, 9, 0, 9);
    }
    cell.textLabel.text = self.numArray[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.num = self.numArray[indexPath.row];
    [self.selectNumBtn setTitle:self.num forState:UIControlStateNormal];
    self.selectNumView.hidden = YES;
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ![touch.view isDescendantOfView:self.numTableView];
}

#pragma mark - getters

- (NSMutableArray *)selectTitleArray {
    if(!_selectTitleArray){
        _selectTitleArray = [NSMutableArray arrayWithArray:@[@"金币礼物", @"钻石礼物", @"座驾", @"魔法", @"贵族"]];
    }
    return _selectTitleArray;
}

- (NSArray *)numArray {
    if (!_numArray) {
        _numArray = @[@"1", @"7", @"17", @"77", @"177", @"777"];
    }
    return _numArray;
}

@end
