//
//  PresentMessageCell.h
//  SimpleDate
//
//  Created by 侯玲 on 16/9/20.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import <RongIMKit/RongIMKit.h>
#import "PresentMessage.h"
#import "UIButton+HHImagePosition.h"
@interface PresentMessageCell : RCMessageCell

/**
 * 消息显示Label
 */
//@property(strong, nonatomic) RCAttributedLabel *textLabel;

/**
 * 消息背景
 */
@property(nonatomic, strong) UIImageView *bubbleBackgroundView;

/**
 
 *选择礼物的index;
 
 */
@property(nonatomic,assign)NSInteger index;
/**
 * 设置消息数据模型
 *
 * @param model 消息数据模型
 */
- (void)setDataModel:(RCMessageModel *)model;


@end
