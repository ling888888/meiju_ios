//
//  MagicCVCell.m
//  SimpleDate
//
//  Created by Monkey on 2017/9/26.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "MagicCVCell.h"
#import "MASConstraint.h"

const CGFloat magicLevelInterval = 4.0f;
const CGFloat magicLevelWidth = 9.0f;
const CGFloat magicLevelHeight = 9.0f;

@interface MagicCVCell ()

@property (weak, nonatomic) IBOutlet UIImageView *magicImageView;
@property (weak, nonatomic) IBOutlet UILabel *magicPropLabel; //magic num label
@property (weak, nonatomic) IBOutlet UILabel *magicNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@end

@implementation MagicCVCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.magicPropLabel.layer.cornerRadius = 2.5;
    self.magicPropLabel.clipsToBounds = YES;
}

- (void)setMagicModel:(SPDMagicModel *)magicModel {
    _magicModel = magicModel;
    if (_magicModel._id.notEmpty) {
        self.hidden = NO;
        [self.magicImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,magicModel.cover]]];
        self.magicPropLabel.text = [NSString stringWithFormat:@"%@",magicModel.num];
        self.magicNameLabel.text = magicModel.name;
        [self handleMagicLevelWith: [magicModel.magic_level integerValue]];
    }else{
        self.hidden = YES;
    }
}

- (void)handleMagicLevelWith:(NSInteger )magicLevel {
    if (magicLevel == 0) {
        magicLevel = 1;
    }
    self.levelLabel.text = [NSString stringWithFormat:@"LV.%ld", magicLevel];
}

- (void)setCellSelected:(BOOL)cellSelected {
    _cellSelected = cellSelected;
    self.layer.borderWidth = _cellSelected ? 1.5 : 0;
    if (_cellSelected) {
        [self heartAnimation];
    }
}

- (void)heartAnimation{
    float bigSize = 1.1;
    CABasicAnimation *heart = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    heart.duration = 0.5;
    heart.toValue = [NSNumber numberWithFloat:bigSize];
    heart.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    heart.autoreverses = YES;
    heart.repeatCount = FLT_MAX;
    [self.magicImageView.layer addAnimation:heart forKey:@"transform.scale"];
}

@end
