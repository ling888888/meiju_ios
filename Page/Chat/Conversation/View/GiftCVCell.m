//
//  GiftCVCell.m
//  GiftTsetView
//
//  Created by 侯玲 on 16/9/18.
//  Copyright © 2016年 侯玲. All rights reserved.
//

#import "GiftCVCell.h"
#import "GiftListModel.h"

@interface GiftCVCell ()

@property (weak, nonatomic) IBOutlet UIImageView *giftImageView;
@property (weak, nonatomic) IBOutlet UIImageView *currencyImageView;
@property (weak, nonatomic) IBOutlet UILabel *goldLabel;
@property (weak, nonatomic) IBOutlet UILabel *charamLabel;
@property (weak, nonatomic) IBOutlet UIImageView *nobleMedalImageView;
@property (weak, nonatomic) IBOutlet UIImageView *flagImageView;

@end

@implementation GiftCVCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(GiftListModel *)model {
    _model = model;
    
    [self.giftImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _model.cover]]];
    self.goldLabel.text = [_model.cost stringValue];
    self.charamLabel.text = [_model.charm stringValue];
    if ([_model.currencyType isEqualToString:@"gold"]) {
        self.currencyImageView.image = [UIImage imageNamed:@"ic_invitation_gold"];
    } else if ([_model.currencyType isEqualToString:@"jewel"]) {
        self.currencyImageView.image = [UIImage imageNamed:@"ic_diamond"];
    }
    
    if ([SPDCommonTool isEmpty:_model.limit_value]) {
        self.nobleMedalImageView.hidden = YES;
    } else {
        self.nobleMedalImageView.hidden = NO;
        self.nobleMedalImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"medal_%@", _model.limit_value]];
    }

    if ([_model.rewardType isEqualToString:@"gold"]) {
        self.flagImageView.hidden = NO;
        self.flagImageView.image = [UIImage imageNamed:@"gift_flag_gold"];
    } else if ([_model.rewardType isEqualToString:@"lottery"]) {
        self.flagImageView.hidden = NO;
        self.flagImageView.image = [UIImage imageNamed:@"gift_flag_lucky"];
    } else {
        self.flagImageView.hidden = YES;
    }
}

@end
