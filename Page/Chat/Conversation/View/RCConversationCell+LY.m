//
//  RCConversationCell+LY.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/17.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RCConversationCell+LY.h"

@implementation RCConversationCell (LY)

static char livingKey;

- (UIButton *)livingBtn
{
    UIButton * __livingBtn = objc_getAssociatedObject(self, &livingKey);
    if (!__livingBtn) {
        
        CGFloat livingBtnX = (CGRectGetWidth(self.headerImageViewBackgroundView.frame) - 38) / 2;
        CGFloat livingBtnY = CGRectGetHeight(self.headerImageViewBackgroundView.frame) - 7.5;
        __livingBtn = [[UIButton alloc] initWithFrame:CGRectMake(livingBtnX, livingBtnY, 38, 15)];
        [__livingBtn setTitle:@"直播中".localized forState:UIControlStateNormal];
        [__livingBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        __livingBtn.titleLabel.font = [UIFont mediumFontOfSize:10];
        __livingBtn.layer.cornerRadius = 7.5;
        __livingBtn.layer.masksToBounds = true;
        UIImage * bgImg = [[UIImage alloc] initWithGradient:@[[UIColor colorWithHexString:@"#22BBB9"], [UIColor colorWithHexString:@"#45E994"]] size:__livingBtn.frame.size direction:UIImageGradientColorsDirectionHorizontal];
        [__livingBtn setBackgroundImage:bgImg forState:UIControlStateNormal];
        
        [self.headerImageViewBackgroundView addSubview:__livingBtn];
        objc_setAssociatedObject(self, &livingKey, __livingBtn, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return __livingBtn;
}

//- (void)setLivingBtn:(UIButton *)livingBtn
//{
//    objc_setAssociatedObject(self, &livingKey, livingBtn, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//}

- (void)showLivingTag {
    [self.livingBtn setHidden:false];
}

- (void)hiddenLivingTag {
    [self.livingBtn setHidden:true];
}

@end
