//
//  SPDPresentFlowLayout.h
//  SimpleDate
//
//  Created by 侯玲 on 16/10/31.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SPDPresentFlowLayoutDelegate <UICollectionViewDelegateFlowLayout>
- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout cellCenteredAtIndexPath:(NSIndexPath *)indexPath page:(int)page;
@end

@interface SPDPresentFlowLayout : UICollectionViewFlowLayout

@property (nonatomic, weak) id<SPDPresentFlowLayoutDelegate> delegate;


// 一行中 cell的个数
@property (nonatomic) IBInspectable NSUInteger cellCountPerRow;
// 一页显示多少行
@property (nonatomic) IBInspectable NSUInteger rowCount;

@end
