//
//  MagicCVCell.h
//  SimpleDate
//
//  Created by Monkey on 2017/9/26.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPDMagicModel.h"

@interface MagicCVCell : UICollectionViewCell

@property(nonatomic,strong)SPDMagicModel *magicModel;

@property (weak, nonatomic) IBOutlet UIImageView *lightImg;

@property (nonatomic, assign) BOOL cellSelected;

@end
