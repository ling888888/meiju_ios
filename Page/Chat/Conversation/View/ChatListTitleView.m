//
//  ChatListTitleView.m
//  SimpleDate
//
//  Created by ling Hou on 2019/12/10.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "ChatListTitleView.h"
#import "UIButton+HHImagePosition.h"
#import "ChatListTitleCell.h"

@interface ChatListTitleView ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView * collectionView;

@end

@implementation ChatListTitleView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
        self.backgroundColor = [UIColor clearColor];
        
        
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    if (![self.entryType isEqualToString:@"chatroom"]) {
        return;
    }
     
    NSDictionary *dic = @{@"type": @"to_me", @"page": @(0)};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"friend" bAnimated:NO bAnimatedViewController:self.VC bSuccessDoSomething:^(id  _Nullable suceess) {
        NSArray *list = suceess[@"list"];
        
        ChatListTitleCell *cell = (ChatListTitleCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
        
        NSInteger count = list.count;
        if (count > 0) {
            [cell.unreadLabel setHidden:false];
            cell.unreadLabel.text = [NSString stringWithFormat:@"%ld", count];
        } else {
            [cell.unreadLabel setHidden:true];
        }
    } bFailure:^(id  _Nullable failure) {

    }];
    
    
//    cell.unreadLabel
}

- (void)setupUI {
    [self addSubview:self.collectionView];
    [self.collectionView registerNib:[UINib nibWithNibName:@"ChatListTitleCell" bundle:nil] forCellWithReuseIdentifier:@"ChatListTitleCell"];
    [self.collectionView reloadData];
}

- (void)updateUnreadMessage {
    [self.collectionView reloadData];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 4;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath  {
    ChatListTitleCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatListTitleCell" forIndexPath:indexPath];
    cell.dict = self.dataArray[indexPath.row];
    [cell updateUnreadCount];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate && [self.delegate respondsToSelector:@selector(chatListTitleView:andSelectedItemIndex:)]) {
        [self.delegate chatListTitleView:self andSelectedItemIndex:indexPath.row];
    }
}


//- (NSArray *)dataArray {
//    if ( nil == _dataArray ) {
//        _dataArray = @[@{
//                           @"title": @"系统通知",
//                           @"image": @"system_msg",
//                           @"userId": @"1"
//                           },
//                       @{
//                           @"title": @"官方公告",
//                           @"image": @"official_msg",
//                           @"userId": @"2"
//                           },
//                       @{
//                           @"title": @"家族召唤",
//                           @"image": @"clancall_msg",
//                           @"userId": @"3"
//                           },
//                       @{
//                           @"title": @"联系客服",
//                           @"image": @"service_msg",
//                           @"userId": @"13"
//                           }];
//    }
//    return _dataArray;
//}

- (UICollectionView *)collectionView {
    if ( nil == _collectionView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.itemSize = CGSizeMake(kScreenW/4, self.bounds.size.height);
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        _collectionView = [[UICollectionView alloc]initWithFrame:self.bounds collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
    }
    return _collectionView;
}

@end
