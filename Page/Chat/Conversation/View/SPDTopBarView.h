//
//  SPDTopBarView.h
//  SimpleDate
//
//  Created by Monkey on 2017/11/14.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RongIMKit/RongIMKit.h>

@protocol SPDTopBarViewDelegate<NSObject>

- (void)clickedTopBarView:(RCUserInfo * )userinfo ;

@end

@interface SPDTopBarView : UIView

+ (id)sharedInstance;

@property(nonatomic,strong)RCTextMessage * msg ;

@property (nonatomic,weak)id<SPDTopBarViewDelegate> delegate;

- (void)animatingStop;

@end
