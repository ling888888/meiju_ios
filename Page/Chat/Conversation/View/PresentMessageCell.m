//
//  PresentMessageCell.m
//  SimpleDate
//
//  Created by 侯玲 on 16/9/20.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "PresentMessageCell.h"
#import "PresentMessage.h"


@interface PresentMessageCell ()
@property (strong, nonatomic) IBOutlet UIView *presentView;

@property (nonatomic,copy)UIImageView *presentImageView;//显示礼物图片
@property (nonatomic,strong)UILabel *goldNumLabel;//显示金币的label
@property (nonatomic,strong)UILabel *charamScoreLabel;//显示魅力值得label
@property (nonatomic,strong)UIButton * expireTimeBtn;// vehicle 过期时间
@property (nonatomic,strong)UILabel * titleLabel ; //送给你一个礼物希望你能喜欢
@end

@implementation PresentMessageCell


- (NSDictionary *)attributeDictionary {
    if (self.messageDirection == MessageDirection_SEND) {
        return @{
                 @(NSTextCheckingTypeLink) : @{NSForegroundColorAttributeName : [UIColor blueColor]},
                 @(NSTextCheckingTypePhoneNumber) : @{NSForegroundColorAttributeName : [UIColor blueColor]}
                 };
    } else {
        return @{
                 @(NSTextCheckingTypeLink) : @{NSForegroundColorAttributeName : [UIColor blueColor]},
                 @(NSTextCheckingTypePhoneNumber) : @{NSForegroundColorAttributeName : [UIColor orangeColor]}
                 };
    }
    return nil;
}

- (NSDictionary *)highlightedAttributeDictionary {
    return [self attributeDictionary];
}
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}
- (void)initialize {
    
    self.bubbleBackgroundView = [[UIImageView alloc] initWithFrame:CGRectZero];
    [self.messageContentView addSubview:self.bubbleBackgroundView];
    
    //添加礼物图片
    _presentImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 10, 95, 95)];
    _presentView.contentMode = UIViewContentModeScaleAspectFit;
    [self.bubbleBackgroundView addSubview:_presentImageView];
    
    _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 40, 90, 100)];
    _titleLabel.font = [UIFont systemFontOfSize:9];
    _titleLabel.numberOfLines = 2;
    [self.bubbleBackgroundView addSubview:_titleLabel];
    //金币
    _goldNumLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 115, 90, 12)];
    _goldNumLabel.textColor = [UIColor colorWithHexString:@"ffffff"];
    _goldNumLabel.font = [UIFont systemFontOfSize:11];
    _goldNumLabel.textAlignment = NSTextAlignmentCenter;
    [self.bubbleBackgroundView addSubview:_goldNumLabel];
    
    //魅力值
    _charamScoreLabel=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.goldNumLabel.frame)+10, 115 ,90, 12)];
    _charamScoreLabel.textColor=[UIColor colorWithHexString:@"ffffff"];
    _charamScoreLabel.font=[UIFont systemFontOfSize:11];
    _charamScoreLabel.textAlignment=NSTextAlignmentCenter;
    
    [self.bubbleBackgroundView addSubview:_charamScoreLabel];
    UILongPressGestureRecognizer *longPress =
    [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressed:)];
    [self.bubbleBackgroundView addGestureRecognizer:longPress];
    
    _expireTimeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _expireTimeBtn.frame = CGRectZero;
    [_expireTimeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _expireTimeBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [self.bubbleBackgroundView addSubview:_expireTimeBtn];
    
}

- (void)setDataModel:(RCMessageModel *)model {
    
    [self clearData];

    [super setDataModel:model];
    
    [self setAutoLayout];
    
}

- (void)setAutoLayout {
    
    CGRect contentFrame=self.messageContentView.frame;
    contentFrame.size.width = 209;
    contentFrame.size.height = 131;
    self.messageContentView.frame = contentFrame;
    self.bubbleBackgroundView.frame = CGRectMake(0, 0, contentFrame.size.width, contentFrame.size.height);
    
    RCMessageModel *model = self.model;
    PresentMessage *message = (PresentMessage *)model.content;
    
    //判断vehicle 还是 gift 消息
    if (!message.type || [message.type isEqualToString:@"a"]) {
        self.charamScoreLabel.hidden = NO;
        self.goldNumLabel.hidden = NO;
        self.expireTimeBtn.hidden = YES;
        self.bubbleBackgroundView.userInteractionEnabled = NO;
        self.titleLabel.text = SPDStringWithKey(@"送给你一份礼物希望你能喜欢", nil);
        self.titleLabel.textColor = SPD_HEXCOlOR(@"8840aa");
        [self layoutCellForGiftMessage:message];

        
    }else if([message.type isEqualToString:@"vehicle"]) {
        
        self.charamScoreLabel.hidden = YES;
        self.goldNumLabel.hidden = YES;
        self.expireTimeBtn.hidden = NO;
        self.titleLabel.text = SPDStringWithKey(@"送给你一辆座驾快去车库装备吧", nil);
        self.titleLabel.textColor = [UIColor whiteColor];

        [self layoutCellForVehicleMessage:message];

    }else if([message.type isEqualToString:@"good"]) {
        self.charamScoreLabel.hidden = YES;
        self.goldNumLabel.hidden = YES;
        self.expireTimeBtn.hidden = NO;
        self.titleLabel.text = SPDStringWithKey(@"送你一个装饰，希望你能喜欢", nil);
        self.titleLabel.textColor = SPD_HEXCOlOR(@"#AE7912");
        self.presentImageView.clipsToBounds = YES;
        [self layoutCellForVehicleMessage:message];
        
    }
    
}

//gift
-(void)layoutCellForGiftMessage:(PresentMessage *)message {
    //根据方向来排版
    
    self.titleLabel.font = [UIFont systemFontOfSize:12];

    if (MessageDirection_SEND == self.messageDirection) {
        
        self.bubbleBackgroundView.image=[UIImage imageNamed:@"img_send_present"];
        
        self.bubbleBackgroundView.userInteractionEnabled = NO;
        
        //修改礼物图片的frame
        CGRect presentImageViewRect=self.presentImageView.frame;
        presentImageViewRect.origin.x = 0;
        presentImageViewRect.origin.y = 10;
        self.presentImageView.frame=presentImageViewRect;
        
        CGRect titleRect = self.titleLabel.frame;
        titleRect.origin.x = CGRectGetMaxX(self.presentImageView.frame)+10;
        self.titleLabel.frame = titleRect;
        //发送方显示
        [_presentImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,message.imageName]]];
        _goldNumLabel.text = [NSString stringWithFormat:@"%@ -%@", SPDStringWithKey(message.currencyType ? @"钻石" : @"金币", nil), message.goldNum];
        _charamScoreLabel.text = [NSString stringWithFormat:@"%@ +%@",SPDStringWithKey(@"土豪值", nil),message.goldNum];
        
    }else if(MessageDirection_RECEIVE ==self.messageDirection){
        
        self.bubbleBackgroundView.image = [UIImage imageNamed:@"ic_receive_present"];
        
        CGRect titleRect = self.titleLabel.frame;
        titleRect.origin.x = 15;
        self.titleLabel.frame = titleRect;
        
        //修改礼物图片的frame
        CGRect presentImageViewRect = self.presentImageView.frame;
        presentImageViewRect.origin.x = 108;
        presentImageViewRect.origin.y = 10;
        self.presentImageView.frame=presentImageViewRect;
        
        //修改金币label的fram  ---frame 根据UI尺寸来的
        CGRect goldFrame = self.goldNumLabel.frame;
        goldFrame.origin.x = 5;
        self.goldNumLabel.frame= goldFrame;
        
        //魅力值frame
        CGRect charmFrame = self.charamScoreLabel.frame;
        charmFrame.origin.x = 0;
        charmFrame.size.width = self.bubbleBackgroundView.frame.size.width;
        self.charamScoreLabel.textAlignment = NSTextAlignmentCenter;
        self.charamScoreLabel.frame=charmFrame;
        
        //接受礼物的时候 接收方只是收到一礼物金额的一般
        [_presentImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,message.imageName]]];
        _charamScoreLabel.text=[NSString stringWithFormat:@"%@ +%@",SPDStringWithKey(@"魅力值", nil),message.charamScore];
    }
}

//vehicle
-(void)layoutCellForVehicleMessage:(PresentMessage *)message {
    //根据方向来排版
    
    self.titleLabel.font = [UIFont systemFontOfSize:9];

    if (MessageDirection_SEND == self.messageDirection) {
        
        //修改礼物图片的frame
        CGRect presentImageViewRect = self.presentImageView.frame;
        presentImageViewRect.origin.x = 17;
        presentImageViewRect.origin.y = [message.type isEqualToString:@"good"] ? 35 : 49;
        presentImageViewRect.size.width = 70.0f;
        presentImageViewRect.size.height = [message.type isEqualToString:@"good"] ? 70 : 46.0f;
        self.presentImageView.frame = presentImageViewRect;
        
        self.bubbleBackgroundView.userInteractionEnabled = NO;
        
        CGRect titleRect = self.titleLabel.frame;
        titleRect.origin.x = CGRectGetMaxX(self.presentImageView.frame)+10;
        self.titleLabel.frame = titleRect;
        
        //expiretime
        CGRect expire_frame = self.expireTimeBtn.frame;
        expire_frame.origin.x = 15;
        expire_frame.origin.y = 113;
        expire_frame.size.width = self.bubbleBackgroundView.frame.size.width-35;
        expire_frame.size.height = 16;
        self.expireTimeBtn.frame = expire_frame;
        self.expireTimeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        self.bubbleBackgroundView.image = [UIImage imageNamed:[message.type isEqualToString:@"vehicle"] ? @"img_send_vehicle":@"img_send_present"];
        //发送方显示
        [_presentImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,message.imageName]]];
        
    }else if(MessageDirection_RECEIVE == self.messageDirection){
        
        CGRect titleRect = self.titleLabel.frame;
        titleRect.origin.x = 15;
        self.titleLabel.frame = titleRect;
        
        //修改礼物图片的frame
        CGRect presentImageViewRect = self.presentImageView.frame;
        presentImageViewRect.origin.x = 122;
        presentImageViewRect.origin.y = [message.type isEqualToString:@"good"] ? 35 : 49;
        presentImageViewRect.size.width = 70.0f;
        presentImageViewRect.size.height = [message.type isEqualToString:@"good"] ? 70 : 46.0f;
        self.presentImageView.frame = presentImageViewRect;
        
        self.bubbleBackgroundView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapPress =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPress:)];
        [self.bubbleBackgroundView addGestureRecognizer:tapPress];
        
        //expiretime
        CGRect expire_frame = self.expireTimeBtn.frame;
        expire_frame.origin.x = 15;
        expire_frame.origin.y = 113;
        expire_frame.size.width = self.bubbleBackgroundView.frame.size.width-35;
        expire_frame.size.height = 16;
        self.expireTimeBtn.frame = expire_frame;
        self.expireTimeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

        self.bubbleBackgroundView.image=[UIImage imageNamed:[message.type isEqualToString:@"vehicle"] ?@"ic_receive_vehicle":@"ic_receive_present"];
        [_presentImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,message.imageName]]];
    }
    
    [self.expireTimeBtn setImage:[UIImage imageNamed:@"ic_chat_vehicle_expire"] forState:UIControlStateNormal];
    [self.expireTimeBtn setTitle:[NSString stringWithFormat:SPDStringWithKey(@"%@", nil),message.expire_time] forState:UIControlStateNormal];
    [self.expireTimeBtn setImagePosition:HHImagePositionLeft spacing:3.0f];

}

- (UIImage *)imageNamed:(NSString *)name ofBundle:(NSString *)bundleName {
    UIImage *image = nil;
    NSString *image_name = [NSString stringWithFormat:@"%@.png", name];
    NSString *resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString *bundlePath = [resourcePath stringByAppendingPathComponent:bundleName];
    NSString *image_path = [bundlePath stringByAppendingPathComponent:image_name];
    image = [[UIImage alloc] initWithContentsOfFile:image_path];
    
    return image;
}

- (void)longPressed:(id)sender {
    UILongPressGestureRecognizer *press = (UILongPressGestureRecognizer *)sender;
    if (press.state == UIGestureRecognizerStateEnded) {
        return;
    } else if (press.state == UIGestureRecognizerStateBegan) {
        [self.delegate didLongTouchMessageCell:self.model inView:self.bubbleBackgroundView];
    }
}

//升级SDK之后-
+(CGSize)sizeForMessageModel:(RCMessageModel *)model withCollectionViewWidth:(CGFloat)collectionViewWidth referenceExtraHeight:(CGFloat)extraHeight{
    
    return CGSizeMake(kScreenW, 131+extraHeight);
}

- (void)clearData {
    self.charamScoreLabel.text = @"";
    self.goldNumLabel.text = @"";
    self.expireTimeBtn.titleLabel.text = @"";
    self.presentImageView.image = [UIImage imageNamed:@""];
    self.bubbleBackgroundView.image = [UIImage imageNamed:@""];
    CGRect presentImageViewRect = self.presentImageView.frame;
    presentImageViewRect.size.width = 95;
    presentImageViewRect.size.height = 95;
    self.presentImageView.frame = presentImageViewRect;
}

- (void)tapPress:(id)sender {
    UITapGestureRecognizer *press = (UITapGestureRecognizer *)sender;
    if (press.state == UIGestureRecognizerStateEnded) {
        [self.delegate didTapMessageCell:self.model];
        return;
    }
}

@end
