//
//  GiftCVCell.h
//  GiftTsetView
//
//  Created by 侯玲 on 16/9/18.
//  Copyright © 2016年 侯玲. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GiftListModel;

@interface GiftCVCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *lightImg;

@property (nonatomic, strong) GiftListModel *model;

@end
