//
//  SPDLockExpireMessageCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/11/20.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "SPDLockExpireMessageCell.h"
#import "SPDLockExpireMessage.h"

@interface SPDLockExpireMessageCell ()

@property (nonatomic, strong) UIImageView *bubbleImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIButton *buyButton;

@end

@implementation SPDLockExpireMessageCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    self.bubbleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, pixwn(271), pixwn(160))];
    self.bubbleImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.bubbleImageView.userInteractionEnabled = YES;
    [self.messageContentView addSubview:self.bubbleImageView];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(pixwn(94), pixwn(10), pixwn(91), pixwn(20))];
    self.titleLabel.text = SPDStringWithKey(@"房间锁", nil);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.textColor = [UIColor colorWithHexString:@"FF5D84"];
    self.titleLabel.font = [UIFont systemFontOfSize:13];
    [self.bubbleImageView addSubview:self.titleLabel];
    
    self.contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(pixwn(22), pixwn(53), pixwn(235), pixwn(35))];
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.textColor = [UIColor whiteColor];
    self.contentLabel.font = [UIFont systemFontOfSize:pixwn(14)];
    self.contentLabel.text = SPDStringWithKey(@"您的房间锁已过期，继续购买房间锁后可再次使用", nil);
    [self.bubbleImageView addSubview:self.contentLabel];
    
    self.buyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.buyButton.frame = CGRectMake(pixwn(92.5), pixwn(112.5), pixwn(94), pixwn(29));
    self.buyButton.backgroundColor = [UIColor whiteColor];
    self.buyButton.clipsToBounds = YES;
    self.buyButton.layer.cornerRadius = pixwn(14.5);
    self.buyButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.buyButton setTitle:SPDStringWithKey(@"购买", nil) forState:UIControlStateNormal];
    [self.buyButton setTitleColor:[UIColor colorWithHexString:@"FF5D84"] forState:UIControlStateNormal];
    [self.buyButton addTarget:self action:@selector(clickBuyButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.bubbleImageView addSubview:self.buyButton];
}

- (void)setDataModel:(RCMessageModel *)model {
    [super setDataModel:model];
    [self setAutoLayout];
}

- (void)setAutoLayout {
    CGRect messageContentFrame = self.messageContentView.frame;
    messageContentFrame.size.width = pixwn(271);
    messageContentFrame.size.height = pixwn(160);
    self.messageContentView.frame = messageContentFrame;
    
    self.bubbleImageView.image = [UIImage imageNamed:@"chatroom_lock_msg"];
}

- (void)clickBuyButton:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didTapMessageCell:)]) {
        [self.delegate didTapMessageCell:self.model];
    }
}

+ (CGSize)sizeForMessageModel:(RCMessageModel *)model withCollectionViewWidth:(CGFloat)collectionViewWidth referenceExtraHeight:(CGFloat)extraHeight{
    return CGSizeMake(kScreenW, pixwn(160) + extraHeight);
}

@end
