//
//  VehicleCVCell.h
//  SimpleDate
//
//  Created by Monkey on 2017/7/25.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPDVehicleModel.h"

@interface VehicleCVCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *moreVehicleLabel;

@property (weak, nonatomic) IBOutlet UIImageView *lightImg;

@property (nonatomic,strong)SPDVehicleModel *vehicleModel;

@end
