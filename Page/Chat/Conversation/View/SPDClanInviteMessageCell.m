//
//  SPDClanInviteMessageCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/10/16.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDClanInviteMessageCell.h"
#import "SPDClanInviteMessage.h"

@interface SPDClanInviteMessageCell ()

@property (nonatomic, strong) UIImageView *bubbleImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIButton *refuseBtn;
@property (nonatomic, strong) UIButton *agreeBtn;

@end

@implementation SPDClanInviteMessageCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    self.bubbleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, pixwn(271), pixwn(158))];
    self.bubbleImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.bubbleImageView.userInteractionEnabled = YES;
    [self.messageContentView addSubview:self.bubbleImageView];

    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(pixwn(90), pixwn(13), pixwn(100), pixwn(15))];
    self.titleLabel.text = SPDStringWithKey(@"家族邀请", nil);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.font = [UIFont systemFontOfSize:13];
    [self.bubbleImageView addSubview:self.titleLabel];
    
    self.contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(pixwn(20), pixwn(54), pixwn(271) - pixwn(20) - pixwn(11), pixwn(40))];
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.textColor = [UIColor whiteColor];
    self.contentLabel.font = [UIFont systemFontOfSize:pixwn(14)];
    [self.bubbleImageView addSubview:self.contentLabel];
    
    self.refuseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.refuseBtn.frame = CGRectMake(pixwn(58), pixwn(107), pixwn(70), pixwn(29));
    self.refuseBtn.clipsToBounds = YES;
    self.refuseBtn.layer.cornerRadius = pixwn(14.5);
    self.refuseBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.refuseBtn setTitle:SPDStringWithKey(@"拒绝", nil) forState:UIControlStateNormal];
    [self.refuseBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.refuseBtn addTarget:self action:@selector(clickRefuseBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.bubbleImageView addSubview:self.refuseBtn];
    
    self.agreeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.agreeBtn.frame = CGRectMake(pixwn(151), pixwn(107), pixwn(70), pixwn(29));
    self.agreeBtn.clipsToBounds = YES;
    self.agreeBtn.layer.cornerRadius = pixwn(14.5);
    self.agreeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.agreeBtn setTitle:SPDStringWithKey(@"同意", nil) forState:UIControlStateNormal];
    [self.agreeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.agreeBtn addTarget:self action:@selector(clickAgreeBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.bubbleImageView addSubview:self.agreeBtn];
}

- (void)setDataModel:(RCMessageModel *)model {
    [super setDataModel:model];
    [self setAutoLayout];
}

- (void)setAutoLayout {
    CGRect messageContentFrame = self.messageContentView.frame;
    messageContentFrame.size.width = pixwn(271);
    messageContentFrame.size.height = pixwn(158);
    self.messageContentView.frame = messageContentFrame;
    
    SPDClanInviteMessage *message = (SPDClanInviteMessage *)self.model.content;
    if ([message.type isEqualToString:@"invite"]) {
        self.contentLabel.text = SPDStringWithKey(@"快来加入我的家族一起签到领金币吧", nil);
        self.refuseBtn.hidden = NO;
        self.agreeBtn.hidden = NO;
        if ([DBUtil queryMessageState:self.model.messageUId]) {
            self.refuseBtn.userInteractionEnabled = NO;
            [self.refuseBtn setBackgroundColor:[UIColor colorWithHexString:@"dc7584"]];
            self.agreeBtn.userInteractionEnabled = NO;
            [self.agreeBtn setBackgroundColor:[UIColor colorWithHexString:@"dc7584"]];
        } else {
            self.refuseBtn.userInteractionEnabled = YES;
            [self.refuseBtn setBackgroundColor:[UIColor colorWithHexString:@"fb5e74"]];
            self.agreeBtn.userInteractionEnabled = YES;
            [self.agreeBtn setBackgroundColor:[UIColor colorWithHexString:@"fb5e74"]];
        }
    } else {
        self.refuseBtn.hidden = YES;
        self.agreeBtn.hidden = YES;
        if ([message.type isEqualToString:@"refuse"]) {
            self.contentLabel.text = SPDStringWithKey(@"不好意思，暂时不想加入家族", nil);
        } else if ([message.type isEqualToString:@"agree"]) {
            self.contentLabel.text = SPDStringWithKey(@"我已经加入你的家族，快带我一起玩吧", nil);
        } else if ([message.type isEqualToString:@"apply"]) {
            self.contentLabel.text = SPDStringWithKey(@"已同意加入家族，请通知管理员审核", nil);
        }
    }
    
    self.bubbleImageView.image = [UIImage imageNamed:@"img_clan_invite_bg"];
}

- (void)clickRefuseBtn:(UIButton *)sender {
    self.model.extra = @"refuse";
    [self handleBtnClick];
}

- (void)clickAgreeBtn:(UIButton *)sender {
    self.model.extra = @"agree";
    [self handleBtnClick];
}

- (void)handleBtnClick {
    [DBUtil recordMessage:self.model.messageUId state:1];
    self.refuseBtn.userInteractionEnabled = NO;
    [self.refuseBtn setBackgroundColor:[UIColor colorWithHexString:@"dc7584"]];
    self.agreeBtn.userInteractionEnabled = NO;
    [self.agreeBtn setBackgroundColor:[UIColor colorWithHexString:@"dc7584"]];
    if ([self.delegate respondsToSelector:@selector(didTapMessageCell:)]) {
        [self.delegate didTapMessageCell:self.model];
    }
}

+ (CGSize)sizeForMessageModel:(RCMessageModel *)model withCollectionViewWidth:(CGFloat)collectionViewWidth referenceExtraHeight:(CGFloat)extraHeight{
    return CGSizeMake(kScreenW, pixwn(158) + extraHeight);
}

@end
