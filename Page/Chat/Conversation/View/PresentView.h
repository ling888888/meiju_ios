//
//  GiftView.h
//  TsetGift
//
//  Created by 侯玲 on 16/9/18.
//  Copyright © 2016年 侯玲. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPDVehicleModel.h"
#import "SPDMagicModel.h"

typedef NS_ENUM(NSInteger,PresentViewSelectIndex){
    SPDSelectedGift,
    SPDSelectedDiamondGift,
    SPDSelectedVehicle,
    SPDSelectedMagic,
    SPDSelectedNobleGift
};

@protocol PresentViewDelegate <NSObject>

@optional
//点击赠送按钮
-(void)clickedSendPresentButtonWithModel:(GiftListModel *)model num:(NSString *)num;

-(void)clickedSendVehicleButtonWithVehicleModel:(SPDVehicleModel * )vehicleModel;

-(void)clickedSendMagicButtonWithSPDMagicModel:(SPDMagicModel * )magicModel;

-(void)clickedBillRechargeButton;//点击 充值金币按钮

-(void)presentViewAlreadyHidden;

@end

@interface PresentView : UIView

@property(nonatomic,strong)NSMutableArray *dataArray;//显示的礼物的金币和魅力

@property(nonatomic,strong)NSMutableArray *vehicleArray;//座驾的数组

@property(nonatomic,strong)NSMutableArray *magicArray;//魔法数组

@property (nonatomic, strong) NSMutableArray *nobleGiftArray; // 贵族礼物数组
@property (nonatomic, strong) NSMutableArray *diamondGiftArray; // 贵族礼物数组

@property(nonatomic,copy)NSString *goldNum;//当前金币数量

@property (nonatomic, assign) PresentViewSelectIndex selectedIndex; //代表选择的是【魔法 礼物 座驾】的index

@property (nonatomic, copy) NSString *lang; //对方语言区

@property(nonatomic,weak)id<PresentViewDelegate> delegate;

+ (instancetype)presentView;
- (void)show;
- (void)dismiss;

@end
