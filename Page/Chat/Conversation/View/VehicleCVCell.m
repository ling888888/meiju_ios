//
//  VehicleCVCell.m
//  SimpleDate
//
//  Created by Monkey on 2017/7/25.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "VehicleCVCell.h"
#import "UIButton+HHImagePosition.h"

@interface VehicleCVCell ()
@property (weak, nonatomic) IBOutlet UIButton *expireTimeButton;
@property (weak, nonatomic) IBOutlet UIImageView *vehicleImageView;
@property (weak, nonatomic) IBOutlet UILabel *vehicleNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *priceButton;

@end

@implementation VehicleCVCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void)setVehicleModel:(SPDVehicleModel *)vehicleModel {
    _vehicleModel = vehicleModel;
    
    [self.expireTimeButton setTitle:SPDStringWithKey(vehicleModel.valid_time, nil)  forState:UIControlStateNormal];
    [self.expireTimeButton setImage:[UIImage imageNamed:@"ic_daoju_valid_time"] forState:UIControlStateNormal];
    [self.expireTimeButton setTitleEdgeInsets:UIEdgeInsetsMake(0,3, 0, 0)];

    [self.vehicleImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,vehicleModel.image]]];
    self.vehicleNameLabel.text = vehicleModel.name;
    
    
    [self.priceButton setTitle:[vehicleModel.price stringValue]    forState:UIControlStateNormal];
    [self.priceButton setImage:[UIImage imageNamed:@"daoju_vehicle_price"] forState:UIControlStateNormal];
    [self.priceButton setImagePosition:HHImagePositionLeft spacing:3.0f];
    
}

@end
