//
//  SPDSearchUserViewController.m
//  SimpleDate
//
//  Created by 李楠 on 17/8/16.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDSearchUserViewController.h"
#import "SPDClanHotListCell.h"
#import "FamilyModel.h"
#import "LY_HomePageViewController.h"
#import "ZegoKitManager.h"

@interface SPDSearchUserViewController ()<UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, SPDClanHotListCellDelegate>

@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property (weak, nonatomic) IBOutlet UIImageView *searchIcon;
@property (weak, nonatomic) IBOutlet UILabel *searchTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *searchTextLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (weak, nonatomic) IBOutlet UIButton *searchClanBtn;
@property (weak, nonatomic) IBOutlet UIImageView *searchClanIcon;
@property (weak, nonatomic) IBOutlet UILabel *searchClanTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *searchClanTextLabel;
@property (weak, nonatomic) IBOutlet UIImageView *clanArrowImageView;
@property (weak, nonatomic) IBOutlet UIView *titleVew;
@property (weak, nonatomic) IBOutlet UILabel *searchHistoryLabel;
@property (weak, nonatomic) IBOutlet UIButton *clearHistroyBtn;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleVewTopConstant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBtnTopConstant;

@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) NSMutableArray *historyArray;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) FamilyModel *resultModel;
@property (nonatomic, assign) BOOL isAgentService;

@end

@implementation SPDSearchUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureSubviews];
    [self requestMyInfoDetail];
    [self reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.searchBar becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.searchBar resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods

- (void)configureSubviews {
    if (self.showSeachBarOnNavigationBar) {
        UIView *wrapView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW - 8 * 2, 44)];
        self.searchBar.frame = wrapView.bounds;
        [wrapView addSubview:self.searchBar];
        self.navigationItem.titleView = wrapView;
        self.titleVewTopConstant.constant = NavHeight;
        self.searchBtnTopConstant.constant = NavHeight;
    } else {
        self.view.backgroundColor = [UIColor whiteColor];
        self.tableView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:self.searchBar];
        [self.searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.and.leading.and.trailing.mas_equalTo(0);
            make.height.mas_equalTo(48);
        }];
        self.titleVewTopConstant.constant = 48;
        self.searchBtnTopConstant.constant = 48;
    }
    self.searchTitleLabel.text = SPDStringWithKey(@"用户ID:", nil);
    self.searchClanTitleLabel.text = SPDStringWithKey(@"家族ID:", nil);
    self.arrowImageView.image = [UIImage imageNamed:@"ic_all_clan_arrow"].adaptiveRtl;
    self.clanArrowImageView.image = [UIImage imageNamed:@"ic_all_clan_arrow"].adaptiveRtl;
    self.searchHistoryLabel.text = SPDStringWithKey(@"搜索历史", nil);
    [self.clearHistroyBtn setTitle:SPDStringWithKey(@"清除历史", nil) forState:UIControlStateNormal];
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"SearchHistoryCell"];
//    self.backBtn = nil;
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = true;
}

- (void)reloadData {
    self.historyArray = [DBUtil querySearchHistory];
    if (self.historyArray.count) {
        self.titleVew.hidden = NO;
        self.tableView.hidden = NO;
    } else {
        self.titleVew.hidden = YES;
        self.tableView.hidden = YES;
    }
    [self.tableView reloadData];
}

- (void)hideSearchBtn:(BOOL)hidden {
    self.searchBtn.hidden = hidden;
    self.searchIcon.hidden = hidden;
    self.searchTitleLabel.hidden = hidden;
    self.searchTextLabel.hidden = hidden;
    self.arrowImageView.hidden = hidden;
    self.searchClanBtn.hidden = hidden;
    self.searchClanIcon.hidden = hidden;
    self.searchClanTitleLabel.hidden = hidden;
    self.searchClanTextLabel.hidden = hidden;
    self.clanArrowImageView.hidden = hidden;
}

- (void)requestMyInfoDetail {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.info.detail" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.isAgentService = [suceess[@"is_agent_gm"] boolValue];
    } bFailure:^(id  _Nullable failure) {

    }];
}

- (void)requestUserBatchWithUserId:(NSString *)userId {
    NSString *originalId = userId;
    NSMutableDictionary *dic = [NSMutableDictionary new];
    if (userId.length >= 6) {
        [dic setObject:@0 forKey:@"is_special"];
        userId = [userId substringFromIndex:1];
        if (userId.length == 5) {
            userId = [NSString stringWithFormat:@"%ld", userId.integerValue];
        }
    } else {
        [dic setObject:@1 forKey:@"is_special"];
    }
    [dic setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:@[userId] options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding] forKey:@"user_ids"];
    [RequestUtils commonGetRequestUtils:dic bURL:@"user.batch" bAnimated:YES bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        NSArray *user = suceess[@"users"];
        if (user.count) {
            [DBUtil recordSearchHistory:originalId];
            
            LY_HomePageViewController *vc = [[LY_HomePageViewController alloc] initWithUserId:user[0][@"_id"]];
            [self.navigationController pushViewController:vc animated:true];
            if (!self.tableView.hidden) {
                [self reloadData];
            }
        } else {
            [LY_HUD showToastTips:@"ID不存在，请检查输入是否有误".localized];
        }
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestClanSearchWithNickId:(NSString *)nickId {
    NSDictionary *dic = @{@"nick_id": nickId};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"search" isClan:YES bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        [DBUtil recordSearchHistory:nickId];
        self.resultModel = [FamilyModel initWithDictionary:suceess[@"clan"]];
        self.collectionView.hidden = NO;
        [self.collectionView reloadData];
    } bFailure:^(id  _Nullable failure) {
        [LY_HUD showToastTips:@"ID不存在，请检查输入是否有误".localized];
    }];
}

#pragma mark - Event response

- (IBAction)search:(UIButton *)sender {
    NSString *searchText = self.searchTextLabel.text;
    if (([searchText hasPrefix:@"9"] && (searchText.length == 6 || (searchText.length > 6 && [[searchText substringWithRange:NSMakeRange(1, 1)] integerValue] != 0))) || searchText.length < 6) {
        [self requestUserBatchWithUserId:searchText];
    } else {
        [LY_HUD showToastTips:@"ID不存在，请检查输入是否有误".localized];
    }
}

- (IBAction)searchClan:(id)sender {
    NSString *searchText = self.searchTextLabel.text;
    if ([searchText hasPrefix:@"6"] && searchText.length >= 6) {
        [self requestClanSearchWithNickId:searchText];
    } else {
        [LY_HUD showToastTips:@"ID不存在，请检查输入是否有误".localized];
    }
}

- (IBAction)clearHistory:(UIButton *)sender {
    [DBUtil deleteSearchHistory];
    [self reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.historyArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchHistoryCell" forIndexPath:indexPath];
    NSString *searchID = _historyArray[indexPath.row];
    if ([searchID hasPrefix:@"6"] && searchID.length >= 6) {
        cell.textLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"家族：%@", nil), searchID];
    } else {
        cell.textLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"用户：%@", nil), searchID];
    }
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.textColor = [UIColor colorWithHexString:@"999999"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *searchID = self.historyArray[indexPath.row];
    if (searchID.length < 6 || [searchID hasPrefix:@"9"]) {
        [self requestUserBatchWithUserId:searchID];
    } else if ([searchID hasPrefix:@"6"]) {
        [self requestClanSearchWithNickId:searchID];
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (searchText.length > 10) {
        searchText = [searchText substringToIndex:10];
        searchBar.text = searchText;
    }
    self.searchTextLabel.text = searchText;
    self.searchClanTextLabel.text = searchText;
    self.collectionView.hidden = YES;
    if ([searchText isEqualToString:@""]) {
        [self hideSearchBtn:YES];
        [self reloadData];
    } else {
        [self hideSearchBtn:NO];
        self.titleVew.hidden = YES;
        self.tableView.hidden = YES;
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    if (self.showSeachBarOnNavigationBar) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        self.searchBar.text = @"";
        [self searchBar:self.searchBar textDidChange:@""];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.resultModel ? 1 : 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SPDClanHotListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SPDClanHotListCell" forIndexPath:indexPath];
    cell.delegate = self;
    cell.isAgentService = self.isAgentService;
    cell.model = self.resultModel;
    cell.nickId = self.resultModel.nick_id;
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([[SPDApiUser currentUser].lang isEqualToString:self.resultModel.lang]) {
        [ZegoManager joinRoomFrom:self clanId:self.resultModel._id];
    } else {
        [LY_HUD showToastTips:@"对方和你不在同一语言区，不能跨区进家族".localized];
    }
}

#pragma mark - SPDClanHotListCellDelegate

- (void)didClickStickBtnWithModel:(FamilyModel *)model {
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"确定置顶该家族".localized message:nil preferredStyle:UIAlertControllerStyleAlert];;
    [alertC addAction:[UIAlertAction actionWithTitle:@"取消".localized style:UIAlertActionStyleCancel handler:nil]];
    [alertC addAction:[UIAlertAction actionWithTitle:@"确定".localized style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSDictionary *dic = @{@"user_id": [SPDApiUser currentUser].userId, @"clan_id": model._id};
        [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"stick" isClan:YES bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
            [LY_HUD showToastTips:@"置顶成功".localized];
        } bFailure:^(id  _Nullable failure) {
            if ([failure isKindOfClass:[NSDictionary class]]) {
                if ([failure[@"code"] integerValue] == 2038) {
                    [LY_HUD showToastTips:@"本次置顶次数用完，请1小时候再进行置顶".localized];
                } else if ([failure[@"code"] integerValue] == 2037) {
                    [LY_HUD showToastTips:@"你不是这个区的客服助手".localized];
                } else {
                    [LY_HUD showToastTips:failure[@"msg"]];
                }
            }
        }];
    }]];
    [self presentViewController:alertC animated:true completion:nil];
}

#pragma mark - setter / getter

- (UISearchBar *)searchBar {
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc] init];
        _searchBar.delegate = self;
        _searchBar.placeholder = SPDStringWithKey(@"请输入FamyID/家族ID搜索", nil);
        _searchBar.showsCancelButton = YES;
        _searchBar.backgroundImage = [[UIImage alloc] init];
        [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil] setTitle:SPDStringWithKey(@"取消", nil)];
        UITextField *searchField = [_searchBar valueForKey:@"searchField"];
        if (searchField) {
            searchField.keyboardType = UIKeyboardTypeASCIICapableNumberPad;
            searchField.layer.masksToBounds = YES;
            if ([[UIDevice currentDevice].systemVersion floatValue] >= 11) {
                searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:SPDStringWithKey(@"请输入FamyID/家族ID搜索", nil) attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15]}];
            }
        }
        
        if (self.showSeachBarOnNavigationBar) {
            _searchBar.tintColor = [UIColor colorWithHexString:COMMON_PINK];
            if (searchField) {
                searchField.backgroundColor = SPD_HEXCOlOR(@"f8f8f8");
                if ([[UIDevice currentDevice].systemVersion floatValue] >= 11) {
                    searchField.layer.cornerRadius = 18;
                } else {
                    searchField.layer.cornerRadius = 14;
                }
            }
        } else {
            _searchBar.tintColor = [UIColor colorWithHexString:COMMON_PINK];
            _searchBar.backgroundColor = [UIColor colorWithHexString:@"f8f8f8"];
            if (searchField) {
                searchField.layer.cornerRadius = 5.0f;
                searchField.layer.borderWidth = 0.5f;
                searchField.layer.borderColor = [UIColor colorWithHexString:@"d4d4d4"].CGColor;
            }
        }
    }
    return _searchBar;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(kScreenW, 101);
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        CGFloat y = self.showSeachBarOnNavigationBar ? NavHeight : 48;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, y, kScreenW, self.view.bounds.size.height - y) collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        [_collectionView registerNib:[UINib nibWithNibName:@"SPDClanHotListCell" bundle:nil] forCellWithReuseIdentifier:@"SPDClanHotListCell"];
        [self.view addSubview:_collectionView];
    }
    return _collectionView;
}

@end
