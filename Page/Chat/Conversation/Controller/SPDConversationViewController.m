//
//  JYConversationViewController.m
//  SimpleDate
//
//  Created by 周佳磊 on 16/1/18.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "SPDConversationViewController.h"
#import "ZLPhoto.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "PresentView.h"
#import "PresentMessage.h"
#import "PresentMessageCell.h"
#import "SPDClanCallMessage.h"
#import "SPDClanCallMessageCell.h"
#import "GiftListModel.h"
#import "SPDApiUser.h"
#import <QuartzCore/QuartzCore.h>
#import "SPDHashGenerator.h"
#import "NSString+XXWAddition.h"
#import "ZegoKitManager.h"
#import "SPDVehicleViewController.h"
#import "SPDPrivateMagicMessage.h"
#import "SPDPrivateMagicCell.h"
#import "SPDUserOnlineTitleView.h"
#import "SPDTipsMessage.h"
#import "SPDTipsMessageCell.h"
#import "SPDHelpController.h"
#import "SPDFriendMessage.h"
#import "SPDFriendMessageCell.h"
#import "JXPopoverView.h"
#import "ReportViewController.h"
#import "SPDLotteryMessage.h"
#import "SPDLotteryMessageCell.h"
#import "SPDGiftDoubleRewardMessage.h"
#import "SPDGiftDoubleRewardMessageCell.h"
#import "SPDClanInviteMessage.h"
#import "SPDClanInviteMessageCell.h"
#import "SPDLockExpireMessage.h"
#import "SPDLockExpireMessageCell.h"
#import "ChatroomLockViewController.h"
#import "KKGiftGivingView.h"
#import "HomeModel.h"
#import "LiveBeginNotificationMessage.h"
#import "LiveNotifyFansMessageCell.h"
#import "LiveBeginNotificationMessage.h"
#import "LiveModel.h"
#import "LiveRoomViewController.h"
@interface RCButton : UIButton

@end

@interface SPDConversationViewController ()<PresentViewDelegate, JYCallSessionDelegate, RCMessageCellDelegate, KKGiftGivingViewDelegate>

@property (nonatomic, copy) NSNumber *balance;

@property (nonatomic, strong) UIButton *presentButton;

@property (nonatomic, copy)  NSString *rechage;

@property (nonatomic, copy) PresentMessage *message;

@property (nonatomic, assign)  BOOL av_iOS;

@property (nonatomic, copy)NSArray * serviceTargetIdsArray;

@property (nonatomic, copy)NSString * callType;

@property (nonatomic, strong)BaseViewController * baseView;

@property (nonatomic, strong)SPDUserOnlineTitleView *titleView;

@property (nonatomic, assign)  int target_level ;
@property (nonatomic, assign)BOOL isFavorite; //shi
@property (nonatomic, assign)BOOL isFriend; //shi
@property (nonatomic, assign)BOOL isBlock; //shi

@property (nonatomic, assign) BOOL isVip;
@property (nonatomic, assign) BOOL isSevenVip;
@property (nonatomic, assign) BOOL selfIsService;
@property (nonatomic, assign) BOOL otherIsService;
@property (nonatomic, assign) BOOL isBlocked;
@property (nonatomic, assign) BOOL strangerMsgSwitch;
@property (nonatomic, strong) HomeModel * currentModel;

@end

NSInteger const RCTOOLBAR_INVITE_ITEM_INDEX       = 2000;

BOOL isVideoStatus=false;
BOOL isInPrivateConversation = false;

@implementation SPDConversationViewController


- (BaseViewController *)baseView {
    if (!_baseView) {
        _baseView = [BaseViewController new];
    }
    return _baseView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.specialDict.allKeys.count == 0) {
        self.navigationItem.titleView = self.titleView;
    }
    self.navigationItem.rightBarButtonItem = nil;


    RCButton *button = self.chatSessionInputBarControl.switchButton;
    [button removeTarget:nil action:nil forControlEvents:UIControlEventTouchUpInside];
    [button addTarget:self action:@selector(__buttonClick:) forControlEvents:UIControlEventTouchUpInside];

    
    [self.chatSessionInputBarControl.pluginBoardView removeAllItems];
    [self.chatSessionInputBarControl.pluginBoardView insertItemWithImage:[RCKitUtility imageNamed:@"voip/actionbar_audio_call_icon" ofBundle:@"RongCloud.bundle"] title:SPDStringWithKey(@"语音通话", nil) atIndex:1 tag:PLUGIN_BOARD_ITEM_VOIP_TAG];
    [self.chatSessionInputBarControl.pluginBoardView insertItemWithImage:[RCKitUtility imageNamed:@"voip/actionbar_video_call_icon" ofBundle:@"RongCloud.bundle"] title:SPDStringWithKey(@"视频聊天" , nil) atIndex:0 tag:PLUGIN_BOARD_ITEM_VIDEO_VOIP_TAG];
    [self.chatSessionInputBarControl.pluginBoardView insertItemWithImage:[RCKitUtility imageNamed:@"actionbar_picture_icon" ofBundle:@"RongCloud.bundle"] title:SPDStringWithKey(@"相册", nil) atIndex:2 tag:PLUGIN_BOARD_ITEM_ALBUM_TAG];
    [self.chatSessionInputBarControl.pluginBoardView insertItemWithImage:[RCKitUtility imageNamed:@"actionbar_camera_icon" ofBundle:@"RongCloud.bundle"] title:SPDStringWithKey(@"照相", nil) atIndex:3 tag:PLUGIN_BOARD_ITEM_CAMERA_TAG];
    
    //设置头像为圆形
    [[RCIM sharedRCIM] setGlobalMessageAvatarStyle:RC_USER_AVATAR_CYCLE];
    
    [self setEnableNewComingMessageIcon:YES];
    
    //注册自定义消息
    [self registerClass:[PresentMessageCell class] forMessageClass:[PresentMessage class]];
    [self registerClass:[SPDClanCallMessageCell class] forMessageClass:[SPDClanCallMessage class]];
    [self registerClass:[SPDPrivateMagicCell class] forMessageClass:[SPDPrivateMagicMessage class]];
    [self registerClass:[SPDTipsMessageCell class] forMessageClass:[SPDTipsMessage class]];
    [self registerClass:[SPDFriendMessageCell class] forMessageClass:[SPDFriendMessage class]];
    [self registerClass:[SPDLotteryMessageCell class] forMessageClass:[SPDLotteryMessage class]];
    [self registerClass:[SPDGiftDoubleRewardMessageCell class] forMessageClass:[SPDGiftDoubleRewardMessage class]];
    [self registerClass:[SPDClanInviteMessageCell class] forMessageClass:[SPDClanInviteMessage class]];
    [self registerClass:[SPDLockExpireMessageCell class] forMessageClass:[SPDLockExpireMessage class]];
    [self registerClass:[LiveNotifyFansMessageCell class] forMessageClass:[LiveBeginNotificationMessage class]];
    
    [self requestMyInfoDetail];
    [self requestApOrderOpposite];
    
}

- (void)__buttonClick:(id)sender {
    //在此添加自己的逻辑
    if ([ZegoKitManager sharedInstance].clan_id != nil) {
        [LY_HUD showToastTips:@"请退出聊天室后使用此功能".localized];
        return;
    }

    //以下代码是切换输入框文本和语音状态的逻辑
    if (self.chatSessionInputBarControl.inputTextView.hidden) {
        [self.chatSessionInputBarControl updateStatus:KBottomBarKeyboardStatus animated:YES];
    } else {
        [self.chatSessionInputBarControl updateStatus:KBottomBarRecordStatus animated:YES];
    }
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    // 源码在这里改变了 collectionview 的frame 所以我直接在这里写了。。。
    if (self.specialDict.allKeys.count >0) {
        self.navigationItem.title = SPDStringWithKey(_specialDict[@"title"], nil);
        NSString * userId = _specialDict[@"userId"];
        if (![userId isEqualToString:@"13"]) {
            self.chatSessionInputBarControl.hidden = YES;
            CGRect collectionFrame = self.conversationMessageCollectionView.frame;
            collectionFrame.size.height = kScreenH - NavHeight;
            self.conversationMessageCollectionView.frame = collectionFrame;
        }
    }
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    isInPrivateConversation = YES;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    isInPrivateConversation = NO;
}

- (void)notifyUpdateUnreadMessageCount {
    
}

- (void)haveBecomeFriend:(NSNotification*)notification {
    NSString * sid = notification.object;
    if ([sid isEqualToString:self.targetId]) {
        self.isFriend = YES;
    }
}

- (void)handleRightBarItemClicked {
    
    JXPopoverView *popoverView = [JXPopoverView popoverView];
    JXPopoverAction *detail = [JXPopoverAction actionWithTitle:SPDStringWithKey(@"查看资料", nil) handler:^(JXPopoverAction *action) {
        [SPDCommonTool pushToDetailTableViewController:self userId:self.targetId];
    }];
    NSString *favorTitle= self.isFavorite ? SPDStringWithKey(@"取消关注", nil): SPDStringWithKey(@"关注", nil);
    JXPopoverAction *favor = [JXPopoverAction actionWithTitle:favorTitle handler:^(JXPopoverAction *action) {
        [self handleFavorUser];
    }];
    NSString *friendTitle= !self.isFriend ? SPDStringWithKey(@"加好友", nil): SPDStringWithKey(@"删除好友", nil);
    JXPopoverAction *addfriend = [JXPopoverAction actionWithTitle:friendTitle handler:^(JXPopoverAction *action) {
        [self handleFriend];
    }];
    NSString * title = self.isBlock ? @"取消拉黑": @"拉黑好友";
    JXPopoverAction *block = [JXPopoverAction actionWithTitle:SPDStringWithKey(title, nil) handler:^(JXPopoverAction *action) {
        NSString *msg = self.isBlock ? @"确定要将此人移出黑名单吗？\n48小时内不可再次拉黑":@"拉黑的用户不能再：\n与你聊天，视频，语音\n加你为好友\n如果你们是好友，拉黑对方后，好友关系也将解除";
        [SPDCommonTool presentAutoController:self title:SPDStringWithKey(title, nil) titleColor:nil titleFontSize:0 messageString:SPDStringWithKey(msg, nil) messageColor:nil messageFontSize:0 cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
            
        } confirmTitle:SPDStringWithKey(@"确认", nil) confirmAction:^{
            [self handleUserFromBlackList];
        }];
        
    }];
    JXPopoverAction *report = [JXPopoverAction actionWithTitle:SPDStringWithKey(@"举报", nil) handler:^(JXPopoverAction *action) {
        ReportViewController * report = [ReportViewController new];
        report.userId = self.targetId;
        [self.navigationController pushViewController:report animated:YES];
    }];
    [popoverView showToPoint:CGPointMake(kScreenW - popoverView.bounds.size.width, NavHeight) withActions:@[detail, addfriend, block, report]];

}

- (void)handleFavorUser {
    NSString * url = self.isFavorite ? @"my.favorite.remove": @"my.favorite.add";
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:self.targetId forKey:@"user_id"];
    [RequestUtils commonPostRequestUtils:dict bURL:url bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.isFavorite=!self.isFavorite;
        [self showToast:SPDStringWithKey(@"操作成功",nil)];
    } bFailure:^(id  _Nullable failure) {
        [self showToast:failure[@"msg"]];
    }];
}

- (void)handleFriend {
    NSString * title = self.isFriend ? @"确定删除好友吗？":@"确定加为好友吗？";
    [SPDCommonTool presentCommonController:self title:SPDStringWithKey(title, nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
        
    } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
        if (self.isFriend) {
            [self handleFriendActionType:@"delete"];
        }else{
            [self handleFriendActionType:@"request"];
        }
    }];
}

- (void)handleFriendActionType:(NSString * )type {
    [SPDCommonTool requestFriendPostWithUserId:self.targetId type:type success:^(id  _Nullable suceess) {
        if ([type isEqualToString:@"request"]) {
            [self showToast:SPDStringWithKey(@"请求发送成功，请等待对方回应", nil)];
        } else {
            self.isFriend = !self.isFriend;
            [self showToast:SPDStringWithKey(@"删除好友成功", nil)];
        }
    } failure:^(id  _Nullable failure) {

    }];
}

#pragma mark - 移出黑名单
- (void)handleUserFromBlackList{
    NSMutableDictionary * dict =[NSMutableDictionary new];
    [dict setObject:self.targetId forKey:@"user_id"];
    NSString * url = self.isBlock ? @"my.blacklist.remove": @"my.blacklist.add";
    [RequestUtils commonPostRequestUtils:dict bURL:url bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.isBlock = !self.isBlock;
        [SPDCommonTool showWindowToast:(self.isBlock ?SPDStringWithKey(@"操作成功", nil): SPDStringWithKey(@"移出黑名单成功,可以与对方视频语音、聊天了", nil))];
    } bFailure:^(id  _Nullable failure) {
        if ([failure isKindOfClass:[NSDictionary class]]) {
            if ([failure[@"code"] integerValue] == 2005) {
                [self showToast:SPDStringWithKey(@"不能拉黑客服", nil)];
            } else {
                [self showToast:failure[@"msg"]];
            }
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.specialDict.allKeys.count == 0 || [self.specialDict[@"userId"] isEqualToString:@"13"]) {
        [self requestUserDetail];
    }
    [self handleUnreadMessage];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handle:) name:Delete_YELLOW_PIC object:nil];
    
}

- (void)handleUnreadMessage {
    __weak typeof(self) __weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        int count = [[RCIMClient sharedRCIMClient]getUnreadCount:self.displayConversationTypeArray];
        if (count>0) {
            [__weakSelf.tabBarController.viewControllers objectAtIndex:unreadCountTabIndex].tabBarItem.badgeValue =  [[NSString alloc]initWithFormat:@"%d",count];
        }else {
            [__weakSelf.tabBarController.viewControllers objectAtIndex:unreadCountTabIndex].tabBarItem.badgeValue = nil;
        }
        
    });
}

- (void)requestUserDetail {
   
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:self.targetId forKey:@"user_id"];
    [RequestUtils commonGetRequestUtils:dict bURL:@"user.detail" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable userDict) {
        
        self.currentModel = [HomeModel initWithDictionary:userDict];
        NSString *nickName = [userDict valueForKey:@"nick_name"];
        self.titleView.nickNameLabel.text = nickName;
        self.titleView.statusLabel.text = SPDStringWithKey([userDict[@"is_online"] boolValue] ? @"在线" : @"离线", nil);
        [self setAudioEnable:[userDict[@"audio_enable"] boolValue]];
        [self setVideoEnable:[userDict[@"video_enable"] boolValue]];
        [self setIsBlock:[userDict[@"is_block"] boolValue]];
        [self setIsFriend:[userDict[@"is_friend"] boolValue]];
        [self setIsFavorite:[userDict[@"is_favorite"] boolValue]];
        self.otherIsService = [userDict[@"is_service"] boolValue];
        self.isBlocked = [userDict[@"is_blocked"] boolValue];
        self.strangerMsgSwitch = [userDict[@"strangerMsgSwitch"] isEqualToString:@"on"];
        //更新于用户聊天的信息 如果用户更新头像的话会及时在与他聊天的 那一边显示
        RCUserInfo *userInfo=[[RCUserInfo alloc]initWithUserId:userDict[@"_id"] name:[userDict valueForKey:@"nick_name"] portrait:[NSString stringWithFormat:STATIC_DOMAIN_URL,userDict[@"avatar"]]];
        [[RCIM sharedRCIM]refreshUserInfoCache:userInfo withUserId:self.targetId];
        
        
        
        if (userDict[@"level"] != nil) {
            NSString * charmSign =userDict[@"level"][@"sign"];
            NSString *secretStr = [NSString stringWithFormat:@"_id=%@&user_level=%d&key=%@",userDict[@"_id"], [userDict[@"level"][@"user_level"] intValue],jianyue_secretKey];
            self.target_level = [secretStr.SHA256AndBase64 isEqualToString:charmSign] ?[userDict[@"level"][@"user_level"] intValue] : 0;
        }
        UIBarButtonItem * right = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ic_gerenzhuye_gengduo_hei"] style:UIBarButtonItemStylePlain target:self action:@selector(handleRightBarItemClicked)];
        if (![self.targetId isEqualToString:@"13"]) {
            self.navigationItem.rightBarButtonItem = right;
        }
        
        [self requestSysConfig];
        
    } bFailure:nil];
}

-(void)handle:(NSNotification *) notify{
    NSDictionary *dic = notify.userInfo;
    RCMessage *msgid  = (RCMessage*)dic[@"msg"];
    RCMessageModel *model = [RCMessageModel modelWithMessage:msgid];
    [self.conversationDataRepository removeObject:model];
    [self.conversationMessageCollectionView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:Delete_YELLOW_PIC object:nil];
}

- (void)requestSysConfig {
    NSDictionary *dic = @{@"ios_version": LOCAL_VERSION_SHORT};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"sys.config" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable dict) {
        NSDictionary *configDic = dict[@"config"];
        ZegoManager.chatroomLock = [configDic[@"chatroomLock"] isEqualToString:@"on"];
        
        if ([configDic[@"gift"] isEqualToString:@"on"] && ![self.targetId isEqualToString:@"13"]) {
            self.presentButton.hidden = NO;
        } else {
            self.presentButton.hidden = YES;
        }
       
    } bFailure:^(id  _Nullable failure) {

    }];
}

- (void)showGiftView {
    KKGiftGivingView *view = [[[NSBundle mainBundle] loadNibNamed:@"KKGiftGivingView" owner:self options:nil]firstObject];
    view.receiveGiftUserModel = self.currentModel;
    view.frame = self.view.bounds;
    view.delegate = self;
    [self.view addSubview:view];
}

#pragma mark -聊天点击礼物按钮

-(void)sendGift:(UIButton *)button {

    [self.chatSessionInputBarControl.inputTextView resignFirstResponder];
    
    [self showGiftView];
    
    //[MobClick event:@"clickMessagesGifts"];
    
}

#pragma mark - RONGCLOUD DELEGATE

//- (void)didJYReceiveCall {
//    [ZegoManager pauseAudio];
//}
//
//- (void)didJYFinishCall {
//    [ZegoManager resumeAudio];
//}
//
//- (void)callWillConnect{
//    isVideoStatus=YES;
//    //给视频页面发送礼物开关
//    BaseViewController *base=[BaseViewController new];
//    [base postGiftConfigToSingelCallVC];
//
//    [[NSNotificationCenter defaultCenter] postNotificationName:SPDLiveCallWillConnect object:nil];
//
//}
//
//- (void)callWillDisconnect{
//    isVideoStatus=NO;
//    [[NSNotificationCenter defaultCenter] postNotificationName:SPDLiveCallWillDisConnect object:nil    ];
//}

#pragma mark - resend方法

-(void)resendMessage:(RCMessageContent *)messageContent{
    
    [super resendMessage:messageContent];
}

-(void)showToast:(NSString *)str{
    MBProgressHUD * HUD = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    HUD.mode = MBProgressHUDModeText;
    HUD.margin = 10.f;
    HUD.yOffset = 15.f;
    HUD.removeFromSuperViewOnHide = YES;
    HUD.detailsLabelText =str;
    HUD.labelFont = [UIFont systemFontOfSize:30]; //Johnkui - added
    [HUD hide:YES afterDelay:1.0];
}

//点击金币充值---跳转到充值页面
-(void)clickedBillRechargeButton{
    [SPDCommonTool pushToRechageController:self];
}

- (void)didTapMessageCell:(RCMessageModel *)model{
    if ([ZegoKitManager sharedInstance].clan_id != nil && [model.content isKindOfClass:[RCVoiceMessage class]]) {
        // 后台有语音聊天室，并且点击的是播放语音按钮
        [LY_HUD showToastTips:@"请退出聊天室后使用此功能".localized];
        return;
    }
    if ([model.content isKindOfClass:[RCCallSummaryMessage class]]) {
        RCCallSummaryMessage *message = (RCCallSummaryMessage *)model.content;
        if (message.mediaType == RCCallMediaAudio) {
            [self clickedStartVOIPButton:message.mediaType];
        }else if (message.mediaType == RCCallMediaVideo){
            [self clickedStartVOIPButton:message.mediaType];
        }
    }else if ([model.content isKindOfClass:[SPDClanCallMessage class]]){
        SPDClanCallMessage *msg = (SPDClanCallMessage *)model.content;
        [self clickedClanCallMessageWithClanId:msg.clan_id andClan_name:msg.clan_name andLang:msg.lang];
    }else if ([model.content isKindOfClass:[PresentMessage class]]) {
        NSLog(@"tap receive message");
        SPDVehicleViewController * vehicle = [[SPDVehicleViewController alloc]init];
        vehicle.isMine = YES;
        vehicle.user_id = [SPDApiUser currentUser].userId;
        [self.navigationController pushViewController:vehicle animated:YES];
        
    }else if ([model.content isKindOfClass:[SPDPrivateMagicMessage class]]) {
        NSLog(@"tap magic message");
        [self showGiftView];
    } else if ([model.content isKindOfClass:[SPDFriendMessage class]]) {
        NSLog(@"tap mentar message");
        [self handleClickedFriendMessage:model];
    } else if ([model.content isKindOfClass:[SPDLotteryMessage class]]) {
//        NSString *url = [NSString stringWithFormat:@"https://%@share.famy.ly/view/andriodlotter.html?user_id=%@&lang=%@", DEV ? @"dev-" : @"", [SPDApiUser currentUser].userId, [SPDApiUser currentUser].lang];
//        [SPDCommonTool loadWebViewController:self title:@"" url:url];
        [self showToast:SPDStringWithKey(@"幸运数字功能优化中，敬请期待", nil)];
    } else if ([model.content isKindOfClass:[SPDGiftDoubleRewardMessage class]]) {
        [SPDCommonTool pushToRechageController:self];
    } else if ([model.content isKindOfClass:[SPDClanInviteMessage class]]) {
        [self handleClickeClanInviteMessage:model];
    } else if ([model.content isKindOfClass:[SPDLockExpireMessage class]]) {
        [self handleClickeLockExpireMessage:model];
    }else if ([model.content isKindOfClass:[LiveBeginNotificationMessage class]]){
        [self handleClickedLiveBeginNotifyMessage:model];
    }
    else{
        NSLog(@"NOT RCCallSummaryMessage");
        [super didTapMessageCell:model];
    }
}

- (void)handleClickedFriendMessage:(RCMessageModel *)model {
    SPDFriendMessage * msg = (SPDFriendMessage *)model.content;
    NSString * extra = model.extra ;
    if ([msg.type isEqualToString:@"request"]) {
        if ([extra isEqualToString:@"refuse"]) {
            [SPDCommonTool requestFriendPostWithUserId:self.targetId type:@"refuse" success:^(id  _Nullable suceess) {
                [self insertTipsMessageWith:SPDStringWithKey(@"消息已发送", nil)];
                [self.conversationMessageCollectionView reloadData];
            } failure:^(id  _Nullable failure) {
                
            }];
        } else if ([extra isEqualToString:@"accept"]) {
            [SPDCommonTool requestFriendPostWithUserId:self.targetId type:@"accept" success:^(id  _Nullable suceess) {
                [self insertTipsMessageWith:SPDStringWithKey(@"你们已成为好友，快去聊天互动吧", nil)];
                [self.conversationMessageCollectionView reloadData];
                self.isFriend = YES;
            } failure:^(id  _Nullable failure) {
                
            }];
        }
    }
}

- (void)handleClickeClanInviteMessage:(RCMessageModel *)model {
    SPDClanInviteMessage *message = (SPDClanInviteMessage *)model.content;
    if ([model.extra isEqualToString:@"refuse"]) {
        [self requestInviteJoinWitMessage:message type:@"refuse"];
        [self insertTipsMessageWith:SPDStringWithKey(@"你拒绝了对方的家族邀请", nil)];
    } else if ([model.extra isEqualToString:@"agree"]) {
        NSDictionary *dic = @{@"user_id": [SPDApiUser currentUser].userId, @"clan_id": message.clanId, @"inviter_id": message.sendUserId};
        [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"apply" isClan:YES bAnimated:YES bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
            if ([suceess[@"is_join"] boolValue]) {
                [self requestInviteJoinWitMessage:message type:@"agree"];
                [self insertTipsMessageWith:SPDStringWithKey(@"加入成功，24小时后可在家族签到领金币", nil)];
            } else {
                [self requestInviteJoinWitMessage:message type:@"apply"];
                [self insertTipsMessageWith:SPDStringWithKey(@"申请已提交，请耐心等待审核", nil)];
            }
        } bFailure:^(id  _Nullable failure) {
            if ([failure isKindOfClass:[NSDictionary class]]) {
                if ([failure[@"code"] integerValue] == 2005 || [failure[@"code"] integerValue] == 2006) {
                    [self insertTipsMessageWith:SPDStringWithKey(@"你已经有家族了，不能加入其他家族", nil)];
                } else if ([failure[@"code"] integerValue] == 2003) {
                    [self insertTipsMessageWith:SPDStringWithKey(@"家族已满员，可加入别的家族", nil)];
                } else {
                    [self showToast:failure[@"msg"]];
                }
            }
        }];
    }
}

- (void)handleClickeLockExpireMessage:(RCMessageModel *)model {
    if (ZegoManager.chatroomLock) {
        SPDLockExpireMessage *message = (SPDLockExpireMessage *)model.content;
        ChatroomLockViewController *vc = [[ChatroomLockViewController alloc] init];
        vc.clanId = message.clanId;
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        [self showToast:SPDStringWithKey(@"功能升级中", nil)];
    }
}

- (void)handleClickedLiveBeginNotifyMessage:(RCMessageModel *)model {
    LiveBeginNotificationMessage *message = (LiveBeginNotificationMessage *)model.content;
    if (message.liveId.notEmpty && message.anchorId.notEmpty) {
        LiveModel * liveModel = [LiveModel new];
        liveModel.liveId = message.liveId;
        liveModel.userId = message.senderUserInfo.userId;
        [ZegoManager joinLiveRoomWithliveInfo:liveModel isAnchor:NO];
    }
}

- (void)requestInviteJoinWitMessage:(SPDClanInviteMessage *)message type:(NSString *)type {
    NSDictionary *dic = @{@"clanId": message.clanId, @"sendUserId": [SPDApiUser currentUser].userId, @"receiverUserId": message.sendUserId, @"type": type};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"invite.join" isClan:YES bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        
    } bFailure:^(id  _Nullable failure) {
        
    }];
}


- (void)insertTipsMessageWith:(NSString *)message{
    //插入一条消息
    SPDTipsMessage * msg = [[SPDTipsMessage alloc]init];
    msg.message = message;
    RCMessage *hh =   [[RCIMClient sharedRCIMClient] insertOutgoingMessage:ConversationType_PRIVATE targetId:self.targetId sentStatus:SentStatus_SENT content:msg];
    [self appendAndDisplayMessage:hh];
}

#pragma mark - 点击聊天中的图片
- (void)presentImagePreviewController:(RCMessageModel *)model{
    RCImageSlideController *previewController =
    [[RCImageSlideController alloc] init];
    previewController.messageModel = model;
    
    UINavigationController *nav = [[UINavigationController alloc]
                                   initWithRootViewController:previewController];
    [self.navigationController presentViewController:nav
                                            animated:YES
                                          completion:nil];
}


#pragma mark - 点击Cell中头像的回调
- (void)didTapCellPortrait:(NSString *)userId{
    if (self.specialDict.allKeys.count == 0) {
        [SPDCommonTool pushToDetailTableViewController:self userId:userId];
    }
}

#pragma mark - RCPluginBoardViewDelegate  扩展功能板的点击回调

- (void)pluginBoardView:(RCPluginBoardView*)pluginBoardView clickedItemWithTag:(NSInteger)tag {
    if ([ZegoKitManager sharedInstance].clan_id != nil) {
        // 后台有语音聊天室
        if (tag == PLUGIN_BOARD_ITEM_VOIP_TAG || tag == PLUGIN_BOARD_ITEM_VIDEO_VOIP_TAG) {
            [LY_HUD showToastTips:@"请退出聊天室后使用此功能".localized];
            return;
        }
    }
    
    if (tag == PLUGIN_BOARD_ITEM_VOIP_TAG) {
        
        self.callType = @"audio";
        [self clickedStartVOIPButton:RCCallMediaAudio];
        //[MobClick event:@"chatAudioBtn"];
        
    }else if (tag == PLUGIN_BOARD_ITEM_VIDEO_VOIP_TAG) {
        
        self.callType = @"video";
        [self clickedStartVOIPButton:RCCallMediaVideo];
        //[MobClick event:@"chatVideoBtn"];
        
    }else if (tag == PLUGIN_BOARD_ITEM_CAMERA_TAG|| tag == PLUGIN_BOARD_ITEM_ALBUM_TAG){
        [super pluginBoardView:pluginBoardView clickedItemWithTag:tag];
//        //如果用户等级大于10 才可以发图片
//        NSInteger myCurrentLevel = [[[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"%@%@",MYCURRENTLEVEL,[SPDApiUser currentUser].userId]] integerValue];
//        if (myCurrentLevel >= 10 || [self.serviceTargetIdsArray containsObject:self.targetId ] || [self.serviceTargetIdsArray containsObject:[SPDApiUser currentUser].userId]) {
//            [super pluginBoardView:pluginBoardView clickedItemWithTag:tag];
//        }else{
//            [self showToast:SPDStringWithKey(@"亲,10级开放此权限,快去升级吧～",nil)];
//        }
    }
    else {
        [super pluginBoardView:pluginBoardView clickedItemWithTag:tag];
    }
}


- (void)clickedStartVOIPButton:(RCCallMediaType) mediaType {
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:self.targetId forKey:@"user_id"];
    [RequestUtils commonGetRequestUtils:dict bURL:@"user.detail" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable userDict) {
        
        [self setAudioEnable:[userDict[@"audio_enable"] boolValue]];
        [self setVideoEnable:[userDict[@"video_enable"] boolValue]];
        [self setIsBlock:[userDict[@"is_block"] boolValue]];
        [self setIsFriend:[userDict[@"is_friend"] boolValue]];
        [self setIsFavorite:[userDict[@"is_favorite"] boolValue]];
        if (mediaType == RCCallMediaAudio) {
            if (!self.audioEnable) {
                [self showTips:@"对方设置不可接听".localized];
                return;
            }
        }else{
            if (!self.videoEnable) {
                [self showTips:@"对方设置不可接听".localized];
                return;
            }
        }
        if (!self.isFriend) {
            [self showTips:@"添加好友后可以进行语音或视频聊天".localized];
            return;
        }
        [[RCCall sharedRCCall] startSingleCall:self.targetId mediaType:mediaType];

    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

/*!
 输入工具栏尺寸（高度）发生变化的回调
 
 @param chatInputBar 输入工具栏
 @param frame        输入工具栏最终需要显示的Frame
 */
- (void)chatInputBar:(RCChatSessionInputBarControl *)chatInputBar shouldChangeFrame:(CGRect)frame {
    CGRect collectionViewFrame = self.conversationMessageCollectionView.frame;
    collectionViewFrame.size.height = CGRectGetMinY(frame) - collectionViewFrame.origin.y;
    self.conversationMessageCollectionView.frame = collectionViewFrame;
    [self scrollToBottomAnimated:NO];

    CGRect presentButtonFrame = self.presentButton.frame;
    presentButtonFrame.origin.y = frame.origin.y - 10 - 43;
    self.presentButton.frame = presentButtonFrame;
}

#pragma mark - RONGCLOUD DELEGATE

//视频界面送礼物的方法
- (void)clickedVideoPresentButtonWithTargetID:(NSString *)targrtID{

//famy
    if (targrtID == self.targetId) {
        [self.chatSessionInputBarControl.inputTextView resignFirstResponder];
        [self showGiftView];
    }else{
        [self.baseView showPresentViewWithTargetId:targrtID];
    }
    if ([RCCall sharedRCCall].currentCallSession.mediaType == RCCallMediaVideo) {
        //[MobClick event:@"clickVideoGifts"];
    } else {
        //[MobClick event:@"clickVoiceGifts"];
    };
}


#pragma mark - action
- (void)openCamera {
    ZLCameraViewController* cameraVc = [[ZLCameraViewController alloc] init];
    cameraVc.maxCount = 1;
    cameraVc.callback = ^(NSArray* cameraImages){
        [self sendMessageWithImages:cameraImages];
    };
    [cameraVc showPickerVc:self];
}

- (void)sendMessageWithImages:(NSArray*)images {
    for (ZLCamera* image in images) {
        RCImageMessage* imageMessage = [RCImageMessage messageWithImage:image.photoImage];
        [self sendImageMessage:imageMessage pushContent:@"您有一条新的消息"];
    }
}


- (void)alertWindow:(NSString *)messageTitle showCancle:(BOOL)cancle action:(AlertAction)alertAction{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:messageTitle preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:SPDStringWithKey(@"确定", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                                            
                                                              alertAction();
                                                          }];
    
    if (cancle) {
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:SPDStringWithKey(@"取消", nil) style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancelAction];
    }
    
    [alert addAction:defaultAction];
    
    UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    alertWindow.rootViewController = [[UIViewController alloc] init];
    alertWindow.windowLevel = UIWindowLevelAlert + 1;
    [alertWindow makeKeyAndVisible];
    
    [alertWindow.rootViewController presentViewController:alert animated:YES completion:^{
        
    }];
    
}

#pragma mark - Setters & Getters

- (UIButton *)presentButton {
    if (!_presentButton) {
        _presentButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _presentButton.frame = CGRectMake(kScreenW - 20 - 43, self.chatSessionInputBarControl.frame.origin.y - 10 - 43, 43, 43);
        [_presentButton setImage:[UIImage imageNamed:@"ic_present_button"] forState:UIControlStateNormal];
        [_presentButton addTarget:self action:@selector(sendGift:) forControlEvents:UIControlEventTouchUpInside];
        _presentButton.hidden = YES;
        [self.view addSubview:_presentButton];
    }
    return _presentButton;
}

-(RCMessageBaseCell *)rcConversationCollectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    RCMessageModel *model = self.conversationDataRepository[indexPath.row];
    RCMessageBaseCell * cell = [[RCMessageBaseCell alloc]init];
    if ([model.content isKindOfClass:[PresentMessage class]]) {
        NSString * cellIndentifier=@"PresentMessageCell";
        PresentMessageCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIndentifier           forIndexPath:indexPath];
        [cell setDataModel:model];
        return cell;
    }else if([model.content isKindOfClass:[SPDClanCallMessage class]]){
        NSString * cellIndentifier=@"SPDClanCallMessageCell";
        SPDClanCallMessageCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIndentifier           forIndexPath:indexPath];
        [cell setDataModel:model];
        cell.delegate = self;
        return cell;
    }else if ([model.content isKindOfClass:[SPDPrivateMagicMessage class]]){
        SPDPrivateMagicCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([SPDPrivateMagicCell class])           forIndexPath:indexPath];
        [cell setDataModel:model];
        cell.delegate = self;
        return cell;
    } else if ([model.content isKindOfClass:[SPDTipsMessage class]]) {
        SPDTipsMessageCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([SPDTipsMessageCell class])           forIndexPath:indexPath];
        [cell setDataModel:model];
        return cell;
    }
    return cell;
}


#pragma mark - clancall delegate

-(void)clickedClanCallMessageWithClanId:(NSString *)clan_id andClan_name:(NSString *)clan_name andLang:(NSString *)lang
{
    if ([[SPDApiUser currentUser].lang isEqualToString:lang]) {
        [ZegoManager joinRoomFrom:self clanId:clan_id];
    } else {
        [self showToast:SPDStringWithKey(@"对方和你不在同一语言区，不能跨区进家族", nil)];
    }
}

- (NSArray *)serviceTargetIdsArray {
    if (!_serviceTargetIdsArray) {
        _serviceTargetIdsArray = @[@"1085",@"1965",@"23573",@"201708",@"350903",@"361789",@"627182",@"31395"];
    }
    return _serviceTargetIdsArray;
}

- (void)requestMyInfoDetail {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.info.detail" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        self.isVip = [suceess[@"is_vip"] boolValue];
        self.isSevenVip = [suceess[@"is_seven_vip"] boolValue];
        self.selfIsService = [suceess[@"is_service"] boolValue];
    } bFailure:nil];
}

- (void)requestApOrderOpposite {
//    NSDictionary *params = @{@"userId": self.targetId};
//    [RequestUtils commonGetRequestUtils:[params mutableCopy] bURL:@"ap.order.opposite" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
//        if ([suceess[@"order"] notEmpty]) {
//            self.orderProgressView.frame = CGRectMake(0, NavHeight, kScreenW, 87);
//            self.orderProgressView.orderModel = [APOrderModel initWithDictionary:suceess[@"order"]];
//            CGRect collectionViewFrame = self.conversationMessageCollectionView.frame;
//            collectionViewFrame.size.height -= (NavHeight + 87 - collectionViewFrame.origin.y);
//            collectionViewFrame.origin.y = NavHeight + 87;
//            self.conversationMessageCollectionView.frame = collectionViewFrame;
//            [self scrollToBottomAnimated:NO];
//        } else if ([suceess[@"game"] notEmpty]) {
//            self.orderProgressView.frame = CGRectMake(0, NavHeight, kScreenW, 55);
//            self.orderProgressView.itemMode = [APItemModel initWithDictionary:suceess[@"game"]];
//            CGRect collectionViewFrame = self.conversationMessageCollectionView.frame;
//            collectionViewFrame.size.height -= (NavHeight + 55 - collectionViewFrame.origin.y);
//            collectionViewFrame.origin.y = NavHeight + 55;
//            self.conversationMessageCollectionView.frame = collectionViewFrame;
//            [self scrollToBottomAnimated:NO];
//        }
//    } bFailure:^(id  _Nullable failure) {
//
//    }];
}

- (SPDUserOnlineTitleView *)titleView {
    if (!_titleView) {
        _titleView = [[[NSBundle mainBundle] loadNibNamed:@"SPDUserOnlineTitleView" owner:self options:nil] firstObject];
    }
    return _titleView;
}


#pragma mark - 私聊限制

//- (RCMessage *)willAppendAndDisplayMessage:(RCMessage *)message {
//    if ([message.content isKindOfClass:[SPDTipsMessage class]]) {
//        SPDTipsMessage *tipsMessage = (SPDTipsMessage *)message.content;
//        if (tipsMessage.orderId.notEmpty) {
//            if (([tipsMessage.type isEqualToString:@"await"] ||
//                 [tipsMessage.type isEqualToString:@"agree"] ||
//                 [tipsMessage.type isEqualToString:@"process"] ||
//                 [tipsMessage.type isEqualToString:@"done"]) &&
//                _orderProgressView.orderModel &&
//                [_orderProgressView.orderModel.orderId isEqualToString:tipsMessage.orderId]) {
//                self.orderProgressView.orderModel.status = tipsMessage.type;
//                self.orderProgressView.orderModel = self.orderProgressView.orderModel;
//            } else {
//                [self requestApOrderOpposite];
//            }
//        }
//    }
//    return message;
//}

- (RCMessageContent *)willSendMessage:(RCMessageContent *)messageContent {
    if ([messageContent isKindOfClass:[RCTextMessage class]]
        || [messageContent isKindOfClass:[RCImageMessage class]]
        || [messageContent isKindOfClass:[RCVoiceMessage class]]) {
        
        if (self.selfIsService || self.otherIsService) {
            return messageContent;
        }
        if (self.isBlock) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showToast:SPDStringWithKey(@"对方已被拉黑不能接收消息", nil)];
            });
            return nil;
        }
        
        if ([messageContent isKindOfClass:[RCImageMessage class]]) {
            if (self.isFriend) {
                return messageContent;
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self showToast:SPDStringWithKey(@"成为对方好友，即可发送图片", nil)];
                });
                return nil;
            }
        } else {
            if (self.isFriend || !self.strangerMsgSwitch) {
                return messageContent;
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.chatSessionInputBarControl resetToDefaultStatus];
                    [SPDCommonTool presentCommonController:self title:SPDStringWithKey(@"成为好友可发私信消息", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"知道了", nil) cancelAction:^{
                        
                    } confirmTitle:SPDStringWithKey(@"加好友", nil) confirmAction:^{
                        [self handleFriendActionType:@"request"];
                    }];
                });
                return nil;
            }
        }
    } else {
        return messageContent;
    }
}

#pragma mark KKGiftGivingViewDelegate

- (void)didClickRechargeButton {
    [SPDCommonTool pushToRechageController:self];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
