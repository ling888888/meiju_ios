//
//  ChatListViewController.m
//  SimpleDate
//
//  Created by xu.juvenile on 2016/5/11.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "ChatListViewController.h"
#import "SPDConversationViewController.h"
#import "ChatListTitleView.h"
#import "SPDNewFriendsViewController.h"
#import "RCConversationCell+LY.h"
#import <ElvaChatServiceSDK/ElvaChatServiceSDK.h>

@interface ChatListViewController ()<ChatListTitleViewDelegate>

@property (nonatomic, strong) UIImageView *emptyImageView;
@property (nonatomic, strong) ChatListTitleView * titleView;
@property (nonatomic, copy) NSArray * dataArray;
@property (nonatomic, copy) NSArray *livingUserIdArray;
@property (nonatomic, strong) NSMutableArray * topConversationArray;
@end

@implementation ChatListViewController

- (NSMutableArray *)topConversationArray {
    if (!_topConversationArray) {
        _topConversationArray = [NSMutableArray new];
    }
    return _topConversationArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    self.conversationListTableView.backgroundColor = [UIColor whiteColor];
    self.conversationListTableView.bounces = YES;
    self.conversationListTableView.tableFooterView = [[UIView alloc] init];
    self.emptyConversationView = self.emptyImageView;
    self.cellBackgroundColor = [UIColor whiteColor];
    [self setDisplayConversationTypes:@[@(ConversationType_PRIVATE)]];
    [self setConversationAvatarStyle:RC_USER_AVATAR_CYCLE];
    
    self.titleView = [[ChatListTitleView alloc]initWithFrame:CGRectMake(0, 0, kScreenW, 100)];
    self.titleView.dataArray = [NSArray arrayWithArray:self.dataArray];
    self.titleView.delegate = self;
    [self.view addSubview:self.titleView];
    self.titleView.entryType = self.entryType;
    
    CGRect listFrame = self.conversationListTableView.frame;
    listFrame.origin.y = CGRectGetMaxY(self.titleView.frame);
    listFrame.size.height = self.view.bounds.size.height -CGRectGetHeight(self.titleView.frame) - naviBarHeight - TabBarHeight;
    self.conversationListTableView.frame = listFrame;
    
    self.emptyConversationView = self.emptyImageView;
        
}



// 全部设置已读
- (void)clearUnreadMessage {
    for (RCConversation *conversation in self.conversationListDataSource) {
        [[RCIMClient sharedRCIMClient] clearMessagesUnreadStatus:conversation.conversationType targetId:conversation.targetId];
    }
    for (RCConversation *conversation in self.topConversationArray) {
        [[RCIMClient sharedRCIMClient] clearMessagesUnreadStatus:conversation.conversationType targetId:conversation.targetId];
    }
    [self.titleView updateUnreadMessage];
    [self refreshConversationTableViewIfNeeded];
    [self notifyUpdateUnreadMessageCount];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"HaveReadAllMessage" object:nil];
}

// 因为要改变view的frame 由于继承关系 需要重写这个方法
- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.emptyConversationView.frame = self.conversationListTableView.bounds;
    
    CGRect listFrame = self.conversationListTableView.frame;
    listFrame.origin.y = CGRectGetMaxY(self.titleView.frame);
    listFrame.size.height = kScreenH -CGRectGetHeight(self.titleView.frame) - naviBarHeight - TabBarHeight;
    self.conversationListTableView.frame = listFrame;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.titleView updateUnreadMessage];
    [self notifyUpdateUnreadMessageCount];
    
    NSArray * testgg = [[RCIMClient sharedRCIMClient] getConversationList:@[@(ConversationType_PRIVATE),@(ConversationType_DISCUSSION),@(ConversationType_GROUP)]];
    
    NSMutableString *userIdStr = [NSMutableString string];
    for (RCConversation *conversation in testgg) {
        if (conversation == testgg.lastObject) {
            [userIdStr appendFormat:@"%@", conversation.targetId];
        } else {
            [userIdStr appendFormat:@"%@,", conversation.targetId];
        }
    }
    NSLog(@"userIdStr:%@", userIdStr);
    
    NSDictionary *params = @{@"userIds": userIdStr};
    __weak typeof(self) weakSelf = self;
    [LY_Network postRequestWithURL:LY_Api.live_rc_user parameters:params success:^(id  _Nullable response) {
        weakSelf.livingUserIdArray = response[@"list"];
        
        [weakSelf refreshConversationTableViewIfNeeded];

    } failture:^(NSError * _Nullable error) {


    }];
}

#pragma mark - Event response

- (void)clickRightBarButtonItem:(UIBarButtonItem *)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:SPDStringWithKey(@"温馨提示", nil) message:SPDStringWithKey(@"确定要清空当前聊天记录吗？", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:SPDStringWithKey(@"确定", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * data = [self.conversationListDataSource copy];
        for (RCConversationModel * model in data) {
            [[RCIMClient sharedRCIMClient] removeConversation:ConversationType_PRIVATE targetId:model.targetId];
        }
        [self refreshConversationTableViewIfNeeded];
        [self notifyUpdateUnreadMessageCount];
    }];
    [alertController addAction:confirmAction];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:SPDStringWithKey(@"取消", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }];
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - rongyun

- (void)willDisplayConversationTableCell:(RCConversationBaseCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    BOOL isLiving = [self.livingUserIdArray containsObject:cell.model.targetId];
    RCConversationCell *conversationCell = (RCConversationCell *)cell;
    
    if (isLiving) {
        [conversationCell showLivingTag];
    } else {
        [conversationCell hiddenLivingTag];
    }
}

- (void)onSelectedTableRow:(RCConversationModelType)conversationModelType
         conversationModel:(RCConversationModel *)model
               atIndexPath:(NSIndexPath *)indexPath {
    
    if ([self.entryType isEqualToString:@"chatroom"]) {
        //[MobClick event:@"FM7_27_5"];
    }
    
    SPDConversationViewController *vc = [[SPDConversationViewController alloc] initWithConversationType:ConversationType_PRIVATE targetId:model.targetId];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)notifyUpdateUnreadMessageCount {
    __weak typeof(self) weakSelf = self;
    int unreadCount = [[RCIMClient sharedRCIMClient] getUnreadCount:self.displayConversationTypeArray];
    dispatch_async(dispatch_get_main_queue(), ^{
        if (unreadCount > 0) {
            [weakSelf.tabBarController.viewControllers objectAtIndex:unreadCountTabIndex].tabBarItem.badgeValue = [NSString stringWithFormat:@"%d", unreadCount];
        } else {
            [weakSelf.tabBarController.viewControllers objectAtIndex:unreadCountTabIndex].tabBarItem.badgeValue = nil;
        }
    });
    
}

- (void)didReceiveMessageNotification:(NSNotification *)notification {
    [super didReceiveMessageNotification:notification];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.titleView updateUnreadMessage];
    });
}

- (NSMutableArray *)willReloadTableData:(NSMutableArray *)dataSource {
    NSArray * userIds = [self.dataArray valueForKeyPath:@"userId"];
    for (RCConversationModel * model in [dataSource reverseObjectEnumerator]) {
        if ([userIds containsObject:model.targetId]) {
            [self.topConversationArray addObject:model];
            [dataSource removeObject:model];
        }
    }
    return dataSource;
}

#pragma mark - ChatListTitleViewDelegate

- (void)chatListTitleView:(ChatListTitleView *)titleView andSelectedItemIndex:(NSInteger )index {
    if ([self.entryType isEqualToString:@"chatroom"] && index == 3) {
        // 新的朋友
        SPDNewFriendsViewController *vc = [[SPDNewFriendsViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
        
        return;
    }
    
    if ([self.entryType isEqualToString:@"chatroom"]) {
        switch (index) {
            case 0: {
                //[MobClick event:@"FM7_27_1"];
                break;
            }
            case 1: {
                //[MobClick event:@"FM7_27_2"];
                break;
            }
            case 2: {
                //[MobClick event:@"FM7_27_3"];
                break;
            }
            case 3: {
                //[MobClick event:@"FM7_27_4"];
                break;
            }
            default:
                break;
        }
    }
    
    if (index == 3) {
        NSString *userName = [SPDApiUser currentUser].nickName;
        NSString *userId = [SPDApiUser currentUser].userId;
        NSString *serverId = userId;
        NSString * showConversationFlag = @"1";
        
        NSString *currentLanguage = [[SPDLanguageTool sharedInstance] currentLanguage];
        NSLog(@"currentLanguage:%@", currentLanguage);
        [ECServiceSdk setSDKLanguage:currentLanguage];
        
        [ECServiceSdk showElva:userName
                     PlayerUid:userId
                      ServerId:serverId
                 PlayerParseId:@""
        PlayershowConversationFlag:showConversationFlag];
    } else {
        NSString * targetId = self.dataArray[index][@"userId"];
        SPDConversationViewController *vc = [[SPDConversationViewController alloc] initWithConversationType:[targetId isEqualToString:@"2"] ? ConversationType_SYSTEM:ConversationType_PRIVATE targetId:targetId];
        vc.specialDict = self.dataArray[index];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

// MARK: - JXPagerViewListViewDelegate

- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return self.conversationListTableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    
}

#pragma mark - setters & Getters

- (UIImageView *)emptyImageView {
    if (!_emptyImageView) {
        _emptyImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH - NavHeight - TabBarHeight - CGRectGetMaxY(self.titleView.frame))];
        _emptyImageView.contentMode = UIViewContentModeCenter;
        _emptyImageView.clipsToBounds = YES;
        _emptyImageView.image = [UIImage imageNamed:@"img_kongyemian_juijinliaotian"];
    }
    return _emptyImageView;
}

- (NSArray *)dataArray {
    if ( nil == _dataArray ) {
        if ([self.entryType isEqualToString:@"chatroom"]) {
            _dataArray = @[@{
                @"title": @"系统通知",
                @"image": @"ic_xiaoxi_xitongtongzhi",
                @"userId": @"1"
                },
            @{
                @"title": @"官方公告",
                @"image": @"ic_xiaoxi_guanfanggonggao",
                @"userId": @"2"
                },
            @{
                @"title": @"家族召唤",
                @"image": @"ic_xiaoxi_jiazuzhaohuan",
                @"userId": @"3"
                },
            @{
                @"title": @"新的朋友",
                @"image": @"ic_haoyou_xindepengyou",
                @"userId": @"13"
                }];
        } else {
            _dataArray = @[@{
                @"title": @"系统通知",
                @"image": @"ic_xiaoxi_xitongtongzhi",
                @"userId": @"1"
                },
            @{
                @"title": @"官方公告",
                @"image": @"ic_xiaoxi_guanfanggonggao",
                @"userId": @"2"
                },
            @{
                @"title": @"家族召唤",
                @"image": @"ic_xiaoxi_jiazuzhaohuan",
                @"userId": @"3"
                },
            @{
                @"title": @"联系客服",
                @"image": @"ic_xiaoxi_lianxikefu",
                @"userId": @"13"
                }];
        }
    }
    return _dataArray;
}

@end
