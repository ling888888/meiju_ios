//
//  JYConversationViewController.h
//  SimpleDate
//
//  Created by 周佳磊 on 16/1/18.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import <RongIMKit/RongIMKit.h>
#import "SPDBannedSpeakMessage.h"

extern BOOL isVideoStatus;
extern BOOL isInPrivateConversation;

@interface SPDConversationViewController : RCConversationViewController

@property (nonatomic,copy) NSDictionary *specialDict; // 系统通知 官方公告 家族召唤 联系客服

@property (nonatomic,assign) BOOL audioEnable;
@property (nonatomic,assign) BOOL videoEnable;

@end
