//
//  SPDSearchUserViewController.h
//  SimpleDate
//
//  Created by 李楠 on 17/8/16.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "LY_BaseViewController.h"

@interface SPDSearchUserViewController : LY_BaseViewController

@property (nonatomic, assign) BOOL showSeachBarOnNavigationBar;

@end
