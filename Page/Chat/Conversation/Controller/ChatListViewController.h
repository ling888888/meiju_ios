//
//  ChatListViewController.h
//  SimpleDate
//
//  Created by xu.juvenile on 2016/5/11.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import <RongIMKit/RongIMKit.h>
#import "JXPagerView.h"

@interface ChatListViewController : RCConversationListViewController <JXPagerViewListViewDelegate>

@property(nonatomic, copy) NSString *entryType;

- (void)clickRightBarButtonItem:(UIBarButtonItem *)sender;

- (void)clearUnreadMessage;

@end
