//
//  PresentMessage.m
//  SimpleDate
//
//  Created by 侯玲 on 16/9/20.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "PresentMessage.h"

@implementation PresentMessage


///初始化
+ (instancetype)messageWithContent:(NSString *)content andGiftName:(NSString *)giftName andImageName:(NSString *)imageName andGoldNum:(NSString *)goldNum andCharamScore:(NSString *)charamScore{
    PresentMessage *text = [[PresentMessage alloc] init];
    if (text) {
        text.content = content;
        text.giftName = giftName;
        text.imageName = imageName;
        text.goldNum = goldNum;
        text.charamScore = charamScore;
    }
    return text;
}

+ (instancetype)messageWithContent:(NSString *)content andVehicle_id:(NSString *)vehicle_id andGiftName:(NSString *)giftName andImageName:(NSString *)imageName andGoldNum:(NSString *)goldNum andCharamScore:(NSString *)charamScore andType:(NSString *)type andExpire_time :(NSString *)expire_time {
    PresentMessage *text = [[PresentMessage alloc] init];
    if (text) {
        text.content = content;
        text.giftName = giftName;
        text.imageName = imageName;
        text.goldNum = goldNum;
        text.charamScore = charamScore;
        text.type = type;
        text.expire_time = expire_time;
        text._id = vehicle_id;
    }
    return text;
}


///消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return (MessagePersistent_ISPERSISTED | MessagePersistent_ISCOUNTED);
}

/// NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        [self hl_decodeWithDeCoder:aDecoder];
    }
    return self;
}


/// NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [self hl_encodeWithCoder:aCoder];
}


///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [NSMutableDictionary dictionary];
    [dataDict setObject:self.content?:@"" forKey:@"content"];
    [dataDict setObject:self.imageName?:@"" forKey:@"imageName"];
    [dataDict setObject:self.goldNum?:@"" forKey:@"goldNum"];
    [dataDict setObject:self.charamScore?:@"" forKey:@"charamScore"];
    [dataDict setObject:self.giftName?:@"" forKey:@"giftName"];
    [dataDict setObject:self._id?:@"" forKey:@"_id"];
    if (self.extra) {
        [dataDict setObject:self.extra ?:@""forKey:@"extra"];
    }
    if (self.type) {
        [dataDict setObject:self.type?:@"" forKey:@"type"];
    }
    if (self.expire_time) {
        [dataDict setObject:self.expire_time ?:@""forKey:@"expire_time"];
    }
    [dataDict setObject:self.num ? : @"1" forKey:@"num"];
    [dataDict setObject:@(self.currencyType) forKey:@"currencyType"];
    
    if (self.senderUserInfo) {
        NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc] init];
        if (self.senderUserInfo.name) {
            [userInfoDic setObject:self.senderUserInfo.name
                 forKeyedSubscript:@"name"];
        }
        if (self.senderUserInfo.portraitUri) {
            [userInfoDic setObject:self.senderUserInfo.portraitUri
                 forKeyedSubscript:@"icon"];
        }
        if (self.senderUserInfo.userId) {
            [userInfoDic setObject:self.senderUserInfo.userId
                 forKeyedSubscript:@"id"];
        }
        [dataDict setObject:userInfoDic forKey:@"user"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        
        if (dictionary) {
            self.content = dictionary[@"content"]?:@"";
            self.extra = dictionary[@"extra"]?:@"";
            self.imageName=dictionary[@"imageName"]?:@"";
            self.goldNum=dictionary[@"goldNum"]?:@"";
            self.charamScore=dictionary[@"charamScore"]?:@"";
            self.giftName=dictionary[@"giftName"]?:@"";
            self._id=dictionary[@"_id"]?:@"";
            if (dictionary[@"type"]) {
                self.type = dictionary[@"type"]?:@"";
            }
            if (dictionary[@"expire_time"]) {
                self.expire_time = dictionary[@"expire_time"]?:@"";
            }
            self.num = dictionary[@"num"] ? : @"1";
            self.currencyType = dictionary[@"currencyType"] ? [dictionary[@"currencyType"] integerValue] : 0;
            NSDictionary *userinfoDic = dictionary[@"user"];
            [self decodeUserInfo:userinfoDic];
        }
    }
}
/// 会话列表中显示的摘要
- (NSString *)conversationDigest {
    return SPDStringWithKey(self.content, nil);
}

///消息的类型名
+ (NSString *)getObjectName {
    return RCDTestMessageTypeIdentifier;
}


@end
