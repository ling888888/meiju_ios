//
//  SPDSPDMagicModel.h
//  SimpleDate
//
//  Created by Monkey on 2017/9/28.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "CommonModel.h"

@interface SPDMagicModel : CommonModel
@property (nonatomic,copy)NSString * _id;
@property (nonatomic,copy)NSString * name;
@property (nonatomic,copy)NSString * cover;
@property (nonatomic,copy)NSNumber * num;
@property (nonatomic,copy)NSNumber * cost;
@property (nonatomic,copy)NSNumber * magic_level; //如果没有默认一级别
@property (nonatomic,copy)NSString * info;
@property (nonatomic,copy)NSString * level_up_info;
@property (nonatomic,copy)NSNumber * now_exp;
@property (nonatomic,copy)NSNumber * level_exp;
@property (nonatomic,copy)NSNumber * all_exp;
@property (nonatomic,copy)NSString * effect; // 魔法效果 eg：禁言XXS 魅力值将-XX
@property (nonatomic,copy)NSNumber * value;
@property (nonatomic,copy)NSNumber * expire_time; // 魔法效果有效期
@property (nonatomic,copy)NSString * expire_time_str;
@property (nonatomic,assign)BOOL  is_buff; // 区别是增益魔法还是减益魔法, yes 增益, no 减益
@property (nonatomic,copy)NSString * color; // 成功率颜色
@property (nonatomic, copy) NSNumber *current_effect;
@property (nonatomic, copy) NSNumber *next_effect;
@property (nonatomic, copy) NSString *popdesc;// topbar描述

//对方向自己使用魔法：
//1.    XXXXX向你使用“XX魔法”成功，你将被禁言XXS
//2.    XXXXX向你使用“XX魔法”成功，你将被封麦XXS
//3.    XXXXX向你使用“XX魔法”成功，你将XXS内无法进入被踢出的房间
//4.    XXXXX向你使用“XX魔法”成功，你的魅力值将-XX
//5.    XXXXX向你使用“XX魔法”成功，你的经验值将-XX
//6.    XXXXX向你使用“XX魔法”但是失败了
//自己向对方使用魔法：
//1.    你向XXXX使用”XX魔法“成功，TA将被禁言XXS
//2.    你向XXXX使用”XX魔法“成功，TA将被封麦XXS
//3.    你向XXXX使用”XX魔法“成功，TA将在XXS内无法进入被踢出的家族
//4.    你向XXXX使用”XX魔法“成功，TA的魅力值将-XX
//5.    你向XXXX使用”XX魔法“成功，TA的魅力值将+XX
//6.    你向XXXX使用”XX魔法“成功，TA的经验值将+XX
//7.    你向XXXX使用“XX魔法”但是失败了

@end
