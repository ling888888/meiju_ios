//
//  SPDClanInviteMessage.m
//  SimpleDate
//
//  Created by 李楠 on 2018/10/16.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDClanInviteMessage.h"
#import "NSObject+Coding.h"

@implementation SPDClanInviteMessage

///消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return MessagePersistent_ISCOUNTED;
}

/// NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        [self hl_decodeWithDeCoder:aDecoder];
    }
    return self;
}

/// NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [self hl_encodeWithCoder:aCoder];
}

///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [NSMutableDictionary dictionary];
    if (self.clanId) {
        [dataDict setObject:self.clanId forKey:@"clanId"];
    }
    if (self.sendUserId) {
        [dataDict setObject:self.sendUserId forKey:@"sendUserId"];
    }
    if (self.receiverUserId) {
        [dataDict setObject:self.receiverUserId forKey:@"receiverUserId"];
    }
    if (self.type) {
        [dataDict setObject:self.type forKey:@"type"];
    }
    if (self.senderUserInfo) {
        NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc] init];
        if (self.senderUserInfo.name) {
            [userInfoDic setObject:self.senderUserInfo.name
                 forKeyedSubscript:@"name"];
        }
        if (self.senderUserInfo.portraitUri) {
            [userInfoDic setObject:self.senderUserInfo.portraitUri
                 forKeyedSubscript:@"icon"];
        }
        if (self.senderUserInfo.userId) {
            [userInfoDic setObject:self.senderUserInfo.userId
                 forKeyedSubscript:@"id"];
        }
        [dataDict setObject:userInfoDic forKey:@"user"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

/// 将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        
        if (dictionary) {
            self.clanId = dictionary[@"clanId"];
            self.sendUserId = dictionary[@"sendUserId"];
            self.receiverUserId = dictionary[@"receiverUserId"];
            self.type = dictionary[@"type"];
            NSDictionary *userinfoDic = dictionary[@"user"];
            [self decodeUserInfo:userinfoDic];
        }
    }
}

/// 会话列表中显示的摘要
- (NSString *)conversationDigest {
    if ([self.type isEqualToString:@"invite"]) {
        return SPDStringWithKey(@"快来加入我的家族一起签到领金币吧", nil);
    } else if ([self.type isEqualToString:@"refuse"]) {
        return SPDStringWithKey(@"不好意思，暂时不想加入家族", nil);
    } else if ([self.type isEqualToString:@"agree"]) {
        return SPDStringWithKey(@"我已经加入你的家族，快带我一起玩吧", nil);
    } else if ([self.type isEqualToString:@"apply"]) {
        return SPDStringWithKey(@"已同意加入家族，请通知管理员审核", nil);
    } else {
        return @"";
    }
}

/// 消息的类型名
+ (NSString *)getObjectName {
    return @"SPD:ClanInviteMsg";
}

@end
