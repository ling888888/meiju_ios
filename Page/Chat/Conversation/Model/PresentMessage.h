//
//  PresentMessage.h
//  SimpleDate
//
//  Created by 侯玲 on 16/9/20.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>
#import "NSObject+Coding.h"

#define RCDTestMessageTypeIdentifier @"presentMsg"


@interface PresentMessage : RCMessageContent<RCMessageCoding, RCMessagePersistentCompatible, RCMessageContentView>

/*!
 测试消息的内容
 */
@property(nonatomic, strong) NSString *content;

/*!
 测试消息的附加信息
 */
@property(nonatomic, strong) NSString *extra;

/*!
 初始化测试消息
 
 @param content 文本内容
 @return        测试消息对象
 */
+ (instancetype)messageWithContent:(NSString *)content andGiftName:(NSString *)giftName andImageName:(NSString *)imageName andGoldNum:(NSString *)goldNum andCharamScore:(NSString *)charamScore;

@property(nonatomic,copy)NSString *_id;
@property(nonatomic,copy)NSString *imageName;//图片地址
@property(nonatomic,copy)NSString *giftName;//礼物名称
@property(nonatomic,copy)NSString *goldNum;
@property(nonatomic,copy)NSString *charamScore;
@property (nonatomic, copy) NSString *num;
@property (nonatomic, assign) NSInteger currencyType; // 0 金币, 1 钻石

+ (instancetype)messageWithContent:(NSString *)content andVehicle_id:(NSString *)vehicle_id andGiftName:(NSString *)giftName andImageName:(NSString *)imageName andGoldNum:(NSString *)goldNum andCharamScore:(NSString *)charamScore andType:(NSString *)type andExpire_time :(NSString *)expire_time ;

//vehicle
@property(nonatomic,copy)NSString *type; //vehicle 是代表是座驾消息 nil 代表是礼物消息（高低版本都是nil）
@property(nonatomic,copy)NSString *expire_time; //座驾过期时间


@end
