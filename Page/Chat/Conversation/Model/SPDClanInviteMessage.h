//
//  SPDClanInviteMessage.h
//  SimpleDate
//
//  Created by 李楠 on 2018/10/16.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

@interface SPDClanInviteMessage : RCMessageContent

@property (nonatomic, strong) NSString *clanId;
@property (nonatomic, strong) NSString *sendUserId;
@property (nonatomic, strong) NSString *receiverUserId;
@property (nonatomic, strong) NSString *type; // invite, agree, refuse, apply

@end
