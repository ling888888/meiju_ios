//
//  SPDLockExpireMessage.h
//  SimpleDate
//
//  Created by 李楠 on 2018/11/20.
//  Copyright © 2018 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

NS_ASSUME_NONNULL_BEGIN

@interface SPDLockExpireMessage : RCMessageContent

@property (nonatomic, copy) NSString *clanId;

@end

NS_ASSUME_NONNULL_END
