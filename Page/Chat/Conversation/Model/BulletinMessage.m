//
//  BulletinMessage.m
//  SimpleDate
//
//  Created by ling Hou on 2019/12/12.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "BulletinMessage.h"
#import "NSObject+Coding.h"

@implementation BulletinMessage

+ (RCMessagePersistent)persistentFlag {
    return MessagePersistent_ISCOUNTED;
}

/// NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        [self hl_decodeWithDeCoder:aDecoder];
    }
    return self;
}

/// NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [self hl_encodeWithCoder:aCoder];
}

///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [NSMutableDictionary dictionary];
    if (self._id) {
        [dataDict setObject:self._id forKey:@"id"];
    }
    [dataDict setObject:self.title forKey:@"title"];
    [dataDict setObject:self.image_url forKey:@"image_url"];
    [dataDict setObject:self.action_url forKey:@"action_url"];
    [dataDict setObject:self.begin_time forKey:@"begin_time"];
    [dataDict setObject:self.end_time forKey:@"end_time"];
    [dataDict setObject:self.lang forKey:@"lang"];

    if (self.senderUserInfo) {
        NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc] init];
        if (self.senderUserInfo.name) {
            [userInfoDic setObject:self.senderUserInfo.name
                 forKeyedSubscript:@"name"];
        }
        if (self.senderUserInfo.portraitUri) {
            [userInfoDic setObject:self.senderUserInfo.portraitUri
                 forKeyedSubscript:@"icon"];
        }
        if (self.senderUserInfo.userId) {
            [userInfoDic setObject:self.senderUserInfo.userId
                 forKeyedSubscript:@"id"];
        }
        [dataDict setObject:userInfoDic forKey:@"user"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

/// 将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        
        if (dictionary) {
            self._id = dictionary[@"id"];
            self.title = dictionary[@"title"];
            self.image_url = dictionary[@"image_url"];
            self.action_url = dictionary[@"action_url"];
            self.begin_time = dictionary[@"begin_time"];
            self.end_time = dictionary[@"end_time"];
            self.lang = dictionary[@"lang"];
            NSDictionary *userinfoDic = dictionary[@"user"];
            [self decodeUserInfo:userinfoDic];
        }
    }
}

/// 消息的类型名
+ (NSString *)getObjectName {
    return @"SPD:Bulletin";
}

@end
