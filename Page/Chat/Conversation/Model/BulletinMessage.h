//
//  BulletinMessage.h
//  SimpleDate
//
//  Created by ling Hou on 2019/12/12.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

NS_ASSUME_NONNULL_BEGIN

@interface BulletinMessage : RCMessageContent

@property (nonatomic, strong) NSString *_id;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *image_url;
@property (nonatomic, strong) NSString *action_url;
@property (nonatomic, strong) NSString *begin_time;
@property (nonatomic, strong) NSString *end_time;
@property (nonatomic, strong) NSString *lang;


@end

NS_ASSUME_NONNULL_END
