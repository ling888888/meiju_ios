//
//  SPDSPDMagicModel.m
//  SimpleDate
//
//  Created by Monkey on 2017/9/28.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDMagicModel.h"

@implementation SPDMagicModel

- (void)setExpire_time:(NSNumber *)expire_time {
    _expire_time = expire_time;
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setTimeZone:[NSTimeZone localTimeZone]];
    [format setDateFormat:@"HH:mm:ss"];
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:[expire_time longLongValue] / 1000];
    self.expire_time_str = [format stringFromDate:date];
}

- (void)setMagic_level:(NSNumber *)magic_level {
    _magic_level = magic_level;
    if (!_magic_level.integerValue) {
        _magic_level = @1;
    }
}

@end
