//
//  LY_ChatContainerController.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/12.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_ChatContainerController.h"
#import "ChatListViewController.h"
#import "SPDMyFriendsViewController.h"
#import "DynamicListViewController.h"
#import "LYPSTextAlertView.h"
#import "AddFriendController.h"

@interface LY_ChatContainerController ()

@property (nonatomic, strong) ChatListViewController *chatListViewController;

@property (nonatomic, strong) SPDMyFriendsViewController *myFriendsViewController;

@property (nonatomic, strong) DynamicListViewController *dynamicListViewController;

@property (nonatomic, strong) UIButton * unReadBtn; //已阅

@property (nonatomic, strong) UIButton * addFriendBtn; // 加好友

@end

@implementation LY_ChatContainerController

- (ChatListViewController *)chatListViewController {
    if (!_chatListViewController) {
        _chatListViewController = [[ChatListViewController alloc] init];
    }
    return _chatListViewController;
}

- (SPDMyFriendsViewController *)myFriendsViewController {
    if (!_myFriendsViewController) {
        _myFriendsViewController = [[SPDMyFriendsViewController alloc] init];
    }
    return _myFriendsViewController;
}

- (DynamicListViewController *)dynamicListViewController {
    if (!_dynamicListViewController) {
        _dynamicListViewController = [[DynamicListViewController alloc] init];
    }
    return _dynamicListViewController;
}

- (NSMutableArray<NSString *> *)titleArray {
    return [NSMutableArray arrayWithArray:@[@"动态".localized, @"消息".localized, @"我的朋友".localized]];
}

- (NSMutableArray<JXPagerViewListViewDelegate> *)VCArray {
    return @[self.dynamicListViewController, self.chatListViewController, self.myFriendsViewController];
}

- (UIButton *)unReadBtn {
    if (!_unReadBtn) {
        _unReadBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_unReadBtn setImage:[UIImage imageNamed:@"ic_xiaoxi_yijianyidu"] forState:UIControlStateNormal];
        [_unReadBtn addTarget:self action:@selector(handleUnRead:)  forControlEvents:UIControlEventTouchUpInside];
    }
    return _unReadBtn;
}

- (UIButton *)addFriendBtn {
    if (!_addFriendBtn) {
        _addFriendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_addFriendBtn setImage:[UIImage imageNamed:@"ic_xiaoxi_jiahaoyou"] forState:UIControlStateNormal];
        [_addFriendBtn addTarget:self action:@selector(handleAddFriend:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addFriendBtn;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:0.0];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveReadAllMessage:) name:@"HaveReadAllMessage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMessageBadgeValue:) name:@"UpdateMessageBadgeValueNotification" object:nil];

    [self setupUI];
    
    [self setupUIFrame];

    [self setupData];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:0.0];
    [self.navigationController.navigationBar setHidden:true];
    
    [self configUnreadCount];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:1.0];
    [self.navigationController.navigationBar setHidden:false];
}

- (void)receiveReadAllMessage:(NSNotification *)noti {
    [self configUnreadCount];
}

- (void)updateMessageBadgeValue:(NSNotification *)noti {
    [self configUnreadCount];
}

- (void)setupUI {
    [self.view addSubview:self.addFriendBtn];
    [self.view addSubview:self.unReadBtn];
    self.categoryView.defaultSelectedIndex = 1;
    self.pagerView.defaultSelectedIndex = 1;
}

- (void)configUnreadCount {
    self.unReadBtn.enabled = [RCIMClient sharedRCIMClient].getTotalUnreadCount;
}

- (void)setupUIFrame {
    [self.addFriendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-15);
        make.width.and.height.mas_equalTo(19);
        make.centerY.equalTo(self.categoryView.mas_centerY).offset(0);
    }];
    [self.unReadBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(self.addFriendBtn.mas_leading).offset(-14);
        make.width.and.height.mas_equalTo(19);
        make.centerY.equalTo(self.categoryView.mas_centerY).offset(0);
    }];
}

- (void)setupData {
//    self.categoryView.indicators = @[self.categoryIndicatorLineView];
//    self.categoryView.listContainer = (id<JXCategoryViewListContainer>)self.pagerView.listContainerView;
//    self.categoryView.titles = self.titleArray;
}

- (void)handleUnRead:(UIButton *)sender {
    __weak typeof(self) WeakSelf = self;
    LYPSTextAlertView * view = [LYPSTextAlertView new];
    view.title = @"提示".localized;
    view.message = @"是否一键清除所有未读数与红点？".localized;
    [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"清除") style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
        [WeakSelf.chatListViewController clearUnreadMessage];
    }]];
    [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"取消") style:LYPSystemAlertActionStyleCancel handler:^(LYPSystemAlertAction * _Nonnull action) {
        
    }]];
    
    [view present];
}

- (void)handleAddFriend:(UIButton *)sender {
    [self.navigationController pushViewController:[AddFriendController new] animated:YES];
}

#pragma mark - JXCategoryViewDelegate

- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    self.unReadBtn.hidden = !(index == 1);
    self.addFriendBtn.hidden = !(index == 1 || index == 2);
}

@end
