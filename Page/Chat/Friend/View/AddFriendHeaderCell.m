//
//  AddFriendHeaderCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "AddFriendHeaderCell.h"

@implementation AddFriendHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapSearch)];
    [self.searchView addGestureRecognizer:tap];
    
    UITapGestureRecognizer * tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapCopyLabel)];
    self.fuzhiImageView.userInteractionEnabled = YES;
    [self.fuzhiImageView addGestureRecognizer:tap1];
}

- (void)setID:(NSString *)ID {
    _ID = ID;
}

- (void)setIsSpecialNum:(BOOL)isSpecialNum {
    _isSpecialNum = isSpecialNum;
    self.specialNumImageView.hidden = !_isSpecialNum;
    self.IDlabel.textColor = _isSpecialNum ? SPD_HEXCOlOR(@"#FED64D") : SPD_HEXCOlOR(@"1a1a1a");
    self.IDlabel.text = _isSpecialNum ? _ID : [NSString stringWithFormat:@"我的ID:%@".localized,_ID];
;
}

- (void)handleTapSearch {
    if (self.delegate && [self.delegate respondsToSelector:@selector(addFriendHeaderCellDidClickedSearchView:)]) {
        [self.delegate addFriendHeaderCellDidClickedSearchView:self];
    }
}

- (void)handleTapCopyLabel {
    [[UIPasteboard generalPasteboard] setString:self.ID];
    [self showTips:@"ID已复制".localized];
}

@end
