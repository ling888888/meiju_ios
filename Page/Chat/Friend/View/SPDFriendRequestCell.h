//
//  SPDFriendRequestCell.h
//  SimpleDate
//
//  Created by 李楠 on 2019/3/18.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class HomeModel;

@protocol SPDFriendRequestCellDelegate <NSObject>

@optional

- (void)didClickAgreeBtnWithModel:(HomeModel *)model;
- (void)didClickDeleteBtnWithModel:(HomeModel *)model;

@end

@interface SPDFriendRequestCell : UITableViewCell

@property (nonatomic, strong) HomeModel *model;
@property (nonatomic, assign) id<SPDFriendRequestCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
