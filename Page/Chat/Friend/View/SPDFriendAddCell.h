//
//  SPDFriendAddCell.h
//  SimpleDate
//
//  Created by 李楠 on 2019/3/18.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class HomeModel;

@protocol SPDFriendAddCellDelegate <NSObject>

@optional

- (void)didClickAddBtnWithModel:(HomeModel *)model;

@end

@interface SPDFriendAddCell : UITableViewCell

@property (nonatomic, strong) HomeModel *model;
@property (nonatomic, assign) id<SPDFriendAddCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
