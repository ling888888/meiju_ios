//
//  SPDFriendTypeCell.h
//  SimpleDate
//
//  Created by 李楠 on 2019/3/18.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, FriendType) {
    FriendTypeNewFriends,
    FriendTypeContacts,
    FriendTypeFacebook,
};

@interface SPDFriendTypeCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *badgeLabel;

@property (nonatomic, assign) FriendType type;

@end

NS_ASSUME_NONNULL_END
