//
//  SPDFriendTypeCell.m
//  SimpleDate
//
//  Created by 李楠 on 2019/3/18.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "SPDFriendTypeCell.h"

@interface SPDFriendTypeCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;


@end

@implementation SPDFriendTypeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setType:(FriendType)type {
    _type = type;
    
    switch (_type) {
        case FriendTypeNewFriends:
            self.imageView.image = [UIImage imageNamed:@"friend_new"];
            self.textLabel.text = SPDStringWithKey(@"新的朋友", nil);
            break;
        case FriendTypeContacts:
            self.imageView.image = [UIImage imageNamed:@"friend_contacts"];
            self.textLabel.text = SPDStringWithKey(@"通讯录", nil);
            break;
        case FriendTypeFacebook:
            self.imageView.image = [UIImage imageNamed:@"friend_facebook"];
            self.textLabel.text = @"Fackbook";
            break;
        default:
            break;
    }
}

@end
