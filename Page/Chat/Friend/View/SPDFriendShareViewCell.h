//
//  SPDFriendShareViewCell.h
//  SimpleDate
//
//  Created by 李楠 on 2019/3/20.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, FriendShareViewCellType) {
    FriendShareViewCellTypeWhatsAPP,
    FriendShareViewCellTypeMessenger,
    FriendShareViewCellTypeTelegram,
    FriendShareViewCellTypeFacebook,
    FriendShareViewCellTypeTwitter,
    FriendShareViewCellTypeWeChat,
    FriendShareViewCellTypeTimeline,
    FriendShareViewCellTypeContacts
};

@interface SPDFriendShareViewCell : UICollectionViewCell

@property (nonatomic, assign) FriendShareViewCellType type;

@end

NS_ASSUME_NONNULL_END
