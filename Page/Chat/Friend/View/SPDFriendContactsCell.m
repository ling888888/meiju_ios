//
//  SPDFriendContactsCell.m
//  SimpleDate
//
//  Created by 李楠 on 2019/3/18.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "SPDFriendContactsCell.h"
#import "HomeModel.h"

@interface SPDFriendContactsCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *inviteBtn;

@end

@implementation SPDFriendContactsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(HomeModel *)model {
    _model = model;
    
    self.nameLabel.text = _model.nick_name;
    if (_model.invited) {
        [self.inviteBtn setTitle:SPDStringWithKey(@"已邀请", nil) forState:UIControlStateNormal];
        [self.inviteBtn setTitleColor:[UIColor colorWithHexString:@"999999"] forState:UIControlStateNormal];
        self.inviteBtn.layer.borderColor = [[UIColor colorWithHexString:@"999999"] CGColor];
    } else {
        [self.inviteBtn setTitle:SPDStringWithKey(@"邀请", nil) forState:UIControlStateNormal];
        [self.inviteBtn setTitleColor:[UIColor colorWithHexString:COMMON_PINK] forState:UIControlStateNormal];
        self.inviteBtn.layer.borderColor = [[UIColor colorWithHexString:COMMON_PINK] CGColor];
    }
}

- (IBAction)clickInviteBtn:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickInviteBtnWithModel:)]) {
        [self.delegate didClickInviteBtnWithModel:self.model];
    }
}

@end
