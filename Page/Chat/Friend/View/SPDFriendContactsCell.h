//
//  SPDFriendContactsCell.h
//  SimpleDate
//
//  Created by 李楠 on 2019/3/18.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class HomeModel;

@protocol SPDFriendContactsCellDelegate <NSObject>

@optional

- (void)didClickInviteBtnWithModel:(HomeModel *)model;

@end

@interface SPDFriendContactsCell : UITableViewCell

@property (nonatomic, strong) HomeModel *model;
@property (nonatomic, assign) id<SPDFriendContactsCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
