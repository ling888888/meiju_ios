//
//  AddFriendHeaderCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class AddFriendHeaderCell;

@protocol AddFriendHeaderCellDelegate <NSObject>

@optional
- (void)addFriendHeaderCellDidClickedSearchView:(AddFriendHeaderCell *)cell;

@end

@interface AddFriendHeaderCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UILabel *IDlabel;
@property (nonatomic, copy) NSString * ID;
@property (nonatomic, assign) BOOL isSpecialNum;
@property (weak, nonatomic) IBOutlet UIImageView *fuzhiImageView;
@property (nonatomic, weak) id<AddFriendHeaderCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *specialNumImageView;

@end

NS_ASSUME_NONNULL_END
