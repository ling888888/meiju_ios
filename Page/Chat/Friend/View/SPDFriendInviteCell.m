//
//  SPDFriendInviteCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/2/10.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDFriendInviteCell.h"

@interface SPDFriendInviteCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *inviteBtn;

@end

@implementation SPDFriendInviteCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.titleLabel.text = SPDStringWithKey(@"快去邀请Facebook好友一起来Famy玩～", nil);
    [self.inviteBtn setTitle:SPDStringWithKey(@"邀请", nil) forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickInviteBtn:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didClickedInviteBtn)]) {
        [self.delegate didClickedInviteBtn];
    }
}

@end
