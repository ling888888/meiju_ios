//
//  SPDFriendInviteCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/2/10.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SPDFriendInviteCellDelegate <NSObject>

@optional

- (void)didClickedInviteBtn;

@end

@interface SPDFriendInviteCell : UITableViewCell

@property (nonatomic, weak) id<SPDFriendInviteCellDelegate> delegate;

@end
