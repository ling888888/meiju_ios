//
//  SPDFriendPlaceholderCell.m
//  SimpleDate
//
//  Created by 李楠 on 2019/3/20.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "SPDFriendPlaceholderCell.h"

@interface SPDFriendPlaceholderCell ()

@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end

@implementation SPDFriendPlaceholderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.textLabel.text = SPDStringWithKey(@"还没有好友，快去邀请吧~", nil);
}

@end
