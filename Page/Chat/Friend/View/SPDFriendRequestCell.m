//
//  SPDFriendRequestCell.m
//  SimpleDate
//
//  Created by 李楠 on 2019/3/18.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "SPDFriendRequestCell.h"
#import "HomeModel.h"

@interface SPDFriendRequestCell ()

//@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property(nonatomic, strong) LY_PortraitView *portraitView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *levelBtn;
@property (weak, nonatomic) IBOutlet UIButton *agreeBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIImageView *genderImageView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@end

@implementation SPDFriendRequestCell

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
    }
    return _portraitView;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.contentView addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(70);
        make.centerY.mas_equalTo(0);
        make.leading.mas_equalTo(5);
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(HomeModel *)model {
    _model = model;
    
//    [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _model.avatar]]];
    self.portraitView.portrait = model.portrait;
    
    self.nickNameLabel.text = _model.nick_name;
    self.genderImageView.image = [UIImage imageNamed:[_model.gender isEqualToString:@"male"] ? @"ic_zhibojian_yonghuziliao_bianqian_nan":@"ic_zhibojian_yonghuziliao_biaoqian_nv"];
    [SPDCommonTool configDataForLevelBtnWithLevel:[_model.level intValue] andUIButton:self.levelBtn];
    self.agreeBtn.hidden = ![_model.friendType isEqualToString:@"process"];
    self.deleteBtn.hidden = ![_model.friendType isEqualToString:@"process"];
    self.statusLabel.hidden = [_model.friendType isEqualToString:@"process"];
    if (![_model.friendType isEqualToString:@"process"]) {
        self.statusLabel.text = [_model.friendType isEqualToString:@"reject"] ? @"已拒绝".localized:@"已同意".localized;
    }
}

- (IBAction)clickAgreeBtn:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickAgreeBtnWithModel:)]) {
        [self.delegate didClickAgreeBtnWithModel:self.model];
    }
}

- (IBAction)clickDeleteBtn:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickDeleteBtnWithModel:)]) {
        [self.delegate didClickDeleteBtnWithModel:self.model];
    }
}

@end
