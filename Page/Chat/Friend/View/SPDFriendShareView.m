//
//  SPDFriendShareView.m
//  SimpleDate
//
//  Created by 李楠 on 2019/3/20.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "SPDFriendShareView.h"
#import "SPDFriendShareViewCell.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "SPDUMShareUtils.h"
#import "SPDContactsViewController.h"

@interface SPDFriendShareView ()<UICollectionViewDataSource, UICollectionViewDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) NSArray *typeArray;
@property (nonatomic, strong) NSDictionary *shareDic;

@end

@implementation SPDFriendShareView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.titleLabel.text = SPDStringWithKey(@"邀请好友", nil);
    [self.collectionView registerNib:[UINib nibWithNibName:@"SPDFriendShareViewCell" bundle:nil] forCellWithReuseIdentifier:@"SPDFriendShareViewCell"];
    [self requestMyInfoDetail];
    [self animating];
}

- (void)animating {
    CAKeyframeAnimation * animation;
    animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = 0.25;
    animation.removedOnCompletion = YES;
    animation.fillMode = kCAFillModeForwards;
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1, 1.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    [self.backgroundView.layer addAnimation:animation forKey:nil];
    
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    }];
}

- (IBAction)tap:(UITapGestureRecognizer *)sender {
    [self removeFromSuperview];
}

- (void)requestMyInfoDetail {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.info.detail" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        self.shareDic = suceess[@"share"];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)shareToPlatform:(SPDUMShareType)platform {
    NSString *shareUrl = self.shareDic[@"share_url"];
    NSString *sharetitle = self.shareDic[@"title"];
    NSString *shareContent = self.shareDic[@"content"];
    NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:self.shareDic[@"thumbnail"]]];
    UIImage *image = [[UIImage alloc] initWithData:data];
//    [[SPDUMShareUtils shareInstance] shareToPlatform:platform WithShareUrl:shareUrl andshareTitle:sharetitle andshareContent:shareContent andImageData:image andShareSourece:@"my_info" andSPDUMShareSuccess:^(id shareSuccess) {
//        [SPDCommonTool showWindowToast:SPDStringWithKey(@"分享成功", nil)];
//    } presentedController:[BaseViewController new] andSPDUMShareFailure:^(id shareFailure) {
//        
//    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.typeArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SPDFriendShareViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SPDFriendShareViewCell" forIndexPath:indexPath];
    cell.type = indexPath.row;
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.shareDic[@"share_url"]) {
        return;
    }
    switch (indexPath.row) {
        case FriendShareViewCellTypeWhatsAPP: {
            NSString *text = [NSString stringWithFormat:@"%@&from=whatsapp", self.shareDic[@"share_url"]];
            NSString *urlStr = [NSString stringWithFormat:@"whatsapp://send?text=%@", [text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
            NSURL *url = [NSURL URLWithString:urlStr];
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            } else {
                [SPDCommonTool showWindowToast:SPDStringWithKey(@"未安裝此应用，请安装后再进行分享", nil)];
            }
            break;
        }
        case FriendShareViewCellTypeMessenger: {
//            FBSDKShareMessengerURLActionButton *urlButton = [[FBSDKShareMessengerURLActionButton alloc] init];
//            NSMutableString *str = [NSMutableString stringWithFormat:@"%@",self.shareDic[@"share_url"]];
//            if(!str) return;
//            [str appendString:@"&from=messenger"];
//            urlButton.title = str;
//            urlButton.url = [NSURL URLWithString:str];
//
//            FBSDKShareMessengerGenericTemplateElement *element = [[FBSDKShareMessengerGenericTemplateElement alloc] init];
//            element.title = self.shareDic[@"content"]?:@"welcome to Famy";
//            element.subtitle = self.shareDic[@"content"]?:@"welcome to Famy";
//            element.imageURL = [NSURL URLWithString:self.shareDic[@"thumbnail"]?:@""];
//            element.button = urlButton;
//
//            FBSDKShareMessengerGenericTemplateContent *content = [[FBSDKShareMessengerGenericTemplateContent alloc] init];
//            content.pageID = FacebookKey;// Your page ID, required for attribution
//            content.element = element;
//
//            FBSDKMessageDialog *messageDialog = [[FBSDKMessageDialog alloc] init];
//            messageDialog.shareContent = content;
//
//            if ([messageDialog canShow]) {
//                [messageDialog show];
//            }
            break;
        }
        case FriendShareViewCellTypeTelegram: {
            NSString *text = [NSString stringWithFormat:@"%@&from=telegram", self.shareDic[@"share_url"]];
            NSString *urlStr = [NSString stringWithFormat:@"https://t.me/share/url?url=%@", text];
            NSURL *url = [NSURL URLWithString:urlStr];
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            } else {
                [SPDCommonTool showWindowToast:SPDStringWithKey(@"未安裝此应用，请安装后再进行分享", nil)];
            }
            break;
        }
        case FriendShareViewCellTypeFacebook: {
            NSString *text = [NSString stringWithFormat:@"%@", self.shareDic[@"share_url"]];
            NSString *urlStr = [NSString stringWithFormat:@"https://www.facebook.com/sharer/sharer.php?u=%@", text];
            NSURL *url = [NSURL URLWithString:urlStr];
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            } else {
                [SPDCommonTool showWindowToast:SPDStringWithKey(@"未安裝此应用，请安装后再进行分享", nil)];
            }
            break;
        }
        case FriendShareViewCellTypeTwitter:
            [self shareToPlatform:SPDShareToTwitter];
            break;
        case FriendShareViewCellTypeWeChat:
            [self shareToPlatform:SPDShareToWechatSession];
            break;
        case FriendShareViewCellTypeTimeline:
            [self shareToPlatform:SPDShareToWechatTimeline];
            break;
        case FriendShareViewCellTypeContacts: {
            SPDContactsViewController *vc = [[SPDContactsViewController alloc] init];
            vc.fromShare = YES;
            [[SPDCommonTool getWindowTopViewController].navigationController pushViewController:vc animated:YES];
            break;
        }
        default:
            break;
    }
    [self removeFromSuperview];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ![touch.view isDescendantOfView:self.backgroundView];
}

#pragma mark - Setters and Getters

- (NSArray *)typeArray {
    if (!_typeArray) {
        _typeArray = @[@(FriendShareViewCellTypeWhatsAPP),
                       @(FriendShareViewCellTypeMessenger),
                       @(FriendShareViewCellTypeTelegram),
                       @(FriendShareViewCellTypeFacebook),
                       @(FriendShareViewCellTypeTwitter),
                       @(FriendShareViewCellTypeWeChat),
                       @(FriendShareViewCellTypeTimeline),
                       @(FriendShareViewCellTypeContacts)];
    }
    return _typeArray;
}

@end
