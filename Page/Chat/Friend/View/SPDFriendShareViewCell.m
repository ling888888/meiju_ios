//
//  SPDFriendShareViewCell.m
//  SimpleDate
//
//  Created by 李楠 on 2019/3/20.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "SPDFriendShareViewCell.h"

@interface SPDFriendShareViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end

@implementation SPDFriendShareViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setType:(FriendShareViewCellType)type {
    _type = type;
    
    switch (_type) {
        case FriendShareViewCellTypeWhatsAPP:
            self.imageView.image = [UIImage imageNamed:@"ic_whatsAPP"];
            self.textLabel.text = @"WhatsAPP";
            break;
        case FriendShareViewCellTypeMessenger:
            self.imageView.image = [UIImage imageNamed:@"ic_messenger"];
            self.textLabel.text = @"Messenger";
            break;
        case FriendShareViewCellTypeTelegram:
            self.imageView.image = [UIImage imageNamed:@"ic_telegram"];
            self.textLabel.text = @"telegram";
            break;
        case FriendShareViewCellTypeFacebook:
            self.imageView.image = [UIImage imageNamed:@"ic_facebook"];
            self.textLabel.text = @"Facebook";
            break;
        case FriendShareViewCellTypeTwitter:
            self.imageView.image = [UIImage imageNamed:@"ic_twitter"];
            self.textLabel.text = @"Twitter";
            break;
        case FriendShareViewCellTypeWeChat:
            self.imageView.image = [UIImage imageNamed:@"ic_wechat"];
            self.textLabel.text = SPDStringWithKey(@"微信", nil);
            break;
        case FriendShareViewCellTypeTimeline:
            self.imageView.image = [UIImage imageNamed:@"ic_moments"];
            self.textLabel.text = SPDStringWithKey(@"朋友圈", nil);
            break;
        case FriendShareViewCellTypeContacts:
            self.imageView.image = [UIImage imageNamed:@"ic_contacts"];
            self.textLabel.text = SPDStringWithKey(@"通讯录", nil);
            break;
        default:
            break;
    }
}

@end
