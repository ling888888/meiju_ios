//
//  SPDMyFriendsViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2019/3/18.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "SPDMyFriendsViewController.h"
#import "SPDFriendShareView.h"
#import "HomeModel.h"
#import "HomeUserListCell.h"
#import "SPDFriendPlaceholderCell.h"
#import "SPDSearchUserViewController.h"
#import "SPDNewFriendsViewController.h"
#import "SPDContactsViewController.h"
#import "SPDFBFriendsViewController.h"
#import "NewFriendCollectionCell.h"
#import "FriendPlaceholderView.h"

@interface SPDMyFriendsViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, HomeUserListCellDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) NSInteger requestCount;
@property (nonatomic, strong) FriendPlaceholderView * placeholderView;
@end

@implementation SPDMyFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.collectionView];
    [self.collectionView addSubview:self.placeholderView];
    self.placeholderView.frame = CGRectMake(0, 70, kScreenW, self.collectionView.bounds.size.height);
    self.placeholderView.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.page = 0;
    [self requestFriendList];
    [self requestFriendToMe];
}

#pragma mark - Event response

- (void)clickRightBarButtonItem:(UIBarButtonItem *)sender {
    SPDFriendShareView *view = [[[NSBundle mainBundle] loadNibNamed:@"SPDFriendShareView" owner:self options:nil] firstObject];
    view.frame = CGRectMake(0, 0, kScreenW, kScreenH);
    [self.tabBarController.view addSubview:view];
}

#pragma mark - Private methods

- (void)requestFriendList {
    NSDictionary *dic = @{@"type": @"list", @"page": @(self.page)};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"friend" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if (self.page == 0) {
            [self.dataArray removeAllObjects];
        }
        
        NSArray *list = suceess[@"list"];
        for (NSDictionary *dic in list) {
            HomeModel *model = [HomeModel initWithDictionary:dic];
            [self.dataArray addObject:model];
        }
        self.placeholderView.hidden = !( self.dataArray.count== 0);
        [self.collectionView reloadData];
        self.page++;
        
        [self.collectionView.mj_header endRefreshing];
        if (self.collectionView.mj_footer.state == MJRefreshStateRefreshing) {
            [self.collectionView.mj_footer endRefreshingWithCompletionBlock:^{
                self.collectionView.mj_footer.hidden = !list.count;
            }];
        } else {
            self.collectionView.mj_footer.hidden = !list.count;
        }
    } bFailure:^(id  _Nullable failure) {
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
    }];
}

- (void)requestFriendToMe {
    NSDictionary *dic = @{@"type": @"to_me", @"page": @(0)};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"friend" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.requestCount = [suceess[@"count"] integerValue];
        [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
    } bFailure:^(id  _Nullable failure) {

    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return section == 0 ? 1: self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        NewFriendCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NewFriendCollectionCell" forIndexPath:indexPath];
        if (self.requestCount > 0) {
            cell.countLabel.hidden = NO;
            cell.countLabel.text = self.requestCount > 99 ? @"99+": [NSString stringWithFormat:@"%@",@( self.requestCount)];
        }else{
            cell.countLabel.hidden = YES;
        }
        return cell;
    }else{
        HomeUserListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeUserListCell" forIndexPath:indexPath];
        cell.model = self.dataArray[indexPath.row];
        cell.hideLevel = NO;
        cell.delegate = self;
        return cell;
    }
    
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1 && self.dataArray.count) {
        HomeModel *model = self.dataArray[indexPath.row];
        [self pushToDetailTableViewController:self userId:model._id];
    }else if (indexPath.section == 0){
        // 新的朋友页面
        SPDNewFriendsViewController *vc = [[SPDNewFriendsViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0: {
            return CGSizeMake(kScreenW, 70);
            break;
        }
        default: {
            return CGSizeMake(kScreenW, self.dataArray.count ? 70 : 320);
            break;
        }
    }
}

#pragma mark - SPDFriendHeaderCellDelegate

- (void)didClickSearchBar {
    SPDSearchUserViewController *vc = [[SPDSearchUserViewController alloc] init];
    vc.showSeachBarOnNavigationBar = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - HomeUserListCellDelegate

- (void)didClickEnterClanBtnWithModel:(HomeModel *)model {
    if (!model.clan_id || [model.clan_id isKindOfClass:[NSNull class]]) {
        [self pushToDetailTableViewController:self userId:model._id];
    } else {
        if ([[SPDApiUser currentUser].lang isEqualToString:model.lang]) {
            [self joinChatroomWithClanId:model.clan_id];
        } else {
            [self showToast:SPDStringWithKey(@"对方和你不在同一语言区，不能跨区进家族", nil)];
        }
    }
}

// MARK: - JXPagerViewListViewDelegate

- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return self.collectionView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    
}

#pragma mark - Setters & Getters

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.sectionInset = UIEdgeInsetsZero;
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH - NavHeight - TabBarHeight) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [_collectionView registerNib:[UINib nibWithNibName:@"NewFriendCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"NewFriendCollectionCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"HomeUserListCell" bundle:nil] forCellWithReuseIdentifier:@"HomeUserListCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"SPDFriendPlaceholderCell" bundle:nil] forCellWithReuseIdentifier:@"SPDFriendPlaceholderCell"];
        
        _collectionView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            self.page = 0;
            [self requestFriendList];
        }];
        _collectionView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            [self requestFriendList];
        }];
        _collectionView.mj_footer.hidden = YES;
        
        if (@available(iOS 11.0, *)) {
            _collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
    }
    return _collectionView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (FriendPlaceholderView *)placeholderView {
    if (!_placeholderView) {
        _placeholderView = [[[NSBundle mainBundle] loadNibNamed:@"FriendPlaceholderView" owner:self options:nil]lastObject];
    }
    return _placeholderView;
}
@end
