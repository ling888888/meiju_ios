//
//  SPDContactsViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2019/3/18.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "SPDContactsViewController.h"
#import <AddressBook/AddressBook.h>
#import <MessageUI/MessageUI.h>
#import "HomeModel.h"
#import "SPDFriendAddCell.h"
#import "SPDFriendContactsCell.h"

@interface SPDContactsViewController ()<UITableViewDataSource, UITableViewDelegate, SPDFriendAddCellDelegate, SPDFriendContactsCellDelegate, MFMessageComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIButton *openBtn;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *famyUserArray;
@property (nonatomic, strong) NSMutableArray *contactsArray;
@property (nonatomic, strong) NSMutableArray *phoneNumArray;
@property (nonatomic, strong) NSMutableArray *areaCodeArray;
@property (nonatomic, strong) NSMutableArray *tmpArray;
@property (nonatomic, strong) HomeModel *invitingModel;

@end

@implementation SPDContactsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = SPDStringWithKey(@"通讯录", nil);
    self.topConstraint.constant = NavHeight;
    self.titleLabel.text = SPDStringWithKey(@"未开启通讯录", nil);
    self.descLabel.text = SPDStringWithKey(@"同步通讯录，看看哪些朋友在用Famy，然后加为好友", nil);
    [self.openBtn setTitle:SPDStringWithKey(@"开启", nil) forState:UIControlStateNormal];
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView registerNib:[UINib nibWithNibName:@"SPDFriendAddCell" bundle:nil] forCellReuseIdentifier:@"SPDFriendAddCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"SPDFriendContactsCell" bundle:nil] forCellReuseIdentifier:@"SPDFriendContactsCell"];
    
    [self checkAuthorizationStatus];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Event response

- (IBAction)clickOpenBtn:(id)sender {
    [self requestAuthorization];
}

#pragma mark - Private methods

- (void)checkAuthorizationStatus {
    if (ABAddressBookGetAuthorizationStatus() != kABAuthorizationStatusAuthorized) {
        self.tableView.hidden = YES;
    } else {
        self.tableView.hidden = NO;
        [self getContacts];
    }
}

- (void)requestAuthorization {
    ABAddressBookRef adressBook = ABAddressBookCreate();
    ABAddressBookRequestAccessWithCompletion(adressBook, ^(bool granted, CFErrorRef error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (granted) {
                self.tableView.hidden = NO;
                [self getContacts];
            } else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }
        });
    });
}

- (void)getContacts {
    ABAddressBookRef addressBook = ABAddressBookCreate();
    CFArrayRef allPeopleArray = ABAddressBookCopyArrayOfAllPeople(addressBook);
    NSInteger allPeopleCount = CFArrayGetCount(allPeopleArray);
    for (NSInteger i = 0; i < allPeopleCount; i++) {
        ABRecordRef record = CFArrayGetValueAtIndex(allPeopleArray, i);
        ABMutableMultiValueRef allPhoneNumbers = ABRecordCopyValue(record, kABPersonPhoneProperty);
        NSString *phoneNumber = ((__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(allPhoneNumbers)).firstObject;
        if (phoneNumber) {
            CFStringRef firstName = ABRecordCopyValue(record, kABPersonFirstNameProperty);
            CFStringRef lastName = ABRecordCopyValue(record, kABPersonLastNameProperty);
            NSString *name = phoneNumber;
            if (firstName && lastName) {
                name = [NSString stringWithFormat:@"%@%@", firstName, lastName];
            } else if (firstName) {
                name = (__bridge NSString *)(firstName);
            } else if (lastName) {
                name = (__bridge NSString *)(lastName);
            }
            
            HomeModel *model = [[HomeModel alloc] init];
            model.nick_name = name;
            model.mobile = [self transformPhoneNumber:phoneNumber];
            if (![self.phoneNumArray containsObject:model.mobile]) {
                [self.contactsArray addObject:model];
                [self.phoneNumArray addObject:model.mobile];
            }
        }
    }
    self.tmpArray = [NSMutableArray arrayWithArray:self.phoneNumArray];
    [self requestFriendContact];
}

- (void)requestFriendContact {
    if (!self.tmpArray.count) {
        return;
    }
    NSArray *array = self.tmpArray;
    if (self.tmpArray.count > 10) {
        array = [self.tmpArray subarrayWithRange:NSMakeRange(0, 10)];
    }
    NSDictionary *dic = @{@"phone_list": [array componentsJoinedByString:@","]};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"friend/contact" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        NSArray *list = suceess[@"list"];
        for (NSDictionary *dic in list) {
            HomeModel *model = [HomeModel initWithDictionary:dic];
            if ([self.phoneNumArray containsObject:model.mobile]) {
                NSUInteger index = [self.phoneNumArray indexOfObject:model.mobile];
                [self.phoneNumArray removeObjectAtIndex:index];
                [self.contactsArray removeObjectAtIndex:index];
            }
            if (!model.is_friend && ![model._id isEqualToString:[SPDApiUser currentUser].userId]) {
                [self.famyUserArray addObject:model];
            }
        }
        [self.tmpArray removeObjectsInArray:array];
        [self requestFriendContact];
        [self.tableView reloadData];
    } bFailure:^(id  _Nullable failure) {

    }];
}

- (NSString *)transformPhoneNumber:(NSString *)phoneNumber {
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"·" withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    phoneNumber = [[phoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet]] componentsJoinedByString:@""];
    if ([phoneNumber hasPrefix:@"+"]) {
        for (NSString *areaCode in self.areaCodeArray) {
            if ([phoneNumber hasPrefix:areaCode]) {
                phoneNumber = [phoneNumber substringFromIndex:areaCode.length];
            }
        }
    }
    return phoneNumber;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.famyUserArray.count ? 2 : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0 && self.famyUserArray.count) {
        return self.famyUserArray.count;
    } else {
        return self.contactsArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && self.famyUserArray.count) {
        SPDFriendAddCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SPDFriendAddCell" forIndexPath:indexPath];
        cell.model = self.famyUserArray[indexPath.row];
        cell.delegate = self;
        return cell;
    } else {
        SPDFriendContactsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SPDFriendContactsCell" forIndexPath:indexPath];
        cell.model = self.contactsArray[indexPath.row];
        cell.delegate = self;
        return cell;
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && self.famyUserArray.count) {
        HomeModel *model = self.famyUserArray[indexPath.row];
        [self pushToDetailTableViewController:self userId:model._id];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel *label = [[UILabel alloc] init];
    label.backgroundColor = [UIColor colorWithHexString:@"F5F5F5"];
    label.textColor = [UIColor colorWithHexString:@"333333"];
    label.font = [UIFont systemFontOfSize:16];
    if (section == 0 && self.famyUserArray.count) {
        label.text = SPDStringWithKey(@"   已在使用Famy的联系人", nil);
    } else {
        label.text = SPDStringWithKey(@"   邀请联系人", nil);
    }
    return label;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && self.famyUserArray.count) {
        return 65;
    } else {
        return 45;
    }
}

#pragma mark - SPDFriendAddCellDelegate

- (void)didClickAddBtnWithModel:(HomeModel *)model {
    [SPDCommonTool requestFriendPostWithUserId:model._id type:@"request" success:^(id  _Nullable suceess) {
        [self showToast:SPDStringWithKey(@"请求发送成功，请等待对方回应", nil)];
        [self.famyUserArray removeObject:model];
        [self.tableView reloadData];
    } failure:^(id  _Nullable failure) {
        
    }];
}

#pragma mark - SPDFriendContactsCellDelegate

- (void)didClickInviteBtnWithModel:(HomeModel *)model {
    MFMessageComposeViewController *vc = [[MFMessageComposeViewController alloc] init];
    vc.recipients = @[model.mobile];
    NSString *invitationCode = [SPDCommonTool getInvitationCodeWithUserId:[SPDApiUser currentUser].userId];
    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDataIdentifier];
    NSString *url = userInfo[@"share"][@"share_url"];
    NSString *body = [NSString stringWithFormat:SPDStringWithKey(@"全世界最热闹的语音聊天室在这里，快和我一起来玩吧！Famy里搜索“用户ID：%@”，就能找到我。下载地址>>%@", nil), invitationCode, url];
    vc.body = body;
    vc.messageComposeDelegate = self;
    [self presentViewController:vc animated:YES completion:nil];
    self.invitingModel = model;
}

#pragma mark - MFMessageComposeViewControllerDelegate

- (void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result {
    [controller dismissViewControllerAnimated:YES completion:nil];
    if (result == MessageComposeResultSent) {
        self.invitingModel.invited = YES;
        [self.tableView reloadData];
    }
}

#pragma mark - Setters & Getters

- (void)setFromShare:(BOOL)fromShare {
    _fromShare = fromShare;
    
    if (_fromShare && ABAddressBookGetAuthorizationStatus() != kABAuthorizationStatusAuthorized) {
        NSString *title = SPDStringWithKey(@"进入设置打开通讯录授权.就可以找到更多好友啦～", nil);
        [SPDCommonTool presentCommonController:self title:title messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
            
        } confirmTitle:SPDStringWithKey(@"去设置", nil) confirmAction:^{
            [self requestAuthorization];
        }];
    }
}

- (NSMutableArray *)famyUserArray {
    if (!_famyUserArray) {
        _famyUserArray = [NSMutableArray array];
    }
    return _famyUserArray;
}

- (NSMutableArray *)contactsArray {
    if (!_contactsArray) {
        _contactsArray = [NSMutableArray array];
    }
    return _contactsArray;
}

- (NSMutableArray *)phoneNumArray {
    if (!_phoneNumArray) {
        _phoneNumArray = [NSMutableArray array];
    }
    return _phoneNumArray;
}

- (NSMutableArray *)areaCodeArray {
    if (!_areaCodeArray) {
        _areaCodeArray = [NSMutableArray array];
        for (NSArray *array in [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Areacode" ofType:@"plist"]]) {
            for (NSArray *subArr in array) {
                NSString *areaCode = [NSString stringWithFormat:@"%@", subArr.lastObject];
                [_areaCodeArray addObject:areaCode];
            }
        }
    }
    return _areaCodeArray;
}

- (NSMutableArray *)tmpArray {
    if (!_tmpArray) {
        _tmpArray = [NSMutableArray array];
    }
    return _tmpArray;
}

@end
