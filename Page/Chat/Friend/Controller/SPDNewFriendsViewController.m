//
//  SPDNewFriendsViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2019/3/18.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "SPDNewFriendsViewController.h"
#import "HomeModel.h"
#import "SPDFriendRequestCell.h"
#import "AddFriendController.h"

@interface SPDNewFriendsViewController ()<UITableViewDataSource, UITabBarDelegate, SPDFriendRequestCellDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation SPDNewFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = SPDStringWithKey(@"新的朋友", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_xiaoxi_jiahaoyou"] style:UIBarButtonItemStylePlain target:self action:@selector(clickRightBarButtonItem:)];
    self.topConstraint.constant = NavHeight;
    self.placeholderLabel.text = SPDStringWithKey(@"没有新的好友请求", nil);
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView registerNib:[UINib nibWithNibName:@"SPDFriendRequestCell" bundle:nil] forCellReuseIdentifier:@"SPDFriendRequestCell"];
    self.tableView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
        self.page = 0;
        [self requestData];
    }];
    self.tableView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
        [self requestData];
    }];
    self.tableView.mj_footer.hidden = YES;
    [self requestData];
}

#pragma mark - Event response

- (void)clickRightBarButtonItem:(UIBarButtonItem *)sender {
  // 去添加朋友页面
    [self.navigationController pushViewController:[AddFriendController new] animated:YES];
}

#pragma mark - Private methods

- (void)requestData {
    NSDictionary *dic = @{@"type": @"to_me", @"page": @(self.page)};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"friend" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if (self.page == 0) {
            [self.dataArray removeAllObjects];
        }
        
        NSArray *list = suceess[@"list"];
        for (NSDictionary *dic in list) {
            HomeModel *model = [HomeModel initWithDictionary:dic];
            [self.dataArray addObject:model];
        }
        
        if (self.dataArray.count) {
            [self.tableView reloadData];
            self.page++;
            
            [self.tableView.mj_header endRefreshing];
            if (self.tableView.mj_footer.state == MJRefreshStateRefreshing) {
                [self.tableView.mj_footer endRefreshingWithCompletionBlock:^{
                    self.tableView.mj_footer.hidden = !list.count;
                }];
            } else {
                self.tableView.mj_footer.hidden = !list.count;
            }
        } else {
            self.tableView.hidden = YES;
        }
    } bFailure:^(id  _Nullable failure) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SPDFriendRequestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SPDFriendRequestCell" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    cell.delegate = self;
    return cell;
}

//侧滑允许编辑cell
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *deleteRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"删除".localized handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        HomeModel * model = self.dataArray[indexPath.row];
        [SPDCommonTool requestFriendPostWithUserId:model.user_id type:@"request_delete" success:^(id  _Nullable suceess) {
            [self.dataArray removeObject:model];
            if (self.dataArray.count) {
                [self.tableView reloadData];
            } else {
                self.tableView.hidden = YES;
            }
        } failure:^(id  _Nullable failure) {
            
        }];
    }];
    return @[deleteRowAction];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    HomeModel *model = self.dataArray[indexPath.row];
    [self pushToDetailTableViewController:self userId:model.user_id];
}

#pragma mark - SPDFriendRequestCellDelegate

- (void)didClickAgreeBtnWithModel:(HomeModel *)model {
    [SPDCommonTool requestFriendPostWithUserId:model.user_id type:@"accept" success:^(id  _Nullable suceess) {
        model.friendType = @"accept";
        [self.tableView reloadData];
    } failure:^(id  _Nullable failure) {
        
    }];
}

- (void)didClickDeleteBtnWithModel:(HomeModel *)model {
    [SPDCommonTool requestFriendPostWithUserId:model.user_id type:@"refuse" success:^(id  _Nullable suceess) {
        model.friendType = @"reject";
        [self.tableView reloadData];
    } failure:^(id  _Nullable failure) {
        
    }];
}

#pragma mark - Setters & Getters

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

@end
