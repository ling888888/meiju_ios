//
//  AddFriendController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "AddFriendController.h"
#import "AddFriendHeaderCell.h"
#import "SPDSearchUserViewController.h"
#import "NewFriendCollectionCell.h"
#import "SPDContactsViewController.h"
#import "SPDFriendShareView.h"

@interface AddFriendController ()<UICollectionViewDelegate,UICollectionViewDataSource,AddFriendHeaderCellDelegate,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView * collectionView;
@property (nonatomic, copy) NSString * specialNum;
@end

@implementation AddFriendController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"添加好友".localized;
    [self.view addSubview:self.collectionView];
    [self requestUserInfo];
}

- (void)requestUserInfo {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.info.detail" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.specialNum = suceess[@"specialNum"];
        [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]]];
    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

#pragma mark - delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 3;
}

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        AddFriendHeaderCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AddFriendHeaderCell" forIndexPath:indexPath];
        cell.delegate = self;
        cell.ID = _specialNum.notEmpty ? _specialNum : [SPDCommonTool getInvitationCodeWithUserId:[SPDApiUser currentUser].userId];
        cell.isSpecialNum = _specialNum.notEmpty;
        return cell;
    }else{
        NewFriendCollectionCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NewFriendCollectionCell" forIndexPath:indexPath];
        cell.img.image = [UIImage imageNamed:indexPath.row == 1 ? @"ic_haoyou_tongxunlu":@"ic_haoyou_fenxiang"];
        cell.friendTitleLabel.text = indexPath.row == 1? @"添加通讯录好友".localized:@"分享Famy".localized;
        cell.countLabel.hidden = YES;
        return cell;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(kScreenW, indexPath.row == 0 ? 93: 70);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        SPDContactsViewController *vc = [[SPDContactsViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row == 2){
        SPDFriendShareView *view = [[[NSBundle mainBundle] loadNibNamed:@"SPDFriendShareView" owner:self options:nil] firstObject];
        view.frame = CGRectMake(0, 0, kScreenW, kScreenH);
        [self.tabBarController.view addSubview:view];
    }
}

- (void)addFriendHeaderCellDidClickedSearchView:(AddFriendHeaderCell *)cell {
    SPDSearchUserViewController *vc = [[SPDSearchUserViewController alloc] init];
    vc.showSeachBarOnNavigationBar = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Getters

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout new];
        _collectionView = [[UICollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerNib:[UINib nibWithNibName:@"AddFriendHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"AddFriendHeaderCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"NewFriendCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"NewFriendCollectionCell"];
        _collectionView.backgroundColor = [UIColor whiteColor];
        
    }
    return _collectionView;
}




@end
