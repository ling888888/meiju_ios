//
//  SPDFBFriendsViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2018/2/1.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDFBFriendsViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "HomeModel.h"
#import "SPDFriendAddCell.h"
#import "SPDFriendInviteCell.h"

@interface SPDFBFriendsViewController ()<UITableViewDataSource, UITableViewDelegate, SPDFriendAddCellDelegate, SPDFriendInviteCellDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIButton *openBtn;

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *famyUserArray;
@property (nonatomic, strong) NSMutableArray *facebookIDArray;
@property (nonatomic, copy) NSString *shareURL;

@end

@implementation SPDFBFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Facebook";
    self.titleLabel.text = SPDStringWithKey(@"Famy因朋友而精彩", nil);
    self.descLabel.text = SPDStringWithKey(@"同步Facebook，看看哪些朋友在用Famy，然后加为好友", nil);
    [self.openBtn setTitle:SPDStringWithKey(@"开启", nil) forState:UIControlStateNormal];

    [self requestMyInfoDetail];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (!_tableView) {
        [self.view addSubview:self.tableView];
        
        if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"user_friends"]) {
            self.tableView.hidden = NO;
            [self requestFriends];
        } else {
            self.tableView.hidden = YES;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Event response

- (IBAction)clickOpenBtn:(UIButton *)sender {
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logInWithPermissions:@[@"public_profile", @"user_friends"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult * _Nullable result, NSError * _Nullable error) {
        if (!error && !result.isCancelled && ![result.declinedPermissions containsObject:@"user_friends"]) {
            self.tableView.hidden = NO;
            [self requestFriends];
        }
    }];
}

#pragma mark - Private methods

- (void)requestMyInfoDetail {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.info.detail" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.shareURL = suceess[@"share"][@"share_url"];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestFriends {
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:@"/me/friends"
                                  parameters:@{@"limit": @10}
                                  HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        if (!error) {
            for (NSDictionary *dic in result[@"data"]) {
                [self.facebookIDArray addObject:dic[@"id"]];
            }
            if (self.facebookIDArray.count) {
                [self requestFriendFacebook];
            }
        }
    }];
}

- (void)requestFriendFacebook {
    NSDictionary *params = @{@"fb_ids": [self.facebookIDArray componentsJoinedByString:@","]};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:params] bURL:@"friend/facebook" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        for (NSDictionary *dic in suceess[@"list"]) {
            HomeModel *model = [HomeModel initWithDictionary:dic];
            if (!model.is_friend) {
                [self.famyUserArray addObject:model];
            }
        }
        [self.tableView reloadData];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.famyUserArray.count ? 2 : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0 && self.famyUserArray.count) {
        return self.famyUserArray.count;
    } else {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && self.famyUserArray.count) {
        SPDFriendAddCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SPDFriendAddCell" forIndexPath:indexPath];
        cell.delegate = self;
        cell.model = self.famyUserArray[indexPath.row];
        return cell;
    } else {
        SPDFriendInviteCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SPDFriendInviteCell" forIndexPath:indexPath];
        cell.delegate = self;
        return cell;
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && self.famyUserArray.count) {
        HomeModel *model = self.famyUserArray[indexPath.row];
        [self pushToDetailTableViewController:self userId:model._id];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && self.famyUserArray.count) {
        return 65;
    } else {
        return 340;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel *label = [[UILabel alloc] init];
    label.backgroundColor = [UIColor colorWithHexString:@"F5F5F5"];
    label.textColor = [UIColor colorWithHexString:@"333333"];
    label.font = [UIFont systemFontOfSize:16];
    if (section == 0 && self.famyUserArray.count) {
        label.text = SPDStringWithKey(@"   已在使用Famy的Facebook好友", nil);
    } else {
        label.text = SPDStringWithKey(@"   邀请Facebook好友", nil);
    }
    return label;
}

#pragma mark - SPDFriendAddCellDelegate

- (void)didClickAddBtnWithModel:(HomeModel *)model {
    NSDictionary *params = @{@"type": @"request", @"to": model._id};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:params] bURL:@"friend" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        [self showToast:SPDStringWithKey(@"请求发送成功，请等待对方回应", nil)];
        [self.famyUserArray removeObject:model];
        [self.tableView reloadData];
    } bFailure:^(id  _Nullable failure) {
        [self showToast:failure[@"msg"]];
    }];
}

#pragma mark - SPDFriendInviteCellDelegate

- (void)didClickedInviteBtn {
    FBSDKShareLinkContent *linkContent = [[FBSDKShareLinkContent alloc] init];
    linkContent.contentURL = [NSURL URLWithString:self.shareURL];
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    dialog.fromViewController = self;
    dialog.shareContent = linkContent;
    dialog.mode = FBSDKShareDialogModeNative;
    [dialog show];
}

#pragma mark - Setters & Getters

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.sectionHeaderHeight = 35;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [[UIView alloc] init];
        [_tableView registerNib:[UINib nibWithNibName:@"SPDFriendAddCell" bundle:nil] forCellReuseIdentifier:@"SPDFriendAddCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"SPDFriendInviteCell" bundle:nil] forCellReuseIdentifier:@"SPDFriendInviteCell"];
    }
    return _tableView;
}

- (NSMutableArray *)facebookIDArray {
    if (!_facebookIDArray) {
        _facebookIDArray = [NSMutableArray array];
    }
    return _facebookIDArray;
}

- (NSMutableArray *)famyUserArray {
    if (!_famyUserArray) {
        _famyUserArray = [NSMutableArray array];
    }
    return _famyUserArray;
}

@end
