//
//  SPDMyFriendsViewController.h
//  SimpleDate
//
//  Created by 李楠 on 2019/3/18.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "BaseViewController.h"
#import "JXPagerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface SPDMyFriendsViewController : BaseViewController <JXPagerViewListViewDelegate>

- (void)clickRightBarButtonItem:(UIBarButtonItem *)sender;

@end

NS_ASSUME_NONNULL_END
