//
//  SPDContactsViewController.h
//  SimpleDate
//
//  Created by 李楠 on 2019/3/18.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "BaseViewController.h"

@interface SPDContactsViewController : BaseViewController

@property (nonatomic, assign) BOOL fromShare;

@end
