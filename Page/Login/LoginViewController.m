//
//  LoginViewController.m
//  SimpleDate
//
//  Created by 程龙军 on 15/12/25.
//  Copyright © 2015年 WYB. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"
#import "AppManager.h"
#import "SPDHashGenerator.h"
#import "PickerViewController.h"
#import "OverSeaRegisterController.h"
#import "SelectAreaCodeController.h"

@interface LoginViewController () <UITextFieldDelegate,SelectAreaCodeControllerDelegate>
    @property (weak, nonatomic) IBOutlet NSLayoutConstraint *telephoneCons;

    @property (weak, nonatomic) IBOutlet UIView *overseaView;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loginActivityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *forgetPwdBtn;

@property (nonatomic, copy) NSString *phoneNumber;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *deviceToken;

@property (nonatomic, copy) NSString *registrationID;
    
//海外版
@property (nonatomic, assign) BOOL isOverSea;

@property (nonatomic, copy) NSString *lac;
@property (weak, nonatomic) IBOutlet UIButton *codeBtn;
@property (weak, nonatomic) IBOutlet UILabel *countryLabel;

@property (weak, nonatomic) IBOutlet UIImageView *codeImgView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topLeading;
@property (weak, nonatomic) IBOutlet UIView *pwdView;

@end

@implementation LoginViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self createNavigation];
    
}

-(void)createNavigation {
    
    self.navigationItem.title = @"登录".localized;
    
    UIButton *rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 60, 40)];
    [rightBtn setTitle:SPDStringWithKey(@"注册", nil)forState:UIControlStateNormal];
    rightBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [rightBtn setTitleColor:SPD_HEXCOlOR(@"1a1a1a") forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(dealTapRightNavigation:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightBarItem;
    
    [self.forgetPwdBtn setTitle:SPDStringWithKey(@"忘记密码？", nil) forState:UIControlStateNormal];
    UIBarButtonItem *leftBarItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ic_arrow_left"].adaptiveRtl style:UIBarButtonItemStylePlain target:self action:@selector(dealTapLeftNavigation:)];
    self.navigationItem.leftBarButtonItem = leftBarItem;
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(codeBtnClicked:)];
    [self.codeImgView addGestureRecognizer:tap];
    
}
    
-(void)dealTapLeftNavigation:(UIBarButtonItem *)item{
    [self dismissViewControllerAnimated:YES completion:nil];
}
    
-(void)dealTapRightNavigation:(UIBarButtonItem *)item{
    
    OverSeaRegisterController * vc = [[UIStoryboard storyboardWithName:@"Login" bundle:nil]instantiateViewControllerWithIdentifier:@"OverSeaRegisterController"];
    vc.isRegister = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
    
- (void)viewDidLoad
{
    [super viewDidLoad];

    [self returnKeyboard];
    
    [self localizeText];
    
    self.lac = @"966";
    [self.codeBtn setTitle:self.lac forState:UIControlStateNormal];
    self.countryLabel.text = @"Saudi Arabia";
 
    self.registrationID = @"";
    self.passwordTextField.delegate = self;
    self.phoneNumberTextField.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [self requestSignupAreaCode];
}

- (void)keyboardWillShow:(NSNotification *)notify {
    if (!IS_IPAD) {return;}
    CGFloat height = CGRectGetMaxY(self.pwdView.frame);
    CGRect keyboardRect = [[[notify userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    if (height +keyboardRect.size.height  >= kScreenH - 30) {
        [UIView animateWithDuration:0.3 animations:^{
            self.topLeading.constant = -50;
        }];
    }
}

- (void)keyboardWillHide:(NSNotification *)notify {
    if (!IS_IPAD) {return;}
    [UIView animateWithDuration:0.3 animations:^{
        self.topLeading.constant = 0;
    }];
}

- (void)requestSignupAreaCode {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"signup.areaCode" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if (suceess[@"areaCode"] && suceess[@"areaName"]) {
            self.lac = [suceess[@"areaCode"] isKindOfClass:[NSString class]] ? suceess[@"areaCode"]: [suceess[@"areaCode"] stringValue];
            self.countryLabel.text = suceess[@"areaName"];
            [self.codeBtn setTitle:self.lac forState:UIControlStateNormal];
        }
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)localizeText {
    
    self.phoneNumberTextField.placeholder = SPDStringWithKey(@"输入您的手机号", nil);
    self.passwordTextField.placeholder = SPDStringWithKey(@"请输入密码", nil);
    [self.loginButton setTitle:SPDStringWithKey(@"登录", nil) forState:UIControlStateNormal];
}

- (IBAction)forgetPwd:(UIButton *)sender {
    
    OverSeaRegisterController * vc = [[UIStoryboard storyboardWithName:@"Login" bundle:nil]instantiateViewControllerWithIdentifier:@"OverSeaRegisterController"];
    vc.isResetPwd = YES;
    [self.navigationController pushViewController:vc animated:YES];

}

- (IBAction)loginButtonTapped:(id)sender
{
    if (![SPDCommonTool isEmpty:self.passwordTextField.text] && [SPDCommonTool isLegalPhoneNumber:self.phoneNumberTextField.text]) {
        [self.loginButton setEnabled:NO];
        [self.loginActivityIndicator startAnimating];
        self.phoneNumber = self.phoneNumberTextField.text;
        self.password = self.passwordTextField.text;
        [self requestLogInApi];
    }

}

- (BOOL)isInputLegal:(NSString *)phoneNumber password:(NSString *)password
{
    if (phoneNumber.length != 11 || password.length == 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"输入信息错误" message:@"请检查手机号和密码后重新输入" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *_Nonnull action){
        }];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }

    NSString *regex = @"^1[3|4|5|7|8][0-9]\\d{8}$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL isMatch = [predicate evaluateWithObject:phoneNumber];
    if (!isMatch) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"输入信息错误" message:@"请检查手机号后重新输入" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *_Nonnull action){
        }];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    return YES;
}

#pragma mark - 数据处理

-(void)requestLogInApi{
    
    NSDictionary *params = @{};
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    NSString *deviceToken = [user objectForKey:DEVICE_TOKEN];
    
    if (deviceToken != nil) {
        self.deviceToken = deviceToken;
    }else{
        
    }
    params = @{
               @"lac":self.lac,
               @"mobile" : self.phoneNumber,
               @"password" : [SPDHashGenerator md5:self.password],
               @"device_id" : self.registrationID,
               @"UDID" :[[AppManager sharedInstance] getDeviceId] //2.2.0之后添加的登录上传UDID
               };

    NSMutableDictionary *dict=[NSMutableDictionary dictionaryWithDictionary:params];
    [RequestUtils commonPostRequestUtils:dict bURL:@"login" bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        
        [self saveUserDefaultFormRequestAPI:suceess];
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [self.view removeFromSuperview];
        [appDelegate loginSuccess];
        
    } bFailure:^(id  _Nullable failure) {
        
        [self showMessageToast:failure[@"msg"]];

        [self.loginButton setEnabled:YES];
    }];
    
    
}
- (void)returnKeyboard
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
    [tap addTarget:self action:@selector(dealTap)];
    [self.view addGestureRecognizer:tap];
}

- (void)dealTap
{
    [self.phoneNumberTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.textColor = [UIColor blackColor];
    return YES;
}

#pragma mark - 海外版
- (IBAction)codeBtnClicked:(id)sender {
    
    [self.view endEditing:YES];

//    PickerViewController *pickerViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PickerViewController"];
//    [pickerViewController setPickerType:PickerTypeAreaCode];
//    [pickerViewController setEnsureAreaCodeBtnClickBlock:^(NSString *area,NSString *code){
//        self.lac = [code substringFromIndex:1];
//        [self.codeBtn setTitle:self.lac forState:UIControlStateNormal];
//        self.countryLabel.text = area;
//    }];
//    [pickerViewController presentInViewController:self complection:^{
//
//    }];
    SelectAreaCodeController * vc = [SelectAreaCodeController new];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - SelectAreaCodeControllerDelegate

- (void)areaCodeSelect:(NSString *)area lac:(NSString *)lac {
    self.lac = [lac substringFromIndex:1] ;
    [self.codeBtn setTitle:self.lac forState:UIControlStateNormal];
    self.countryLabel.text = area;
}


@end
