//
//  SelectAreaCodeController.h
//  Seeky
//
//  Created by ling Hou on 2019/7/22.
//  Copyright © 2019 简约互动. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectAreaCodeControllerDelegate<NSObject>

@optional
- (void)areaCodeSelect:(NSString *)area lac:(NSString *)lac;

@end

NS_ASSUME_NONNULL_BEGIN

@interface SelectAreaCodeController : BaseViewController

@property (nonatomic,weak) id<SelectAreaCodeControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
