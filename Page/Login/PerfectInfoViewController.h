//
//  PerfectInfoViewController.h
//  SimpleDate
//
//  Created by 程龙军 on 16/1/18.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "BaseViewController.h"

@interface PerfectInfoViewController : BaseViewController

@property(nonatomic,assign)BOOL isThirdPlatformLogin;
@property(nonatomic,copy)NSDictionary *userInfo;


@end
