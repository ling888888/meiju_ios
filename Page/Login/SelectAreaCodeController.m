//
//  SelectAreaCodeController.m
//  Seeky
//
//  Created by ling Hou on 2019/7/22.
//  Copyright © 2019 简约互动. All rights reserved.
//

#import "SelectAreaCodeController.h"

@interface SelectAreaCodeController ()<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UISearchBar *searchBar;

@property (nonatomic,strong) UITableView * listTable;

@property (nonatomic,strong) NSDictionary * dataDict;

@property (nonatomic,strong) NSMutableArray * dataArray;

@end

@implementation SelectAreaCodeController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = SPDStringWithKey(@"选择国家和地区",nil);
    self.searchBar.frame = CGRectMake(12, NavHeight + 10, kScreenW -24, 30);
    [self.view addSubview:self.searchBar];
    
    if (@available(iOS 11.0, *)) {
        self.listTable.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    self.dataDict = [self readLocalFileWithName:@"AREACODE_zh-Hans"];
    self.dataArray = [self.dataDict[@"area_code"] mutableCopy];

    [self.listTable reloadData];
    
    self.title = SPDStringWithKey(@"区号",nil);
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (NSDictionary *)readLocalFileWithName:(NSString *)name {
    NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:@"json"];
    NSData *data = [[NSData alloc] initWithContentsOfFile:path];
    return [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
}

#pragma mark - TableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellID=@"cellID";
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    NSDictionary * dict = self.dataArray[indexPath.row];
    cell.textLabel.text = SPDStringWithKey(dict[@"name"],nil)?:@"China";
    cell.detailTextLabel.text = dict[@"lac"]?:@"+86";
    cell.accessoryType =  UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary * dict = self.dataArray[indexPath.row];
    if (self.delegate && [self.delegate respondsToSelector:@selector(areaCodeSelect:lac:)]) {
        [self.delegate areaCodeSelect:dict[@"name"]?:@"China" lac:dict[@"lac"]?:@"+86"];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if ([SPDCommonTool isEmpty:searchText] ) {
        self.dataDict = [self readLocalFileWithName:@"AREACODE_zh-Hans"];
        self.dataArray = [self.dataDict[@"area_code"] mutableCopy];
        [self.listTable reloadData];
    }else{
        self.dataDict = [self readLocalFileWithName:@"AREACODE_zh-Hans"];
        NSMutableArray * data = [self.dataDict[@"area_code"] mutableCopy];
        [self.dataArray removeAllObjects];
        for (NSDictionary * dict in data) {
            if ([dict[@"name"] containsString:searchText] || [dict[@"lac"] containsString:searchText]) {
                [self.dataArray  addObject:dict];
            }
        }
        [self.listTable reloadData];
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
}


#pragma mark - setter getter

- (UITableView *)listTable {
    if (!_listTable) {
        _listTable = [[UITableView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.searchBar.frame) + 15, kScreenW, kScreenH - NavHeight-IphoneX_Bottom) style:UITableViewStylePlain];
        _listTable.delegate = self;
        _listTable.dataSource = self;
        _listTable.tableFooterView = [UIView new];
        [self.view addSubview:_listTable];
    }
    return _listTable;
}

- (NSDictionary *)dataDict {
    if (!_dataDict) {
        _dataDict = [NSMutableDictionary new];
    }
    return _dataDict;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

- (UISearchBar *)searchBar {
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc] init];
        _searchBar.delegate = self;
        _searchBar.placeholder = SPDStringWithKey(@"搜索国家和地区",nil);
        _searchBar.backgroundImage = [[UIImage alloc] init];
        
//        [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil] setTitle:SPDStringWithKey(@"取消",nil)];
        UITextField *searchField = [_searchBar valueForKey:@"searchField"];
        if (searchField) {
//            searchField.keyboardType = UIKeyboardTypeASCIICapableNumberPad;
            searchField.backgroundColor = [UIColor colorWithHexString:@"#F0F0F0"];
//            if ([[UIDevice currentDevice].systemVersion floatValue] >= 11) {
//                [searchField setValue:[UIFont systemFontOfSize:15] forKeyPath:@"_placeholderLabel.font"];
//            }
        }
    }
    return _searchBar;
}


@end
