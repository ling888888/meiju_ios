//
//  SPDLoginViewController.m
//  SimpleDate
//
//  Created by Monkey on 2017/10/9.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDLoginViewController.h"
#import "AppDelegate.h"
#import "PerfectInfoViewController.h"
#import "SPDApiUser.h"
#import "AppManager.h"
#import "SPDUMShareUtils.h"
#import "RequestUtils.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "OverSeaRegisterController.h"
#import "SPDSignUpInfoController.h"
#import <AuthenticationServices/AuthenticationServices.h>
#import "WXManager.h"

@interface SPDLoginViewController ()<ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding,WXApiDelegate>

@property (weak, nonatomic) IBOutlet UIButton *fbBtn;
@property (weak, nonatomic) IBOutlet UIButton *wechatBtn;
@property (weak, nonatomic) IBOutlet UIButton *telephoneBtn;
@property (weak, nonatomic) IBOutlet UILabel *agreeLabel;
@property (nonatomic,assign) BOOL clickedUserProtocol;
@property (weak, nonatomic) IBOutlet UIImageView *bgView;
@property (nonatomic, strong) NSMutableDictionary * platformLoginDict;
@property (nonatomic, strong) NSMutableDictionary * platformRegisterDict; //注册需要的参数
@property (nonatomic, copy) NSString *registrationID;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomCons;

@end

@implementation SPDLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.clickedUserProtocol = YES;
    
    self.wechatBtn.hidden = ![WXApi isWXAppInstalled];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hanleWXAuthSuccess:) name:@"WXAuthSuccess" object:nil];
    self.agreeLabel.text = SPDStringWithKey(@"我已同意《Hala》", nil);
    [self.agreeLabel setUserInteractionEnabled:YES];
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dealLabelClicked:)];
    [self.agreeLabel addGestureRecognizer:tap];
    
    
    self.bottomCons.constant = 20 + IphoneX_Bottom;
    
//    if (@available(iOS 13.0, *)) {
//        ASAuthorizationAppleIDButton *appleIDButton = [ASAuthorizationAppleIDButton buttonWithType:ASAuthorizationAppleIDButtonTypeSignIn style:ASAuthorizationAppleIDButtonStyleWhite];
//        [appleIDButton addTarget:self action:@selector(signInWithApple:) forControlEvents:UIControlEventTouchUpInside];
//        appleIDButton.cornerRadius = 37 / 2.0;
//        [self.view addSubview:appleIDButton];
//        [appleIDButton mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.mas_equalTo(self.fbBtn.mas_bottom).offset(30);
//            make.centerX.mas_equalTo(self.fbBtn.mas_centerX);
//            make.width.mas_equalTo(150);
//            make.height.mas_equalTo(37);
//        }];
//    }
}

#pragma mark - ASAuthorizationControllerDelegate

- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithAuthorization:(ASAuthorization *)authorization  API_AVAILABLE(ios(13.0)) {
    
    if ([authorization.credential isKindOfClass:[ASAuthorizationAppleIDCredential class]]) {
        ASAuthorizationAppleIDCredential *credential = authorization.credential;
        NSString *name = [[NSPersonNameComponentsFormatter new] stringFromPersonNameComponents:credential.fullName];
        NSString *authorizationCode = [[NSString alloc] initWithData:credential.authorizationCode encoding:NSUTF8StringEncoding];
        NSString *identityToken = [[NSString alloc] initWithData:credential.identityToken encoding:NSUTF8StringEncoding];
        
        NSDictionary *params = @{@"channel": @"AppStore",
                                 @"user": credential.user,
                                 @"name": name,
                                 @"email": credential.email ? : @"",
                                 @"authorizationCode": authorizationCode,
                                 @"identityToken": identityToken,
                                 @"realUserStatus": @(credential.realUserStatus),
                                 @"lang": [SPDCommonTool getLanguageStrPostToServer:[SPDCommonTool getPreferredLanguage]] ? : @"en"};
        [RequestUtils commonPostRequestUtils:[params mutableCopy] bURL:@"signup.apple" bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
            SPDApiUser *user = [SPDApiUser currentUser];
            [user initWithDefaultsData];
            [user saveUserInfoToDefault:suceess[@"user"]];
            [self saveUserDefaultFormRequestAPI:suceess[@"user"]];
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [appDelegate loginSuccess];
        } bFailure:^(id  _Nullable failure) {
            [self showToast:failure[@"msg"]];
        }];
    }
}

- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithError:(NSError *)error  API_AVAILABLE(ios(13.0)) {
    NSString *errorMsg = nil;
    switch (error.code) {
        case ASAuthorizationErrorCanceled:
            errorMsg = @"用户取消了授权请求";
            break;
        case ASAuthorizationErrorFailed:
            errorMsg = @"授权请求失败";
            break;
        case ASAuthorizationErrorInvalidResponse:
            errorMsg = @"授权请求响应无效";
            break;
        case ASAuthorizationErrorNotHandled:
            errorMsg = @"未能处理授权请求";
            break;
        case ASAuthorizationErrorUnknown:
            errorMsg = @"授权请求失败未知原因";
            break;
        default:
            break;
    }
    NSLog(@"%@", errorMsg);
}

#pragma mark - ASAuthorizationControllerPresentationContextProviding

- (ASPresentationAnchor)presentationAnchorForAuthorizationController:(ASAuthorizationController *)controller  API_AVAILABLE(ios(13.0)) {
    return self.view.window;
}

#pragma mark - Event responses

- (void)signInWithApple:(ASAuthorizationAppleIDButton *)sender  API_AVAILABLE(ios(13.0)) {
    ASAuthorizationAppleIDProvider *provider = [[ASAuthorizationAppleIDProvider alloc] init];
    ASAuthorizationAppleIDRequest *request = [provider createRequest];
    request.requestedScopes = @[ASAuthorizationScopeFullName, ASAuthorizationScopeEmail];
    
    ASAuthorizationController *controller = [[ASAuthorizationController alloc] initWithAuthorizationRequests:@[request]];
    controller.delegate = self;
    controller.presentationContextProvider = self;
    [controller performRequests];
}

- (IBAction)fbButtonClicked:(id)sender {
    if (self.clickedUserProtocol) {
        [self.platformLoginDict setObject:@"facebook" forKey:@"platform"];
        [self requestAuthWithFacebook];
    }else{
        [self showToast:SPDStringWithKey(@"请同意《Hala》后登录", nil)];
    }
}

- (IBAction)wechatButtonClicked:(id)sender {
    if (self.clickedUserProtocol) {
        [self.platformLoginDict setObject:@"wechat" forKey:@"platform"];
        [[WXManager sharedInstance] sendLoginReq];
    }else{
        [self showToast:SPDStringWithKey(@"请同意《Hala》后登录", nil)];
    }
}

- (IBAction)telephoneButtonClicked:(id)sender {
    [self performSegueWithIdentifier:@"FAMYToLogView" sender:self];
}

-(void)dealLabelClicked:(UITapGestureRecognizer *)tap {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:URL_PrivacyPolicy]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//login with fb
- (void)requestAuthWithFacebook {
    FBSDKLoginManager *login_manager = [[FBSDKLoginManager alloc] init];
    [login_manager logOut];
    [login_manager logInWithPermissions:@[@"public_profile", @"user_friends"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult * _Nullable result, NSError * _Nullable error) {
        if (error) {
        } else if (result.isCancelled) {
        } else {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [self handleFacebookUserInfoWithFBSDKLoginManagerLoginResult:result];
            
        }
    }];
}

- (void)handleFacebookUserInfoWithFBSDKLoginManagerLoginResult:(FBSDKLoginManagerLoginResult *)result {
    
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  
                                  initWithGraphPath:result.token.userID
                                  
                                  parameters:@{@"fields": @"id,name,picture,gender"}
                                  
                                  HTTPMethod:@"GET"];
    
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id user_result,NSError *error) {
        
        if (!error) {
//            BOOL registered = NO; ,ids_for_business
//            for (NSDictionary *dic in user_result[@"ids_for_business"][@"data"]) {
//                if ([dic[@"app"][@"id"] integerValue] == 484025978643192) {
//                    [self.platformLoginDict setObject:dic[@"id"] forKey:@"openid"];
//                    registered = YES;
//                }
//            }
//            if (!registered) {
//                [self.platformLoginDict setObject:user_result[@"id"] forKey:@"openid"];
//            }
            [self.platformLoginDict setObject:user_result[@"id"] forKey:@"openid"];
            [self.platformLoginDict setObject:@"facebook-unionid" forKey:@"unionid"];
            [self.platformLoginDict setObject:result.token.tokenString forKey:@"access_token"];
            [self.platformRegisterDict setValuesForKeysWithDictionary:self.platformLoginDict];
            [self.platformRegisterDict setValue:user_result[@"name"] forKey:@"username"];
            [self.platformRegisterDict setValue:user_result[@"name"] forKey:@"nickname"];
            [self.platformRegisterDict setValue:user_result[@"gender"] forKey:@"gender"];
            [self.platformRegisterDict setValue:user_result[@"picture"][@"data"][@"url"] forKey:@"iconURL"];
            // 获取到用户信息看看是否是登陆过
            [self requestPlatformLogin];
            
        }
    }];
    
}
// request
- (void)requestPlatformLogin {
    
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:self.platformLoginDict] bURL:@"platform.login" bAnimated:YES bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    
        BOOL firstlogin = [suceess[@"firstlogin"] boolValue];
        if (firstlogin) {
            if ([self.platformLoginDict[@"platform"] isEqualToString:@"wechat"]) {
                [self getWXUserInfo:self.platformLoginDict];
            }else{
                [self performSegueWithIdentifier:@"LogToInfo" sender:self.platformRegisterDict];
            }
            
        }else{//firstlogin
            NSDictionary *userInfoDict=suceess[@"user"];
            SPDApiUser *users=[SPDApiUser currentUser];
            [users initWithDefaultsData];
            [users saveUserInfoToDefault:userInfoDict];
            [self saveUserDefaultFormRequestAPI:suceess[@"user"]];
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [appDelegate loginSuccess];
        }
    } bFailure:^(id  _Nullable failure) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showToast:failure[@"msg"]];
    }];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"LogToInfo"]) {
        UINavigationController *nav = segue.destinationViewController;
        PerfectInfoViewController *vc = nav.viewControllers[0];
        vc.isThirdPlatformLogin = true;
        vc.userInfo = sender;
    }
}

#pragma mark - WeeXinn

- (void)hanleWXAuthSuccess:(NSNotification *)notify {
    [RequestUtils SpecialGetRequestUtils:[@{@"appid":WXAppId,@"secret":WXAppSecret,@"code":notify.userInfo[@"code"],@"grant_type":@"authorization_code"} mutableCopy] bURL:@"https://api.weixin.qq.com/sns/oauth2/access_token" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        
        NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:suceess options:NSJSONReadingAllowFragments error:nil];
        [self.platformLoginDict setObject:dict[@"openid"] forKey:@"openid"];
        [self.platformLoginDict setObject:dict[@"unionid"] forKey:@"unionid"];
        [self.platformLoginDict setObject:dict[@"access_token"] forKey:@"access_token"];
        [self requestPlatformLogin];
        
    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)getWXUserInfo:(NSDictionary *)dict {
    
    [RequestUtils SpecialGetRequestUtils:[@{@"access_token":dict[@"access_token"],@"openid":dict[@"openid"]} mutableCopy] bURL:@"https://api.weixin.qq.com/sns/userinfo" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        
        NSDictionary * userInfoDict = [NSJSONSerialization JSONObjectWithData:suceess options:NSJSONReadingAllowFragments error:nil];
        [self.platformRegisterDict setValuesForKeysWithDictionary:dict];
        [self.platformRegisterDict setValue:userInfoDict[@"nickname"] forKey:@"username"];
        [self.platformRegisterDict setValue:userInfoDict[@"nickname"] forKey:@"nickname"];
        [self.platformRegisterDict setObject:[userInfoDict[@"sex"] intValue] == 2 ? @"female":@"male"  forKey:@"gender"];
        [self.platformRegisterDict setObject:userInfoDict[@"headimgurl"] forKey:@"iconURL"];
        [self performSegueWithIdentifier:@"LogToInfo" sender:self.platformRegisterDict];
        
    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (NSString *)registrationID {
    if (!_registrationID) {
        _registrationID = @"";
    }
    return _registrationID;
}

- (NSMutableDictionary *)platformLoginDict {
    if (!_platformLoginDict) {
        _platformLoginDict = [NSMutableDictionary new];
        [_platformLoginDict setValue:self.registrationID forKey:@"device_id"];
        [_platformLoginDict setValue:[[AppManager sharedInstance] getDeviceId] forKey:@"UDID"];
    }
    return _platformLoginDict;
}

- (NSMutableDictionary *)platformRegisterDict {
    if (!_platformRegisterDict) {
        _platformRegisterDict = [NSMutableDictionary new];
    }
    return _platformRegisterDict;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
