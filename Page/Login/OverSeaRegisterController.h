//
//  OverSeaRegisterController.h
//  SimpleDate
//
//  Created by Monkey on 2017/8/18.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIButton+HHImagePosition.h"

@interface OverSeaRegisterController : BaseViewController

@property (assign, nonatomic) BOOL isRegister;
    
@property (assign, nonatomic) BOOL isResetPwd;//修改密码
    
@end
