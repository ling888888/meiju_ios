//
//  OverSeaRegisterController.m
//  SimpleDate
//
//  Created by Monkey on 2017/8/18.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "OverSeaRegisterController.h"
#import "PickerViewController.h"
#import "PerfectInfoViewController.h"
#import "SPDHashGenerator.h"
#import "SelectAreaCodeController.h"

@interface OverSeaRegisterController ()<UITextFieldDelegate,SelectAreaCodeControllerDelegate>
    
    {
        NSTimer* timer;
    }
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topLeadingConstraint;
@property (weak, nonatomic) IBOutlet UIButton *codeBtn;
    @property (weak, nonatomic) IBOutlet UILabel *countryLabel;

@property (weak, nonatomic) IBOutlet UIView *invitationView;
@property (weak, nonatomic) IBOutlet UIButton *getVerificationBtn;
@property (weak, nonatomic) IBOutlet UIButton *timeBtn;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *verificationTextField;
@property (weak, nonatomic) IBOutlet UILabel *privacyLink;
@property (weak, nonatomic) IBOutlet UILabel *serviceLink;
@property (weak, nonatomic) IBOutlet UIButton *nextStepButton;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet UIView *pwdView;

@property(nonatomic, copy) NSString* phoneNumber;
@property(nonatomic, copy) NSString* verification;
@property(nonatomic, copy) NSString* invitaionCodeString;

@property(nonatomic, assign) int second;
@property (weak, nonatomic) IBOutlet UITextField *myPasswordTextFeild;

@property(nonatomic, copy) NSString *password;
@property(nonatomic, copy) NSString *lac; //区号
@property (weak, nonatomic) IBOutlet UILabel *lajiLabel;

@end

@implementation OverSeaRegisterController

    
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //默认选择中国
    [self.codeBtn setTitle:@"966" forState:UIControlStateNormal];
    [self.codeBtn setImage:[UIImage imageNamed:@"ic_code_yanzheng"] forState:UIControlStateNormal];
    [self.codeBtn setImagePosition:HHImagePositionRight spacing:3.0f];
    self.countryLabel.text = @"Saudi Arabia";
    self.lac = @"966";
    [self.registerButton setBackgroundColor:SPD_HEXCOlOR(COMMON_PINK)];
    
    //设置placeholder
    self.phoneNumberTextField.placeholder = SPDStringWithKey(@"请输入您的手机号", nil);
    self.myPasswordTextFeild.placeholder = SPDStringWithKey(@"设置您的密码(6-16位)", nil);
    self.verificationTextField.placeholder = SPDStringWithKey(@"输入您收到的验证码", nil);
    [self.registerButton setTitle:SPDStringWithKey(@"注册", nil) forState:UIControlStateNormal];
    [self.registerButton setTitle:SPDStringWithKey(@"注册", nil) forState:UIControlStateHighlighted];
    [self.getVerificationBtn setTitle:SPDStringWithKey(@"获取验证码", nil) forState:UIControlStateNormal];
    
    [self registeNotification];

    [self requestSignupAreaCode];
    
}
    
- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (!self.isRegister) {
        [self.navigationItem setTitle:SPDStringWithKey(@"找回密码", nil)];
        [self.registerButton setTitle:SPDStringWithKey(@"找回密码", nil) forState:UIControlStateNormal];
    }else{
        [self.navigationItem setTitle:SPDStringWithKey(@"注册", nil)];
    }
    self.navigationController.navigationBar.hidden = false;
    self.second = 60;
}

- (void)registeNotification {
    
//增加监听，当键盘出现或改变时收出消息
[[NSNotificationCenter defaultCenter] addObserver:self
                                         selector:@selector(keyboardWillShow:)
                                             name:UIKeyboardWillShowNotification
                                           object:nil];
[[NSNotificationCenter defaultCenter] addObserver:self
                                         selector:@selector(keyboardWillHide:)
                                             name:UIKeyboardWillHideNotification
                                           object:nil];

}

- (void)keyboardWillShow:(NSNotification *)notify {
    if (!IS_IPAD) {return;}
    CGFloat height = CGRectGetMaxY(self.pwdView.frame);
    CGRect keyboardRect = [[[notify userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    if (height +keyboardRect.size.height  >= kScreenH - 30) {
        [UIView animateWithDuration:0.3 animations:^{
            self.topLeadingConstraint.constant = -44;
        }];
    }
}

- (void)keyboardWillHide:(NSNotification *)notify {
    if (!IS_IPAD) {return;}
    [UIView animateWithDuration:0.3 animations:^{
        self.topLeadingConstraint.constant = 0;
    }];
}

- (void)requestSignupAreaCode {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"signup.areaCode" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if (suceess[@"areaCode"] && suceess[@"areaName"]) {
            self.lac = [suceess[@"areaCode"] isKindOfClass:[NSString class]] ? suceess[@"areaCode"]: [suceess[@"areaCode"] stringValue];
            self.countryLabel.text = suceess[@"areaName"];
            [self.codeBtn setTitle:self.lac forState:UIControlStateNormal];
            [self.codeBtn setImage:[UIImage imageNamed:@"ic_code_yanzheng"] forState:UIControlStateNormal];
            [self.codeBtn setImagePosition:HHImagePositionRight spacing:3.0f];
        }
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (IBAction)codeBtnClicked:(id)sender {
    
    [self.view endEditing:YES];
    
    SelectAreaCodeController * vc = [SelectAreaCodeController new];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
    
//    PickerViewController *pickerViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PickerViewController"];
//    [pickerViewController setPickerType:PickerTypeAreaCode];
//    [pickerViewController setEnsureAreaCodeBtnClickBlock:^(NSString *area,NSString *code){
//        self.lac = [code substringFromIndex:1];
//        [self.codeBtn setTitle:self.lac forState:UIControlStateNormal];
//        [self.codeBtn setImage:[UIImage imageNamed:@"ic_code_yanzheng"] forState:UIControlStateNormal];
//        [self.codeBtn setImagePosition:HHImagePositionRight spacing:3.0f];
//        self.countryLabel.text = area;
//    }];
//    [pickerViewController presentInViewController:self complection:^{
//
//    }];
    
    
}
    
- (void)viewDidDisappear:(BOOL)animated {
    [timer invalidate];
    [self.navigationItem setTitle:@""];
}
    
- (void)setupLinkToSafari{
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    [self.privacyLink setUserInteractionEnabled:YES];
    [self.serviceLink setUserInteractionEnabled:YES];
    
    self.privacyLink.attributedText = [[NSAttributedString alloc] initWithString:self.privacyLink.text attributes:underlineAttribute];
    
    self.serviceLink.attributedText = [[NSAttributedString alloc] initWithString:self.serviceLink.text attributes:underlineAttribute];
}
    
- (IBAction)privacyLinkClickToSafari:(UITapGestureRecognizer *)sender {
    [self jumpToSafari:[NSURL URLWithString:URL_PrivacyPolicy]];
}
    
- (IBAction)serviceLinkClickToSafari:(UITapGestureRecognizer *)sender {
    [self jumpToSafari:[NSURL URLWithString:URL_TOS]];
}
    
- (IBAction)viewTappedToResignFirstResponder:(id)sender {
    [self.phoneNumberTextField resignFirstResponder];
    [self.verificationTextField resignFirstResponder];
}
# pragma mark - UITextFieldDelegate
    
- (BOOL) textFieldShouldReturn:(UITextField*)textField {
    [textField resignFirstResponder];
    return YES;
}
    
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    textField.textColor = [UIColor blackColor];
    return YES;
}
    
# pragma mark - Interface Builder interactive
- (IBAction) getVerificationButtonTapped:(UIButton *)sender {
    // 显示正在获取后台消息，若callback显示成功
    if ([SPDCommonTool isLegalPhoneNumber:self.phoneNumberTextField.text]) {
        [self.getVerificationBtn setEnabled:NO];
        self.getVerificationBtn.alpha           = 0.5;
        [self.getVerificationBtn setTitle:SPDStringWithKey(@"获取中", nil) forState:UIControlStateNormal];
        self.phoneNumber = self.phoneNumberTextField.text;
        //没有 正则表达式判断了
        if (self.isRegister) {
            [self requestSignUpSmsAPIWithUrl:@"signup.sms"];
        } else {
            [self requestSignUpSmsAPIWithUrl:@"findpwd.sms"];
            
        }
    }else{
        [self showToast:@"please input your telphone"];
    }
   
}
    
-(BOOL)isPhoneNumberLegal:(NSString *)phoneNumber {
    if (phoneNumber.length != 11) {
        UIAlertController *alert     = [UIAlertController alertControllerWithTitle:@"输入信息错误" message:@"请检查手机号后重新输入" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    NSString *regex        = @"^1[3|4|5|7|8][0-9]\\d{8}$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL isMatch           = [predicate evaluateWithObject:phoneNumber];
    if (!isMatch) {
        UIAlertController *alert     = [UIAlertController alertControllerWithTitle:@"输入信息错误" message:@"请检查手机号后重新输入" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    return YES;
}
    
    //注册账号点击的
- (IBAction) nextStepButtonTapped:(UIButton *)sender {
    
    if ([SPDCommonTool isLegalPhoneNumber:self.phoneNumberTextField.text] && ![SPDCommonTool isEmpty:self.verificationTextField.text] && ![SPDCommonTool isEmpty:self.myPasswordTextFeild.text]) {
        [self.nextStepButton setEnabled:NO];
        self.phoneNumber  = self.phoneNumberTextField.text;
        self.verification = self.verificationTextField.text;
        self.password     = self.myPasswordTextFeild.text;
        [self checkPswandVerifyCode];
    }else {
        [self showMessageToast:SPDStringWithKey(@"格式不正确,请重新输入", nil)];
    }
    
}

- (void)checkPswandVerifyCode {
    
    if ([SPDCommonTool isEmpty:self.password] || [SPDCommonTool isEmpty:self.verification] || self.password.length < 6 || self.password.length >16) {
        [self showToast:SPDStringWithKey(@"格式不正确,请重新输入", nil)];
        self.nextStepButton.enabled = YES;
    }else{
        if (self.isRegister) {
            [self requestCheakPswandVerifyCodeApiWithUrl:@"signup.mobile.pwd"];
        } else {
//            [self requestCheakPswandVerifyCodeApiWithUrl:@"findpwd.mobile"];
            [self requestSignUpMobileApiWithUrl:@"findpwd.mobile"];
        }
    }
    
}
    

- (IBAction)backButtonTapped:(id)sender {
    if (self.navigationController.viewControllers.count == 1) {
        [self dismissViewControllerAnimated:true completion:nil];
    }else {
        [self.navigationController popViewControllerAnimated:true];
    }
}
    
-(void)cheakPassWord{
    
    if (self.password.length == 0) {
        UIAlertController *alert     = [UIAlertController alertControllerWithTitle:@"输入错误" message:@"密码不能为空" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    if (self.password.length <=5) {
        UIAlertController *alert     = [UIAlertController alertControllerWithTitle:@"密码位数不足" message:@"请输入6位及以上的密码" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    if (self.password.length>=6&&self.password.length<=16) {
        //密码格式正确 开始验证验证码 与手机号
        if (self.isRegister) {
            [self requestSignUpMobileApiWithUrl:@"signup.mobile"];
            
        } else {
            [self requestSignUpMobileApiWithUrl:@"findpwd.mobile"];
        }
    }
    
}
    
# pragma mark - 数据处理
    
-(void)requestSignUpSmsAPIWithUrl:(NSString *)url{
    
    NSDictionary*  params = @{
                              @"lac":self.lac,
                              @"mobile": self.phoneNumber,
                              };
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:params] bURL:url bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                 target:self
                                               selector:@selector(timerFireMethod:)
                                               userInfo:nil
                                                repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        [self.getVerificationBtn setTitle:[NSString stringWithFormat:@"%d",self.second] forState:UIControlStateNormal];
        [self.getVerificationBtn setBackgroundColor:SPD_HEXCOlOR(@"cacaca")];
        [self.getVerificationBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    } bFailure:^(id  _Nullable failure) {
        [self.getVerificationBtn setEnabled:YES];
        self.getVerificationBtn.alpha           = 1.0;
        [self.getVerificationBtn setTitle:SPDStringWithKey(@"获取验证码", nil) forState:UIControlStateNormal];
        [self showMessageToast:failure[@"msg"]];
    }];
    
}

//9.12change api
-(void)requestCheakPswandVerifyCodeApiWithUrl:(NSString *)url{
    
    NSMutableDictionary * param = [NSMutableDictionary new];
    [param setObject:self.phoneNumber forKey:@"mobile"];
    [param setObject:self.verification forKey:@"verify_code"];
    [param setObject:[SPDHashGenerator md5:self.password] forKey:@"pwd"];
    [param setObject:self.lac forKey:@"lac"];

    [RequestUtils commonPostRequestUtils:param bURL:url bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        
        if ([url isEqualToString:@"findpwd.mobile"]) {
            [self showToast:SPDStringWithKey(@"修改密码成功", nil)];
            [self dismissViewControllerAnimated:YES completion:nil];
        }else{
            [self performSegueWithIdentifier:@"OverSeaGoToInfoView" sender:self];
        }
        
    } bFailure:^(id  _Nullable failure) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showMessageToast:failure[@"msg"]];
        });
    }];
    
}

//找回密码需要先提交mobile 一个api --》验证密码一个api
-(void)requestSignUpMobileApiWithUrl:(NSString *)url{
    
    NSDictionary *params = @{
                             @"lac":self.lac,
                             @"mobile": self.phoneNumber,
                             @"verify_code": self.verification,
                             };
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:params] bURL:url bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        
        if (self.isRegister) {
//            [self requestSignUpPasswordAPIWithUrl:@"signup.password"];
        }else{
            [self requestSignUpPasswordAPIWithUrl:@"findpwd.reset"];
        }
        
    } bFailure:^(id  _Nullable failure) {
        [self showMessageToast:failure[@"msg"]];
    }];
    
    
}
    
    //验证密码
-(void)requestSignUpPasswordAPIWithUrl:(NSString *)url{
    
    NSDictionary *params = @{
                             @"password": [SPDHashGenerator md5:self.password]
                             };
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:params] bURL:url bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        
        if ([url isEqualToString:@"findpwd.reset"]) {
            [self showToast:SPDStringWithKey(@"修改密码成功", nil)];
            [self dismissViewControllerAnimated:YES completion:nil];
        }else{
            [self performSegueWithIdentifier:@"OverSeaGoToInfoView" sender:self];
        }
        
    } bFailure:nil];
    
}
    
    
    
# pragma mark - NSTimer
    
- (void)timerFireMethod:(NSTimer*)theTimer {
    if (self.second == 1) {
        [theTimer invalidate];
        self.timeBtn.hidden = YES;
        self.second = 60;
        [self.getVerificationBtn setTitle:SPDStringWithKey(@"点击重新获取", nil) forState:UIControlStateNormal];
        [self.getVerificationBtn setBackgroundColor:[UIColor whiteColor]];
        self.getVerificationBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [self.getVerificationBtn setTitleColor:SPD_HEXCOlOR(@"666666") forState:UIControlStateNormal];
        [self.getVerificationBtn setEnabled:YES];
        self.getVerificationBtn.alpha = 1.0;
    } else {
        self.getVerificationBtn.enabled         = NO;
        self.second--;
        NSString* title = [NSString stringWithFormat:@"%d", self.second];
        [self.getVerificationBtn setTitle:title forState:UIControlStateNormal];
    }
}
 
# pragma mark - memory management
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}
    
- (IBAction)invitationTextFieldDIdChanged:(UITextField *)sender {
    [self setInvitaionCodeString:sender.text];
}
    
- (IBAction)getInvationCodeClick:(UIButton *)sender {
    [self performSegueWithIdentifier:@"PushToInvitationCode" sender:self];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - SelectAreaCodeControllerDelegate

- (void)areaCodeSelect:(NSString *)area lac:(NSString *)lac {
    self.lac = [lac substringFromIndex:1] ;
    [self.codeBtn setTitle:self.lac forState:UIControlStateNormal];
    self.countryLabel.text = area;
}


@end
