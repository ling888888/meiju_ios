//
//  PerfectInfoViewController.m
//  SimpleDate
//
//  Created by 程龙军 on 16/1/18.
//  Copyright © 2016年 WYB. All rights reserved.
//


#import "PerfectInfoViewController.h"
#import "UIImage+Resize.h"
#import "PickerViewController.h"
#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>

@interface PerfectInfoViewController () < UITextFieldDelegate,CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;

@property (nonatomic, copy) NSString *genderString;

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UIButton *citySelectedButton;
@property (weak, nonatomic) IBOutlet UIButton *birthdaySelectedButton;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *birthdayLabel;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet UIButton *maleSelectedButton;
@property (weak, nonatomic) IBOutlet UIButton *femaleSelectedButton;
@property (weak, nonatomic) IBOutlet UIButton *selectBirthDayButton;
@property (weak, nonatomic) IBOutlet UIButton *agreeSendBtn;

@property (nonatomic, assign) BOOL isUserPostHeadView;
@property (nonatomic, strong) NSMutableDictionary *parmsForAPI;
@property (weak, nonatomic) IBOutlet UIImageView *takePicImageView;
@property (nonatomic, copy)   __block NSString *myProviceAndCity;
@property (weak, nonatomic) IBOutlet UITextField *invite_codeTextFeild;
@property (weak, nonatomic) IBOutlet UILabel *inviteCodeNameLabel;
@property (nonatomic, strong) CLLocationManager* clLocationManager;
@property (nonatomic,copy)    NSString *currentCity;//城市
@end


@implementation PerfectInfoViewController

- (NSMutableDictionary *)parmsForAPI{
    if (!_parmsForAPI) {
        _parmsForAPI = [NSMutableDictionary new];
    }
    return _parmsForAPI;
}

- (CLLocationManager *)clLocationManager{
    if (!_clLocationManager) {
        _clLocationManager = [[CLLocationManager alloc]init];
        _clLocationManager.delegate = self;
        _clLocationManager.desiredAccuracy = kCLLocationAccuracyBest;
    }
    return _clLocationManager;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self createNav];
    
    [self.takePicImageView setUserInteractionEnabled:YES];
    
    [self thirdLoginGetPlatFormBirth];
    
    //防止location == null
    self.myProviceAndCity = @"火星 火星 火星";
    
    [self.parmsForAPI setObject:self.myProviceAndCity forKey:@"city"];
    if ([CLLocationManager locationServicesEnabled]) {
        [self.clLocationManager startUpdatingLocation];
    }
}
#pragma mark - 定位成功
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    [self.clLocationManager stopUpdatingLocation];
    CLLocation *currentLocation = [locations lastObject];
    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
    //当前的经纬度
    NSLog(@"当前的经纬度 %f,%f",currentLocation.coordinate.latitude,currentLocation.coordinate.longitude);
    NSTimeInterval locationAge = -[currentLocation.timestamp timeIntervalSinceNow];
    if (locationAge > 1.0){//如果调用已经一次，不再执行
        //        return;
    }
    [geoCoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        if (placemarks.count >0) {
            CLPlacemark *placeMark = placemarks[0];
            _currentCity = placeMark.locality;
            if (!_currentCity) {
                _currentCity = @"无法定位当前城市";
            }
            self.myProviceAndCity = [NSString stringWithFormat:@"%@ %@ %@",placeMark.country?:@"火星",_currentCity?:@"火星",placeMark.subLocality?:@"火星"];
            [self.parmsForAPI setObject:self.myProviceAndCity forKey:@"city"];
        }else if (error == nil && placemarks.count){

            self.myProviceAndCity = @"火星 火星 火星";
        }else if (error){
            self.myProviceAndCity = @"火星 火星 火星";
            NSLog(@"loction error:%@",error);
        }
    }];
}

#pragma mark - 定位失败
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    self.myProviceAndCity = SPDStringWithKey(@"火星 火星 火星", nil);
}

- (void)thirdLoginGetPlatFormBirth {
//    [self.selectBirthDayButton setTitle:@"1997-01-01" forState:UIControlStateNormal];
    if (self.isThirdPlatformLogin) {
        [self.parmsForAPI setValue:@"1997-01-01" forKey:@"birth"];
    } else {
        [self.parmsForAPI setValue:@"1997-01-01" forKey:@"birthday"];
    }
}

-(void)createNav {
    
    self.title = SPDStringWithKey(@"完善资料", nil);
    
    UIBarButtonItem *left = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ic_arrow_left"] style:UIBarButtonItemStylePlain target:self action:@selector(dealBarLeftTap:)];
    self.navigationItem.leftBarButtonItem = left;

}

-(void)dealBarLeftTap:(UIBarButtonItem *)item{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.contentViewHeight.constant = 150;
    
    [self LocalizedString];
    
    if (self.isThirdPlatformLogin) {
        [self bindWithData];
    } else {
//        [self maleButtonTapped:self.maleSelectedButton];
        self.genderString = @"male";
    }

    UITapGestureRecognizer *avatarImageViewTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dealAvatarTap:)];
    [self.avatarImageView addGestureRecognizer:avatarImageViewTap];
    [self.avatarImageView setClipsToBounds:YES];
    
}

-(void)LocalizedString{
   
    [self.femaleSelectedButton setTitle:SPDStringWithKey(@"女", nil) forState:UIControlStateNormal];
    [self.maleSelectedButton setTitle:SPDStringWithKey(@"男", nil) forState:UIControlStateNormal];
    self.userNameTextField.placeholder = SPDStringWithKey(@"请先填写您的昵称", nil);
    self.invite_codeTextFeild.placeholder = SPDStringWithKey(@"没有可不填", nil);
    self.inviteCodeNameLabel.text = SPDStringWithKey(@"邀请码", nil);
    
    [self.confirmButton setTitle:SPDStringWithKey(@"确定", nil) forState:UIControlStateNormal];
    [self.agreeSendBtn setTitle:SPDStringWithKey(@"允许Hala帮你认识更多好友", nil) forState:UIControlStateNormal];
    if ([SPDCommonTool currentVersionIsArbic]) {
        self.agreeSendBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -4);
    }
}
-(void)bindWithData{
    
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:self.userInfo[@"iconURL"]] placeholderImage:[UIImage imageNamed:@"default_avatar"]];

    [self setIsUserPostHeadView:YES];
    
    self.userNameTextField.text = [self.userInfo[@"username"] length] > 8 ? [self.userInfo[@"username"] substringToIndex:8] : self.userInfo[@"username"];
    
    [self.parmsForAPI setObject:self.userInfo[@"username"] forKey:@"nickname"];
    
    if ([self.userInfo[@"gender"] isEqualToString:@"female"]) {
        [self femaleButtonTapped:self.femaleSelectedButton];
    } else {
        [self maleButtonTapped:self.maleSelectedButton];
    }
}

#pragma mark - 定位
- (IBAction)takePic:(id)sender {
    [self presentImagePickerController:SPDStringWithKey(@"上传头像", nil)];
}

- (IBAction)uploadPic:(id)sender
{
    self.avatarImageView.backgroundColor = [UIColor clearColor];
//    [self editAvatarImage];
    
    [self presentImagePickerController:SPDStringWithKey(@"上传头像", nil)];

}

-(void)dealAvatarTap:(UIGestureRecognizer *)tap{
    
    self.avatarImageView.backgroundColor = [UIColor clearColor];
    [self presentImagePickerController:SPDStringWithKey(@"上传头像", nil)];
}

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info {
    //something after selected image
    UIImage *image = info[UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        
        UIImage *resizedImage = [image resizedImageToFitInSize:CGSizeMake(200, 200) scaleIfSmaller:NO];
        
        self.avatarImageView.image = resizedImage;
        
        [self setIsUserPostHeadView:YES];
    }];
}

#pragma mark -portraitImageView getter

- (IBAction)selectBirth:(id)sender {
    [self.view endEditing:YES];
    
    PickerViewController *pickerViewController =  [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PickerViewController"];
    __weak __typeof(self) weakSelf = self;
    [pickerViewController setPickerType:PickerTypeBirthDay];
    [pickerViewController setEnsureBtnClickBlock:^(NSString *myText) {
        if (weakSelf.isThirdPlatformLogin) {
            [weakSelf.parmsForAPI setValue:myText forKey:@"birth"];
        }else{
            [weakSelf.parmsForAPI setValue:myText forKey:@"birthday"];
        }
        [weakSelf.selectBirthDayButton setTitle:myText forState:UIControlStateNormal];
    }];
    
    [pickerViewController presentInViewController:self complection:^{
        
    }];
}

- (IBAction)selectCity:(id)sender {
    
    [self.view endEditing:YES];
    
    PickerViewController *pickerViewController =  [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PickerViewController"];
    [pickerViewController setPickerType:PickerTypeCity];
    __weak __typeof(self) weakSelf = self;
    [pickerViewController setEnsureBtnClickBlock:^(NSString *mytext) {
        [weakSelf.parmsForAPI setValue:mytext forKey:@"city"];
        [weakSelf.cityLabel setText:[SPDCommonTool getStringFromCityWholeText:mytext]];
    }];
    [pickerViewController presentInViewController:self complection:^{
        
    }];
}

#pragma mark -性别
- (IBAction)maleButtonTapped:(id)sender
{
    // TODO: change the image of selected button to highlightened
    [self.maleSelectedButton setImage:[UIImage imageNamed:@"ic_genderMale2"] forState:UIControlStateNormal];
    [self.maleSelectedButton setTitleColor:[UIColor colorWithHexString:@"6ebff5"] forState:UIControlStateNormal];
    [self.femaleSelectedButton setImage:[UIImage imageNamed:@"ic_genderFemale1"] forState:UIControlStateNormal];
    [self.femaleSelectedButton setTitleColor:[UIColor colorWithHexString:@"dadada"] forState:UIControlStateNormal];
    self.genderString = @"male";
}

- (IBAction)femaleButtonTapped:(id)sender
{
    // TODO: change the image of selected button to highlightened
    [self.maleSelectedButton setImage:[UIImage imageNamed:@"ic_genderMale1"] forState:UIControlStateNormal];
    [self.maleSelectedButton setTitleColor:[UIColor colorWithHexString:@"dadada"] forState:UIControlStateNormal];
    [self.femaleSelectedButton setImage:[UIImage imageNamed:@"ic_genderFemale2"] forState:UIControlStateNormal];
    [self.femaleSelectedButton setTitleColor:[UIColor colorWithHexString:@"ffb9fd"] forState:UIControlStateNormal];
    self.genderString = @"female";
}

#pragma userTextField 用户名

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)dealTap {
    [self.userNameTextField resignFirstResponder];
}

- (IBAction)clickAgreeSendBtn:(UIButton *)sender {
    sender.selected = !sender.selected;
}

- (IBAction)confirmButtonTapped:(id)sender {
        
//    if (!self.isUserPostHeadView) {
//        [self showMessageToast:SPDStringWithKey(@"请上传头像", nil)];
//        return;
//
//    }else  if (self.genderString == nil) {
//        [self showMessageToast:SPDStringWithKey(@"请选择性别", nil)];
//        return;
//    }else if ([SPDCommonTool isEmpty:self.userNameTextField.text]) {
//        [self showMessageToast:SPDStringWithKey(@"请输入昵称～", nil)];
//        return;
//    }else if (self.selectBirthDayButton.titleLabel.text == nil) {
//        [self showMessageToast:SPDStringWithKey(@"请输入生日～", nil)];
//        return;
//    }else
    if (self.userNameTextField.text .length > 8) {
        [self showMessageToast:SPDStringWithKey(@"昵称长度超过限制～", nil)];
        return;
    }else{
        
        [self getUserInfoDictWithIsThirdPlatformLogin:self.isThirdPlatformLogin];
        
        if (self.isThirdPlatformLogin) {
            
            [self postDataToServerWithUrl:@"platform.signup"];
            
        }else{
            [self postDataToServerWithUrl:@"signup.info"];
        }
        
    }
    


}

//处理一下需要上传的参数
-(void)getUserInfoDictWithIsThirdPlatformLogin: (BOOL)isThirdPlatformLogin{
    
    [self.parmsForAPI setObject:@"AppStore" forKey:@"channel"];

    if (self.invite_codeTextFeild.text) {
        NSString *invite_code= self.invite_codeTextFeild.text;
        [self.parmsForAPI setObject:invite_code forKey:@"invite_code"];
    }

    if (isThirdPlatformLogin) {
        [self.parmsForAPI setObject:self.userInfo[@"openid"] forKey:@"openid"];
        [self.parmsForAPI setObject:self.genderString forKey:@"gender"];
        [self.parmsForAPI setObject:self.userInfo[@"access_token"] forKey:@"access_token"];
        [self.parmsForAPI setObject:self.userInfo[@"platform"] forKey:@"platform"];
        if (![SPDCommonTool isEmpty:self.userNameTextField.text]) {
            [self.parmsForAPI setObject:self.userNameTextField.text forKey:@"username"];
        }
        [self.parmsForAPI setObject:self.userInfo[@"unionid"] forKey:@"unionid"];

    }else{
        [self.parmsForAPI setObject:self.genderString forKey:@"gender"];
        if (![SPDCommonTool isEmpty:self.userNameTextField.text]) {
            [self.parmsForAPI setObject:self.userNameTextField.text forKey:@"nick_name"];
        }
    }
}

-(void)postDataToServerWithUrl:(NSString *) url{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMddHHmmss";
    NSString *str = [formatter stringFromDate:[NSDate date]];
    NSString *fileName = [NSString stringWithFormat:@"%@.png", str];
    NSString *mimeType = @"image/png";
    //添加语言
    [self.parmsForAPI setValue:[SPDCommonTool getLanguageStrPostToServer:[SPDCommonTool getPreferredLanguage]] forKey:@"lang"];
    [self.parmsForAPI setValue:@(self.agreeSendBtn.selected) forKey:@"isSend"];
    [RequestUtils commonPostMultipartFormDataUtils:self.parmsForAPI bURL:url bAnimatedViewController:self constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        if (self.avatarImageView.image) {
            [formData appendPartWithFileData:UIImageJPEGRepresentation(self.avatarImageView.image, 0.5) name:@"photo" fileName:fileName mimeType:mimeType];
        }
        
    } success:^(id _Nullable dict) {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self saveUserDefaultFormRequestAPI:dict];
        dispatch_async(dispatch_get_main_queue(), ^{
            //发送红包通知
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FIRSTREGISTE" object:nil];
        });
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [self.view removeFromSuperview];
        [appDelegate loginSuccess];
        
    } failure:^(id  _Nullable failure) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)returnTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
