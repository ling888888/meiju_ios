//
//  SPDSignUpInfoController.m
//  SimpleDate
//
//  Created by Monkey on 2017/10/13.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDSignUpInfoController.h"
#import "AppDelegate.h"
#import "UIImage+Resize.h"
#import <CoreLocation/CoreLocation.h>

@interface SPDSignUpInfoController ()<UITextFieldDelegate,CLLocationManagerDelegate>

@property (nonatomic, copy) NSString *genderString;

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UIButton *citySelectedButton;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *birthdayLabel;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet UIButton *maleSelectedButton;
@property (weak, nonatomic) IBOutlet UIButton *femaleSelectedButton;
@property (weak, nonatomic) IBOutlet UIButton *agreeSendBtn;

@property (nonatomic, assign) BOOL isUserPostHeadView;
@property (nonatomic, strong) NSMutableDictionary *parmsForAPI;
@property (weak, nonatomic) IBOutlet UIImageView *takePicImageView;

@property (nonatomic,copy)NSString * myProviceAndCity;
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;

@property (nonatomic, strong) CLLocationManager* clLocationManager;
@property (nonatomic,copy)    NSString *currentCity;//城市
@property (weak, nonatomic) IBOutlet UITextField *invite_codeTextFeild;
@property (weak, nonatomic) IBOutlet UILabel *inviteCodeNameLabel;

@end

@implementation SPDSignUpInfoController

- (CLLocationManager *)clLocationManager{
    if (!_clLocationManager) {
        _clLocationManager = [[CLLocationManager alloc]init];
        _clLocationManager.delegate = self;
        _clLocationManager.desiredAccuracy = kCLLocationAccuracyBest;
    }
    return _clLocationManager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self createNav];
    
    [self.takePicImageView setUserInteractionEnabled:YES];
    self.avatarImageView.layer.cornerRadius = 50;
    [self.avatarImageView setUserInteractionEnabled:YES];

    UIGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTakePic)];
    [self.takePicImageView addGestureRecognizer:tap];
    UIGestureRecognizer * tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTakeAvatar)];
    [self.avatarImageView addGestureRecognizer:tap1];
    
    [self.confirmButton setTitle:SPDStringWithKey(@"注册", nil) forState:UIControlStateNormal];
    self.genderLabel.text = SPDStringWithKey(@"可选的，之后可修改", nil);
    self.userNameTextField.placeholder = SPDStringWithKey(@"请先填写您的昵称", nil);
    self.invite_codeTextFeild.placeholder = SPDStringWithKey(@"没有可不填", nil);
    self.inviteCodeNameLabel.text = SPDStringWithKey(@"邀请码", nil);
    self.myProviceAndCity = @"火星 火星 火星";
    
    if ([CLLocationManager locationServicesEnabled]) {
        [self.clLocationManager startUpdatingLocation];
    }
//    [self maleButtonClicked:self.maleSelectedButton];
    self.genderString = @"male";
    
    [self.agreeSendBtn setTitle:SPDStringWithKey(@"允许Hala帮你认识更多好友", nil) forState:UIControlStateNormal];
    if ([SPDCommonTool currentVersionIsArbic]) {
        self.agreeSendBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -4);
    }
}

#pragma mark - 定位成功
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    [self.clLocationManager stopUpdatingLocation];
    CLLocation *currentLocation = [locations lastObject];
    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
    NSTimeInterval locationAge = -[currentLocation.timestamp timeIntervalSinceNow];
    if (locationAge > 1.0){//如果调用已经一次，不再执行
//        return;
    }
    [geoCoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        if (placemarks.count >0) {
            CLPlacemark *placeMark = placemarks[0];
            _currentCity = placeMark.locality;
            if (!_currentCity) {
                _currentCity = @"无法定位当前城市";
            }
            self.myProviceAndCity = [NSString stringWithFormat:@"%@ %@ %@",placeMark.country?:@"火星",_currentCity?:@"火星",placeMark.subLocality?:@"火星"];
            [self.parmsForAPI setObject:self.myProviceAndCity forKey:@"city"];
        }else if (error == nil && placemarks.count){
            self.myProviceAndCity = @"火星 火星 火星";
        }else if (error){
            self.myProviceAndCity = @"火星 火星 火星";
        }
    }];
}

#pragma mark - 定位失败
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    self.myProviceAndCity = @"火星 火星 火星";
}

-(void)createNav {
    
    UIBarButtonItem *left = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ic_arrow_left"] style:UIBarButtonItemStylePlain target:self action:@selector(dealBarLeftTap:)];
    self.navigationItem.leftBarButtonItem = left;
    
}

-(void)dealBarLeftTap:(UIBarButtonItem *)item{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)maleButtonClicked:(id)sender {
    [self.maleSelectedButton setImage:[UIImage imageNamed:@"ic_genderMale2"] forState:UIControlStateNormal];
    [self.maleSelectedButton setTitleColor:[UIColor colorWithHexString:@"6ebff5"] forState:UIControlStateNormal];
    [self.femaleSelectedButton setImage:[UIImage imageNamed:@"ic_genderFemale1"] forState:UIControlStateNormal];
    [self.femaleSelectedButton setTitleColor:[UIColor colorWithHexString:@"dadada"] forState:UIControlStateNormal];
    self.genderString = @"male";
}

- (void)handleTakePic {
    [self presentImagePickerController:SPDStringWithKey(@"上传头像", nil)];
}

- (void)handleTakeAvatar {
    [self presentImagePickerController:SPDStringWithKey(@"上传头像", nil)];
}

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //something after selected image
    UIImage *image = info[UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        
        UIImage *resizedImage = [image resizedImageToFitInSize:CGSizeMake(200, 200) scaleIfSmaller:NO];
        
        self.avatarImageView.image = resizedImage;
        
        [self setIsUserPostHeadView:YES];
    }];
}
- (IBAction)femaleButtonClicked:(id)sender {
    // TODO: change the image of selected button to highlightened
    [self.maleSelectedButton setImage:[UIImage imageNamed:@"ic_genderMale1"] forState:UIControlStateNormal];
    [self.maleSelectedButton setTitleColor:[UIColor colorWithHexString:@"dadada"] forState:UIControlStateNormal];
    [self.femaleSelectedButton setImage:[UIImage imageNamed:@"ic_genderFemale2"] forState:UIControlStateNormal];
    [self.femaleSelectedButton setTitleColor:[UIColor colorWithHexString:@"ffb9fd"] forState:UIControlStateNormal];
    self.genderString = @"female";
}

- (IBAction)clickAgreeSendBtn:(UIButton *)sender {
    sender.selected = !sender.selected;
}

- (IBAction)registerButtonClicked:(id)sender {
    [self.confirmButton setUserInteractionEnabled:NO];
    NSMutableDictionary * dict = [NSMutableDictionary new];
    if (self.genderString) {
        [dict setObject:self.genderString forKey:@"gender"];
    } else {
        [self showToast:SPDStringWithKey(@"请选择性别", nil)];
        [self.confirmButton setUserInteractionEnabled:YES];
        return;
    }
    if (self.userNameTextField.text) {
        [dict setObject:self.userNameTextField.text forKey:@"user_name"];
    }
    if (self.avatarImageView.image) {
        
    }
    [dict setObject:@"AppStore" forKey:@"channel"];
    [dict setObject:self.myProviceAndCity forKey:@"city"];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMddHHmmss";
    NSString *str = [formatter stringFromDate:[NSDate date]];
    NSString *fileName = [NSString stringWithFormat:@"%@.png", str];
    NSString *mimeType = @"image/png";
    
    if (![SPDCommonTool isEmpty:self.invite_codeTextFeild.text]) {
        [dict setObject:self.invite_codeTextFeild.text forKey:@"invite_code"];
    }
    [dict setObject:[SPDCommonTool getLanguageStrPostToServer:[SPDCommonTool getPreferredLanguage]] forKey:@"lang"];
    [dict setValue:@(self.agreeSendBtn.selected) forKey:@"isSend"];
    [RequestUtils commonPostMultipartFormDataUtils: dict bURL:@"signup/onetap" bAnimatedViewController:self constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        if (self.avatarImageView.image) {
            [formData appendPartWithFileData:UIImageJPEGRepresentation(self.avatarImageView.image, 0.5) name:@"photo" fileName:fileName mimeType:mimeType];
        }
    } success:^(id _Nullable dict) {
        
        [self.confirmButton setEnabled:YES];
        [self saveUserDefaultFormRequestAPI:dict];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:SIGNUP_ONETAP(dict[@"_id"])];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [self.view removeFromSuperview];
        [appDelegate loginSuccess];
        
    } failure:^(id  _Nullable failure) {
        [self.confirmButton setEnabled:YES];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
