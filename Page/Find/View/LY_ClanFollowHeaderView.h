//
//  LY_ClanFollowHeaderView.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/9.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FamilyModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_ClanFollowHeaderView : UICollectionReusableView

- (void)showNoDataViewWithIsShowRecommendText:(BOOL)isShowRecommendText;

- (void)showMyClanViewWith:(FamilyModel *)clan isShowRecommendText:(BOOL)isShowRecommendText;

@end

NS_ASSUME_NONNULL_END
