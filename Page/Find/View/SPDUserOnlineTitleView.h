//
//  SPDUserOnlineTitleView.h
//  SimpleDate
//
//  Created by 李楠 on 2017/11/24.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPDUserOnlineTitleView : UIView

@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@property (nonatomic, strong) NSNumber *level;
@property (nonatomic, strong) NSArray *tasks;

@end
