//
//  SPDClanCountryView.h
//  SimpleDate
//
//  Created by 李楠 on 2018/3/28.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ClanCountryModel;

@protocol SPDClanCountryViewDelegate <NSObject>

@optional

- (void)didSelectedCountry:(ClanCountryModel *)model;

@end

@interface SPDClanCountryView : UIView

@property (nonatomic, strong) NSMutableArray *countryArray;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, weak) id<SPDClanCountryViewDelegate> delegate;

@end
