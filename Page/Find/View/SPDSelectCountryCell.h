//
//  SPDSelectCountryCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/3/28.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ClanCountryModel;

@interface SPDSelectCountryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *selectedImageView;

@property (nonatomic, strong) ClanCountryModel *model;

@end
