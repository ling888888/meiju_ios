//
//  SPDClanCountryView.m
//  SimpleDate
//
//  Created by 李楠 on 2018/3/28.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDClanCountryView.h"
#import "SPDSelectCountryCell.h"
#import "ClanCountryModel.h"

@interface SPDClanCountryView ()<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SPDClanCountryView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.titleLabel.text = SPDStringWithKey(@"选择地区", nil);
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView registerNib:[UINib nibWithNibName:@"SPDSelectCountryCell" bundle:nil] forCellReuseIdentifier:@"SPDSelectCountryCell"];
    [self animating];
}

- (void)setCountryArray:(NSMutableArray *)countryArray {
    _countryArray = countryArray;
    [self.tableView reloadData];
}

- (IBAction)tap:(UITapGestureRecognizer *)sender {
    [self removeFromSuperview];
}

- (IBAction)close:(UIButton *)sender {
    [self removeFromSuperview];
}

- (void)animating {
    CAKeyframeAnimation * animation;
    animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = 0.30;
    animation.removedOnCompletion = YES;
    animation.fillMode = kCAFillModeForwards;
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1, 1.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    [self.bgView.layer addAnimation:animation forKey:nil];
    
    [UIView animateWithDuration:0.30 animations:^{
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.25];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.countryArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SPDSelectCountryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SPDSelectCountryCell" forIndexPath:indexPath];
    cell.model = self.countryArray[indexPath.row];
    if ([cell.model.country isEqualToString:self.country]) {
        cell.nameLabel.textColor = [UIColor colorWithHexString:COMMON_PINK];
        cell.selectedImageView.hidden = NO;
    } else {
        cell.nameLabel.textColor = [UIColor colorWithHexString:@"333333"];
        cell.selectedImageView.hidden = YES;
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectedCountry:)]) {
        [self.delegate didSelectedCountry:self.countryArray[indexPath.row]];
        [self removeFromSuperview];
    }
}

@end
