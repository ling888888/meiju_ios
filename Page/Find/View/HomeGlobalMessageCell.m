//
//  HomeGlobalMessageCell.m
//  SimpleDate
//
//  Created by 侯玲 on 17/8/12.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "HomeGlobalMessageCell.h"

@interface HomeGlobalMessageCell ()<SPDVoiceLiveGlobalScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (nonatomic, strong) UILabel *clanNameLabel;

@end

@implementation HomeGlobalMessageCell

- (void)awakeFromNib {
    [super awakeFromNib];

    self.titleLabel.text = @"World";
}

#pragma mark - SPDVoiceLiveGlobalScrollViewDelegate

- (void)beginScrollWithDic:(NSDictionary *)dic {
    self.clanID = dic[@"clan_id"];
    self.clanNameLabel.text = dic[@"clan_name"];
}

- (void)didResetView:(UIView *)view {
    self.clanID = nil;
    self.clanNameLabel.text = @"";
}

#pragma mark - setters and getters

- (SPDVoiceLiveGlobalScrollView *)globalScrollView {
    if (!_globalScrollView) {
        _globalScrollView = [[SPDVoiceLiveGlobalScrollView alloc] initWithFrame:CGRectMake(0, 26, kScreenW, 48)];
        _globalScrollView.space = _globalScrollView.bounds.size.width;
        _globalScrollView.speed = 50;
        _globalScrollView.isRepeat = YES;
        _globalScrollView.delegate = self;
        [self.contentView insertSubview:_globalScrollView atIndex:1];
    }
    return _globalScrollView;
}

- (UILabel *)clanNameLabel {
    if (!_clanNameLabel) {
        _clanNameLabel = [[UILabel alloc] init];
        _clanNameLabel.font = [UIFont systemFontOfSize:12];
        _clanNameLabel.textColor = [UIColor colorWithHexString:@"258dd9"];
        [self.contentView insertSubview:_clanNameLabel belowSubview:self.globalScrollView];
        [_clanNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-11);
            make.bottom.mas_equalTo(-17);
            make.height.mas_equalTo(14);
        }];
    }
    return _clanNameLabel;
}

@end
