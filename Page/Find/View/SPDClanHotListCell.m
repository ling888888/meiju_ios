//
//  SPDClanHotListCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/7/3.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDClanHotListCell.h"
#import "FamilyModel.h"
#import "ZegoKitManager.h"

@interface SPDClanHotListCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *clanLevelImageView;
@property (weak, nonatomic) IBOutlet UILabel *clanLevelLabel;
@property (weak, nonatomic) IBOutlet UIView *lockView;
@property (weak, nonatomic) IBOutlet UIImageView *nickIdImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickIdLabel;
@property (weak, nonatomic) IBOutlet UIImageView *flagImageView;
@property (weak, nonatomic) IBOutlet UIImageView *agentServiceImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLabelLeading;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIImageView *livenessImage;
@property (weak, nonatomic) IBOutlet UILabel *livenessLabel;
@property (weak, nonatomic) IBOutlet UIButton *stickBtn;


@end

@implementation SPDClanHotListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.clanLevelImageView.image = [UIImage imageNamed:@"ic_clan_level_bg"].adaptiveRtl;
    [self.stickBtn setTitle:SPDStringWithKey(@"置顶", nil) forState:UIControlStateNormal];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.avatarImageView.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(6,6)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.avatarImageView.bounds;
    maskLayer.path = maskPath.CGPath;
    self.avatarImageView.layer.mask = maskLayer;
}

- (void)setModel:(FamilyModel *)model {
    _model = model;
    
    self.contentView.backgroundColor = [UIColor colorWithHexString:_model.myclan ? @"e9fdf8" : _model.isStickClan ? @"f3f3f7" : @"ffffff"];
    self.contentView.layer.cornerRadius = 6;
    self.contentView.clipsToBounds = YES;
    
    if ([model.avatar containsString:@"http"]) {
        [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:_model.avatar]];
    } else {
        [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _model.avatar]]];
    }
    self.clanLevelLabel.text = [NSString stringWithFormat:@"LV.%@", _model.clanLevel];
    self.lockView.hidden = !(_model.isLock && ZegoManager.chatroomLock);
    [self.flagImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _model.flag]]];
    if (![SPDCommonTool isEmpty:_model.agentServiceImage]) {
        self.agentServiceImageView.hidden = NO;
        [self.agentServiceImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _model.agentServiceImage]]];
    } else {
        self.agentServiceImageView.hidden = YES;
    }
    if (self.agentServiceImageView.hidden && self.nameLabelLeading.constant != 8) {
        self.nameLabelLeading.constant = 8;
    } else if (!self.agentServiceImageView.hidden && self.nameLabelLeading.constant != 33) {
         self.nameLabelLeading.constant = 8 + 17 + 8;
    }
    self.nameLabel.text = _model.name;
    self.descLabel.text = _model.desc;
    self.livenessLabel.text = [_model.onlineNum stringValue];
//    self.livenessImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"liveness_list_%@", _model.liveness_color]];
//    if (_model.liveness_num.integerValue > 9999999) {
//        self.livenessLabel.text = @"9999999";
//    } else {
//        self.livenessLabel.text = [NSString stringWithFormat:@"%ld", _model.liveness_num.integerValue];
//    }
    self.stickBtn.hidden = !self.isAgentService;
}

- (void)setNickId:(NSString *)nickId {
    _nickId = nickId;
    
    self.nickIdImageView.hidden = NO;
    self.nickIdLabel.hidden = NO;
    self.nickIdLabel.text = [NSString stringWithFormat:@"ID:%@", _model.nick_id];
}

- (IBAction)clickStickBtn:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickStickBtnWithModel:)]) {
        [self.delegate didClickStickBtnWithModel:self.model];
    }
}

@end
