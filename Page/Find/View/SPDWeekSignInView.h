//
//  SPDWeekSignInView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/1/9.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SPDWeekSignInViewDelegate <NSObject>

@optional
- (void)weekSignInViewSignInComplete;

@end

@interface SPDWeekSignInView : UIView

@property (nonatomic, copy) NSDictionary * dataDict;

@property (nonatomic, weak) id<SPDWeekSignInViewDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
