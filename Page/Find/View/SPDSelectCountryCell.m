//
//  SPDSelectCountryCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/3/28.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDSelectCountryCell.h"
#import "ClanCountryModel.h"

@implementation SPDSelectCountryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setModel:(ClanCountryModel *)model {
    _model = model;
    self.nameLabel.text = model.country_name;
}

@end
