//
//  VipPrivilegeView.h
//  SimpleDate
//
//  Created by 李楠 on 2018/7/30.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VipPrivilegeView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIButton *actionBtn;

@property (nonatomic, copy) NSString *type; // video: 视频, call: 家族召唤, gold: 金币, vehicle: 座驾.

@end
