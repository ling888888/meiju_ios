//
//  VipCommonPrivilegeCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/7/30.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "VipCommonPrivilegeCell.h"
#import "VipPrivilegeView.h"

@interface VipCommonPrivilegeCell ()

@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet VipPrivilegeView *privilegeView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;
@property (weak, nonatomic) IBOutlet UIImageView *isBuyImageView;

@end

@implementation VipCommonPrivilegeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.titleImageView.image = [UIImage imageNamed:@"vip_title_bg"].adaptiveRtl;
    self.titleLabel.text = SPDStringWithKey(@"初级会员", nil);
    self.privilegeView.type = @"video";
    self.priceLabel.text = @"$0.99";
    self.timeLabel.text = [NSString stringWithFormat:@"7%@", SPDStringWithKey(@"天", nil)];
    self.isBuyImageView.image = [UIImage imageNamed:@"vip_bought"].adaptiveRtl;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setIsBuy:(BOOL)isBuy {
    _isBuy = isBuy;
    
    self.isBuyImageView.hidden = !_isBuy;
    if (_isBuy) {
        [self.buyButton setTitle:SPDStringWithKey(@"续费", nil) forState:UIControlStateNormal];
    } else {
        [self.buyButton setTitle:SPDStringWithKey(@"开通", nil) forState:UIControlStateNormal];
    }
}

- (IBAction)clickBuyBtn:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickBuyCommonVip)]) {
        [self.delegate didClickBuyCommonVip];
        
        if (self.isBuy) {
            //[MobClick event:@"clickVIP_099Renewal"];
        } else {
            //[MobClick event:@"clickVIP_099Open"];
        }
    }
}

@end
