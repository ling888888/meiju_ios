//
//  VipCommonPrivilegeCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/7/30.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VipCommonPrivilegeCellDelegate <NSObject>

@optional

- (void)didClickBuyCommonVip;

@end

@interface VipCommonPrivilegeCell : UITableViewCell

@property (nonatomic, weak) id<VipCommonPrivilegeCellDelegate> delegate;

@property (nonatomic, assign) BOOL isBuy;

@end
