//
//  RankListCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/12/12.
//  Copyright © 2018 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RankModel;

NS_ASSUME_NONNULL_BEGIN

@interface RankListCell : UICollectionViewCell

@property (nonatomic, copy) NSString *type; // @[@"charm", @"riche", @"person", @"clan", @"magic"]
@property (nonatomic, strong) RankModel *model;

@end

NS_ASSUME_NONNULL_END
