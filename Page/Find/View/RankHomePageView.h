//
//  RankHomePageView.h
//  SimpleDate
//
//  Created by 李楠 on 2020/2/10.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class RankHomePageView;

@protocol RankHomePageViewDelegate <NSObject>

@optional

- (void)rankHomePageView:(RankHomePageView *)view didTapWithIndex:(NSInteger)index;

@end

@interface RankHomePageView : UIView

@property (nonatomic, copy) NSArray *firstImageURLStringsGroup;
@property (nonatomic, copy) NSArray *secondImageURLStringsGroup;
@property (nonatomic, copy) NSArray *thirdImageURLStringsGroup;

@property (nonatomic, weak) id<RankHomePageViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
