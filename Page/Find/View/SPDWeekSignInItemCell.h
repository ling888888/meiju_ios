//
//  SPDWeekSignInItemCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/1/9.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SPDWeekSignInItemCell : UICollectionViewCell

@property (nonatomic, strong) NSDictionary * dic;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@property (weak, nonatomic) IBOutlet UIView *itemContentView;

@end

NS_ASSUME_NONNULL_END
