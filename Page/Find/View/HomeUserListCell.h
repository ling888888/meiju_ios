//
//  HomeUserListCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/7/5.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HomeModel;

@protocol HomeUserListCellDelegate <NSObject>

@optional

- (void)didClickEnterClanBtnWithModel:(HomeModel *)model;

@end

@interface HomeUserListCell : UICollectionViewCell

@property (nonatomic, strong) HomeModel *model;
@property (nonatomic, assign) BOOL hideLevel;
@property (nonatomic, assign) id<HomeUserListCellDelegate> delegate;

@end
