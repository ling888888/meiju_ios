//
//  VipMyInfoCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/7/30.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VipMyInfoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *doNotHaveLabel;
@property (weak, nonatomic) IBOutlet UILabel *expireTime1Label;
@property (weak, nonatomic) IBOutlet UILabel *expireTime2Label;
@property (weak, nonatomic) IBOutlet UIImageView *vipIcon;

@end
