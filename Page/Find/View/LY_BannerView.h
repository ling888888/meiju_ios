//
//  LY_BannerView.h
//  SimpleDate
//
//  Created by jianyue on 2020/5/7.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BannerModel.h"

NS_ASSUME_NONNULL_BEGIN

#define LYBannerViewHeight (kScreenW-30)/345*135

@interface LY_BannerView : UIView

//@property (nonatomic, strong) NSArray<NSString *> *imgUrlArray;

@property (nonatomic, strong) NSArray<BannerModel *> *bannerList;

@property (nonatomic, assign) BOOL isLiveViewBanner;

@end

NS_ASSUME_NONNULL_END
