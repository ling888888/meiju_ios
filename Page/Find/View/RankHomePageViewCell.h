//
//  RankHomePageViewCell.h
//  SimpleDate
//
//  Created by 李楠 on 2020/2/10.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RankHomePageViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *rankLabel;

@end

NS_ASSUME_NONNULL_END
