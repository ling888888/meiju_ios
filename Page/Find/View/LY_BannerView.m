//
//  LY_BannerView.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/7.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_BannerView.h"
#import "SDCycleScrollView.h"
#import "LY_BannerViewCollectionViewCell.h"
#import "SPDHelpController.h"
@interface LY_BannerView () <SDCycleScrollViewDelegate>

@property (nonatomic, strong) SDCycleScrollView * cycleScrollView;

@end

@implementation LY_BannerView

- (SDCycleScrollView *)cycleScrollView{
    if (!_cycleScrollView) {
        _cycleScrollView = [[SDCycleScrollView alloc] init];
        _cycleScrollView.delegate = self;
        _cycleScrollView.backgroundColor = [UIColor clearColor];
        _cycleScrollView.autoScroll = YES;
        _cycleScrollView.infiniteLoop = YES;
        _cycleScrollView.showPageControl = YES;
        _cycleScrollView.pageControlDotSize = CGSizeMake(5, 5);
        _cycleScrollView.pageDotColor = [UIColor colorWithWhite:1 alpha:0.8];
        _cycleScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleClassic;
        _cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        _cycleScrollView.placeholderImage = [UIImage imageNamed:@"ic_tongyong_banner_moren"];
    }
    return _cycleScrollView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.cycleScrollView];
}

- (void)setupUIFrame {
    [self.cycleScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
}

- (void)setBannerList:(NSArray<BannerModel *> *)bannerList {
    _bannerList = bannerList;
    NSMutableArray *imgUrlArray = [NSMutableArray array];
    for (BannerModel *model in bannerList) {
//        double beginTime = [model.begin_time doubleValue] / 1000;
//        double endTime = [model.end_time doubleValue] / 1000;
//        NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
//        if (currentTime >= beginTime && currentTime <= endTime) {
            NSString *imgaeUrl = [NSString stringWithFormat:STATIC_DOMAIN_URL, model.image_url];
            [imgUrlArray addObject:imgaeUrl];
//        }
    }
    if (imgUrlArray.count > 0 && imgUrlArray != self.cycleScrollView.imageURLStringsGroup) {
        self.cycleScrollView.placeholderImage = nil;
        self.cycleScrollView.imageURLStringsGroup = imgUrlArray;
    }
}

#pragma mark - SDCycleScrollViewDelegate

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    SPDHelpController * helper = [[SPDHelpController alloc]init];
    BannerModel * bannerModel = self.bannerList[index];
    if ([bannerModel.action_url containsString:@"?"]) {
        helper.url = [NSString stringWithFormat:@"%@&version=%@", bannerModel.action_url, LOCAL_VERSION_SHORT];
    } else {
       helper.url  = [NSString stringWithFormat:@"%@?version=%@", bannerModel.action_url, LOCAL_VERSION_SHORT];
    }
    [self.VC.navigationController pushViewController:helper animated:true];
    if (self.isLiveViewBanner) {
        //[MobClick event:[NSString stringWithFormat:@"FM1_10_2_%ld", index+1]];
    }
}

- (Class)customCollectionViewCellClassForCycleScrollView:(SDCycleScrollView *)view {
    return [LY_BannerViewCollectionViewCell class];
}

- (void)setupCustomCell:(UICollectionViewCell *)cell forIndex:(NSInteger)index cycleScrollView:(SDCycleScrollView *)view {
    LY_BannerViewCollectionViewCell *newCell = (LY_BannerViewCollectionViewCell *)cell;
    
    [newCell.imgView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:view.imageURLStringsGroup[index]]] placeholderImage:[UIImage imageNamed:@"ic_tongyong_banner_moren"]];
}

@end
