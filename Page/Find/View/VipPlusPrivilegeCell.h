//
//  VipPlusPrivilegeCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/7/31.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VipPlusPrivilegeCellDelegate <NSObject>

@optional

- (void)didClickEquipVehicle;
- (void)didClickGetAward;
- (void)didClickBuyPlusVip;

@end

@interface VipPlusPrivilegeCell : UITableViewCell

@property (nonatomic, weak) id<VipPlusPrivilegeCellDelegate> delegate;

@property (nonatomic, assign) BOOL isBuy;
@property (nonatomic, copy) NSNumber *award_num;
@property (nonatomic, assign) BOOL is_get;
@property (nonatomic, assign) BOOL is_outfit;

@end
