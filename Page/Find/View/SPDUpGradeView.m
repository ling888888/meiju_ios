//
//  SPDUpGradeView.m
//  SimpleDate
//
//  Created by Monkey on 2017/9/1.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDUpGradeView.h"

@interface SPDUpGradeView ()

@property (weak, nonatomic) IBOutlet UILabel *upgradetitle;
@property (weak, nonatomic) IBOutlet UILabel *expLabel;
@property (weak, nonatomic) IBOutlet UILabel *upgradeTipsLabel;

@property (weak, nonatomic) IBOutlet UIButton *sureBtn;
@end

@implementation SPDUpGradeView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.upgradetitle.text = SPDStringWithKey(@"升级成功", nil);
    self.upgradeTipsLabel.text = SPDStringWithKey(@"哇塞，你又升级了～", nil);
    [self.sureBtn setTitle:SPDStringWithKey(@"确定", nil) forState:UIControlStateNormal];

}

- (void)setCurrent_level:(NSInteger)current_level {
    _current_level = current_level;
    self.expLabel.text = [NSString stringWithFormat:@"%ld",current_level];
}

- (IBAction)hideBtnClicked:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(upgradeViewHiddenBtnClicked)]) {
        [self.delegate upgradeViewHiddenBtnClicked];
    }
    
}

- (IBAction)sureBtnClicked:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(upgradeViewSureBtnClicked)]) {
        [self.delegate upgradeViewSureBtnClicked];
    }
    
    
}


@end
