//
//  SPDClanCoverCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/3/22.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SPDClanCoverCellDelegate <NSObject>

@optional

- (void)selectCover;

@end

@interface SPDClanCoverCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *coverBtn;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;
@property (weak, nonatomic) IBOutlet UILabel *tipsLabel;

@property (nonatomic, weak) id<SPDClanCoverCellDelegate> delegate;
@property (nonatomic, strong) UIImage *cover;

@end
