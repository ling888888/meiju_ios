//
//  WealthCollectionViewCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/5/7.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class SPDMagicModel,SPDVehicleModel;
@interface WealthCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) SPDMagicModel *magicModel;
@property (nonatomic, assign) NSInteger magicLevel;
@property (nonatomic, strong) SPDVehicleModel *vehicleModel;

@end

NS_ASSUME_NONNULL_END
