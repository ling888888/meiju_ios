//
//  RankHomePageView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/2/10.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RankHomePageView.h"
#import "SDCycleScrollView.h"
#import "RankHomePageViewCell.h"

@interface RankHomePageView ()<SDCycleScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *typeImageView;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet SDCycleScrollView *firstScrollView;
@property (weak, nonatomic) IBOutlet SDCycleScrollView *secondScrollView;
@property (weak, nonatomic) IBOutlet SDCycleScrollView *thirdScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;

@property (nonatomic, copy) NSArray *typeNameArray;
@property (nonatomic, copy) NSArray *typeImageArray;
@property (nonatomic, copy) NSArray *arrowImageArray;
@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, assign) NSInteger resetTime;

@end

@implementation RankHomePageView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.typeNameArray = @[@"土豪榜", @"魅力榜", @"个人等级榜", @"家族等级榜", @"魔法等级榜"];
    self.typeImageArray = @[@"rank_hp_icon_rich", @"rank_hp_icon_charm", @"rank_hp_icon_personal", @"rank_hp_icon_clan", @"rank_hp_icon_magic"];
    self.arrowImageArray = @[@"rank_hp_arrow_rich", @"rank_hp_arrow_charm", @"rank_hp_arrow_personal", @"rank_hp_arrow_clan", @"rank_hp_arrow_magic"];
    
    self.typeImageView.image = [UIImage mirroredImageNamed:self.typeImageArray.firstObject];
    self.typeLabel.text = SPDStringWithKey(self.typeNameArray.firstObject, nil);
    self.arrowImageView.image = [UIImage mirroredImageNamed:self.arrowImageArray.firstObject];
}

- (IBAction)tap:(UITapGestureRecognizer *)sender {
    if ([self.delegate respondsToSelector:@selector(rankHomePageView:didTapWithIndex:)]) {
        [self.delegate rankHomePageView:self didTapWithIndex:self.currentIndex];
    }
}

- (void)setFirstImageURLStringsGroup:(NSArray *)firstImageURLStringsGroup {
    _firstImageURLStringsGroup = firstImageURLStringsGroup;
    
    [self.typeImageView.layer removeAllAnimations];
    [self.typeLabel.layer removeAllAnimations];
    [self.arrowImageView.layer removeAllAnimations];
    self.typeImageView.image = [UIImage mirroredImageNamed:self.typeImageArray.firstObject];
    self.typeLabel.text = SPDStringWithKey(self.typeNameArray.firstObject, nil);
    self.arrowImageView.image = [UIImage mirroredImageNamed:self.arrowImageArray.firstObject];
    self.firstScrollView.hidden = YES;
    self.secondScrollView.hidden = YES;
    self.thirdScrollView.hidden = YES;
    self.currentIndex = 0;
    self.resetTime = [NSDate date].timeIntervalSince1970;
        
    self.firstScrollView.backgroundColor = [UIColor whiteColor];
    self.firstScrollView.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.firstScrollView.showPageControl = NO;
    self.firstScrollView.delegate = self;
    self.firstScrollView.hidden = NO;
    self.firstScrollView.imageURLStringsGroup = _firstImageURLStringsGroup;
}

- (void)setSecondImageURLStringsGroup:(NSArray *)secondImageURLStringsGroup {
    _secondImageURLStringsGroup = secondImageURLStringsGroup;
    
    self.secondScrollView.backgroundColor = [UIColor whiteColor];
    self.secondScrollView.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.secondScrollView.showPageControl = NO;
    self.secondScrollView.delegate = self;
    self.secondScrollView.hidden = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.secondScrollView.imageURLStringsGroup = _secondImageURLStringsGroup;
    });
}

- (void)setThirdImageURLStringsGroup:(NSArray *)thirdImageURLStringsGroup {
    _thirdImageURLStringsGroup = thirdImageURLStringsGroup;
    
    self.thirdScrollView.backgroundColor = [UIColor whiteColor];
    self.thirdScrollView.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.thirdScrollView.showPageControl = NO;
    self.thirdScrollView.delegate = self;
    self.thirdScrollView.hidden = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.thirdScrollView.imageURLStringsGroup = _thirdImageURLStringsGroup;
    });
}

#pragma mark - SDCycleScrollViewDelegate

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index {
    self.currentIndex = index;
    
    if (cycleScrollView == self.firstScrollView && [NSDate date].timeIntervalSince1970 - self.resetTime > 1.5) {
        CATransition *transition = [CATransition animation];
        transition.duration = 0.25;
        transition.type = kCATransitionFade;
        [self.typeImageView.layer addAnimation:transition forKey:@"kCATransitionFade"];
        self.typeImageView.image = [UIImage mirroredImageNamed:self.typeImageArray[index]];
        [self.typeLabel.layer addAnimation:transition forKey:@"kCATransitionFade"];
        self.typeLabel.text = SPDStringWithKey(self.typeNameArray[index], nil);
        [self.arrowImageView.layer addAnimation:transition forKey:@"kCATransitionFade"];
        self.arrowImageView.image = [UIImage mirroredImageNamed:self.arrowImageArray[index]];
    }
}

- (Class)customCollectionViewCellClassForCycleScrollView:(SDCycleScrollView *)view {
    return [RankHomePageViewCell class];
}

- (void)setupCustomCell:(UICollectionViewCell *)cell forIndex:(NSInteger)index cycleScrollView:(SDCycleScrollView *)view {
    RankHomePageViewCell *customCell = (RankHomePageViewCell *)cell;
    customCell.avatarImageView.layer.cornerRadius = index == 3 ? 7 : 17;
    if (view == self.firstScrollView) {
        [customCell.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, self.firstImageURLStringsGroup[index]]]];
        customCell.avatarImageView.layer.borderColor = [UIColor colorWithHexString:@"#FFD249"].CGColor;
        customCell.rankLabel.text = @"1";
        customCell.rankLabel.backgroundColor = [UIColor colorWithHexString:@"#FFD249"];
    } else if (view == self.secondScrollView) {
        [customCell.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, self.secondImageURLStringsGroup[index]]]];
        customCell.avatarImageView.layer.borderColor = [UIColor colorWithHexString:@"#C6C6C6"].CGColor;
        customCell.rankLabel.text = @"2";
        customCell.rankLabel.backgroundColor = [UIColor colorWithHexString:@"#C6C6C6"];
    } else {
        [customCell.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, self.thirdImageURLStringsGroup[index]]]];
        customCell.avatarImageView.layer.borderColor = [UIColor colorWithHexString:@"#E1A571"].CGColor;
        customCell.rankLabel.text = @"3";
        customCell.rankLabel.backgroundColor = [UIColor colorWithHexString:@"#E1A571"];
    }
}

@end
