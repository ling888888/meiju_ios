//
//  VipMyInfoCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/7/30.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "VipMyInfoCell.h"

@implementation VipMyInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.doNotHaveLabel.text = SPDStringWithKey(@"还没有VIP喔，不享受VIP特权", nil);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
