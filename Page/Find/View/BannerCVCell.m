//
//  BannerCVCell.m
//  SimpleDate
//
//  Created by Monkey on 2017/8/14.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "BannerCVCell.h"

@implementation BannerCVCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setImgUrl:(NSString *)imgUrl {
    _imgUrl = imgUrl;
    [self.bannerView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:imgUrl]];
}

@end
