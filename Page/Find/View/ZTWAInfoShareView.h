//
//  ZTWAInfoShareView.h
//  zhitou
//
//  Created by TragedyStar on 16/4/10.
//  Copyright © 2016年 zhitou. All rights reserved.
//

#import <UIKit/UIKit.h>

UIKIT_EXTERN NSString *const ZTWAInfoShareViewPlatformWeChatTimeLine;
UIKIT_EXTERN NSString *const ZTWAInfoShareViewPlatformWeChatFriend;
UIKIT_EXTERN NSString *const ZTWAInfoShareViewPlatformQQFriend ;
UIKIT_EXTERN NSString *const ZTWAInfoShareViewPlatformQQZone ;
UIKIT_EXTERN NSString *const ZTWAInfoShareViewPlatformSina;

@class ZTWAInfoShareView;

@protocol ZTWAInfoShareViewDelegate <NSObject>

@optional
- (void)infoShareView:(ZTWAInfoShareView *)infoShareView shareToPlatform:(NSString *)platform;

- (void)infoShareView:(ZTWAInfoShareView *)infoShareView shareToPlatformType:(SPDUMShareType)sharePlatformType;

-(void)cancleShare;

- (void)clickedInviteCount;

@end

@interface ZTWAInfoShareView : UIView

+ (instancetype)shareView;

- (void)show;

- (void)dismiss;

@property (nonatomic, weak) id<ZTWAInfoShareViewDelegate> delegate;

@property (nonatomic, copy) NSString *invite_code; //废弃

@property (nonatomic, copy) NSString *invite_link ; //邀请链接 可以通过这个设置

@property (nonatomic, assign) int invite_count;

@property (nonatomic, copy) NSDictionary *shareDict ; //邀请链接 也可以通过这个设置


@end
