//
//  SPDCountryListCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/3/30.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDCountryListCell.h"
#import "ClanCountryModel.h"

@interface SPDCountryListCell ()

@property (weak, nonatomic) IBOutlet UIImageView *flagImageView;
@property (weak, nonatomic) IBOutlet UILabel *countryNameLabel;

@end

@implementation SPDCountryListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(ClanCountryModel *)model {
    _model = model;
    [self.flagImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _model.flag]]];
    self.countryNameLabel.text = _model.country_name;
}

@end
