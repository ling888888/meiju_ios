//
//  BannerCVCell.h
//  SimpleDate
//
//  Created by Monkey on 2017/8/14.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BannerCVCell : UICollectionViewCell


@property (weak, nonatomic) IBOutlet UIImageView *bannerView;

@property (nonatomic,copy)NSString * imgUrl ;
@end
