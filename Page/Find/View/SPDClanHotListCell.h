//
//  SPDClanHotListCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/7/3.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FamilyModel;

@protocol SPDClanHotListCellDelegate <NSObject>

@optional

- (void)didClickStickBtnWithModel:(FamilyModel *)model;

@end

@interface SPDClanHotListCell : UICollectionViewCell

@property (nonatomic, assign) BOOL isAgentService;
@property (nonatomic, strong) FamilyModel *model;
@property (nonatomic, copy) NSString *nickId;
@property (nonatomic, weak) id<SPDClanHotListCellDelegate> delegate;

@end
