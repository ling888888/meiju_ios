//
//  ChatroomWealthTitleCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/5/7.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "ChatroomWealthTitleCell.h"

@interface ChatroomWealthTitleCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrow;

@end

@implementation ChatroomWealthTitleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setType:(NSString *)type {
    _type = type;
    if ([_type isEqualToString:@"magic"]) {
        self.titleLabel.text = @"魔法".localized;
        self.subTitleLabel.text = @"升级".localized;
        self.subTitleLabel.hidden = ![[SPDApiUser currentUser].userId isEqualToString:self.userId];
        self.arrow.hidden = ![[SPDApiUser currentUser].userId isEqualToString:self.userId];
    }else if ([_type isEqualToString:@"vehicle"]){
        self.titleLabel.text =  @"座驾".localized;
        self.subTitleLabel.text = ![[SPDApiUser currentUser].userId isEqualToString:self.userId]  ? @"赠送".localized:@"购买".localized;
        self.subTitleLabel.hidden = NO;
        self.arrow.hidden = NO;
    }else if ([_type isEqualToString:@"receive"]){
        self.titleLabel.text = @"收到礼物".localized;
        self.subTitleLabel.hidden = YES;
        self.arrow.hidden = YES;
    }else if ([_type isEqualToString:@"send"]){
        self.titleLabel.text = @"送出礼物".localized;
        self.subTitleLabel.hidden = YES;
        self.arrow.hidden = YES;
    }
}


@end
