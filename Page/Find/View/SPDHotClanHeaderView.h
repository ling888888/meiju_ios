//
//  SPDHotClanHeaderView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/2/7.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LY_BannerView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol SPDHotClanHeaderViewDelegate <NSObject>

@optional

- (void)clickedBannerCellWithIndex: (NSInteger)index;

@end

@interface SPDHotClanHeaderView : UIView

@property (nonatomic, strong) NSArray<BannerModel *> *bannerList;

@property (nonatomic, strong) NSMutableDictionary * rankDict;

@property (nonatomic, weak) id<SPDHotClanHeaderViewDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
