//
//  WealthCollectionViewCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/5/7.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "WealthCollectionViewCell.h"
#import "SPDMagicModel.h"
#import "SPDVehicleModel.h"

@interface WealthCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UIImageView *drivingImageView;
@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondLabel;

@end

@implementation WealthCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.drivingImageView.image = [UIImage imageNamed:@"vehicle_drive"].adaptiveRtl;
}

- (void)setMagicModel:(SPDMagicModel *)magicModel {
    _magicModel = magicModel;
    [self.coverImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _magicModel.cover]]];
    self.firstLabel.text = [NSString stringWithFormat:@"%@",_magicModel.magic_level];
    self.secondLabel.text = SPDStringWithKey(_magicModel.name, nil);
    self.drivingImageView.hidden = YES;

}

- (void)setVehicleModel:(SPDVehicleModel *)vehicleModel {
    _vehicleModel = vehicleModel;
    [self.coverImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _vehicleModel.image]]];
    self.firstLabel.textColor = SPD_HEXCOlOR(@"FFDDAC");
    self.firstLabel.text = _vehicleModel.name;
    self.secondLabel.text = [_vehicleModel.expire_time_str substringFromIndex:5];
    self.drivingImageView.hidden = !_vehicleModel.is_drive;
}

@end
