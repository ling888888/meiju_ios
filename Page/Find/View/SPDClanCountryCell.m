//
//  SPDClanCountryCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/3/28.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDClanCountryCell.h"

@interface SPDClanCountryCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *countryLabel;

@end

@implementation SPDClanCountryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.titleLabel.text = SPDStringWithKey(@"所属地区:", nil);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCountry:(NSString *)country {
    _country = country;
    self.countryLabel.text = _country ? : SPDStringWithKey(@"请选择", nil);
}

- (IBAction)clickCountryBtn:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(selectCountry)]) {
        [self.delegate selectCountry];
    }
}

@end
