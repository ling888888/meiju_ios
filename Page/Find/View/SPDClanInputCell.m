//
//  SPDClanInputCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/3/22.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDClanInputCell.h"

@interface SPDClanInputCell ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *textField;

@property (nonatomic, assign) NSInteger textLimit;
@property (nonatomic, assign) BOOL isZh;

@end

@implementation SPDClanInputCell

- (void)awakeFromNib {
    [super awakeFromNib];

    self.textField.delegate = self;
    self.isZh = [[SPDCommonTool getFamyLanguage] containsString:@"zh"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setType:(NSString *)type {
    _type = type;
    if ([_type isEqualToString:@"clan_name"]) {
        self.titleLabel.text = SPDStringWithKey(@"名称:", nil);
        self.textField.placeholder = SPDStringWithKey(@"请输入健康文明的家族名称(15个字以内)", nil);
        self.textLimit = self.isZh ? 4 : 15;
    } else if ([_type isEqualToString:@"clan_declar"]) {
        self.titleLabel.text = SPDStringWithKey(@"简介:", nil);
        self.textField.placeholder = SPDStringWithKey(@"请输入健康文明的家族宣言(40个字以内)", nil);
        self.textLimit = self.isZh ? 15 : 40;
    } else if ([_type isEqualToString:@"greeting"]) {
        self.titleLabel.text = SPDStringWithKey(@"欢迎语:", nil);
        self.textField.placeholder = SPDStringWithKey(@"请输入家族欢迎语(60个字以内)", nil);
        self.textLimit = self.isZh ? 40 : 60;
    }
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-5);
    }];
}

- (void)setText:(NSString *)text {
    _text = text;
    self.textField.text = _text;
}

- (IBAction)editingChanged:(UITextField *)sender {
    if (sender.text.length > self.textLimit) {
        UITextRange *markedRange = [sender markedTextRange];
        if (markedRange) {
            return;
        }
        // Emoji占2个字符，如果是超出了半个Emoji，用15位置来截取会出现Emoji截为2半
        // 超出最大长度的那个字符序列(Emoji算一个字符序列)的range
        NSRange range = [sender.text rangeOfComposedCharacterSequenceAtIndex:self.textLimit];
        sender.text = [sender.text substringToIndex:range.location];
    }
    if ([SPDCommonTool isEmpty:sender.text] && [self.type isEqualToString:@"greeting"]) {
        [self.params removeObjectForKey:self.type];
    } else {
        [self.params setObject:sender.text forKey:self.type];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
