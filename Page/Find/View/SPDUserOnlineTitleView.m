//
//  SPDUserOnlineTitleView.m
//  SimpleDate
//
//  Created by 李楠 on 2017/11/24.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDUserOnlineTitleView.h"

@interface SPDUserOnlineTitleView ()

@property (weak, nonatomic) IBOutlet UILabel *LevelLabel;
@property (weak, nonatomic) IBOutlet UIImageView *starImageView1;
@property (weak, nonatomic) IBOutlet UIImageView *starImageView2;
@property (weak, nonatomic) IBOutlet UIImageView *starImageView3;
@property (weak, nonatomic) IBOutlet UIImageView *starImageView4;
@property (weak, nonatomic) IBOutlet UIImageView *starImageView5;
@property (weak, nonatomic) IBOutlet UIImageView *starImageView6;
@property (weak, nonatomic) IBOutlet UIImageView *starImageView7;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nickNameLabelCenterX;

@end

@implementation SPDUserOnlineTitleView

- (void)setLevel:(NSNumber *)level {
    _level = level;
    
    self.LevelLabel.text = [NSString stringWithFormat:@"LV.%@", _level];
    CGFloat offset = _level.integerValue > 10 ? 18.5 : 15;
    self.nickNameLabelCenterX.constant = [SPDCommonTool currentVersionIsArbic] ? offset : -offset;
}

- (void)setTasks:(NSArray *)tasks {
    _tasks = tasks;
    
    self.statusLabel.textAlignment = NSTextAlignmentNatural;
    NSMutableArray *starImageViews = [@[self.starImageView7, self.starImageView1, self.starImageView2, self.starImageView3, self.starImageView4, self.starImageView5, self.starImageView6] mutableCopy];
    for (UIImageView *imageView in starImageViews) {
        imageView.image = [UIImage imageNamed:@"ic_clan_star_s"];
        imageView.hidden = NO;
    }
    for (NSNumber *index in _tasks) {
        UIImageView *imageView = starImageViews[index.integerValue];
        imageView.image = [UIImage imageNamed:@"ic_clan_star_hl_s"];
    }
}

@end
