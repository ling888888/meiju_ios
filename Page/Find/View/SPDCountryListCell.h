//
//  SPDCountryListCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/3/30.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ClanCountryModel;

@interface SPDCountryListCell : UICollectionViewCell

@property (nonatomic, strong) ClanCountryModel *model;

@end
