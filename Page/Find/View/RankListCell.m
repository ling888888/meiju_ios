//
//  RankListCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/12/12.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "RankListCell.h"
#import "RankMedalsView.h"
#import "RankModel.h"

@interface RankListCell ()

@property (weak, nonatomic) IBOutlet UILabel *rankLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLabelTop;
@property (weak, nonatomic) IBOutlet UIImageView *genderImageView;
@property (weak, nonatomic) IBOutlet UIImageView *tagImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tagImageViewTrailing;
@property (weak, nonatomic) IBOutlet UIImageView *charmImageView;
@property (weak, nonatomic) IBOutlet UILabel *charmLabel;
@property (weak, nonatomic) IBOutlet UIButton *levelBtn;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet RankMedalsView *medalsView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *medalsViewWidth;

@end

@implementation RankListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(RankModel *)model {
    _model = model;
    
    self.rankLabel.text = [NSString stringWithFormat:@"%ld", _model.rank.integerValue + 1];
    [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _model.avatar]]];
    self.nameLabel.text = _model.name;
    self.genderImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"rank_gender_%@", _model.gender]];
    CGFloat medalsViewWidth = [self.medalsView widthWithMedals:_model.medals vip:_model.isVip specialNum:_model.specialNum];
    if (medalsViewWidth > 0) {
        self.nameLabelTop.constant = 10.5;
        self.medalsViewWidth.constant = medalsViewWidth;
    } else {
        self.nameLabelTop.constant = 21;
    }
    
    if ([self.type isEqualToString:@"charm"] || [self.type isEqualToString:@"riche"]) {
        if (_model.rank.integerValue < 10) {
            self.tagImageView.hidden = NO;
            self.tagImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"rank_tag_%@4", self.type]];
        } else {
            self.tagImageView.hidden = YES;
        }
        if ([self.type isEqualToString:@"charm"]) {
            self.tagImageViewTrailing.constant = 118;
            self.charmImageView.hidden = NO;
            self.charmLabel.hidden = NO;
            self.charmLabel.text = model.score;
        }
    } else if ([self.type isEqualToString:@"person"]) {
        self.levelBtn.hidden = NO;
        self.levelBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [SPDCommonTool configDataForLevelBtnWithLevel:model.level.intValue andUIButton:self.levelBtn];
    } else if ([self.type isEqualToString:@"clan"]) {
        self.avatarImageView.layer.cornerRadius = 7;
        self.genderImageView.hidden = YES;
        self.levelBtn.hidden = NO;
        self.levelBtn.titleLabel.font = [UIFont systemFontOfSize:11];
        [self.levelBtn setTitle:[NSString stringWithFormat:@"LV.%ld", model.level.integerValue] forState:UIControlStateNormal];
        [self.levelBtn setBackgroundImage:[UIImage imageNamed:@"rank_level_clan"] forState:UIControlStateNormal];
    } else if ([self.type isEqualToString:@"magic"]) {
        self.levelLabel.hidden = NO;
        self.levelLabel.text = [NSString stringWithFormat:@"LV.%ld", _model.level.integerValue];
    }
}

@end
