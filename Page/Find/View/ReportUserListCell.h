//
//  ReportUserListCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/5/8.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReportUserListCell : BaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *icon;

@property (nonatomic, copy) NSDictionary * dict;

@end

NS_ASSUME_NONNULL_END
