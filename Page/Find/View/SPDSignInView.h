//
//  SPDSignInView.h
//  SimpleDate
//
//  Created by Monkey on 2017/8/31.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SPDSignInViewDelegate <NSObject>

- (void)clickedSignIn;
- (void)clickedDismiss;
@end

@interface SPDSignInView : UIView

+ (instancetype)shareInstance ;

@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

- (void)configsigninExp:(NSString *)signin_exp andStr:(NSString *)str andSigninCoins:(NSString *)signin_coins;
- (void)confignSignInSuccess ;

@property (nonatomic,weak)id<SPDSignInViewDelegate> delegate;

@end
