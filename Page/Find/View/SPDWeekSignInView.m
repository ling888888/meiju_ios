//
//  SPDWeekSignInView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/1/9.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "SPDWeekSignInView.h"
#import "SPDWeekSignInItemCell.h"
#import "MarqueeLabel.h"
#import "NSString+XXWAddition.h"

@interface SPDWeekSignInView ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *signinButton;
@property (weak, nonatomic) IBOutlet UIImageView *day7CoverImageView;
@property (weak, nonatomic) IBOutlet UILabel *day7DescLabel;
@property (weak, nonatomic) IBOutlet UILabel *day7TimeLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *day7ContentView;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIView *completeView;
@property (weak, nonatomic) IBOutlet UILabel *tomorrowRewardLabel;
@property (weak, nonatomic) IBOutlet UILabel *completeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *todayRewardImageView;
@property (weak, nonatomic) IBOutlet UILabel *todayEXPLabel;
@property (weak, nonatomic) IBOutlet UILabel *todayRewardLabel;
@property (weak, nonatomic) IBOutlet MarqueeLabel *tipsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *day7OKImageView;
@property (weak, nonatomic) IBOutlet UIButton *day7TimeButton;

@end

@implementation SPDWeekSignInView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.signinButton.frame = CGRectMake(103,406.5,170,44);
    self.signinButton.backgroundColor = [UIColor colorWithRed:0/255.0 green:212/255.0 blue:160/255.0 alpha:1.0];
    self.signinButton.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:212/255.0 blue:160/255.0 alpha:0.4].CGColor;
    self.signinButton.layer.shadowOffset = CGSizeMake(0,2);
    self.signinButton.layer.shadowOpacity = 1;
    self.signinButton.layer.shadowRadius = 13;
    self.signinButton.layer.cornerRadius = 22;
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"SPDWeekSignInItemCell" bundle:nil] forCellWithReuseIdentifier:@"SPDWeekSignInItemCell"];
    [self animating];
    self.completeLabel.text = SPDStringWithKey(@"恭喜你获得", nil);
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hide)];
    [self addGestureRecognizer:tap];
    
}

#pragma mark - data

- (IBAction)closeBtnClicked:(id)sender {
    [self removeFromSuperview];

}

- (IBAction)signinBtnClicked:(id)sender {
    [self requestSignIn];
}

#pragma mark - request

- (void)requestSignIn {
   NSMutableDictionary * dict = [NSMutableDictionary new];
   [dict setObject:[SPDApiUser currentUser].userId forKey:@"user_id"];
   [RequestUtils commonPostUrlRequestUtils:dict bURL:@"signin.week" RequestType:SPDRankUrl bAnimated:NO bAnimatedViewController:nil bSuccessDoSomething:^(id  _Nullable suceess) {
       self.todayRewardLabel.text = [NSString stringWithFormat:@"%@",suceess[@"rewardMsgToday"]];
       [self.todayRewardImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,suceess[@"rewardImgToday"]]]];
       self.todayEXPLabel.text = [NSString stringWithFormat:@"%d",[suceess[@"exp"] intValue]];
       
       NSTextAttachment * attach = [[NSTextAttachment alloc] init];
       attach.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,suceess[@"rewardImgTomorrow"]]]]];
       CGFloat paddingTop = [UIFont boldSystemFontOfSize:12].lineHeight - [UIFont boldSystemFontOfSize:12].pointSize;
       attach.bounds = CGRectMake(0, -paddingTop, 15, 15);
       NSAttributedString * imageStr = [NSAttributedString attributedStringWithAttachment:attach];
       NSMutableAttributedString * mutableAttriStr = [[NSMutableAttributedString alloc] initWithString:SPDStringWithKey(@"明天将获得：", nil)];
       [mutableAttriStr appendAttributedString:imageStr];
       NSAttributedString * str = [[NSAttributedString alloc]initWithString:suceess[@"rewardMsgTomorrow"]];
       [mutableAttriStr appendAttributedString:str];
       self.tomorrowRewardLabel.attributedText = mutableAttriStr;
       NSString * tip = suceess[@"tip"];
       self.tipsLabel.text = tip;
       self.completeView.hidden = NO;
       if (self.delegate && [self.delegate respondsToSelector:@selector(weekSignInViewSignInComplete)]) {
           [self.delegate weekSignInViewSignInComplete];
       }
       [[SPDCommonTool shareTool]checkExpToWhetherToUpgrage];

   } bFailure:^(id  _Nullable failure) {
       [self hide];
       [self showTips:failure[@"msg"]];
   }];
}

#pragma mark - method

- (void)setDataDict:(NSDictionary *)dataDict {
    _dataDict = dataDict;
    self.timeLabel.text = [NSString stringWithFormat:SPDLocalizedString(@"%@ 签到奖励"),_dataDict[@"weekData"]?:@""];
     NSArray * array = _dataDict[@"list"];
     NSDictionary * dict = array[6];
     [self.day7CoverImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,dict[@"rewardImg"]]]];
    self.day7DescLabel.text = [NSString stringWithFormat:@"%@",dict[@"rewardMsg"]?:@""];
    [self.day7TimeButton setTitle:([dict[@"isSign"] boolValue]) ?SPDStringWithKey(@"已领取", nil): SPDStringWithKey(@"7天", nil) forState:UIControlStateNormal];
    UIColor * color = ([dict[@"isSign"] boolValue]) ? [UIColor colorWithRed:26/255.0 green:26/255.0 blue:26/255.0 alpha:0.4] :[UIColor colorWithRed:26/255.0 green:26/255.0 blue:26/255.0 alpha:0.8];
    [self.day7TimeButton setTitleColor:color forState:UIControlStateNormal];
     self.day7ContentView.layer.borderWidth = ([dict[@"isSign"] boolValue]) ? 2: 0;
    self.day7OKImageView.hidden = !([dict[@"isSign"] boolValue]);
    self.day7ContentView.alpha = ([dict[@"isSign"] boolValue]) ? 0.6 : 1;
    [self.collectionView reloadData];
    NSString * check = _dataDict[@"check"];
     [self.signinButton setTitle:[check isEqualToString:@"true"] ? SPDStringWithKey(@"今日已签到", nil) : SPDStringWithKey(@"签到并领取", nil) forState:UIControlStateNormal];
    self.signinButton.userInteractionEnabled = ![check isEqualToString:@"true"];
    [self.signinButton setBackgroundColor:[check isEqualToString:@"true"] ? [UIColor whiteColor]: [UIColor colorWithHexString:COMMON_PINK]];
    [self.signinButton setTitleColor:[check isEqualToString:@"true"] ? [UIColor colorWithRed:26/255.0 green:26/255.0  blue:26/255.0  alpha:0.4] :[UIColor whiteColor] forState:UIControlStateNormal];
}

- (void)hide {
    [self removeFromSuperview];
}

- (void)animating {
    CAKeyframeAnimation * animation;
    animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = 0.30;
    animation.removedOnCompletion = YES;
    animation.fillMode = kCAFillModeForwards;
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1, 1.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    [_bgView.layer addAnimation:animation forKey:nil];
    
    [UIView animateWithDuration:0.30 animations:^{
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.25];
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 6;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSArray * array = self.dataDict[@"list"];
    SPDWeekSignInItemCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SPDWeekSignInItemCell" forIndexPath:indexPath];
    NSDictionary * dict = array[indexPath.row];
    BOOL isSign= [dict[@"isSign"] boolValue];
    NSInteger week = [_dataDict[@"week"] integerValue];
    cell.statusLabel.text = isSign ? SPDStringWithKey(@"已领取", nil) : [NSString stringWithFormat:@"%@ %@",@(indexPath.row + 1),SPDStringWithKey(@"天", nil)];
    cell.dic = array[indexPath.row];
    cell.itemContentView.layer.borderWidth  = (week == (indexPath.row +1) && [_dataDict[@"check"] isEqualToString:@"false"]) ? 2 : 0;
    return cell;
}


@end
