//
//  RankOptionCollectionViewCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/12/12.
//  Copyright © 2018 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RankOptionCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end

NS_ASSUME_NONNULL_END
