//
//  HomeClanNoClanCell.m
//  SimpleDate
//
//  Created by 李楠 on 17/8/1.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "HomeClanNoClanCell.h"

@interface HomeClanNoClanCell()

@property (weak, nonatomic) IBOutlet UIButton *joinClanBtn;
@property (weak, nonatomic) IBOutlet UIButton *createClanBtn;

@end

@implementation HomeClanNoClanCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.createClanBtn setTitle:SPDStringWithKey(@"创建家族", nil) forState:UIControlStateNormal];
    [self.joinClanBtn setTitle:SPDStringWithKey(@"加入家族", nil) forState:UIControlStateNormal];

    self.NoClanTipLabel.text = [NSString stringWithFormat:@"%@%@%@", SPDStringWithKey(@"你还没有家族，快来", nil), SPDStringWithKey(@"加入/创建", nil),SPDStringWithKey(@"属于自己的家族吧", nil)];
    self.tipsLabel.text = SPDStringWithKey(@"还没有访问过家族房间，快去找小伙伴玩耍吧~", nil);
}

- (IBAction)joinInClan:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didClickJoinInClan)]) {
        [self.delegate didClickJoinInClan];
    }
}

- (IBAction)createClan:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didClickCreateClan)]) {
        [self.delegate didClickCreateClan];
    }
}

@end
