//
//  HomeBannerCell.h
//  SimpleDate
//
//  Created by 侯玲 on 17/8/12.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"

@protocol HomeBannerCellDelegate <NSObject>

@optional
- (void)clickedBannerCellWithIndex: (NSInteger)index;

@end

@interface HomeBannerCell : UICollectionViewCell

//@property (nonatomic,strong)SDCycleScrollView * headerscorllView;
@property (weak, nonatomic) IBOutlet SDCycleScrollView *headerscorllView;

@property (nonatomic,strong)NSMutableArray * imagesArray;

@property (nonatomic,weak)id<HomeBannerCellDelegate>delegate;

@end
