//
//  SPDUpGradeView.h
//  SimpleDate
//
//  Created by Monkey on 2017/9/1.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SPDUpGradeViewDelegate <NSObject>

@optional
- (void)upgradeViewHiddenBtnClicked;
- (void)upgradeViewSureBtnClicked;

@end

@interface SPDUpGradeView : UIView

@property (nonatomic,weak)id<SPDUpGradeViewDelegate> delegate;

@property (nonatomic,assign)NSInteger current_level;

@end
