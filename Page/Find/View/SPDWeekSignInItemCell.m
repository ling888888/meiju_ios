//
//  SPDWeekSignInItemCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/1/9.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "SPDWeekSignInItemCell.h"

@interface SPDWeekSignInItemCell ()
@property (weak, nonatomic) IBOutlet UIImageView *OKImageView;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIImageView *itemImageView;

@end

@implementation SPDWeekSignInItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.OKImageView.image = [UIImage imageNamed: !kIsMirroredLayout ? @"ic_wode_qiandaotanchuang_yilingqu_duohao":@"ic_wode_qiandaotanchuang_yilingqu_duohao_j"];
}

- (void)setDic:(NSDictionary *)dic {
    _dic = dic;
    BOOL isSign= [_dic[@"isSign"] boolValue];
    self.itemContentView.alpha = isSign ? 0.6 :1;
    self.OKImageView.hidden = !isSign;
    [self.itemImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,_dic[@"rewardImg"]]]];
    self.descLabel.text = [NSString stringWithFormat:@"%@",_dic[@"rewardMsg"]?:@""];// 兼容服务器 字段类型
    self.statusLabel.textColor  = isSign? [UIColor colorWithRed:26/255.0 green:26/255.0 blue:26/255.0 alpha:0.4] :[UIColor colorWithRed:26/255.0 green:26/255.0 blue:26/255.0 alpha:0.8];
}

@end
