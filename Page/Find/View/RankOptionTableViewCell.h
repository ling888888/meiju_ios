//
//  RankOptionTableViewCell.h
//  SimpleDate
//
//  Created by 李楠 on 2020/2/9.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RankOptionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *optionLabel;

@end

NS_ASSUME_NONNULL_END
