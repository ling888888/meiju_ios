//
//  VipPlusPrivilegeCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/7/31.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "VipPlusPrivilegeCell.h"
#import "VipPrivilegeView.h"

@interface VipPlusPrivilegeCell ()

@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet VipPrivilegeView *privilege1View;
@property (weak, nonatomic) IBOutlet VipPrivilegeView *privilege2View;
@property (weak, nonatomic) IBOutlet VipPrivilegeView *privilege3View;
@property (weak, nonatomic) IBOutlet VipPrivilegeView *privilege4View;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;
@property (weak, nonatomic) IBOutlet UIImageView *isBuyImageView;

@end

@implementation VipPlusPrivilegeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.titleImageView.image = [UIImage imageNamed:@"vip_title_bg"].adaptiveRtl;
    self.titleLabel.text = SPDStringWithKey(@"高级会员", nil);
    self.privilege1View.type = @"video";
    self.privilege2View.type = @"call";
    self.privilege3View.type = @"gold";
    [self.privilege3View.actionBtn addTarget:self action:@selector(clickGetBtn:) forControlEvents:UIControlEventTouchUpInside];
    self.privilege4View.type = @"vehicle";
    [self.privilege4View.actionBtn addTarget:self action:@selector(clickEquipBtn:) forControlEvents:UIControlEventTouchUpInside];
    self.priceLabel.text = @"$99.99";
    self.timeLabel.text = [NSString stringWithFormat:@"30%@", SPDStringWithKey(@"天", nil)];
    self.isBuyImageView.image = [UIImage imageNamed:@"vip_bought"].adaptiveRtl;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setIsBuy:(BOOL)isBuy {
    _isBuy = isBuy;
    
    self.isBuyImageView.hidden = !_isBuy;
    if (_isBuy) {
        [self.buyButton setTitle:SPDStringWithKey(@"续费", nil) forState:UIControlStateNormal];
    } else {
        [self.buyButton setTitle:SPDStringWithKey(@"开通", nil) forState:UIControlStateNormal];
    }
}

- (void)setAward_num:(NSNumber *)award_num{
    _award_num = award_num;
    
    self.privilege3View.descLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"每日可领取%@金币", nil), award_num];
}

- (void)setIs_get:(BOOL)is_get {
    _is_get = is_get;
    
    NSString *title = _is_get ? SPDStringWithKey(@"已领取", nil) : SPDStringWithKey(@"领取", nil);
    [self.privilege3View.actionBtn setTitle:title forState:UIControlStateNormal];
    self.privilege3View.actionBtn.userInteractionEnabled = !_is_get;
    self.privilege3View.actionBtn.backgroundColor = [UIColor colorWithHexString:_is_get ? @"999999" : @"ff7781"];
}

- (void)setIs_outfit:(BOOL)is_outfit {
    _is_outfit = is_outfit;
    
    NSString *title = _is_outfit ? SPDStringWithKey(@"装备中", nil) : SPDStringWithKey(@"装备", nil);
    [self.privilege4View.actionBtn setTitle:title forState:UIControlStateNormal];
    self.privilege4View.actionBtn.userInteractionEnabled = !_is_outfit;
    self.privilege4View.actionBtn.backgroundColor = [UIColor colorWithHexString:_is_outfit ? @"999999" : @"ff7781"];
}

- (IBAction)clickBuyBtn:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickBuyPlusVip)]) {
        [self.delegate didClickBuyPlusVip];
        
        if (self.isBuy) {
            //[MobClick event:@"clickVIP_9999Renewal"];
        } else {
            //[MobClick event:@"clickVIP_9999Open"];
        }
    }
}

- (void)clickGetBtn:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickGetAward)]) {
        [self.delegate didClickGetAward];
        
        //[MobClick event:@"clickVIP_9999Receive"];
    }
}

- (void)clickEquipBtn:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickEquipVehicle)]) {
        [self.delegate didClickEquipVehicle];
        
        //[MobClick event:@"clickVIP_9999Equip"];
    }
}

@end
