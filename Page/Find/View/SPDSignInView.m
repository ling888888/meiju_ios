//
//  SPDSignInView.m
//  SimpleDate
//
//  Created by Monkey on 2017/8/31.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDSignInView.h"

@interface SPDSignInView ()

@property (weak, nonatomic) IBOutlet UILabel *sigin_exp_Label;
@property (weak, nonatomic) IBOutlet UIButton *signinBtn;
@property (weak, nonatomic) IBOutlet UIImageView *signInBgImg;
@property (weak, nonatomic) IBOutlet UILabel *signInSuccessLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *signin_coins_Label; //此次签到 将要获得的金币

@end

@implementation SPDSignInView

+ (instancetype)shareInstance {
    static SPDSignInView * view;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        view = [[[NSBundle mainBundle]loadNibNamed:@"SPDSignInView" owner:self options:nil]lastObject];
    });
    return view;
}


- (void)awakeFromNib{
    [super awakeFromNib];
    self.titleLabel.text = SPDStringWithKey(@"每日签到",nil);
}

- (void)configsigninExp:(NSString *)signin_exp andStr:(NSString *)str andSigninCoins:(NSString *)signin_coins{
    self.deleteBtn.hidden = NO;
    self.signInSuccessLabel.hidden = YES;
    self.signInBgImg.image = [UIImage imageNamed:@"ic_signin"].adaptiveRtl;
    [self.signinBtn setBackgroundColor:[UIColor clearColor]];
    [self.signinBtn setBackgroundColor:SPD_HEXCOlOR(COMMON_PINK)];
    [self.signinBtn setTitle:SPDStringWithKey(@"签到",nil) forState:UIControlStateNormal];
    [self.signinBtn setTitleColor:SPD_HEXCOlOR(@"ffffff") forState:UIControlStateNormal];
    self.signinBtn.titleLabel.font = [UIFont systemFontOfSize:18];

    self.sigin_exp_Label.text = [NSString stringWithFormat:@"+%@",signin_exp];
    self.signin_coins_Label.text = [NSString stringWithFormat:@"+%@",signin_coins];

}
- (IBAction)dismissBtnClicked:(id)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedDismiss)]) {
        [self.delegate clickedDismiss];
    }
}

- (void)confignSignInSuccess {
    self.deleteBtn.hidden = YES;
    self.signInSuccessLabel.hidden = NO;
    self.sigin_exp_Label.hidden = YES;
    self.signin_coins_Label.hidden = YES;
    self.signinBtn.hidden = YES;
    self.signInSuccessLabel.text = SPDStringWithKey(@"签到成功\n记得明天再来哦～", nil);
    self.signInBgImg.image = [UIImage imageNamed:@"signin_complete"];
}

- (IBAction)signinBtnClicked:(id)sender {
//    [self confignSignInSuccess];
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedSignIn)]) {
        [self.delegate clickedSignIn];
    }
}


@end
