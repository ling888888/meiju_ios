//
//  LY_ClanFollowHeaderView.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/9.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_ClanFollowHeaderView.h"
#import "SPDCreateClanViewController.h"
#import "SPDClanListViewController.h"
#import "SPDClanHotListCell.h"
#import "ZegoKitManager.h"

@interface LY_ClanFollowHeaderView () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, SPDClanHotListCellDelegate>

@property(nonatomic, strong) UIImageView *myFamilyIconView;

@property(nonatomic, strong) UILabel *myFamilyTitleLabel;

@property(nonatomic, strong) UIImageView *myFollowIconView;

@property(nonatomic, strong) UILabel *myFollowTitleLabel;

@property(nonatomic, strong) UILabel *recommendTextLabel;

@property(nonatomic, strong) UIImageView *noDataBgImgView;

@property(nonatomic, strong) UICollectionView *collectionView;

@property(nonatomic, strong) FamilyModel *clan;

@end

@implementation LY_ClanFollowHeaderView

- (UIImageView *)myFamilyIconView {
    if (!_myFamilyIconView) {
        _myFamilyIconView = [[UIImageView alloc] init];
        _myFamilyIconView.contentMode = UIViewContentModeScaleAspectFit;
        _myFamilyIconView.image = [UIImage imageNamed:@"ic_shouye_guanzhu_wodejiazu"];
    }
    return _myFamilyIconView;
    
}

- (UILabel *)myFamilyTitleLabel {
    if (!_myFamilyTitleLabel) {
        _myFamilyTitleLabel = [[UILabel alloc] init];
        _myFamilyTitleLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _myFamilyTitleLabel.font = [UIFont mediumFontOfSize:15];
        _myFamilyTitleLabel.text = @"我的家族".localized;
    }
    return _myFamilyTitleLabel;
}

- (UIImageView *)myFollowIconView {
    if (!_myFollowIconView) {
        _myFollowIconView = [[UIImageView alloc] init];
        _myFollowIconView.contentMode = UIViewContentModeScaleAspectFit;
        _myFollowIconView.image = [UIImage imageNamed:@"ic_shouye_guanzhu_wodegunzhu"];
    }
    return _myFollowIconView;
}

- (UILabel *)myFollowTitleLabel {
    if (!_myFollowTitleLabel) {
        _myFollowTitleLabel = [[UILabel alloc] init];
        _myFollowTitleLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _myFollowTitleLabel.font = [UIFont mediumFontOfSize:15];
        _myFollowTitleLabel.text = @"我的关注".localized;
    }
    return _myFollowTitleLabel;
}

- (UILabel *)recommendTextLabel {
    if (!_recommendTextLabel) {
        _recommendTextLabel = [[UILabel alloc] init];
        _recommendTextLabel.textColor = [UIColor colorWithHexString:@"#00D4A0"];
        _recommendTextLabel.font = [UIFont mediumFontOfSize:15];
        _recommendTextLabel.numberOfLines = 0;
        _recommendTextLabel.textAlignment = NSTextAlignmentCenter;
        _recommendTextLabel.text = @"你还没有关注的家族，为你推荐以下家族~".localized;
    }
    return _recommendTextLabel;
}

- (UIImageView *)noDataBgImgView {
    if (!_noDataBgImgView) {
        _noDataBgImgView = [[UIImageView alloc] init];
        _noDataBgImgView.image = [UIImage imageNamed:@"img_shouye_guanzhu_meiyoujiazu"];
        _noDataBgImgView.userInteractionEnabled = true;
    }
    return _noDataBgImgView;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.scrollEnabled = false;
        [_collectionView registerNib:[UINib nibWithNibName:@"SPDClanHotListCell" bundle:nil] forCellWithReuseIdentifier:@"SPDClanHotListCell"];
    }
    return _collectionView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.myFamilyIconView];
    [self addSubview:self.myFamilyTitleLabel];
    [self addSubview:self.recommendTextLabel];
    [self addSubview:self.myFollowIconView];
    [self addSubview:self.myFollowTitleLabel];
}

- (void)setupUIFrame {
    [self.myFamilyIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(15);
    }];
    [self.myFamilyTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.myFamilyIconView.mas_trailing).offset(5);
        make.centerY.mas_equalTo(self.myFamilyIconView);
    }];
    [self.recommendTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(45);
        make.trailing.mas_equalTo(-45);
        make.height.mas_equalTo(50);
        make.bottom.mas_equalTo(-15);
    }];
    [self.myFollowIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.bottom.mas_equalTo(-15);
    }];
    [self.myFollowTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.myFollowIconView.mas_trailing).offset(5);
        make.centerY.mas_equalTo(self.myFollowIconView);
    }];
}

- (void)showNoDataViewWithIsShowRecommendText:(BOOL)isShowRecommendText {
    if (_collectionView) {
        [_collectionView removeFromSuperview];
        _collectionView = nil;
    }
    UILabel *noDataTitleLabel = [[UILabel alloc] init];
    noDataTitleLabel.numberOfLines = 0;
    noDataTitleLabel.textColor = [UIColor whiteColor];
    noDataTitleLabel.font = [UIFont systemFontOfSize:15];
    noDataTitleLabel.textAlignment = NSTextAlignmentCenter;
    noDataTitleLabel.text = @"你还没有家族，快来加入/创建属于自己的家族吧".localized;

    UIButton *noDataCreatClanBtn = [[UIButton alloc] init];
    [noDataCreatClanBtn setTitleColor:[UIColor colorWithHexString:@"#00D4A0"] forState:UIControlStateNormal];
    noDataCreatClanBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [noDataCreatClanBtn setTitle:@"创建家族".localized forState:UIControlStateNormal];
    noDataCreatClanBtn.layer.cornerRadius = 15;
    noDataCreatClanBtn.layer.masksToBounds = true;
    noDataCreatClanBtn.backgroundColor = [UIColor whiteColor];
    [noDataCreatClanBtn addTarget:self action:@selector(creatClanBtnAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *noDataJoinClanBtn = [[UIButton alloc] init];
    [noDataJoinClanBtn setTitleColor:[UIColor colorWithHexString:@"#00D4A0"] forState:UIControlStateNormal];
    noDataJoinClanBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [noDataJoinClanBtn setTitle:@"加入家族".localized forState:UIControlStateNormal];
    noDataJoinClanBtn.layer.cornerRadius = 15;
    noDataJoinClanBtn.layer.masksToBounds = true;
    noDataJoinClanBtn.backgroundColor = [UIColor whiteColor];
    [noDataJoinClanBtn addTarget:self action:@selector(joinClanBtnAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.noDataBgImgView];
    [self.noDataBgImgView addSubview:noDataTitleLabel];
    [self.noDataBgImgView addSubview:noDataCreatClanBtn];
    [self.noDataBgImgView addSubview:noDataJoinClanBtn];
    
    [self.noDataBgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.myFamilyIconView.mas_bottom).offset(15);
        make.leading.mas_equalTo(8);
        make.trailing.mas_equalTo(-8);
        make.height.mas_equalTo((kScreenW - 16) / 359 * 120);
    }];

    [noDataTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(15);
        make.leading.mas_equalTo(70);
        make.trailing.mas_equalTo(-70);
    }];
    
    NSArray *btnArray = @[noDataCreatClanBtn, noDataJoinClanBtn];
    [btnArray mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedItemLength:105 leadSpacing:50 tailSpacing:50];
    [btnArray mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(30);
        make.bottom.mas_equalTo(-18);
    }];
    
    if (isShowRecommendText) {
        [self.myFollowIconView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.recommendTextLabel.mas_top).offset(-15);
        }];
        [self.recommendTextLabel setHidden:false];
    } else {
        [self.myFollowIconView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(-10);
        }];
        [self.recommendTextLabel setHidden:true];
    }
}

- (void)showMyClanViewWith:(FamilyModel *)clan isShowRecommendText:(BOOL)isShowRecommendText {
    self.clan = clan;
    if (_noDataBgImgView) {
        [_noDataBgImgView removeFromSuperview];
        _noDataBgImgView = nil;
    }
    [self addSubview:self.collectionView];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.myFamilyIconView.mas_bottom).offset(15);
        make.leading.trailing.mas_equalTo(self);
        make.bottom.mas_equalTo(self.myFollowIconView.mas_top).offset(-15);
    }];
    
    if (isShowRecommendText) {
        [self.myFollowIconView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.recommendTextLabel.mas_top).offset(-10);
        }];
        [self.recommendTextLabel setHidden:false];
    } else {
        [self.myFollowIconView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(-10);
        }];
        [self.recommendTextLabel setHidden:true];
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SPDClanHotListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SPDClanHotListCell" forIndexPath:indexPath];
    cell.model = self.clan;
    cell.delegate = self;
    
    cell.backgroundColor = [UIColor yellowColor];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [ZegoManager joinRoomFrom:self.VC clanId:self.clan._id];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(kScreenW - 16, 90);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, 8, 0, 8);
}

- (void)creatClanBtnAction {
    SPDCreateClanViewController *vc = [[SPDCreateClanViewController alloc] init];
    [self.VC.navigationController pushViewController:vc animated:true];
}

- (void)joinClanBtnAction {
    SPDClanListViewController *vc = [[SPDClanListViewController alloc] init];
    [self.VC.navigationController pushViewController:vc animated:true];
}


@end
