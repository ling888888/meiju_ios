//
//  RankListHeaderCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/12/12.
//  Copyright © 2018 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class RankListHeaderCell, RankModel;

@protocol RankListHeaderCellDelegate <NSObject>

@optional

- (void)rankListHeaderCell:(RankListHeaderCell *)cell didTapAvatarWithModel:(RankModel *)model;

@end

@interface RankListHeaderCell : UICollectionViewCell

@property (nonatomic, copy) NSString *type; // @[@"charm", @"riche", @"person", @"clan", @"magic"]
@property (nonatomic, strong) NSArray *dataArr;

@property (nonatomic, weak) id<RankListHeaderCellDelegate> delegate;


@end

NS_ASSUME_NONNULL_END
