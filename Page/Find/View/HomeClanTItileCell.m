//
//  HomeClanTItileCell.m
//  SimpleDate
//
//  Created by 李楠 on 17/8/1.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "HomeClanTItileCell.h"
#import "UIButton+HHImagePosition.h"


@implementation HomeClanTItileCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.moreButton setTitle:SPDStringWithKey(@"浏览全部", nil) forState:UIControlStateNormal];
    self.arrowImageView.image = [UIImage imageNamed:@"ic_all_clan_arrow"].adaptiveRtl;
}

- (IBAction)clickMoreButton:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didClickMoreButtonWithTitle:)]) {
        [self.delegate didClickMoreButtonWithTitle:self.titleLabel.text];
    }
}

@end
