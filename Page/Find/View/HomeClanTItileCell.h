//
//  HomeClanTItileCell.h
//  SimpleDate
//
//  Created by 李楠 on 17/8/1.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HomeClanTItileCellDelegate <NSObject>

@optional

- (void)didClickMoreButtonWithTitle:(NSString *)title;

@end

@interface HomeClanTItileCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *moreButton;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (weak, nonatomic) IBOutlet UIView *separator;

@property (nonatomic, weak) id<HomeClanTItileCellDelegate> delegate;

@end
