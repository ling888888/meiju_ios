//
//  SPDClanInputCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/3/22.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPDClanInputCell : UITableViewCell

@property (nonatomic, copy) NSString *type; // clan_name, clan_declar, greeting
@property (nonatomic, strong) NSMutableDictionary *params;
@property (nonatomic, copy) NSString *text; 

@end
