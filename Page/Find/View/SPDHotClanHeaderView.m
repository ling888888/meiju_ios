//
//  SPDHotClanHeaderView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/2/7.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "SPDHotClanHeaderView.h"
#import "RankHomePageView.h"
#import "RankContainerController.h"

@interface SPDHotClanHeaderView ()<RankHomePageViewDelegate>

@property (nonatomic, strong) LY_BannerView *bannerView;

@property (nonatomic, strong) RankHomePageView *rankView;

@end


@implementation SPDHotClanHeaderView

- (LY_BannerView *)bannerView {
    if (!_bannerView) {
        _bannerView = [[LY_BannerView alloc] init];
        _bannerView.isLiveViewBanner = true;
    }
    return _bannerView;
}

- (RankHomePageView *)rankView {
    if (!_rankView) {
        _rankView = [[[NSBundle mainBundle]loadNibNamed:@"RankHomePageView" owner:self options:nil]firstObject];
    }
    return _rankView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.bannerView];
    [self addSubview:self.rankView];
}

- (void)setupUIFrame {
    [self.bannerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.mas_equalTo(self);
        make.height.mas_equalTo(LYBannerViewHeight);
    }];
    [self.rankView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bannerView.mas_bottom).offset(10);
        make.leading.mas_equalTo(10);
        make.trailing.mas_equalTo(-10);
        make.bottom.mas_equalTo(-10);
    }];
}

- (void)setBannerList:(NSArray<BannerModel *> *)bannerList {
    _bannerList = bannerList;
    
    self.bannerView.bannerList = bannerList;
}

- (void)setRankDict:(NSMutableDictionary *)rankDict {
    _rankDict = rankDict;
    NSMutableArray * firstArray = [NSMutableArray new];
    NSMutableArray * secondArray = [NSMutableArray new];
    NSMutableArray * thirdArray = [NSMutableArray new];

    NSArray *riche = _rankDict[@"riche"];
    NSArray *charm = _rankDict[@"charm"];
    NSArray *person = _rankDict[@"person"];
    NSArray *clan = _rankDict[@"clan"];
    NSArray *magic = _rankDict[@"magic"];

    for (int i = 0 ; i < 3; i++) {
        NSMutableArray *array;
        if (i == 0) {
            array = firstArray;
        } else if (i == 1) {
            array = secondArray;
        } else {
            array = thirdArray;
        }
        
        if (riche.count > i) {
            [array addObject:riche[i][@"avatar"]];
        } else {
            [array addObject:@"404.jpg"];
        }
        if (charm.count > i) {
            [array addObject:charm[i][@"avatar"]];
        } else {
            [array addObject:@"404.jpg"];
        }
        if (person.count > i) {
            [array addObject:person[i][@"avatar"]];
        } else {
            [array addObject:@"404.jpg"];
        }
        if (clan.count > i) {
            [array addObject:clan[i][@"avatar"]];
        } else {
            [array addObject:@"404.jpg"];
        }
        if (magic.count > i) {
            [array addObject:magic[i][@"avatar"]];
        } else {
            [array addObject:@"404.jpg"];
        }
    }
    
    self.rankView.firstImageURLStringsGroup = firstArray;
    self.rankView.secondImageURLStringsGroup = secondArray;
    self.rankView.thirdImageURLStringsGroup = thirdArray;
    self.rankView.delegate = self;
}

#pragma mark - RankHomePageView

- (void)rankHomePageView:(RankHomePageView *)view didTapWithIndex:(NSInteger)index {
    RankContainerController * rank = [[RankContainerController alloc] init];
    rank.selectedType = @"riche";
    [self.VC.navigationController pushViewController:rank animated:true];
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"WMPageConrtollerJumpToRank" object:@(index)];
}

@end
