//
//  ReportUserListCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/5/8.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "ReportUserListCell.h"

@interface ReportUserListCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation ReportUserListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setDict:(NSDictionary *)dict {
    _dict = dict;
    self.titleLabel.text = _dict[@"name"];
}

@end
