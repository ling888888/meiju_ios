//
//  HomeGlobalMessageCell.h
//  SimpleDate
//
//  Created by 侯玲 on 17/8/12.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPDVoiceLiveGlobalScrollView.h"

@interface HomeGlobalMessageCell : UICollectionViewCell

@property (nonatomic, strong) SPDVoiceLiveGlobalScrollView *globalScrollView;
@property (nonatomic, copy) NSString *clanID;

@end
