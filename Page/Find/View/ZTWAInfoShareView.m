//
//  ZTWAInfoShareView.m
//  zhitou
//
//  Created by TragedyStar on 16/4/10.
//  Copyright © 2016年 zhitou. All rights reserved.
//

#import "ZTWAInfoShareView.h"
#import "UIButton+HHImagePosition.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "UIImageView+AFNetworking.h"
#import "UIImage+GIF.h"
#import "SPDContactsViewController.h"

NSString *const ZTWAInfoShareViewPlatformWeChatTimeLine = @"ZTWAInfoShareViewPlatformWeChatTimeLine";
NSString *const ZTWAInfoShareViewPlatformWeChatFriend = @"ZTWAInfoShareViewPlatformWeChatFriend";
NSString *const ZTWAInfoShareViewPlatformQQFriend = @"ZTWAInfoShareViewPlatformQQFriend";
NSString *const ZTWAInfoShareViewPlatformQQZone = @"ZTWAInfoShareViewPlatformQQZone";
NSString *const ZTWAInfoShareViewPlatformSina = @"ZTWAInfoShareViewPlatformSina";

@interface ZTWAInfoShareView ()

@property (nonatomic, weak) IBOutlet UIView *selfView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;


@property (weak, nonatomic) IBOutlet UIView *invite_codeView;
@property (weak, nonatomic) IBOutlet UILabel *inviteCountLabel;

@property (weak, nonatomic) IBOutlet UILabel *myinviteCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *inviteTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *linkTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *linkLabel;

@property (weak, nonatomic) IBOutlet UIButton *linkBtn;

@property (weak, nonatomic) IBOutlet UIImageView *whatsappImgView;
@property (weak, nonatomic) IBOutlet UIImageView *twitterImgVIEW;
@property (weak, nonatomic) IBOutlet UIImageView *messengerImgView;
@property (weak, nonatomic) IBOutlet UIImageView *telegramImgView;
@property (weak, nonatomic) IBOutlet UIImageView *facebookImgView;
@property (weak, nonatomic) IBOutlet UIImageView *wechatImg;
@property (weak, nonatomic) IBOutlet UIImageView *timelineImg;
@property (weak, nonatomic) IBOutlet UIImageView *contactImg;

@end

@implementation ZTWAInfoShareView

+ (instancetype)shareView
{
    return [[self alloc] initWithFrame:[UIScreen mainScreen].bounds];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self loadSelfView];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self loadSelfView];
    }
    return self;
}

- (void)loadSelfView
{
    [[NSBundle mainBundle] loadNibNamed:@"ZTWAInfoShareView" owner:self options:nil];
    [self addSubview:self.selfView];
    
    self.bottomView.frame = CGRectMake(0, self.bounds.size.height, self.bounds.size.width, 370);
    
    UITapGestureRecognizer *tapCancel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancelShare)];
    [self addGestureRecognizer:tapCancel];
    
    [self localStrings];
    
    [self addSubViewTapGesture];
    
}


- (void)addSubViewTapGesture {
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handle:)];
    [self.whatsappImgView addGestureRecognizer:tap];
    UITapGestureRecognizer * tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handle:)];
    [self.telegramImgView addGestureRecognizer:tap1];
    self.telegramImgView.tag = 3;
    
    UITapGestureRecognizer * tapm = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shareToMessager)];
    self.messengerImgView.tag = 2;
    [self.messengerImgView addGestureRecognizer:tapm];
    
    UITapGestureRecognizer * tapf = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handle:)];
    self.facebookImgView.tag = 4;
    [self.facebookImgView addGestureRecognizer:tapf];
    
    UITapGestureRecognizer * tapt = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shareToSina:)];
    [self.twitterImgVIEW addGestureRecognizer:tapt];
    
    UITapGestureRecognizer * tapwl = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shareToWeChatTimeLine:)];
    [self.timelineImg addGestureRecognizer:tapwl];
    
    UITapGestureRecognizer * tapw = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shareToWeChatFriend:)];
    [self.wechatImg addGestureRecognizer:tapw];
    
    UITapGestureRecognizer * tapc = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shareToContact:)];
    [self.contactImg addGestureRecognizer:tapc];
}

- (void)handle:(UITapGestureRecognizer *)tap{
    NSMutableString *str = [NSMutableString stringWithFormat:@"%@",self.shareDict[@"share_url"] ?:self.invite_link];
    if (!str) {return;}
    NSString * url ;
    UIView * v = (UIView *)tap.view;
    if (v.tag == 0) {
        NSString *msg = [NSString stringWithFormat:@"%@&from=whatsapp",self.shareDict[@"share_url"] ?:self.invite_link];
        NSLog(@"%@",msg);
        url = [NSString stringWithFormat:@"whatsapp://send?text=%@", [msg stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
        
        //[MobClick event:@"clickShare_WhatsApp"];
    }else if (v.tag == 3){
        NSString *msg = [NSString stringWithFormat:@"%@&from=telegram",self.shareDict[@"share_url"] ?:self.invite_link];
        url = [NSString stringWithFormat:@"https://t.me/share/url?url=%@", msg];
        NSLog(@"%@",msg);
        
        //[MobClick event:@"clickShare_Telegram"];
    }else if (v.tag == 4){
        NSString *msg = [NSString stringWithFormat:@"%@",self.shareDict[@"share_url"] ?:self.invite_link];
        NSLog(@"%@",msg);
        url = [NSString stringWithFormat:@"https://www.facebook.com/sharer/sharer.php?u=%@",msg];
        //[MobClick event:@"clickShare_FB"];
    }
    
    NSURL *whatsappURL = [NSURL URLWithString: url];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
    } else {
        [SPDCommonTool showWindowToast:SPDStringWithKey(@"未安裝此应用，请安装后再进行分享", nil)];
    }
}

- (void)localStrings {
    [self.linkBtn setTitle:SPDStringWithKey(@"点击复制", nil) forState:UIControlStateNormal];
    self.myinviteCodeLabel.text = [SPDCommonTool getInvitationCodeWithUserId:[SPDApiUser currentUser].userId];
    self.inviteTitleLabel.text = SPDStringWithKey(@"我的邀请码:", nil);
    self.linkTitleLabel.text = SPDStringWithKey(@"邀请链接:", nil);
}

//点击复制邀请链接
- (IBAction)linkBtnClicked:(id)sender {
    UIPasteboard *pab = [UIPasteboard generalPasteboard];
    NSString *string = self.linkLabel.text ? : @"";
    [pab setString:string];
    if (pab == nil) {
        [SPDCommonTool showWindowToast:SPDStringWithKey(@"复制失败", nil)];
    }else{
        [SPDCommonTool showWindowToast:SPDStringWithKey(@"复制成功", nil)];
    }
}

- (void)setInvite_link:(NSString *)invite_link {
    _invite_link = invite_link;
    self.linkLabel.text = [NSString stringWithFormat:@"%@",_invite_link];
}

- (void)setShareDict:(NSDictionary *)shareDict {
    _shareDict = shareDict;
    if (_shareDict[@"share_url"]) {
        self.linkLabel.text = [NSString stringWithFormat:@"%@",_shareDict[@"share_url"]];
    }else{
        self.linkLabel.text = @"";
    }
}

- (void)show {
    [self requestUserInfoData];
    NSNumber *invite_count = [[NSUserDefaults standardUserDefaults] objectForKey:MYINVITECOUNT];
    if (invite_count) {
        self.invite_count = invite_count.intValue;
    } else {
        self.invite_count = 0;
        [self requesInviteCount];
    }
}

-(void)requestUserInfoData {
    
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.info.detail" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        NSDictionary *myInfoDict = [NSDictionary dictionaryWithDictionary:suceess];
        self.shareDict = myInfoDict[@"share"];
        //给分享弹出框邀请码赋值！
        [[UIApplication sharedApplication].keyWindow addSubview:self];
        [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.selfView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
            self.bottomView.frame = CGRectMake(0, self.frame.size.height-370, self.frame.size.width, 370);
            
        } completion:^(BOOL finished) {
            [self addAnimation];
        }];
        
        
    } bFailure:nil];
    
}

- (void)requesInviteCount {
    NSDictionary *dic = @{@"user_id": [SPDApiUser currentUser].userId};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"invite.count" isClan:YES bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        [[NSUserDefaults standardUserDefaults] setObject:@([suceess[@"count"] intValue]) forKey:MYINVITECOUNT];
        self.invite_count = [suceess[@"count"] intValue];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)addAnimation {
    float bigSize = 1.3;
    CABasicAnimation *pulseAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    pulseAnimation.duration = 0.5;
    pulseAnimation.toValue = [NSNumber numberWithFloat:bigSize];
    pulseAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    // 倒转动画
    pulseAnimation.autoreverses = YES;
    // 设置重复次数为无限大
    pulseAnimation.repeatCount = FLT_MAX;
    // 添加动画到layer
    [self.whatsappImgView.layer addAnimation:pulseAnimation forKey:@"transform.scale"];
}

- (void)dismiss {
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.selfView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
        self.bottomView.frame = CGRectMake(0, self.frame.size.height, self.frame.size.width, 370);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}


- (IBAction)cancelShare {
    [self dismiss];
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancleShare)]) {
        [self.delegate cancleShare];
    }
}

- (IBAction)shareToWeChatTimeLine:(id)sender {
    [self shareToPlatform:SPDShareToWechatTimeline];
    
    //[MobClick event:@"clickShare_WeChatMoment"];
}

- (IBAction)shareToWeChatFriend:(id)sender {
    [self shareToPlatform:SPDShareToWechatSession];
    
    //[MobClick event:@"clickShare_WeChat"];
}

- (void)shareToFacebook {
    [self shareToPlatform:SPDShareToFacebook];
}

- (IBAction)shareToContact:(id)sender {
    [self dismiss];
    SPDContactsViewController *vc = [[SPDContactsViewController alloc] init];
    vc.fromShare = YES;
    [[SPDCommonTool getWindowTopViewController].navigationController pushViewController:vc animated:YES];
    
    //[MobClick event:@"clickShare_AddressBook"];
}

//分享twittwe
- (IBAction)shareToSina:(id)sender {
    [self shareToPlatform:SPDShareToTwitter];
    
    //[MobClick event:@"clickShare_Twitter"];
}

- (void)shareToMessager {
//    FBSDKShareMessengerURLActionButton *urlButton = [[FBSDKShareMessengerURLActionButton alloc] init];
//    NSMutableString * str = [NSMutableString stringWithFormat:@"%@",self.shareDict[@"share_url"]?:self.invite_link];
//    if(!str) return;
//    [str appendString:@"&from=messenger"];
//    urlButton.title = str;
//    urlButton.url = [NSURL URLWithString:str];
//
//    FBSDKShareMessengerGenericTemplateElement *element = [[FBSDKShareMessengerGenericTemplateElement alloc] init];
//    element.title = self.shareDict[@"content"]?:@"welcome to Famy";
//        element.subtitle = self.shareDict[@"content"]?:@"welcome to Famy";
//    element.imageURL = [NSURL URLWithString:self.shareDict[@"thumbnail"]?:@""];
//    element.button = urlButton;
//
//    FBSDKShareMessengerGenericTemplateContent *content = [[FBSDKShareMessengerGenericTemplateContent alloc] init];
//    content.pageID = FacebookKey;// Your page ID, required for attribution
//    content.element = element;
//
//    FBSDKMessageDialog *messageDialog = [[FBSDKMessageDialog alloc] init];
//    messageDialog.shareContent = content;
//
//    if ([messageDialog canShow]) {
//        [messageDialog show];
//    }
    
    //[MobClick event:@"clickShare_Messenger"];
}

- (void)shareToPlatform:(SPDUMShareType )platform {
    if ([self.delegate respondsToSelector:@selector(infoShareView:shareToPlatformType:)]) {
        [self.delegate infoShareView:self shareToPlatformType:platform];
    }
}

- (void)layoutSubviews {
    self.selfView.frame = self.bounds;
}

- (void)setInvite_count:(int)invite_count {
    _invite_count = invite_count;
    
    NSMutableAttributedString *mutableInvite_codeStr=[NSMutableAttributedString new];
    NSDictionary *fristDict=@{
                              NSFontAttributeName:[UIFont systemFontOfSize:15],
                              NSForegroundColorAttributeName:[UIColor colorWithHexString:@"666666"]
                              };
    NSAttributedString *firstStr=[[NSAttributedString alloc]initWithString:SPDStringWithKey(@"已邀请：", nil) attributes:fristDict];
    [mutableInvite_codeStr appendAttributedString:firstStr];
    
    NSDictionary *secondDict=@{
                               NSFontAttributeName:[UIFont systemFontOfSize:15],
                               NSForegroundColorAttributeName:[UIColor colorWithHexString:@"fd1339"]
                               };
    NSAttributedString *secondStr=[[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"%d",invite_count] attributes:secondDict];
    [mutableInvite_codeStr appendAttributedString:secondStr];
    
    NSDictionary *thirdDict=@{
                               NSFontAttributeName:[UIFont systemFontOfSize:15],
                               NSForegroundColorAttributeName:[UIColor colorWithHexString:@"666666"]
                               };
    NSAttributedString *thirdStr=[[NSAttributedString alloc]initWithString:SPDStringWithKey(@"人", nil) attributes:thirdDict];
    [mutableInvite_codeStr appendAttributedString:thirdStr];
    
    self.inviteCountLabel.attributedText = mutableInvite_codeStr;
}

@end
