//
//  VipTitleCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/7/30.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VipTitleCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
