//
//  HomeClanNoClanCell.h
//  SimpleDate
//
//  Created by 李楠 on 17/8/1.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HomeClanNoClanCellDelegate <NSObject>

@optional

- (void)didClickJoinInClan;
- (void)didClickCreateClan;

@end

@interface HomeClanNoClanCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *NoClanTipLabel;
@property (weak, nonatomic) IBOutlet UILabel *tipsLabel;
@property (nonatomic, weak) id<HomeClanNoClanCellDelegate> delegate;

@end
