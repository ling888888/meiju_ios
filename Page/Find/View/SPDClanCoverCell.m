//
//  SPDClanCoverCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/3/22.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDClanCoverCell.h"

@implementation SPDClanCoverCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.tipsLabel.text = SPDStringWithKey(@"(请上传健康且不包含国家领导人和国旗的图片)", nil);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)selectCover:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(selectCover)]) {
        [self.delegate selectCover];
    }
}

@end
