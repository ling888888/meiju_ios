//
//  SPDClanSaveCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/3/27.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPDClanSaveCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *actionBtn;
@property (weak, nonatomic) IBOutlet UILabel *instructionLabel;

@end
