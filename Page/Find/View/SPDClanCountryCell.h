//
//  SPDClanCountryCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/3/28.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SPDClanCountryCellDelegate <NSObject>

@optional

- (void)selectCountry;

@end

@interface SPDClanCountryCell : UITableViewCell

@property (nonatomic, strong) NSString *country;
@property (nonatomic, weak) id<SPDClanCountryCellDelegate> delegate;

@end
