//
//  VipPrivilegeView.m
//  SimpleDate
//
//  Created by 李楠 on 2018/7/30.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "VipPrivilegeView.h"

@implementation VipPrivilegeView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        UIView *view = [[[NSBundle mainBundle] loadNibNamed:@"VipPrivilegeView" owner:self options:nil] firstObject];
        view.frame = self.bounds;
        [self addSubview:view];
    }
    return self;
}

- (void)setType:(NSString *)type {
    _type = type;
    
    self.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"vip_privilege_%@", type]];
    if ([_type isEqualToString:@"video"]) {
        self.titleLabel.text = SPDStringWithKey(@"免费畅聊", nil);
        self.descLabel.text = SPDStringWithKey(@"可无限视频/语音/私信", nil);
    } else if ([_type isEqualToString:@"call"]) {
        self.titleLabel.text = SPDStringWithKey(@"家族召唤", nil);
        self.descLabel.text = SPDStringWithKey(@"免费召唤好友到家族玩", nil);
    } else if ([_type isEqualToString:@"gold"]) {
        self.titleLabel.text = SPDStringWithKey(@"金币福利", nil);
        self.descLabel.text = @"";
    } else if ([_type isEqualToString:@"vehicle"]) {
        self.titleLabel.text = SPDStringWithKey(@"专属座驾", nil);
        self.descLabel.text = SPDStringWithKey(@"可装备会员专属座驾", nil);
    }
}

@end
