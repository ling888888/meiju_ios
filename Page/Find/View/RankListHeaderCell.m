//
//  RankListHeaderCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/12/12.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "RankListHeaderCell.h"
#import "RankMedalsView.h"
#import "RankModel.h"

@interface RankListHeaderCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView1;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView2;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView3;

@property(nonatomic, strong) LY_PortraitView *portraitView1;
@property(nonatomic, strong) LY_PortraitView *portraitView2;
@property(nonatomic, strong) LY_PortraitView *portraitView3;

@property (weak, nonatomic) IBOutlet UIImageView *crownImageView1;
@property (weak, nonatomic) IBOutlet UIImageView *crownImageView2;
@property (weak, nonatomic) IBOutlet UIImageView *crownImageView3;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel1;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel2;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel3;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLabel1CenterX;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLabel2CenterX;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLabel3CenterX;
@property (weak, nonatomic) IBOutlet UIImageView *genderImageView1;
@property (weak, nonatomic) IBOutlet UIImageView *genderImageView2;
@property (weak, nonatomic) IBOutlet UIImageView *genderImageView3;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *genderImageView1Top;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *genderImageView2Top;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *genderImageView3Top;
@property (weak, nonatomic) IBOutlet RankMedalsView *medalsView1;
@property (weak, nonatomic) IBOutlet RankMedalsView *medalsView2;
@property (weak, nonatomic) IBOutlet RankMedalsView *medalsView3;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *medalsView1Width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *medalsView2Width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *medalsView3Width;
@property (weak, nonatomic) IBOutlet UIImageView *tagImageView1;
@property (weak, nonatomic) IBOutlet UIImageView *tagImageView2;
@property (weak, nonatomic) IBOutlet UIImageView *tagImageView3;
@property (weak, nonatomic) IBOutlet UIView *charmView1;
@property (weak, nonatomic) IBOutlet UIView *charmView2;
@property (weak, nonatomic) IBOutlet UIView *charmView3;
@property (weak, nonatomic) IBOutlet UIImageView *charmImageView1;
@property (weak, nonatomic) IBOutlet UIImageView *charmImageView2;
@property (weak, nonatomic) IBOutlet UIImageView *charmImageView3;
@property (weak, nonatomic) IBOutlet UILabel *charmLabel1;
@property (weak, nonatomic) IBOutlet UILabel *charmLabel2;
@property (weak, nonatomic) IBOutlet UILabel *charmLabel3;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *charmLabel1CenterX;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *charmLabel2CenterX;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *charmLabel3CenterX;
@property (weak, nonatomic) IBOutlet UIButton *levelBtn1;
@property (weak, nonatomic) IBOutlet UIButton *levelBtn2;
@property (weak, nonatomic) IBOutlet UIButton *levelBtn3;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel1;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel2;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel3;

@property (nonatomic, strong) NSArray<LY_PortraitView *> *avatarImageViewArr;
@property (nonatomic, strong) NSArray<UIImageView *> *crownImageViewArr;
@property (nonatomic, strong) NSArray<UILabel *> *nameLabelArr;
@property (nonatomic, strong) NSArray<NSLayoutConstraint *> *nameLabelCenterXArr;
@property (nonatomic, strong) NSArray<UIImageView *> *genderImageViewArr;
@property (nonatomic, strong) NSArray<NSLayoutConstraint *> *genderImageViewTopArr;
@property (nonatomic, strong) NSArray<RankMedalsView *> *medalsViewArr;
@property (nonatomic, strong) NSArray<NSLayoutConstraint *> *medalsViewWidthArr;
@property (nonatomic, strong) NSArray<UIImageView *> *tagImageViewArr;
@property (nonatomic, strong) NSArray<UIView *> *charmViewArr;
@property (nonatomic, strong) NSArray<UIImageView *> *charmImageViewArr;
@property (nonatomic, strong) NSArray<UILabel *> *charmLabelArr;
@property (nonatomic, strong) NSArray<UIButton *> *levelBtnArr;
@property (nonatomic, strong) NSArray<UILabel *> *levelLabelArr;

@end

@implementation RankListHeaderCell

- (LY_PortraitView *)portraitView1 {
    if (!_portraitView1) {
        _portraitView1 = [[LY_PortraitView alloc] init];
        _portraitView1.userInteractionEnabled = YES;
    }
    return _portraitView1;
}

- (LY_PortraitView *)portraitView2 {
    if (!_portraitView2) {
        _portraitView2 = [[LY_PortraitView alloc] init];
        _portraitView2.userInteractionEnabled = YES;
    }
    return _portraitView2;
}

- (LY_PortraitView *)portraitView3 {
    if (!_portraitView3) {
        _portraitView3 = [[LY_PortraitView alloc] init];
        _portraitView3.userInteractionEnabled = YES;
    }
    return _portraitView3;
}


- (void)awakeFromNib {
    [super awakeFromNib];
        
    if ([SPDCommonTool currentVersionIsArbic]) {
        self.nameLabel1CenterX.constant = 8.5;
        self.nameLabel2CenterX.constant = 8.5;
        self.nameLabel3CenterX.constant = 8.5;
        self.charmLabel1CenterX.constant = -7.75;
        self.charmLabel2CenterX.constant = -7.75;
        self.charmLabel3CenterX.constant = -7.75;
    }
    
    [self.avatarImageView1 addSubview:self.portraitView1];
    [self.avatarImageView2 addSubview:self.portraitView2];
    [self.avatarImageView3 addSubview:self.portraitView3];
    
    [self.portraitView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.avatarImageView1);
    }];
    [self.portraitView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.avatarImageView2);
    }];
    [self.portraitView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.avatarImageView3);
    }];
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAvatarImageView:)];
    [self.avatarImageView1 addGestureRecognizer:tap1];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAvatarImageView:)];
    [self.avatarImageView2 addGestureRecognizer:tap2];
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAvatarImageView:)];
    [self.avatarImageView3 addGestureRecognizer:tap3];
}

#pragma mark - Event responses

- (void)tapAvatarImageView:(UITapGestureRecognizer *)sender {
    if ([self.delegate respondsToSelector:@selector(rankListHeaderCell:didTapAvatarWithModel:)]) {
        [self.delegate rankListHeaderCell:self didTapAvatarWithModel:self.dataArr[sender.view.tag - 2020]];
    }
}

#pragma mark - Setters

- (void)setType:(NSString *)type {
    _type = type;
    
}

- (void)setDataArr:(NSArray *)dataArr {
    _dataArr = dataArr;
    
    for (NSInteger i = 0; i < 3; i++) {
        self.genderImageViewArr[i].hidden = YES;
        self.tagImageViewArr[i].hidden = YES;
        self.crownImageViewArr[i].hidden = YES;
        self.charmViewArr[i].hidden = YES;
        self.charmImageViewArr[i].hidden = YES;
        self.charmLabelArr[i].hidden = YES;
        self.levelBtnArr[i].hidden = YES;
        self.levelLabelArr[i].hidden = YES;
    }
    
    for (NSInteger i = 0; i < 3; i++) {
        BOOL notEmpty = (i < _dataArr.count);
        RankModel *model = notEmpty ? _dataArr[i] : [RankModel initWithDictionary:@{@"avatar": @"404.jpg", @"name": @"--", @"score": @"--", @"level": @"--"}];

        LY_PortraitView *portraitView = self.avatarImageViewArr[i];
        portraitView.portrait = model.portrait;
        UIImageView *avatarImageView = portraitView.superview;
        avatarImageView.userInteractionEnabled = notEmpty;
//        [avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, model.avatar]]];
        
        self.nameLabelArr[i].text = model.name;
        
        UIImageView *genderImageView = self.genderImageViewArr[i];
        if (notEmpty) {
            self.nameLabelCenterXArr[i].constant = [SPDCommonTool currentVersionIsArbic] ? 8.5 : -8.5;
            genderImageView.hidden = NO;
            genderImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"rank_gender_%@", model.gender]];
        } else {
            self.nameLabelCenterXArr[i].constant = 0;
            genderImageView.hidden = YES;
        }
        
        CGFloat medalsViewWidth = [self.medalsViewArr[i] widthWithMedals:model.medals vip:model.isVip specialNum:model.specialNum];
        if (medalsViewWidth > 0) {
            self.medalsViewWidthArr[i].constant = medalsViewWidth;
        }
        
        if ([self.type isEqualToString:@"riche"]) {
            self.genderImageViewTopArr[i].constant = i == 0 ? 14.5 : 13.5;
            UIImageView *tagImageView = self.tagImageViewArr[i];
            tagImageView.hidden = NO;
            tagImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"rank_tag_riche%ld", i + 1]];
        } else if ([self.type isEqualToString:@"charm"]) {
            self.genderImageViewTopArr[i].constant = i == 0 ? 39.5 : 36.5;
            self.charmViewArr[i].hidden = NO;
            self.charmImageViewArr[i].hidden = NO;
            UIImageView *tagImageView = self.tagImageViewArr[i];
            tagImageView.hidden = NO;
            tagImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"rank_tag_charm%ld", i + 1]];
            UILabel *charmLabel = self.charmLabelArr[i];
            charmLabel.hidden = NO;
            charmLabel.text = model.score;
        } else if ([self.type isEqualToString:@"person"]) {
            self.crownImageViewArr[i].hidden = NO;
            UIButton *levelBtn = self.levelBtnArr[i];
            levelBtn.hidden = NO;
            if (notEmpty) {
                [SPDCommonTool configDataForLevelBtnWithLevel:model.level.intValue andUIButton:levelBtn];
            } else {
                [levelBtn setBackgroundImage:nil forState:UIControlStateNormal];
                [levelBtn setTitle:@"--" forState:UIControlStateNormal];
                [levelBtn setTitleEdgeInsets:UIEdgeInsetsZero];
            }
        } else if ([self.type isEqualToString:@"clan"]) {
            self.crownImageViewArr[i].hidden = NO;
//            avatarImageView.layer.cornerRadius = 12;
            avatarImageView.userInteractionEnabled = NO;
            self.nameLabelCenterXArr[i].constant = 0;
            genderImageView.hidden = YES;
            UIButton *levelBtn = self.levelBtnArr[i];
            levelBtn.hidden = NO;
            if (notEmpty) {
                [levelBtn setBackgroundImage:[UIImage imageNamed:@"rank_level_clan"] forState:UIControlStateNormal];
                [levelBtn setTitle:[NSString stringWithFormat:@"LV.%@", model.level] forState:UIControlStateNormal];
            } else {
                [levelBtn setBackgroundImage:nil forState:UIControlStateNormal];
                [levelBtn setTitle:@"--" forState:UIControlStateNormal];
            }
        } else if ([self.type isEqualToString:@"magic"]) {
            self.genderImageViewTopArr[i].constant = i == 0 ? 39.5 : 36.5;
            self.crownImageViewArr[i].hidden = NO;
            UILabel *levelLabel = self.levelLabelArr[i];
            levelLabel.hidden = NO;
            if (notEmpty) {
                levelLabel.text = [NSString stringWithFormat:@"LV.%@", model.level];
            } else {
                levelLabel.text = @"--";
            }
        }
    }
}

#pragma mark - Getters

- (NSArray *)avatarImageViewArr {
    if (!_avatarImageViewArr) {
        _avatarImageViewArr = @[self.portraitView1, self.portraitView2, self.portraitView3];
    }
    return _avatarImageViewArr;
}

- (NSArray *)nameLabelArr {
    if (!_nameLabelArr) {
        _nameLabelArr = @[self.nameLabel1, self.nameLabel2, self.nameLabel3];
    }
    return _nameLabelArr;
}

- (NSArray *)nameLabelCenterXArr {
    if (!_nameLabelCenterXArr) {
        _nameLabelCenterXArr = @[self.nameLabel1CenterX, self.nameLabel2CenterX, self.nameLabel3CenterX];
    }
    return _nameLabelCenterXArr;
}

- (NSArray *)genderImageViewArr {
    if (!_genderImageViewArr) {
        _genderImageViewArr = @[self.genderImageView1, self.genderImageView2, self.genderImageView3];
    }
    return _genderImageViewArr;
}

- (NSArray *)genderImageViewTopArr {
    if (!_genderImageViewTopArr) {
        _genderImageViewTopArr = @[self.genderImageView1Top, self.genderImageView2Top, self.genderImageView3Top];
    }
    return _genderImageViewTopArr;
}

- (NSArray *)medalsViewArr {
    if (!_medalsViewArr) {
        _medalsViewArr = @[self.medalsView1, self.medalsView2, self.medalsView3];
    }
    return _medalsViewArr;
}

- (NSArray *)medalsViewWidthArr {
    if (!_medalsViewWidthArr) {
        _medalsViewWidthArr = @[self.medalsView1Width, self.medalsView2Width, self.medalsView3Width];
    }
    return _medalsViewWidthArr;
}

- (NSArray *)tagImageViewArr {
    if (!_tagImageViewArr) {
        _tagImageViewArr = @[self.tagImageView1, self.tagImageView2, self.tagImageView3];
    }
    return _tagImageViewArr;
}

- (NSArray *)crownImageViewArr {
    if (!_crownImageViewArr) {
        _crownImageViewArr = @[self.crownImageView1, self.crownImageView2, self.crownImageView3];
    }
    return _crownImageViewArr;
}

- (NSArray *)charmViewArr {
    if (!_charmViewArr) {
        _charmViewArr = @[self.charmView1, self.charmView2, self.charmView3];
    }
    return _charmViewArr;
}

- (NSArray *)charmImageViewArr {
    if (!_charmImageViewArr) {
        _charmImageViewArr = @[self.charmImageView1, self.charmImageView2, self.charmImageView3];
    }
    return _charmImageViewArr;
}

- (NSArray *)charmLabelArr {
    if (!_charmLabelArr) {
        _charmLabelArr = @[self.charmLabel1, self.charmLabel2, self.charmLabel3];
    }
    return _charmLabelArr;
}

- (NSArray *)levelBtnArr {
    if (!_levelBtnArr) {
        _levelBtnArr = @[self.levelBtn1, self.levelBtn2, self.levelBtn3];
    }
    return _levelBtnArr;
}

- (NSArray *)levelLabelArr {
    if (!_levelLabelArr) {
        _levelLabelArr = @[self.levelLabel1, self.levelLabel2, self.levelLabel3];
    }
    return _levelLabelArr;
}

@end
