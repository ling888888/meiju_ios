//
//  HomeUserListCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/7/5.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "HomeUserListCell.h"
#import "HomeModel.h"

@interface HomeUserListCell ()

@property(nonatomic, strong) LY_PortraitView *portraitView;

@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nickNameLabelTrailing;
@property (weak, nonatomic) IBOutlet UIButton *levelBtn;
@property (weak, nonatomic) IBOutlet UIImageView *genderImageView;

@property(nonatomic, strong) UIButton *livingBtn;

@end

@implementation HomeUserListCell

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
    }
    return _portraitView;
}

- (UIButton *)livingBtn {
    if (!_livingBtn) {
        _livingBtn = [[UIButton alloc] init];
        [_livingBtn setTitle:@"直播中".localized forState:UIControlStateNormal];
        [_livingBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _livingBtn.titleLabel.font = [UIFont mediumFontOfSize:10];
//        _livingBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 5);
        _livingBtn.layer.cornerRadius = 7.5;
        _livingBtn.layer.masksToBounds = true;
    }
    return _livingBtn;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.contentView addSubview:self.portraitView];
    [self.contentView addSubview:self.livingBtn];
    
    
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(70);
        make.leading.mas_equalTo(5);
        make.centerY.mas_equalTo(0);
    }];
    [self.livingBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.portraitView.imageView.mas_bottom);
        make.centerX.mas_equalTo(self.portraitView);
        make.height.mas_equalTo(15);
        make.width.mas_equalTo(38);
    }];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    UIImage * bgImg = [[UIImage alloc] initWithGradient:@[[UIColor colorWithHexString:@"#22BBB9"], [UIColor colorWithHexString:@"#45E994"]] size:self.livingBtn.frame.size direction:UIImageGradientColorsDirectionVertical];
    [self.livingBtn setBackgroundImage:bgImg forState:UIControlStateNormal];
}

- (void)setModel:(HomeModel *)model {
    _model = model;
    
    self.portraitView.portrait = model.portrait;
    
    self.nickNameLabel.text = _model.nick_name;
    
    self.genderImageView.image = [UIImage imageNamed:[_model.gender isEqualToString:@"male"] ? @"ic_zhibojian_yonghuziliao_bianqian_nan":@"ic_zhibojian_yonghuziliao_biaoqian_nv"];
    
    [SPDCommonTool configDataForLevelBtnWithLevel:[_model.level intValue] andUIButton:self.levelBtn];
    
    
    if ([_model.status isEqualToNumber:@1]) {
        // 开播
        [self.livingBtn setHidden:false];
    } else {
        // 未开播
        [self.livingBtn setHidden:true];
    }
}

- (void)setHideLevel:(BOOL)hideLevel {
    if (_hideLevel != hideLevel) {
        _hideLevel = hideLevel;
        self.levelBtn.hidden = YES;
    }
}


@end
