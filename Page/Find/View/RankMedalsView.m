//
//  RankMedalsView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/2/6.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RankMedalsView.h"

@interface RankMedalsView ()

@property (nonatomic, strong) UIImageView *medalImageView1;
@property (nonatomic, strong) UIImageView *vipImageView;
@property (nonatomic, strong) UIImageView *specialNumImageView;
@property (nonatomic, strong) UIImageView *medalImageView2;

@end

@implementation RankMedalsView

- (CGFloat)widthWithMedals:(NSArray *)medals vip:(BOOL)vip specialNum:(NSString *)specialNum {
    for (UIView *view in self.subviews) {
        view.hidden = YES;
    }
    
    CGFloat medalWidth = 16;
    CGFloat space = 5;
    CGFloat totalWidth = 0;
    CGFloat x = 0;
    if (medals.count > 0) {
        self.medalImageView1.hidden = NO;
        [self.medalImageView1 sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, medals.firstObject]]];
        self.medalImageView1.frame = CGRectMake(x, 0, medalWidth, medalWidth);
        totalWidth = x + medalWidth;
        x += (medalWidth + space);
    }
    if (vip) {
        self.vipImageView.hidden = NO;
        self.vipImageView.frame = CGRectMake(x, 0, medalWidth, medalWidth);
        totalWidth = x + medalWidth;
        x += (medalWidth + space);
    }
    if (specialNum) {
        self.specialNumImageView.hidden = NO;
        self.specialNumImageView.frame = CGRectMake(x, 0, medalWidth, medalWidth);
        totalWidth = x + medalWidth;
        x += (medalWidth + space);
    }
    if (medals.count > 1) {
        self.medalImageView2.hidden = NO;
        [self.medalImageView2 sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, medals[1]]]];
        self.medalImageView2.frame = CGRectMake(x, 0, medalWidth, medalWidth);
        totalWidth = x + medalWidth;
        x += (medalWidth + space);
    }
    
    self.hidden = totalWidth == 0;
    return totalWidth;
}

#pragma mark - Getters

- (UIImageView *)medalImageView1 {
    if (!_medalImageView1) {
        _medalImageView1 = [UIImageView new];
        _medalImageView1.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_medalImageView1];
    }
    return _medalImageView1;
}

- (UIImageView *)vipImageView {
    if (!_vipImageView) {
        _vipImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"vipIcon"]];
        _vipImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_vipImageView];
    }
    return _vipImageView;
}

- (UIImageView *)specialNumImageView {
    if (!_specialNumImageView) {
        _specialNumImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"specialNum_icon_13"]];
        _specialNumImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_specialNumImageView];
    }
    return _specialNumImageView;
}

- (UIImageView *)medalImageView2 {
    if (!_medalImageView2) {
        _medalImageView2 = [UIImageView new];
        _medalImageView2.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_medalImageView2];
    }
    return _medalImageView2;
}

@end
