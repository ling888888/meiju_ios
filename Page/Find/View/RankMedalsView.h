//
//  RankMedalsView.h
//  SimpleDate
//
//  Created by 李楠 on 2020/2/6.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RankMedalsView : UIView

- (CGFloat)widthWithMedals:(NSArray *)medals vip:(BOOL)vip specialNum:(NSString *)specialNum;

@end

NS_ASSUME_NONNULL_END
