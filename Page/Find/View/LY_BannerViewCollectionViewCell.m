//
//  LY_BannerViewCollectionViewCell.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/7.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_BannerViewCollectionViewCell.h"

@implementation LY_BannerViewCollectionViewCell

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = [[UIImageView alloc] init];
        _imgView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _imgView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self addSubview:self.imgView];
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(self);
        make.top.bottom.mas_equalTo(self);
        make.leading.mas_equalTo(15);
        make.trailing.mas_equalTo(-15);
    }];
}
@end
