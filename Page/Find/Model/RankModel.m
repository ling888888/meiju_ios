//
//  RankModel.m
//  SimpleDate
//
//  Created by 李楠 on 2018/12/12.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "RankModel.h"

@implementation RankModel

- (void)setValue:(id)value forKey:(NSString *)key {
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"avatar"]) {
        self.portrait.url = value;
    } else if ([key isEqualToString:@"headwearWebp"]) {
        self.portrait.headwearUrl = value;
    }
}

- (LY_Portrait *)portrait {
    if (!_portrait) {
        _portrait = [[LY_Portrait alloc] init];
    }
    return _portrait;
}

@end
