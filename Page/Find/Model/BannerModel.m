//
//  BannerModel.m
//  SimpleDate
//
//  Created by 侯玲 on 16/8/2.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "BannerModel.h"

@implementation BannerModel

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqualToString:@"id"]) {
        self.banner_ID=value;
    }
}

-(id)copyWithZone:(NSZone *)zone{
    BannerModel *copy=[[BannerModel allocWithZone:zone] init];
    copy.action_url=self.action_url;
    copy.begin_time=self.begin_time;
    copy.content=self.content;
    copy.banner_ID=self.banner_ID;
    copy.image_url=self.image_url;
    copy.share_url=self.share_url;
    copy.thumbnail=self.thumbnail;
    copy.end_time=self.end_time;
    copy.title=self.title;
    return copy;
}
@end
