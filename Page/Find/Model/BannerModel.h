//
//  BannerModel.h
//  SimpleDate
//
//  Created by 侯玲 on 16/8/2.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "CommonModel.h"

@interface BannerModel : CommonModel<NSCopying>

@property(nonatomic,copy)NSString *action_url;
@property(nonatomic,copy)NSString *begin_time;
@property(nonatomic,copy)NSString *end_time;
@property(nonatomic,copy)NSString *content;
@property(nonatomic,copy)NSString *banner_ID;
@property(nonatomic,copy)NSString *image_url;
@property(nonatomic,copy)NSString *share_url;
@property(nonatomic,copy)NSString *thumbnail;
@property(nonatomic,copy)NSString *title;

//本地做第一屏幕缓存
@property(nonatomic,copy)NSString *local_banner_image;


@end
