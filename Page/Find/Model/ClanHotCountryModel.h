//
//  ClanHotCountryModel.h
//  SimpleDate
//
//  Created by 李楠 on 2018/3/29.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "CommonModel.h"

@interface ClanHotCountryModel : CommonModel

@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *country_name;
@property (nonatomic, copy) NSString *flag;
@property (nonatomic, strong) NSMutableArray *list;

@end
