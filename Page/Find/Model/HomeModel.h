//
//  HomeModel.h
//  SimpleDate
//
//  Created by 程龙军 on 16/1/3.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "CommonModel.h"
#import "LY_HPTagView.h"


@interface HomeModel : CommonModel

@property(nonatomic, copy) NSString *_id;
@property(nonatomic, copy) NSString *nick_name;
@property(nonatomic, copy) NSString *cover_img;
@property(nonatomic, copy) NSString *avatar;
@property(nonatomic, copy) NSString *noble; // 贵族爵位
@property(nonatomic, copy) LY_Portrait *portrait;

@property(nonatomic, copy) NSString *city;
@property(nonatomic, copy) NSString *personal_desc;
@property(nonatomic, copy) NSString *gender;

@property(nonatomic, copy) NSNumber *level;
@property(nonatomic, copy) NSNumber *age;
@property(nonatomic, copy) NSNumber *last_login_time;
@property(nonatomic, copy) NSNumber *distance;
@property(nonatomic, assign) BOOL is_favorite;
@property(nonatomic, assign) BOOL is_verified_video;
@property(nonatomic, copy) NSNumber *connect_rate;
@property(nonatomic, assign) BOOL is_anchor;
@property(nonatomic, assign) BOOL is_vip;
@property(nonatomic, assign) BOOL is_service;
@property(nonatomic, copy) NSDictionary * charm; // 魅力值
@property(nonatomic, copy) NSDictionary * riche; // 魅力值
@property(nonatomic, copy) NSString *invite_code;

@property(nonatomic, strong) LY_CustomTag *customTag;
@property(nonatomic, strong) LY_FansTag *fansTag;
//本地封面
@property(nonatomic, copy) NSString *local_avatar;
@property(nonatomic, copy) NSString *local_image;
@property(nonatomic, assign) BOOL is_agent_gm; // 是否是代理客服
@property(nonatomic, assign) NSNumber* is_agent_charge;//是否可以代理充值

//好友要用的
@property(nonatomic, copy) NSString *mobile;
@property(nonatomic, copy) NSData *avatarImageData ;//通讯录头像
@property(nonatomic, assign) BOOL invited;
@property(nonatomic, copy) NSString *type;//推荐类型 ["common", "clan", "nearby"]
@property(nonatomic, copy) NSNumber *common_friend_num;//共同好友的数量
@property(nonatomic, copy) NSString *user_id;
@property(nonatomic, assign) BOOL is_friend; //与我是不是好友
@property(nonatomic, assign) BOOL online;
@property(nonatomic, assign) BOOL video_switch; // 用户视频开关
@property(nonatomic, assign) BOOL audio_switch; // 用户语音开关
@property(nonatomic, copy) NSString *specialNum; // 靓号
@property(nonatomic, copy) NSString *clan_id;
@property(nonatomic, copy) NSString *lang;
@property(nonatomic, copy) NSString *clan_name;
@property(nonatomic, copy) NSDictionary * packages; // 我的背包
@property(nonatomic, copy) NSString * headwear;
@property(nonatomic, copy) NSString * medal;
@property(nonatomic, copy) NSString * homepage_falling;
@property (nonatomic, copy) NSString * liveId;
@property(nonatomic, copy) NSNumber *status;
@property (nonatomic, copy) NSString * friendType;


@end
