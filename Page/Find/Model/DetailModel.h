//
//  DetailModel.h
//  SimpleDate
//
//  Created by 程龙军 on 15/12/9.
//  Copyright © 2015年 WYB. All rights reserved.
//

#import "CommonModel.h"

@interface DetailModel : CommonModel
@property (nonatomic, copy) NSString *_id;
@property (nonatomic, copy) NSString *nick_name;
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSArray *album;
@property (nonatomic, copy) NSString *cover_img;
@property (nonatomic, copy) NSString *personal_desc;
@property (nonatomic, copy) NSNumber *age;
@property (nonatomic, copy) NSNumber *height;
@property (nonatomic, copy) NSNumber *weight;
@property (nonatomic, copy) NSString *constellation;
@property (nonatomic, copy) NSString *hobby;
@property (nonatomic, copy) NSString *job;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *request;
@property (nonatomic, copy) NSString *date_way;
@property (nonatomic, copy) NSString *expertise;
@property (nonatomic, copy) NSString *chat;

@property (nonatomic, copy) NSNumber *distance;
@property (nonatomic, strong) UIImage *image;

@property (nonatomic) BOOL is_favorite;
@property (nonatomic) BOOL is_online;
@property (nonatomic) BOOL is_unlock;
@property (nonatomic) BOOL is_verified_video;
@property (nonatomic) BOOL is_verified_zhima;

@property (nonatomic) BOOL is_audio;
@property (nonatomic) BOOL is_video;
@property (nonatomic) NSNumber *connect_rate;






@end
