//
//  RankModel.h
//  SimpleDate
//
//  Created by 李楠 on 2018/12/12.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "CommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RankModel : CommonModel

@property (nonatomic, copy) NSNumber *rank;
@property (nonatomic, copy) NSString *_id;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, strong) LY_Portrait *portrait;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSArray *medals;
@property (nonatomic, assign) BOOL isVip;
@property (nonatomic, copy) NSString *specialNum;
@property (nonatomic, copy) NSString *score;
@property (nonatomic, copy) NSNumber *level;
@property (nonatomic, copy) NSNumber *exp;
@property (nonatomic, copy) NSNumber *distance;

@end

NS_ASSUME_NONNULL_END
