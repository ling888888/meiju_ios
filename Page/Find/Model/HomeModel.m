//
//  HomeModel.m
//  SimpleDate
//
//  Created by 程龙军 on 16/1/3.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "HomeModel.h"
#import "SPDCommonDefine.h"
@implementation HomeModel

- (void)setValue:(id)value forKey:(NSString *)key {
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"level"]) {
        if ([value isKindOfClass:[NSDictionary class]]) {
            self.level = value[@"user_level"];
        }
    }
    if ([key isEqualToString:@"avatar"]) {
        self.portrait.url = value;
    } else if ([key isEqualToString:@"headwearWebp"]) {
        self.portrait.headwearUrl = value;
    }
    if ([key isEqualToString:@"fansLevel"]) {
        self.fansTag.level = value;
    } else if ([key isEqualToString:@"fansNum"]) {
        self.fansTag.name = value;
    }
}

- (void)setUser_id:(NSString *)user_id {
    _user_id = user_id;
    
    self._id = user_id;
}

- (void)setDiyLabel:(NSDictionary *)diyLabel {
    self.customTag = [LY_CustomTag yy_modelWithDictionary:diyLabel];
}

- (void)setPackages:(NSDictionary *)packages {
    _packages = packages;
    if ([_packages[Headwear] isKindOfClass:[NSString class]] && ![SPDCommonTool isEmpty:_packages[Headwear]]) {
        self.headwear = _packages[Headwear];
    }
    if ([_packages[Medal] isKindOfClass:[NSString class]] && ![SPDCommonTool isEmpty:_packages[Medal]]) {
        self.medal = _packages[Medal];
    }
    if ([_packages[Headwear] isKindOfClass:[NSDictionary class]] && ![SPDCommonTool isEmpty:_packages[Headwear][@"headwearWebp"]]) {
        self.portrait.headwearUrl = _packages[Headwear][@"headwearWebp"];
    }
    if ([_packages[@"headwearWebp"] isKindOfClass:[NSString class]] && ![SPDCommonTool isEmpty:_packages[@"headwearWebp"]]) {
        self.portrait.headwearUrl = _packages[@"headwearWebp"];
    }
    if ([_packages[Headwear] isKindOfClass:[NSDictionary class]] && ![SPDCommonTool isEmpty:_packages[Headwear][@"cover"]]) {
        self.headwear = _packages[Headwear][@"cover"];
    }
    if ([_packages[Medal] isKindOfClass:[NSDictionary class]] && ![SPDCommonTool isEmpty:_packages[Medal][@"cover"]]) {
        self.medal = _packages[Medal][@"cover"];
    }
}
    
    

- (LY_Portrait *)portrait{
    if (!_portrait) {
        _portrait = [[LY_Portrait alloc] init];
    }
    return _portrait;
}

- (LY_CustomTag *)customTag {
    if (!_customTag) {
        _customTag = [[LY_CustomTag alloc] init];
    }
    return _customTag;
}

- (LY_FansTag *)fansTag {
    if (!_fansTag) {
        _fansTag = [[LY_FansTag alloc] init];
    }
    return _fansTag;
}

@end
