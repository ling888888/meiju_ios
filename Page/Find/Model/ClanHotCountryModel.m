//
//  ClanHotCountryModel.m
//  SimpleDate
//
//  Created by 李楠 on 2018/3/29.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "ClanHotCountryModel.h"
#import "FamilyModel.h"

@implementation ClanHotCountryModel

- (void)setList:(NSMutableArray *)list {
    if (!_list) {
        _list = [NSMutableArray array];
    }
    for (NSDictionary *dic in list) {
        FamilyModel *model = [FamilyModel initWithDictionary:dic];
        [_list addObject:model];
    }
}

@end
