//
//  LY_ClanViewController.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/29.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_ClanViewController.h"
#import "SPDHotClanViewController.h"
#import "SPDClanNewestController.h"
#import "LY_ClanFollowViewController.h"
#import "SPDHotClanHeaderView.h"
#import "BannerModel.h"
//#import "BannerDetailController.h"

#import "ZegoKitManager.h"

//#define LYTableHeaderViewHeight (kScreenW-30)/345*135+70

@interface LY_ClanViewController () <SPDHotClanHeaderViewDelegate>

@property (nonatomic, strong) SPDHotClanHeaderView *headerView;

@property (nonatomic, strong) SPDHotClanViewController *clanHotVC;

@property (nonatomic, strong) SPDClanNewestController *clanNewestVC;

@property (nonatomic, strong) LY_ClanFollowViewController *clanFollowVC;

// 直播间最小化显示
@property (nonatomic, strong) UIImageView *backgroundClanView;
@property (nonatomic, strong) UIButton *enterBgClanBtn;

@end

@implementation LY_ClanViewController

- (SPDHotClanHeaderView *)headerView{
    if (!_headerView) {
        _headerView = [[SPDHotClanHeaderView alloc]init];
        _headerView.frame = CGRectMake(0, 0, kScreenW, 235);
        _headerView.delegate = self;
    }
    return _headerView;
}

- (UIImageView *)backgroundClanView {
    if (!_backgroundClanView) {
        _backgroundClanView = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenW - 13 - 82, kScreenH - TabBarHeight - 50 - 30 - 101, 82, 101)];
        _backgroundClanView.image = [UIImage imageNamed:@"img_background_run"];
        _backgroundClanView.hidden = YES;
        _backgroundClanView.userInteractionEnabled = YES;
        
        self.enterBgClanBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.enterBgClanBtn.imageView.contentMode = UIViewContentModeScaleAspectFill;
        self.enterBgClanBtn.frame = CGRectMake(6, 23, 69, 69);
        self.enterBgClanBtn.clipsToBounds = YES;
        self.enterBgClanBtn.layer.cornerRadius = 68 / 2;
        [self.enterBgClanBtn addTarget:self action:@selector(clickEnterBgClanBtn:) forControlEvents:UIControlEventTouchUpInside];
        [_backgroundClanView addSubview:self.enterBgClanBtn];
        
        UIButton *closeBgClanBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        closeBgClanBtn.frame = CGRectMake(54, 0, 28, 24);
        [closeBgClanBtn addTarget:self action:@selector(clickCloseBgClanBtn:) forControlEvents:UIControlEventTouchUpInside];
        [_backgroundClanView addSubview:closeBgClanBtn];
    }
    return _backgroundClanView;
}

- (SPDHotClanViewController *)clanHotVC {
    if (!_clanHotVC) {
        _clanHotVC = [[SPDHotClanViewController alloc] init];
    }
    return _clanHotVC;
}

- (SPDClanNewestController *)clanNewestVC {
    if (!_clanNewestVC) {
        _clanNewestVC = [[SPDClanNewestController alloc] init];
    }
    return _clanNewestVC;
}

- (LY_ClanFollowViewController *)clanFollowVC {
    if (!_clanFollowVC) {
        _clanFollowVC = [[LY_ClanFollowViewController alloc] init];
    }
    return _clanFollowVC;
}

- (NSMutableArray<NSString *> *)titleArray {
    return [NSMutableArray arrayWithArray:@[@"最新".localized, [NSString stringWithFormat:@" %@ ", @"热门".localized], @"关注".localized]];
}

- (NSMutableArray<JXPagerViewListViewDelegate> *)VCArray {
    return [NSMutableArray arrayWithArray:@[self.clanHotVC, self.clanNewestVC, self.clanFollowVC]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.categoryView addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:nil];
    [self setupUI];
    [self setupUIFrame];
    
    //FIXME:如果和JXPagingView联动
    self.categoryView.contentScrollView = self.pagerView.listContainerView.scrollView;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"frame"]) {
        CGRect frame = self.categoryView.frame;
        if (frame.size.width == kScreenW) {
            CGFloat categoryViewW = 0.0;
            for (NSString *title in self.titleArray) {
                CGFloat titleWidth = [title sizeWithFont:[UIFont mediumFontOfSize:18] andMaxSize:CGSizeMake(kScreenW, 44)].width;
                categoryViewW += titleWidth;
            }
            categoryViewW = categoryViewW + 15 + 2 * 10 + 3 * 10 + 15;
            frame.origin.x = kIsMirroredLayout ? kScreenW - categoryViewW : 0;
            frame.size.width = categoryViewW;
            self.categoryView.frame = frame;// baolong 让我这么写的 因为我们实在找不到为什么宽度会变
        }
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setupData];
    
    if (ZegoManager.sceneType == SceneTypeClanRoom && ZegoManager.isBackground) {
         NSURL *coverURL = [NSURL URLWithString:ZegoManager.clan_cover];
         [self.enterBgClanBtn sd_setImageWithURL:coverURL forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"img_portrait_placeholder"]];
         self.backgroundClanView.hidden = NO;
     } else {
         self.backgroundClanView.hidden = YES;
     }
}

- (void)setupUI {
    [self.navigationController.navigationBar setHidden:true];
    [self.view addSubview:self.backgroundClanView];
    ZegoManager.clanBackgroundView = self.backgroundClanView;
}

- (void)setupUIFrame {
    CGFloat categoryViewW = 0.0;
    for (NSString *title in self.titleArray) {
        CGFloat titleWidth = [title sizeWithFont:[UIFont mediumFontOfSize:18] andMaxSize:CGSizeMake(kScreenW, 44)].width;
        categoryViewW += titleWidth;
    }
    categoryViewW = categoryViewW + 15 + 2 * 10 + 3 * 10 + 15;
    if (kIsMirroredLayout) {
        self.categoryView.frame = CGRectMake(kScreenW - categoryViewW, 0, categoryViewW, 44);
    } else {
        self.categoryView.frame = CGRectMake(0, 0, categoryViewW, 44);
    }
}

- (void)__setupUIFrame {
    
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    self.pagerView.frame = self.view.bounds;
}

- (void)setupData {
    // 获取聊天室榜单
    [LY_Network getRequestWithURL:LY_Api.clan_rank parameters:nil success:^(id  _Nullable response) {
        self.headerView.rankDict = response;
    } failture:^(NSError * _Nullable error) {
//        error
    }];
    
    // 获取Banner
    NSDictionary *param = @{@"name": @"iOS"
//                            @"type":@"clan"
    };
    [LY_Network getRequestWithURL:LY_Api.clan_banner parameters:param success:^(id  _Nullable response) {
        NSArray *bannerArray = response[@"banner"];
        NSMutableArray *models = [NSMutableArray array];
        for (NSDictionary *dict in bannerArray) {
            BannerModel *model = [BannerModel initWithDictionary:dict];
            double beginTime = [model.begin_time doubleValue] / 1000;
            double endTime = [model.end_time doubleValue] / 1000;
            NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
            if (currentTime >= beginTime && currentTime <= endTime) {
                [models addObject:model];
            }
        }
        self.headerView.bannerList = models;
    } failture:^(NSError * _Nullable error) {
        
    }];
}

- (void)clickEnterBgClanBtn:(UIButton *)sender {
    [ZegoManager joinRoomFrom:self clanId:ZegoManager.clan_id];
}

- (void)clickCloseBgClanBtn:(UIButton *)sender {
    [ZegoManager leaveRoom];
    self.backgroundClanView.hidden = YES;
}

// MARK: - JXPagerViewListViewDelegate

- (UIView *)tableHeaderViewInPagerView:(JXPagerView *)pagerView {
    return self.headerView;
}
- (NSUInteger)tableHeaderViewHeightInPagerView:(JXPagerView *)pagerView {
    return (kScreenW-30)/345*135+70;
}

- (NSUInteger)heightForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    return 44;
}

//- (UIView *)viewForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
//    return self.categoryView;
//}
//
//- (NSInteger)numberOfListsInPagerView:(JXPagerView *)pagerView {
//    return self.categoryView.titles.count;
//}
//
//- (void)mainTableViewDidScroll:(UIScrollView *)scrollView {
////    [self.headerView scrollViewDidScroll:scrollView.contentOffset.y];
//}
//
//- (id<JXPagerViewListViewDelegate>)pagerView:(JXPagerView *)pagerView initListAtIndex:(NSInteger)index {
//    return self.VCArray[index];
//}

#pragma mark - JXCategoryViewDelegate

//- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
//    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
//}

#pragma mark - JXPagerMainTableViewGestureDelegate

//- (BOOL)mainTableViewGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
//    //禁止categoryView左右滑动的时候，上下和左右都可以滚动
//    return true;
////    if (otherGestureRecognizer == self.categoryView.collectionView.panGestureRecognizer) {
////        return NO;
////    }
////    return [gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]];
//}

#pragma mark - JXPagingViewListViewDelegate

- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return [[UIScrollView alloc] init];
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *scrollView))callback {
    
}

#pragma mark - SPDHotClanHeaderViewDelegate

- (void)clickedBannerCellWithIndex: (NSInteger)index {
    UIStoryboard *homeStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//      BannerDetailController *bannerDetailController = [homeStoryboard instantiateViewControllerWithIdentifier:@"BannerDetailController"];
//      bannerDetailController.bannerModel = self.bannerArray[index];
//      [self.navigationController pushViewController:bannerDetailController animated:YES];
}

@end
