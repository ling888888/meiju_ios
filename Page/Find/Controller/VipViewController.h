//
//  VipViewController.h
//  SimpleDate
//
//  Created by 侯玲 on 16/7/26.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VipViewController : BaseViewController

@property (nonatomic, assign) BOOL is_vip;

@end
