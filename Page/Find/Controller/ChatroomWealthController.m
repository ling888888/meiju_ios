//
//  ChatroomWealthController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/5/7.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "ChatroomWealthController.h"
#import "ChatroomWealthTitleCell.h"
#import "WealthCollectionViewCell.h"
#import "SPDVehicleModel.h"
#import "SPDMagicModel.h"
#import "presentReceivcCVCell.h"
#import "SPDMagicViewController.h"
#import "SPDVehicleViewController.h"

@interface ChatroomWealthController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView * collectionView;
@property (nonatomic, strong) NSArray * type;
@property (nonatomic, strong) NSMutableArray * magicArray;
@property (nonatomic, strong) NSMutableArray * vehicleArray;
@property(nonatomic, strong)NSMutableArray *sendGiftArray;
@property(nonatomic, strong)NSMutableArray *receiveGiftArray;
@property (nonatomic, strong) UIImageView * bgImageView;
@end

@implementation ChatroomWealthController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (@available(iOS 11.0, *)) {
       self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
       self.automaticallyAdjustsScrollViewInsets = false;
    }
    self.navigationItem.title = @"交际".localized;
    self.type = @[@"magic",@"",@"vehicle",@"",@"receive",@"",@"send",@""];
    
    self.bgImageView = [[UIImageView alloc]initWithFrame:self.view.bounds];
    self.bgImageView.image = [UIImage imageNamed:@"img_caifubang_bg"];
    [self.view addSubview:self.bgImageView];
    [self.view addSubview:self.collectionView];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:0.0];
    self.navigationController.navigationBar.titleTextAttributes = @{
        NSFontAttributeName:[UIFont mediumFontOfSize:18],
        NSForegroundColorAttributeName:[UIColor colorWithHexString:@"ffffff"]
    };
    [self requestData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.tintColor =SPD_HEXCOlOR(@"1a1a1a");
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{
        NSFontAttributeName:[UIFont mediumFontOfSize:18],
        NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#1A1A1A"]
    };
}

- (void)requestData {
    [RequestUtils commonGetRequestUtils:[@{@"user_id":self.ID} mutableCopy] bURL:@"user.gifts" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable dataDict) {
        [self.magicArray removeAllObjects];
        [self.vehicleArray removeAllObjects];
        [self.sendGiftArray removeAllObjects];
        [self.sendGiftArray removeAllObjects];
        for (NSDictionary *dic in dataDict[@"vehicleList"]) {
            SPDVehicleModel *model = [SPDVehicleModel initWithDictionary:dic];
            [self.vehicleArray addObject:model];
        }
        for (NSDictionary *dic in dataDict[@"user_magic"]) {
            SPDMagicModel *model = [SPDMagicModel initWithDictionary:dic];
            [self.magicArray addObject:model];
        }
        for (NSDictionary *dict in dataDict[@"gift_receive"]) {
            GiftListModel *model = [GiftListModel initWithDictionary:dict];
            [self.receiveGiftArray addObject:model];
        }
        if (self.receiveGiftArray.count == 0) {
            [self.receiveGiftArray addObject:[GiftListModel new]];
        }
        for (NSDictionary *dict in dataDict[@"gift_send"]) {
            GiftListModel *model = [GiftListModel initWithDictionary:dict];
            [self.sendGiftArray addObject:model];
        }
        if (self.sendGiftArray.count == 0) {
            [self.sendGiftArray addObject:[GiftListModel new]];
        }
        [self.collectionView reloadData];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 8;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    switch (section) {
        case 1:
            return self.magicArray.count;
            break;
        case 3:
            return self.vehicleArray.count;
            break;
        case 5:
            return self.receiveGiftArray.count;
            break;
        case 7:
            return self.sendGiftArray.count;
            break;
        default:
            return 1;
            break;
    }
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 || indexPath.section == 2 || indexPath.section == 4 || indexPath.section == 6) {
        ChatroomWealthTitleCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatroomWealthTitleCell" forIndexPath:indexPath];
        cell.userId = self.ID;
        cell.type = self.type[indexPath.section];
        return cell;
    }else if (indexPath.section == 1 || indexPath.section == 3){
        WealthCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WealthCollectionViewCell" forIndexPath:indexPath];
        if (indexPath.section == 1) {
            cell.magicModel = self.magicArray[indexPath.row];
        }else{
            cell.vehicleModel = self.vehicleArray[indexPath.row];
        }
        return cell;
    }else{
        presentReceivcCVCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"presentReceivcCVCell" forIndexPath:indexPath];
        cell.model = indexPath.section == 5 ? self.receiveGiftArray[indexPath.row] :self.sendGiftArray[indexPath.row];
        return cell;
    }
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 1:
            return CGSizeMake(floor((kScreenW - 15 * 2 - 9 * 3)/4.0f), 105);
            break;
        case 3:
            return CGSizeMake(floor((kScreenW - 15 * 2 - 9 * 3)/4.0f), 105);
            break;
        case 5:
            return CGSizeMake(floor((kScreenW - 15 * 2 - 9 * 3)/4.0f), floor((kScreenW - 15 * 2 - 9 * 3)/4.0f));
            break;
        case 7:
            return CGSizeMake(floor((kScreenW - 15 * 2 - 9 * 3)/4.0f), floor((kScreenW - 15 * 2 - 9 * 3)/4.0f));
            break;
        default:
            return CGSizeMake(kScreenW, 34);
            break;
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return section %2 != 0 ? UIEdgeInsetsMake(0, 15, 0, 15): UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return section %2 != 0 ? 9 : 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return section %2 != 0 ? 9 : 0;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        SPDMagicViewController *vc = [[SPDMagicViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section == 2){
        SPDVehicleViewController *vc = [[SPDVehicleViewController alloc] init];
        vc.user_id = self.ID;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout new];
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, naviBarHeight + 13, kScreenW, self.view.bounds.size.height - naviBarHeight - 13) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.backgroundColor = [UIColor clearColor];
        [_collectionView registerNib:[UINib nibWithNibName:@"ChatroomWealthTitleCell" bundle:nil] forCellWithReuseIdentifier:@"ChatroomWealthTitleCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"WealthCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"WealthCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"presentReceivcCVCell" bundle:nil] forCellWithReuseIdentifier:@"presentReceivcCVCell"];
    }
    return _collectionView;
}

- (NSMutableArray *)magicArray {
    if (!_magicArray) {
        _magicArray = [NSMutableArray new];
    }
    return _magicArray;
}

- (NSMutableArray *)vehicleArray {
    if (!_vehicleArray) {
        _vehicleArray = [NSMutableArray new];
    }
    return _vehicleArray;
}

-(NSMutableArray *)sendGiftArray{
    if (!_sendGiftArray) {
        _sendGiftArray=[NSMutableArray array];
    }
    return _sendGiftArray;
}

-(NSMutableArray *)receiveGiftArray{
    if (!_receiveGiftArray) {
        _receiveGiftArray=[NSMutableArray array];
    }
    return _receiveGiftArray;
}

@end
