//
//  VipViewController.m
//  SimpleDate
//
//  Created by 侯玲 on 16/7/26.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "VipViewController.h"
#import "VipMyInfoCell.h"
#import "VipTitleCell.h"
#import "VipCommonPrivilegeCell.h"
#import "VipPlusPrivilegeCell.h"
#import "SPDVehicleViewController.h"
#import "IAPManager.h"

@interface VipViewController ()<UITableViewDataSource, UITableViewDelegate, VipPlusPrivilegeCellDelegate, VipCommonPrivilegeCellDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, copy) NSNumber *award_num;
@property (nonatomic, assign) BOOL is_get;
@property (nonatomic, assign) BOOL is_outfit;
@property (nonatomic, assign) BOOL is_seven_vip;
@property (nonatomic, copy) NSString *seven_vip_valid_time;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *nick_name;
@property (nonatomic, copy) NSString *vip_valid_time;

@end

@implementation VipViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = SPDStringWithKey(@"会员", nil);
    [self.view addSubview:self.tableView];

    [kIAPManager checkUnfinishedPaymentsOfType:IAPTypeVIP];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self requestMyInfoDetailWithRefresh:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods
    
- (void)requestMyInfoDetailWithRefresh:(BOOL)refresh {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.info.detail" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.avatar = suceess[@"avatar"];
        self.nick_name = suceess[@"nick_name"];
        NSString *vip_valid_time = [self getDateStringWithDate:[NSDate dateWithTimeIntervalSince1970:[suceess[@"vip_valid_time"] integerValue] / 1000] DateFormat:@"yyyy/MM/dd"];
        self.vip_valid_time = [NSString stringWithFormat:SPDStringWithKey(@"高级会员 %@ 到期", nil), vip_valid_time];;
        self.is_vip = [suceess[@"is_vip"] boolValue];
        self.is_seven_vip = [suceess[@"is_seven_vip"] boolValue];
        NSString *seven_vip_valid_time = [self getDateStringWithDate:[NSDate dateWithTimeIntervalSince1970:[suceess[@"seven_vip_valid_time"] integerValue] / 1000] DateFormat:@"yyyy/MM/dd"];
        self.seven_vip_valid_time = [NSString stringWithFormat:SPDStringWithKey(@"初级会员 %@ 到期", nil), seven_vip_valid_time];
        if (refresh) {
            [self.tableView reloadData];
        } else {
            [self getVipAward];
        }
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)getVipAward {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"vip/award" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.award_num = suceess[@"award_gold_num"];
        self.is_get = [suceess[@"is_get"] boolValue];
        self.is_outfit = [suceess[@"is_outfit_vip_car"] boolValue];
        [self.tableView reloadData];
    } bFailure:^(id  _Nullable failure) {
        self.award_num = @(0);
        [self.tableView reloadData];
    }];
}

- (void)postVipAward {
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary new] bURL:@"vip/award" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        [self requestMyInfoDetailWithRefresh:NO];
    } bFailure:^(id  _Nullable failure) {
        if ([failure isKindOfClass:[NSDictionary class]]) {
            [self showToast:failure[@"msg"]];
        }
    }];
}

- (NSString *)getDateStringWithDate:(NSDate *)date DateFormat:(NSString *)formatString {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:formatString];
    NSString *dateString = [dateFormat stringFromDate:date];
    return dateString;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        VipMyInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VipMyInfoCell" forIndexPath:indexPath];
        [cell.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, self.avatar]]];
        cell.nickNameLabel.text = self.nick_name;
        if (self.is_seven_vip || self.is_vip) {
            cell.avatarImageView.layer.borderWidth = 2;
            cell.doNotHaveLabel.hidden = YES;
            cell.vipIcon.hidden = NO;
            cell.expireTime1Label.hidden = NO;
            if (self.is_seven_vip && self.is_vip) {
                cell.expireTime2Label.hidden = NO;
                cell.expireTime1Label.text = self.seven_vip_valid_time;
                cell.expireTime2Label.text = self.vip_valid_time;
            } else if (self.is_seven_vip) {
                cell.expireTime2Label.hidden = YES;
                cell.expireTime1Label.text = self.seven_vip_valid_time;
            } else if (self.is_vip) {
                cell.expireTime2Label.hidden = YES;
                cell.expireTime1Label.text = self.vip_valid_time;
            }
        } else {
            cell.avatarImageView.layer.borderWidth = 0;
            cell.doNotHaveLabel.hidden = NO;
            cell.vipIcon.hidden = YES;
            cell.expireTime1Label.hidden = YES;
            cell.expireTime2Label.hidden = YES;
        }
        return cell;
    } else if (indexPath.section == 1) {
        VipTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VipTitleCell" forIndexPath:indexPath];
        cell.titleLabel.text = SPDStringWithKey(@"会员特权", nil);
        return cell;
    } else if (indexPath.section == 2) {
        if (self.is_vip) {
            VipPlusPrivilegeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VipPlusPrivilegeCell" forIndexPath:indexPath];
            cell.delegate = self;
            cell.isBuy = self.is_vip;
            cell.award_num = self.award_num;
            cell.is_get = self.is_get;
            cell.is_outfit = self.is_outfit;
            return cell;
        } else {
            VipCommonPrivilegeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VipCommonPrivilegeCell" forIndexPath:indexPath];
            cell.delegate = self;
            cell.isBuy = self.is_seven_vip;
            return cell;
        }
    } else {
        if (self.is_vip) {
            VipCommonPrivilegeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VipCommonPrivilegeCell" forIndexPath:indexPath];
            cell.delegate = self;
            cell.isBuy = self.is_seven_vip;
            return cell;
        } else {
            VipPlusPrivilegeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VipPlusPrivilegeCell" forIndexPath:indexPath];
            cell.delegate = self;
            cell.isBuy = self.is_vip;
            cell.award_num = self.award_num;
            cell.is_get = self.is_get;
            cell.is_outfit = self.is_outfit;
            return cell;
        }
    }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 118;
    } else if (indexPath.section == 1) {
        return 50;
    } else if (indexPath.section == 2) {
        return self.is_vip ? 291 : 0.1;
    } else {
        return self.is_vip ? 0.1 : 249;
    }
}

#pragma mark - VipCommonPrivilegeCellDelegate

- (void)didClickBuyCommonVip {

}

#pragma mark - VipPlusPrivilegeCellDelegate

- (void)didClickBuyPlusVip {
    [kIAPManager startPaymentWithProductId:@"com.fafafa.plus.30daysVIP.NR" completion:^(BOOL succeeded) {
        if (succeeded) {
            [self requestMyInfoDetailWithRefresh:YES];
        }
    }];
}

- (void)didClickGetAward {
    [self postVipAward];
}

- (void)didClickEquipVehicle {
    SPDVehicleViewController *vc = [[SPDVehicleViewController alloc] init];
    vc.isMine = YES;
    vc.user_id = [SPDApiUser currentUser].userId;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Setters and Getters

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor colorWithHexString:@"eeeeee"];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerNib:[UINib nibWithNibName:@"VipMyInfoCell" bundle:nil] forCellReuseIdentifier:@"VipMyInfoCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"VipTitleCell" bundle:nil] forCellReuseIdentifier:@"VipTitleCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"VipCommonPrivilegeCell" bundle:nil] forCellReuseIdentifier:@"VipCommonPrivilegeCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"VipPlusPrivilegeCell" bundle:nil] forCellReuseIdentifier:@"VipPlusPrivilegeCell"];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

@end
