//
//  RankListController.m
//  SimpleDate
//
//  Created by 李楠 on 2018/12/11.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "RankListController.h"
#import "RankOptionCollectionViewCell.h"
#import "RankOptionTableViewCell.h"
#import "RankModel.h"
#import "RankListHeaderCell.h"
#import "RankListCell.h"

@interface RankListController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, RankListHeaderCellDelegate, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *optionCollectionView;
@property (weak, nonatomic) IBOutlet UIView *optionView;
@property (weak, nonatomic) IBOutlet UILabel *optionLabel;
@property (weak, nonatomic) IBOutlet UIView *optionBackgroundView;
@property (weak, nonatomic) IBOutlet UITableView *optionTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *optionTableViewWidth;
@property (weak, nonatomic) IBOutlet UICollectionView *listCollectionView;
@property (weak, nonatomic) IBOutlet UIView *myRankView;
@property (weak, nonatomic) IBOutlet UILabel *rankLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property(nonatomic, strong) LY_PortraitView *portraitView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLabelTop;
@property (weak, nonatomic) IBOutlet UIImageView *genderImageView;
@property (weak, nonatomic) IBOutlet UIButton *levelBtn;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet UIImageView *valueImageView;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@property (weak, nonatomic) IBOutlet UIImageView *distanceImageView;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *noClanLabel;

@property (nonatomic, strong) NSArray *optionArr;
@property (nonatomic, strong) NSDictionary *optionNameDic;
@property (nonatomic, assign) NSInteger selectedOptionIndex;
@property (nonatomic, strong) NSDictionary *selectedColorDic;
@property (nonatomic, strong) NSDictionary *urlDic;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) RankModel *myRankModel;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) NSURLSessionDataTask *task;
@property (nonatomic, strong) UIView *listWhiteView;
@property (nonatomic, strong) UILabel *emptyLabel;

@end

@implementation RankListController

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
    }
    return _portraitView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureSubviews];
    self.page = 0;
    [self requestDataWithAnimated:YES];
    
    [self.avatarImageView addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.avatarImageView);
    }];
}

#pragma mark - Private methods

- (void)configureSubviews {
    if ([self.type isEqualToString:@"magic"]) {
        NSString *option = self.optionArr[self.selectedOptionIndex];
        self.optionLabel.text = SPDStringWithKey(self.optionNameDic[option], nil);
        [self.optionTableView registerNib:[UINib nibWithNibName:@"RankOptionTableViewCell" bundle:nil] forCellReuseIdentifier:@"RankOptionTableViewCell"];
    } else {
        self.optionView.hidden = YES;
        [self.optionCollectionView registerNib:[UINib nibWithNibName:@"RankOptionCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"RankOptionCollectionViewCell"];
    }
    
    [self.listCollectionView insertSubview:self.listWhiteView atIndex:0];
    [self.listCollectionView registerNib:[UINib nibWithNibName:@"RankListHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"RankListHeaderCell"];
    [self.listCollectionView registerNib:[UINib nibWithNibName:@"RankListCell" bundle:nil] forCellWithReuseIdentifier:@"RankListCell"];
    self.listCollectionView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
        self.page = 0;
        [self requestDataWithAnimated:NO];
    }];
    self.listCollectionView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
        [self requestDataWithAnimated:NO];
    }];
    self.listCollectionView.mj_footer.hidden = YES;
    
    self.myRankView.layer.shadowColor = [UIColor colorWithRed:47 / 255.0 green:45 / 255.0 blue:51 / 255.0 alpha:0.1].CGColor;
    self.myRankView.layer.shadowOffset = CGSizeMake(0, 4);
    self.myRankView.layer.shadowOpacity = 1;
    self.myRankView.layer.shadowRadius = 10;
}

- (void)requestDataWithAnimated:(BOOL)animated {
    NSString *url = self.urlDic[self.type];
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObject:@(self.page) forKey:@"page"];
    if ([self.type isEqualToString:@"charm"] || [self.type isEqualToString:@"riche"]) {
        [params setObject:self.type forKey:@"type"];
        [params setObject:self.optionArr[self.selectedOptionIndex] forKey:@"time"];
    } else if ([self.type isEqualToString:@"magic"]) {
        [params setObject:self.optionArr[self.selectedOptionIndex] forKey:@"magic"];
    }
    [self.task cancel];
    self.task = [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:params] bURL:url bAnimated:animated bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if (self.page == 0) {
            if (suceess[@"mine"]) {
                self.myRankModel = [RankModel initWithDictionary:suceess[@"mine"]];
            } else {
                self.myRankModel = nil;
            }
            [self reloadMyRankView];
            [self.dataArr removeAllObjects];
        }
        NSArray *list = suceess[@"list"];
        for (NSDictionary *dic in list) {
            RankModel *model = [RankModel initWithDictionary:dic];
            [self.dataArr addObject:model];
        }
        [self.listCollectionView reloadData];
        [self.listCollectionView sendSubviewToBack:self.listWhiteView];
        self.page++;
        
        self.emptyLabel.hidden = self.dataArr.count > 3;
        [self.listCollectionView.mj_header endRefreshing];
        if (self.listCollectionView.mj_footer.state == MJRefreshStateRefreshing) {
            [self.listCollectionView.mj_footer endRefreshingWithCompletionBlock:^{
                self.listCollectionView.mj_footer.hidden = (self.page >= 5 || list.count == 0);
            }];
        } else {
            self.listCollectionView.mj_footer.hidden = (self.page >= 5 || list.count == 0);
        }
    } bFailure:^(id  _Nullable failure) {
        [self.listCollectionView.mj_header endRefreshing];
        [self.listCollectionView.mj_footer endRefreshing];
    }];
}

- (void)reloadMyRankView {
    self.myRankView.hidden = NO;
    
    if (self.myRankModel) {
        NSInteger rank = self.myRankModel.rank ? self.myRankModel.rank.integerValue + 1 : 1000;
        self.rankLabel.text = rank > 999 ? @"999+" : [NSString stringWithFormat:@"%ld", rank];
//        [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, self.myRankModel.avatar]]];
        
        self.portraitView.portrait = self.myRankModel.portrait;
        
        self.nameLabel.text = self.myRankModel.name;
        self.genderImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"rank_gender_%@", self.myRankModel.gender]];
        self.distanceImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"rank_distance_%@", rank == 1 ? @"below" : @"above"]];
        self.distanceLabel.text = self.myRankModel.distance && rank < 1000 ? self.myRankModel.distance.stringValue : @"--";
        self.distanceLabel.textColor = [UIColor colorWithHexString:rank == 1 ? @"FE4978" : @"24CC9F"];
        self.noClanLabel.hidden = YES;
        
        if ([self.type isEqualToString:@"charm"] || [self.type isEqualToString:@"riche"]) {
            self.nameLabelTop.constant = 21;
            self.valueImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"rank_score_%@", self.type]];
            self.valueLabel.text = self.myRankModel.score ? : @"--";
            self.valueLabel.textColor = [UIColor colorWithHexString:[self.type isEqualToString:@"charm"] ? @"FF3962" : @"FCCD62"];
        } else {
            self.valueImageView.image = [UIImage imageNamed:@"rank_exp"];
            self.valueLabel.text = self.myRankModel.exp ? self.myRankModel.exp.stringValue : @"--";
            self.valueLabel.textColor = [UIColor colorWithHexString:@"3BDB9F"];
            
            if ([self.type isEqualToString:@"person"]) {
                self.levelBtn.hidden = NO;
                self.levelBtn.titleLabel.font = [UIFont systemFontOfSize:12.5];
                [SPDCommonTool configDataForLevelBtnWithLevel:self.myRankModel.level.intValue andUIButton:self.levelBtn];
            } else if ([self.type isEqualToString:@"clan"]) {
//                self.avatarImageView.layer.cornerRadius = 7;
                self.genderImageView.hidden = YES;
                self.levelBtn.hidden = NO;
                self.levelBtn.titleLabel.font = [UIFont systemFontOfSize:11];
                [self.levelBtn setTitle:[NSString stringWithFormat:@"LV.%ld", self.myRankModel.level.integerValue] forState:UIControlStateNormal];
                [self.levelBtn setBackgroundImage:[UIImage imageNamed:@"rank_level_clan"] forState:UIControlStateNormal];
            } else if ([self.type isEqualToString:@"magic"]) {
                self.levelLabel.hidden = NO;
                self.levelLabel.text = [NSString stringWithFormat:@"LV.%ld", self.myRankModel.level.integerValue];
            }
        }
    } else {
        self.noClanLabel.hidden = NO;
        self.noClanLabel.text = SPDStringWithKey(@"暂无家族，快加入或创建家族吧～", nil);
    }
}

#pragma mark - Event responses

- (IBAction)tapOptionView:(UITapGestureRecognizer *)sender {
    self.optionBackgroundView.hidden = NO;
    self.optionTableView.hidden = NO;
}

- (IBAction)tapOptionBackgroundView:(UITapGestureRecognizer *)sender {
    self.optionBackgroundView.hidden = YES;
    self.optionTableView.hidden = YES;
}

- (IBAction)tapMyRankView:(UITapGestureRecognizer *)sender {
    if (![self.type isEqualToString:@"clan"]) {
        [self pushToDetailTableViewController:self userId:[SPDApiUser currentUser].userId];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if (collectionView == self.optionCollectionView) {
        if ([self.type isEqualToString:@"riche"] || [self.type isEqualToString:@"charm"]) {
            return self.optionArr.count;
        } else {
            return 0;
        }
    } else {
        return 1;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == self.optionCollectionView) {
        return 1;
    } else {
        if (self.dataArr.count <= 3) {
            return 1;
        } else {
            return self.dataArr.count - 2;
        }
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.optionCollectionView) {
        RankOptionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RankOptionCollectionViewCell" forIndexPath:indexPath];
        NSString *option = self.optionArr[indexPath.section];
        cell.textLabel.text = SPDStringWithKey(self.optionNameDic[option], nil);
        if (indexPath.section == self.selectedOptionIndex) {
            cell.backgroundColor = [UIColor whiteColor];
            cell.textLabel.textColor = [UIColor colorWithHexString:self.selectedColorDic[self.type]];
        } else {
            cell.backgroundColor = [UIColor clearColor];
            cell.textLabel.textColor = [UIColor whiteColor];
        }
        return cell;
    } else {
        if (indexPath.row == 0) {
            RankListHeaderCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RankListHeaderCell" forIndexPath:indexPath];
            cell.type = self.type;
            cell.dataArr = [self.dataArr subarrayWithRange:NSMakeRange(0, self.dataArr.count < 3 ? self.dataArr.count : 3)];
            cell.delegate = self;
            return cell;
        } else {
            RankListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RankListCell" forIndexPath:indexPath];
            cell.type = self.type;
            cell.model = self.dataArr[indexPath.row + 2];
            return cell;
        }
    }
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.optionCollectionView) {
        if (indexPath.section != self.selectedOptionIndex) {
            self.selectedOptionIndex = indexPath.section;
            [collectionView reloadData];
            if ([UIDevice currentDevice].systemVersion.floatValue >= 11.0) {
                [collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
            }
            self.page = 0;
            [self requestDataWithAnimated:YES];
        }
    } else {
        if (![self.type isEqualToString:@"clan"] && indexPath.row != 0) {
            RankModel *model = self.dataArr[indexPath.row + 2];
            [self pushToDetailTableViewController:self userId:model._id];
        }
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.optionCollectionView) {
        NSString *option = self.optionArr[indexPath.section];
        CGFloat width = [SPDStringWithKey(self.optionNameDic[option], nil) boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 22) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:11]} context:nil].size.width;
        return CGSizeMake(ceilf(width) + 10.5 * 2, 22);
    } else {
        if (indexPath.row == 0) {
            return CGSizeMake(kScreenW, 271.5);
        } else {
            return CGSizeMake(kScreenW, 60);
        }
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (collectionView == self.optionCollectionView) {
        BOOL isArabic = [SPDCommonTool currentVersionIsArbic];
        BOOL iOS10 = [[UIDevice currentDevice].systemVersion floatValue] < 11.0;
        if (section == 0) {
            return UIEdgeInsetsMake(0, isArabic && iOS10 ? 5 : 15, 0, isArabic && iOS10 ? 15 : 5);
        } else if (section == self.optionArr.count - 1) {
            return UIEdgeInsetsMake(0, isArabic && iOS10 ? 15 : 5, 0, isArabic && iOS10 ? 5 : 15);
        } else {
            return UIEdgeInsetsMake(0, 5, 0, 5);
        }
    } else {
        return UIEdgeInsetsZero;
    }
}

#pragma mark - RankListHeaderCellDelegate

- (void)rankListHeaderCell:(RankListHeaderCell *)cell didTapAvatarWithModel:(RankModel *)model {
    [self pushToDetailTableViewController:self userId:model._id];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.listCollectionView) {
        CGPoint translation = [scrollView.panGestureRecognizer translationInView:scrollView];
        self.myRankView.hidden = translation.y < 0;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self.type isEqualToString:@"magic"]) {
        return self.optionArr.count;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RankOptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RankOptionTableViewCell" forIndexPath:indexPath];
    NSString *option = self.optionArr[indexPath.row];
    cell.optionLabel.text = SPDStringWithKey(self.optionNameDic[option], nil);
    if (indexPath.row == self.selectedOptionIndex) {
        cell.backgroundColor = [UIColor colorWithHexString:@"#E7DAF9"];
    } else {
        cell.backgroundColor = [UIColor colorWithHexString:@"#F7F7F7"];
    }
    
    [cell.optionLabel sizeToFit];
    CGFloat cellWidth = cell.optionLabel.bounds.size.width + 10.5 + 4 + 8 + 9;
    if (cellWidth > self.optionTableViewWidth.constant) {
        self.optionTableViewWidth.constant = cellWidth;
    }
    return cell;
}

#pragma mark - UITableViewDelegaate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.optionBackgroundView.hidden = YES;
    tableView.hidden = YES;
    
    if (indexPath.row != self.selectedOptionIndex) {
        self.selectedOptionIndex = indexPath.row;
        [tableView reloadData];
        NSString *option = self.optionArr[self.selectedOptionIndex];
        self.optionLabel.text = SPDStringWithKey(self.optionNameDic[option], nil);
        
        self.page = 0;
        [self requestDataWithAnimated:YES];
    }
}

#pragma mark - Setters & Getters

- (NSDictionary *)urlDic {
    if (!_urlDic) {
        _urlDic = @{@"riche": @"leader_board",
                    @"charm": @"leader_board",
                    @"person": @"rank.person",
                    @"clan": @"rank.clan",
                    @"magic": @"rank.magic"};
    }
    return _urlDic;
}

- (NSArray *)optionArr {
    if (!_optionArr) {
        if ([self.type isEqualToString:@"riche"] || [self.type isEqualToString:@"charm"]) {
            _optionArr = @[@"today", @"this", @"last", @"thismonth", @"lastmonth"];
            self.optionNameDic = @{@"today": @"今日排行",
                                   @"this": @"本周排行",
                                   @"last": @"上周排行",
                                   @"thismonth": @"本月排行",
                                   @"lastmonth": @"上月排行"};
        } else if ([self.type isEqualToString:@"magic"]) {
            _optionArr = @[@"blessing", @"waterball", @"fireball", @"blizzard", @"lightning", @"wind"];
            self.optionNameDic = @{@"blessing": @"增加魅力值",
                                   @"waterball": @"减少魅力值",
                                   @"fireball": @"减少经验值",
                                   @"blizzard": @"禁止发言",
                                   @"lightning": @"禁止上麦",
                                   @"wind": @"踢出房间"};
        }
    }
    return _optionArr;
}

- (NSDictionary *)selectedColorDic {
    if (!_selectedColorDic) {
        _selectedColorDic = @{@"riche": @"#F7BC5A",
                              @"charm": @"#FF6B93",
                              @"magic": @"#A25CFF"};
    }
    return _selectedColorDic;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (UIView *)listWhiteView {
    if (!_listWhiteView) {
        _listWhiteView = [UIView new];
        _listWhiteView.frame = CGRectMake(0, 271.5, kScreenW, 60 * 97 + kScreenH);
        _listWhiteView.backgroundColor = [UIColor colorWithHexString:@"#F7F7F7"];
        
        self.emptyLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 80, kScreenW - 10 * 2, 20)];
        self.emptyLabel.font = [UIFont systemFontOfSize:17];
        self.emptyLabel.textColor = [UIColor colorWithHexString:@"#666666"];
        self.emptyLabel.textAlignment = NSTextAlignmentCenter;
        self.emptyLabel.text = SPDStringWithKey(@"虚位以待，快来抢榜", nil);
        [_listWhiteView addSubview:self.emptyLabel];
    }
    return _listWhiteView;
}

@end
