//
//  RankContainerController.m
//  SimpleDate
//
//  Created by 李楠 on 2018/12/11.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "RankContainerController.h"
#import "RankListController.h"

@interface RankContainerController ()

@property (nonatomic, strong) NSArray *typeArr;
@property (nonatomic, strong) NSDictionary *nameDic;
@property (nonatomic, strong) NSDictionary *colorDic;
@property (nonatomic, strong) CAGradientLayer *gradientLayer;
@property (nonatomic, strong) NSString *helpDesc;

@end

@implementation RankContainerController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"排行榜".localized;

    [self requestRankCatalogue];
    
    self.automaticallyCalculatesItemWidths = YES;;
    self.titleSizeNormal = 15;
    self.titleSizeSelected = 15;
    self.titleColorNormal = [UIColor colorWithWhite:1 alpha:0.5];
    self.titleColorSelected = [UIColor whiteColor];
    self.selectIndex = (int)[self.typeArr indexOfObject:self.selectedType];
    [self reloadData];
    self.scrollView.backgroundColor = [UIColor clearColor];
    
    UIButton *helpButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [helpButton setBackgroundImage:[UIImage imageNamed:@"rank_help"] forState:UIControlStateNormal];
    [helpButton addTarget:self action:@selector(clickHelpButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:helpButton];
    [helpButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavHeight + 85.5);
        make.right.mas_equalTo(0);
        make.width.mas_equalTo(30);
        make.height.mas_equalTo(22);
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    [self.navigationController.navigationBar vhl_setShadowImageHidden:true];
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:0.0];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
//    [self.navigationController.navigationBar vhl_setShadowImageHidden:false];
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:1.0];
}

#pragma mark - Private methods

- (void)requestRankCatalogue {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionary] bURL:@"rank.catalogue" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.helpDesc = suceess[@"desc"];
    } bFailure:^(id  _Nullable failure) {

    }];
}

#pragma mark - Event response

- (void)clickHelpButton:(UIButton *)sender {
    if (![SPDCommonTool isEmpty:self.helpDesc]) {
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil message:self.helpDesc preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:SPDStringWithKey(@"确定", nil) style:UIAlertActionStyleCancel handler:nil];
        [controller addAction:action];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

#pragma mark - WMPageControllerDataSource

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.typeArr.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    NSString *type = self.typeArr[index];
    return SPDStringWithKey(self.nameDic[type], nil);
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    RankListController *vc = [[RankListController alloc] init];
    NSString *type = self.typeArr[index];
    vc.type = type;
    return vc;
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    return CGRectMake(0, NavHeight + 14 + 10.5 * 2 + 14, self.view.frame.size.width, self.view.frame.size.height - NavHeight - (14 + 10.5 * 2) - 14);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    return CGRectMake(0, NavHeight, self.view.frame.size.width, 14 + 10.5 * 2);
}

#pragma mark - WMMenuViewDelegate

- (CGFloat)menuView:(WMMenuView *)menu widthForItemAtIndex:(NSInteger)index {
    CGFloat width = [super menuView:menu widthForItemAtIndex:index];
    return width + 31;
}

#pragma mark - WMPageControllerDelegate

- (void)pageController:(WMPageController *)pageController didEnterViewController:(nonnull __kindof UIViewController *)viewController withInfo:(nonnull NSDictionary *)info {
    NSArray *colors = self.colorDic[[viewController type]];
    self.gradientLayer.colors = @[(__bridge id)[colors[0] CGColor], (__bridge id)[colors[1] CGColor]];
}

- (void)pageController:(WMPageController *)pageController didScrollWithPageRatio:(CGFloat)ratio {
    NSInteger lastIndex = floor(ratio);
    NSInteger nextIndex = ceil(ratio);
    if (lastIndex != nextIndex) {
        NSString *lastType = self.typeArr[lastIndex];
        UIColor *lastStartColor = self.colorDic[lastType][0];
        CGFloat lastStartRed, lastStartGreen, lastStartBlue, lastStartAlpha;
        [lastStartColor getRed:&lastStartRed green:&lastStartGreen blue:&lastStartBlue alpha:&lastStartAlpha];
        UIColor *lastEndColor = self.colorDic[lastType][1];
        CGFloat lastEndRed, lastEndGreen, lastEndBlue, lastEndAlpha;
        [lastEndColor getRed:&lastEndRed green:&lastEndGreen blue:&lastEndBlue alpha:&lastEndAlpha];
        
        NSString *nextType = self.typeArr[nextIndex];
        UIColor *nextStartColor = self.colorDic[nextType][0];
        CGFloat nextStartRed, nextStartGreen, nextStartBlue, nextStartAlpha;
        [nextStartColor getRed:&nextStartRed green:&nextStartGreen blue:&nextStartBlue alpha:&nextStartAlpha];
        UIColor *nextEndColor = self.colorDic[nextType][1];
        CGFloat nextEndRed, nextEndGreen, nextEndBlue, nextEndAlpha;
        [nextEndColor getRed:&nextEndRed green:&nextEndGreen blue:&nextEndBlue alpha:&nextEndAlpha];
        
        CGFloat currentStartRed = lastStartRed + (nextStartRed - lastStartRed) * (ratio - lastIndex);
        CGFloat currentStartGreen = lastStartGreen + (nextStartGreen - lastStartGreen) * (ratio - lastIndex);
        CGFloat currentStartBlue = lastStartBlue + (nextStartBlue - lastStartBlue) * (ratio - lastIndex);
        UIColor *currentStartColor = [UIColor colorWithRed:currentStartRed green:currentStartGreen blue:currentStartBlue alpha:1];
        
        CGFloat currentEndRed = lastEndRed + (nextEndRed - lastEndRed) * (ratio - lastIndex);
        CGFloat currentEndGreen = lastEndGreen + (nextEndGreen - lastEndGreen) * (ratio - lastIndex);
        CGFloat currentEndBlue = lastEndBlue + (nextEndBlue - lastEndBlue) * (ratio - lastIndex);
        UIColor *currentEndColor = [UIColor colorWithRed:currentEndRed green:currentEndGreen blue:currentEndBlue alpha:1];
        
        [CATransaction begin];
        [CATransaction setDisableActions:YES];
        self.gradientLayer.colors = @[(__bridge id)currentStartColor.CGColor, (__bridge id)currentEndColor.CGColor];
        [CATransaction commit];
    }
}

#pragma mark - Setters and Getters

- (NSArray *)typeArr {
    if (!_typeArr) {
        if ([SPDCommonTool currentVersionIsArbic]) {
            _typeArr = @[@"magic", @"clan", @"person", @"charm", @"riche"];
        } else {
            _typeArr = @[@"riche", @"charm", @"person", @"clan", @"magic"];
        }
    }
    return _typeArr;
}

- (NSDictionary *)nameDic {
    if (!_nameDic) {
        _nameDic = @{@"riche": @"土豪榜",
                     @"charm": @"魅力榜",
                     @"person": @"个人榜",
                     @"clan": @"家族榜",
                     @"magic": @"魔法榜"};
    }
    return _nameDic;
}

- (NSDictionary *)colorDic {
    if (!_colorDic) {
        _colorDic = @{@"riche": @[[UIColor colorWithHexString:@"#F2AE53"],
                                  [UIColor colorWithHexString:@"#FFD767"]],
                      @"charm": @[[UIColor colorWithHexString:@"#FF6B93"],
                                  [UIColor colorWithHexString:@"#FF9577"]],
                      @"person": @[[UIColor colorWithHexString:@"#6E56FF"],
                                   [UIColor colorWithHexString:@"#5591FF"]],
                      @"clan": @[[UIColor colorWithHexString:@"#22BBB9"],
                                 [UIColor colorWithHexString:@"#45E994"]],
                      @"magic": @[[UIColor colorWithHexString:@"#9853FF"],
                                  [UIColor colorWithHexString:@"#DD8FFF"]]};
    }
    return _colorDic;
}

- (CAGradientLayer *)gradientLayer {
    if (!_gradientLayer) {
        _gradientLayer = [CAGradientLayer layer];
        _gradientLayer.frame = self.view.bounds;
        _gradientLayer.startPoint = CGPointMake(0, 0.5);
        _gradientLayer.endPoint = CGPointMake(1, 0.5);
        [self.view.layer insertSublayer:_gradientLayer atIndex:0];
    }
    return _gradientLayer;
}

@end
