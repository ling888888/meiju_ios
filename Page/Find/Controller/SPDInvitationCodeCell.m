//
//  SPDInvitationCodeCell.m
//  SimpleDate
//
//  Created by Monkey on 2018/9/19.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDInvitationCodeCell.h"
#import "UIButton+HHImagePosition.h"

@interface SPDInvitationCodeCell()
@property (weak, nonatomic) IBOutlet UILabel *inviteCodeLabel;

@end

@implementation SPDInvitationCodeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.inviteCodeLabel.text = [NSString stringWithFormat:@"%@%@",SPDStringWithKey(@"我的邀请码:", nil),[SPDCommonTool getInvitationCodeWithUserId:[SPDApiUser currentUser].userId]];
    [self.inviteBtn setTitle:SPDStringWithKey(@"邀请", nil)forState:UIControlStateNormal];
    [self.inviteBtn setContentEdgeInsets:UIEdgeInsetsMake(-2, 0, 0, 0)];
}

- (void)showBtnAnimation:(UIButton *)button {
    CAKeyframeAnimation *animation;
    animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = 0.8;
    animation.repeatCount= MAXFLOAT;
    animation.fillMode = kCAFillModeForwards;
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.2, 1.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    [button.layer addAnimation:animation forKey:@"hl-position"];
}

//#define Angle2Radian(angle) ((angle) / 180.0 * M_PI)
//- (void)shakingAnimation {
//    CAKeyframeAnimation *anim = [CAKeyframeAnimation animation];
//    anim.keyPath = @"transform.rotation";
//
//    anim.values = @[@(Angle2Radian(-5)), @(Angle2Radian(5)), @(Angle2Radian(-5))];
//    anim.duration = 0.25;
//
//    // 动画次数设置为最大
//    anim.repeatCount = MAXFLOAT;
//    // 保持动画执行完毕后的状态
//    anim.removedOnCompletion = NO;
//    anim.fillMode = kCAFillModeForwards;
//
//    [self.inviteBtn.layer addAnimation:anim forKey:@"shake"];
//}


- (void)setGold:(NSString *)gold {
    _gold = gold;
//    [self shakingAnimation];
//    [self loadShakeAnimationForView:self.inviteBtn];
//    [self.inviteBtn setTitle:[NSString stringWithFormat:SPDStringWithKey(@"邀请+%@", nil),gold] forState:UIControlStateNormal];
//    [self.inviteBtn setImage:[UIImage imageNamed:@"ic_invitation_gold"] forState:UIControlStateNormal];
//    [self.inviteBtn setImagePosition:HHImagePositionRight spacing:3.0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
