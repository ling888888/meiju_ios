//
//  DynamicListViewController.h
//  SimpleDate
//
//  Created by 李楠 on 2018/6/27.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "BaseViewController.h"
#import "JXPagerView.h"

@interface DynamicListViewController : BaseViewController <JXPagerViewListViewDelegate>

@end
