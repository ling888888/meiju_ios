//
//  DetailsUserFansController.h
//  SimpleDate
//
//  Created by ling Hou on 2020/5/8.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_BaseTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface DetailsUserFansController : LY_BaseTableViewController

@property (nonatomic, copy) NSString * userId;

@end

NS_ASSUME_NONNULL_END
