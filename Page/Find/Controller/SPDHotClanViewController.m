//
//  SPDHotClanViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2018/1/3.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDHotClanViewController.h"
//#import "HomeGlobalMessageCell.h"
#import "HomeClanTItileCell.h"
#import "SPDClanHotListCell.h"
//#import "SPDWorldChatMessage.h"
//#import "SPDCRSendMagicMessage.h"
#import "SPDSearchUserViewController.h"
#import "BannerModel.h"
#import "FamilyModel.h"
#import "SPDCountryListViewController.h"
#import "ZegoKitManager.h"
#import "JXPagerView.h"

@interface SPDHotClanViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, HomeClanTItileCellDelegate, SPDClanHotListCellDelegate>
@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *bannerArray;
@property (nonatomic, strong) NSMutableArray *imageArray;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) BOOL isAgentService;
@property (nonatomic, strong) NSMutableArray *clanArray;

@end

@implementation SPDHotClanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        
    [self.view addSubview:self.collectionView];

    [self requestSysConfig];
    [self requestMyInfoDetail];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.collectionView reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
//    [self loadMessageWithRCMessageContent:[SPDRCIMDataSource shareInstance].lastGlobalMessage];
//    [self.globalScrollView resumeAnimation];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleChatRoomGlobalMsg:) name:SPDLiveKitChatRoomGlobalNotification object:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:SPDLiveKitChatRoomGlobalNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods

- (void)requestSysConfig {
    NSDictionary *dic = @{@"ios_version": LOCAL_VERSION_SHORT};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"sys.config" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        NSDictionary *config = suceess[@"config"];
        ZegoManager.chatroomLock = [config[@"chatroomLock"] isEqualToString:@"on"];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}



- (void)requestMyInfoDetail {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.info.detail" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.isAgentService = [suceess[@"is_agent_gm"] boolValue];
        [self requestClanHot];
    } bFailure:^(id  _Nullable failure) {
        [self requestClanHot];
    }];
}

- (void)requestClanHot {
    NSDictionary *dic = @{@"page": @(self.page)};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"clan.hot" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if (self.page == 0) {
            [self.clanArray removeAllObjects];
        }
        
        NSArray *list = suceess[@"list"];
        for (NSDictionary *dic in list) {
            FamilyModel *model = [FamilyModel initWithDictionary:dic];
            __block BOOL exist = NO;
            if (self.page != 0) {
                [self.clanArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if ([model._id isEqualToString:[obj valueForKey:@"_id"]]) {
                        exist = YES;
                        *stop = YES;
                    }
                }];
            }
            if (!exist) {
                [self.clanArray addObject:model];
            }
        }
        [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
        self.page++;
        
        [self.collectionView.mj_header endRefreshing];
        if (self.collectionView.mj_footer.state == MJRefreshStateRefreshing) {
            [self.collectionView.mj_footer endRefreshingWithCompletionBlock:^{
                self.collectionView.mj_footer.hidden = !list.count;
            }];
        } else {
            self.collectionView.mj_footer.hidden = !list.count;
        }
    } bFailure:^(id  _Nullable failure) {
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
    }];
}

- (void)requestClanBatch {
    NSMutableArray *clanIds = [DBUtil queryClanHistory];
    if (clanIds.count) {
        NSString *clanIdsStr = [clanIds componentsJoinedByString:@","];
        [RequestUtils commonGetRequestUtils:[@{@"clan_ids": clanIdsStr} mutableCopy] bURL:@"batch" isClan:YES bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
            for (NSDictionary *dic in suceess[@"clans"]) {
                FamilyModel *model = [FamilyModel initWithDictionary:dic];
                if ([model.lang isEqualToString:[SPDApiUser currentUser].lang]) {
                    if ([model.name isEqualToString:@"404"]) {
                        [DBUtil deleteClanHistory:model._id];
                    } else {
                        __block BOOL exist = NO;
                        [self.clanArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            if ([model._id isEqualToString:[obj valueForKey:@"_id"]]) {
                                exist = YES;
                                *stop = YES;
                            }
                        }];
                        if (!exist) {
                            [self.clanArray addObject:model];
                        }
                    }
                }
            }
            [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
        } bFailure:^(id  _Nullable failure) {
            
        }];
    }
}

#pragma mark - Event response

- (void)clickRightBarButtonItem {
//    NSString *url = [NSString stringWithFormat:IMMIGRANT_RULE_URL, [SPDCommonTool getFamyLanguage]];
//    [self loadWebViewController:self title:SPDStringWithKey(@"移民规则", nil) url:url];
//
//    //[MobClick event:@"clickImmigrantBtn"];
    
    SPDSearchUserViewController *vc = [[SPDSearchUserViewController alloc] init];
    vc.showSeachBarOnNavigationBar = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - 世界消息

//- (void)handleChatRoomGlobalMsg:(NSNotification *)notify {
//    __block RCMessage *rcMessage = notify.object;
//    [self loadMessageWithRCMessageContent:rcMessage.content];
//}

//- (void)loadMessageWithRCMessageContent:(RCMessageContent *)rcMessage {
//    if ([rcMessage isKindOfClass:[SPDWorldChatMessage class]]) {
//        SPDWorldChatMessage *msg = (SPDWorldChatMessage *)rcMessage;
//        RCUserInfo *usrInfo = msg.senderUserInfo;
//        if (msg.type == nil) {
//            if (msg.senderUserInfo) {
//                NSDictionary *dict = @{
//                                       @"nickname": usrInfo.name ? : @"",
//                                       @"avatar": usrInfo.portraitUri ? : @"",
//                                       @"content": msg.content ? : @"",
//                                       @"gender": msg.gender ? : @"",
//                                       @"clan_id": msg.send_ClanId ? : @"",
//                                       @"clan_name": msg.send_ClanName ? : @"",
//                                       @"mentionedType": msg.mentionedType ? : @"",
//                                       @"mentionedList": msg.mentionedList ? : @[]
//                                       };
//                self.globalScrollView.dic = dict;
//            } else {
//                [SPDCommonTool postiOSMethodToRecoredRCall:@"home SPDWorldChatMessage senderUserInfo nil"];
//            }
//        } else if ([msg.type isEqualToString:@"2"] ||[msg.type isEqualToString:@"3"]) {
//            if (msg.senderUserInfo) {
//                NSDictionary *dict = @{
//                                       @"user_id": usrInfo.userId,
//                                       @"sender_nickname": usrInfo.name ? : @"",
//                                       @"savatar": usrInfo.portraitUri ? : @"",
//                                       @"ravatar": msg.receive_avatar ? : @"",
//                                       @"receive_nickname": msg.receive_nickname ? : @"",
//                                       @"clan_name": msg.send_ClanName ? : @"",
//                                       @"gift_url": msg.present_url ? : @"",
//                                       @"clan_id": msg.send_ClanId ? : @"",
//                                       @"type": msg.type,
//                                       @"num": msg.num ? : @"1"
//                                       };
//                self.globalScrollView.dic = dict;
//            } else {
//                [SPDCommonTool postiOSMethodToRecoredRCall:@"home SPDWorldChatMessage senderUserInfo nil"];
//            }
//        }
//    } else if ([rcMessage isKindOfClass:[SPDCRSendMagicMessage class]]) {
//        SPDCRSendMagicMessage *msg = (SPDCRSendMagicMessage *)rcMessage;
//        RCUserInfo *usrInfo = msg.senderUserInfo;
//        if (msg.senderUserInfo) {
//            NSDictionary *dict = @{
//                                   @"user_id": usrInfo.userId,
//                                   @"sender_nickname": usrInfo.name ? : @"",
//                                   @"savatar": usrInfo.portraitUri ? : @"",
//                                   @"ravatar": msg.receive_avatar ? : @"",
//                                   @"receive_nickname": msg.receive_nickname ? : @"",
//                                   @"clan_name": msg.clan_name ? : @"",
//                                   @"gift_url": msg.magic_url ? : @"",
//                                   @"clan_id": msg.clan_id ? : @"",
//                                   @"type": @"magic",
//                                   @"result": msg.result ? : @"fail"
//                                   };
//            self.globalScrollView.dic = dict;
//        }
//    } else {
//        [self loadUnkonwnGlobalMessage];
//    }
//}

//- (void)loadUnkonwnGlobalMessage {
//    if ([SPDRCIMDataSource shareInstance].lastGlobalMessage.senderUserInfo) {
//        RCUserInfo *usrInfo = [SPDRCIMDataSource shareInstance].lastGlobalMessage.senderUserInfo;
//        NSDictionary *dict = @{
//                               @"user_id": usrInfo.userId,
//                               @"nickname": usrInfo.name,
//                               @"avatar": usrInfo.portraitUri,
//                               @"content": SPDStringWithKey(@"当前版本无法查看此消息，请升级至最新版本。", nil),
//                               @"gender": @"",
//                               @"clan_id": @"",
//                               @"clan_name": @""
//                               };
//        self.globalScrollView.dic = dict;
//    }
//}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return self.clanArray.count;
        default:
            return 1;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
//        case 0: {
//            HomeGlobalMessageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeGlobalMessageCell" forIndexPath:indexPath];
//            self.globalScrollView = cell.globalScrollView;
//            [self.globalScrollView resumeAnimation];
//            return cell;
//        }
//        case 0: {
//            HomeBannerCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeBannerCell" forIndexPath:indexPath];
//            cell.imagesArray = self.imageArray;
//            cell.delegate = self;
//            return cell;
//        }
//        case 0: {
//            HomeClanTItileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeClanTItileCell" forIndexPath:indexPath];
//            cell.titleLabel.text = SPDStringWithKey(@"地区", nil);
//            cell.imageView.image = [UIImage imageNamed:@"home_country"];
//            cell.separator.hidden = NO;
//            cell.delegate = self;
//            return cell;
//        }
        default: {
            SPDClanHotListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SPDClanHotListCell" forIndexPath:indexPath];
            cell.isAgentService = self.isAgentService;
            cell.model = self.clanArray[indexPath.row];
            cell.delegate = self;
            return cell;
        }
    }
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
//        case 0: {
//            HomeGlobalMessageCell *cell = (HomeGlobalMessageCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
//            [self joinChatroomWithClanId:cell.clanID];
//            break;
//        }
        case 0: {
            FamilyModel *model = self.clanArray[indexPath.row];
            [self joinChatroomWithClanId:model._id];
            break;
        }
        default:
            break;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback ?: self.scrollCallback(scrollView);
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(kScreenW - 16, 90);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, 8, 0, 8);
}

#pragma mark - HomeBannerCellDelegate

- (void)clickedBannerCellWithIndex:(NSInteger)index {
    if (self.bannerArray.count != 0) {
        UIStoryboard *homeStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        BannerDetailController *bannerDetailController = [homeStoryboard instantiateViewControllerWithIdentifier:@"BannerDetailController"];
//        bannerDetailController.bannerModel = self.bannerArray[index];
//        [self.navigationController pushViewController:bannerDetailController animated:YES];
    }
}

#pragma mark - HomeClanTItileCellDelegate

- (void)didClickMoreButtonWithTitle:(NSString *)title {
    if ([title isEqualToString:SPDStringWithKey(@"地区", nil)]) {
        SPDCountryListViewController *vc = [[SPDCountryListViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - SPDClanHotListCellDelegate

- (void)didClickStickBtnWithModel:(FamilyModel *)model {
    [self presentCommonController:SPDStringWithKey(@"确定置顶该家族", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
        NSDictionary *dic = @{@"user_id": [SPDApiUser currentUser].userId, @"clan_id": model._id};
        [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"stick" isClan:YES bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
            [self showToast:SPDStringWithKey(@"置顶成功", nil)];
        } bFailure:^(id  _Nullable failure) {
            if ([failure isKindOfClass:[NSDictionary class]]) {
                if ([failure[@"code"] integerValue] == 2038) {
                    [self showToast:SPDStringWithKey(@"本次置顶次数用完，请1小时候再进行置顶", nil)];
                } else if ([failure[@"code"] integerValue] == 2037) {
                    [self showToast:SPDStringWithKey(@"你不是这个区的客服助手", nil)];
                } else {
                    [self showToast:failure[@"msg"]];
                }
            }
        }];
    }];
}

#pragma mark -

- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return self.collectionView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}

- (void)listWillAppear {
    NSLog(@"%@:%@", self.title, NSStringFromSelector(_cmd));
}

- (void)listDidAppear {
    NSLog(@"%@:%@", self.title, NSStringFromSelector(_cmd));
}

- (void)listWillDisappear {
    NSLog(@"%@:%@", self.title, NSStringFromSelector(_cmd));
}

- (void)listDidDisappear {
    NSLog(@"%@:%@", self.title, NSStringFromSelector(_cmd));
}


#pragma mark - Setters & Getters

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumLineSpacing = 8;
        layout.minimumInteritemSpacing = 8;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - NavHeight - TabBarHeight - 45) collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        [_collectionView registerNib:[UINib nibWithNibName:@"SPDClanHotListCell" bundle:nil] forCellWithReuseIdentifier:@"SPDClanHotListCell"];
        
        __weak typeof(self) weakSelf = self;
        _collectionView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            weakSelf.page = 0;
            [weakSelf requestSysConfig];
//            [weakSelf requestBanner];
            [weakSelf requestMyInfoDetail];
        }];
        _collectionView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            [weakSelf requestClanHot];
        }];
        _collectionView.mj_footer.hidden = YES;
    }
    return _collectionView;
}

- (NSMutableArray *)bannerArray {
    if (!_bannerArray) {
        _bannerArray = [NSMutableArray array];
    }
    return _bannerArray;
}

- (NSMutableArray *)imageArray {
    if (!_imageArray) {
        _imageArray = [NSMutableArray array];
    }
    return _imageArray;
}

- (NSMutableArray *)clanArray {
    if (!_clanArray) {
        _clanArray = [NSMutableArray array];
    }
    return _clanArray;
}

@end
