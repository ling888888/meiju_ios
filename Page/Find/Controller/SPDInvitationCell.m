//
//  SPDInvitationCell.m
//  SimpleDate
//
//  Created by Monkey on 2018/9/19.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDInvitationCell.h"

@interface SPDInvitationCell()
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *allContributionLabel;
@property (weak, nonatomic) IBOutlet UILabel *allGoldNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *yestodayLabel;
@property (weak, nonatomic) IBOutlet UILabel *yestodayGoldLabel;
@property (weak, nonatomic) IBOutlet UILabel *noContributionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *invitation_gold_image1;
@property (weak, nonatomic) IBOutlet UIImageView *invitation_gold_image2;
@property (weak, nonatomic) IBOutlet UIImageView *bindImageView;

@end


@implementation SPDInvitationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.allContributionLabel.text = SPDStringWithKey(@"全部贡献:", nil);
    self.yestodayLabel.text = SPDStringWithKey(@"昨日贡献:", nil);
    self.noContributionLabel.text = SPDStringWithKey(@"暂无贡献", nil);
    self.noContributionLabel.hidden = NO;

}

- (void)setModel:(SPDInviteModel *)model {
    _model = model;
    self.nameLabel.text = model.nick_name;
    [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.avatar]]];
    
    BOOL b = ([model.totalinvit integerValue] == 0 && [model.yesdayinvit integerValue] == 0) ? true : false;
    self.allContributionLabel.hidden = b;
    self.allGoldNumLabel.hidden = b;
    self.yestodayLabel.hidden = b;
    self.yestodayGoldLabel.hidden = b;
    self.invitation_gold_image1.hidden = b;
    self.invitation_gold_image2.hidden = b;
    self.noContributionLabel.hidden = !b;
    self.allGoldNumLabel.text = [model.totalinvit stringValue];
    self.yestodayGoldLabel.text = [model.yesdayinvit stringValue];
 
    NSString * str = model.isapprove ? @"bind" :@"unbind";
    NSString * lang =@"en";
    if ([[SPDCommonTool getFamyLanguage] isEqualToString:@"zh-Hans"] || [[SPDCommonTool getFamyLanguage] isEqualToString:@"ar"] || [[SPDCommonTool getFamyLanguage] isEqualToString:@"en"]) {
        lang = [SPDCommonTool getFamyLanguage];
    }
    self.bindImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_%@",str,lang]];

   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
