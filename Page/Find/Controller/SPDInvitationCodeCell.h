//
//  SPDInvitationCodeCell.h
//  SimpleDate
//
//  Created by Monkey on 2018/9/19.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPDInvitationCodeCell : BaseTableViewCell

@property (nonatomic, copy) NSString * gold;

- (void)showBtnAnimation:(UIButton *)button;

@property (weak, nonatomic) IBOutlet UIButton *inviteBtn;
@end
