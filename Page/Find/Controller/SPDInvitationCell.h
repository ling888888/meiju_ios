//
//  SPDInvitationCell.h
//  SimpleDate
//
//  Created by Monkey on 2018/9/19.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPDInviteModel.h"

@interface SPDInvitationCell : BaseTableViewCell

@property (nonatomic ,strong)SPDInviteModel *model;

@end
