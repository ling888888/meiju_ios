//
//  LY_HomeViewController.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/29.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_BaseCategoryViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_HomeViewController : LY_BaseCategoryViewController

@end

NS_ASSUME_NONNULL_END
