//
//  ClanFollowController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/2/4.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "ClanFollowController.h"
#import "JXCategoryView.h"
#import "JXPagerListRefreshView.h"
#import "ListViewController.h"
#import "ClanFollowHeaderView.h"
#import "FamilyModel.h"
#import "SPDCreateClanViewController.h"

@interface ClanFollowController ()<JXCategoryViewDelegate,ClanFollowHeaderViewDelegate>

@property (nonatomic, strong) NSArray <NSString *> *titles;
@property (nonatomic, strong) JXCategoryTitleView *categoryView;
@property (nonatomic, strong) ClanFollowHeaderView * headerView;
@property (nonatomic, strong) NSMutableArray * followingClans;
@property (nonatomic, strong) NSMutableArray * clanHistory;
@property (nonatomic, strong) NSMutableArray * recommendClans;
@property (nonatomic, strong) UICollectionView * collection;
@end


@implementation ClanFollowController

- (void)viewDidLoad {
    [super viewDidLoad];
    
        _titles = @[SPDStringWithKey(@"已关注", nil),SPDStringWithKey(@"最近访问", nil)]; // 文字长度太长不行
        _categoryView = [[JXCategoryTitleView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 45)];
        self.categoryView.titles = self.titles;
        self.categoryView.defaultSelectedIndex = 0;
        self.categoryView.backgroundColor = [UIColor whiteColor];
        self.categoryView.delegate = self;
        self.categoryView.titleSelectedColor = [UIColor colorWithHexString:COMMON_PINK];
        self.categoryView.titleColor = [UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:0.6];
        self.categoryView.titleFont = [UIFont systemFontOfSize:16];
        self.categoryView.titleSelectedFont = [UIFont boldSystemFontOfSize:16];
        self.categoryView.titleColorGradientEnabled = YES;
        self.categoryView.contentScrollViewClickTransitionAnimationEnabled = NO;

        _pagerView = [[JXPagerListRefreshView alloc] initWithDelegate:self];
        self.pagerView.frame = CGRectMake(0, NavHeight, kScreenW, kScreenH - NavHeight - TabBarHeight);
        self.pagerView.mainTableView.gestureDelegate = self;
        [self.view addSubview:self.pagerView];
        self.categoryView.listContainer = (id<JXCategoryViewListContainer>)self.pagerView.listContainerView;
        [self.pagerView setDefaultSelectedIndex:0];
        [self.pagerView reloadData];
        dispatch_async(dispatch_get_main_queue(), ^{
//             [self.pagerView.listContainerView.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
        });
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self requestData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = (self.categoryView.selectedIndex == 0);
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}


#pragma mark - RequestData

- (void)requestData {
    [RequestUtils commonGetRequestUtils:[@{@"user_id": [SPDApiUser currentUser].userId} mutableCopy] bURL:@"list.special" isClan:YES bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        NSDictionary *my_clan = suceess[@"my_clan"];
        if (my_clan && my_clan.count) {
            FamilyModel * model = [FamilyModel initWithDictionary:my_clan];
            self.headerView.familyModel = model;
        }else{
            self.headerView.familyModel = nil;
        }
    } bFailure:^(id  _Nullable failure) {
        
    }];
}


#pragma mark - JXPagerViewDelegate

- (UIView *)tableHeaderViewInPagerView:(JXPagerView *)pagerView {
    return self.headerView;
}

- (NSUInteger)tableHeaderViewHeightInPagerView:(JXPagerView *)pagerView {
    return 155;
}

- (NSUInteger)heightForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    return 50;
}

- (UIView *)viewForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    return self.categoryView;
}

- (NSInteger)numberOfListsInPagerView:(JXPagerView *)pagerView {
    return self.categoryView.titles.count;
}

- (id<JXPagerViewListViewDelegate>)pagerView:(JXPagerView *)pagerView initListAtIndex:(NSInteger)index {
    ListViewController *list = [[ListViewController alloc] init];
    list.title = self.titles[index];
    list.isNeedHeader = YES;
    list.isNeedFooter = NO;
    list.isFollowList = (index == 0);
    return list;
}

#pragma mark -

- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return self.collection;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
//    self.scrollCallback = callback;
}
//
//- (void)listWillAppear {
//    NSLog(@"%@:%@", self.title, NSStringFromSelector(_cmd));
//    [self requestClanNew];
//}
//
//- (void)listDidAppear {
//    NSLog(@"%@:%@", self.title, NSStringFromSelector(_cmd));
//
//}
//
//- (void)listWillDisappear {
//    NSLog(@"%@:%@", self.title, NSStringFromSelector(_cmd));
//}
//
//- (void)listDidDisappear {
//    NSLog(@"%@:%@", self.title, NSStringFromSelector(_cmd));
//}

#pragma mark - JXCategoryViewDelegate

- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}

#pragma mark - JXPagerMainTableViewGestureDelegate

- (BOOL)mainTableViewGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    //禁止categoryView左右滑动的时候，上下和左右都可以滚动
    if (otherGestureRecognizer == self.categoryView.collectionView.panGestureRecognizer) {
        return NO;
    }
    return [gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]];
}

#pragma mark - ClanFollowHeaderView

-(void)didClickedClanFollowHeaderViewCreateClan {
    SPDCreateClanViewController *vc = [[SPDCreateClanViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)didClickedClanFollowHeaderViewJoinClan {
    [self pushToAllClanList];
}

-(void)didClickedClanFollowHeaderViewMyClan:(FamilyModel *)model {
    [self joinChatroomWithClanId:model._id];
}

#pragma mark - Getter

- (ClanFollowHeaderView *) headerView {
    if (!_headerView) {
        _headerView = [[[NSBundle mainBundle]loadNibNamed:@"ClanFollowHeaderView" owner:self options:nil]lastObject];
        _headerView.delegate = self;
        _headerView.frame = CGRectMake(0, 0, kScreenW, 155);
    }
    return _headerView;
}

- (NSMutableArray *)followingClans {
    if (!_followingClans) {
        _followingClans = [NSMutableArray array];
    }
    return _followingClans;
}

- (NSMutableArray *)clanHistory {
    if (!_clanHistory) {
        _clanHistory = [NSMutableArray array];
    }
    return _clanHistory;
}

- (NSMutableArray *)recommendClans {
    if (!_recommendClans) {
        _recommendClans = [NSMutableArray array];
    }
    return _recommendClans;
}


@end
