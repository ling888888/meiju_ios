//
//  HotClanHomeViewController.h
//  SimpleDate
//
//  Created by ling Hou on 2020/9/14.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_BaseCategoryViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HotClanHomeViewController : LY_BaseCategoryViewController

@end

NS_ASSUME_NONNULL_END
