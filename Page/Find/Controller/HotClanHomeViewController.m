//
//  HotClanHomeViewController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/14.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "HotClanHomeViewController.h"
#import "LY_ClanViewController.h"
#import "SPDSearchUserViewController.h"

@interface HotClanHomeViewController ()

@property (nonatomic, strong) LY_ClanViewController *clanVC;

@property (nonatomic, strong) UIButton *searchBtn;


@end

@implementation HotClanHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:0.0];
    [self.view addSubview:self.searchBtn];
    [self.searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(35);
        make.top.mas_equalTo(naviBarHeight - 35);
        make.trailing.mas_equalTo(-20);
    }];
}

- (NSArray *)titleArray {
    return [NSMutableArray arrayWithArray:@[@"聊天室".localized]];
}

- (NSArray<JXPagerViewListViewDelegate> *)VCArray {
    return [NSMutableArray arrayWithArray:@[self.clanVC]];
}

- (LY_ClanViewController *)clanVC {
    if (!_clanVC) {
        _clanVC = [[LY_ClanViewController alloc] init];
    }
    return _clanVC;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:0.0];
    [self.navigationController.navigationBar setHidden:true];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:1.0];
    [self.navigationController.navigationBar setHidden:false];
}

- (UIButton *)searchBtn {
    if (!_searchBtn) {
        _searchBtn = [[UIButton alloc] init];
        [_searchBtn setImage:[UIImage imageNamed:@"ic_shouye_sousuo"] forState:UIControlStateNormal];
        [_searchBtn addTarget:self action:@selector(searchBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _searchBtn;
}

/// 搜索按钮点击事件
- (void)searchBtnAction {
    SPDSearchUserViewController *vc = [[SPDSearchUserViewController alloc] init];
    vc.showSeachBarOnNavigationBar = YES;
    [self.navigationController pushViewController:vc animated:true];
}

@end
