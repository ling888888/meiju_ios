//
//  SPDClanListViewController.h
//  SimpleDate
//
//  Created by 李楠 on 2019/5/10.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SPDClanListViewController : BaseViewController

@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *country_name;

@end

NS_ASSUME_NONNULL_END
