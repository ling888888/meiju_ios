//
//  SPDInvitationEmptyCell.h
//  SimpleDate
//
//  Created by Monkey on 2018/9/20.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SPDInvitationEmptyCellDelegate <NSObject>

-(void)clickedEmptyBtnInInvitationEmptyCell;

@end

@interface SPDInvitationEmptyCell : BaseTableViewCell

@property (nonatomic, weak)id<SPDInvitationEmptyCellDelegate> delegate;

@end
