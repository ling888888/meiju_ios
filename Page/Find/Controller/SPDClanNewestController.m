//
//  SPDClanNewestController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/2/7.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "SPDClanNewestController.h"
#import "SPDClanHotListCell.h"
#import "FamilyModel.h"
#import "ZegoKitManager.h"
#import "JXPagerView.h"

@interface SPDClanNewestController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, SPDClanHotListCellDelegate, UIScrollViewDelegate>

@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *clanArray;
@property (nonatomic, assign) NSInteger page;

@end

@implementation SPDClanNewestController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.collectionView];
    self.page = 0;

}

- (void)requestClanNew {
    NSDictionary *dic = @{@"page": @(self.page)};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"clan.new" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if (self.page == 0) {
            [self.clanArray removeAllObjects];
        }
        
        NSArray *list = suceess[@"list"];
        for (NSDictionary *dic in list) {
            FamilyModel *model = [FamilyModel initWithDictionary:dic];
            __block BOOL exist = NO;
            if (self.page != 0) {
                [self.clanArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if ([model._id isEqualToString:[obj valueForKey:@"_id"]]) {
                        exist = YES;
                        *stop = YES;
                    }
                }];
            }
            if (!exist) {
                [self.clanArray addObject:model];
            }
        }
        [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];

        self.page++;
        
        [self.collectionView.mj_header endRefreshing];
        if (self.collectionView.mj_footer.state == MJRefreshStateRefreshing) {
            [self.collectionView.mj_footer endRefreshingWithCompletionBlock:^{
                self.collectionView.mj_footer.hidden = !list.count;
            }];
        } else {
            self.collectionView.mj_footer.hidden = !list.count;
        }
    } bFailure:^(id  _Nullable failure) {
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
    }];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return self.clanArray.count;
        default:
            return 1;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
     SPDClanHotListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SPDClanHotListCell" forIndexPath:indexPath];
//       cell.isAgentService = self.isAgentService;
       cell.model = self.clanArray[indexPath.row];
       cell.delegate = self;
       return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0: {
            FamilyModel *model = self.clanArray[indexPath.row];
            [self joinChatroomWithClanId:model._id];
            break;
        }
        default:
            break;
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(kScreenW - 16, 90);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, 8, 0, 8);
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback ?: self.scrollCallback(scrollView);
}

#pragma mark -

- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return self.collectionView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}

- (void)listWillAppear {
    NSLog(@"%@:%@", self.title, NSStringFromSelector(_cmd));
    [self requestClanNew];
}

- (void)listDidAppear {
    NSLog(@"%@:%@", self.title, NSStringFromSelector(_cmd));

}

- (void)listWillDisappear {
    NSLog(@"%@:%@", self.title, NSStringFromSelector(_cmd));
}

- (void)listDidDisappear {
    NSLog(@"%@:%@", self.title, NSStringFromSelector(_cmd));
}


#pragma mark - Setters & Getters

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumLineSpacing = 8;
        layout.minimumInteritemSpacing = 8;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - NavHeight - TabBarHeight - 45) collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        [_collectionView registerNib:[UINib nibWithNibName:@"SPDClanHotListCell" bundle:nil] forCellWithReuseIdentifier:@"SPDClanHotListCell"];
        __weak typeof(self) weakSelf = self;
        _collectionView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            weakSelf.page = 0;
            [weakSelf requestClanNew];
        }];
        _collectionView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            [weakSelf requestClanNew];
        }];
        _collectionView.mj_footer.hidden = YES;
    }
    return _collectionView;
}

- (NSMutableArray *)clanArray {
    if (!_clanArray) {
        _clanArray = [NSMutableArray new];
    }
    return _clanArray;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
