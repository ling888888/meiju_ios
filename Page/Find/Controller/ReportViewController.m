//
//  ReportViewController.m
//  SimpleDate
//
//  Created by xu.juvenile on 2016/5/3.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "ReportViewController.h"
#import "ReportUserListCell.h"

@interface ReportViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * dataArray;
@property (nonatomic, assign) NSInteger selectectIndex;
@property (nonatomic, strong) UIButton * reportBtn;
@property (nonatomic, strong) UIView * headerView;
@end

@implementation ReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenW, 50)];
    self.headerView.backgroundColor = SPD_HEXCOlOR(@"#F0F0F0");
    UILabel * label = [UILabel new];
    label.text = @"请选择举报原因".localized;
    label.font = [UIFont mediumFontOfSize:18];
    label.textColor = SPD_HEXCOlOR(@"#1A1A1A");
    [self.headerView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.centerY.mas_equalTo(0);
    }];
    
    self.navigationItem.title = @"举报用户".localized;
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.reportBtn];
    self.reportBtn.userInteractionEnabled = NO;
    self.reportBtn.layer.cornerRadius = 25;
    self.reportBtn.clipsToBounds = YES;
    [self.reportBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(170);
        make.height.mas_equalTo(50);
        make.bottom.mas_equalTo(-91);
    }];
    self.selectectIndex = -1;
    [self requestData];
}

- (void)handle:(UIButton *)sender {
    sender.userInteractionEnabled = NO;
    [self requestReport];
}

- (void)requestReport {
    [RequestUtils POST:URL_NewServer(@"live/live.report.record.save") parameters:@{@"reportedUserId":self.userId,@"keyId":self.dataArray[self.selectectIndex][@"keyId"]} success:^(id  _Nullable suceess) {
        self.reportBtn.userInteractionEnabled = YES;
        [self showTips:@"举报成功".localized];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
        self.reportBtn.userInteractionEnabled = YES;
    }];
}

- (void)requestData {
    [RequestUtils GET:URL_NewServer(@"live/live.report.reason") parameters:@{} success:^(id  _Nullable suceess) {
        for (NSDictionary * dic in suceess[@"reasons"]) {
            [self.dataArray addObject:dic];
        }
        [self.tableView reloadData];
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ReportUserListCell * cell = [ReportUserListCell cellWithTableView:tableView];
    cell.dict = self.dataArray[indexPath.row];
    cell.icon.image = (indexPath.row != self.selectectIndex) ? [UIImage imageNamed:@"ic_jubaoyonghu_weixuan"]:[UIImage imageNamed:@"ic_jubaoyonghu_yixuan"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectectIndex = indexPath.row;
    [tableView reloadData];
    self.reportBtn.userInteractionEnabled = YES;
    [self.reportBtn setBackgroundColor:SPD_HEXCOlOR(COMMON_PINK)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return self.headerView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [UIView new];
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    return _tableView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

- (UIButton *)reportBtn {
    if (!_reportBtn) {
        _reportBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_reportBtn setTitle:@"举报".localized forState:UIControlStateNormal];
        [_reportBtn setBackgroundColor:[UIColor colorWithRed:26/255.0f green:26/255.0f blue:26/255.0f alpha:0.1]];
        _reportBtn.titleLabel.font = [UIFont mediumFontOfSize:18];
        [_reportBtn setTitleColor:SPD_HEXCOlOR(@"#FFFFFF") forState:UIControlStateNormal];
        [_reportBtn addTarget:self action:@selector(handle:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _reportBtn;
}

@end
