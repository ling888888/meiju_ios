//
//  LY_HomeViewController.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/29.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_HomeViewController.h"
#import "LiveApplyAnchorConditionsViewController.h"
#import "LY_ClanViewController.h"
#import "LiveRecommendController.h"
#import "LY_LiveFiringViewController.h"
#import "SPDSearchUserViewController.h"
//#import "Mine.h"
#import "LY_LivePlayMusicView.h"

@interface LY_HomeViewController ()

@property (nonatomic, strong) LiveRecommendController *liveRecommendVC;

@property (nonatomic, strong) LY_ClanViewController *clanVC;
 
//@property (nonatomic, strong) RadioRecommendController *radioRecommendVC;

@property (nonatomic, strong) UIButton *startLiveBtn;

@property (nonatomic, strong) UIButton *searchBtn;

@property (nonatomic, assign) BOOL isAnchor;

@end

@implementation LY_HomeViewController



- (UIButton *)startLiveBtn {
    if (!_startLiveBtn) {
        _startLiveBtn = [[UIButton alloc] init];
        _startLiveBtn.backgroundColor = [UIColor colorWithHexString:@"#00D4A0"];
        [_startLiveBtn setTitle:@"开播".localized forState:UIControlStateNormal];
        _startLiveBtn.titleLabel.font = [UIFont mediumFontOfSize:12];
        _startLiveBtn.layer.cornerRadius = 10;
        _startLiveBtn.layer.masksToBounds = true;
//        _startLiveBtn.hidden = true;
        _startLiveBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 12, 0, 12);
        [_startLiveBtn addTarget:self action:@selector(startLiveBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _startLiveBtn;
}

- (UIButton *)searchBtn {
    if (!_searchBtn) {
        _searchBtn = [[UIButton alloc] init];
        [_searchBtn setImage:[UIImage imageNamed:@"ic_shouye_sousuo"] forState:UIControlStateNormal];
        [_searchBtn addTarget:self action:@selector(searchBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _searchBtn;
}

- (LiveRecommendController *)liveRecommendVC {
    if (!_liveRecommendVC) {
        _liveRecommendVC = [[LiveRecommendController alloc] init];
    }
    return _liveRecommendVC;
}

- (LY_ClanViewController *)clanVC {
    if (!_clanVC) {
        _clanVC = [[LY_ClanViewController alloc] init];
    }
    return _clanVC;
}

//- (RadioRecommendController *)radioRecommendVC {
//    if (!_radioRecommendVC) {
//        _radioRecommendVC = [[RadioRecommendController alloc] init];
//    }
//    return _radioRecommendVC;
//}

- (NSArray *)titleArray {
    return [NSMutableArray arrayWithArray:@[[NSString stringWithFormat:@"%@ ",@"直播".localized]]];
}

- (NSArray<JXPagerViewListViewDelegate> *)VCArray {
    return [NSMutableArray arrayWithArray:@[self.liveRecommendVC]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:0.0];
    
    [self setupUI];
    
    [self setupUIFrame];

    [self setupData];
    
    self.categoryView.defaultSelectedIndex = 0;
    self.pagerView.defaultSelectedIndex = 0;
    
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:0.0];
    [self.navigationController.navigationBar setHidden:true];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:1.0];
    [self.navigationController.navigationBar setHidden:false];
}

- (void)setupUI {
    [self.view addSubview:self.searchBtn];
    [self.view addSubview:self.startLiveBtn];
}

- (void)setupUIFrame {
    [self.searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(35);
        make.top.mas_equalTo(naviBarHeight - 35);
        make.trailing.mas_equalTo(-20);
    }];
    [self.startLiveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.centerY.mas_equalTo(self.searchBtn);
        make.trailing.equalTo(self.searchBtn.mas_leading).offset(-15);;
    }];
}

- (void)setupData {
    [LY_Network getRequestWithURL:LY_Api.mine_info parameters:nil success:^(id  _Nullable response) {
//<<<<<<< HEAD
        NSString * liveId = response[@"liveId"];
        if (liveId.notEmpty) {
            self.isAnchor = true;
        } else {
            self.isAnchor = false;
        }
//        [self updateViewsWithSelectedItemAtIndex:0];
//=======
//        self.mine = [Mine yy_modelWithDictionary:response];
//        self.showStartLiveBtn = mine.liveId.notEmpty;
//        [self updateViewsWithSelectedItemAtIndex:0];
//>>>>>>> acc630a5d1ea06642d619dce75f6f16e25f64fb5
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

/// 开始直播按钮点击事件
- (void)startLiveBtnAction {
    //[MobClick event:@"FM1_10_2_6"];
    if (self.isAnchor) {
        // 开直播
        LY_LiveFiringViewController *liveFiringVC = [[LY_LiveFiringViewController alloc] init];
        [self.navigationController pushViewController:liveFiringVC animated:true];
    } else {
        // 申请主播
        [self.navigationController pushViewController:[LiveApplyAnchorConditionsViewController new] animated:YES];
    }
}

/// 搜索按钮点击事件
- (void)searchBtnAction {
    SPDSearchUserViewController *vc = [[SPDSearchUserViewController alloc] init];
    vc.showSeachBarOnNavigationBar = YES;
    [self.navigationController pushViewController:vc animated:true];
//    LY_LivePlayMusicView *view = [[LY_LivePlayMusicView alloc] init];
//    [view present];
}

- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
//    [self updateViewsWithSelectedItemAtIndex:index];
}

//- (void)updateViewsWithSelectedItemAtIndex:(NSInteger)index {
//    NSString *selectedItemTitle = self.categoryView.titles[index];
//    if ([selectedItemTitle isEqualToString:@"聊天室".localized]) {
//        self.startLiveBtn.hidden = true;
//        [self.searchBtn mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.trailing.mas_equalTo(-10);
//        }];
//        [UIView animateWithDuration:0.5 animations:^{
//            [self.view layoutIfNeeded];
//        }];
//    } else if ([selectedItemTitle isEqualToString:@"直播".localized]) {
//        self.startLiveBtn.hidden = false;
//        [self.searchBtn mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.trailing.mas_equalTo(-10);
//        }];
//        [UIView animateWithDuration:0.5 animations:^{
//            [self.view layoutIfNeeded];
//        }];
//    } else {
//        self.startLiveBtn.hidden = true;
//        [self.searchBtn mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.trailing.mas_equalTo(35);
//        }];
//        [UIView animateWithDuration:0.5 animations:^{
//            [self.view layoutIfNeeded];
//        }];
//    }
//}

@end
