//
//  RankContainerController.h
//  SimpleDate
//
//  Created by 李楠 on 2018/12/11.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "WMPageController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RankContainerController : WMPageController

@property (nonatomic, copy) NSString *selectedType; // @[@"riche", @"charm", @"person", @"clan", @"magic"]

@end

NS_ASSUME_NONNULL_END
