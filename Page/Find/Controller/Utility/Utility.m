//
//  Utility.m
//  MomentKit
//
//  Created by LEA on 2017/12/12.
//  Copyright © 2017年 LEA. All rights reserved.
//

#import "Utility.h"
#import "MomentKit.h"

@implementation Utility

#pragma mark - 时间戳转换
+ (NSString *)getDateFormatByTimestamp:(long long)timestamp
{
    NSDate *dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval nowTimestamp = [dat timeIntervalSince1970] ;
    long long int timeDifference = nowTimestamp - timestamp / 1000;
    long long int secondTime = timeDifference;
    long long int minuteTime = secondTime/60;
    long long int hoursTime = minuteTime/60;
    long long int dayTime = hoursTime/24;
    long long int monthTime = dayTime/30;
    long long int yearTime = monthTime/12;
    
    if (1 <= yearTime) {
        return [NSString stringWithFormat:SPDStringWithKey(@"%lld年前", nil),yearTime];
    }
    else if(1 <= monthTime) {
        return [NSString stringWithFormat:SPDStringWithKey(@"%lld月前", nil),monthTime];
    }
    else if(1 <= dayTime) {
        return [NSString stringWithFormat:SPDStringWithKey(@"%lld天前", nil),dayTime];
    }
    else if(1 <= hoursTime) {
        return [NSString stringWithFormat:SPDStringWithKey(@"%lld小时前", nil),hoursTime];
    }
    else if(1 <= minuteTime) {
        return [NSString stringWithFormat:SPDStringWithKey(@"%lld分钟前", nil),minuteTime];
    }
    else if(1 <= secondTime) {
        return [NSString stringWithFormat:SPDStringWithKey(@"%lld秒前", nil),secondTime];
    }
    else {
        return SPDStringWithKey(@"刚刚", nil);
    }
}

#pragma mark - 获取单张图片的实际size
+ (CGSize)getSingleSize:(CGSize)singleSize
{
    CGFloat max_width = kWidth-150;
    CGFloat max_height = kWidth-130;
    CGFloat image_width = singleSize.width;
    CGFloat image_height = singleSize.height;
    
    CGFloat result_width = 0;
    CGFloat result_height = 0;
    if (image_height/image_width > 3.0) {
        result_height = max_height;
        result_width = result_height/2;
    }  else  {
        result_width = max_width;
        result_height = max_width*image_height/image_width;
        if (result_height > max_height) {
            result_height = max_height;
            result_width = max_height*image_width/image_height;
        }
    }
    return CGSizeMake(result_width, result_height);
}


@end
