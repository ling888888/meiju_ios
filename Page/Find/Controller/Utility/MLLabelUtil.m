//
//  MLLabelUtil.m
//  MomentKit
//
//  Created by LEA on 2017/12/13.
//  Copyright © 2017年 LEA. All rights reserved.
//

#import "MLLabelUtil.h"
#import "MomentKit.h"

@implementation MLLabelUtil

MLLinkLabel *kMLLinkLabel()
{
    MLLinkLabel *_linkLabel = [MLLinkLabel new];
    _linkLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _linkLabel.textColor = [UIColor blackColor];
    _linkLabel.font = kComTextFont;
    _linkLabel.numberOfLines = 0;
    _linkLabel.linkTextAttributes = @{NSForegroundColorAttributeName:kHLTextColor};
    _linkLabel.activeLinkTextAttributes = @{NSForegroundColorAttributeName:kHLTextColor,NSBackgroundColorAttributeName:kHLBgColor};
    _linkLabel.activeLinkToNilDelay = 0.3;
    return _linkLabel;
}

NSMutableAttributedString *kMLLinkLabelAttributedText(id object)
{
    NSMutableAttributedString *attributedText = nil;
    if ([object isKindOfClass:[Comment class]])
    {
        Comment *comment = (Comment *)object;
        NSString *likeString  = [NSString stringWithFormat:@"%@：%@",comment.userName,comment.content];
        attributedText = [[NSMutableAttributedString alloc] initWithString:likeString];
//        [attributedText setAttributes:@{NSFontAttributeName:kComHLTextFont,NSLinkAttributeName:comment.userName}
//                                range:[likeString rangeOfString:comment.userName]];
//        if ([SPDCommonTool currentVersionIsArbic]) {
//            [attributedText addAttributes:@{NSWritingDirectionAttributeName:@[@(NSWritingDirectionRightToLeft | NSTextWritingDirectionOverride)]}
//                                    range:NSMakeRange(0, attributedText.length)];
//        } else {
//            [attributedText addAttributes:@{NSWritingDirectionAttributeName:@[@(NSWritingDirectionLeftToRight | NSTextWritingDirectionOverride)]}
//                                    range:NSMakeRange(0, attributedText.length)];
//        }

    }
    if ([object isKindOfClass:[NSString class]])
    {
        NSString *content = (NSString *)object;
        NSString *likeString = [NSString stringWithFormat:@"[赞] %@",content];
        attributedText = [[NSMutableAttributedString alloc] initWithString:likeString];
//        NSArray *nameList = [content componentsSeparatedByString:@"，"];
//        for (NSString *name in nameList) {
//            [attributedText setAttributes:@{NSFontAttributeName:kComHLTextFont,NSLinkAttributeName:name}
//                                    range:[likeString rangeOfString:name]];
//        }
        
        //添加'赞'的图片
        NSRange range = NSMakeRange(0, 3);
        NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
        textAttachment.image = [UIImage imageNamed:@"moment_like_hl"];
        textAttachment.bounds = CGRectMake(0, -3, textAttachment.image.size.width, textAttachment.image.size.height);
        NSAttributedString *imageStr = [NSAttributedString attributedStringWithAttachment:textAttachment];
        [attributedText replaceCharactersInRange:range withAttributedString:imageStr];
        
//        if ([SPDCommonTool currentVersionIsArbic]) {
//            [attributedText addAttributes:@{NSWritingDirectionAttributeName:@[@(NSWritingDirectionRightToLeft | NSTextWritingDirectionOverride)]}
//                                    range:NSMakeRange(0, attributedText.length)];
//        } else {
//            [attributedText addAttributes:@{NSWritingDirectionAttributeName:@[@(NSWritingDirectionLeftToRight | NSTextWritingDirectionOverride)]}
//                                    range:NSMakeRange(0, attributedText.length)];
//        }
    }
    return attributedText;
}


@end
