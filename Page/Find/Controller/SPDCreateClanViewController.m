//
//  SPDCreateClanViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2018/3/22.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDCreateClanViewController.h"
#import "SPDClanCoverCell.h"
#import "SPDClanCountryCell.h"
#import "SPDClanInputCell.h"
#import "SPDClanSaveCell.h"
#import "ClanCountryModel.h"
#import "SPDClanCountryView.h"
#import "SPDKeywordMatcher.h"
#import "ZegoKitManager.h"

@interface SPDCreateClanViewController ()<UITableViewDataSource, UITableViewDelegate, SPDClanCoverCellDelegate, SPDClanCountryCellDelegate, SPDClanCountryViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIImage *cover;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSMutableDictionary *params;
@property (nonatomic, strong) NSMutableArray *countryArray;
@property (nonatomic, strong) SPDKeywordMatcher *keywordMatcher;
@property (nonatomic, strong) KeywordMap *keywordMap;
@property (nonatomic, strong) NSMutableDictionary *wordDict;

@end

@implementation SPDCreateClanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = SPDStringWithKey(@"创建家族", nil);
    [self.view addSubview:self.tableView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    [self requestCountryList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)KeyboardWillShow:(NSNotification *)notification {
    CGRect keyboardBounds = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    UIViewAnimationCurve curve = [[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    CGRect frame = self.tableView.frame;
    frame.size.height = self.view.frame.size.height - keyboardBounds.size.height;
    CGFloat animationDuration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^{
        [UIView setAnimationCurve:curve];
        [self.tableView setFrame:frame];
        [UIView commitAnimations];
    }];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

- (void)KeyboardWillHide:(NSNotification *)notification {
    UIViewAnimationCurve curve = [[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    CGFloat animationDuration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^{
        [UIView setAnimationCurve:curve];
        [self.tableView setFrame:self.view.bounds];
        [UIView commitAnimations];
    }];
}

- (void)requestCountryList {
    NSDictionary *params = @{@"lang": [SPDApiUser currentUser].lang};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:params] bURL:@"country.list" isClan:YES bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        for (NSDictionary *dic in suceess[@"list"]) {
            ClanCountryModel *model = [ClanCountryModel initWithDictionary:dic];
            if (![[model.country lowercaseString] containsString:@"world"]) {
                [self.countryArray addObject:model];
            }
        }
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestClanCreate {
    NSLog(@"%@", self.params);
    [self.view endEditing:YES];
    if (!self.cover) {
        [self showToast:SPDStringWithKey(@"请上传家族封面", nil)];
    } else if (!self.params[@"country"]) {
        [self showToast:SPDStringWithKey(@"还没有选择国家／地区，快去选择吧～", nil)];
    } else if ([SPDCommonTool isEmpty:self.params[@"clan_name"]]) {
        [self showToast:SPDStringWithKey(@"请输入家族名称", nil)];
    } else if ([SPDCommonTool isEmpty:self.params[@"clan_declar"]]) {
        [self showToast:SPDStringWithKey(@"请输入家族宣言", nil)];
    } else if ([self.keywordMatcher match:self.params[@"clan_name"] withKeywordMap:self.keywordMap]
               || [self.keywordMatcher match:self.params[@"clan_declar"] withKeywordMap:self.keywordMap]
               || [self.keywordMatcher match:self.params[@"greeting"] withKeywordMap:self.keywordMap]) {
        [self showToast:SPDStringWithKey(@"家族名称/宣言/欢迎语有敏感词，请重新输入", nil)];
    } else {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [RequestUtils commonPostMultipartFormDataUtils:self.params bURL:@"create" isClan:YES bAnimatedViewController:self constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            if (self.cover) {
                NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
                NSString *timeStampString = [NSString stringWithFormat:@"%f", timeStamp];
                [formData appendPartWithFileData:UIImageJPEGRepresentation(self.cover, 0.5) name:@"avatar" fileName:[NSString stringWithFormat:@"%@.jpg", timeStampString] mimeType:@"image/jpg"];
            }
        } success:^(id  _Nullable suceess) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self showToast:SPDStringWithKey(@"创建成功", nil)];
            [self joinChatroomWithClanId:suceess[@"clan_id"]];
            ZegoManager.popIndex = self.navigationController.viewControllers.count;
        } failure:^(id  _Nullable failure) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if ([failure isKindOfClass:[NSDictionary class]]) {
                if ([failure[@"code"] integerValue] == 205) {
                    [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
                        
                    } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                        [self pushToRechageController];
                    }];
                } else {
                    [self showToast:failure[@"msg"]];
                }
            }
        }];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0: {
            SPDClanCoverCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SPDClanCoverCell" forIndexPath:indexPath];
            cell.delegate = self;
            if (self.cover) {
                [cell.coverBtn setBackgroundImage:self.cover forState:UIControlStateNormal];
            } else {
                [cell.coverBtn setBackgroundImage:[UIImage imageNamed:@"ic_select_cover"] forState:UIControlStateNormal];
            }
            [cell.selectBtn setTitle:SPDStringWithKey(@"上传家族头像", nil) forState:UIControlStateNormal];
            return cell;
            break;
        }
        case 1: {
            SPDClanCountryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SPDClanCountryCell" forIndexPath:indexPath];
            cell.delegate = self;
            cell.country = self.country;
            return cell;
            break;
        }
        case 2: {
            SPDClanInputCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SPDClanInputCell" forIndexPath:indexPath];
            cell.params = self.params;
            cell.type = @"clan_name";
            return cell;
            break;
        }
        case 3: {
            SPDClanInputCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SPDClanInputCell" forIndexPath:indexPath];
            cell.params = self.params;
            cell.type = @"clan_declar";
            return cell;
            break;
        }
        case 4: {
            SPDClanInputCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SPDClanInputCell" forIndexPath:indexPath];
            cell.params = self.params;
            cell.type = @"greeting";
            return cell;
            break;
        }
        default: {
            SPDClanSaveCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SPDClanSaveCell" forIndexPath:indexPath];
            [cell.actionBtn setTitle:SPDStringWithKey(@"创建", nil) forState:UIControlStateNormal];
            [cell.actionBtn addTarget:self action:@selector(requestClanCreate) forControlEvents:UIControlEventTouchUpInside];
            cell.instructionLabel.text = SPDStringWithKey(@"说明：\n1.可通过花费金币修改家族资料；\n2.修改家族数据要重新进入审核阶段，审核通过才会显示修改后的家族数据；\n3.家族族长可花费金币解散家族", nil);
            return cell;
            break;
        }
    }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 183;
    } else if (indexPath.row == 5) {
        return 225;
    } else {
        return 50;
    }
}

#pragma mark - SPDClanCoverCellDelegate

- (void)selectCover {
    [self.view endEditing:YES];
    [self presentImagePickerController:@""];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = info[UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        self.cover = image;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }];
}

#pragma mark - SPDClanCountryCellDelegate

- (void)selectCountry {
    [self.view endEditing:YES];
    SPDClanCountryView *view = [[[NSBundle mainBundle] loadNibNamed:@"SPDClanCountryView" owner:self options:nil] firstObject];
    view.frame = self.view.bounds;
    view.delegate = self;
    view.countryArray = self.countryArray;
    view.country = self.params[@"country"];
    [self.view addSubview:view];
}

#pragma mark - SPDClanCountryViewDelegate

- (void)didSelectedCountry:(ClanCountryModel *)model {
    self.country = model.country_name;
    [self.params setObject:model.country forKey:@"country"];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - setter / getter

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor colorWithHexString:@"f5f5f5"];
        _tableView.separatorColor = [UIColor colorWithHexString:@"dddddd"];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView registerNib:[UINib nibWithNibName:@"SPDClanCoverCell" bundle:nil] forCellReuseIdentifier:@"SPDClanCoverCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"SPDClanInputCell" bundle:nil] forCellReuseIdentifier:@"SPDClanInputCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"SPDClanSaveCell" bundle:nil] forCellReuseIdentifier:@"SPDClanSaveCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"SPDClanCountryCell" bundle:nil] forCellReuseIdentifier:@"SPDClanCountryCell"];
    }
    return _tableView;
}

- (NSMutableDictionary *)params {
    if (!_params) {
        _params = [NSMutableDictionary dictionary];
        [_params setObject:[SPDApiUser currentUser].userId forKey:@"user_id"];
    }
    return _params;
}

- (NSMutableArray *)countryArray {
    if (!_countryArray) {
        _countryArray = [NSMutableArray array];
    }
    return _countryArray;
}

- (SPDKeywordMatcher *)keywordMatcher {
    if (!_keywordMatcher) {
        _keywordMatcher = [[SPDKeywordMatcher alloc]init];
    }
    return _keywordMatcher;
}

- (KeywordMap *)keywordMap {
    if (!_keywordMap) {
        _keywordMap = [self.keywordMatcher convert:[self.wordDict allKeys]];
    }
    return _keywordMap;
}

- (NSMutableDictionary *)wordDict {
    if (!_wordDict) {
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"word" ofType:@"plist"];
        _wordDict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    }
    return _wordDict;
}

@end
