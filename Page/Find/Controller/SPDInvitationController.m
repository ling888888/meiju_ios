//
//  SPDInvitationController.m
//  SimpleDate
//
//  Created by Monkey on 2018/9/19.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDInvitationController.h"
#import "SPDInvitationHeaderCell.h"
#import "SPDInvitationCodeCell.h"
#import "SPDInvitationCell.h"
#import "SPDInviteModel.h"
#import "SPDInvitationEmptyCell.h"
#import "ZTWAInfoShareView.h"
#import "SPDUMShareUtils.h"

@interface SPDInvitationController ()<UITableViewDelegate,UITableViewDataSource,SPDInvitationEmptyCellDelegate,ZTWAInfoShareViewDelegate>

@property (nonatomic, strong) UITableView * tableView;

@property (nonatomic, strong) ZTWAInfoShareView * shareView;

@property (nonatomic, copy) NSDictionary * dict;

@property (nonatomic, copy) NSMutableDictionary * shareDict;

@property (nonatomic, copy) NSMutableArray * dataArray;

@property (nonatomic,copy) NSNumber * page;

@property (nonatomic, strong) UIButton * ruleBtn;



@end

@implementation SPDInvitationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = SPDStringWithKey(@"我的邀请", nil);
    [self.navigationController.navigationBar vhl_setBackgroundColor:[UIColor whiteColor]];
    self.view.backgroundColor = [UIColor whiteColor];
    self.page = @(0);
    [self requestInviteDetail:self.page];
    [self.tableView reloadData];
    [self.view addSubview:self.ruleBtn];
    [self.view bringSubviewToFront:self.ruleBtn];
    [self requesInviteCountAPI];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)requestInviteDetail:(NSNumber *)page {
    NSMutableDictionary * dict = [NSMutableDictionary new];
    [dict setValue:[SPDApiUser currentUser].userId forKey:@"user_id"];
    [dict setValue:[page stringValue]  forKey:@"page_num"];
    if ([page integerValue] == 0) {
        [self.dataArray removeAllObjects];
    }
    [RequestUtils commonGetRequestUtils:dict bURL:@"invite.detail" isClan:YES bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        
        self.dict = suceess;
        NSArray * list = suceess[@"invite_list"];
        for (NSDictionary * dict in list) {
            SPDInviteModel * model = [SPDInviteModel initWithDictionary:dict];
            [self.dataArray addObject:model];
        }
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        if (list.count < 15) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        } else {
            [self.tableView.mj_footer endRefreshing];
        }
        self.page = @([self.page integerValue] + 1);
        
    } bFailure:^(id  _Nullable failure) {
        [self showToast:failure[@"msg"]];
    }];
}

//-(void)requestUserInfoData {
//
//    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.info.detail" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
//        NSLog(@"请求个人资料成功");
//        NSDictionary *myInfoDict = [NSDictionary dictionaryWithDictionary:suceess];
//        //给分享弹出框邀请码赋值！
//        self.shareDict = myInfoDict[@"share"];
////        self.shareView.invite_link = self.shareDict[@"share_url"];
//        [self.shareView setShareDict:self.shareDict];
//        [self requesInviteCountAPI];
//
//    } bFailure:nil];
//
//}

- (void)requesInviteCountAPI {
    
    NSDictionary * dict = @{
                            @"user_id":[SPDApiUser currentUser].userId
                            };
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dict] bURL:@"invite.count" isClan:YES bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        
        [[NSUserDefaults standardUserDefaults] setObject:@([suceess[@"count"] intValue]) forKey:MYINVITECOUNT];
        self.shareView.invite_count = [suceess[@"count"] intValue];
        
    } bFailure:^(id  _Nullable failure) {
    }];
    
}

#pragma mark - tableView delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count >0 ?  self.dataArray.count + 2 : 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row ==0) {
        SPDInvitationHeaderCell * cell = [SPDInvitationHeaderCell cellWithTableView:tableView];
        cell.dict = self.dict;
        return cell;
    }else if (indexPath.row == 1){
        SPDInvitationCodeCell * cell = [SPDInvitationCodeCell cellWithTableView:tableView];
        cell.gold = @"1000";
        [cell showBtnAnimation:cell.inviteBtn];
        return cell;
    }else if(indexPath.row == 2 && self.dataArray.count ==0 ){
        SPDInvitationEmptyCell * cell = [SPDInvitationEmptyCell cellWithTableView:tableView];
        cell.delegate = self;
        return cell;
    }
    else{
        SPDInvitationCell * cell = [SPDInvitationCell cellWithTableView:tableView];
        cell.model = self.dataArray[indexPath.row -2] ;
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row > 1 && self.dataArray.count >0) {
        SPDInviteModel *model = self.dataArray[indexPath.row - 2];
        [self pushToDetailTableViewController:self userId:model.user_id];
    }else if (indexPath.row == 1 && self.shareDict){
        [self.shareView show];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 104;
    }else if (indexPath.row == 1){
        return 47;
    }else if (indexPath.row == 2 && self.dataArray.count == 0){
        return kScreenH -  NavHeight -TabBarHeight - 104 - 47;
    }else{
        return 90;
    }
}

-(void)clickedEmptyBtnInInvitationEmptyCell {
    if (!self.shareDict) {
        return;
    }
    [self.shareView show];
}

- (void)clickedInviteCount {
}

- (void)infoShareView:(ZTWAInfoShareView *)infoShareView shareToPlatformType:(SPDUMShareType)sharePlatformType {
    
    NSString *shareUrl = infoShareView.shareDict[@"share_url"];
    NSString *sharetitle = infoShareView.shareDict[@"title"];
    NSString *shareContent = infoShareView.shareDict[@"content"];
    NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:infoShareView.shareDict[@"thumbnail"]]];
    UIImage *image = [[UIImage alloc] initWithData:data];
    
//    [[SPDUMShareUtils shareInstance] shareToPlatform:sharePlatformType WithShareUrl:shareUrl andshareTitle:sharetitle andshareContent:shareContent andImageData:image andShareSourece:@"my_info" andSPDUMShareSuccess:^(id shareSuccess) {
//
//        [SPDCommonTool showWindowToast:SPDStringWithKey(@"分享成功", nil)];
//        [self.shareView dismiss];
//
//    } presentedController:self andSPDUMShareFailure:^(id shareFailure) {
//
//    }];
    
}

- (void)clickruleBtn:(UIButton *)btn {
    NSString * str = [NSString stringWithFormat:NEW_INVITE_DETAIL,[SPDCommonTool getFamyLanguage]];
    [self loadWebViewController:self title:SPDStringWithKey(@"邀请", nil) url:str];
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH - NavHeight) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        __weak typeof(self) __weakSelf = self;
        _tableView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            __weakSelf.page=@(0);
            [__weakSelf requestInviteDetail:__weakSelf.page];
        }];
        //上拉加载更多
        _tableView.mj_footer=[HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            [__weakSelf requestInviteDetail:__weakSelf.page];
        }];
        _tableView.tableFooterView = [UIView new];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

- (NSMutableDictionary *)shareDict {
    if (!_shareDict) {
        _shareDict = [NSMutableDictionary new];
    }
    return _shareDict;
}

-(ZTWAInfoShareView *)shareView {
    if (!_shareView) {
        _shareView=[ZTWAInfoShareView shareView];
        _shareView.delegate=self;
    }
    return _shareView;
}

- (UIButton *)ruleBtn {
    if (!_ruleBtn) {
        _ruleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _ruleBtn.frame = CGRectMake(self.view.bounds.size.width - 13 - 64, self.view.bounds.size.height - TabBarHeight - 30 - 64 - NavHeight, 64, 64);
        NSString * lang =@"en";
        if ([[SPDCommonTool getFamyLanguage] isEqualToString:@"zh-Hans"] || [[SPDCommonTool getFamyLanguage] isEqualToString:@"ar"] || [[SPDCommonTool getFamyLanguage] isEqualToString:@"en"]) {
            lang = [SPDCommonTool getFamyLanguage];
        }
        UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"rule_%@",lang]];
        [_ruleBtn setImage:img forState:UIControlStateNormal];
        [_ruleBtn addTarget:self action:@selector(clickruleBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _ruleBtn;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
