//
//  SPDEditClanViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2018/3/28.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDEditClanViewController.h"
#import "SPDClanCoverCell.h"
#import "SPDClanCountryCell.h"
#import "SPDClanInputCell.h"
#import "SPDClanSaveCell.h"
#import "ClanCountryModel.h"
#import "SPDClanCountryView.h"
#import "SPDClanEditTitleCell.h"
#import "SPDKeywordMatcher.h"

@interface SPDEditClanViewController ()<UITableViewDataSource, UITableViewDelegate, SPDClanCoverCellDelegate, SPDClanCountryCellDelegate, SPDClanCountryViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIImage *cover;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSMutableDictionary *params;
@property (nonatomic, strong) NSMutableArray *countryArray;
@property (nonatomic, strong) SPDKeywordMatcher *keywordMatcher;
@property (nonatomic, strong) KeywordMap *keywordMap;
@property (nonatomic, strong) NSMutableDictionary *wordDict;
@property (nonatomic, assign) BOOL noCountry;

@end

@implementation SPDEditClanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = SPDStringWithKey(@"家族资料", nil);
    self.noCountry = [SPDCommonTool isEmpty:self.clanInfo[@"country"]] || [[self.clanInfo[@"country"] lowercaseString] containsString:@"world"];
    [self.view addSubview:self.tableView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
//    [self requestClanEdit];
    [self requestCountryList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)KeyboardWillShow:(NSNotification *)notification {
    CGRect keyboardBounds = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    UIViewAnimationCurve curve = [[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    CGRect frame = self.tableView.frame;
    frame.size.height = self.view.frame.size.height - keyboardBounds.size.height;
    CGFloat animationDuration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^{
        [UIView setAnimationCurve:curve];
        [self.tableView setFrame:frame];
        [UIView commitAnimations];
    }];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

- (void)KeyboardWillHide:(NSNotification *)notification {
    UIViewAnimationCurve curve = [[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    CGFloat animationDuration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^{
        [UIView setAnimationCurve:curve];
        [self.tableView setFrame:self.view.bounds];
        [UIView commitAnimations];
    }];
}

- (void)requestClanEdit {
    NSDictionary *params = @{@"clan_id": self.clan_id};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:params] bURL:@"edit" isClan:YES bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.clanInfo = suceess;
        [self.tableView reloadData];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestCountryList {
    NSDictionary *params = @{@"lang": [SPDApiUser currentUser].lang};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:params] bURL:@"country.list" isClan:YES bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        for (NSDictionary *dic in suceess[@"list"]) {
            ClanCountryModel *model = [ClanCountryModel initWithDictionary:dic];
            if (![[model.country lowercaseString] containsString:@"world"]) {
                [self.countryArray addObject:model];
            }
        }
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)save {
    NSLog(@"%@", self.params);
    [self.view endEditing:YES];
    if (self.noCountry && !self.params[@"country"]) {
        [self showToast:SPDStringWithKey(@"还没有选择国家／地区，快去选择吧～", nil)];
    } else if (self.params[@"clan_name"] && [SPDCommonTool isEmpty:self.params[@"clan_name"]]) {
        [self showToast:SPDStringWithKey(@"请输入家族名称", nil)];
    } else if (self.params[@"clan_declar"] && [SPDCommonTool isEmpty:self.params[@"clan_declar"]]) {
        [self showToast:SPDStringWithKey(@"请输入家族宣言", nil)];
    } else if ([self.keywordMatcher match:self.params[@"clan_name"] withKeywordMap:self.keywordMap]
               || [self.keywordMatcher match:self.params[@"clan_declar"] withKeywordMap:self.keywordMap]) {
        [self showToast:SPDStringWithKey(@"家族名称/宣言/欢迎语有敏感词，请重新输入", nil)];
    } else {
        if ([self.clanInfo[@"edit_cost"] integerValue]) {
            [self presentCommonController:[NSString stringWithFormat:SPDStringWithKey(@"确定花费%@金币修改家族资料", nil), self.clanInfo[@"edit_cost"]] messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
                
            } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                [self requestClanSave];
            }];
        } else {
            [self requestClanSave];
        }
    }
}

- (void)requestClanSave {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [RequestUtils commonPostMultipartFormDataUtils:self.params bURL:@"save" isClan:YES bAnimatedViewController:self constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        if (self.cover) {
            NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
            NSString *timeStampString = [NSString stringWithFormat:@"%f", timeStamp];
            [formData appendPartWithFileData:UIImageJPEGRepresentation(self.cover, 0.5) name:@"avatar" fileName:[NSString stringWithFormat:@"%@.jpg", timeStampString] mimeType:@"image/jpg"];
        }
    } success:^(id  _Nullable suceess) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showToast:SPDStringWithKey(@"修改家族资料已提交，请耐心等待审核～", nil)];
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(id  _Nullable failure) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([failure isKindOfClass:[NSDictionary class]]) {
            if ([failure[@"code"] integerValue] == 205) {
                [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
                    
                } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                    [self pushToRechageController];
                }];
            } else {
                [self showToast:failure[@"msg"]];
            }
        }
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0: {
            SPDClanCoverCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SPDClanCoverCell" forIndexPath:indexPath];
            cell.delegate = self;
            if (self.cover) {
                [cell.coverBtn setBackgroundImage:self.cover forState:UIControlStateNormal];
            } else {
                NSURL *coverURL = [NSURL URLWithString:self.clanInfo[@"avatar"]];
                [cell.coverBtn sd_setBackgroundImageWithURL:coverURL forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"img_portrait_placeholder"]];
            }
            [cell.selectBtn setTitle:SPDStringWithKey(@"点击更换封面", nil) forState:UIControlStateNormal];
            return cell;
            break;
        }
        case 1: {
            SPDClanEditTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SPDClanEditTitleCell" forIndexPath:indexPath];
            return cell;
            break;
        }
        case 2: {
            SPDClanCountryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SPDClanCountryCell" forIndexPath:indexPath];
            cell.delegate = self;
            cell.country = self.country ? : (self.noCountry ? SPDStringWithKey(@"请选择", nil) : self.clanInfo[@"country_name"]);
            return cell;
            break;
        }
        case 3: {
            SPDClanInputCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SPDClanInputCell" forIndexPath:indexPath];
            cell.params = self.params;
            cell.type = @"clan_name";
            cell.text = self.params[@"clan_name"] ? : self.clanInfo[@"name"];
            return cell;
            break;
        }
        case 4: {
            SPDClanInputCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SPDClanInputCell" forIndexPath:indexPath];
            cell.params = self.params;
            cell.type = @"clan_declar";
            cell.text = self.params[@"clan_declar"] ? : self.clanInfo[@"desc"];
            return cell;
            break;
        }
        default: {
            SPDClanSaveCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SPDClanSaveCell" forIndexPath:indexPath];
            [cell.actionBtn setTitle:SPDStringWithKey(@"保存", nil) forState:UIControlStateNormal];
            [cell.actionBtn addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
            cell.instructionLabel.text = SPDStringWithKey(@"说明：\n1.可通过花费金币修改家族资料；\n2.修改家族数据要重新进入审核阶段，审核通过才会显示修改后的家族数据；\n3.家族族长可花费金币解散家族", nil);
            return cell;
            break;
        }
    }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 183;
    } else if (indexPath.row == 1) {
        return 45;
    } else if (indexPath.row == 5) {
        return 225;
    } else {
        return 50;
    }
}

#pragma mark - SPDClanCoverCellDelegate

- (void)selectCover {
    [self.view endEditing:YES];
    [self presentImagePickerController:@""];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = info[UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        self.cover = image;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }];
}

#pragma mark - SPDClanCountryCellDelegate

- (void)selectCountry {
    if (self.noCountry) {
        [self.view endEditing:YES];
        SPDClanCountryView *view = [[[NSBundle mainBundle] loadNibNamed:@"SPDClanCountryView" owner:self options:nil] firstObject];
        view.frame = self.view.bounds;
        view.delegate = self;
        view.countryArray = self.countryArray;
        view.country =  self.params[@"country"];
        [self.view addSubview:view];
    } else {
        [self showToast:SPDStringWithKey(@"已有归属地区不能修改", nil)];
    }
}

#pragma mark - SPDClanCountryViewDelegate

- (void)didSelectedCountry:(ClanCountryModel *)model {
    self.country = model.country_name;
    [self.params setObject:model.country forKey:@"country"];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - setter / getter

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor colorWithHexString:@"f5f5f5"];
        _tableView.separatorColor = [UIColor colorWithHexString:@"dddddd"];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView registerNib:[UINib nibWithNibName:@"SPDClanCoverCell" bundle:nil] forCellReuseIdentifier:@"SPDClanCoverCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"SPDClanInputCell" bundle:nil] forCellReuseIdentifier:@"SPDClanInputCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"SPDClanSaveCell" bundle:nil] forCellReuseIdentifier:@"SPDClanSaveCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"SPDClanCountryCell" bundle:nil] forCellReuseIdentifier:@"SPDClanCountryCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"SPDClanEditTitleCell" bundle:nil] forCellReuseIdentifier:@"SPDClanEditTitleCell"];
    }
    return _tableView;
}

- (NSMutableDictionary *)params {
    if (!_params) {
        _params = [NSMutableDictionary dictionary];
        [_params setObject:[SPDApiUser currentUser].userId forKey:@"user_id"];
        [_params setObject:self.clan_id forKey:@"clan_id"];
    }
    return _params;
}

- (NSMutableArray *)countryArray {
    if (!_countryArray) {
        _countryArray = [NSMutableArray array];
    }
    return _countryArray;
}

- (SPDKeywordMatcher *)keywordMatcher {
    if (!_keywordMatcher) {
        _keywordMatcher = [[SPDKeywordMatcher alloc]init];
    }
    return _keywordMatcher;
}

- (KeywordMap *)keywordMap {
    if (!_keywordMap) {
        _keywordMap = [self.keywordMatcher convert:[self.wordDict allKeys]];
    }
    return _keywordMap;
}

- (NSMutableDictionary *)wordDict {
    if (!_wordDict) {
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"word" ofType:@"plist"];
        _wordDict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    }
    return _wordDict;
}

@end
