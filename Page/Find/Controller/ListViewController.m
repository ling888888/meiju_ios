//
//  ListViewController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/2/5.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "ListViewController.h"
#import "SPDClanHotListCell.h"
#import "FamilyModel.h"

@interface ListViewController ()<UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,SPDClanHotListCellDelegate>

@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, strong) UICollectionView * collectionView;
@property (nonatomic, strong) UIImageView * emptyImageView;
@property (nonatomic, strong) UILabel * emptyLabel;
@property (nonatomic, assign) NSInteger page;
@end

@implementation ListViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"ffffff"];
    [self.view addSubview:self.collectionView];
    [self.view addSubview:self.emptyLabel];
    self.emptyLabel.hidden = YES;
    [self.emptyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_offset(0);
        make.centerY.mas_offset(80);
    }];
     [self.view addSubview:self.emptyImageView];
     self.emptyImageView.hidden = YES;
     [self.emptyImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_offset(0);
        make.width.and.height.mas_equalTo(150);
         make.bottom.equalTo(self.emptyLabel.mas_top).offset(-10);
     }];
    [self requestData];
}

#pragma mark - RequestData

- (void)requestData {
    [RequestUtils commonGetRequestUtils:[@{@"user_id":self.userId,@"page":@(_page)} mutableCopy] bURL:@"my.followclan" isClan:YES bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if (self.page == 0) {
            [self.dataSource removeAllObjects];
            [self.collectionView.mj_header endRefreshing];
        }else{
            [self.collectionView.mj_footer endRefreshing];
        }
        NSArray * list = suceess[@"following_clan"];
        for (NSDictionary *dic in list) {
           FamilyModel *model = [FamilyModel initWithDictionary:dic];
           [self.dataSource addObject:model];
        }
        if (self.dataSource.count == 0) {
            self.emptyImageView.hidden = NO;
            self.emptyLabel.hidden = NO;
            self.collectionView.hidden = YES;
        }
        [self.collectionView reloadData];
        self.page ++;
        self.collectionView.mj_footer.hidden = !list.count;

    } bFailure:^(id  _Nullable failure) {
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
    }];
}

// 暂时用不到
- (void)requestHistory {
     
     NSMutableArray *clan_ids = [DBUtil queryClanHistory];
     NSString *clan_ids_str = clan_ids.count > 0 ? [clan_ids componentsJoinedByString:@","]:@"";
    [RequestUtils commonGetRequestUtils:[@{@"clan_ids": clan_ids_str,@"user_id":[SPDApiUser currentUser].userId} mutableCopy] bURL:@"batch" isClan:YES bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        [self.dataSource removeAllObjects];
        for (NSDictionary *dic in suceess[@"clans"]) {
            FamilyModel *model = [FamilyModel initWithDictionary:dic];
            if ([model.name isEqualToString:@"404"]) {
                [DBUtil deleteClanHistory:model._id];
            } else {
                [self.dataSource addObject:model];
            }
        }
        [self.collectionView reloadData];
     } bFailure:^(id  _Nullable failure) {
         
     }];

}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SPDClanHotListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SPDClanHotListCell" forIndexPath:indexPath];
    cell.model = self.dataSource[indexPath.row];
    cell.delegate = self;
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
   FamilyModel *model = self.dataSource[indexPath.row];
   [self joinChatroomWithClanId:model._id];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake( 5, 8, 0, 8);
}

#pragma mark - SPDClanHotListCellDelegate

- (void)didClickStickBtnWithModel:(FamilyModel *)model {
    [self presentCommonController:SPDStringWithKey(@"确定置顶该家族", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
        NSDictionary *dic = @{@"user_id": [SPDApiUser currentUser].userId, @"clan_id": model._id};
        [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"stick" isClan:YES bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
            [self showToast:SPDStringWithKey(@"置顶成功", nil)];
        } bFailure:^(id  _Nullable failure) {
            if ([failure isKindOfClass:[NSDictionary class]]) {
                if ([failure[@"code"] integerValue] == 2038) {
                    [self showToast:SPDStringWithKey(@"本次置顶次数用完，请1小时候再进行置顶", nil)];
                } else if ([failure[@"code"] integerValue] == 2037) {
                    [self showToast:SPDStringWithKey(@"你不是这个区的客服助手", nil)];
                } else {
                    [self showToast:failure[@"msg"]];
                }
            }
        }];
    }];
}


#pragma mark - getter

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.minimumLineSpacing = 8;
        layout.minimumInteritemSpacing = 0;
        layout.itemSize = CGSizeMake(kScreenW - 16, 90);
        _collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor clearColor];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        [_collectionView registerNib:[UINib nibWithNibName:@"SPDClanHotListCell" bundle:nil] forCellWithReuseIdentifier:@"SPDClanHotListCell"];
        _collectionView.alwaysBounceVertical = YES;
        __weak typeof(self) weakSelf = self;
        _collectionView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            weakSelf.page = 0;
            [weakSelf requestData];
        }];
        _collectionView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            [weakSelf requestData];
        }];
    }
    return _collectionView;
}

- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [NSMutableArray new];
    }
    return _dataSource;
}

- (UIImageView *)emptyImageView {
    if (!_emptyImageView) {
        _emptyImageView = [[UIImageView alloc]init];
        _emptyImageView.image = [UIImage imageNamed:@"img_kongyemian_guanzhujiazu"];
    }
    return _emptyImageView;
}

- (UILabel *)emptyLabel {
    if (!_emptyLabel) {
        _emptyLabel = [[UILabel alloc]init];
        _emptyLabel.text = @"还没有关注的家族".localized;
        _emptyLabel.font = [UIFont systemFontOfSize:15];
        _emptyLabel.textColor = [SPD_HEXCOlOR(@"#1A1A1A") colorWithAlphaComponent:0.6];
    }
    return _emptyLabel;
}

@end
