//
//  SPDInvitationHeaderCell.m
//  SimpleDate
//
//  Created by Monkey on 2018/9/19.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDInvitationHeaderCell.h"

@interface SPDInvitationHeaderCell()

@property (weak, nonatomic) IBOutlet UILabel *inviteTtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *inviteCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *allRewardtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *yestodayTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *allGoldImg;
@property (weak, nonatomic) IBOutlet UILabel *allGoldLabel;
@property (weak, nonatomic) IBOutlet UILabel *yestodayGoldLabel;
@property (weak, nonatomic) IBOutlet UIImageView *yestodayGoldImageView;

@end

@implementation SPDInvitationHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.inviteTtitleLabel.text = SPDStringWithKey(@"已邀请", nil);
    self.allRewardtitleLabel.text = SPDStringWithKey(@"全部奖励:", nil);
    self.yestodayTitleLabel.text = SPDStringWithKey(@"昨日奖励:", nil);


}

- (void)setDict:(NSDictionary *)dict {
    _dict = dict;
    self.allGoldLabel.text =[NSString stringWithFormat:@"%ld",[dict[@"totalcoins"] integerValue]] ;
    if (dict[@"personnums"]) {
        self.inviteCountLabel.text =[NSString stringWithFormat:@"%ld",[dict[@"personnums"] integerValue]];
    }else{
        self.inviteCountLabel.text =@"0";
    }
    
    self.yestodayGoldLabel.text =[NSString stringWithFormat:@"%ld",[dict[@"yesdaycoins"] integerValue]] ;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
