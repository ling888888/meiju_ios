//
//  CommentEditView.m
//  SimpleDate
//
//  Created by 李楠 on 2018/6/28.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "CommentEditView.h"

static void(^staticCompletionBlock)(NSString *content);

@interface CommentEditView ()<UITextFieldDelegate>

@property (nonatomic, strong) UIView *clearView;
@property (nonatomic, strong) UIView *inputView;
@property (nonatomic, strong) UITextField *textField;

@end

@implementation CommentEditView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.clearView];
        [self addSubview:self.inputView];
    }
    return self;
}

+ (void)beginEditingWithCompletion:(void(^)(NSString *content))completionBlock {
    staticCompletionBlock = completionBlock;
    
    CommentEditView *editView;
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    if ([window viewWithTag:180]) {
        editView = (CommentEditView *)[window viewWithTag:180];
        editView.hidden = NO;
    } else {
        editView = [[self alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH)];
        editView.tag = 180;
        [window addSubview:editView];
    }
    [[NSNotificationCenter defaultCenter] addObserver:editView selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
    editView.textField.placeholder = SPDStringWithKey(@"点击输入评论", nil);
    [editView.superview bringSubviewToFront:editView];
    [editView.textField becomeFirstResponder];
}

- (void)endEditing {
    [self.textField resignFirstResponder];
    self.textField.text = @"";
    self.inputView.frame = CGRectMake(0, kScreenH, kScreenW, 50);
    self.hidden = YES;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)keyboardWillAppear:(NSNotification *)noti {
    CGFloat duration = [noti.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGRect rect = [noti.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat y = rect.origin.y;
    [UIView animateWithDuration:duration animations:^{
        CGRect rect = _inputView.frame;
        rect.origin.y = y - 50;
        self.inputView.frame = rect;
    }];
}

- (void)tapClearView:(UITapGestureRecognizer *)tap {
    [self endEditing];
}

- (void)editingChanged:(UITextField *)sender {
    NSInteger textLimit = 1500;
    if (sender.text.length > textLimit) {
        UITextRange *markedRange = [sender markedTextRange];
        if (markedRange) {
            return;
        }
        NSRange range = [sender.text rangeOfComposedCharacterSequenceAtIndex:textLimit];
        sender.text = [sender.text substringToIndex:range.location];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (![SPDCommonTool isEmpty:self.textField.text]) {
        staticCompletionBlock(textField.text);
        [self endEditing];
    }
    return YES;
}

#pragma mark - setters and getters

- (UIView *)clearView {
    if (!_clearView) {
        _clearView = [[UIView alloc] initWithFrame:self.frame];
        _clearView.backgroundColor = [UIColor clearColor];
        [_clearView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClearView:)]];
    }
    return _clearView;
}

- (UIView *)inputView {
    if (!_inputView) {
        _inputView = [[UIView alloc] initWithFrame:CGRectMake(0, kScreenH, kScreenW, 50)];
        _inputView.backgroundColor = [UIColor whiteColor];
        
        UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, 1)];
        separator.backgroundColor = [UIColor colorWithHexString:@"e6e6e6"];
        [_inputView addSubview:separator];
        
        [_inputView addSubview:self.textField];
    }
    return _inputView;
}

- (UITextField *)textField {
    if (!_textField) {
        _textField = [[UITextField alloc] initWithFrame:CGRectMake(10, 10, kScreenW - 20, 30)];
        _textField.font = [UIFont systemFontOfSize:13];
        _textField.borderStyle = UITextBorderStyleRoundedRect;
        _textField.returnKeyType = UIReturnKeySend;
        _textField.enablesReturnKeyAutomatically = YES;
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.delegate = self;
        [_textField addTarget:self action:@selector(editingChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _textField;
}

@end
