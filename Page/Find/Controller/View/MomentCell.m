//
//  MomentCell.m
//  MomentKit
//
//  Created by LEA on 2017/12/14.
//  Copyright © 2017年 LEA. All rights reserved.
//

#import "MomentCell.h"
#import "MomentKit.h"
#import "Utility.h"
#import "MLLabelUtil.h"
#import "UIView+Geometry.h"

#pragma mark - ------------------ 动态 ------------------

// 最大高度限制
CGFloat maxLimitHeight = 0;

@implementation MomentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI
{
    self.contentView.backgroundColor = [UIColor whiteColor];
    // 头像视图
    self.headImageView = [[UIImageView alloc] init];
    self.headImageView.backgroundColor = [UIColor whiteColor];
    self.headImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.headImageView.layer.masksToBounds = YES;
    self.headImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapHeadImageView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickUser:)];
    [self.headImageView addGestureRecognizer:tapHeadImageView];
    [self.contentView addSubview:self.headImageView];
    [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(10);
        make.top.mas_equalTo(kBlank);
        make.width.and.height.mas_equalTo(kFaceWidth);
    }];
    // 名字视图
    _nameLab = [[UILabel alloc] init];
    _nameLab.userInteractionEnabled = YES;
    _nameLab.font = [UIFont boldSystemFontOfSize:17.0];
    _nameLab.textColor = kHLTextColor;
    _nameLab.backgroundColor = [UIColor whiteColor];
    UITapGestureRecognizer *tapNameLab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickUser:)];
    [_nameLab addGestureRecognizer:tapNameLab];
    [self.contentView addSubview:_nameLab];
    [self.nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.headImageView.mas_trailing).with.offset(10);
        make.top.mas_equalTo(self.headImageView.mas_top);
        make.height.mas_equalTo(20);
    }];
    // 靓号
    self.specialNumImageView = [[UIImageView alloc] init];
    self.specialNumImageView.opaque = YES;
    self.specialNumImageView.backgroundColor = [UIColor whiteColor];
    self.specialNumImageView.hidden = YES;
    [self.contentView addSubview:self.specialNumImageView];
    [self.specialNumImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.nameLab.mas_trailing).with.offset(10);
        make.centerY.mas_equalTo(self.nameLab.mas_centerY);
        make.width.mas_equalTo(22);
        make.height.mas_equalTo(13);
    }];
    // 代理客服
    self.agentServiceImageView = [[UIImageView alloc] init];
    self.agentServiceImageView.opaque = YES;
    self.agentServiceImageView.backgroundColor = [UIColor whiteColor];
    self.agentServiceImageView.hidden = YES;
    [self.contentView addSubview:self.agentServiceImageView];
    [self.agentServiceImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.specialNumImageView.mas_trailing).with.offset(10);
        make.trailing.mas_lessThanOrEqualTo(-10);
        make.centerY.mas_equalTo(self.specialNumImageView.mas_centerY);
        make.width.mas_equalTo(22);
        make.height.mas_equalTo(13);
    }];
    // 正文视图
    _linkLabel = [[UILabel alloc] init];
    _linkLabel.backgroundColor = [UIColor whiteColor];
    _linkLabel.font = kTextFont;
    _linkLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _linkLabel.textColor = [UIColor blackColor];
    _linkLabel.numberOfLines = 0;
    [self.contentView addSubview:_linkLabel];
    [self.linkLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.nameLab.mas_leading);
        make.top.mas_equalTo(self.nameLab.mas_bottom).with.offset(kPaddingValue);
        make.width.mas_equalTo(kTextWidth);
        make.height.mas_equalTo(0);
    }];
    // 查看'全文'按钮
    _showAllBtn = [[UIButton alloc] init];
    _showAllBtn.opaque = YES;
    _showAllBtn.backgroundColor = [UIColor whiteColor];
    _showAllBtn.titleLabel.font = kTextFont;
    _showAllBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_showAllBtn setTitle:SPDStringWithKey(@"全文", nil) forState:UIControlStateNormal];
    [_showAllBtn setTitleColor:kHLTextColor forState:UIControlStateNormal];
    [_showAllBtn addTarget:self action:@selector(fullTextClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_showAllBtn];
    [self.showAllBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.linkLabel.mas_leading);
        make.top.mas_equalTo(self.linkLabel.mas_bottom).with.offset(kArrowHeight);
        make.height.mas_equalTo(kMoreLabHeight);
    }];
    // 图片区
    _imageListView = [[MMImageListView alloc] initWithFrame:CGRectZero];
    _imageListView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:_imageListView];
    [self.imageListView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.linkLabel.mas_leading);
        make.top.mas_equalTo(self.linkLabel.mas_bottom).with.offset(0);
        make.width.mas_equalTo(kTextWidth);
        make.height.mas_equalTo(0);
    }];
    // 位置视图
//    _locationLab = [[UILabel alloc] init];
//    _locationLab.textColor = [UIColor colorWithRed:0.43 green:0.43 blue:0.43 alpha:1.0];
//    _locationLab.font = [UIFont systemFontOfSize:13.0f];
//    [self.contentView addSubview:_locationLab];
    // 时间视图
    _timeLab = [[UILabel alloc] init];
    _timeLab.backgroundColor = [UIColor whiteColor];
    _timeLab.textColor = [UIColor colorWithRed:0.43 green:0.43 blue:0.43 alpha:1.0];
    _timeLab.font = [UIFont systemFontOfSize:13.0f];
    [self.contentView addSubview:_timeLab];
    [self.timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.linkLabel.mas_leading);
        make.top.mas_equalTo(self.imageListView.mas_bottom).with.offset(kPaddingValue);
        make.height.mas_equalTo(kTimeLabelH);
    }];
    // 删除视图
    _deleteBtn = [[UIButton alloc] init];
    _deleteBtn.opaque = YES;
    _deleteBtn.backgroundColor = [UIColor whiteColor];
    _deleteBtn.titleLabel.font = [UIFont systemFontOfSize:13.0f];
    [_deleteBtn setTitle:SPDStringWithKey(@"删除", nil) forState:UIControlStateNormal];
    [_deleteBtn setTitleColor:kHLTextColor forState:UIControlStateNormal];
    [_deleteBtn addTarget:self action:@selector(deleteMoment:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_deleteBtn];
    [self.deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.timeLab.mas_trailing).with.offset(10);
        make.top.mas_equalTo(self.timeLab.mas_top);
        make.height.mas_equalTo(self.timeLab.mas_height);
    }];
    // 点赞按钮
    self.likeBtn = [[UIButton alloc] init];
    self.likeBtn.opaque = YES;
    self.likeBtn.backgroundColor = [UIColor whiteColor];
    [self.likeBtn setImage:[UIImage imageNamed:@"dy_like"] forState:UIControlStateNormal];
    [self.likeBtn setImage:[UIImage imageNamed:@"dy_like_light"] forState:UIControlStateSelected];
    [self.likeBtn addTarget:self action:@selector(clickLikeBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.likeBtn];
    [self.likeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(1);
        make.top.mas_equalTo(self.timeLab.mas_top).with.offset(-5);
        make.width.mas_equalTo(36);
        make.height.mas_equalTo(25);
    }];
    // 评论按钮
    self.commentBtn = [[UIButton alloc] init];
    self.commentBtn.opaque = YES;
    self.commentBtn.backgroundColor = [UIColor whiteColor];
    [self.commentBtn setImage:[UIImage imageNamed:@"dy_comment"] forState:UIControlStateNormal];
    [self.commentBtn addTarget:self action:@selector(clickCommentBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.commentBtn];
    [self.commentBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(self.likeBtn.mas_leading).with.offset(1);
        make.top.mas_equalTo(self.likeBtn.mas_top);
        make.width.mas_equalTo(self.likeBtn.mas_width);
        make.height.mas_equalTo(self.likeBtn.mas_height);
    }];
    // 代理客服 屏蔽按钮
    self.hideBtn = [[UIButton alloc] init];
    self.hideBtn.opaque = YES;
    self.hideBtn.backgroundColor = [UIColor whiteColor];
    [self.hideBtn setImage:[UIImage imageNamed:@"dy_hide"] forState:UIControlStateNormal];
    [self.hideBtn addTarget:self action:@selector(clickHideBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.hideBtn];
    [self.hideBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(self.commentBtn.mas_leading).with.offset(1);
        make.top.mas_equalTo(self.commentBtn.mas_top);
        make.width.mas_equalTo(self.commentBtn.mas_width);
        make.height.mas_equalTo(self.commentBtn.mas_height);
    }];
    // 评论视图
    _bgImageView = [[UIImageView alloc] init];
    _bgImageView.backgroundColor = [UIColor whiteColor];
    UIImage *image = [UIImage imageNamed:@"comment_bg"].adaptiveRtl;
    _bgImageView.image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(10, 30, 10, 30) resizingMode:UIImageResizingModeStretch];
    [self.contentView addSubview:_bgImageView];
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.linkLabel.mas_leading);
        make.top.mas_equalTo(self.timeLab.mas_bottom).with.offset(kPaddingValue);
        make.trailing.mas_equalTo(-kRightMargin);
        make.height.mas_equalTo(0);
    }];
    _commentView = [[UIView alloc] init];
    _commentView.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
    [self.contentView addSubview:_commentView];
    [self.commentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.linkLabel.mas_leading);
        make.top.mas_equalTo(self.timeLab.mas_bottom).with.offset(kPaddingValue + kArrowHeight);
        make.trailing.mas_equalTo(-kRightMargin);
        make.height.mas_equalTo(0);
    }];
    // 点赞列表
    self.likeLabel = kMLLinkLabel();
    self.likeLabel.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
    self.likeLabel.delegate = self;
    [self.commentView addSubview:self.likeLabel];
    // 点赞列表分割线
    self.likeLableBottomLine = [[UIView alloc] init];
    self.likeLableBottomLine.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.3];
    [self.commentView addSubview:self.likeLableBottomLine];
    // 最大高度限制
    maxLimitHeight = _linkLabel.font.lineHeight * 6.1;
}

#pragma mark - setter / getter
- (void)setMoment:(Moment *)moment
{
    _moment = moment;
    // 头像
    [_headImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _moment.avatar]]];
    // 昵称
    _nameLab.text = moment.userName;
    // 靓号、代理客服
    if (moment.specialNum && moment.is_agent_gm) {
        self.specialNumImageView.hidden = NO;
        self.specialNumImageView.image = [UIImage imageNamed:@"specialNum_icon_13"];
        self.agentServiceImageView.hidden = NO;
        self.agentServiceImageView.image = [UIImage imageNamed:@"agent_service_13"];
    } else {
        self.agentServiceImageView.hidden = YES;
        if (moment.specialNum) {
            self.specialNumImageView.hidden = NO;
            self.specialNumImageView.image = [UIImage imageNamed:@"specialNum_icon_13"];
        } else if (moment.is_agent_gm) {
            self.specialNumImageView.hidden = NO;
            self.specialNumImageView.image = [UIImage imageNamed:@"agent_service_13"];
        } else {
            self.specialNumImageView.hidden = YES;
        }
    }
    // 正文
    _showAllBtn.hidden = YES;
    _linkLabel.hidden = YES;
    CGFloat topOffset;
    if ([moment.text length]) {
        _linkLabel.hidden = NO;
        _linkLabel.text = moment.text;
        if (_moment.moreThanLimit) {
            if (!_moment.isFullText) {
                [self.showAllBtn setTitle:SPDStringWithKey(@"全文", nil) forState:UIControlStateNormal];
            } else {
                [self.showAllBtn setTitle:SPDStringWithKey(@"收起", nil) forState:UIControlStateNormal];
            }
            _showAllBtn.hidden = NO;
        }
        [self.linkLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(_moment.textHeight);
        }];

        if (_showAllBtn.hidden) {
            topOffset = kPaddingValue;
        } else {
            topOffset = kArrowHeight + kMoreLabHeight + kPaddingValue;
        }
    } else {
        [self.linkLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(0);
        }];
        topOffset = 0;
    }
    // 图片
    _imageListView.moment = moment;
    if (moment.images.count > 0) {
        self.imageListView.hidden = NO;
        [self.imageListView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.linkLabel.mas_bottom).with.offset(topOffset);
            make.height.mas_equalTo(_moment.imageHeight);
        }];
    }  else {
        self.imageListView.hidden = YES;
        [self.imageListView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.linkLabel.mas_bottom).with.offset(topOffset);
            make.height.mas_equalTo(0);
        }];
    }
    // 时间
    _timeLab.text = [NSString stringWithFormat:@"%@", [Utility getDateFormatByTimestamp:moment.time.longLongValue]];
    // 删除按钮
    _deleteBtn.hidden = !([_moment.userID isEqualToString:[SPDApiUser currentUser].userId] || self.isService);
    // 点赞按钮
    self.likeBtn.selected = _moment.isLike;
    // 处理赞
    CGFloat top = 0;
    CGFloat width = kWidth - kRightMargin - 60;
    if (moment.likes.count) {
        self.likeLabel.hidden = NO;
        self.likeLableBottomLine.hidden = NO;
        self.likeLabel.attributedText = _moment.praiseNameList;
        self.likeLabel.frame = CGRectMake(5, 8, kTextWidth, _moment.likesHeight);
        // 分割线
        self.likeLableBottomLine.frame = CGRectMake(0, self.likeLabel.bottom + 7, width, 0.5);
        // 更新
        top = _moment.likesHeight + 15;
    } else {
        self.likeLabel.hidden = YES;
        self.likeLableBottomLine.hidden = YES;
    }
    // 处理评论
    NSInteger needsToAddCount = moment.comments.count > self.commentLabelsArray.count ? (moment.comments.count - self.commentLabelsArray.count) : 0;
    for (int i = 0; i < needsToAddCount; i++) {
        CommentLabel *label = [[CommentLabel alloc] init];
        [label setDidClickText:^(Comment *comment) {
            if ([self.delegate respondsToSelector:@selector(didSelectComment:)]) {
                [self.delegate didSelectComment:comment];
            }
        }];
        [label setDidClickLinkText:^(MLLink *link, NSString *linkText) {
            if ([self.delegate respondsToSelector:@selector(didClickLink:linkText:momentCell:)]) {
                [self.delegate didClickLink:link linkText:linkText momentCell:self];
            }
        }];
        [_commentView addSubview:label];
        [self.commentLabelsArray addObject:label];
    }
    
    for (int i = 0; i < self.commentLabelsArray.count; i++) {
        CommentLabel *label = self.commentLabelsArray[i];
        if (i < moment.comments.count) {
            label.hidden = NO;
            label.frame = CGRectMake(0, top + 0.5, width, 0);
            label.comment = moment.comments[i];
            top += label.height;
        } else {
            label.hidden = YES;
        }
    }
    // 更新UI
    if (top > 0) {
        self.bgImageView.hidden = NO;
        self.commentView.hidden = NO;
        [self.bgImageView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(top + kArrowHeight);
        }];
        [self.commentView mas_updateConstraints:^(MASConstraintMaker *make) {
             make.height.mas_equalTo(top);
        }];
    } else {
        self.bgImageView.hidden = YES;
        self.commentView.hidden = YES;
    }
}

- (NSMutableArray *)commentLabelsArray {
    if (!_commentLabelsArray) {
        _commentLabelsArray = [NSMutableArray array];
    }
    return _commentLabelsArray;
}

#pragma mark - 点击事件
// 查看全文/收起
- (void)fullTextClicked:(UIButton *)bt
{
    _showAllBtn.titleLabel.backgroundColor = kHLBgColor;
    dispatch_time_t when = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC));
    dispatch_after(when, dispatch_get_main_queue(), ^{
        _showAllBtn.titleLabel.backgroundColor = [UIColor whiteColor];
        _moment.isFullText = !_moment.isFullText;
        if ([self.delegate respondsToSelector:@selector(didSelectFullText:)]) {
            [self.delegate didSelectFullText:self];
        }
    });
}

// 点击头像或昵称
- (void)clickUser:(UITapGestureRecognizer *)gesture
{
    if ([self.delegate respondsToSelector:@selector(didClickUser:)]) {
        [self.delegate didClickUser:self];
    }
}

// 删除动态
- (void)deleteMoment:(UIButton *)bt
{
    _deleteBtn.titleLabel.backgroundColor = [UIColor lightGrayColor];
    dispatch_time_t when = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC));
    dispatch_after(when, dispatch_get_main_queue(), ^{
        _deleteBtn.titleLabel.backgroundColor = [UIColor whiteColor];
        if ([self.delegate respondsToSelector:@selector(didDeleteMoment:)]) {
            [self.delegate didDeleteMoment:self];
        }
    });
}

- (void)clickCommentBtn:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didAddComment:)]) {
        [self.delegate didAddComment:self];
    }
}

- (void)clickLikeBtn:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didLikeMoment:)]) {
        [self.delegate didLikeMoment:self];
    }
}

- (void)clickHideBtn:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didHideMoment:)]) {
        [self.delegate didHideMoment:self];
    }
}

#pragma mark - MLLinkLabelDelegate
- (void)didClickLink:(MLLink *)link linkText:(NSString *)linkText linkLabel:(MLLinkLabel *)linkLabel
{
    // 点击动态正文或者赞高亮
    if ([self.delegate respondsToSelector:@selector(didClickLink:linkText:momentCell:)]) {
        [self.delegate didClickLink:link linkText:linkText momentCell:self];
    }
}
@end

#pragma mark - ------------------ 评论 ------------------
@implementation CommentLabel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
        _linkLabel = kMLLinkLabel();
        _linkLabel.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
        _linkLabel.delegate = self;
        [self addSubview:_linkLabel];
    }
    return self;
}

#pragma mark - Setter
- (void)setComment:(Comment *)comment
{
    _comment = comment;
    _linkLabel.attributedText = comment.text;
    _linkLabel.frame = CGRectMake(5, 3, kTextWidth, comment.height);
    self.height = comment.height + 5;
}

#pragma mark - MLLinkLabelDelegate
- (void)didClickLink:(MLLink *)link linkText:(NSString *)linkText linkLabel:(MLLinkLabel *)linkLabel
{
    if (self.didClickLinkText) {
        self.didClickLinkText(link,linkText);
    }
}

#pragma mark - 点击
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.backgroundColor = kHLBgColor;
    self.linkLabel.backgroundColor = kHLBgColor;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    dispatch_time_t when = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC));
    dispatch_after(when, dispatch_get_main_queue(), ^{
        self.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
        self.linkLabel.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
        if (self.didClickText) {
            self.didClickText(_comment);
        }
    });
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    self.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
    self.linkLabel.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
}

@end
