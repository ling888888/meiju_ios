//
//  ImageCollectionViewCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/6/28.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ImageCollectionViewCellDelegate <NSObject>

@optional

- (void)didClickDeleteBtnWithImage:(UIImage *)image;

@end

@interface ImageCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

@property (nonatomic, weak) id<ImageCollectionViewCellDelegate> delegate;

@end
