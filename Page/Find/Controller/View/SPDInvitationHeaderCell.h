//
//  SPDInvitationHeaderCell.h
//  SimpleDate
//
//  Created by Monkey on 2018/9/19.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPDInvitationHeaderCell : BaseTableViewCell

@property (nonatomic , copy) NSDictionary * dict;

@end
