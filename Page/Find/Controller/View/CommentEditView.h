//
//  CommentEditView.h
//  SimpleDate
//
//  Created by 李楠 on 2018/6/28.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentEditView : UIView

+ (void)beginEditingWithCompletion:(void(^)(NSString *content))completionBlock;

@end
