//
//  ImageCollectionViewCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/6/28.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "ImageCollectionViewCell.h"

@implementation ImageCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (IBAction)clickDeleteBtn:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickDeleteBtnWithImage:)]) {
        [self.delegate didClickDeleteBtnWithImage:self.imageView.image];
    }
}

@end
