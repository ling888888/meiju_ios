//
//  SPDCountryListViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2018/3/30.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDCountryListViewController.h"
#import "ClanCountryModel.h"
#import "SPDCountryListCell.h"
#import "SPDClanListViewController.h"

@interface SPDCountryListViewController ()<UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *countryArray;

@end

@implementation SPDCountryListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = SPDStringWithKey(@"地区", nil);
    [self.view addSubview:self.collectionView];
    [self requestCountryList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)requestCountryList {
    NSDictionary *params = @{@"lang": [SPDApiUser currentUser].lang};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:params] bURL:@"country.list" isClan:YES bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        for (NSDictionary *dic in suceess[@"list"]) {
            ClanCountryModel *model = [ClanCountryModel initWithDictionary:dic];
            [self.countryArray addObject:model];
        }
        [self.collectionView reloadData];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.countryArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SPDCountryListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SPDCountryListCell" forIndexPath:indexPath];
    cell.model = self.countryArray[indexPath.row];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    ClanCountryModel *model = self.countryArray[indexPath.row];
    SPDClanListViewController *vc = [SPDClanListViewController new];
    vc.country = model.country;
    vc.country_name = model.country_name;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - setter / getter

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(93, 106);
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        layout.sectionInset = UIEdgeInsetsMake(15, 1.5, 0, 1.5);
        _collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        [_collectionView registerNib:[UINib nibWithNibName:@"SPDCountryListCell" bundle:nil] forCellWithReuseIdentifier:@"SPDCountryListCell"];
    }
    return _collectionView;
}

- (NSMutableArray *)countryArray {
    if (!_countryArray) {
        _countryArray = [NSMutableArray array];
    }
    return _countryArray;
}

@end
