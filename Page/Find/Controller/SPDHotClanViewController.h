//
//  SPDHotClanViewController.h
//  SimpleDate
//
//  Created by 李楠 on 2018/1/3.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "BaseViewController.h"
#import "JXPagerView.h"
@interface SPDHotClanViewController : BaseViewController<JXPagerViewListViewDelegate>

@end
