//
//  LY_ClanFollowViewController.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/9.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_BaseCollectionViewController.h"
#import "JXPagerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_ClanFollowViewController : LY_BaseCollectionViewController <JXPagerViewListViewDelegate>

@end

NS_ASSUME_NONNULL_END
