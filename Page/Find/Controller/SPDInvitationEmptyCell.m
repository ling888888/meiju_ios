//
//  SPDInvitationEmptyCell.m
//  SimpleDate
//
//  Created by Monkey on 2018/9/20.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDInvitationEmptyCell.h"

@interface SPDInvitationEmptyCell()
@property (weak, nonatomic) IBOutlet UILabel *tipsLabel;
@property (weak, nonatomic) IBOutlet UIButton *inviteBtn;

@end

@implementation SPDInvitationEmptyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.tipsLabel.text = SPDStringWithKey(@"您还没有邀请好友，快去邀请好友赚金币吧", nil);
    [self.inviteBtn setTitle:SPDStringWithKey(@"去邀请", nil) forState:UIControlStateNormal];
    [self.inviteBtn setContentEdgeInsets:UIEdgeInsetsMake(-5, 0, 0, 0)];
}
- (IBAction)inviteBtnClicked:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedEmptyBtnInInvitationEmptyCell)]) {
        [self.delegate clickedEmptyBtnInInvitationEmptyCell];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
