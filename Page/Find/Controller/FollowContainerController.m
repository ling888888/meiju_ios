//
//  FollowContainerController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/5/7.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "FollowContainerController.h"
#import "LY_LiveFollowViewController.h"
#import "ListViewController.h"
@interface FollowContainerController ()

@property (nonatomic, copy) NSArray *titleArray;
@property (nonatomic, strong) LY_LiveFollowViewController * follow;// 主播
@property (nonatomic, strong) ListViewController * clan; //家族

@end

@implementation FollowContainerController

- (void)viewDidLoad {
    [super viewDidLoad];
        
    self.showOnNavigationBar = YES;
    self.menuViewLayoutMode = WMMenuViewLayoutModeCenter;
    self.menuViewStyle = WMMenuViewStyleLine;
    self.progressColor = SPD_HEXCOlOR(COMMON_PINK);
    self.progressHeight = 2;
    self.progressWidth = 40;
    self.menuItemWidth = 95;
    self.titleSizeNormal = 18;
    self.titleSizeSelected = 21;
    self.titleColorNormal = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.8];
    self.titleColorSelected = SPD_HEXCOlOR(@"#1A1A1A");
    self.scrollEnable = NO;
    self.titleArray = kIsMirroredLayout ? @[@"家族", @"主播"]: @[@"主播", @"家族"];
    self.selectIndex = kIsMirroredLayout ? 1:0;
    [self reloadData];
}

#pragma mark - WMPageControllerDataSource

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.titleArray.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    return SPDStringWithKey(self.titleArray[index], nil);
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    if ([SPDCommonTool currentVersionIsArbic]) {
        switch (index) {
            case 0:
                return self.clan;
            default:
                return self.follow;
        }
    } else {
        switch (index) {
            case 0:
                return self.follow;
            default:
                return self.clan;
        }
    }
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    return self.view.bounds;
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    CGFloat x;
    if ([DEVICE_VERSION_NAME floatValue] >= 11.0) {
        x = 0;
    } else {
        x = (kScreenW - self.menuItemWidth * self.titleArray.count) / 2;
    }
    return CGRectMake(x, 0, self.menuItemWidth * self.titleArray.count, self.navigationController.navigationBar.frame.size.height);
}

- (ListViewController *)clan {
    if (!_clan) {
        _clan = [[ListViewController alloc]init];
        _clan.userId = self.userId;
    }
    return _clan;
}

- (LY_LiveFollowViewController *)follow {
    if (!_follow) {
        _follow = [[LY_LiveFollowViewController alloc]init];
        _follow.userId = self.userId;
    }
    return _follow;
}


@end
