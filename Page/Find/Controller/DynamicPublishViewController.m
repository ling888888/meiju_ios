//
//  DynamicPublishViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2018/6/28.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "DynamicPublishViewController.h"
#import "LDTextView.h"
#import "ZLPhoto.h"
#import "ImageCollectionViewCell.h"

@interface DynamicPublishViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, ImageCollectionViewCellDelegate, UITextViewDelegate>

@property (nonatomic, strong) LDTextView *textView;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation DynamicPublishViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = SPDStringWithKey(@"发动态", nil);
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:SPDStringWithKey(@"取消", nil) style:UIBarButtonItemStylePlain target:self action:@selector(clickLeftBarButtonItem:)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:SPDStringWithKey(@"发布", nil) style:UIBarButtonItemStylePlain target:self action:@selector(clickRightBarButtonItem:)];
    self.navigationItem.rightBarButtonItem.enabled = NO;
    if (@available(iOS 11.0, *)) {
        self.textView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [self.view addSubview:self.textView];
    [self.view addSubview:self.collectionView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private methods

- (void)requestDynamicPublish {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if (![SPDCommonTool isEmpty:self.textView.text]) {
        [dic setObject:self.textView.text forKey:@"text"];
    } else if (!self.dataArray.count) {
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [RequestUtils commonPostMultipartFormDataUtils:dic bURL:@"dynamic.publish" bAnimatedViewController:self constructingBodyWithBlock:^(id<AFMultipartFormData> _Nonnull formData) {
        if (self.dataArray.count) {
            for (UIImage *image in self.dataArray) {
                NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
                NSString *timeStampString = [NSString stringWithFormat:@"%f", timeStamp];
                UIImage *fileImage = [self scaleImage:image toKb:50];
                [formData appendPartWithFileData:UIImageJPEGRepresentation(fileImage, 0.5) name:@"image" fileName:[NSString stringWithFormat:@"%@.jpg", timeStampString] mimeType:@"image/jpg"];
            }
        }
    } success:^(id _Nullable dict) {
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        [self showToast:dict[@"msg"]];
        self.completionBlock();
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(id _Nullable failure) {
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        if ([failure isKindOfClass:[NSDictionary class]]) {
            [self showToast:failure[@"msg"]];
        }
    }];
}

- (void)reloadImageData {
    if ([SPDCommonTool isEmpty:self.textView.text]) {
        self.navigationItem.rightBarButtonItem.enabled = self.dataArray.count;
    }
    [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
}

#pragma mark - event response

- (void)clickLeftBarButtonItem:(UIBarButtonItem *)sender {
    if (![SPDCommonTool isEmpty:self.textView.text] || self.dataArray.count) {
        [self presentAlertController:SPDStringWithKey(@"是否退出此次编辑？", nil) cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)clickRightBarButtonItem:(UIBarButtonItem *)sender {
    [self requestDynamicPublish];
    
    //[MobClick event:@"editclickPublish"];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count == 9 ? self.dataArray.count : self.dataArray.count + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ImageCollectionViewCell" forIndexPath:indexPath];
    cell.delegate = self;
    if (indexPath.row == self.dataArray.count) {
        cell.deleteBtn.hidden = YES;
        cell.imageView.image = [UIImage imageNamed:@"dy_add_image"];
    } else {
        cell.deleteBtn.hidden = NO;
        cell.imageView.image = self.dataArray[indexPath.row];
    }
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.dataArray.count && self.dataArray.count < 9) {
        ZLPhotoPickerViewController *picker = [[ZLPhotoPickerViewController alloc] init];
        picker.maxCount = 9 - self.dataArray.count;
        picker.status = PickerViewShowStatusCameraRoll;
        picker.callBack = ^(NSArray *status) {
            for (ZLPhotoAssets *assets in status) {
                [self.dataArray addObject:assets.originImage];
            }
            [self reloadImageData];
        };
        [picker showPickerVc:self];
        
        //[MobClick event:@"editclickPicture"];
    }
}

#pragma mark - ImageCollectionViewCellDelegate

- (void)didClickDeleteBtnWithImage:(UIImage *)image {
    [self.dataArray removeObject:image];
    [self reloadImageData];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    NSInteger textLimit = 1500;
    if (textView.text.length > textLimit) {
        UITextRange *markedRange = [textView markedTextRange];
        if (markedRange) {
            return;
        }
        NSRange range = [textView.text rangeOfComposedCharacterSequenceAtIndex:textLimit];
        textView.text = [textView.text substringToIndex:range.location];
    }
    if (!self.dataArray.count) {
        self.navigationItem.rightBarButtonItem.enabled = ![SPDCommonTool isEmpty:textView.text];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

#pragma mark - setters and getters

- (LDTextView *)textView {
    if (!_textView) {
        _textView = [[LDTextView alloc] initWithFrame:CGRectMake(12, NavHeight + 5, kScreenW - 12 * 2, 120 - 5)];
        _textView.delegate = self;
        _textView.returnKeyType = UIReturnKeyDone;
        _textView.font = [UIFont systemFontOfSize:15];
        _textView.textColor = [UIColor colorWithHexString:@"232426"];
        _textView.placeholderFont = [UIFont systemFontOfSize:15];
        _textView.placeholderColor = [UIColor colorWithHexString:@"cccccc"];
        _textView.placeholder = SPDStringWithKey(@"这一刻你想说些......", nil);
    }
    return _textView;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake((kScreenW - 11 * 2 - 3 * 2) / 3, (kScreenW - 11 * 2 - 3 * 2) / 3);
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 3;
        layout.sectionInset = UIEdgeInsetsMake(11, 11, 11, 11);
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.textView.frame), kScreenW, kScreenH - CGRectGetMaxY(self.textView.frame)) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [_collectionView registerNib:[UINib nibWithNibName:@"ImageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ImageCollectionViewCell"];

    }
    return _collectionView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

@end
