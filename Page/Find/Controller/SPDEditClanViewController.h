//
//  SPDEditClanViewController.h
//  SimpleDate
//
//  Created by 李楠 on 2018/3/28.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "BaseViewController.h"

@interface SPDEditClanViewController : BaseViewController

@property (nonatomic, copy) NSString *clan_id;
@property (nonatomic, strong) NSDictionary *clanInfo;

@end
