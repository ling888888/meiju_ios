//
//  DynamicPublishViewController.h
//  SimpleDate
//
//  Created by 李楠 on 2018/6/28.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "BaseViewController.h"

@interface DynamicPublishViewController : BaseViewController

@property (nonatomic, copy) void(^completionBlock)();

@end
