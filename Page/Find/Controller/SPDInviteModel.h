//
//  SPDInviteModel.h
//  SimpleDate
//
//  Created by Monkey on 2018/9/20.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "CommonModel.h"

@interface SPDInviteModel : CommonModel

@property (nonatomic, copy) NSString * user_id;
@property (nonatomic, copy) NSString * avatar;
@property (nonatomic, copy) NSString * nick_name;
@property (nonatomic, copy) NSString * regist_time;
@property (nonatomic, assign) BOOL  isapprove;
@property (nonatomic, copy) NSNumber * totalinvit;
@property (nonatomic, copy) NSNumber * yesdayinvit;

@end
