//
//  RankListController.h
//  SimpleDate
//
//  Created by 李楠 on 2018/12/11.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RankListController : BaseViewController

@property (nonatomic, copy) NSString *type; // @[@"charm", @"riche", @"person", @"clan", @"magic"]

@end

NS_ASSUME_NONNULL_END
