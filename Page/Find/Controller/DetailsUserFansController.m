//
//  DetailsUserFansController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/5/8.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "DetailsUserFansController.h"
#import "LY_LiveFollowTableViewCell.h"
#import "LiveModel.h"
#import "ZegoKitManager.h"
#import "LY_Fans.h"
#import "LY_HomePageViewController.h"

@interface DetailsUserFansController ()

@property (nonatomic, copy) NSString * allNum;
@property (nonatomic, strong) UIView * headerView;
@property (nonatomic, strong) UILabel * headerLabel;

@end

@implementation DetailsUserFansController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavigation];
    
    [self setupUI];
    
    [self setupUIFrame];
    
    self.pageNumber = 0;
    
    [self loadNetDataWith:RequestNetDataTypeRefresh];
}

- (void)setupNavigation {
    self.title = @"粉丝".localized;
}

- (void)setupUI {
    self.headerView = [UIView new];
    self.headerView.frame = CGRectMake(0, 0, kScreenW, 40);
    self.headerView.backgroundColor = SPD_HEXCOlOR(@"ffffff");
    
    self.headerLabel=[UILabel new];
    self.headerLabel.textColor = SPD_HEXCOlOR(@"#333333");
    self.headerLabel.font = [UIFont mediumFontOfSize:15];
    [self.headerView addSubview:self.headerLabel];
    [self.headerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.centerY.mas_equalTo(0);
    }];
    
    self.tableView.tipText = @"还没有人关注过主播".localized;
    self.tableView.tipImageName = @"img_kongyemian_guanzhu";
    self.tableView.rowHeight = 75;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[LY_LiveFollowTableViewCell class] forCellReuseIdentifier:@"LiveFollowIdentifier"];
}

- (void)setupUIFrame {
    
}

- (void)loadNetDataWith:(RequestNetDataType)type {
    [super loadNetDataWith:type];
    NSDictionary * params = @{@"userId":self.userId,@"page":@(self.pageNumber)};
    [LY_Network getRequestWithURL:LY_Api.live_Anchor_fans_num parameters:params success:^(id  _Nullable response) {
        if (self.pageNumber == 0) {
            [self.dataList removeAllObjects];
        }
        self.allNum = response[@"allNum"];
        self.headerLabel.text = [NSString stringWithFormat:@"共有%@个粉丝".localized,self.allNum];

        NSArray *dataArray = response[@"list"];
        NSMutableArray *models = [NSMutableArray array];
        for (NSDictionary *data in dataArray) {
            LY_Fans *fans = [LY_Fans yy_modelWithDictionary:data];
            [models addObject:fans];
        }
        if (type == RequestNetDataTypeRefresh) {
            [self.dataList addObjectsFromArray:models];
        } else {
            if (models.count == 0) {
                // 没有更多数据了
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                [self.dataList addObjectsFromArray:models];
            }
        }
        
        self.tableView.showEmptyTipView = (self.dataList.count == 0 ? true : false);
        
        [self reloadData];
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LY_LiveFollowTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LiveFollowIdentifier" forIndexPath:indexPath];
    cell.fans = self.dataList[indexPath.row];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return self.headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    LY_Fans * fans = self.dataList[indexPath.row];
    
    LY_HomePageViewController *vc = [[LY_HomePageViewController alloc] initWithUserId:fans.userId];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)dealloc
{
    NSLog(@"DetailsUserFansController dealloc");
}




@end
