//
//  ReportViewController.h
//  SimpleDate
//
//  Created by xu.juvenile on 2016/5/3.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "BaseViewController.h"

@interface ReportViewController : BaseViewController

@property (nonatomic, copy) NSString *userId;

@end
