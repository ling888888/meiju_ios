//
//  DynamicListViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2018/6/27.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "DynamicListViewController.h"
#import "MomentCell.h"
#import "Moment.h"
#import "Comment.h"
#import "DynamicPublishViewController.h"
#import "CommentEditView.h"

@interface DynamicListViewController ()<UITableViewDataSource, UITableViewDelegate, MomentCellDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) UIButton *publishBtn;
@property (nonatomic, assign) CGFloat lastContentOffsetY;
@property (nonatomic, assign) BOOL isService;
@property (nonatomic, assign) BOOL isAgentService;

@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);

@end

@implementation DynamicListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = SPDStringWithKey(@"动态", nil);
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.publishBtn];
    [self.tableView.mj_header beginRefreshing];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - request

- (void)requestDynamicList {
    NSDictionary *dic = @{@"page": @(self.page)};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"dynamic.list" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable dict) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSArray *list = dict[@"list"];
            NSMutableArray *tempArray = [NSMutableArray array];
            for (NSDictionary *dic in list) {
                __block BOOL isExist = NO;
                if (self.page != 0) {
                    [self.dataArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        if ([dic[@"dynamicID"] isEqualToString:[obj valueForKey:@"dynamicID"]]) {
                            isExist = YES;
                            *stop = YES;
                        }
                    }];
                }
                if (!isExist) {
                    Moment *moment = [Moment initWithDictionary:dic];
                    [tempArray addObject:moment];
                }
            }

            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.page == 0) {
                    [self.dataArray removeAllObjects];
                }
                [self.dataArray addObjectsFromArray:tempArray];
                self.page++;
                [self.tableView reloadData];
                [self.tableView.mj_header endRefreshing];
                [self.tableView.mj_footer endRefreshing];
                self.tableView.mj_footer.hidden = !list.count;
            });
        });
    } bFailure:^(id  _Nullable failure) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }];
}

- (void)requestDynamicCommentWithModel:(Moment *)model content:(NSString *)content {
    NSDictionary *dic = @{@"dynamicID": model.dynamicID, @"content": content};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"dynamic.comment" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable dict) {
        [self showToast:SPDStringWithKey(@"评论成功", nil)];
        Comment *comment = [Comment initWithDictionary:dict];
        comment.moment = model;
        [model.comments addObject:comment];
        [model calculateHeight];
        [self.tableView reloadData];
    } bFailure:^(id  _Nullable failure) {
        if ([failure isKindOfClass:[NSDictionary class]]) {
            [self showToast:failure[@"msg"]];
        }
    }];
}

- (void)requestDynamicCommentDeleteWithModel:(Comment *)model {
    NSDictionary *dic = @{@"commentID": model.commentID};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"dynamic.comment.delete" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable dict) {
        [self showToast:SPDStringWithKey(@"已删除评论", nil)];
        [model.moment.comments removeObject:model];
        [model.moment calculateHeight];
        [self.tableView reloadData];
    } bFailure:^(id  _Nullable failure) {
        if ([failure isKindOfClass:[NSDictionary class]]) {
            [self showToast:failure[@"msg"]];
        }
    }];
}

- (void)requestDynamicLikeWithhModel:(Moment *)model {
    NSDictionary *dic = @{@"dynamicID": model.dynamicID, @"is_like": @(!model.isLike)};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"dynamic.like" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable dict) {
        if (model.isLike) {
            [self showToast:SPDStringWithKey(@"已取消点赞", nil)];
            [model.likes enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([[obj valueForKey:@"userID"] isEqualToString:[SPDApiUser currentUser].userId]) {
                    [model.likes removeObject:obj];
                    *stop = YES;
                }
            }];
            
            //[MobClick event:@"clickCancellike"];
        } else {
            [self showToast:SPDStringWithKey(@"点赞成功", nil)];
            Comment *comment = [[Comment alloc] init];
            comment.userID = [SPDApiUser currentUser].userId;
            comment.userName = [SPDApiUser currentUser].nickName;
            [model.likes addObject:comment];
            
            //[MobClick event:@"clicklike"];
        }
        model.isLike = !model.isLike;
        [model calculateHeight];
        [self.tableView reloadData];
    } bFailure:^(id  _Nullable failure) {
        if ([failure isKindOfClass:[NSDictionary class]]) {
            [self showToast:failure[@"msg"]];
        }
    }];
}

- (void)requestDynamicDeleteWithModel:(Moment *)model {
    NSDictionary *dic = @{@"dynamicID": model.dynamicID};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"dynamic.delete" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable dict) {
        [self showToast:SPDStringWithKey(@"刪除成功", nil)];
        [self.dataArray removeObject:model];
        [self.tableView reloadData];
    } bFailure:^(id  _Nullable failure) {
        if ([failure isKindOfClass:[NSDictionary class]]) {
            [self showToast:failure[@"msg"]];
        }
    }];
}

- (void)requestDynamicHideWithModel:(Moment *)model {
    NSDictionary *dic = @{@"dynamicID": model.dynamicID};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"dynamic.hide" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable dict) {
        [self showToast:SPDStringWithKey(@"刪除成功", nil)];
        [self.dataArray removeObject:model];
        [self.tableView reloadData];
    } bFailure:^(id  _Nullable failure) {
        if ([failure isKindOfClass:[NSDictionary class]]) {
            [self showToast:failure[@"msg"]];
        }
    }];
}

- (void)requestMyInfoDetail {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.info.detail" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.isAgentService = [suceess[@"is_agent_gm"] boolValue];
        [self requestDynamicList];
    } bFailure:^(id  _Nullable failure) {
        [self requestDynamicList];
    }];
}

#pragma mark - event response

- (void)clickPublishBtn:(UIButton *)sender {
    DynamicPublishViewController *vc = [[DynamicPublishViewController alloc] init];
    [vc setCompletionBlock:^{
        [self.tableView setContentOffset:CGPointZero animated:YES];
        [self.tableView.mj_header beginRefreshing];
    }];
    [self.navigationController pushViewController:vc animated:YES];
    
    //[MobClick event:@"clickPublish"];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"MomentCell";
    MomentCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[MomentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor whiteColor];
    }
    cell.tag = indexPath.row;
    cell.isService = self.isService;
    cell.hideBtn.hidden = !self.isAgentService;
    cell.moment = [self.dataArray objectAtIndex:indexPath.row];
    cell.delegate = self;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [(Moment *)self.dataArray[indexPath.row] height];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    self.lastContentOffsetY = scrollView.contentOffset.y;
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    if (self.lastContentOffsetY < scrollView.contentOffset.y) {
        self.publishBtn.hidden = YES;
        
        //[MobClick event:@"dynamiclistUpglide"];
    } else {
        self.publishBtn.hidden = NO;
    }
}

#pragma mark - MomentCellDelegate

// 点击用户头像或昵称
- (void)didClickUser:(MomentCell *)cell {
    [self pushToDetailTableViewController:self userId:cell.moment.userID];
    
    //[MobClick event:@"clickUser"];
}

// 赞
- (void)didLikeMoment:(MomentCell *)cell {
    [self requestDynamicLikeWithhModel:cell.moment];
}

// 评论
- (void)didAddComment:(MomentCell *)cell {
    [CommentEditView beginEditingWithCompletion:^(NSString *content) {
        [self requestDynamicCommentWithModel:cell.moment content:content];
    }];
    
    //[MobClick event:@"clickComment"];
}

// 查看全文/收起
- (void)didSelectFullText:(MomentCell *)cell {
    [self.tableView reloadData];
}

// 删除
- (void)didDeleteMoment:(MomentCell *)cell {
    [self presentCommonController:SPDStringWithKey(@"确定删除吗？", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
        [self requestDynamicDeleteWithModel:cell.moment];
        
        //[MobClick event:@"clickDelete"];
    }];
}

// 选择评论
- (void)didSelectComment:(Comment *)comment {
    if ([comment.userID isEqualToString:[SPDApiUser currentUser].userId] || self.isService) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *deleateAction = [UIAlertAction actionWithTitle:SPDStringWithKey(@"删除", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [self requestDynamicCommentDeleteWithModel:comment];
            
            //[MobClick event:@"clickDeletecomment"];
        }];
        [alertController addAction:deleateAction];
        UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:SPDStringWithKey(@"取消", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alertController addAction:cancleAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

// 点击高亮文字
- (void)didClickLink:(MLLink *)link linkText:(NSString *)linkText momentCell:(MomentCell *)cell {
    [self pushToDetailTableViewController:self userId:link.linkValue];
}

// 代理客服 屏蔽动态
- (void)didHideMoment:(MomentCell *)cell {
    [self presentCommonController:SPDStringWithKey(@"确定删除吗？", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
        [self requestDynamicHideWithModel:cell.moment];
    }];
}

// MARK: - JXPagerViewListViewDelegate

- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return self.tableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback ?: self.scrollCallback(scrollView);
}

#pragma mark - setters and getters

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - NavHeight) style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorColor = [UIColor colorWithHexString:@"e6e6e6"];
        _tableView.separatorInset = UIEdgeInsetsZero;
        if (@available(iOS 11.0, *)) {
            _tableView.estimatedRowHeight = 0;
        }
        
        _tableView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            self.page = 0;
            [self requestMyInfoDetail];
        }];
        _tableView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            [self requestDynamicList];
        }];
        _tableView.mj_footer.hidden = YES;
    }
    return _tableView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (UIButton *)publishBtn {
    if (!_publishBtn) {
        _publishBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _publishBtn.frame = CGRectMake(self.view.bounds.size.width - 13 - 64, self.view.bounds.size.height - NavHeight - TabBarHeight - 30 - 64, 64, 64);
        [_publishBtn setImage:[UIImage imageNamed:@"dy_edit"] forState:UIControlStateNormal];
        [_publishBtn addTarget:self action:@selector(clickPublishBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _publishBtn;
}

- (BOOL)isService {
    NSArray *serviceIDs = @[@"13", @"32", @"832370", @"99888", @"33978", @"832410"];
    for (NSString *serviceID in serviceIDs) {
        if ([[SPDApiUser currentUser].userId isEqualToString:serviceID]) {
            return YES;
        }
    }
    return NO;
}

@end
