//
//  SPDClanListViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2019/5/10.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "SPDClanListViewController.h"
#import "FamilyModel.h"
#import "SPDClanHotListCell.h"

@interface SPDClanListViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, SPDClanHotListCellDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) BOOL isAgentService;

@end

@implementation SPDClanListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = self.country_name ? : SPDStringWithKey(@"家族列表", nil);
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.collectionView];
    
    [self requestMyInfoDetail];
}

#pragma mark - Private methods

- (void)requestMyInfoDetail {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.info.detail" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.isAgentService = [suceess[@"is_agent_gm"] boolValue];
        [[NSUserDefaults standardUserDefaults] setObject:suceess forKey:@"MyInfoDetailDict"];
        
        [self requestClanListWithPage:0];
    } bFailure:^(id  _Nullable failure) {
        [self requestClanListWithPage:0];
    }];
}

- (void)requestClanListWithPage:(NSInteger)page {
    NSMutableDictionary *dic = [@{@"page": @(page), @"lang": [SPDApiUser currentUser].lang} mutableCopy];
    if (self.country) {
        [dic setObject:self.country forKey:@"country"];
    } else {
        [dic setObject:[SPDApiUser currentUser].userId forKey:@"user_id"];
    }
    [RequestUtils commonGetRequestUtils:dic bURL:@"list" isClan:YES bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if (page == 0) {
            [self.dataArray removeAllObjects];
        }
        
        NSArray *listArray = suceess[@"clans"];
        for (NSDictionary *dic in listArray) {
            BOOL existing = NO;
            if (page != 0) {
                for (FamilyModel *existingModel in self.dataArray) {
                    if ([dic[@"_id"] isEqualToString:existingModel._id]) {
                        existing = YES;
                        break;
                    }
                }
            }
            if (!existing) {
                FamilyModel *model = [FamilyModel initWithDictionary:dic];
                [self.dataArray addObject:model];
            }
        }
        [self.collectionView reloadData];
        self.page = page + 1;
        
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
        self.collectionView.mj_footer.hidden = listArray.count < 10;
    } bFailure:^(id  _Nullable failure) {
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SPDClanHotListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SPDClanHotListCell" forIndexPath:indexPath];
    cell.isAgentService = self.isAgentService;
    cell.model = self.dataArray[indexPath.row];
    cell.delegate = self;
    return cell;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, 8, 0, 8);
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    FamilyModel *model = self.dataArray[indexPath.row];
    [self joinChatroomWithClanId:model._id];
}

#pragma mark - SPDClanHotListCellDelegate

- (void)didClickStickBtnWithModel:(FamilyModel *)model {
    [self presentCommonController:SPDStringWithKey(@"确定置顶该家族", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
        NSDictionary *dic = @{@"user_id": [SPDApiUser currentUser].userId, @"clan_id": model._id};
        [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"stick" isClan:YES bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
            [self showToast:SPDStringWithKey(@"置顶成功", nil)];
        } bFailure:^(id  _Nullable failure) {
            if ([failure isKindOfClass:[NSDictionary class]]) {
                if ([failure[@"code"] integerValue] == 2038) {
                    [self showToast:SPDStringWithKey(@"本次置顶次数用完，请1小时候再进行置顶", nil)];
                } else if ([failure[@"code"] integerValue] == 2037) {
                    [self showToast:SPDStringWithKey(@"你不是这个区的客服助手", nil)];
                } else {
                    [self showToast:failure[@"msg"]];
                }
            }
        }];
    }];
}

#pragma mark - Setters & Getters

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.itemSize = CGSizeMake(kScreenW -16, 90);
        layout.minimumLineSpacing = 8.0f;
        layout.minimumInteritemSpacing = 8.0f;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0 ,NavHeight, kScreenW, kScreenH - NavHeight) collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor colorWithHexString:@"f7f7f7"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerNib:[UINib nibWithNibName:@"SPDClanHotListCell" bundle:nil] forCellWithReuseIdentifier:@"SPDClanHotListCell"];
        
        __weak typeof(self) weakself = self;
        _collectionView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            [weakself requestMyInfoDetail];
        }];
        _collectionView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            [weakself requestClanListWithPage:weakself.page];
        }];
        _collectionView.mj_footer.hidden = YES;
        
        if (@available(iOS 11.0, *)) {
            _collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
    }
    return _collectionView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

@end
