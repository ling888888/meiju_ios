//
//  ListViewController.h
//  SimpleDate
//
//  Created by ling Hou on 2020/2/5.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "BaseViewController.h"
#import "JXPagerView.h"

NS_ASSUME_NONNULL_BEGIN

@class FamilyModel;

@interface ListViewController : BaseViewController

@property (nonatomic, copy) NSString * userId;

@end

NS_ASSUME_NONNULL_END
