//
//  FollowContainerController.h
//  SimpleDate
//
//  Created by ling Hou on 2020/5/7.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WMPageController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FollowContainerController : WMPageController

@property (nonatomic, copy) NSString * userId;

@end

NS_ASSUME_NONNULL_END
