//
//  Moment.h
//  MomentKit
//
//  Created by LEA on 2017/12/12.
//  Copyright © 2017年 LEA. All rights reserved.
//
//  动态Model
//

#import <Foundation/Foundation.h>

@interface Moment : CommonModel

@property (nonatomic, copy) NSString *dynamicID;
@property (nonatomic, copy) NSString *userID;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, strong) NSNumber *age;
@property (nonatomic, copy) NSString *specialNum;
@property (nonatomic, assign) BOOL is_agent_gm; // 是否是代理客服

@property (nonatomic, copy) NSString *text;
@property (nonatomic, assign) CGFloat textHeight;
@property (nonatomic, assign) CGFloat textOriginHeight;
@property (nonatomic, assign) CGFloat textLimitHeight;
@property (nonatomic, assign) BOOL moreThanLimit;
@property (nonatomic, assign) BOOL isFullText;

@property (nonatomic, strong) NSMutableArray *images;
@property (nonatomic, assign) CGFloat imageHeight;
@property (nonatomic, assign) CGSize singleImageSize;
@property (nonatomic, assign) BOOL imageHadLoaded;

@property (nonatomic, strong) NSMutableArray *likes;
@property (nonatomic, strong) NSMutableAttributedString *praiseNameList;
@property (nonatomic, assign) CGFloat likesHeight;
@property (nonatomic, assign) BOOL isLike;

@property (nonatomic, strong) NSMutableArray *comments;
@property (nonatomic, strong) NSNumber *time;
@property (nonatomic, assign) CGFloat height;

- (void)calculateHeight;

@end
