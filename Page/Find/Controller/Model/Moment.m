//
//  Moment.m
//  MomentKit
//
//  Created by LEA on 2017/12/12.
//  Copyright © 2017年 LEA. All rights reserved.
//

#import "Moment.h"
#import "Comment.h"
#import "MomentKit.h"
#import "Utility.h"

@implementation Moment

@synthesize images = _images;
@synthesize likes = _likes;
@synthesize comments = _comments;

+ (id)initWithDictionary:(NSDictionary *)dic {
    Moment *moment = [super initWithDictionary:dic];
    [moment calculateHeight];
    return moment;
}

- (void)calculateHeight {
    CGFloat height = kBlank;
    // 名字
    height += kNameLabelH + kPaddingValue;
    // 正文
    if (self.text.length) {
        if (!self.moreThanLimit) {
            height += self.textHeight + kPaddingValue;
        } else {
            height += self.textHeight + kArrowHeight + kMoreLabHeight + kPaddingValue;
        }
    }
    // 图片
    height += self.imageHeight + kPaddingValue;
    // 时间
    height += kTimeLabelH + kPaddingValue;
    // 如果赞或评论不空，加kArrowHeight
    CGFloat addH = 0;
    // 赞
    if (self.likes.count) {
        addH = kArrowHeight;
        height += self.likesHeight + 15;
    }
    // 评论
    for (Comment *comment in self.comments) {
        addH = kArrowHeight;
        height += (comment.height + 5);
    }
    if (addH == 0) {
        height -= kPaddingValue;
    }
    height += kBlank + addH;
    self.height = height;
}

- (void)setText:(NSString *)text {
    _text = text;
    
    UIFont *font = [UIFont systemFontOfSize:15.0];
    self.textLimitHeight = font.lineHeight * 6.1;
    CGFloat height = [text boundingRectWithSize:CGSizeMake(kTextWidth, CGFLOAT_MAX)
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:@{NSFontAttributeName: font}
                                     context:nil].size.height;
    self.textOriginHeight = ceilf(height);
    if (self.textOriginHeight > self.textLimitHeight) {
        self.moreThanLimit = YES;
        self.textHeight = self.textLimitHeight;
    } else {
        self.moreThanLimit = NO;
        self.textHeight = self.textOriginHeight;
    }
}

- (void)setImages:(NSMutableArray *)images {
    _images = images;

    if (_images.count == 0) {
        self.imageHeight = 0;
    } else if (_images.count == 1) {
//        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _images.firstObject]];
//        self.singleImageSize = [Utility getSingleSize:[self getImageSizeWithURL:url]];
        self.singleImageSize = CGSizeMake(225, 225);
        self.imageHadLoaded = NO;
    } else if (_images.count < 4) {
        self.imageHeight = kImageWidth;
    } else if (_images.count < 7) {
        self.imageHeight = (kImageWidth * 2 + kImagePadding);
    } else {
        self.imageHeight = (kImageWidth * 3 + kImagePadding * 2);
    }
}

- (void)setSingleImageSize:(CGSize)singleImageSize {
    _singleImageSize = singleImageSize;
    self.imageHeight = _singleImageSize.height;
}

- (void)setLikes:(NSMutableArray *)likes {
    if (!_likes) {
        _likes = [NSMutableArray array];
    }

    for (int i = 0; i < likes.count; i++) {
        NSDictionary *dic = likes[i];
        Comment *model = [Comment initWithDictionary:dic];
        [_likes addObject:model];
        if ([model.userID isEqualToString:[SPDApiUser currentUser].userId]) {
            _isLike = YES;
        }
        
        if (i > 0) {
            [self.praiseNameList appendAttributedString:[[NSAttributedString alloc] initWithString:@"，"]];
        }
        NSString *text = model.userName;
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:text];
        [attString setAttributes:@{NSFontAttributeName: kComHLTextFont, NSLinkAttributeName: model.userID} range:NSMakeRange(0, attString.length)];
        [self.praiseNameList appendAttributedString:attString];
    }
    CGFloat height = [self.praiseNameList boundingRectWithSize:CGSizeMake(kTextWidth, CGFLOAT_MAX)
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                       context:nil].size.height;
    self.likesHeight = ceilf(height);
}

- (void)setComments:(NSMutableArray *)comments {
    if (!_comments) {
        _comments = [NSMutableArray array];
    }
    
    for (NSDictionary *dic in comments) {
        Comment *comment = [Comment initWithDictionary:dic];
        comment.moment = self;
        [_comments addObject:comment];
    }
}

- (void)setIsLike:(BOOL)isLike {
    _isLike = isLike;
    
    self.praiseNameList = nil;
    for (int i = 0; i < self.likes.count; i++) {
        Comment *model = self.likes[i];
        if (i > 0) {
            [self.praiseNameList appendAttributedString:[[NSAttributedString alloc] initWithString:@"，"]];
        }
        NSString *text = model.userName;
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:text];
        [attString setAttributes:@{NSFontAttributeName: kComHLTextFont, NSLinkAttributeName: model.userID} range:NSMakeRange(0, attString.length)];
        [self.praiseNameList appendAttributedString:attString];
    }
    CGFloat height = [self.praiseNameList boundingRectWithSize:CGSizeMake(kTextWidth, CGFLOAT_MAX)
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                       context:nil].size.height;
    self.likesHeight = ceilf(height);
}

- (void)setIsFullText:(BOOL)isFullText {
    _isFullText = isFullText;
    
    if (_isFullText) {
        self.textHeight = self.textOriginHeight;
    } else {
        self.textHeight = self.textLimitHeight;
    }
    [self calculateHeight];
}

- (NSMutableAttributedString *)praiseNameList {
    if (!_praiseNameList) {
        NSTextAttachment *attach = [NSTextAttachment new];
        attach.image = [UIImage imageNamed:@"moment_like_hl"];
        attach.bounds = CGRectMake(0, -3, attach.image.size.width, attach.image.size.height);
        NSAttributedString *likeIcon = [NSAttributedString attributedStringWithAttachment:attach];
        
        _praiseNameList = [[NSMutableAttributedString alloc] initWithAttributedString:likeIcon];
        [_praiseNameList appendAttributedString:[[NSAttributedString alloc] initWithString:@" "]];
    }
    return _praiseNameList;
}

- (NSMutableArray *)images {
    if (!_images) {
        _images = [NSMutableArray array];
    }
    return _images;
}

- (NSMutableArray *)likes {
    if (!_likes) {
        _likes = [NSMutableArray array];
    }
    return _likes;
}

- (NSMutableArray *)comments {
    if (!_comments) {
        _comments = [NSMutableArray array];
    }
    return _comments;
}

- (CGSize)getImageSizeWithURL:(id)URL {
    NSURL *url = nil;
    if ([URL isKindOfClass:[NSURL class]]) {
        url = URL;
    }
    if ([URL isKindOfClass:[NSString class]]) {
        url = [NSURL URLWithString:URL];
    }
    if (!URL) {
        return CGSizeZero;
    }
    CGImageSourceRef imageSourceRef = CGImageSourceCreateWithURL((CFURLRef)url, NULL);
    CGFloat width = 0, height = 0;

    if (imageSourceRef) {

        // 获取图像属性
        CFDictionaryRef imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSourceRef, 0, NULL);

        //以下是对手机32位、64位的处理
        if (imageProperties != NULL) {

            CFNumberRef widthNumberRef = CFDictionaryGetValue(imageProperties, kCGImagePropertyPixelWidth);

#if defined(__LP64__) && __LP64__
            if (widthNumberRef != NULL) {
                CFNumberGetValue(widthNumberRef, kCFNumberFloat64Type, &width);
            }

            CFNumberRef heightNumberRef = CFDictionaryGetValue(imageProperties, kCGImagePropertyPixelHeight);

            if (heightNumberRef != NULL) {
                CFNumberGetValue(heightNumberRef, kCFNumberFloat64Type, &height);
            }
#else
            if (widthNumberRef != NULL) {
                CFNumberGetValue(widthNumberRef, kCFNumberFloat32Type, &width);
            }

            CFNumberRef heightNumberRef = CFDictionaryGetValue(imageProperties, kCGImagePropertyPixelHeight);

            if (heightNumberRef != NULL) {
                CFNumberGetValue(heightNumberRef, kCFNumberFloat32Type, &height);
            }
#endif
            /***************** 此处解决返回图片宽高相反问题 *****************/
            // 图像旋转的方向属性
            NSInteger orientation = [(__bridge NSNumber *)CFDictionaryGetValue(imageProperties, kCGImagePropertyOrientation) integerValue];
            CGFloat temp = 0;
            switch (orientation) {  // 如果图像的方向不是正的，则宽高互换
                case UIImageOrientationLeft: // 向左逆时针旋转90度
                case UIImageOrientationRight: // 向右顺时针旋转90度
                case UIImageOrientationLeftMirrored: // 在水平翻转之后向左逆时针旋转90度
                case UIImageOrientationRightMirrored: { // 在水平翻转之后向右顺时针旋转90度
                    temp = width;
                    width = height;
                    height = temp;
                }
                    break;
                default:
                    break;
            }
            /***************** 此处解决返回图片宽高相反问题 *****************/

            CFRelease(imageProperties);
        }
        CFRelease(imageSourceRef);
    }
    if (!width || !height) {
        return CGSizeMake(75, 75);
    } else {
        return CGSizeMake(width, height);
    }
}

@end
