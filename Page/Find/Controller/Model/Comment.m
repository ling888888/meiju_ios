//
//  Comment.m
//  MomentKit
//
//  Created by LEA on 2017/12/12.
//  Copyright © 2017年 LEA. All rights reserved.
//

#import "Comment.h"
#import "MomentKit.h"

@implementation Comment

+ (id)initWithDictionary:(NSDictionary *)dic {
    Comment *comment = [super initWithDictionary:dic];
    
    NSString *text = [NSString stringWithFormat:@"%@：%@",comment.userName, comment.content];
    comment.text = [[NSMutableAttributedString alloc] initWithString:text];
    [comment.text setAttributes:@{NSFontAttributeName:kComHLTextFont, NSLinkAttributeName: comment.userID}
                          range:NSMakeRange(0, comment.userName.length)];
    
    CGFloat height = [comment.text boundingRectWithSize:CGSizeMake(kTextWidth, CGFLOAT_MAX)
                                                options:NSStringDrawingUsesLineFragmentOrigin
                                                context:nil].size.height;
    comment.height = ceilf(height);
    return comment;
}

@end
