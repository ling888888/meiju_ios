//
//  Comment.h
//  MomentKit
//
//  Created by LEA on 2017/12/12.
//  Copyright © 2017年 LEA. All rights reserved.
//
//  评论Model
//

#import <Foundation/Foundation.h>

@class Moment;

@interface Comment : CommonModel

@property (nonatomic, weak) Moment *moment;
@property (nonatomic, copy) NSString *commentID;
@property (nonatomic, copy) NSString *userID;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, strong) NSMutableAttributedString *text;
@property (nonatomic, assign) CGFloat height;

@end
