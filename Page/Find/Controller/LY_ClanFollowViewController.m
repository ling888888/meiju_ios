//
//  LY_ClanFollowViewController.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/9.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_ClanFollowViewController.h"
#import "LY_ClanFollowHeaderView.h"
#import "SPDClanHotListCell.h"
#import "FamilyModel.h"
#import "ZegoKitManager.h"


@interface LY_ClanFollowViewController () <SPDClanHotListCellDelegate>

@property(nonatomic, strong) FamilyModel *myClan;

@property(nonatomic, assign) BOOL isShowRecommendText;

@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);

@end

@implementation LY_ClanFollowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    
    [self setupUIFrame];
    
//    [self setupMyClanData];
    
    [self loadNetDataWith:RequestNetDataTypeRefresh];
}

- (void)setupUI {
    self.collectionView.mj_footer = nil;
    [self.collectionView registerNib:[UINib nibWithNibName:@"SPDClanHotListCell" bundle:nil] forCellWithReuseIdentifier:@"SPDClanHotListCell"];
    [self.collectionView registerClass:[LY_ClanFollowHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"ClanFollowHeaderViewIdentifier"];
}

- (void)setupUIFrame {
    
}

- (void)loadNetDataWith:(RequestNetDataType)type {
    NSDictionary *params = @{@"user_id": [SPDApiUser currentUser].userId};
    [LY_Network getRequestWithURL:LY_Api.clan_mine parameters:params success:^(id  _Nullable response) {
        
        NSMutableArray *models = [NSMutableArray array];
        for (NSDictionary *dic in response[@"following_clan"]) {
           FamilyModel *model = [FamilyModel initWithDictionary:dic];
           [models addObject:model];
        }
        
        self.isShowRecommendText = models.count == 0;
        
        NSDictionary *myClanDic = response[@"my_clan"];
        UICollectionViewFlowLayout * flowLayout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
        if (myClanDic && myClanDic.count) {
            // 有自己的家族
            FamilyModel * model = [FamilyModel initWithDictionary:myClanDic];
            self.myClan = model;
            if (self.isShowRecommendText) {
                flowLayout.headerReferenceSize = CGSizeMake(kScreenW, 175 + 55);
            } else {
                flowLayout.headerReferenceSize = CGSizeMake(kScreenW, 175);
            }
        }else{
            // 没有自己的家族
            self.myClan = nil;
            if (self.isShowRecommendText) {
                flowLayout.headerReferenceSize = CGSizeMake(kScreenW, (kScreenW - 16) / 359 * 120 + 85 + 55);
            } else {
                flowLayout.headerReferenceSize = CGSizeMake(kScreenW, (kScreenW - 16) / 359 * 120 + 85);
            }
        }
        
        if (models.count == 0) {
            for (NSDictionary *dic in response[@"recommend_clan"]) {
               FamilyModel *model = [FamilyModel initWithDictionary:dic];
               [models addObject:model];
            }
        }
        
        self.dataList = models;
        
        [self reloadData];
    } failture:^(NSError * _Nullable error) {
        [self reloadData];
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SPDClanHotListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SPDClanHotListCell" forIndexPath:indexPath];
    
    cell.model = self.dataList[indexPath.row];
    
    cell.delegate = self;
    
    cell.backgroundColor = [UIColor yellowColor];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    FamilyModel *model = self.dataList[indexPath.row];
    [ZegoManager joinRoomFrom:self clanId:model._id];
//   [self joinChatroomWithClanId:model._id];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(kScreenW - 16, 90);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, 8, 0, 8);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    LY_ClanFollowHeaderView *view = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"ClanFollowHeaderViewIdentifier" forIndexPath:indexPath];
    
    if (self.myClan) {
        // 有自己家族
        [view showMyClanViewWith:self.myClan isShowRecommendText:self.isShowRecommendText];
    } else {
        // 没自己家族
        [view showNoDataViewWithIsShowRecommendText:self.isShowRecommendText];
    }
    
    return view;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback ?: self.scrollCallback(scrollView);
}

- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return self.collectionView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *scrollView))callback {
    self.scrollCallback = callback;
}

- (void)didClickStickBtnWithModel:(FamilyModel *)model {
    
}

@end
