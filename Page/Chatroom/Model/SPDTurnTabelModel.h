//
//  SPDTurnTabelModel.h
//  SimpleDate
//
//  Created by Monkey on 2018/12/17.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPDTurnTabelModel : CommonModel

@property (nonatomic, copy) NSString * image; // 图片的地址

@property (nonatomic, copy) NSString * desc; // 描述

@property (nonatomic, copy) NSString * hint_info; //中奖的提示信息


@end
