//
//  ChatroomRedPacketModel.m
//  SimpleDate
//
//  Created by 李楠 on 2018/9/4.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "ChatroomRedPacketModel.h"

@implementation ChatroomRedPacketModel

- (void)setList:(NSMutableArray *)list {
    if (!_list) {
        _list = [NSMutableArray array];
    }
    for (NSDictionary *dic in list) {
        ChatroomRedPacketUserModel *model = [ChatroomRedPacketUserModel initWithDictionary:dic];
        [_list addObject:model];
    }
}

@end


@implementation ChatroomRedPacketUserModel

@end
