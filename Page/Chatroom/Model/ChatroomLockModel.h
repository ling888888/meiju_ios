//
//  ChatroomLockModel.h
//  SimpleDate
//
//  Created by 李楠 on 2018/11/19.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "CommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChatroomLockModel : CommonModel

@property (nonatomic, copy) NSString *lockId;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, strong) NSNumber *day;

@end

NS_ASSUME_NONNULL_END
