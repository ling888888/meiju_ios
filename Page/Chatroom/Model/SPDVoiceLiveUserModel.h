//
//  SPDVoiceLiveUserModel.h
//  SimpleDate
//
//  Created by 李楠 on 17/5/16.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SPDVoiceLiveUserModel : CommonModel

@property (nonatomic, copy) NSString *_id;
@property (nonatomic, copy) NSString *nick_name;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, strong) LY_Portrait *portrait;
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSString *position; //职位描述["tourist","chief","deputychief","member"],游客，族长，副族长，成员
@property (nonatomic, assign) NSInteger positionLevel; // 权限值：0 游客，25 家族成员，75 副族长，150 族长
@property (nonatomic, strong) NSNumber *level; // 用户等级
@property (nonatomic, copy) NSString *noble; // 贵族
@property (nonatomic, strong) NSNumber *age;
@property (nonatomic, copy) NSString *specialNum; // 靓号
@property (nonatomic, copy) NSString *driveVehicle;
@property (nonatomic, copy) NSString *micIndex;
@property (nonatomic, strong) NSNumber *nobleValue;
@property (nonatomic, copy) NSString * headwear; // 头饰
@end
