//
//  SPDVoiceLiveUserModel.m
//  SimpleDate
//
//  Created by 李楠 on 17/5/16.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDVoiceLiveUserModel.h"

@implementation SPDVoiceLiveUserModel

- (void)setValue:(id)value forKey:(NSString *)key {
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"avatar"]) {
        self.portrait.url = value;
    } else if ([key isEqualToString:@"headwearWebp"]) {
        self.portrait.headwearUrl = value;
    }
}

- (void)setPosition:(NSString *)position {
    _position = position;
    self.positionLevel = [self getPositionLevel:position];
}

- (NSInteger)getPositionLevel:(NSString *)position {
    NSInteger positionLevel;
    if ([position isEqualToString:@"member"]) {
        positionLevel = 25;
    } else if ([position isEqualToString:@"deputychief"]) {
        positionLevel = 75;
    } else if ([position isEqualToString:@"chief"]) {
        positionLevel = 150;
    } else if ([position isEqualToString:@"superadmin"]) {
        positionLevel = 250;
    } else {
        positionLevel = 0;
    }
    return positionLevel;
}

- (void)setNobleValue:(NSNumber *)nobleValue {
    if (nobleValue.integerValue < 150) {
        _nobleValue = @(0);
    } else {
        _nobleValue = nobleValue;
    }
}

- (LY_Portrait *)portrait {
    if (!_portrait) {
        _portrait = [[LY_Portrait alloc] init];
    }
    return _portrait;
}

@end
