//
//  ChatroomBgImageModel.h
//  SimpleDate
//
//  Created by 李楠 on 2018/8/21.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "CommonModel.h"

@interface ChatroomBgImageModel : CommonModel

@property (nonatomic, copy) NSString *backgroundId;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *buy_type;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, strong) NSNumber *renewPrice;
@property (nonatomic, strong) NSNumber *day;
@property (nonatomic, assign) BOOL isOwn;
@property (nonatomic, assign) BOOL isUse;
@property (nonatomic, strong) NSNumber *expireTime;
@property (nonatomic, copy) NSString *expireTimeStr;
@property (nonatomic, copy) NSString *activity;

@end
