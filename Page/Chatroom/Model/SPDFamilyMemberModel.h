//
//  SPDFamilyMemberModel.h
//  SimpleDate
//
//  Created by 李楠 on 17/5/15.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPDFamilyMemberModel : CommonModel

@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *avatar;
@property(nonatomic, strong) LY_Portrait *portrait;
@property (nonatomic, copy) NSNumber *age;
@property (nonatomic, copy) NSString *nick_name;
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSNumber *bill;
@property (nonatomic, copy) NSString *last_login_time; //最后登录时间
@property (nonatomic, copy) NSString *position; //职位描述["tourist","chief","deputychief","member"],游客，族长，副族长，成员
@property (nonatomic, copy) NSNumber *level;

@end
