//
//  CRSendGiftMessage.m
//  SimpleDate
//
//  Created by Monkey on 2017/9/19.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "CRSendGiftMessage.h"

@implementation CRSendGiftMessage


+(instancetype)cRSendGiftMessageWithfromUserId:(NSString *)fromUserId toUserId:(NSString *)toUserId ClanId:(NSString *)clan_id and_id:(NSString *)_id andNum:(NSString *)num {
    CRSendGiftMessage *Msg = [[CRSendGiftMessage alloc]init];
    if (Msg) {
        Msg.fromUserId = fromUserId;
        Msg.toUserId = toUserId;
        Msg.clan_id = clan_id;
        Msg._id = _id;
        Msg.num = num;
    }
    return Msg;
}

/// NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.fromUserId = [aDecoder decodeObjectForKey:@"fromUserId"];
        self.toUserId = [aDecoder decodeObjectForKey:@"toUserId"];
        self._id = [aDecoder decodeObjectForKey:@"_id"];
        self.clan_id = [aDecoder decodeObjectForKey:@"clan_id"];
        self.num = [aDecoder decodeObjectForKey:@"num"];
    }
    return self;
}


/// NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.fromUserId forKey:@"fromUserId"];
    if (self.toUserId) {
        [aCoder encodeObject:self.toUserId forKey:@"toUserId"];
    }
    [aCoder encodeObject:self._id forKey:@"_id"];
    [aCoder encodeObject:self.clan_id  forKey:@"clan_id"];
    [aCoder encodeObject:self.num forKey:@"num"];
}


///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [NSMutableDictionary dictionary];
    [dataDict setObject:self.fromUserId forKey:@"fromUserId"];
    if (self.toUserId) {
        [dataDict setObject:self.toUserId forKey:@"toUserId"];
    }
    [dataDict setObject:self.clan_id forKey:@"clan_id"];
    [dataDict setObject:self._id forKey:@"_id"];
    [dataDict setObject:self.num forKey:@"num"];
    
    if (self.senderUserInfo) {
        NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc] init];
        if (self.senderUserInfo.name) {
            [userInfoDic setObject:self.senderUserInfo.name
                 forKeyedSubscript:@"name"];
        }
        if (self.senderUserInfo.portraitUri) {
            [userInfoDic setObject:self.senderUserInfo.portraitUri
                 forKeyedSubscript:@"icon"];
        }
        if (self.senderUserInfo.userId) {
            [userInfoDic setObject:self.senderUserInfo.userId
                 forKeyedSubscript:@"id"];
        }
        [dataDict setObject:userInfoDic forKey:@"user"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        
        if (dictionary) {
            self.fromUserId = dictionary[@"fromUserId"];
            self.toUserId = dictionary[@"toUserId"];
            self.clan_id = dictionary[@"clan_id"];
            self._id = dictionary[@"_id"];
            self.num = dictionary[@"num"];
            NSDictionary *userinfoDic = dictionary[@"user"];
            [self decodeUserInfo:userinfoDic];
        }
    }
}


///消息的类型名
+ (NSString *)getObjectName {
    return CRSendGiftMessageIdentifier;
}


@end
