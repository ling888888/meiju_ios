//
//  SPDWorldChatMessage.m
//  SimpleDate
//
//  Created by Monkey on 2017/5/31.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDWorldChatMessage.h"
#import "SPDVoiceLiveUserModel.h"

@implementation SPDWorldChatMessage


+ (instancetype)SPDWorldChatMessageWithContent:(NSString *)content andsend_ClanId:(NSString *)send_ClanId andsend_ClanName:(NSString *)send_ClanName andGender:(NSString *)gender {
    
    SPDWorldChatMessage * worldMsg = [[SPDWorldChatMessage alloc]init];
    if (worldMsg) {
        worldMsg.content = content;
        worldMsg.send_ClanId = send_ClanId;
        worldMsg.send_ClanName = send_ClanName;
        worldMsg.gender = gender;
    }
    return worldMsg;
}

///消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return    MessagePersistent_ISCOUNTED;
}

///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict =[self encodeBaseDict];
    [dataDict setObject:self.content ?:@""forKey:@"content"];
    [dataDict setObject:self.send_ClanId ?:@""forKey:@"send_ClanId"];
    [dataDict setObject:self.send_ClanName ?:@""forKey:@"send_ClanName"];
    [dataDict setObject:self.gender ?:@""forKey:@"gender"];
    if (self.type) {
        [dataDict setObject:self.receive_avatar?:@"" forKey:@"receive_avatar"];
        [dataDict setObject:self.receive_nickname ?:@""forKey:@"receive_nickname"];
        [dataDict setObject:self.receive_Id?:@"" forKey:@"receive_Id"];
        [dataDict setObject:self.receive_gender?:@"" forKey:@"receive_gender"];
        [dataDict setObject:self.present_url ?:@""forKey:@"present_url"];
        [dataDict setObject:self.gift_id ?:@""forKey:@"gift_id"];
        [dataDict setObject:self.type ?:@""forKey:@"type"];
        [dataDict setObject:self.send_msg_world?:@"" forKey:@"send_msg_world"];
    }
    if (self.mentionedType) {
        [dataDict setObject:self.mentionedType forKey:@"mentionedType"];
        [dataDict setObject:self.mentionedList forKey:@"mentionedList"];
    }
    if (self.send_headwear) {
        [dataDict setObject:self.send_headwear forKey:@"send_headwear"];
    }
    if (self.receive_headwear) {
        [dataDict setObject:self.receive_headwear forKey:@"receive_headwear"];
    }
    if (self.receiveHeadwearWebp) {
        [dataDict setObject:self.receiveHeadwearWebp forKey:@"receiveHeadwearWebp"];
    }

    [dataDict setObject:self.num ? : @"1" forKey:@"num"];
    [dataDict setObject:self.curSel ?: @"1" forKey:@"curSel"];
    [dataDict setObject:self.sumSel ?: @"1" forKey:@"sumSel"];
    [dataDict setObject:self.receive_user_type?:@"" forKey:@"receive_user_type"];
    NSMutableArray * array = [NSMutableArray new];
    for (SPDVoiceLiveUserModel * model in self.receiveUser) {
        NSMutableDictionary * dict = [NSMutableDictionary new];
        [dict setValue:model._id forKey:@"_id"];
        [dict setValue:model.nick_name forKey:@"nick_name"];
        [dict setValue:model.gender forKey:@"gender"];
        [dict setValue:model.avatar forKey:@"avatar"];
        [array addObject:dict];
    }
    [dataDict setObject:array forKey:@"receiveUser"];
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                               options:kNilOptions
                                                 error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        
        if (dictionary) {
            self.content = dictionary[@"content"];
            self.send_ClanName = dictionary[@"send_ClanName"];
            self.send_ClanId  = dictionary[@"send_ClanId"];
            self.gender  = dictionary[@"gender"];
            if (dictionary[@"type"]) {
                self.receive_avatar = dictionary[@"receive_avatar"];
                self.receive_nickname = dictionary[@"receive_nickname"];
                self.receive_Id = dictionary[@"receive_Id"];
                self.receive_gender = dictionary[@"receive_gender"];
                self.present_url = dictionary[@"present_url"];
                self.gift_id = dictionary[@"gift_id"];
                self.type    = dictionary[@"type"];
                self.send_msg_world = dictionary[@"send_msg_world"];
            }
            if (dictionary[@"mentionedType"]) {
                self.mentionedType = dictionary[@"mentionedType"];
                self.mentionedList = dictionary[@"mentionedList"];
            }
            self.send_headwear = dictionary[@"send_headwear"]?:@"";
            self.receive_headwear = dictionary[@"receive_headwear"]?:@"";
            self.num = dictionary[@"num"] ? : @"1";
            self.curSel = dictionary[@"curSel"];
            self.sumSel = dictionary[@"sumSel"];
            self.receive_user_type = dictionary[@"receive_user_type"];
            self.receiveUser = dictionary[@"receiveUser"];
            self.receiveHeadwearWebp = dictionary[@"receiveHeadwearWebp"];
//            self.senderHeadwearWebp = dictionary[@"senderHeadwearWebp"];
            [self decodeWithDict:dictionary];
        }
    }
}

/// 会话列表中显示的摘要
- (NSString *)conversationDigest {
    return self.content;
}

///消息的类型名
+ (NSString *)getObjectName {
    return SPDWorldChatMessageIdentifier;
}


@end
