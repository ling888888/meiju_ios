//
//  SPDTipsMessage.h
//  SimpleDate
//
//  Created by Monkey on 2018/1/9.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

@interface SPDTipsMessage : RCMessageContent

@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *orderId;

+ (instancetype)tipsMessageWithTips:(NSString *)tips;

@end
