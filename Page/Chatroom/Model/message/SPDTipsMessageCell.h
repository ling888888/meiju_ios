//
//  SPDTipsMessageCell.h
//  SimpleDate
//
//  Created by Monkey on 2018/1/9.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

@interface SPDTipsMessageCell : RCMessageBaseCell

@property (nonatomic,strong) UILabel *messageLabel;

@property (nonatomic,strong) UIView  *balckBackView;

+ (CGSize)getMessageCellSize:(NSString *)content withWidth:(CGFloat)width;
@end
