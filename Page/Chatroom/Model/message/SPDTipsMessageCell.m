//
//  SPDTipsMessageCell.m
//  SimpleDate
//
//  Created by Monkey on 2018/1/9.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDTipsMessageCell.h"
#import "SPDTipsMessage.h"

@implementation SPDTipsMessageCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}


- (void)initialize {
    
    self.balckBackView = [[UIView alloc]initWithFrame:CGRectZero];
    self.balckBackView .backgroundColor = [UIColor colorWithWhite:0 alpha:0.2
                                           ];
    self.balckBackView .layer.cornerRadius =12;
    self.balckBackView.clipsToBounds =YES;
    [self.baseContentView addSubview:self.balckBackView];
    
    self.messageLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kScreenW, 12)];
    self.messageLabel.font = [UIFont systemFontOfSize:12];
    self.messageLabel.textColor = SPD_HEXCOlOR(@"#ffffff");
    self.messageLabel.numberOfLines = 0;
    self.messageLabel.textAlignment = NSTextAlignmentCenter;
    [self.balckBackView addSubview:self.messageLabel];
}

- (void)setDataModel:(RCMessageModel *)model {
    [super setDataModel:model];
    
    [self updateUI];
}

-(void)updateUI {
    
    CGRect contentsizeframe = self.baseContentView.frame;
    contentsizeframe.origin.x =0;
    contentsizeframe.size.width = kScreenW;
    contentsizeframe.size.height = 40;
    self.baseContentView.frame = contentsizeframe;
    SPDTipsMessage *_textMessage = (SPDTipsMessage *)self.model.content;
    self.messageLabel.text = SPDStringWithKey(_textMessage.message, nil);
//    CGSize _messageSize = [self getAttributeSizeWithText:SPDStringWithKey(_textMessage.message, nil) fontSize:12];
     CGSize _messageSize= [self labelAutoCalculateRectWith:SPDStringWithKey(_textMessage.message, nil) FontSize:12 MaxSize:CGSizeMake(kScreenW-50, 50)];
    CGRect balckBackViewRect = self.balckBackView.frame;
    balckBackViewRect.size.width = _messageSize.width+20;
    balckBackViewRect.origin.x = (contentsizeframe.size.width -balckBackViewRect.size.width)/2;
    balckBackViewRect.size.height = _messageSize.height+10;
    self.balckBackView.frame = balckBackViewRect;

    CGRect messageLabelRect = self.messageLabel.frame;
    messageLabelRect.origin.x = 10;
    messageLabelRect.origin.y= 5;
    messageLabelRect .size.width = _messageSize.width;
    messageLabelRect.size.height = _messageSize.height;
    self.messageLabel.frame = messageLabelRect;
}

-(CGSize) getAttributeSizeWithText:(NSString *)text fontSize:(int)fontSize
{
    CGSize size=[text sizeWithAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:fontSize]}];
    NSAttributedString *attributeSting = [[NSAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:fontSize]}];
    size = [attributeSting size];
    return size;
}
+(CGSize)sizeForMessageModel:(RCMessageModel *)model withCollectionViewWidth:(CGFloat)collectionViewWidth referenceExtraHeight:(CGFloat)extraHeight{
    
    return CGSizeMake(kScreenW, 40+extraHeight);
}

- (CGSize)labelAutoCalculateRectWith:(NSString*)text FontSize:(CGFloat)fontSize MaxSize:(CGSize)maxSize {
    
    NSMutableParagraphStyle* paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode=NSLineBreakByWordWrapping;
    NSDictionary* attributes =@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize],NSParagraphStyleAttributeName:paragraphStyle.copy};
    //如果系统为iOS7.0；
    CGSize labelSize;
    if (![text respondsToSelector:@selector(boundingRectWithSize:options:attributes:context:)]){
        labelSize = [text sizeWithFont:[UIFont systemFontOfSize:fontSize] constrainedToSize:maxSize lineBreakMode:NSLineBreakByWordWrapping];
    }else{
            labelSize = [text boundingRectWithSize: maxSize
                                           options: NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine
                                        attributes:attributes
                                           context:nil].size;
        }
    labelSize.height=ceil(labelSize.height);
    labelSize.width=ceil(labelSize.width);
    return labelSize;
    
}


@end
