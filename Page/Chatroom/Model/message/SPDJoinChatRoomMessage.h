//
//  SPDJoinChatRoomMessage.h
//  SimpleDate
//
//  Created by Monkey on 2017/5/18.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>
#import "LY_LiveBaseMessageContent.h"
/*!
 的类型名
 */
#define SPDJoinChatRoomMessageIdentifier @"CR:Join"

@interface SPDJoinChatRoomMessage : LY_LiveBaseMessageContent<NSCoding>

//加入消息的内容
@property(nonatomic, strong) NSString *content;

@property(nonatomic, strong) NSString *gender;

//座驾
@property(nonatomic, strong) NSString *vehicle; // 弃用

@property(nonatomic, strong) NSString *vehicle_id;
@property(nonatomic, strong) NSNumber *grade; //座驾的等级 - 控制他显示的位置
@property(nonatomic, strong) NSString *image;
@property(nonatomic, strong) NSString *noble; // 贵族["baron",""]
@property (nonatomic, assign) CGFloat nobleEnterRoomTextLength;
//初始化信息

+ (instancetype)joinChatRoomMessageWithContent:(NSString *)content
                                     andGender:(NSString *)gender
                                    andVehicle:(NSString *)vehicle
                                 andVehicle_id:(NSString *)vehicle_id
                                      andGrade:(NSString *)grade
                                      andImage:(NSString *)image;

@end
