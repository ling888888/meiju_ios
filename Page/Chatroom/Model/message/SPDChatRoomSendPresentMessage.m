//
//  SPDChatRoomSendPresentMessage.m
//  SimpleDate
//
//  Created by Monkey on 2017/6/6.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDChatRoomSendPresentMessage.h"
#import "SPDVoiceLiveUserModel.h"

@implementation SPDChatRoomSendPresentMessage

+ (instancetype)messageWithAvatar:(NSString *)receive_avatar andNickname:(NSString *)receive_nickname andGender:(NSString *)receive_gender andPresent:(NSString *)present_url andSenderGender:(NSString *)sender_gender andPosition:(NSString *)position {
    SPDChatRoomSendPresentMessage * msg = [[SPDChatRoomSendPresentMessage alloc]init];
    if (msg) {
        msg.receive_avatar = receive_avatar;
        msg.receive_gender = receive_gender;
        msg.receive_nickname = receive_nickname;
        msg.present_url = present_url;
        msg.send_gender = sender_gender;
        msg.position   = position;
    }
    return msg;
}

///消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return    MessagePersistent_ISCOUNTED;
}

/// NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.receive_avatar = [aDecoder decodeObjectForKey:@"receive_avatar"]?:@"";
        self.receive_nickname = [aDecoder decodeObjectForKey:@"receive_nickname"]?:@"";
        self.receive_gender = [aDecoder decodeObjectForKey:@"receive_gender"]?:@"";
        self.present_url = [aDecoder decodeObjectForKey:@"present_url"]?:@"";
        self.send_gender = [aDecoder decodeObjectForKey:@"send_gender"]?:@"";
        if ([aDecoder decodeObjectForKey:@"gift_id"]) {
            self.gift_id = [aDecoder decodeObjectForKey:@"gift_id"]?:@"";
            self.position = [aDecoder decodeObjectForKey:@"position"]?:@"";
            self.send_msg_world = [aDecoder decodeObjectForKey:@"send_msg_world"]?:@"";
            self.send_all_chatroom = [aDecoder decodeObjectForKey:@"send_all_chatroom"]?:@"";
        }
        self.num = [aDecoder decodeObjectForKey:@"num"] ? : @"1";
        self.curSel = [aDecoder decodeObjectForKey:@"curSel"]?:@"1";
        self.sumSel = [aDecoder decodeObjectForKey:@"sumSel"]?:@"1";
        self.receive_user_type = [aDecoder decodeObjectForKey:@"receive_user_type"]?:@"all";
        self.receiveUser = [aDecoder decodeObjectForKey:@"receiveUser"];
        
    }
    return self;
}


/// NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.receive_avatar?:@"" forKey:@"receive_avatar"];
    [aCoder encodeObject:self.receive_nickname?:@"" forKey:@"receive_nickname"];
    [aCoder encodeObject:self.receive_gender?:@"" forKey:@"receive_gender"];
    [aCoder encodeObject:self.present_url?:@"" forKey:@"present_url"];
    [aCoder encodeObject:self.send_gender?:@"" forKey:@"send_gender"];
    if (self.gift_id) {
        [aCoder encodeObject:self.gift_id?:@"" forKey:@"gift_id"];
        [aCoder encodeObject:self.position ?:@""forKey:@"position"];
        [aCoder encodeObject:self.send_msg_world ?:@""forKey:@"send_msg_world"];
        [aCoder encodeObject:self.send_all_chatroom ?:@""forKey:@"send_all_chatroom"];
    }
    [aCoder encodeObject:self.num ? : @"1" forKey:@"num"];
    [aCoder encodeObject:self.curSel ? : @"1" forKey:@"curSel"];
    [aCoder encodeObject:self.sumSel ? : @"1" forKey:@"sumSel"];
    [aCoder encodeObject:self.receive_user_type ? : @"1" forKey:@"receive_user_type"];

}


///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [NSMutableDictionary dictionary];
    [dataDict setObject:self.receive_avatar?:@"" forKey:@"receive_avatar"];
    [dataDict setObject:self.receive_nickname ?:@""forKey:@"receive_nickname"];
    [dataDict setObject:self.receive_gender?:@"" forKey:@"receive_gender"];
    [dataDict setObject:self.present_url?:@"" forKey:@"present_url"];
    [dataDict setObject:self.send_gender?:@""forKey:@"send_gender"];
    if (self.gift_id) {
        [dataDict setObject:self.gift_id?:@"" forKey:@"gift_id"];
        [dataDict setObject:self.position?:@"" forKey:@"position"];
        [dataDict setObject:self.send_msg_world?:@"" forKey:@"send_msg_world"];
        [dataDict setObject:self.send_all_chatroom?:@"" forKey:@"send_all_chatroom"];
    }
    [dataDict setObject:self.num ?: @"1" forKey:@"num"];
    [dataDict setObject:self.curSel ?: @"1" forKey:@"curSel"];
    [dataDict setObject:self.sumSel ?: @"1" forKey:@"sumSel"];
    [dataDict setObject:self.receive_user_type ?:@"normal" forKey:@"receive_user_type"];
    NSMutableArray * array = [NSMutableArray new];
    for (SPDVoiceLiveUserModel * model in self.receiveUser) {
        NSMutableDictionary * dict = [NSMutableDictionary new];
        [dict setValue:model._id?:@"" forKey:@"_id"];
        [dict setValue:model.nick_name?:@"" forKey:@"nick_name"];
        [dict setValue:model.gender?:@"" forKey:@"gender"];
        [dict setValue:model.avatar?:@"" forKey:@"avatar"];
        [array addObject:dict];
    }
    [dataDict setObject:array forKey:@"receiveUser"];

    if (self.senderUserInfo) {
        NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc] init];
        if (self.senderUserInfo.name) {
            [userInfoDic setObject:self.senderUserInfo.name
                 forKeyedSubscript:@"name"];
        }
        if (self.senderUserInfo.portraitUri) {
            [userInfoDic setObject:self.senderUserInfo.portraitUri
                 forKeyedSubscript:@"icon"];
        }
        if (self.senderUserInfo.userId) {
            [userInfoDic setObject:self.senderUserInfo.userId
                 forKeyedSubscript:@"id"];
        }
        [dataDict setObject:userInfoDic forKey:@"user"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        
        if (dictionary) {
            self.receive_avatar = dictionary[@"receive_avatar"]?:@"";
            self.receive_nickname = dictionary[@"receive_nickname"]?:@"";
            self.receive_gender  = dictionary[@"receive_gender"]?:@"";
            self.present_url  = dictionary[@"present_url"]?:@"";
            self.send_gender = dictionary[@"send_gender"]?:@"";
            if (dictionary[@"gift_id"]) {
                self.gift_id = dictionary[@"gift_id"]?:@"";
                self.position = dictionary[@"position"]?:@"";
                self.send_all_chatroom = dictionary[@"send_all_chatroom"]?:@"";
                self.send_msg_world = dictionary[@"send_msg_world"]?:@"";
  
            }
            self.num = dictionary[@"num"] ? : @"1";
            self.receiveUser = dictionary[@"receiveUser"];
            self.sumSel = dictionary[@"sumSel"]?:@"";
            self.curSel = dictionary[@"curSel"]?:@"";
            self.receive_user_type = dictionary[@"receive_user_type"]?:@"all";
            NSDictionary *userinfoDic = dictionary[@"user"];
            [self decodeUserInfo:userinfoDic];
        }
    }
}

///消息的类型名
+ (NSString *)getObjectName {
    return SPDChatRoomSendPresentMessageIdentifier;
}

@end
