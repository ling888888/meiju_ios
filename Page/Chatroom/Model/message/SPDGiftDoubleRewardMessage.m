//
//  SPDGiftDoubleRewardMessage.m
//  SimpleDate
//
//  Created by Monkey on 2018/7/23.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDGiftDoubleRewardMessage.h"

@implementation SPDGiftDoubleRewardMessage
/// 消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return MessagePersistent_ISCOUNTED;
}

/// NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.gift_name = [aDecoder decodeObjectForKey:@"gift_name"];
        self.reward_rate = [aDecoder decodeObjectForKey:@"reward_rate"];
        self.reward_count = [aDecoder decodeObjectForKey:@"reward_count"];
    }
    return self;
}


/// NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.gift_name forKey:@"gift_name"];
    [aCoder encodeObject:self.reward_rate forKey:@"reward_rate"];
    [aCoder encodeObject:self.reward_count forKey:@"reward_count"];
}


/// 将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [NSMutableDictionary dictionary];
    [dataDict setObject:self.gift_name forKey:@"gift_name"];
    [dataDict setObject:self.reward_rate forKey:@"reward_rate"];
    [dataDict setObject:self.reward_count forKey:@"reward_count"];
    
    if (self.senderUserInfo) {
        NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc] init];
        if (self.senderUserInfo.name) {
            [userInfoDic setObject:self.senderUserInfo.name
                 forKeyedSubscript:@"name"];
        }
        if (self.senderUserInfo.portraitUri) {
            [userInfoDic setObject:self.senderUserInfo.portraitUri
                 forKeyedSubscript:@"icon"];
        }
        if (self.senderUserInfo.userId) {
            [userInfoDic setObject:self.senderUserInfo.userId
                 forKeyedSubscript:@"id"];
        }
        [dataDict setObject:userInfoDic forKey:@"user"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

/// 将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        
        if (dictionary) {
            self.gift_name = dictionary[@"gift_name"];
            self.reward_rate = dictionary[@"reward_rate"];
            self.reward_count = dictionary[@"reward_count"];
            NSDictionary *userinfoDic = dictionary[@"user"];
            [self decodeUserInfo:userinfoDic];
        }
    }
}

/// 会话列表中显示的摘要
- (NSString *)conversationDigest {
    
    NSString *content = [NSString stringWithFormat:SPDStringWithKey(@"你在刷幸运礼物%@中获得%@倍奖励,奖励金币%@个已发至你的钱包中,快去查看吧~", nil), self.gift_name, self.reward_rate, self.reward_count];
    return content;
}

/// 消息的类型名
+ (NSString *)getObjectName {
    return SPDGiftDoubleRewardMessageIdentifier;
}

@end
