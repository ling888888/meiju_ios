//
//  SPDTextMessage.h
//  SimpleDate
//
//  Created by Monkey on 2017/6/16.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "LY_LiveBaseMessageContent.h"

#define SPDTextMessageIdentifier @"CR:TxtMsg"

@interface SPDTextMessage : LY_LiveBaseMessageContent<NSCoding>

@property(nonatomic, strong) NSString *content;

@property (nonatomic, strong) NSMutableAttributedString * messageAttributeString; // 属性字符串

@property (nonatomic, assign) CGFloat contentHeight; // 内容高度

@property(nonatomic, strong) NSString *age;

@property(nonatomic, strong) NSString *gender;

@property(nonatomic, strong) NSString *clan_level;

@property(nonatomic, strong) NSString *rich_level;//rich charam 都是转化后的值 客户端约定好的
@property(nonatomic, strong) NSString *user_level;//代表用户等级

@property(nonatomic, strong) NSString *charm_level;

@property (nonatomic, strong) NSString *mentionedType; // @类型 "1": @全部用户, "2": @部分用户

@property (nonatomic, strong) NSArray *mentionedList; // @用户列表 [{"user_id": "", "nick_name": "", "avatar": "" // 地址全称}]

@property(nonatomic, strong) NSString *is_greetings; // 是否是欢迎语 【true false】

@property (nonatomic, strong) NSString *specialNum; // 靓号
@property (nonatomic, assign) BOOL is_agent_gm; // 是否是代理客服
@property (nonatomic, strong) NSString *noble; // 贵族
@property (nonatomic, assign) BOOL isHaveClan; // 是否有家族

@property (nonatomic , strong) NSString * lucky_number; // 6 如果不为空话 代表是幸运数字消息全都是黑色背景 空的情况代表是普通文本消息

@property (nonatomic, strong) NSString *usecover_ios; // 气泡
@property (nonatomic, strong) NSString *usecover_ios_ar; // 气泡
@property (nonatomic, strong) NSString *usecover_android; // 安卓
@property (nonatomic, strong) NSString *usecover_android_ar; // 安卓
@property (nonatomic, strong) NSString *text_color; // 气泡字体颜色
@property (nonatomic, strong) NSString *headwear; // 头饰
@property (nonatomic, strong) NSString *medal; // 头饰

+(SPDTextMessage *)spdTextMessageWith:(NSString *)content Age:(NSString *)age Gender:(NSString *)gender Clan_level:(NSString *)clan_level andRich_level:(NSString *)rich_level Charm_level:(NSString *)charm_level User_level:(NSString *)user_level;

@end
