//
//  SPDLotteryMessage.m
//  SimpleDate
//
//  Created by Monkey on 2018/4/9.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDLotteryMessage.h"

static float cell_gh = 0;

@implementation SPDLotteryMessage

/// NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.status = [aDecoder decodeObjectForKey:@"status"];
        self.current_date = [aDecoder decodeObjectForKey:@"current_date"];
        self.sel_num = [aDecoder decodeObjectForKey:@"sel_num"];
        self.win_num = [aDecoder decodeObjectForKey:@"win_num"];
        if ([aDecoder decodeObjectForKey:@"reward"]) {
            self.reward = [aDecoder decodeObjectForKey:@"reward"];
        }
        if ([aDecoder decodeObjectForKey:@"double_num"]) {
            self.double_num = [aDecoder decodeObjectForKey:@"double_num"];
        }
    }
    return self;
}


/// NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.status forKey:@"status"];
    [aCoder encodeObject:self.current_date forKey:@"current_date"];
    [aCoder encodeObject:self.sel_num forKey:@"sel_num"];
    [aCoder encodeObject:self.win_num forKey:@"win_num"];
    if (self.reward) {
        [aCoder encodeObject:self.reward forKey:@"reward"];
    }
    if (self.double_num) {
        [aCoder encodeObject:self.double_num forKey:@"double_num"];
    }
}


///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [NSMutableDictionary dictionary];
    [dataDict setObject:self.status forKey:@"status"];
    [dataDict setObject:self.current_date forKey:@"current_date"];
    [dataDict setObject:self.sel_num forKey:@"sel_num"];
    [dataDict setObject:self.win_num forKey:@"win_num"];
    if (self.reward) {
        [dataDict setObject:self.reward forKey:@"reward"];
    }
    if (self.double_num) {
        [dataDict setObject:self.double_num forKey:@"double_num"];
    }
    if (self.senderUserInfo) {
        NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc] init];
        if (self.senderUserInfo.name) {
            [userInfoDic setObject:self.senderUserInfo.name
                 forKeyedSubscript:@"name"];
        }
        if (self.senderUserInfo.portraitUri) {
            [userInfoDic setObject:self.senderUserInfo.portraitUri
                 forKeyedSubscript:@"icon"];
        }
        if (self.senderUserInfo.userId) {
            [userInfoDic setObject:self.senderUserInfo.userId
                 forKeyedSubscript:@"id"];
        }
        [dataDict setObject:userInfoDic forKey:@"user"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        
        if (dictionary) {
            self.status = dictionary[@"status"];
            self.current_date = dictionary[@"current_date"];
            self.sel_num = dictionary[@"sel_num"];
            self.win_num = dictionary[@"win_num"];
            if (dictionary[@"reward"]) {
                self.reward = dictionary[@"reward"];
            }
            if (dictionary[@"double_num"]) {
                self.double_num = dictionary[@"double_num"];
            }
            NSDictionary *userinfoDic = dictionary[@"user"];
            [self decodeUserInfo:userinfoDic];
        }
    }
}


///消息的类型名
+ (NSString *)getObjectName {
    return SPDLotteryMessageIdentifier;
}

- (NSString *)conversationDigest {
    if ([self.status isEqualToString:@"win"]) {
        //再判断是否翻倍
        NSString * content = self.double_num ? [NSString stringWithFormat:SPDStringWithKey(@"%@期开奖幸运数字是\"%@\"，您下注数字是\"%@\"投注%@倍，包含中将号码；恭喜中奖，%@奖励金币已发至你的钱包中", nil), self.current_date,self.win_num,self.sel_num,self.double_num,self.reward] : [NSString stringWithFormat:SPDStringWithKey(@"%@期开奖幸运数字是\"%@\"，您下注数字是\"%@\"，包含中将号码；恭喜中奖，%@奖励金币已发至你的钱包中",nil), self.current_date,self.win_num,self.sel_num,self.reward];
        return content;

    }else {
        NSString * content =  [NSString stringWithFormat:SPDStringWithKey(@"%@期开奖幸运数字是\"%@\"，您下注数字是\"%@\"，很遗憾没有中奖，不要气馁，再接再厉", nil), self.current_date,self.win_num,self.sel_num];
        return content;
    }
    
}

- (CGFloat)cell_height {
    if (cell_gh != 0) {
        return cell_gh;
    }else{
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        [style setLineSpacing:3];
        NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:13], NSParagraphStyleAttributeName:style};
        CGFloat height = [self.conversationDigest boundingRectWithSize:CGSizeMake(pixwn(275), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:dic context:nil].size.height; // 文字高度加 间距
        return height;
    }
}



@end
