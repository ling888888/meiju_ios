//
//  SPDChatroomRedPacketMessage.h
//  SimpleDate
//
//  Created by 李楠 on 2018/9/4.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

@interface SPDChatroomRedPacketMessage : RCMessageContent

@property (nonatomic, copy) NSString *redPacketId;

@end
