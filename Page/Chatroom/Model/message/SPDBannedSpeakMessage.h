//
//  SPDBannedSpeakMessage.h
//  SimpleDate
//
//  Created by Monkey on 2017/5/20.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

#define SPDBannedSpeakMessageIdentifier @"bannedinformationMsg"

@interface SPDBannedSpeakMessage : RCMessageContent

//禁言消息的内容-草莓姑娘被禁言三分钟

@property(nonatomic, strong) NSString *message;

+(instancetype)bannedSpeakMessageWith:(NSString *)message;

@end
