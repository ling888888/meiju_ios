//
//  SPDCRSendMagicMessage.h
//  SimpleDate
//
//  Created by Monkey on 2017/9/27.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>
#import "LY_LiveBaseMessageContent.h"

#define SPDCRSendMagicMessageIdentifier @"CR:SendMagic"

//即发送在聊天室里面了 也发送了世界的

@interface SPDCRSendMagicMessage : LY_LiveBaseMessageContent

@property(nonatomic, strong) NSString *send_gender;

@property(nonatomic, strong) NSString *receive_id;

@property(nonatomic, strong) NSString *receive_avatar;

@property(nonatomic, strong) NSString *receive_nickname;

@property(nonatomic, strong) NSString *receive_gender;

@property(nonatomic, strong) NSString *magic_url; //魔法展示图片

@property(nonatomic, strong) NSString *magic_effect; //魔法的效果

@property(nonatomic,strong) NSString * magic_id; //要根据id 来取 动效

@property(nonatomic,strong) NSString * result; //使用魔法的成功还是失败 success fail

@property(nonatomic,strong) NSString * type; //kicked banned_talk

@property(nonatomic,strong) NSString * magic_effect_value; // 多少秒

@property(nonatomic,strong) NSString * clan_id; //

@property(nonatomic,strong) NSString * clan_name; //

@property(nonatomic,strong) NSString * is_cost; //标志 是否使用金币发送魔法 true false

@property (nonatomic,strong) NSString * send_headwear; // send方的 贵族名称 无贵族 不传递这个

@property (nonatomic,strong) NSString * receive_headwear; // send方的 贵族名称 无贵族 不传递这个

@property(nonatomic,strong) NSString * receiveHeadwearWebp; //


@end
