//
//  SPDTipsMessage.m
//  SimpleDate
//
//  Created by Monkey on 2018/1/9.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDTipsMessage.h"

@implementation SPDTipsMessage

+ (instancetype)tipsMessageWithTips:(NSString *)tips {
    SPDTipsMessage *tipsMessage = [SPDTipsMessage new];
    tipsMessage.message = tips;
    return tipsMessage;
}

///消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return    MessagePersistent_ISCOUNTED;
}

/// NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.message = [aDecoder decodeObjectForKey:@"message"];
    }
    return self;
}

/// NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.message ? : @"" forKey:@"message"];
    [aCoder encodeObject:self.type ? : @"" forKey:@"message"];
    [aCoder encodeObject:self.orderId ? : @"" forKey:@"message"];
}

///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [NSMutableDictionary dictionary];
    [dataDict setObject:self.message ? : @"" forKey:@"message"];
    [dataDict setObject:self.type ? : @"" forKey:@"type"];
    [dataDict setObject:self.orderId ? : @"" forKey:@"orderId"];
    if (self.senderUserInfo) {
        NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc] init];
        if (self.senderUserInfo.name) {
            [userInfoDic setObject:self.senderUserInfo.name ? : @""
                 forKeyedSubscript:@"name"];
        }
        if (self.senderUserInfo.portraitUri) {
            [userInfoDic setObject:self.senderUserInfo.portraitUri ? : @""
                 forKeyedSubscript:@"icon"];
        }
        if (self.senderUserInfo.userId) {
            [userInfoDic setObject:self.senderUserInfo.userId ? : @""
                 forKeyedSubscript:@"id"];
        }
        [dataDict setObject:userInfoDic forKey:@"user"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        
        if (dictionary) {
            self.message = dictionary[@"message"];
            self.type = dictionary[@"type"];
            self.orderId = dictionary[@"orderId"];
            NSDictionary *userinfoDic = dictionary[@"user"];
            [self decodeUserInfo:userinfoDic];
        }
    }
}

/// 会话列表中显示的摘要
- (NSString *)conversationDigest {
    return SPDStringWithKey(self.message, nil);
}

///消息的类型名
+ (NSString *)getObjectName {
    return @"SPD:tipsMsg";
}

@end
