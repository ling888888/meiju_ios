//
//  SPDCRSendMagicMessage.m
//  SimpleDate
//
//  Created by Monkey on 2017/9/27.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDCRSendMagicMessage.h"
#import "NSObject+Coding.h"

@implementation SPDCRSendMagicMessage

+ (RCMessagePersistent)persistentFlag {
    return    MessagePersistent_ISCOUNTED;
}


///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [self encodeBaseDict];
    [dataDict setObject:self.receive_avatar forKey:@"receive_avatar"];
    [dataDict setObject:self.receive_nickname forKey:@"receive_nickname"];
    [dataDict setObject:self.receive_gender forKey:@"receive_gender"];
    [dataDict setObject:self.magic_url forKey:@"magic_url"];
    [dataDict setObject:self.send_gender forKey:@"send_gender"];
    [dataDict setObject:self.magic_effect forKey:@"magic_effect"];
    [dataDict setObject:self.result forKey:@"result"];
    [dataDict setObject:self.receive_id forKey:@"receive_id"];
    [dataDict setObject:self.magic_effect_value forKey:@"magic_effect_value"];
    [dataDict setObject:self.type forKey:@"type"];
    [dataDict setObject:self.is_cost forKey:@"is_cost"];
    if (self.magic_id) {
        [dataDict setObject:self.magic_id forKey:@"magic_id"];
    }
    if (self.clan_id) {
        [dataDict setObject:self.clan_id forKey:@"clan_id"];
    }
    if (self.clan_name) {
        [dataDict setObject:self.clan_name forKey:@"clan_name"];
    }
    if (self.send_headwear) {
        [dataDict setObject:self.send_headwear forKey:@"send_headwear"];
    }
    if (self.receive_headwear) {
        [dataDict setObject:self.receive_headwear forKey:@"receive_headwear"];
    }
    if (self.receiveHeadwearWebp) {
        [dataDict setObject:self.receiveHeadwearWebp forKey:@"receiveHeadwearWebp"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        
        if (dictionary) {
            [self decodeWithDict:dictionary];
            
            self.receive_avatar = dictionary[@"receive_avatar"];
            self.receive_nickname = dictionary[@"receive_nickname"];
            self.receive_gender  = dictionary[@"receive_gender"];
            self.magic_url  = dictionary[@"magic_url"];
            self.send_gender = dictionary[@"send_gender"];
            self.magic_effect = dictionary [@"magic_effect"];
            self.clan_name = dictionary [@"clan_name"];
            self.clan_id = dictionary[@"clan_id"];
            self.type = dictionary[@"type"];
            self.magic_effect_value = dictionary[@"magic_effect_value"];
            self.is_cost = dictionary[@"is_cost"];
            if (dictionary[@"magic_id"]) {
                self.magic_id = dictionary[@"magic_id"];
            }
            self.receive_id = dictionary[@"receive_id"];
            self.result = dictionary[@"result"];
            if (dictionary[@"send_headwear"]) {
                self.send_headwear = dictionary[@"send_headwear"];
            }
            if (dictionary[@"receive_headwear"]) {
                self.receive_headwear = dictionary[@"receive_headwear"];
            }
            if (dictionary[@"receiveHeadwearWebp"]) {
                self.receiveHeadwearWebp = dictionary[@"receiveHeadwearWebp"];
            }
            NSDictionary *userinfoDic = dictionary[@"user"];
            [self decodeUserInfo:userinfoDic];
        }
    }
}

///消息的类型名
+ (NSString *)getObjectName {
    return SPDCRSendMagicMessageIdentifier;
}

@end
