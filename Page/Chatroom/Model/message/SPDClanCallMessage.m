//
//  SPDClanCallMessage.m
//  SimpleDate
//
//  Created by Monkey on 2017/6/22.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDClanCallMessage.h"
#import "NSObject+Coding.h"

@implementation SPDClanCallMessage

+(instancetype)clanCallMessageWithContent:(NSString *)content ClanId:(NSString *)clan_id andClan_name:(NSString *)clan_name andPushContent:(NSString *)pushContent{

    SPDClanCallMessage *clanMsg = [[SPDClanCallMessage alloc]init];
    if (clanMsg) {
        clanMsg.content = content;
        clanMsg.clan_id = clan_id;
        clanMsg.clan_name = clan_name;
        clanMsg.pc = pushContent;
    }
    return clanMsg;
}
///消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return    MessagePersistent_ISCOUNTED;
}

/// NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
//        self.content = [aDecoder decodeObjectForKey:@"content"];
//        self.clan_id = [aDecoder decodeObjectForKey:@"clan_id"];
//        self.clan_name = [aDecoder decodeObjectForKey:@"clan_name"];
//        self.pc = [aDecoder decodeObjectForKey:@"pc"];
//        self.lang = [aDecoder decodeObjectForKey:@"lang"];
        [self hl_decodeWithDeCoder:aDecoder];
    }
    return self;
}


/// NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
//    [aCoder encodeObject:self.content forKey:@"content"];
//    [aCoder encodeObject:self.clan_id forKey:@"clan_id"];
//    [aCoder encodeObject:self.clan_name forKey:@"clan_name"];
//    [aCoder encodeObject:self.pc  forKey:@"pc"];
//    [aCoder encodeObject:self.lang  forKey:@"lang"];
    [self hl_encodeWithCoder:aCoder];
}


///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [NSMutableDictionary dictionary];
    [dataDict setObject:self.content forKey:@"content"];
    [dataDict setObject:self.clan_id forKey:@"clan_id"];
    [dataDict setObject:self.clan_name forKey:@"clan_name"];
    [dataDict setObject:self.pc forKey:@"pc"];
    [dataDict setObject:self.lang forKey:@"lang"];

    if (self.senderUserInfo) {
        NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc] init];
        if (self.senderUserInfo.name) {
            [userInfoDic setObject:self.senderUserInfo.name
                 forKeyedSubscript:@"name"];
        }
        if (self.senderUserInfo.portraitUri) {
            [userInfoDic setObject:self.senderUserInfo.portraitUri
                 forKeyedSubscript:@"icon"];
        }
        if (self.senderUserInfo.userId) {
            [userInfoDic setObject:self.senderUserInfo.userId
                 forKeyedSubscript:@"id"];
        }
        [dataDict setObject:userInfoDic forKey:@"user"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        
        if (dictionary) {
            self.content = dictionary[@"content"];
            self.clan_id = dictionary[@"clan_id"];
            self.clan_name = dictionary[@"clan_name"];
            self.pc = dictionary[@"pc"];
            self.lang = dictionary[@"lang"];
            NSDictionary *userinfoDic = dictionary[@"user"];
            [self decodeUserInfo:userinfoDic];
        }
    }
}

/// 会话列表中显示的摘要
- (NSString *)conversationDigest {
    return self.pc;
}

///消息的类型名
+ (NSString *)getObjectName {
    return SPDClanCallMessageIdentifier;
}


@end
