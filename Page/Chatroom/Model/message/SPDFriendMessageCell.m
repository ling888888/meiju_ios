//
//  SPDFriendMessageCell.m
//  SimpleDate
//
//  Created by Monkey on 2018/2/1.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDFriendMessageCell.h"
#import "SPDFriendMessage.h"


@interface SPDFriendMessageCell ()
@property (nonatomic,strong)UIButton * titleBtn;
@property (nonatomic,strong)UIButton * fuctionBtnOne;
@property (nonatomic,strong)UIButton * fuctionBtnTwo;
@property (nonatomic,strong)UIImageView * backView; //bg图片
@property (nonatomic,strong)UILabel *contentLabel; //内容label

@end

@implementation SPDFriendMessageCell
- (NSDictionary *)attributeDictionary {
    if (self.messageDirection == MessageDirection_SEND) {
        return @{
                 @(NSTextCheckingTypeLink) : @{NSForegroundColorAttributeName : [UIColor blueColor]},
                 @(NSTextCheckingTypePhoneNumber) : @{NSForegroundColorAttributeName : [UIColor blueColor]}
                 };
    } else {
        return @{
                 @(NSTextCheckingTypeLink) : @{NSForegroundColorAttributeName : [UIColor blueColor]},
                 @(NSTextCheckingTypePhoneNumber) : @{NSForegroundColorAttributeName : [UIColor orangeColor]}
                 };
    }
    return nil;
}

- (NSDictionary *)highlightedAttributeDictionary {
    return [self attributeDictionary];
}
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    self.userInteractionEnabled = YES;
    //设置bgwidth = 屏幕宽度 减去 两边spcace 左边50
    self.backView = [[UIImageView alloc]init];
    [self.messageContentView addSubview:self.backView];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.messageContentView.mas_top).offset(0);
        make.left.equalTo(self.messageContentView.mas_left).offset(0);
        make.width.mas_equalTo(pixwn(275));
        make.height.mas_equalTo(170);
    }];
    self.backView.userInteractionEnabled = YES;
    self.messageContentView.userInteractionEnabled = YES;
    
    self.titleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.messageContentView addSubview:self.titleBtn];
    [self.titleBtn setUserInteractionEnabled:NO];
    self.titleBtn.titleLabel.textColor = [UIColor whiteColor];
    self.titleBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [self.titleBtn setTitleEdgeInsets:UIEdgeInsetsMake(-4, 0, 0, 0)];
    [self.titleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo((pixwn(275)-111)/2+8);
        make.top.equalTo(self.messageContentView.mas_top).offset(14);
        make.height.mas_equalTo(30);
    }];
    
    self.contentLabel = [UILabel new];
    self.contentLabel.numberOfLines= 0;
    self.contentLabel.textColor = [UIColor whiteColor];
    self.contentLabel.font = [UIFont systemFontOfSize:14];
    self.contentLabel.numberOfLines = 0;
    [self.messageContentView addSubview:self.contentLabel];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.messageContentView.mas_top).offset(53);
        make.leading.equalTo(self.messageContentView.mas_leading).offset(22);
        make.trailing.equalTo(self.messageContentView.mas_trailing).offset(-22);
    }];

}

- (void)setDataModel:(RCMessageModel *)model {
    
    [self cleanData];
    
    [super setDataModel:model];
    
    [self setAutoLayout];
    
}

- (void)cleanData {
    [self.titleBtn setTitle:@"" forState:UIControlStateNormal];
    [self.fuctionBtnOne setTitle:@"" forState:UIControlStateNormal];
    [self.fuctionBtnTwo setTitle:@"" forState:UIControlStateNormal];
    self.backView.image = [UIImage imageNamed:@""];
    [self.fuctionBtnOne removeFromSuperview];
    [self.fuctionBtnTwo removeFromSuperview];
}

-(void)setAutoLayout {
    
    CGRect messageContentFrame = self.messageContentView.frame;
    messageContentFrame.size.width = pixwn(275);
    messageContentFrame.size.height = 170;
    self.messageContentView.frame = messageContentFrame;
    
    SPDFriendMessage * friendMsg = (SPDFriendMessage *)self.model.content;
    NSString * title = @"";
    NSString * btnTitle = @"";
    NSString * btnBgColor = @"";
    NSString * bgImageName = @"";
    NSString * title_bg = @"";
    if( [friendMsg.type isEqualToString:@"request"]){
        title = @"好友请求";
    }else if ([friendMsg.type isEqualToString:@"accept"]){
        title = @"同意请求";
        btnTitle = @"我的好友";
        btnBgColor = @"ff4682";

    }else if ([friendMsg.type isEqualToString:@"refuse"]){
        title = @"拒绝请求";
        btnTitle = @"添加好友";
        btnBgColor = @"1d92ff";
    }
    CGSize  size = [SPDCommonTool getAttributeSizeWithText:SPDStringWithKey(title, nil) fontSize:13];
    CGFloat titleWidth = size.width < 111 ? 111 : size.width +20;
    [self.titleBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo((pixwn(275)-titleWidth)/2+8);
        make.width.mas_equalTo(titleWidth);
    }];
    [self.titleBtn setTitle:SPDStringWithKey(title, nil) forState:UIControlStateNormal];
    
    [self.titleBtn setImageEdgeInsets:UIEdgeInsetsMake(10, 0, 0, 0)];
    title_bg = [NSString stringWithFormat:@"%@_title",friendMsg.type];
    [self.titleBtn setBackgroundImage:[UIImage imageNamed:title_bg] forState:UIControlStateNormal];
    self.contentLabel.text = SPDStringWithKey(friendMsg.content, nil);
    bgImageName = [NSString stringWithFormat:@"%@_friend",friendMsg.type];
    self.backView.image = [UIImage imageNamed:bgImageName];
    
    if ([friendMsg.type isEqualToString:@"request"]) {
        [self.messageContentView addSubview:self.fuctionBtnOne];
        [self.fuctionBtnOne mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo((pixwn(275)-140-24)/2+8);
            make.bottom.equalTo(self.messageContentView.mas_bottom).offset(-15);
            make.height.mas_equalTo(29);
            make.width.mas_equalTo(70);
        }];
        
        self.fuctionBtnTwo = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.messageContentView addSubview:self.fuctionBtnTwo];
        [self.fuctionBtnTwo  addTarget:self action:@selector(handleAgree:)
                      forControlEvents:UIControlEventTouchUpInside];
        self.fuctionBtnTwo.layer.cornerRadius = 12;
        self.fuctionBtnTwo.clipsToBounds = YES;
        self.fuctionBtnTwo.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.fuctionBtnTwo mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(self.fuctionBtnOne.mas_trailing).offset(24);
            make.bottom.equalTo(self.messageContentView.mas_bottom).offset(-15);
            make.height.mas_equalTo(29);
            make.width.mas_equalTo(70);
        }];
        [self.fuctionBtnOne setTitle:SPDStringWithKey(@"拒绝", nil) forState:UIControlStateNormal];
        [self.fuctionBtnTwo setTitle:SPDStringWithKey(@"同意", nil) forState:UIControlStateNormal];
        
        NSString * color;
        if ([DBUtil queryMessageState:self.model.messageUId]) {
            color=@"#fc7184";
            [self.fuctionBtnOne setBackgroundColor:[UIColor colorWithHexString:color]];
            [self.fuctionBtnTwo setBackgroundColor:[UIColor colorWithHexString:color]];
            self.fuctionBtnOne.titleLabel.textColor = SPD_HEXCOlOR(@"f0f0f0");
            self.fuctionBtnTwo.titleLabel.textColor = SPD_HEXCOlOR(@"f0f0f0");
            self.fuctionBtnOne.userInteractionEnabled = NO;
            self.fuctionBtnTwo.userInteractionEnabled = NO;
        }else{
            color=@"f75168";
            [self.fuctionBtnOne setBackgroundColor:[UIColor colorWithHexString:color]];
            [self.fuctionBtnTwo setBackgroundColor:[UIColor colorWithHexString:color]];
            self.fuctionBtnOne.titleLabel.textColor = SPD_HEXCOlOR(@"ffffff");
            self.fuctionBtnTwo.titleLabel.textColor = SPD_HEXCOlOR(@"ffffff");
            self.fuctionBtnOne.userInteractionEnabled = YES;
            self.fuctionBtnTwo.userInteractionEnabled = YES;
        }
        
    } else {
//        [self.messageContentView addSubview:self.fuctionBtnOne];
//        [self.fuctionBtnOne mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.leading.mas_equalTo((pixwn(275)-140)/2+8);
//            make.bottom.equalTo(self.messageContentView.mas_bottom).offset(-15);
//            make.height.mas_equalTo(29);
//            make.width.mas_equalTo(140);
//        }];
//        [self.fuctionBtnOne setTitle:SPDStringWithKey(btnTitle, nil) forState:UIControlStateNormal];
//        [self.fuctionBtnOne setBackgroundColor:[UIColor colorWithHexString:btnBgColor]];
//        self.fuctionBtnOne.layer.cornerRadius = 12;
//        self.fuctionBtnOne.clipsToBounds = YES;
    }
}

- (UIButton *)fuctionBtnOne {
    if (!_fuctionBtnOne) {
        _fuctionBtnOne = [UIButton buttonWithType:UIButtonTypeCustom];
        _fuctionBtnOne.layer.cornerRadius = 12;
        _fuctionBtnOne.clipsToBounds = YES;
        _fuctionBtnOne.titleLabel.font = [UIFont systemFontOfSize:13];
        [_fuctionBtnOne  addTarget:self action:@selector(handlefuctionBtnOneClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return _fuctionBtnOne;
}

-(void)handlefuctionBtnOneClicked {
    self.model.extra = @"refuse"; //用来区别点击哪个按钮
    [DBUtil recordMessage:self.model.messageUId state:1];
    [self.delegate didTapMessageCell:self.model];
}

- (void)handleAgree:(UIButton *) sender {
    self.model.extra = @"accept";
    [DBUtil recordMessage:self.model.messageUId state:1];
    [self.delegate didTapMessageCell:self.model];
}

//升级SDK之后-
+(CGSize)sizeForMessageModel:(RCMessageModel *)model withCollectionViewWidth:(CGFloat)collectionViewWidth referenceExtraHeight:(CGFloat)extraHeight{
    return CGSizeMake(kScreenW, 170+extraHeight);
}

@end
