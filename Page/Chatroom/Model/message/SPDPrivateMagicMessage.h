//
//  SPDPrivateMagicMessage.h
//  SimpleDate
//
//  Created by Monkey on 2017/10/24.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

#define SPDPrivateMagicMessageIdentifier @"SPD:PrivateSendMagic"


@interface SPDPrivateMagicMessage : RCMessageContent

@property(nonatomic,strong) NSString * magic_id; //要根据id 来取 动效

@property(nonatomic,strong) NSString * magic_name; //魔法名字

@property(nonatomic,strong) NSString * magic_level; //发送者的魔法等级

@property(nonatomic,strong) NSString * magic_cover; //要根据id 来取 动效

@property(nonatomic,strong)NSString * magic_effect; //魔法的效果 禁言

@property(nonatomic,strong)NSString * magic_effect_value; //10s

@property(nonatomic,strong) NSString * magic_result; //成功 失败

@property(nonatomic,assign) BOOL  magic_power; //yes 代表正能量的

@property(nonatomic,strong) NSString * receive_id;

@property(nonatomic,strong) NSString * magic_type;

@property(nonatomic,strong) NSString * send_gender;

// 不需要发送的字段
@property(nonatomic,strong) NSString * receive_avatar;
@property(nonatomic,strong) NSString * receive_gender;
@property(nonatomic,strong) NSString * receive_nickname;

+ (instancetype)privateMagicMessageWith:(NSString *)magic_id
                             magic_name:(NSString *)magic_name
                            magic_level:(NSString *)magic_level
                           magic_effect:(NSString *)magic_effect
                           magic_result:(NSString *)magic_result
                                 power :(BOOL )power
                             receive_id:(NSString *)receive_id
                            magic_cover:(NSString *)magic_cover
                             magic_type:(NSString *)magic_type
                            send_gender:(NSString *)send_gender
                     magic_effect_value:(NSString*)magic_effect_value;

@end
