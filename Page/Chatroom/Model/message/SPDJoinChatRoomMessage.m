//
//  SPDJoinChatRoomMessage.m
//  SimpleDate
//
//  Created by Monkey on 2017/5/18.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDJoinChatRoomMessage.h"
#import "NSObject+Coding.h"
@implementation SPDJoinChatRoomMessage

+ (instancetype)joinChatRoomMessageWithContent:(NSString *)content andGender:(NSString *)gender andVehicle:(NSString *)vehicle andVehicle_id:(NSString *)vehicle_id andGrade:(NSNumber *)grade andImage:(NSString *)image {
    SPDJoinChatRoomMessage *joinMessage = [[SPDJoinChatRoomMessage alloc]init];
    if (joinMessage) {
        joinMessage.content = content;
        joinMessage.gender  = gender;
        joinMessage.vehicle = vehicle;
        joinMessage.vehicle_id = vehicle_id;
        joinMessage.grade = grade;
        joinMessage.image = image;
    }
    return joinMessage;
}


///消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return    MessagePersistent_ISPERSISTED;
}

///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [self encodeBaseDict];
    [dataDict setObject:self.content forKey:@"content"];
    [dataDict setObject:self.gender forKey:@"gender"];
    [dataDict setObject:self.vehicle forKey:@"vehicle"];
    [dataDict setObject:self.vehicle_id forKey:@"vehicle_id"];
    [dataDict setObject:self.grade forKey:@"grade"];
    [dataDict setObject:self.image forKey:@"image"];
    [dataDict setObject:self.noble ? : @"" forKey:@"noble"];
    NSString * txt = [NSString stringWithFormat:SPDStringWithKey(@"%@ 进入房间", nil), self.senderUserInfo.name];
               self.nobleEnterRoomTextLength = [txt sizeWithFont:[UIFont systemFontOfSize:13] andMaxSize:CGSizeMake(kScreenW, 24)].width;
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        
        if (dictionary) {
            
            [self decodeWithDict:dictionary];
            
            self.content = dictionary[@"content"];
            self.gender  = dictionary[@"gender"];
            self.vehicle = dictionary[@"vehicle"];
            self.vehicle_id = dictionary[@"vehicle_id"];
            self.grade = dictionary[@"grade"];
            self.image = dictionary[@"image"];
            self.noble = dictionary[@"noble"];
            NSString * txt = [NSString stringWithFormat:SPDStringWithKey(@"%@ 进入房间", nil), self.senderUserInfo.name];
            self.nobleEnterRoomTextLength = [txt sizeWithFont:[UIFont systemFontOfSize:13] andMaxSize:CGSizeMake(kScreenW, 24)].width;
        }
    }
}

/// 会话列表中显示的摘要
- (NSString *)conversationDigest {
    return self.content;
}

///消息的类型名
+ (NSString *)getObjectName {
    return SPDJoinChatRoomMessageIdentifier;
}


@end
