//
//  CRWorldMsgMessage.h
//  SimpleDate
//
//  Created by Monkey on 2017/9/19.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

#define CRWorldMsgMessageIdentifier @"CR:WorldMsg"

@interface CRWorldMsgMessage : RCMessageContent

@property  (nonatomic,strong)NSString * userId; //user_id

@property  (nonatomic,strong)NSString * clanId;

+(instancetype)cRWorldMsgMessageWithUserId:(NSString *)user_id andClan_id:(NSString *)clan_id ;


@end
