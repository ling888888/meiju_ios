//
//  SPDTextMessage.m
//  SimpleDate
//
//  Created by Monkey on 2017/6/16.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDTextMessage.h"
#import "NSObject+Coding.h"
@implementation SPDTextMessage

+(SPDTextMessage *)spdTextMessageWith:(NSString *)content Age:(NSString *)age Gender:(NSString *)gender Clan_level:(NSString *)clan_level andRich_level:(NSString *)rich_level Charm_level:(NSString *)charm_level User_level:(NSString *)user_level {
    SPDTextMessage *textMsg = [[SPDTextMessage alloc]init];
    if (textMsg) {
        textMsg.content = content;
        textMsg.age = age;
        textMsg.gender = gender;
        textMsg.clan_level= clan_level;
        textMsg.rich_level = rich_level;
        textMsg.charm_level = charm_level;
        textMsg.user_level = user_level;
    }
    return textMsg;
}

/// NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [self hl_encodeWithCoder:aCoder];
}

- (void)setContent:(NSString *)content {
    _content = content;
    self.messageAttributeString = [[NSMutableAttributedString alloc]initWithString:_content];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    [paragraphStyle setLineSpacing:5];
    [self.messageAttributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, _content.length)];
    [self.messageAttributeString addAttribute:NSBaselineOffsetAttributeName value:@(0) range:NSMakeRange(0, _content.length)];
    self.contentHeight = [self getMessageCellSize:self.content withWidth:kScreenW - 90].height;
}

///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [self encodeBaseDict];
    if (self.content) {
        [dataDict setObject:self.content forKey:@"content"];
        self.messageAttributeString = [[NSMutableAttributedString alloc]initWithString:_content];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
        [paragraphStyle setLineSpacing:5];
        [self.messageAttributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, _content.length)];
        [self.messageAttributeString addAttribute:NSBaselineOffsetAttributeName value:@(0) range:NSMakeRange(0, _content.length)];
        self.contentHeight = [self getMessageCellSize:self.content withWidth:kScreenW - 90].height;
    }
    if (self.age) {
        [dataDict setObject:self.age forKey:@"age"];
    }
    if (self.is_greetings) {
        [dataDict setObject:self.is_greetings forKey:@"is_greetings"];
    }
    if (self.gender) {
        [dataDict setObject:self.gender forKey:@"gender"];
    }
    if (self.clan_level) {
        [dataDict setObject:self.clan_level forKey:@"clan_level"];
    }
    if (self.rich_level) {
        [dataDict setObject:self.rich_level forKey:@"rich_level"];
    }
    if (self.charm_level) {
        [dataDict setObject:self.charm_level forKey:@"charm_level"];
    }
    if (self.mentionedType) {
        [dataDict setObject:self.mentionedType forKey:@"mentionedType"];
        [dataDict setObject:self.mentionedList forKey:@"mentionedList"];
    }
    if (self.user_level) {
        [dataDict setObject:self.user_level forKey:@"user_level"];
    }
    if (self.specialNum) {
        [dataDict setObject:self.specialNum forKey:@"specialNum"];
    }
    if (self.is_agent_gm) {
        [dataDict setObject:@(self.is_agent_gm) forKey:@"is_agent_gm"];
    }
    if (self.noble) {
        [dataDict setObject:self.noble forKey:@"noble"];
    }
    if (self.lucky_number) {
        [dataDict setObject:self.lucky_number forKey:@"lucky_number"];
    }
    if (self.usecover_ios) {
        [dataDict setObject:self.usecover_ios forKey:@"usecover_ios"];
    }
    if (self.usecover_ios_ar) {
        [dataDict setObject:self.usecover_ios_ar forKey:@"usecover_ios_ar"];
    }
    if (self.usecover_android_ar) {
        [dataDict setObject:self.usecover_android_ar forKey:@"usecover_android_ar"];
    }
    if (self.usecover_android) {
        [dataDict setObject:self.usecover_android forKey:@"usecover_android"];
    }
    if (self.text_color) {
        [dataDict setObject:self.text_color forKey:@"text_color"];
    }
    if (self.headwear) {
        [dataDict setObject:self.headwear forKey:@"headwear"];
    }
    if (self.medal){
        [dataDict setObject:self.medal forKey:@"medal"];
    }
    [dataDict setObject:@(self.isHaveClan) forKey:@"isHaveClan"];
     
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        
        if (dictionary) {
            [self decodeWithDict:dictionary];
            
            self.content = dictionary[@"content"];
            
            if (dictionary[@"age"]) {
                self.age = dictionary[@"age"];
            }
            if (dictionary[@"gender"]) {
                self.gender = dictionary[@"gender"];
            }
            if (dictionary[@"is_greetings"]) {
                self.is_greetings = dictionary[@"is_greetings"];
            }
            if (dictionary[@"mentionedType"]) {
                self.mentionedType = dictionary[@"mentionedType"];
                self.mentionedList = dictionary[@"mentionedList"];
            }
            if (dictionary[@"clan_level"]) {
                self.clan_level = dictionary[@"clan_level"];
            }
            if (dictionary[@"rich_level"]) {
                self.rich_level = dictionary[@"rich_level"];
            }
            if (dictionary[@"charm_level"]) {
                self.charm_level = dictionary[@"charm_level"];
            }
            if (dictionary[@"user_level"]) {
                self.user_level = dictionary[@"user_level"];
            }
            if (dictionary[@"specialNum"]) {
                self.specialNum = dictionary[@"specialNum"];
            }
            if (dictionary[@"is_agent_gm"]) {
                self.is_agent_gm = [dictionary[@"is_agent_gm"] boolValue];
            }
            if (dictionary[@"noble"]) {
                self.noble = dictionary[@"noble"];
            }
            if (dictionary[@"isHaveClan"]) {
                self.isHaveClan = [dictionary[@"isHaveClan"] boolValue];
            } else {
                self.isHaveClan = YES;
            }
            if (dictionary[@"lucky_number"]) {
                self.lucky_number = dictionary[@"lucky_number"];
            }
            if (dictionary[@"headwear"]) {
                self.headwear = dictionary[@"headwear"];
            }
            if (dictionary[@"usecover_android_ar"]) {
                self.usecover_android_ar = dictionary[@"usecover_android_ar"];
            }
            if (dictionary[@"usecover_android"]) {
                self.usecover_android = dictionary[@"usecover_android"];
            }
            if (dictionary[@"usecover_ios"]) {
                self.usecover_ios = dictionary[@"usecover_ios"];
            }
            if (dictionary[@"usecover_ios_ar"]) {
                self.usecover_ios_ar = dictionary[@"usecover_ios_ar"];
            }
            if (dictionary[@"text_color"]) {
                self.text_color = dictionary[@"text_color"];
            }
            if (dictionary[@"medal"]) {
                self.medal = dictionary[@"medal"];
            }
        }
    }
}

-(CGSize)getMessageCellSize:(NSString *)content withWidth:(CGFloat)width{
    CGSize textSize = CGSizeZero;
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    [paragraphStyle setLineSpacing:5];
    textSize = [content boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSParagraphStyleAttributeName:paragraphStyle,NSBaselineOffsetAttributeName:@(0)} context:nil].size;
    textSize.height = textSize.height+10;
    return textSize;
}


/// 会话列表中显示的摘要
- (NSString *)conversationDigest {
    return self.content;
}

///消息的类型名
+ (NSString *)getObjectName {
    return SPDTextMessageIdentifier;
}


@end
