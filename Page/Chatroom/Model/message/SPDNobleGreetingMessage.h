//
//  SPDNobleGreetingMessage.h
//  SimpleDate
//
//  Created by 李楠 on 2018/9/18.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "LY_LiveBaseMessageContent.h"

@interface SPDNobleGreetingMessage : LY_LiveBaseMessageContent

@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *noble; // 贵族
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, assign) CGSize textSize;

@end
