//
//  SPDGiftDoubleRewardMessage.h
//  SimpleDate
//
//  Created by Monkey on 2018/7/23.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

#define SPDGiftDoubleRewardMessageIdentifier @"SPD:GiftDoubleRewardMsg"


// 送礼物得到奖励金币倍 起源于 mishi uto 礼物
@interface SPDGiftDoubleRewardMessage : RCMessageContent

@property (nonatomic, strong) NSString *gift_name;
@property (nonatomic, strong) NSNumber *reward_rate; // 奖励倍率
@property (nonatomic, strong) NSNumber *reward_count; // 奖励数量

@end
