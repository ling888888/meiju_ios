//
//  SPDLotteryMessage.h
//  SimpleDate
//
//  Created by Monkey on 2018/4/9.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

#define SPDLotteryMessageIdentifier @"SPD:lottery"


//
//开奖时给用户发送的融云消息 中奖／未中奖：（投注翻倍／没有投注翻倍） SPD:lottery
//
//status: 'win'  'nowin'
//
//current_date : ‘2018032701‘ //第**期
//
//sel_num: '1'  //用户投注的号码
//
//win_num: '2'  //开奖号码
//
//reward:  '2000' // 中奖的奖励金币
//
//double_num： '4' ／／ 如果用户翻倍投 就传递这个字段 否则不传递
// 福彩的消息
@interface SPDLotteryMessage : RCMessageContent

@property  (nonatomic,strong)NSString * status; //win nowin

@property (nonatomic,strong)NSString * current_date;

@property (nonatomic,strong)NSString * sel_num;

@property (nonatomic,strong)NSString * win_num;

@property (nonatomic,strong)NSString * reward;

@property (nonatomic,strong)NSString * double_num;

@property (nonatomic, assign)CGFloat cell_height;

@end
