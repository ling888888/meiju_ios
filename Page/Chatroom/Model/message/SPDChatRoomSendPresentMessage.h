//
//  SPDChatRoomSendPresentMessage.h
//  SimpleDate
//
//  Created by Monkey on 2017/6/6.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>
#define SPDChatRoomSendPresentMessageIdentifier @"SPD:chatRoomSPresentMsg"

@class SPDVoiceLiveUserModel;
@interface SPDChatRoomSendPresentMessage : RCMessageContent<NSCoding>

@property(nonatomic, strong) NSString *send_gender;

@property(nonatomic, strong) NSString *receive_avatar;

@property(nonatomic, strong) NSString *receive_nickname;

@property(nonatomic, strong) NSString *receive_id; // 我去

@property(nonatomic, strong) NSString *receive_gender;

@property(nonatomic, strong) NSString *present_url;

@property(nonatomic,strong) NSString * gift_id;

@property(nonatomic,copy)  NSString  *position; //礼物特效显示的位置

@property(nonatomic,copy) NSString  *send_msg_world; //礼物特效之后是否发送世界消息

@property(nonatomic,copy) NSString  *send_all_chatroom; //通知全世界

@property(nonatomic, copy) NSString *num; // 礼物数量

@property(nonatomic, copy) NSString *curSel; // 选中的人的数量
@property(nonatomic, copy) NSString *sumSel; // 总数
@property(nonatomic, copy) NSString *receive_user_type; // 总数
@property(nonatomic, copy) NSArray * receiveUser; // 收到礼物的users array 里面是 dict类型 not ids


+ (instancetype)messageWithAvatar:(NSString *)receive_avatar andNickname:(NSString *)receive_nickname andGender:(NSString *)receive_gender andPresent:(NSString *)present_url andSenderGender:(NSString *)sender_gender andPosition:(NSString *)position;

@end
