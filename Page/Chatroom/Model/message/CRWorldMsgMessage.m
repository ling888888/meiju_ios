//
//  CRWorldMsgMessage.m
//  SimpleDate
//
//  Created by Monkey on 2017/9/19.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "CRWorldMsgMessage.h"

@implementation CRWorldMsgMessage

+(instancetype)cRWorldMsgMessageWithUserId:(NSString *)user_id andClan_id:(NSString *)clan_id {
    CRWorldMsgMessage *msg = [[CRWorldMsgMessage alloc]init];
    if (msg) {
        msg.clanId = clan_id;
        msg.userId = user_id;
    }
    return msg;
}

/// NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.userId = [aDecoder decodeObjectForKey:@"userId"];
        self.clanId = [aDecoder decodeObjectForKey:@"clanId"];
    }
    return self;
}


/// NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.userId forKey:@"userId"];
    [aCoder encodeObject:self.clanId  forKey:@"clanId"];
}


///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [NSMutableDictionary dictionary];
    [dataDict setObject:self.userId forKey:@"userId"];
    [dataDict setObject:self.clanId forKey:@"clanId"];
    
    if (self.senderUserInfo) {
        NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc] init];
        if (self.senderUserInfo.name) {
            [userInfoDic setObject:self.senderUserInfo.name
                 forKeyedSubscript:@"name"];
        }
        if (self.senderUserInfo.portraitUri) {
            [userInfoDic setObject:self.senderUserInfo.portraitUri
                 forKeyedSubscript:@"icon"];
        }
        if (self.senderUserInfo.userId) {
            [userInfoDic setObject:self.senderUserInfo.userId
                 forKeyedSubscript:@"id"];
        }
        [dataDict setObject:userInfoDic forKey:@"user"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        
        if (dictionary) {
            self.userId = dictionary[@"userId"];
            self.clanId = dictionary[@"clanId"];
            NSDictionary *userinfoDic = dictionary[@"user"];
            [self decodeUserInfo:userinfoDic];
        }
    }
}


///消息的类型名
+ (NSString *)getObjectName {
    return CRWorldMsgMessageIdentifier;
}



@end
