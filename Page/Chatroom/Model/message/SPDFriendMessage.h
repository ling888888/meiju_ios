//
//  SPDFriendMessage.h
//  SimpleDate
//
//  Created by Monkey on 2018/2/1.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

#define SPDFriendMessageIdentifier @"SPD:friend"

@interface SPDFriendMessage : RCMessageContent

@property  (nonatomic,strong)NSString * type; //'request' 'refuse' 'accept'

@property (nonatomic,strong)NSString * content;

@end
