//
//  SPDClanCallMessage.h
//  SimpleDate
//
//  Created by Monkey on 2017/6/22.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

#define SPDClanCallMessageIdentifier @"CR:GroupCall"


@interface SPDClanCallMessage : RCMessageContent

@property  (nonatomic,strong)NSString * content;

@property  (nonatomic,strong)NSString * pc;//显示在绘画列表里面的

@property  (nonatomic,strong)NSString * clan_id;

@property  (nonatomic,strong)NSString * clan_name;

@property  (nonatomic,strong)NSString * lang;


+(instancetype)clanCallMessageWithContent:(NSString *)content ClanId:(NSString *)clan_id andClan_name:(NSString *)clan_name;

@end
