//
//  SPDPrivateMagicMessage.m
//  SimpleDate
//
//  Created by Monkey on 2017/10/24.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDPrivateMagicMessage.h"
#import "SPDApiUser.h"
#import "NSObject+Coding.h"

@implementation SPDPrivateMagicMessage

+ (instancetype)privateMagicMessageWith:(NSString *)magic_id
                             magic_name:(NSString *)magic_name
                            magic_level:(NSString *)magic_level
                           magic_effect:(NSString *)magic_effect
                           magic_result:(NSString *)magic_result
                                 power :(BOOL )power
                             receive_id:(NSString *)receive_id
                            magic_cover:(NSString *)magic_cover
                             magic_type:(NSString *)magic_type
                            send_gender:(NSString *)send_gender
                                  magic_effect_value:(NSString*)magic_effect_value{
    SPDPrivateMagicMessage *msg = [[SPDPrivateMagicMessage alloc]init];
    if (msg) {
        msg.magic_id = magic_id;
        msg.magic_name = magic_name;
        msg.magic_level = magic_level;
        msg.magic_effect = magic_effect;
        msg.magic_power = power;
        msg.magic_result = magic_result;
        msg.receive_id = receive_id;
        msg.magic_cover = magic_cover;
        msg.magic_type = magic_type;
        msg.send_gender = send_gender;
        msg.magic_effect_value = magic_effect_value;
    }
    return msg;
}

///消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return    MessagePersistent_ISCOUNTED;
}

/// NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
//        self.magic_id = [aDecoder decodeObjectForKey:@"magic_id"];
//        self.magic_name = [aDecoder decodeObjectForKey:@"magic_name"];
//        self.magic_level  = [aDecoder decodeObjectForKey:@"magic_level"];
//        self.magic_effect = [aDecoder decodeObjectForKey:@"magic_effect"];
//        self.magic_result = [aDecoder decodeObjectForKey:@"magic_result"];
//        self.magic_power = [aDecoder decodeObjectForKey:@"magic_power"];
//        self.receive_id = [aDecoder decodeObjectForKey:@"receive_id"];
//        self.magic_cover = [aDecoder decodeObjectForKey:@"magic_cover"];
//        self.magic_type = [aDecoder decodeObjectForKey:@"magic_type"];
//        self.send_gender = [aDecoder decodeObjectForKey:@"send_gender"];
//        self.magic_effect_value = [aDecoder decodeObjectForKey:@"magic_effect_value"];
        [self hl_decodeWithDeCoder:aDecoder];
    }
    return self;
}


/// NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
//    [aCoder encodeObject:self.magic_id forKey:@"magic_id"];
//    [aCoder encodeObject:self.magic_name forKey:@"magic_name"];
//    [aCoder encodeObject:self.magic_level forKey:@"magic_level"];
//    [aCoder encodeObject:self.magic_effect forKey:@"magic_effect"];
//    [aCoder encodeObject:self.magic_result forKey:@"magic_result"];
//    [aCoder encodeBool:self.magic_power forKey:@"magic_power"];
//    [aCoder encodeObject:self.receive_id forKey:@"receive_id"];
//    [aCoder encodeObject:self.magic_cover forKey:@"magic_cover"];
//    [aCoder encodeObject:self.magic_type forKey:@"magic_type"];
//    [aCoder encodeObject:self.send_gender forKey:@"send_gender"];
//    [aCoder encodeObject:self.magic_effect_value forKey:@"magic_effect_value"];
    [self hl_encodeWithCoder:aCoder];
}


///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [NSMutableDictionary dictionary];
    [dataDict setObject:self.magic_id forKey:@"magic_id"];
    [dataDict setObject:self.magic_name forKey:@"magic_name"];
    [dataDict setObject:self.magic_level forKey:@"magic_level"];
    [dataDict setObject:self.magic_effect forKey:@"magic_effect"];
    [dataDict setObject:self.magic_result forKey:@"magic_result"];
    [dataDict setObject:@(self.magic_power) forKey:@"magic_power"];
    [dataDict setObject:self.receive_id forKey:@"receive_id"];
    [dataDict setObject:self.magic_cover forKey:@"magic_cover"];
    [dataDict setObject:self.magic_type forKey:@"magic_type"];
    [dataDict setObject:self.send_gender forKey:@"send_gender"];
    [dataDict setObject:self.magic_effect_value forKey:@"magic_effect_value"];

    if (self.senderUserInfo) {
        NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc] init];
        if (self.senderUserInfo.name) {
            [userInfoDic setObject:self.senderUserInfo.name
                 forKeyedSubscript:@"name"];
        }
        if (self.senderUserInfo.portraitUri) {
            [userInfoDic setObject:self.senderUserInfo.portraitUri
                 forKeyedSubscript:@"icon"];
        }
        if (self.senderUserInfo.userId) {
            [userInfoDic setObject:self.senderUserInfo.userId
                 forKeyedSubscript:@"id"];
        }
        [dataDict setObject:userInfoDic forKey:@"user"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        
        if (dictionary) {
            self.magic_id = dictionary[@"magic_id"];
            self.magic_name = dictionary[@"magic_name"];
            self.magic_level  = dictionary[@"magic_level"];
            self.magic_effect  = dictionary[@"magic_effect"];
            self.magic_result = dictionary[@"magic_result"];
            self.magic_power = [dictionary[@"magic_power"] boolValue];
            self.receive_id = dictionary[@"receive_id"];
            self.magic_cover = dictionary[@"magic_cover"];
            self.magic_type = dictionary[@"magic_type"];
            self.send_gender = dictionary[@"send_gender"];
            self.magic_effect_value = dictionary[@"magic_effect_value"];
            NSDictionary *userinfoDic = dictionary[@"user"];
            [self decodeUserInfo:userinfoDic];
        }
    }
}

/// 会话列表中显示的摘要
- (NSString *)conversationDigest {
    NSString  * digest = SPDStringWithKey(@"魔法", nil) ;
    if ([self.senderUserInfo.userId isEqualToString:[SPDApiUser currentUser].userId]) {
        digest=  [self getSendConversationDigestStr];;
        
    }else{
        digest=  [self getReceiveConversationDigestStr];
    }
    return  digest;
}

- (NSString *)getReceiveConversationDigestStr {
    NSString * digest;
    NSString * effect;
    if ([self.magic_effect isEqualToString:@"inc"]) {
        effect = @"增加";
    }else{
        effect = @"减少";
    }
    if (self.senderUserInfo &&  self.magic_result && [ self.magic_result isEqualToString:@"success"]) {
        NSString * str ;
        if ([self.magic_type isEqualToString:@"charm"]) {
            str = [NSString stringWithFormat:@"%@魅力值",effect];
        }else{
            str = [NSString stringWithFormat:@"%@经验值",effect];
        }
        
        if ([self.magic_type isEqualToString:@"charm"] || [self.magic_type isEqualToString:@"exp"]) {
            digest = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",SPDStringWithKey(@"对方向你使用", nil),SPDStringWithKey(self.magic_name, nil),SPDStringWithKey(@"魔法", nil),SPDStringWithKey(@"成功", nil),SPDStringWithKey(@"你", nil), SPDStringWithKey(str, nil),self.magic_effect_value];
        }else if ([self.magic_type isEqualToString:@"banned_mic"] || [self.magic_type isEqualToString:@"banned_talk"]){
           digest = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",SPDStringWithKey(@"对方向你使用", nil),SPDStringWithKey(self.magic_name, nil),SPDStringWithKey(@"魔法", nil),SPDStringWithKey(@"成功", nil),SPDStringWithKey(@"你", nil),[self getEffectWithString:self.magic_type],[NSString stringWithFormat:@"%@s",self.magic_effect_value]];
        }else{
            digest = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",SPDStringWithKey(@"对方向你使用", nil),SPDStringWithKey(self.magic_name, nil),SPDStringWithKey(@"魔法", nil),SPDStringWithKey(@"成功", nil),SPDStringWithKey(@"你", nil),[self getEffectWithString:self.magic_type],self.magic_effect_value];
            
        }
        
    }else{
        digest = [NSString stringWithFormat:@"%@%@%@%@",SPDStringWithKey(@"对方向你使用", nil),SPDStringWithKey(self.magic_name, nil),SPDStringWithKey(@"魔法", nil),SPDStringWithKey(@"但是失败了", nil)];
    }
    return digest;
}

- (NSString *)getSendConversationDigestStr{
    
    NSString *digest;
    NSString * effect;
    if ([self.magic_effect isEqualToString:@"inc"]) {
        effect = @"增加";
    }else{
        effect = @"减少";
    }
    if (self.senderUserInfo &&  self.magic_result && [ self.magic_result isEqualToString:@"success"]) {
        NSString * str ;
        if ([self.magic_type isEqualToString:@"charm"]) {
            str = [NSString stringWithFormat:@"%@魅力值",effect];
        }else{
            str = [NSString stringWithFormat:@"%@经验值",effect];
        }
        
        if ([self.magic_type isEqualToString:@"charm"] || [self.magic_type isEqualToString:@"exp"]) {
                digest= [NSString stringWithFormat:@"%@%@%@%@%@%@%@",SPDStringWithKey(@"你向对方使用", nil),SPDStringWithKey(self.magic_name, nil),SPDStringWithKey(@"魔法", nil),SPDStringWithKey(@"成功", nil),SPDStringWithKey(@"TA", nil), SPDStringWithKey(str, nil),self.magic_effect_value];
            
        }else if ([self.magic_type isEqualToString:@"banned_mic"] || [self.magic_type isEqualToString:@"banned_talk"]){
            digest = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",SPDStringWithKey(@"你向对方使用", nil),SPDStringWithKey(self.magic_name, nil),SPDStringWithKey(@"魔法", nil),SPDStringWithKey(@"成功", nil),SPDStringWithKey(@"TA", nil),[self getEffectWithString:self.magic_type],[NSString stringWithFormat:@"%@s",self.magic_effect_value]];
        }else{
            digest = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",SPDStringWithKey(@"你向对方使用", nil),SPDStringWithKey(self.magic_name, nil),SPDStringWithKey(@"魔法", nil),SPDStringWithKey(@"成功", nil),SPDStringWithKey(@"TA", nil),self.magic_effect_value,[self getEffectWithString:self.magic_type]];
        }
        
    }else{
        digest = [NSString stringWithFormat:@"%@%@%@%@",SPDStringWithKey(@"你向对方使用", nil),SPDStringWithKey(self.magic_name, nil),SPDStringWithKey(@"魔法", nil),SPDStringWithKey(@"但是失败了", nil)];
    }
    return digest;
}

- (NSString *)getEffectWithString:(NSString *)str {
    
    NSString* effect;
    if ([str isEqualToString:@"charm"]) {
        effect = SPDStringWithKey(@"魅力值", nil);
    }else if ([str isEqualToString:@"exp"]){
        effect = SPDStringWithKey(@"经验值", nil);
    }else if ([str isEqualToString:@"banned_talk"]){
        effect = SPDStringWithKey(@"被禁言", nil);
    }else if ([str isEqualToString:@"banned_mic"]){
        effect = SPDStringWithKey(@"被封麦", nil);
    }else if ([str isEqualToString:@"kicked"]){
        effect = SPDStringWithKey(@"内无法进入被踢出的房间", nil);
    }
    return effect;
}

///消息的类型名
+ (NSString *)getObjectName {
    return SPDPrivateMagicMessageIdentifier;
}

@end
