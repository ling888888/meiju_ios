//
//  SPDWorldChatMessage.h
//  SimpleDate
//
//  Created by Monkey on 2017/5/31.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>
#import "LY_LiveBaseMessageContent.h"
/*!
 的类型名
 */
#define SPDWorldChatMessageIdentifier @"worldMsg"

@interface SPDWorldChatMessage : LY_LiveBaseMessageContent

@property(nonatomic, strong) NSString *content;

@property(nonatomic, strong) NSString *send_ClanId;

@property(nonatomic, strong) NSString *send_ClanName;

@property(nonatomic, strong) NSString *gender;

@property (nonatomic, strong) NSString *mentionedType; // @类型 "1": @全部用户, "2": @部分用户

@property (nonatomic, strong) NSArray *mentionedList; // @用户列表 [{"user_id": "", "nick_name": "", "avatar": "地址全拼", "gender": ""}]

//gender

@property(nonatomic, strong) NSString *receive_avatar;

@property(nonatomic, strong) NSString *receive_nickname;

@property(nonatomic, strong) NSString *receive_gender;

@property(nonatomic, strong) NSString *receive_Id;

@property(nonatomic, strong) NSString *present_url;

@property(nonatomic,strong) NSString * gift_id;

@property(nonatomic,strong) NSString * type;

@property(nonatomic,strong) NSString * send_msg_world;

@property(nonatomic, strong) NSString *num;

@property (nonatomic,strong) NSString * send_headwear;
@property (nonatomic,strong) NSString * receive_headwear;

@property(nonatomic, copy) NSString *curSel; // 选中的人的数量
@property(nonatomic, copy) NSString *sumSel; // 总数
@property(nonatomic, copy) NSString *receive_user_type; // 总数
@property(nonatomic, copy) NSArray * receiveUser; // 收到礼物的users array 里面是 dict类型 not ids

//@property(nonatomic, copy) NSString *senderHeadwearWebp;
@property(nonatomic, copy) NSString *receiveHeadwearWebp; // 收到礼物人的头饰 送礼物人的头饰 headwearWebp
@property(nonatomic, copy) NSString *resource;
@property(nonatomic, copy) NSNumber *rewardNumber;

+ (instancetype)SPDWorldChatMessageWithContent:(NSString *)content andsend_ClanId:(NSString *)send_ClanId andsend_ClanName:(NSString *)send_ClanName andGender:(NSString *)gender;

@end
