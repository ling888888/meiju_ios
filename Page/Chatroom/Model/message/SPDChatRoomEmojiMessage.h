//
//  SPDChatRoomEmojiMessage.h
//  SimpleDate
//
//  Created by Monkey on 2019/5/29.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

NS_ASSUME_NONNULL_BEGIN

@interface SPDChatRoomEmojiMessage : RCMessageContent

@property (nonatomic, strong)NSString * position; //[0-9]麦位
@property (nonatomic, strong)NSString * url; // //表情的地址

@end

NS_ASSUME_NONNULL_END
