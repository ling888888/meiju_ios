//
//  CRSendVehicleMessage.h
//  SimpleDate
//
//  Created by Monkey on 2017/9/19.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

#define CRSendVehicleMessageIdentifier @"CR:SendVehicle"

@interface CRSendVehicleMessage : RCMessageContent

@property  (nonatomic,strong)NSString * fromUserId;

@property  (nonatomic,strong)NSString * toUserId;

@property  (nonatomic,strong)NSString * num;

@property  (nonatomic,strong)NSString * _id; //礼物或座驾的 ID,

@property  (nonatomic,strong)NSString * clan_id;

+(instancetype)cRSendVehicleWithfromUserId:(NSString *)fromUserId toUserId:(NSString *)toUserId ClanId:(NSString *)clan_id and_id:(NSString *)_id andNum:(NSString *)num ;

@end
