//
//  SPDNobleGreetingMessage.m
//  SimpleDate
//
//  Created by 李楠 on 2018/9/18.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDNobleGreetingMessage.h"
#import "NSObject+Coding.h"

@implementation SPDNobleGreetingMessage

///消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return MessagePersistent_ISCOUNTED;
}

///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [self encodeBaseDict];
    if (self.content) {
        [dataDict setObject:self.content forKey:@"content"];
    }
    if (self.noble) {
        [dataDict setObject:self.noble forKey:@"noble"];
    }
    if (self.gender) {
        [dataDict setObject:self.gender forKey:@"noble"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

/// 将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        
        if (dictionary) {
            [self decodeWithDict:dictionary];
            self.content = dictionary[@"content"];
            self.noble = dictionary[@"noble"];
            self.gender = dictionary[@"gender"];
            NSDictionary *userinfoDic = dictionary[@"user"];
            [self decodeUserInfo:userinfoDic];
        }
    }
}

/// 会话列表中显示的摘要
- (NSString *)conversationDigest {
    return self.content;
}

/// 消息的类型名
+ (NSString *)getObjectName {
    return @"CR:NobleGreetingMsg";
}

@end
