//
//  ChatroomRankModel.m
//  SimpleDate
//
//  Created by 李楠 on 2018/11/26.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "ChatroomRankModel.h"

@implementation ChatroomRankModel

- (void)setUpdateTime:(NSNumber *)updateTime {
    _updateTime = updateTime;
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setTimeZone:[NSTimeZone localTimeZone]];
    [format setDateFormat:@"HH:mm"];
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:_updateTime.longLongValue / 1000];
    self.updateTimeStr = [NSString stringWithFormat:@"%@%@", SPDStringWithKey(@"更新时间: ", nil), [format stringFromDate:date]];
}

- (void)setTotalGold:(NSNumber *)totalGold {
    _totalGold = totalGold;
    
    self.totalGoldStr = [SPDCommonTool transformIntegerToBriefStr:_totalGold.integerValue];
}

- (void)setGold:(NSNumber *)gold {
    _gold = gold;
    
    self.goldStr = [SPDCommonTool transformIntegerToBriefStr:_gold.integerValue];
}

@end
