//
//  SPDFamilyMemberModel.m
//  SimpleDate
//
//  Created by 李楠 on 17/5/15.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDFamilyMemberModel.h"

@implementation SPDFamilyMemberModel

- (void)setValue:(id)value forKey:(NSString *)key {
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"avatar"]) {
        self.portrait.url = value;
    } else if ([key isEqualToString:@"headwearWebp"]) {
        self.portrait.headwearUrl = value;
    }
}

- (LY_Portrait *)portrait {
    if (!_portrait) {
        _portrait = [[LY_Portrait alloc] init];
    }
    return _portrait;
}

@end
