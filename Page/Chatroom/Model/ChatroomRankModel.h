//
//  ChatroomRankModel.h
//  SimpleDate
//
//  Created by 李楠 on 2018/11/26.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "CommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChatroomRankModel : CommonModel

@property (nonatomic, strong) NSNumber *updateTime;
@property (nonatomic, copy) NSString *updateTimeStr;
@property (nonatomic, strong) NSNumber *totalGold;
@property (nonatomic, copy) NSString *totalGoldStr;

@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, strong) NSNumber *age;
@property (nonatomic, strong) NSNumber *level;
@property (nonatomic, copy) NSString *noble;
@property (nonatomic, strong) NSNumber *rank;
@property (nonatomic, strong) NSNumber *gold;
@property (nonatomic, copy) NSString *goldStr;

@end

NS_ASSUME_NONNULL_END
