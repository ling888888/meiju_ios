//
//  ChatroomRedPacketModel.h
//  SimpleDate
//
//  Created by 李楠 on 2018/9/4.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "CommonModel.h"

@interface ChatroomRedPacketModel : CommonModel

@property (nonatomic, copy) NSString *redPacketId;
@property (nonatomic, strong) NSNumber *status; //［"0" 可领取, "1" 领取成功, "2" 已被领完, "3" 已过期]
@property (nonatomic, strong) NSNumber *gold;
@property (nonatomic, strong) NSNumber *totalCount;
@property (nonatomic, strong) NSNumber *gotCount;
@property (nonatomic, strong) NSMutableArray *list;

@end


@interface ChatroomRedPacketUserModel : CommonModel

@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, strong) NSNumber *gold;

@end
