//
//  SPDEmojiModel.h
//  SimpleDate
//
//  Created by Monkey on 2019/5/28.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "CommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SPDEmojiModel : CommonModel

@property (nonatomic, copy) NSString *_id;
@property (nonatomic, copy) NSString *desc;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *image_gif;

@end

NS_ASSUME_NONNULL_END
