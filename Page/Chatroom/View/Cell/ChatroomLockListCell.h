//
//  ChatroomLockListCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/11/19.
//  Copyright © 2018 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChatroomLockModel;

@protocol ChatroomLockListCellDelegate <NSObject>

@optional

- (void)didClickBuyButtonWithModel:(ChatroomLockModel *)model;

@end

NS_ASSUME_NONNULL_BEGIN

@interface ChatroomLockListCell : UICollectionViewCell

@property (nonatomic, strong) ChatroomLockModel *model;
@property (nonatomic, weak) id<ChatroomLockListCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
