//
//  NobleGreetingMsgCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/9/18.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "NobleGreetingMsgCell.h"
#import "SPDNobleGreetingMessage.h"

@interface NobleGreetingMsgCell ()

@property (nonatomic, strong) LY_PortraitView *portraitView;
//@property (nonatomic, strong) UIImageView *avatarImageView;
//@property (nonatomic, strong) UIImageView *nobleAvatarImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIImageView *messageBackgroundImageView;
@property (nonatomic, strong) UIImageView *nobleMedalImageView;
@property (nonatomic, strong) UILabel *textLabel;

@end

@implementation NobleGreetingMsgCell

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
    }
    return _portraitView;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews {
//    self.avatarImageView = [[UIImageView alloc]init];
//    self.avatarImageView.layer.cornerRadius = 18.0f;
//    self.avatarImageView.clipsToBounds = YES;
//    self.avatarImageView.layer.borderColor = [UIColor whiteColor].CGColor;
//    self.avatarImageView.layer.borderWidth = 1.0f;
//    self.avatarImageView.userInteractionEnabled =YES;
    UITapGestureRecognizer *tapAvatarImageView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAvatarImageView:)];
    [self.portraitView addGestureRecognizer:tapAvatarImageView];
    UILongPressGestureRecognizer *longPressAvatarImageView = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressAvatarImageView:)];
    [self.portraitView addGestureRecognizer:longPressAvatarImageView];
    [self.contentView addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(1.5);
        make.leading.mas_equalTo(5);
        make.width.mas_equalTo(54);
        make.height.mas_equalTo(54);
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font= [UIFont systemFontOfSize:12];
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.leading.mas_equalTo(self.portraitView.mas_trailing).with.offset(6);
        make.height.mas_equalTo(14);
    }];
    
    self.textLabel = [[UILabel alloc] init];
    self.textLabel.textColor = [UIColor whiteColor];
    self.textLabel.font = [UIFont systemFontOfSize:14];
    self.textLabel.numberOfLines = 0;
    [self.contentView addSubview:self.textLabel];
    
    self.messageBackgroundImageView = [[UIImageView alloc] init];
    [self.contentView insertSubview:self.messageBackgroundImageView belowSubview:self.textLabel];
    [self.messageBackgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.textLabel).offset(-3);
        make.bottom.mas_equalTo(self.textLabel).offset(3);
        make.leading.mas_equalTo(self.nameLabel.mas_leading).offset(0);
        make.trailing.mas_equalTo(self.textLabel).offset(26);
    }];
    
     self.nobleMedalImageView = [[UIImageView alloc] init];
     [self.contentView addSubview:self.nobleMedalImageView];
     [self.nobleMedalImageView mas_makeConstraints:^(MASConstraintMaker *make) {
         make.bottom.mas_equalTo(self.messageBackgroundImageView.mas_bottom).offset(4);
         make.trailing.mas_equalTo(self.messageBackgroundImageView.mas_trailing).offset(4);
         make.width.mas_equalTo(26);
         make.height.mas_equalTo(26);
     }];
}

- (void)tapAvatarImageView:(UITapGestureRecognizer *)tap {
    if (self.delegate && [self.delegate respondsToSelector:@selector(clicledAvatarWithUserId:)]) {
        if (self.message.senderUserInfo) {
            RCUserInfo *userInfo = self.message.senderUserInfo;
            [self.delegate clicledAvatarWithUserId:userInfo.userId];
        }
    }
}

- (void)longPressAvatarImageView:(UILongPressGestureRecognizer *)longPress {
    RCUserInfo *user = self.message.senderUserInfo;
    if (user && longPress.state == UIGestureRecognizerStateBegan) {
        NSDictionary *userInfo = @{@"user_id": user.userId ? : @"", @"nick_name": user.name ? : @"", @"avatar": user.portraitUri ? : @"", @"gender": self.message.gender ? : @""};
        [[NSNotificationCenter defaultCenter] postNotificationName:CHATROOM_LONGPRESS object:nil userInfo:userInfo];
    }
}

- (void)setMessage:(SPDNobleGreetingMessage *)message {
    _message = message;
    
    RCUserInfo *userInfo = _message.senderUserInfo;
    
    self.portraitView.portrait = message.portrait;
//    [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:userInfo.portraitUri]];
    self.nameLabel.text = userInfo.name;
    self.nameLabel.textColor = SPD_HEXCOlOR([_message.gender isEqualToString:@"female"] ? @"fe69a1" : @"3c91f1");
    self.nobleMedalImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"medal_%@", _message.noble]];
    self.textLabel.text = _message.content;
    [self.textLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameLabel.mas_bottom).offset(10);
        make.leading.mas_equalTo(self.messageBackgroundImageView.mas_leading).offset(13);
        make.width.mas_equalTo(_message.textSize.width + 6);
        make.height.mas_equalTo(_message.textSize.height);
    }];
    UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"greeting_%@", _message.noble]].adaptiveRtl;
    self.messageBackgroundImageView.image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(11, 12.5, 7, 12.5) resizingMode:UIImageResizingModeStretch];    
}

@end
