//
//  ChatroomLockListCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/11/19.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "ChatroomLockListCell.h"
#import "ChatroomLockModel.h"

@interface ChatroomLockListCell ()

@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;

@end

@implementation ChatroomLockListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    if ([SPDCommonTool currentVersionIsArbic]) {
        [self.buyButton setImageEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    }
}

- (void)setModel:(ChatroomLockModel *)model {
    _model = model;
    
    self.dayLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"%@天", nil), _model.day];
    [self.buyButton setTitle:_model.price.stringValue forState:UIControlStateNormal];
}

- (IBAction)clickBuyButton:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickBuyButtonWithModel:)]) {
        [self.delegate didClickBuyButtonWithModel:self.model];
    }
}

@end
