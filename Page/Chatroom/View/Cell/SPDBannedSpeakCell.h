//
//  SPDBannedSpeakCell.h
//  SimpleDate
//
//  Created by Monkey on 2017/5/20.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "RCDLiveMessageCell.h"

@interface SPDBannedSpeakCell : RCDLiveMessageCell


@property (nonatomic,strong) UILabel *messageLabel;

@property (nonatomic,strong) UIView  *balckBackView;

- (void)setDataModel:(RCDLiveMessageModel *)model;

+ (CGSize)getMessageCellSize:(NSString *)content withWidth:(CGFloat)width;

@end
