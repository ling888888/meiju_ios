//
//  ChatroomLockStateCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/11/19.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "ChatroomLockStateCell.h"

@interface ChatroomLockStateCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;

@end

@implementation ChatroomLockStateCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.titleLabel.text = SPDStringWithKey(@"说明", nil);
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 5;
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    [attributes setObject:paragraphStyle forKey:NSParagraphStyleAttributeName];
    self.stateLabel.attributedText = [[NSAttributedString alloc] initWithString:SPDStringWithKey(@"1.只有族长可以给房间上锁；\n2.上锁的房间需要密码进入或者是上锁房间的家族成员可以不输密码进入；\n3.房间锁到期后，上锁房间将自动解除上锁；\n4.房间内点击···可看到房间锁选项。", nil) attributes:attributes];
}

@end
