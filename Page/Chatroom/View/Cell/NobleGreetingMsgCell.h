//
//  NobleGreetingMsgCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/9/18.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RCDMSChatRoomCell.h"

@class SPDNobleGreetingMessage;

@interface NobleGreetingMsgCell : UICollectionViewCell

@property (nonatomic, strong) SPDNobleGreetingMessage *message;
@property (nonatomic, weak) id<RCDMSChatRoomCellDelegate> delegate;

@end
