//
//  ChatroomRankFirstCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/11/26.
//  Copyright © 2018 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChatroomRankModel;

NS_ASSUME_NONNULL_BEGIN

@interface ChatroomRankFirstCell : UICollectionViewCell

@property (nonatomic, strong) ChatroomRankModel *model;

@end

NS_ASSUME_NONNULL_END
