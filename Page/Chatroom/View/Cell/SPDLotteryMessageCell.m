//
//  SPDLotteryMessageCell.m
//  SimpleDate
//
//  Created by Monkey on 2018/4/9.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDLotteryMessageCell.h"
#import "SPDLotteryMessage.h"


@interface SPDLotteryMessageCell()

@property (nonatomic,strong) UIButton * titleBtn;
@property (nonatomic,strong) UILabel * titleLabel;
@property (nonatomic,strong) UIImageView * titleImageView;

@property (nonatomic,strong) UIButton * fuctionBtn;
@property (nonatomic,strong) UIImageView * backView; //bg图片
@property (nonatomic,strong) UILabel *contentLabel; //内容label

@end

@implementation SPDLotteryMessageCell

- (NSDictionary *)attributeDictionary {
    if (self.messageDirection == MessageDirection_SEND) {
        return @{
                 @(NSTextCheckingTypeLink) : @{NSForegroundColorAttributeName : [UIColor blueColor]},
                 @(NSTextCheckingTypePhoneNumber) : @{NSForegroundColorAttributeName : [UIColor blueColor]}
                 };
    } else {
        return @{
                 @(NSTextCheckingTypeLink) : @{NSForegroundColorAttributeName : [UIColor blueColor]},
                 @(NSTextCheckingTypePhoneNumber) : @{NSForegroundColorAttributeName : [UIColor orangeColor]}
                 };
    }
    return nil;
}

- (NSDictionary *)highlightedAttributeDictionary {
    return [self attributeDictionary];
}
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (UIImageView *)backView {
    if (!_backView) {
        _backView =[[UIImageView alloc]init];
        [self.messageContentView addSubview:_backView];
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.messageContentView.mas_top).offset(0);
            make.left.equalTo(self.messageContentView.mas_left).offset(0);
            make.width.mas_equalTo(pixwn(275));
            make.height.equalTo(self.messageContentView.mas_height);
        }];
    }
    return _backView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        [self.messageContentView addSubview:_titleLabel];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.textColor =[UIColor whiteColor];
        _titleLabel.font = [UIFont systemFontOfSize:13];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.messageContentView.mas_top).offset(8);
            make.centerX.mas_equalTo(self.backView.center.x);
            make.height.mas_equalTo(30);
        }];
    }
    return _titleLabel;
}

- (UIImageView *) titleImageView {
    if (!_titleImageView) {
        _titleImageView = [UIImageView new];
        [self.messageContentView insertSubview:_titleImageView belowSubview:self.titleLabel];
        [_titleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titleLabel.mas_top).offset(0);
            make.height.equalTo(self.titleLabel.mas_height).offset(0);
            make.leading.equalTo(self.titleLabel.mas_leading).offset(-15);
            make.trailing.equalTo(self.titleLabel.mas_trailing).offset(15);
        }];
    }
    return _titleImageView;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [UILabel new];
        _contentLabel.numberOfLines= 0;
        _contentLabel.textColor = [UIColor whiteColor];
        _contentLabel.font = [UIFont systemFontOfSize:13];
       _contentLabel.numberOfLines = 0;
        [self.messageContentView addSubview:_contentLabel];
        [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titleLabel.mas_bottom).offset(10);
            make.leading.equalTo(self.messageContentView.mas_leading).offset(22);
            make.trailing.equalTo(self.messageContentView.mas_trailing).offset(-10);
        }];
    }
    return _contentLabel;
}


- (void)initialize {
    
    self.userInteractionEnabled = YES;
    
    self.backView.userInteractionEnabled = YES;
    self.messageContentView.userInteractionEnabled = YES;

    [self.messageContentView addSubview:self.fuctionBtn];
    [self.fuctionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentLabel.mas_bottom).offset(10);
        make.centerX.mas_equalTo(self.backView.center.x);
        make.height.mas_equalTo (29);
        make.width.mas_greaterThanOrEqualTo (30);
    }];
     
}

- (void)setDataModel:(RCMessageModel *)model {
    
    [self cleanData];
    
    [super setDataModel:model];
    
    [self setAutoLayout];
    
}

- (void)cleanData {
    [self.titleBtn setTitle:@"" forState:UIControlStateNormal];
    [self.fuctionBtn setTitle:@"" forState:UIControlStateNormal];
    self.contentLabel.text = @"";
    self.backView.image = [UIImage imageNamed:@""];
}

-(void)setAutoLayout {
    
    CGRect messageContentFrame = self.messageContentView.frame;
    messageContentFrame.size.width = pixwn(275);
    messageContentFrame.size.height = 170;
    self.messageContentView.frame = messageContentFrame;
    
    SPDLotteryMessage * msg = (SPDLotteryMessage *)self.model.content;
    self.titleLabel.text = SPDStringWithKey(@"开奖通知", nil);
    NSString * titlebg = [msg.status isEqualToString:@"win"] ? @"win_title_bg":@"nowin_title_bg";
    self.titleImageView.image = [UIImage imageNamed:titlebg];
    
    [self.fuctionBtn setTitle:SPDStringWithKey(@"继续预约", nil) forState:UIControlStateNormal];
    NSString * color = [msg.status isEqualToString:@"win"] ? @"f82d54":@"fe6a3d";
    [self.fuctionBtn setBackgroundColor:SPD_HEXCOlOR(color)];
    
    NSString * back_bg = [msg.status isEqualToString:@"win"] ? @"win_bg":@"nowin_bg";
    self.backView.image = [UIImage imageNamed:back_bg];
    //先判断输赢
    if ([msg.status isEqualToString:@"win"]) {
       //再判断是否翻倍
        NSString * content = msg.double_num  && [msg.double_num integerValue] >1? [NSString stringWithFormat:SPDStringWithKey(@"%@期开奖幸运数字是\"%@\"，您下注数字是\"%@\"投注%@倍，包含中将号码；恭喜中奖，%@奖励金币已发至你的钱包中", nil), msg.current_date,msg.win_num,msg.sel_num,msg.double_num,msg.reward] :[NSString stringWithFormat:SPDStringWithKey(@"%@期开奖幸运数字是\"%@\"，您下注数字是\"%@\"，包含中将号码；恭喜中奖，%@奖励金币已发至你的钱包中",nil), msg.current_date,msg.win_num,msg.sel_num,msg.reward];
        self.contentLabel.text = content;
    }else {
        NSString * content =  [NSString stringWithFormat:SPDStringWithKey(@"%@期开奖幸运数字是\"%@\"，您下注数字是\"%@\"，很遗憾没有中奖，不要气馁，再接再厉", nil), msg.current_date,msg.win_num,msg.sel_num];
        self.contentLabel.text = content;
    }

}


- (void)layoutSubviews {
    [super layoutSubviews];
    SPDLotteryMessage * msg = (SPDLotteryMessage *)self.model.content;
    CGRect messageContentFrame = self.messageContentView.frame;
    messageContentFrame.size.height = 8+ 30 +10  + msg.cell_height+ 10  +29 + 10;
    self.messageContentView.frame = messageContentFrame;
}

- (UIButton *)fuctionBtn {
    if (!_fuctionBtn) {
        _fuctionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _fuctionBtn.layer.cornerRadius = 12;
        _fuctionBtn.clipsToBounds = YES;
        _fuctionBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        [_fuctionBtn  addTarget:self action:@selector(handlefuctionBtnOneClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return _fuctionBtn;
}

-(void)handlefuctionBtnOneClicked {
    [self.delegate didTapMessageCell:self.model];
}


//升级SDK之后-
+(CGSize)sizeForMessageModel:(RCMessageModel *)model withCollectionViewWidth:(CGFloat)collectionViewWidth referenceExtraHeight:(CGFloat)extraHeight{
    SPDLotteryMessage * msg = (SPDLotteryMessage *)model.content;
    CGFloat height =  8+ 30 +10  + msg.cell_height+ 10  +29 + 10;
    return CGSizeMake(kScreenW, height+extraHeight);
}


@end
