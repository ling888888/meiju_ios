//
//  SPDGiftDoubleRewardMessageCell.m
//  SimpleDate
//
//  Created by Monkey on 2018/7/23.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDGiftDoubleRewardMessageCell.h"
#import "SPDGiftDoubleRewardMessage.h"
@implementation SPDGiftDoubleRewardMessageCell

- (NSDictionary *)attributeDictionary {
    if (self.messageDirection == MessageDirection_SEND) {
        return @{
                 @(NSTextCheckingTypeLink): @{NSForegroundColorAttributeName : [UIColor blueColor]},
                 @(NSTextCheckingTypePhoneNumber): @{NSForegroundColorAttributeName : [UIColor blueColor]}
                 };
    } else {
        return @{
                 @(NSTextCheckingTypeLink): @{NSForegroundColorAttributeName : [UIColor blueColor]},
                 @(NSTextCheckingTypePhoneNumber): @{NSForegroundColorAttributeName : [UIColor orangeColor]}
                 };
    }
}

- (NSDictionary *)highlightedAttributeDictionary {
    return [self attributeDictionary];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    self.bubbleBackgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, pixwn(271), pixwn(183))];
    self.bubbleBackgroundView.contentMode = UIViewContentModeScaleAspectFit;
    self.bubbleBackgroundView.userInteractionEnabled = YES;
    [self.messageContentView addSubview:self.bubbleBackgroundView];
    
    UITapGestureRecognizer *tapPress = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPress:)];
    [self.bubbleBackgroundView addGestureRecognizer:tapPress];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(pixwn(95), pixwn(10), pixwn(90), pixwn(20))];
    self.titleLabel.text = SPDStringWithKey(@"财神驾到", nil);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.font = [UIFont systemFontOfSize:13];
    [self.messageContentView addSubview:self.titleLabel];
    
    self.contentLabel  = [[UILabel alloc] initWithFrame:CGRectMake(pixwn(20), pixwn(52), pixwn(271) - pixwn(20) - pixwn(13), pixwn(60))];
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.textColor = SPD_HEXCOlOR(@"ffffff");
    self.contentLabel.font = [UIFont systemFontOfSize:pixwn(14)];
    [self.messageContentView addSubview:self.contentLabel];
    
    self.walletBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.walletBtn.frame = CGRectMake(pixwn(94.5), pixwn(133.5), pixwn(90), pixwn(29));
    self.walletBtn.backgroundColor = [UIColor colorWithHexString:@"ffaa16"];
    [self.walletBtn setTitle:SPDStringWithKey(@"我的钱包",nil) forState:UIControlStateNormal];
    self.walletBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    self.walletBtn.userInteractionEnabled = NO;
    self.walletBtn.clipsToBounds = YES;
    self.walletBtn.layer.cornerRadius = pixwn(14.5);
    [self.messageContentView addSubview:self.walletBtn];
}

- (void)setDataModel:(RCMessageModel *)model {
    [super setDataModel:model];
    [self setAutoLayout];
}

- (void)setAutoLayout {
    CGRect messageContentFrame = self.messageContentView.frame;
    messageContentFrame.size.width = pixwn(271);
    messageContentFrame.size.height = pixwn(183);
    self.messageContentView.frame = messageContentFrame;
    
    SPDGiftDoubleRewardMessage *message = (SPDGiftDoubleRewardMessage *)self.model.content;
    NSString *content = [NSString stringWithFormat:SPDStringWithKey(@"你在刷幸运礼物%@中获得%@倍奖励,奖励金币%@个已发至你的钱包中,快去查看吧~", nil), message.gift_name, message.reward_rate, message.reward_count];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:content];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:2.5];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [attributedString length])];
    
    [self.contentLabel setAttributedText:attributedString];
    
    self.bubbleBackgroundView.image = [UIImage imageNamed:@"img_caishenjiadao"];
}

- (void)tapPress:(id)sender {
    UITapGestureRecognizer *press = (UITapGestureRecognizer *)sender;
    if (press.state == UIGestureRecognizerStateEnded) {
        [self.delegate didTapMessageCell:self.model];
        return;
    }
}

// 升级SDK之后-
+ (CGSize)sizeForMessageModel:(RCMessageModel *)model withCollectionViewWidth:(CGFloat)collectionViewWidth referenceExtraHeight:(CGFloat)extraHeight {
    return CGSizeMake(kScreenW, pixwn(183) + extraHeight);
}

@end
