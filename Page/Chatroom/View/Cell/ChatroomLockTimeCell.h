//
//  ChatroomLockTimeCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/11/19.
//  Copyright © 2018 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatroomLockTimeCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *expireTimeLabel;

@end

NS_ASSUME_NONNULL_END
