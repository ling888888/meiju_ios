//
//  RCDMSChatRoomCell.h
//  SimpleDate
//
//  Created by Monkey on 2017/5/16.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "RCDLiveMessageCell.h"
#import "RCDLiveAttributedLabel.h"


@protocol RCDMSChatRoomCellDelegate <NSObject>

-(void)clicledAvatarWithUserId:(NSString *)userid;

@end

@interface RCDMSChatRoomCell : RCDLiveMessageCell<RCDLiveAttributedLabelDelegate>

@property(strong, nonatomic)LY_PortraitView *portraitView;
@property(strong, nonatomic)UILabel *nameLabel;
@property(strong, nonatomic)UIImageView *messageBackgroundImageView;

@property (nonatomic,weak)id<RCDMSChatRoomCellDelegate> cellDelegate;

/*!
 显示消息内容的Label
 */
@property(strong, nonatomic) UILabel *textLabel;

@property(assign, nonatomic) BOOL isFullScreenMode;

@property(strong,nonatomic)  UIButton *ageGenderBtn;

@property(strong,nonatomic) UIImageView *firstImgView;

@property(strong,nonatomic) UIImageView *secondImgView;

@property(strong,nonatomic) UIImageView *thirdImgView;

@property(strong,nonatomic) UIButton *user_levelBtn;

/*!
 设置当前消息Cell的数据模型
 
 @param model 消息Cell的数据模型
 */
- (void)setDataModel:(RCDLiveMessageModel *)model;

+ (CGSize)getMessageCellSize:(NSString *)content withWidth:(CGFloat)width;



@end
