//
//  SPDVoiceLiveAudienceCell.h
//  SimpleDate
//
//  Created by 李楠 on 17/5/16.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SPDVoiceLiveUserModel;

@interface SPDVoiceLiveAudienceCell : UICollectionViewCell

@property (nonatomic, strong) SPDVoiceLiveUserModel *model;

@property (weak, nonatomic) IBOutlet UIImageView *userIcon;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;

@end
