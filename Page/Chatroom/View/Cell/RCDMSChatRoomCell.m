//
//  RCDMSChatRoomCell.m
//  SimpleDate
//
//  Created by Monkey on 2017/5/16.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "RCDMSChatRoomCell.h"
#import "RCDLiveKitUtility.h"
#import "RCDLiveKitCommonDefine.h"
#import "SPDTextMessage.h"
#import "UIButton+HHImagePosition.h"
#import "NSString+XXWAddition.h"
#import "SPDApiUser.h"
#import "SPDCommonDefine.h"

#define CHATROOM_LEADING 13.0f
#define CHATROOM_AVATAR 37.0f
#define CHATROOM_NICKANDAVATAR_INTERAL 5.0f

@interface RCDMSChatRoomCell ()

@property (nonatomic, strong) UIImageView *specialNumImageView;
@property (nonatomic, strong) UIImageView *agentServiceImageView;
//@property (nonatomic, strong) UIImageView *nobleAvatarImageView;
@property (nonatomic, strong) UIImageView *nobleMedalImageView;
@property (nonatomic, strong) NSDictionary *nobleColorDic;

@end

@implementation RCDMSChatRoomCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
    }
    return _portraitView;
}

- (void)initialize {
    self.nicknameLabel.hidden=YES;
    
    UITapGestureRecognizer *tapAvatarImageView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAvatarImageView:)];
    [self.portraitView addGestureRecognizer:tapAvatarImageView];
    UILongPressGestureRecognizer *longPressAvatarImageView = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressAvatarImageView:)];
    [self.portraitView addGestureRecognizer:longPressAvatarImageView];
    [self.contentView addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.leading.mas_equalTo(5);
        make.width.mas_equalTo(54);
        make.height.mas_equalTo(54);
    }];

    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font= [UIFont systemFontOfSize:12];
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.leading.mas_equalTo(self.portraitView.mas_trailing).with.offset(6);
        make.height.mas_equalTo(14);
    }];

    self.ageGenderBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.ageGenderBtn.titleLabel.font = [UIFont systemFontOfSize:8];
    [self.ageGenderBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.ageGenderBtn.clipsToBounds = YES;
    self.ageGenderBtn.layer.cornerRadius = 7;
    self.ageGenderBtn.userInteractionEnabled = NO;
    [self.contentView addSubview:self.ageGenderBtn];
    [self.ageGenderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.leading.mas_equalTo(self.nameLabel.mas_trailing).with.offset(5);
        make.width.mas_equalTo(27);
        make.height.mas_equalTo(14);
    }];
    
    self.textLabel = [[UILabel alloc] init];
    [self.textLabel setFont:[UIFont systemFontOfSize:14]];
    self.textLabel.numberOfLines = 0;
    [self.textLabel setTextAlignment:kIsMirroredLayout? NSTextAlignmentRight :NSTextAlignmentLeft];
    self.textLabel.textColor = SPD_HEXCOlOR(@"ffffff");
    [self.contentView addSubview:self.textLabel];
    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nameLabel.mas_bottom).offset(7);
        make.leading.equalTo(self.nameLabel.mas_leading).offset(10);
        make.width.mas_lessThanOrEqualTo(kScreenW -90);
        make.height.mas_equalTo(0);
    }];
    
    //消息的backgroundView
    self.messageBackgroundImageView = [[UIImageView alloc] init];
    [self.contentView insertSubview:self.messageBackgroundImageView belowSubview:self.textLabel];
    [self.messageBackgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textLabel.mas_top).offset(-3);
        make.leading.equalTo(self.nameLabel.mas_leading).offset(0);
        make.bottom.equalTo(self.textLabel.mas_bottom).offset(3);
        make.trailing.equalTo(self.textLabel.mas_trailing).offset(15);
    }];
    
    self.contentView.userInteractionEnabled = YES;
    UILongPressGestureRecognizer *longPress =
    [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressed:)];
    [self.contentView addGestureRecognizer:longPress];
    
}

- (void)setDataModel:(RCDLiveMessageModel *)model {
    [super setDataModel:model];
    [self clearData];
    [self updateUI];
}

- (void)clearData {
    self.nameLabel.text = @"";
    [self.ageGenderBtn setTitle:@"" forState:UIControlStateNormal];
    self.firstImgView.image = [UIImage new];
    self.secondImgView.image = [UIImage new];
    self.thirdImgView.image = [UIImage new];
    [self.user_levelBtn setBackgroundImage:[UIImage new] forState:UIControlStateNormal];
    [self.user_levelBtn setTitle:@"" forState:UIControlStateNormal];
    self.textLabel.text = @"";
    self.specialNumImageView.hidden = YES;
    self.agentServiceImageView.hidden = YES;
    self.nobleMedalImageView.hidden = YES;
}

//在这里更改布局***

- (void)updateUI {
    
    if (self.model.content.senderUserInfo) {
        
        self.nameLabel.text = self.model.content.senderUserInfo.name;
        NSString * luckyNumber = nil;
        SPDTextMessage *_textMessage = (SPDTextMessage *)self.model.content;
        
        self.portraitView.portrait = _textMessage.portrait;
        self.textLabel.attributedText = _textMessage.messageAttributeString;
        
        [self.ageGenderBtn setHidden:NO];
        [self.ageGenderBtn setTitle:_textMessage.age forState:UIControlStateNormal];
        if ([_textMessage.gender isEqualToString:@"female"]) {
            [self.ageGenderBtn setImage:[UIImage imageNamed:@"chat_msg_female"] forState:UIControlStateNormal];
            [self.ageGenderBtn setBackgroundColor:SPD_HEXCOlOR(@"fe69a1")];
            self.nameLabel.textColor = SPD_HEXCOlOR(@"fe69a1");
        }else{
            [self.ageGenderBtn setImage:[UIImage imageNamed:@"chat_msg_male"] forState:UIControlStateNormal];
            [self.ageGenderBtn setBackgroundColor:SPD_HEXCOlOR(@"3c91f1")];
            self.nameLabel.textColor = SPD_HEXCOlOR(@"3c91f1");
        }
        if ([SPDCommonTool currentVersionIsArbic]) {
            [self.ageGenderBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 3)];
        }else{
            [self.ageGenderBtn setImagePosition:HHImagePositionLeft spacing:3.0f];
        }
        if (_textMessage.noble.notEmpty) {
            self.nameLabel.textColor = SPD_HEXCOlOR([SPDCommonTool getNobleNameColor:_textMessage.noble]);
        }
        //设置标签的位置
        [self configLabelWithMessage:_textMessage];
        
        [self.textLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(_textMessage.contentHeight);
        }];
        NSLog(@"contentheight--- %f",_textMessage.contentHeight);
        NSString *mentionedType = nil;
        NSArray *mentionedList = nil;
        NSString *noble = nil;
        NSString * headwear = nil;
        NSString * bubble = nil;
        NSString * bubbleTextColor = nil;
        if ([self.model.content isKindOfClass:[SPDTextMessage class]]) {
            SPDTextMessage *message = (SPDTextMessage *)self.model.content;
            mentionedType = message.mentionedType;
            mentionedList = message.mentionedList;
            noble = message.noble;
            headwear = message.headwear?:@"";
            bubble = [SPDCommonTool currentVersionIsArbic]?message.usecover_ios_ar:message.usecover_ios?:@"";
            bubbleTextColor = message.text_color?:@"";
        }
        if (![SPDCommonTool isEmpty:luckyNumber]) {
            UIImage *image = [UIImage imageNamed:@"luckybumber_bubble"].adaptiveRtl;
            self.messageBackgroundImageView.image = [self resizableImage:image];
        }else{
            if (![SPDCommonTool isEmpty:bubble] && ![SPDCommonTool isEmpty:bubbleTextColor]) {
                self.textLabel.textColor = [UIColor colorWithHexString:bubbleTextColor];
                [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:bubble] options:SDWebImageDownloaderUseNSURLCache progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                    
                } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIImage *img = [UIImage imageWithData:data scale:2.0f];
                        self.messageBackgroundImageView.image = [self resizableImage:img];
                    });
                }];
            }
            else if (mentionedType && [mentionedType isEqualToString:@"1"]) {
                self.messageBackgroundImageView.image = [self resizableImage:[UIImage imageNamed:@"notice_bubble"]];
            } else {
                if (self.messageDirection == MessageDirection_RECEIVE) {
                    self.textLabel.textColor = [UIColor colorWithHexString:@"ffffff"];
                    UIImage *image = [UIImage imageNamed:@"chatroom_bubble"];
                    if (mentionedType && [mentionedType isEqualToString:@"2"] && mentionedList) {
                        for (NSDictionary *dic in mentionedList) {
                            if ([dic[@"user_id"] isEqualToString:[SPDApiUser currentUser].userId]) {
                                image = [UIImage imageNamed:@"notice_bubble"];
                                self.textLabel.textColor = [UIColor colorWithHexString:@"333333"];
                            }
                        }
                    }
                    self.messageBackgroundImageView.image =[self resizableImage:image];
                } else {
                    UIImage *image = nil;
                    if ([self.model.content isKindOfClass:[SPDTextMessage class]]) {
                        SPDTextMessage *message = (SPDTextMessage *)self.model.content;
                        NSString *  imageName = [message.is_greetings isEqualToString:@"true"] ?@"greeting_chatroom_bubble" : @"my_chat_bubble";
                        image = [UIImage imageNamed:imageName];
                    }
                    if (mentionedType && [mentionedType isEqualToString:@"2"]) {
                        image = [UIImage imageNamed:@"notice_bubble"].adaptiveRtl;
                    }
                    self.messageBackgroundImageView.image = [self resizableImage:image];
                    
                }
            }
        }
    }
}

- (UIImage *)resizableImage:(UIImage *)originImage {
    CGFloat height = originImage.size.height / 2.0;
    CGFloat width = originImage.size.width / 2.0;
    UIImage *newImage = [originImage resizableImageWithCapInsets:UIEdgeInsetsMake(height-1,width-1,height,width) resizingMode:UIImageResizingModeStretch];
    return newImage;
}

- (void)configLabelWithMessage:(SPDTextMessage *)msg {

#pragma  mark -  9-11 去掉排行榜的标签
    int  clan_level = [msg.clan_level intValue];
    int  rich_level = [msg.rich_level intValue];
    int  charm_level = [msg.charm_level intValue];
    int  user_level = [msg.user_level intValue];
    NSMutableArray * array = [NSMutableArray array];

    if (user_level == 0) {
        user_level = 1;
    }
    [array addObject:self.user_levelBtn];
    if (![SPDCommonTool isEmpty:msg.medal]) {
        [self.nobleMedalImageView sd_setImageWithURL:[NSURL URLWithString:msg.medal]];
        [array addObject:self.nobleMedalImageView];
    }
    if (![SPDCommonTool isEmpty:msg.specialNum]) {
        [array addObject:self.specialNumImageView];
    }
    if (msg.is_agent_gm) {
        [array addObject:self.agentServiceImageView];
    }
    if (clan_level > Clan_Tourist_Level) {
        [array addObject:self.firstImgView];
    }
    if (rich_level) {
        if (charm_level) {
            
            if ( rich_level >= charm_level) {
                [array addObject:self.secondImgView];
            }else{
                [array addObject:self.thirdImgView];
            }
        }else{
            [array addObject:self.secondImgView];
        }
    }else{
        if (charm_level) {
            [array addObject:self.thirdImgView];
        }
    }
    for (int i =0; i<array.count; i++) {
        UIImageView * view = array[i];
        if (i == 0) {

            [view mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.leading.equalTo(self.ageGenderBtn.mas_trailing).offset(5);
                make.top.equalTo(self.ageGenderBtn.mas_top).offset(0);
                make.width.mas_equalTo(32);
                make.height.mas_equalTo(14);
            }];
            [SPDCommonTool configDataForLevelBtnWithLevel:user_level andUIButton:self.user_levelBtn];
        }else{

            UIImageView * last_view = array[i-1];
            CGFloat width = 40;
            if (view == self.specialNumImageView || view == self.agentServiceImageView) {
                width = 23;
                view.hidden = NO;
            } else if (view == self.nobleMedalImageView) {
                width = 14;
                view.hidden = NO;
            }
            [view mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.leading.equalTo(last_view.mas_trailing).offset(5);
                make.top.equalTo(last_view.mas_top).offset(0);
                make.width.mas_equalTo(width);
                make.height.mas_equalTo(14);
            }];
            
            if (self.firstImgView == view) {
                [SPDCommonTool manageTagWithImageView:self.firstImgView andLevel: clan_level];
            }
            if (self.secondImgView == view) {
                [SPDCommonTool manageTagWithImageView:self.secondImgView andLevel: rich_level];
            }
            if (self.thirdImgView == view) {
                [SPDCommonTool manageTagWithImageView:self.thirdImgView andLevel:charm_level];
            }
        }
    }
    
    
    
}

- (void)longPressed:(id)sender {
    UILongPressGestureRecognizer *press = (UILongPressGestureRecognizer *)sender;
    if (press.state == UIGestureRecognizerStateEnded) {
        return;
    } else if (press.state == UIGestureRecognizerStateBegan) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(didLongTouchMessageCell:inView:)]) {
            [self.delegate didLongTouchMessageCell:self.model inView:self.self.contentView];
        }
    }
}

- (void)attributedLabel:(RCDLiveAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    NSString *urlString=[url absoluteString];
    if (![urlString hasPrefix:@"http"]) {
        urlString = [@"http://" stringByAppendingString:urlString];
    }
    if ([self.delegate respondsToSelector:@selector(didTapUrlInMessageCell:model:)]) {
        [self.delegate didTapUrlInMessageCell:urlString model:self.model];
        return;
    }
}

/**
 Tells the delegate that the user did select a link to an address.
 
 @param label The label whose link was selected.
 @param addressComponents The components of the address for the selected link.
 */
- (void)attributedLabel:(RCDLiveAttributedLabel *)label didSelectLinkWithAddress:(NSDictionary *)addressComponents
{
    
}

/**
 Tells the delegate that the user did select a link to a phone number.
 
 @param label The label whose link was selected.
 @param phoneNumber The phone number for the selected link.
 */
- (void)attributedLabel:(RCDLiveAttributedLabel *)label didSelectLinkWithPhoneNumber:(NSString *)phoneNumber
{
    NSString *number = [@"tel://" stringByAppendingString:phoneNumber];
    if ([self.delegate respondsToSelector:@selector(didTapPhoneNumberInMessageCell:model:)]) {
        [self.delegate didTapPhoneNumberInMessageCell:number model:self.model];
        return;
    }
}

-(void)attributedLabel:(RCDLiveAttributedLabel *)label didTapLabel:(NSString *)content
{
    if ([self.delegate respondsToSelector:@selector(didTapMessageCell:)]) {
        [self.delegate didTapMessageCell:self.model];
    }
}

+(CGSize)getMessageCellSize:(NSString *)content withWidth:(CGFloat)width{
    CGSize textSize = CGSizeZero;
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    [paragraphStyle setLineSpacing:5];
    textSize = [content boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSParagraphStyleAttributeName:paragraphStyle,NSBaselineOffsetAttributeName:@(0)} context:nil].size;
    textSize.height = textSize.height+10;
    return textSize;
}

-(void)tapAvatarImageView:(UITapGestureRecognizer *)tap{
    if (self.cellDelegate && [self.cellDelegate respondsToSelector:@selector(clicledAvatarWithUserId:)]) {
        if (self.model.content.senderUserInfo) {
            RCUserInfo * userInfo = self.model.content.senderUserInfo;
            [self.cellDelegate clicledAvatarWithUserId:userInfo.userId];
        }
    }
    
}

- (void)longPressAvatarImageView:(UILongPressGestureRecognizer *)longPress {
    if ([self.model.content isKindOfClass:[SPDTextMessage class]]) {
        SPDTextMessage *message = (SPDTextMessage *)self.model.content;
        RCUserInfo *user = message.senderUserInfo;
        if (user && longPress.state == UIGestureRecognizerStateBegan) {
            NSDictionary *userInfo = @{@"user_id": user.userId ?:@"", @"nick_name": user.name?:@"", @"avatar": user.portraitUri?:@"", @"gender": message.gender?:@"",Headwear:![SPDCommonTool isEmpty:message.portrait.headwearUrl]?message.portrait.headwearUrl:@""};
            [[NSNotificationCenter defaultCenter] postNotificationName:CHATROOM_LONGPRESS object:nil userInfo:userInfo];
        }
    }
}

//clan
- (UIImageView *)firstImgView {
    if (!_firstImgView) {
        _firstImgView = [[UIImageView alloc]init];
        [self.contentView addSubview:_firstImgView];
    }
    return _firstImgView;
}

//rank
- (UIImageView *)secondImgView {
    if (!_secondImgView) {
        _secondImgView = [[UIImageView alloc]init];
        [self.contentView addSubview:_secondImgView];
    }
    return _secondImgView;
}
- (UIImageView *)thirdImgView {
    if (!_thirdImgView) {
        _thirdImgView = [[UIImageView alloc]init];
        [self.contentView addSubview:_thirdImgView];
    }
    return _thirdImgView;
}

- (UIButton *)user_levelBtn {
    if (!_user_levelBtn) {
        _user_levelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _user_levelBtn.bounds = CGRectMake(0, 0, 32, 14);
        _user_levelBtn.titleLabel.font = [UIFont systemFontOfSize:9];
        _user_levelBtn.userInteractionEnabled = NO;
        [self.contentView addSubview:_user_levelBtn];
    }
    return _user_levelBtn;
}

- (UIImageView *)specialNumImageView {
    if (!_specialNumImageView) {
        _specialNumImageView = [[UIImageView alloc] init];
        _specialNumImageView.image = [UIImage imageNamed:@"specialNum_icon_15"];
        _specialNumImageView.hidden = YES;
        [self.contentView addSubview:_specialNumImageView];
    }
    return _specialNumImageView;
}

- (UIImageView *)agentServiceImageView {
    if (!_agentServiceImageView) {
        _agentServiceImageView = [[UIImageView alloc] init];
        _agentServiceImageView.image = [UIImage imageNamed:@"agent_service_13"];
        _agentServiceImageView.hidden = YES;
        [self.contentView addSubview:_agentServiceImageView];
    }
    return _agentServiceImageView;
}

- (UIImageView *)nobleMedalImageView {
    if (!_nobleMedalImageView) {
        _nobleMedalImageView = [[UIImageView alloc] init];
        _nobleMedalImageView.hidden = YES;
        [self.contentView addSubview:_nobleMedalImageView];
    }
    return _nobleMedalImageView;
}

@end
