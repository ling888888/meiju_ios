//
//  ChatroomUserCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/12/26.
//  Copyright © 2018 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class SPDVoiceLiveUserModel;

@interface ChatroomUserCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;

@property (strong, nonatomic) SPDVoiceLiveUserModel *model;

@end

NS_ASSUME_NONNULL_END
