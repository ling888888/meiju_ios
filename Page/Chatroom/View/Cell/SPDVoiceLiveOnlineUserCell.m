//
//  SPDVoiceLiveOnlineUserCell.m
//  SimpleDate
//
//  Created by 李楠 on 17/5/15.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDVoiceLiveOnlineUserCell.h"
#import "SPDVoiceLiveUserModel.h"
#import "SPDCommonDefine.h"

@implementation SPDVoiceLiveOnlineUserCell

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
        _portraitView.userInteractionEnabled = YES;
    }
    return _portraitView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    NSMutableArray *array = [NSMutableArray array];
    for (NSInteger i = 1; i < 4; i++) {
        [array addObject:[UIImage imageNamed:[NSString stringWithFormat:@"语音效果%ld", i]]];
    }
    self.animationView.animationImages = array;
    self.animationView.animationDuration = 1;
    self.animationView.animationRepeatCount = 0;
    
    [self.contentView insertSubview:self.portraitView belowSubview:self.publicMicImage];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    UILongPressGestureRecognizer *longPressUserIcon = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressUserIcon:)];
    [self.portraitView addGestureRecognizer:longPressUserIcon];
    
}

- (void)longPressUserIcon:(UILongPressGestureRecognizer *)longPress {
    if (_model && longPress.state == UIGestureRecognizerStateBegan) {
        NSDictionary *userInfo = @{@"user_id": _model._id, @"nick_name": _model.nick_name, @"avatar": [NSString stringWithFormat:STATIC_DOMAIN_URL, _model.avatar], @"gender": _model.gender,Headwear:[SPDCommonTool isEmpty:_model.headwear]?@"":_model.portrait.headwearUrl};
        [[NSNotificationCenter defaultCenter] postNotificationName:CHATROOM_LONGPRESS object:nil userInfo:userInfo];
    }
}

- (void)setMicStatus:(MicStatus)micStatus {
    _micStatus = micStatus;
    [self.userIcon sd_cancelCurrentImageLoad];
    switch (_micStatus) {
        case NoStatus:
            self.portraitView.imageView.image = [UIImage imageNamed:@"ic_weishangmai"];
            self.micStatusImage.hidden = YES;
            break;
        case Speeking:
            self.micStatusImage.hidden = NO;
            self.micStatusImage.image = [UIImage imageNamed:@"ic_shangmaishuohuashi"];
            break;
        case Mute:
            self.micStatusImage.hidden = NO;
            self.micStatusImage.image = [UIImage imageNamed:@"ic_jingyinshi"];
            break;
        case Forbidden:
            self.micStatusImage.hidden = NO;
            self.micStatusImage.image = [UIImage imageNamed:@"ic_jinmai"];
            break;
        case CannotUse:
            self.portraitView.imageView.image = [UIImage imageNamed:@"fengmaizhuangtai"];
            self.micStatusImage.hidden = YES;
            break;
        case Freeze:
            self.portraitView.imageView.image = [UIImage imageNamed:@"ic_weishangmai"];
            self.micStatusImage.hidden = YES;
            break;
        default:
            break;
    }
}

- (void)setModel:(SPDVoiceLiveUserModel *)model {
    _model = model;
    self.portraitView.headwearImageView.hidden = !_model.portrait.headwearUrl.notEmpty;
    
    if (self.micStatusImage.hidden) {
        return;
    }
    if (self.micStatus == NoStatus) {
        self.portraitView.imageView.image = [UIImage imageNamed:@"ic_weishangmai"];
    } else if (self.micStatus == CannotUse) {
        self.portraitView.imageView.image = [UIImage imageNamed:@"fengmaizhuangtai"];
    } else if (_model) {
        self.portraitView.portrait = model.portrait;
    } else {
        self.portraitView.imageView.image = [UIImage imageNamed:@"ic_weishangmai"];
    }
}

- (void)setAnimating:(BOOL)animating {
    _animating = animating;
    self.animationView.hidden = !_animating;
    if (_animating) {
        [self.animationView startAnimating];
//        self.userIcon.layer.borderWidth = 0;
    } else {
        [self.animationView stopAnimating];
//        self.userIcon.layer.borderWidth = 1;
    }
}

- (void)setHighlighted:(BOOL)highlighted {
    
}

- (void)setSelected:(BOOL)selected {
    
}

@end
