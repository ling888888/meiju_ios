//
//  ChatroomRankFirstCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/11/26.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "ChatroomRankFirstCell.h"
#import "ChatroomRankModel.h"

@interface ChatroomRankFirstCell ()

@property (weak, nonatomic) IBOutlet UIImageView *totalImageView;
@property (weak, nonatomic) IBOutlet UILabel *totalGoldLabel;
@property (weak, nonatomic) IBOutlet UILabel *updateTimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *nobleAvatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nickNameLabelCenterX;
@property (weak, nonatomic) IBOutlet UIButton *genderAgeBtn;
@property (weak, nonatomic) IBOutlet UIButton *levelBtn;
@property (weak, nonatomic) IBOutlet UILabel *goldLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *goldLabelCenterX;

@end

@implementation ChatroomRankFirstCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    if ([SPDCommonTool currentVersionIsArbic]) {
        self.totalImageView.image = [[UIImage imageNamed:@"chatroom_rank_total_ar"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10) resizingMode:UIImageResizingModeStretch];
        [self.genderAgeBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        [self.genderAgeBtn setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 2.5)];
        self.nickNameLabelCenterX.constant = 37.5;
        self.goldLabelCenterX.constant = -11;
    } else {
        self.totalImageView.image = [[UIImage imageNamed:@"chatroom_rank_total"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10) resizingMode:UIImageResizingModeStretch];
    }
}

- (void)setModel:(ChatroomRankModel *)model {
    _model = model;
    
    self.totalGoldLabel.text = _model.totalGoldStr;
    self.updateTimeLabel.text = _model.updateTimeStr;
    
    [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _model.avatar]]];
    if (![SPDCommonTool isEmpty:_model.noble]) {
        self.nobleAvatarImageView.hidden = NO;
        self.nobleAvatarImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"avatar_%@_91", _model.noble]];
    } else {
        self.nobleAvatarImageView.hidden = YES;
    }
    self.nickNameLabel.text = _model.nickName;
    if ([_model.gender isEqualToString:@"female"]) {
        [self.genderAgeBtn setBackgroundColor:SPD_HEXCOlOR(@"ff7eca")];
        [self.genderAgeBtn setImage:[UIImage imageNamed:@"chatroomuser_female"] forState:UIControlStateNormal];
    } else if ([_model.gender isEqualToString:@"male"]) {
        [self.genderAgeBtn setBackgroundColor:SPD_HEXCOlOR(@"5db4fd")];
        [self.genderAgeBtn setImage:[UIImage imageNamed:@"chatroomuser_male"] forState:UIControlStateNormal];
    }
    [self.genderAgeBtn setTitle:_model.age.stringValue forState:UIControlStateNormal];
    [SPDCommonTool configDataForLevelBtnWithLevel:[_model.level intValue] andUIButton:self.levelBtn];
    self.goldLabel.text = _model.goldStr;
}

@end
