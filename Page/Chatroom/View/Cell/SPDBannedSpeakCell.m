//
//  SPDBannedSpeakCell.m
//  SimpleDate
//
//  Created by Monkey on 2017/5/20.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDBannedSpeakCell.h"
#import "SPDBannedSpeakMessage.h"
#import "RCDLiveKitUtility.h"

@implementation SPDBannedSpeakCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}


- (void)initialize {
    
    self.balckBackView = [[UIView alloc]initWithFrame:CGRectZero];
    self.balckBackView .backgroundColor = [UIColor colorWithWhite:0 alpha:0.2
                                           ];
    self.balckBackView .layer.cornerRadius =12;
    self.balckBackView.clipsToBounds =YES;
    [self.bubbleBackgroundView addSubview:self.balckBackView];
    
    self.messageLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    self.messageLabel.font = [UIFont systemFontOfSize:12];
    self.messageLabel.textColor = SPD_HEXCOlOR(@"#ffffff");
    [self.messageLabel sizeToFit];
    [self.balckBackView addSubview:self.messageLabel];
}

-(void)setDataModel:(RCDLiveMessageModel *)model{
    [super setDataModel:model];
    
    [self updateUI];
}

-(void)updateUI {
    
    SPDBannedSpeakMessage *_textMessage = (SPDBannedSpeakMessage *)self.model.content;
    if (_textMessage) {
        self.messageLabel.text = _textMessage.message;
    }
    self.nicknameLabel.hidden =YES;
    CGSize _messageSize = [RCDLiveKitUtility getContentSize:_textMessage.message withFrontSize:12.0f withWidth:self.baseContentView.bounds.size.width];
    CGRect balckBackViewRect = self.balckBackView.frame;
    balckBackViewRect.size.width = _messageSize.width+20;
    balckBackViewRect.size.height = _messageSize.height+10;
    self.balckBackView.frame = balckBackViewRect;
    self.balckBackView.center = self.contentView.center;

    CGRect messageLabelRect = self.messageLabel.frame;
    messageLabelRect.origin.x = 10;
    messageLabelRect.origin.y= 5;
    messageLabelRect .size.width = _messageSize.width;
    messageLabelRect.size.height = _messageSize.height;
    self.messageLabel.frame = messageLabelRect;
    NSLog(@"message label %@",NSStringFromCGRect(self.messageLabel.frame));
    
}

//+ (CGSize)getMessageCellSize:(NSString *)content withWidth:(CGFloat)width {
//    
//    CGSize textSize = CGSizeZero;
//    float maxWidth =kScreenW;
//    textSize = [RCDLiveKitUtility getContentSize:content withFrontSize:Text_Message_Font_Size withWidth:maxWidth];
//    textSize.height = 27;
//    return textSize;
//}

@end
