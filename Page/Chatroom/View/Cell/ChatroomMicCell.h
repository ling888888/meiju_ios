//
//  ChatroomMicCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/12/26.
//  Copyright © 2018 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPDVoiceLiveOnlineUserCell.h"

@class SPDVoiceLiveUserModel;

NS_ASSUME_NONNULL_BEGIN

@interface ChatroomMicCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *publicMicImageView;
@property (weak, nonatomic) IBOutlet UIImageView *emojiImageView;

@property (assign, nonatomic) MicStatus micStatus;
@property (strong, nonatomic) SPDVoiceLiveUserModel *model;
@property (assign, nonatomic) BOOL animating;

- (void)startPlayEmoji:(NSString *)url;// 开始播放emoji表情

@end

NS_ASSUME_NONNULL_END
