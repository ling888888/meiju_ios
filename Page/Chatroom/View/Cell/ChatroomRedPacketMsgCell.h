//
//  ChatroomRedPacketMsgCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/9/4.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SPDChatroomRedPacketMessage;

@protocol ChatroomRedPacketMsgCellDelegate <NSObject>

@optional

- (void)didClickRedPacketWithMessage:(SPDChatroomRedPacketMessage *)message;

@end

@interface ChatroomRedPacketMsgCell : UICollectionViewCell

@property (nonatomic, strong) SPDChatroomRedPacketMessage *message;
@property (nonatomic, weak) id<ChatroomRedPacketMsgCellDelegate> delegate;

@end
