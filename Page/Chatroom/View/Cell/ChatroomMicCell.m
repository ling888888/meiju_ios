//
//  ChatroomMicCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/12/26.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "ChatroomMicCell.h"
#import "SPDVoiceLiveUserModel.h"
#import "SPDCommonDefine.h"

@interface ChatroomMicCell ()

@property (weak, nonatomic) IBOutlet UIImageView *animationImageView;
@property (weak, nonatomic) IBOutlet UIImageView *nobleAvatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *micStatusImageView;

@property(nonatomic, strong) LY_PortraitView *portraitView;
@end

@implementation ChatroomMicCell

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
        _portraitView.userInteractionEnabled = YES;
    }
    return _portraitView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UILongPressGestureRecognizer *longPressAvatarImageView = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressAvatarImageView:)];
    [self.portraitView addGestureRecognizer:longPressAvatarImageView];
    
    NSMutableArray *array = [NSMutableArray array];
    for (NSInteger i = 1; i < 4; i++) {
        [array addObject:[UIImage imageNamed:[NSString stringWithFormat:@"chatroom_voice_effect%ld", i]]];
    }
    self.animationImageView.animationImages = array;
    self.animationImageView.animationDuration = 1;
    self.animationImageView.animationRepeatCount = 0;
    
    [self.avatarImageView addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.avatarImageView);
    }];
    
    self.nobleAvatarImageView.hidden = YES;
}

- (void)longPressAvatarImageView:(UILongPressGestureRecognizer *)sender {
    if (self.model && sender.state == UIGestureRecognizerStateBegan) {
        NSDictionary *userInfo = @{@"user_id": self.model._id, @"nick_name": self.model.nick_name, @"avatar": [NSString stringWithFormat:STATIC_DOMAIN_URL, self.model.avatar], @"gender": self.model.gender,Headwear:[SPDCommonTool isEmpty:_model.headwear]?@"":_model.portrait.headwearUrl};
        [[NSNotificationCenter defaultCenter] postNotificationName:CHATROOM_LONGPRESS object:nil userInfo:userInfo];
    }
}

- (void)setMicStatus:(MicStatus)micStatus {
    _micStatus = micStatus;
    
    [self.avatarImageView sd_cancelCurrentImageLoad];
    switch (_micStatus) {
        case NoStatus:
            self.portraitView.imageView.image = [UIImage imageNamed:@"ic_weishangmai"];
//            self.avatarImageView.image = [UIImage imageNamed:@"ic_weishangmai"];
            self.micStatusImageView.hidden = YES;
            break;
        case Speeking:
            self.micStatusImageView.hidden = NO;
            self.micStatusImageView.image = [UIImage imageNamed:@"ic_shangmaishuohuashi"];
            break;
        case Mute:
            self.micStatusImageView.hidden = NO;
            self.micStatusImageView.image = [UIImage imageNamed:@"ic_jingyinshi"];
            break;
        case Forbidden:
            self.micStatusImageView.hidden = NO;
            self.micStatusImageView.image = [UIImage imageNamed:@"ic_jinmai"];
            break;
        case CannotUse:
            self.portraitView.imageView.image = [UIImage imageNamed:@"fengmaizhuangtai"];
//            self.avatarImageView.image = [UIImage imageNamed:@"fengmaizhuangtai"];
            self.micStatusImageView.hidden = YES;
            break;
        case Freeze:
            self.portraitView.imageView.image = [UIImage imageNamed:@"ic_weishangmai"];
//            self.avatarImageView.image = [UIImage imageNamed:@"ic_weishangmai"];
            self.micStatusImageView.hidden = YES;
            break;
        default:
            break;
    }
}

- (void)setModel:(SPDVoiceLiveUserModel *)model {
    _model = model;
    self.portraitView.headwearImageView.hidden = !_model.portrait.headwearUrl.notEmpty;

    if (!self.micStatusImageView.hidden) {
        if (_model) {
            self.portraitView.portrait = model.portrait;
        } else {
            self.portraitView.imageView.image = [UIImage imageNamed:@"ic_weishangmai"];
        }
    }
}

- (void)setAnimating:(BOOL)animating {
    _animating = animating;
    self.animationImageView.hidden = !_animating;
    if (_animating) {
        [self.animationImageView startAnimating];
//        self.avatarImageView.layer.borderWidth = 0;
    } else {
        [self.animationImageView stopAnimating];
//        self.avatarImageView.layer.borderWidth = 1;
    }
}

- (void)setHighlighted:(BOOL)highlighted {
    
}

- (void)setSelected:(BOOL)selected {
    
}

- (void)startPlayEmoji:(NSString *)url {
    if ([SPDCommonTool isEmpty:url]) {
        return;
    }
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
    CGImageSourceRef gifSource = CGImageSourceCreateWithData(CFBridgingRetain(data), nil);
    size_t gifCount =CGImageSourceGetCount(gifSource);
    NSMutableArray *images = [[NSMutableArray alloc]init];
    for(size_t i = 0 ; i< gifCount; i++) {
        CGImageRef imageRef =CGImageSourceCreateImageAtIndex(gifSource, i,NULL);
        UIImage*image = [UIImage imageWithCGImage:imageRef];
        [images addObject:image];
        CGImageRelease(imageRef);
    }
    self.emojiImageView.animationImages = images;
    self.emojiImageView.animationDuration = 0.1 * images.count;
    self.emojiImageView.animationRepeatCount = 1;
    [self.emojiImageView startAnimating];
}

@end
