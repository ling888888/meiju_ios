//
//  ChatroomLockTitleCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/11/19.
//  Copyright © 2018 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatroomLockTitleCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;

@end

NS_ASSUME_NONNULL_END
