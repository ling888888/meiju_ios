//
//  SPDClanCallMessageCell.m
//  SimpleDate
//
//  Created by Monkey on 2017/6/22.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDClanCallMessageCell.h"
#import "SPDClanCallMessage.h"

@interface SPDClanCallMessageCell ()

@property (nonatomic,strong)UIImageView * bubbleBackgroundView;

@property (nonatomic,strong)UILabel *contentLabel;

@property (nonatomic,strong)UIButton *joinButton;

@property (nonatomic,copy) NSString * clan_id;
@property (nonatomic,copy) NSString * clan_name;

@property (nonatomic,strong)UILabel * titleLabel ;



@end

@implementation SPDClanCallMessageCell

- (NSDictionary *)attributeDictionary {
    if (self.messageDirection == MessageDirection_SEND) {
        return @{
                 @(NSTextCheckingTypeLink) : @{NSForegroundColorAttributeName : [UIColor blueColor]},
                 @(NSTextCheckingTypePhoneNumber) : @{NSForegroundColorAttributeName : [UIColor blueColor]}
                 };
    } else {
        return @{
                 @(NSTextCheckingTypeLink) : @{NSForegroundColorAttributeName : [UIColor blueColor]},
                 @(NSTextCheckingTypePhoneNumber) : @{NSForegroundColorAttributeName : [UIColor orangeColor]}
                 };
    }
    return nil;
}

- (NSDictionary *)highlightedAttributeDictionary {
    return [self attributeDictionary];
}
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    self.bubbleBackgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, pixwn(275),pixwn(128))];
    self.bubbleBackgroundView.contentMode = UIViewContentModeScaleAspectFit;
    [self.messageContentView addSubview:self.bubbleBackgroundView];
    
    self.bubbleBackgroundView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapPress =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPress:)];
    [self.bubbleBackgroundView addGestureRecognizer:tapPress];
    
    //初始化content label
    self.contentLabel  = [[UILabel alloc]initWithFrame:CGRectMake(pixwn(20), pixwn(30), pixwn(271)-pixwn(20), pixwn(60))];
//    self.contentLabel.backgroundColor = [UIColor redColor];
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.textColor = SPD_HEXCOlOR(@"ffffff");
    self.contentLabel.font = [UIFont systemFontOfSize:pixwn(14)];
    [self.messageContentView addSubview:self.contentLabel];
    
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.frame = CGRectMake(0, 6, pixwn(275), 14);
    self.titleLabel.text = SPDStringWithKey(@"家族召唤", nil);
//    self.titleLabel.text = @"家族召唤";
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.messageContentView addSubview:self.titleLabel];
    
    self.joinButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.joinButton setTitle:SPDStringWithKey(@"进入", nil) forState:UIControlStateNormal];
    self.joinButton.titleLabel.font = [UIFont systemFontOfSize:12];
    self.joinButton.frame = CGRectMake(self.bubbleBackgroundView.frame.size.width - 10 - 55, CGRectGetMaxY(self.contentLabel.frame)+pixwn(8), 55, 25);
    self.joinButton.userInteractionEnabled = NO;
    [self.joinButton setBackgroundImage:[UIImage imageNamed:@"clancall_enter"] forState:UIControlStateNormal];
    [self.messageContentView addSubview:self.joinButton];
}

-(void)handleMsgClicked:(UIButton *)btn {
   
}

- (void)setDataModel:(RCMessageModel *)model {
    
    [super setDataModel:model];
    
    [self setAutoLayout];
    
}
- (void)setAutoLayout {

    CGRect messageContentFrame = self.messageContentView.frame;
    messageContentFrame.size.width = pixwn(275);
    messageContentFrame.size.height = pixwn(128);
    self.messageContentView.frame = messageContentFrame;
    
//    self.contentLabel.backgroundColor = [UIColor redColor];
    
    RCMessageModel *model  =       self.model;
    SPDClanCallMessage *message = (SPDClanCallMessage *)model.content;
    
    NSMutableAttributedString * attributedString1 = [[NSMutableAttributedString alloc] initWithString:message.content?:SPDStringWithKey(@"家族召唤", nil)];
    NSMutableParagraphStyle * paragraphStyle1 = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle1 setLineSpacing:3];
    [attributedString1 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle1 range:NSMakeRange(0, [attributedString1 length])];
    
    [self.contentLabel setAttributedText:attributedString1];
    
    self.clan_id = message.clan_id;
    self.clan_name = message.clan_name;
   
    //根据方向来排版
    if (MessageDirection_SEND == self.messageDirection) {
        
//        self.bubbleBackgroundView.image=[UIImage imageNamed:@"clancall_bg"];
        
    }else if(MessageDirection_RECEIVE ==self.messageDirection){
        
        self.bubbleBackgroundView.image=[UIImage imageNamed:@"clancall_bg"];
        
    }
    
    
    
}
- (UIImage *)imageNamed:(NSString *)name ofBundle:(NSString *)bundleName {
    UIImage *image = nil;
    NSString *image_name = [NSString stringWithFormat:@"%@.png", name];
    NSString *resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString *bundlePath = [resourcePath stringByAppendingPathComponent:bundleName];
    NSString *image_path = [bundlePath stringByAppendingPathComponent:image_name];
    image = [[UIImage alloc] initWithContentsOfFile:image_path];
    return image;
}

- (void)tapPress:(id)sender {
    UITapGestureRecognizer *press = (UITapGestureRecognizer *)sender;
    if (press.state == UIGestureRecognizerStateEnded) {
        [self.delegate didTapMessageCell:self.model];
        return;
    }
}

//升级SDK之后-
+(CGSize)sizeForMessageModel:(RCMessageModel *)model withCollectionViewWidth:(CGFloat)collectionViewWidth referenceExtraHeight:(CGFloat)extraHeight{
    return CGSizeMake(kScreenW, 128+extraHeight);
}


@end
