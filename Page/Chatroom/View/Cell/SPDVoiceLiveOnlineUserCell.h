//
//  SPDVoiceLiveOnlineUserCell.h
//  SimpleDate
//
//  Created by 李楠 on 17/5/15.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    NoStatus,
    Speeking,
    Mute,
    Forbidden,
    CannotUse,
    Freeze
}MicStatus;

@class SPDVoiceLiveUserModel;

@interface SPDVoiceLiveOnlineUserCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userIcon;
@property (weak, nonatomic) IBOutlet UIImageView *micStatusImage;
@property (weak, nonatomic) IBOutlet UIImageView *animationView;
@property (weak, nonatomic) IBOutlet UIImageView *publicMicImage;
@property (weak, nonatomic) IBOutlet UIImageView *nobleAvatarImageView;

@property(nonatomic, strong) LY_PortraitView *portraitView;

@property (assign, nonatomic) MicStatus micStatus;
@property (strong, nonatomic) SPDVoiceLiveUserModel *model;
@property (assign, nonatomic) BOOL animating;

@end
