//
//  ChatroomBgImageCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/8/21.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "ChatroomBgImageCell.h"
#import "ChatroomBgImageModel.h"

@interface ChatroomBgImageCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIImageView *activityImageView;
@property (weak, nonatomic) IBOutlet UILabel *activityLabel;
@property (weak, nonatomic) IBOutlet UIImageView *ownedImageView;
@property (weak, nonatomic) IBOutlet UIImageView *usedImageView;
@property (weak, nonatomic) IBOutlet UILabel *expireTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *goldImageView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;

@end

@implementation ChatroomBgImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.ownedImageView.image = [UIImage imageNamed:@"ic_chatroom_bg_owned"].adaptiveRtl;
    self.activityImageView.image = [UIImage imageNamed:@"specialNum_activity_bg"].adaptiveRtl;
}

- (void)setModel:(ChatroomBgImageModel *)model {
    _model = model;
    
    [self.imageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _model.image]]];
    self.nameLabel.text = _model.name;
    self.ownedImageView.hidden = !_model.isOwn;
    if (!_model.isOwn) {
        self.activityImageView.hidden = [SPDCommonTool isEmpty:_model.activity];
        self.activityLabel.hidden = [SPDCommonTool isEmpty:_model.activity];
        self.activityLabel.text = _model.activity;
    } else {
        self.activityImageView.hidden = YES;
        self.activityLabel.hidden = YES;
    }
    self.usedImageView.hidden = !_model.isUse;
    self.goldImageView.image = [UIImage imageNamed:[_model.buy_type isEqualToString:@"diamond"] ? @"ic_store_diamonds":@"ic_gold_num"];

    if (!_model.price || !_model.price.integerValue) {
        self.goldImageView.hidden = YES;
        self.priceLabel.hidden = YES;
        self.buyButton.hidden = YES;
        self.expireTimeLabel.hidden = YES;
    } else {
        self.goldImageView.hidden = NO;
        self.priceLabel.hidden = NO;
        self.buyButton.hidden = NO;
        if (_model.isOwn) {
            self.expireTimeLabel.hidden = NO;
            self.expireTimeLabel.text = _model.expireTimeStr;
            self.priceLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"%@／%@天", nil), _model.renewPrice, _model.day];
            [self.buyButton setTitle:SPDStringWithKey(@"续费", nil) forState:UIControlStateNormal];
        } else {
            self.expireTimeLabel.hidden = YES;
            self.priceLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"%@／%@天", nil), _model.price, _model.day];
            [self.buyButton setTitle:SPDStringWithKey(@"购买", nil) forState:UIControlStateNormal];
        }
    }
}

- (IBAction)clickBuyButton:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickBuyBgImageWithModel:)]) {
        [self.delegate didClickBuyBgImageWithModel:self.model];
    }
}

@end
