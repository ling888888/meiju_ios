//
//  ChatroomRankListCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/11/27.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "ChatroomRankListCell.h"
#import "ChatroomRankModel.h"

@interface ChatroomRankListCell ()

@property (weak, nonatomic) IBOutlet UIImageView *rankImageView;
@property (weak, nonatomic) IBOutlet UILabel *rankLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *nobleAvatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *genderAgeBtn;
@property (weak, nonatomic) IBOutlet UIButton *levelBtn;
@property (weak, nonatomic) IBOutlet UILabel *goldLabel;

@end

@implementation ChatroomRankListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    if ([SPDCommonTool currentVersionIsArbic]) {
        [self.genderAgeBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        [self.genderAgeBtn setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 2.5)];
    }
}

- (void)setModel:(ChatroomRankModel *)model {
    _model = model;
    
    if (_model.rank.integerValue == 2 || _model.rank.integerValue == 3) {
        self.rankImageView.hidden = NO;
        self.rankLabel.hidden = YES;
        self.rankImageView.image = [UIImage imageNamed:_model.rank.integerValue == 2 ? @"chatroom_rank_second" : @"chatroom_rank_third"];
    } else {
        self.rankImageView.hidden = YES;
        self.rankLabel.hidden = NO;
        self.rankLabel.text = _model.rank.stringValue;
    }
    [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _model.avatar]]];
    if (![SPDCommonTool isEmpty:_model.noble]) {
        self.nobleAvatarImageView.hidden = NO;
        self.nobleAvatarImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"avatar_%@_50", _model.noble]];
    } else {
        self.nobleAvatarImageView.hidden = YES;
    }
    self.nickNameLabel.text = _model.nickName;
    if ([_model.gender isEqualToString:@"female"]) {
        [self.genderAgeBtn setBackgroundColor:[UIColor colorWithHexString:@"ff7eca"]];
        [self.genderAgeBtn setImage:[UIImage imageNamed:@"chatroomuser_female"] forState:UIControlStateNormal];
    } else if ([_model.gender isEqualToString:@"male"]) {
        [self.genderAgeBtn setBackgroundColor:[UIColor colorWithHexString:@"5db4fd"]];
        [self.genderAgeBtn setImage:[UIImage imageNamed:@"chatroomuser_male"] forState:UIControlStateNormal];
    }
    [self.genderAgeBtn setTitle:_model.age.stringValue forState:UIControlStateNormal];
    [SPDCommonTool configDataForLevelBtnWithLevel:[_model.level intValue] andUIButton:self.levelBtn];
    self.goldLabel.text = _model.goldStr;
}

@end
