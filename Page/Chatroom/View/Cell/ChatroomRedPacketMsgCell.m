//
//  ChatroomRedPacketMsgCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/9/4.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "ChatroomRedPacketMsgCell.h"

@interface ChatroomRedPacketMsgCell ()

@property (nonatomic, strong) UIImageView *bgImageView;
@property (nonatomic, strong) UILabel *messageLabel;

@end

@implementation ChatroomRedPacketMsgCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews {
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"chatroom_rp_famy"]];
    imageView.layer.cornerRadius = 18;
    imageView.clipsToBounds = YES;
    imageView.layer.borderColor = [UIColor whiteColor].CGColor;
    imageView.layer.borderWidth = 1;
    [self addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(1.5);
        make.leading.mas_equalTo(7.5);
        make.width.mas_equalTo(36);
        make.height.mas_equalTo(36);
    }];
    
    UILabel *label = [[UILabel alloc] init];
    label.text = @"Famy Plus";
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont systemFontOfSize:12];
    [self addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.leading.mas_equalTo(54);
        make.height.mas_equalTo(14);
    }];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"chatroom_rp_msg"]];
    imageView1.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickRedPacket)];
    [imageView1 addGestureRecognizer:tap];
    [self addSubview:imageView1];
    [imageView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(label.mas_bottom).with.offset(7);
        make.leading.mas_equalTo(59);
        make.width.mas_equalTo(230);
        make.height.mas_equalTo(85);
    }];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"chatroom_rp_bag"]];
    [imageView1 addSubview:imageView2];
    [imageView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(8);
        make.leading.mas_equalTo(5);
        make.width.mas_equalTo(57);
        make.height.mas_equalTo(51);
    }];
    
    UILabel *label1 = [[UILabel alloc] init];
    label1.text = SPDStringWithKey(@"家族奖励金币", nil);
    label1.textColor = [UIColor colorWithHexString:@"ffe2b1"];
    label1.font = [UIFont systemFontOfSize:16];
    [imageView1 addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(15);
        make.leading.mas_equalTo(imageView2.mas_trailing).with.offset(10);
        make.trailing.mas_equalTo(-10);
        make.height.mas_equalTo(19);
    }];
    
    UILabel *label2 = [[UILabel alloc] init];
    label2.text = SPDStringWithKey(@"领取金币", nil);
    label2.textColor = [UIColor colorWithHexString:@"ffe2b1"];
    label2.font = [UIFont systemFontOfSize:12];
    [imageView1 addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(label1.mas_bottom).with.offset(7);
        make.leading.mas_equalTo(imageView2.mas_trailing).with.offset(10);
        make.trailing.mas_equalTo(-10);
        make.height.mas_equalTo(15);
    }];
    
    UILabel *label3 = [[UILabel alloc] init];
    label3.text = SPDStringWithKey(@"Famy奖励", nil);
    label3.textColor = [UIColor colorWithHexString:@"333333"];
    label3.font = [UIFont systemFontOfSize:11];
    [imageView1 addSubview:label3];
    [label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(imageView2.mas_bottom).with.offset(9);
        make.leading.mas_equalTo(10);
        make.trailing.mas_equalTo(-10);
        make.height.mas_equalTo(14);
    }];
}

- (void)clickRedPacket {
    if ([self.delegate respondsToSelector:@selector(didClickRedPacketWithMessage:)]) {
        [self.delegate didClickRedPacketWithMessage:self.message];
    }
}

@end
