//
//  SPDVoiceLiveAudienceCell.m
//  SimpleDate
//
//  Created by 李楠 on 17/5/16.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDVoiceLiveAudienceCell.h"
#import "SPDVoiceLiveUserModel.h"

@implementation SPDVoiceLiveAudienceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.userIcon.layer.cornerRadius = 55 / 2;
}

- (void)setModel:(SPDVoiceLiveUserModel *)model {
    _model = model;
    [self.userIcon sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _model.avatar]]];
    self.nickNameLabel.text = _model.nick_name;
}

@end
