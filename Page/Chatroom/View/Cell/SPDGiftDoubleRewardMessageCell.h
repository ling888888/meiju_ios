//
//  SPDGiftDoubleRewardMessageCell.h
//  SimpleDate
//
//  Created by Monkey on 2018/7/23.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <RongIMKit/RongIMKit.h>

@interface SPDGiftDoubleRewardMessageCell : RCMessageCell

@property (nonatomic, strong) UIImageView *bubbleBackgroundView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIButton *walletBtn;

@end
