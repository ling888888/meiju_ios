//
//  ChatroomBgImageCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/8/21.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChatroomBgImageModel;

@protocol ChatroomBgImageCellDelegate <NSObject>

@optional

- (void)didClickBuyBgImageWithModel:(ChatroomBgImageModel *)model;

@end

@interface ChatroomBgImageCell : UICollectionViewCell

@property (nonatomic, strong) ChatroomBgImageModel *model;
@property (nonatomic, weak) id<ChatroomBgImageCellDelegate> delegate;

@end
