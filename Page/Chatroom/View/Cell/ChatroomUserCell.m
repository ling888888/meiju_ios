//
//  ChatroomUserCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/12/26.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "ChatroomUserCell.h"
#import "SPDVoiceLiveUserModel.h"
#import "SPDCommonDefine.h"

@interface ChatroomUserCell ()

@property (weak, nonatomic) IBOutlet UIImageView *headwearImageView;

@property (strong, nonatomic) LY_PortraitView *portraitView;

@end

@implementation ChatroomUserCell

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc]init];
    }
    return _portraitView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UILongPressGestureRecognizer *longPressAvatarImageView = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressAvatarImageView:)];
    self.portraitView.userInteractionEnabled = YES;
    [self.portraitView addGestureRecognizer:longPressAvatarImageView];
    [self addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
}

- (void)longPressAvatarImageView:(UILongPressGestureRecognizer *)sender {
    if (self.model && sender.state == UIGestureRecognizerStateBegan) {
        NSDictionary *userInfo = @{@"user_id": self.model._id, @"nick_name": self.model.nick_name, @"avatar": [NSString stringWithFormat:STATIC_DOMAIN_URL, self.model.avatar], @"gender": self.model.gender, Headwear:_model.headwear.notEmpty ? _model.portrait.headwearUrl : @""};
        [[NSNotificationCenter defaultCenter] postNotificationName:CHATROOM_LONGPRESS object:nil userInfo:userInfo];
    }
}

- (void)setModel:(SPDVoiceLiveUserModel *)model {
    _model = model;
    self.portraitView.portrait = model.portrait;
}

@end
