//
//  LY_ChatListAlertView.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/9.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_ChatListAlertView : UIView

@property(nonatomic, strong) UIView *contentView;

@property(nonatomic, assign) CGFloat safeAreaHeight;

/// 显示
- (void)present;

/// 移除消失
- (void)dismiss;

@end

NS_ASSUME_NONNULL_END
