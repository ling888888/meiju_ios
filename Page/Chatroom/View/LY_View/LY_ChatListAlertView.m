//
//  LY_ChatListAlertView.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/9.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_ChatListAlertView.h"

@interface LY_ChatListAlertView ()

@property(nonatomic, strong) UIButton *backgroundBtn;

@property(nonatomic, strong) UIView *bottomBgView;

@end

@implementation LY_ChatListAlertView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    [self.bottomBgView addRounded:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:10];
    [self.contentView addRounded:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:10];
}

- (void)setupUI {
    [self addSubview:self.backgroundBtn];
    
    [self addSubview:self.bottomBgView];
}

- (void)setupUIFrame {
    [self.backgroundBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
}


- (void)present {
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
//    [keyWindow addSubview:self];
    self.frame = keyWindow.bounds;
    
    CGFloat bottomBgViewH = self.bottomBgView.frame.size.height;
    self.bottomBgView.frame = CGRectMake(0, kScreenH, kScreenW, bottomBgViewH);
    self.backgroundBtn.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        weakSelf.bottomBgView.frame = CGRectMake(0, kScreenH - bottomBgViewH, kScreenW, bottomBgViewH);
        weakSelf.backgroundBtn.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismiss {
    CGFloat bottomBgViewH = self.bottomBgView.frame.size.height;
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        weakSelf.bottomBgView.frame = CGRectMake(0, kScreenH, kScreenW, bottomBgViewH);
        weakSelf.backgroundBtn.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
    } completion:^(BOOL finished) {
        [weakSelf removeFromSuperview];
    }];
}

- (UIButton *)backgroundBtn {
    if (!_backgroundBtn) {
        _backgroundBtn = [[UIButton alloc] init];
//        _backgroundBtn.backgroundColor = [UIColor clearColor];
        [_backgroundBtn addTarget:self action:@selector(backgroundBtnAction) forControlEvents:true];
    }
    return _backgroundBtn;
}

- (UIView *)bottomBgView {
    if (!_bottomBgView) {
        _bottomBgView = [[UIView alloc] init];
        _bottomBgView.backgroundColor = [UIColor whiteColor];
        
    }
    return _bottomBgView;
}

- (void)setContentView:(UIView *)contentView {
    _contentView = contentView;
    
    [self setContentView:contentView AndSafeAreaHeight:self.safeAreaHeight];
}

- (void)setSafeAreaHeight:(CGFloat)safeAreaHeight {
    _safeAreaHeight = safeAreaHeight;
    
    [self setContentView:self.contentView AndSafeAreaHeight:safeAreaHeight];
}

- (void)setContentView:(UIView *)contentView AndSafeAreaHeight:(CGFloat)safeAreaHeight {
    if (contentView == nil || safeAreaHeight == 0) {
        return;
    }
    CGFloat bottomBgViewH = safeAreaHeight;
    if ([UIDevice currentDevice].is_iPhoneX) {
        bottomBgViewH = safeAreaHeight + 34;
    }
    
    self.bottomBgView.frame = CGRectMake(0, kScreenH, kScreenW, bottomBgViewH);
    
    contentView.frame = CGRectMake(0, 0, kScreenW, safeAreaHeight);
    
    [self.bottomBgView addSubview:contentView];
}

- (void)backgroundBtnAction {
    // 移除当前弹窗
    [self dismiss];
    
    
    
}

@end
