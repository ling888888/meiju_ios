//
//  SPDVoiceLiveAudienceView.m
//  SimpleDate
//
//  Created by 李楠 on 17/5/18.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDVoiceLiveAudienceView.h"
#import "SPDVoiceLiveAudienceCell.h"
#import "SPDVoiceLiveUserModel.h"

@implementation SPDVoiceLiveAudienceView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.dataArray = [NSArray array];
    self.bgView.layer.cornerRadius = 7;
    [self animating];
    
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    [self.collectionView registerNib:[UINib nibWithNibName:@"SPDVoiceLiveAudienceCell" bundle:nil] forCellWithReuseIdentifier:@"SPDVoiceLiveAudienceCell"];
}

- (IBAction)close:(UIButton *)sender {
    [self removeFromSuperview];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SPDVoiceLiveAudienceCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SPDVoiceLiveAudienceCell" forIndexPath:indexPath];
    SPDVoiceLiveUserModel *model = _dataArray[indexPath.row];
    cell.model = model;
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    SPDVoiceLiveUserModel *model = _dataArray[indexPath.row];
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectAudience:index:)]) {
        [self.delegate didSelectAudience:model._id index:_index];
        [self removeFromSuperview];
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 26;
}

- (void)setDataArray:(NSArray *)dataArray {
    _dataArray = dataArray;
    [self.collectionView reloadData];
}

- (void)animating {
    CAKeyframeAnimation * animation;
    animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = 0.30;
    animation.removedOnCompletion = YES;
    animation.fillMode = kCAFillModeForwards;
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1, 1.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    [_bgView.layer addAnimation:animation forKey:nil];
    
    [UIView animateWithDuration:0.30 animations:^{
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    }];
}

@end
