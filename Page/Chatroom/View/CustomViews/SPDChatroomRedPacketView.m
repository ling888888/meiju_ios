//
//  SPDChatroomRedPacketView.m
//  SimpleDate
//
//  Created by 李楠 on 2018/9/3.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDChatroomRedPacketView.h"
#import "SPDChatroomRedPacketCell.h"
#import "ChatroomRedPacketModel.h"

@interface SPDChatroomRedPacketView ()<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *welfareLabel;
@property (weak, nonatomic) IBOutlet UILabel *getLabel;
@property (weak, nonatomic) IBOutlet UIButton *getBtn;
@property (weak, nonatomic) IBOutlet UILabel *ruleLabel;
@property (weak, nonatomic) IBOutlet UILabel *successLabel;
@property (weak, nonatomic) IBOutlet UILabel *goldLabel;
@property (weak, nonatomic) IBOutlet UIImageView *goldImageView;
@property (weak, nonatomic) IBOutlet UILabel *depositLabel;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;
@property (weak, nonatomic) IBOutlet UIView *listBgView;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation SPDChatroomRedPacketView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.welfareLabel.text = SPDStringWithKey(@"家族奖励金币", nil);
    self.getLabel.text = SPDStringWithKey(@"快来领取吧", nil);
    [self.getBtn setTitle:SPDStringWithKey(@"领", nil) forState:UIControlStateNormal];
    self.ruleLabel.text = SPDStringWithKey(@"在线人数达到10人即可产生福利金币", nil);
    if ([SPDCommonTool currentVersionIsArbic]) {
        [self.goldLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(-21);
        }];
    }
    self.depositLabel.text = SPDStringWithKey(@"金币已发至你的钱包", nil);
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerNib:[UINib nibWithNibName:@"SPDChatroomRedPacketCell" bundle:nil] forCellReuseIdentifier:@"SPDChatroomRedPacketCell"];
    [self animate];
}

- (void)setModel:(ChatroomRedPacketModel *)model {
    _model = model;

    if (_model.status.integerValue == 0) {
        self.welfareLabel.hidden = NO;
        self.getLabel.hidden = NO;
        self.successLabel.hidden = YES;
        self.goldLabel.hidden = YES;
        self.goldImageView.hidden = YES;
        self.depositLabel.hidden = YES;
        self.resultLabel.hidden = YES;
        self.listBgView.hidden = YES;
    } else {
        self.welfareLabel.hidden = YES;
        self.getLabel.hidden = YES;
        self.successLabel.hidden = NO;
        self.listBgView.hidden = NO;
        self.countLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"领取%@/%@个", nil), _model.gotCount, _model.totalCount];
        self.dataArray = _model.list;
        [self.tableView reloadData];
        if (_model.status.integerValue == 1) {
            self.goldLabel.hidden = NO;
            self.goldImageView.hidden = NO;
            self.depositLabel.hidden = NO;
            self.resultLabel.hidden = YES;
            self.successLabel.text = SPDStringWithKey(@"领取金币成功", nil);
            self.goldLabel.text = [NSString stringWithFormat:@"%@", _model.gold];
            [self.listBgView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(230);
            }];
        } else {
            self.goldLabel.hidden = YES;
            self.goldImageView.hidden = YES;
            self.depositLabel.hidden = YES;
            self.resultLabel.hidden = NO;
            self.successLabel.text = SPDStringWithKey(@"家族奖励金币", nil);
            if (_model.status.integerValue == 2) {
                self.resultLabel.text = SPDStringWithKey(@"奖励金币已领完", nil);
            } else {
                self.resultLabel.text = SPDStringWithKey(@"奖励已过期", nil);
            }
            [self.listBgView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(260);
            }];
        }
    }
}

- (IBAction)clickCloseBtn:(UIButton *)sender {
    [self removeFromSuperview];
}

- (IBAction)clickGetBtn:(UIButton *)sender {
    sender.enabled = NO;
    NSDictionary *dic = @{@"redPacketId": _model.redPacketId};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"chatroom.redPacket.get" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        ChatroomRedPacketModel *model = [ChatroomRedPacketModel initWithDictionary:suceess];
        model.redPacketId = _model.redPacketId;
        self.model = model;
        sender.enabled = YES;
    } bFailure:^(id  _Nullable failure) {
        sender.enabled = YES;
        if ([failure isKindOfClass:[NSDictionary class]]) {
            if ([failure[@"code"] integerValue] == 2009) {
                [SPDCommonTool showWindowToast:SPDStringWithKey(@"本次福利金币已领取，请下次再来", nil)];
            } else {
                [SPDCommonTool showWindowToast:failure[@"msg"]];
            }
        }
    }];
}

- (void)animate {
    CAKeyframeAnimation *animation;
    animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = 0.30;
    animation.removedOnCompletion = YES;
    animation.fillMode = kCAFillModeForwards;
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1, 1.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    [self.bgView.layer addAnimation:animation forKey:nil];
    
    [UIView animateWithDuration:0.30 animations:^{
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.25];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SPDChatroomRedPacketCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SPDChatroomRedPacketCell" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

@end
