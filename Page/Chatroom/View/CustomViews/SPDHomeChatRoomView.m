//
//  SPDHomeChatRoomView.m
//  SimpleDate
//
//  Created by Monkey on 2017/6/7.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDHomeChatRoomView.h"

@interface SPDHomeChatRoomView ()

@property(nonatomic,strong)UIImageView *avatarImageView;

@property(nonatomic,strong)UILabel *nicknameLabel;

@property(nonatomic, strong) UILabel *noticeLabel;

@property(nonatomic,strong) UIImageView *nAvatarImageView;

@property(nonatomic, strong) UILabel *nNicknameLabel;

@property(nonatomic,strong)UILabel *contentLabel;

@property(nonatomic,assign)__block BOOL isShowing;

@property(nonatomic,assign)__block BOOL isWaiting;

@property (nonatomic,strong)NSMutableArray *globalMutableArray;

@property (nonatomic,strong)UILabel * clanlabel;

@property (nonatomic,strong)UIView * wordTxtMsgView;
@property (nonatomic,strong)UIView * sendGiftMsgView;

@property (nonatomic,strong)UIImageView *sgift_avatar;
@property (nonatomic,strong)UIImageView *rgift_avatar;
@property (nonatomic,strong)UILabel *rname_Label;
@property (nonatomic,strong)UILabel *sname_Label;
@property (nonatomic,strong)UIImageView *giftImageView;

@property (nonatomic,strong)NSDictionary * tapCurrentDict;

@property (nonatomic,strong)UILabel *fuction_Label; //送给 和 向 的label

@property (nonatomic,strong)UILabel *magicResultLabel; //魔法额成功失败

@end

@implementation SPDHomeChatRoomView

//- (UIImageView *)avatarImageView {
//    if (!_avatarImageView) {
//        _avatarImageView = [[UIImageView alloc]initWithFrame:CGRectZero];
//        [_avatarImageView setFrame:CGRectMake(13, 8, 57/2, 57/2)];
//        _avatarImageView.layer.cornerRadius = 57/4;
//        _avatarImageView.clipsToBounds = YES;
//        _avatarImageView.layer.borderWidth= 1.0;
//       _avatarImageView.layer.borderColor = SPD_HEXCOlOR(@"f2ca4e").CGColor;
//        [self.wordTxtMsgView addSubview:_avatarImageView];
//    }
//    return _avatarImageView;
//}
//
//- (UIView *)wordTxtMsgView {
//    if (!_wordTxtMsgView) {
//        _wordTxtMsgView = [[UIView alloc]initWithFrame:self.bounds];
//        [self addSubview:_wordTxtMsgView];
//        _wordTxtMsgView.userInteractionEnabled = NO;
//    }
//    return _wordTxtMsgView;
//}
//
//- (UILabel *)nicknameLabel {
//    if (!_nicknameLabel) {
//        _nicknameLabel = [[UILabel alloc]initWithFrame:CGRectZero];
//        _nicknameLabel.font = [UIFont systemFontOfSize:14];
//        [self.wordTxtMsgView addSubview:_nicknameLabel];
//    }
//    return _nicknameLabel;
//}
//
//- (UILabel *)noticeLabel {
//    if (!_noticeLabel) {
//        _noticeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
//        _noticeLabel.text = @"@";
//        _noticeLabel.textColor = SPD_HEXCOlOR(@"fae93b");
//        _noticeLabel.font = [UIFont boldSystemFontOfSize:20];
//        [self.wordTxtMsgView addSubview:_noticeLabel];
//    }
//    return _noticeLabel;
//}
//
//- (UIImageView *)nAvatarImageView {
//    if (!_nAvatarImageView) {
//        _nAvatarImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
//        _nAvatarImageView.layer.cornerRadius = 57 / 4;
//        _nAvatarImageView.clipsToBounds = YES;
//        _nAvatarImageView.layer.borderWidth= 1.0;
//        _nAvatarImageView.layer.borderColor = SPD_HEXCOlOR(@"f2ca4e").CGColor;
//        _nAvatarImageView.userInteractionEnabled = YES;
//        [self.wordTxtMsgView addSubview:_nAvatarImageView];
//    }
//    return _nAvatarImageView;
//}
//
//-(UILabel *)giveLabel {
//    if (!_giveLabel) {
//        _giveLabel = [[UILabel alloc]initWithFrame:CGRectMake(28, CGRectGetMaxY(self.sgift_avatar.frame)+12, 50, 22)];
//        _giveLabel.text = SPDStringWithKey(@"送给", nil);
//        _giveLabel.textColor = SPD_HEXCOlOR(@"fdcb18");
//        _giveLabel.font = [UIFont systemFontOfSize:22];
//        [self.sendGiftMsgView addSubview:_giveLabel];
//    }
//    return _giveLabel;
//}

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self configUI];

    }
    return self;
}

- (void)configUI {
    
    if (self.avatarImageView) {
        [self.avatarImageView removeFromSuperview];
        self.avatarImageView = nil;
    }
    
    if (self.nicknameLabel) {
        [self.nicknameLabel removeFromSuperview];
        self.nicknameLabel = nil;
    }

    if (self.contentLabel) {
        [self.contentLabel removeFromSuperview];
        self.contentLabel = nil;
    }
    
    if (self.clanlabel) {
        [self.clanlabel removeFromSuperview];
        self.clanlabel = nil;
    }
    
    if (self.wordTxtMsgView) {
        [self.wordTxtMsgView removeFromSuperview];
        self.wordTxtMsgView = nil;
    }
    if (self.sendGiftMsgView) {
        [self.sendGiftMsgView removeFromSuperview];
        self.sendGiftMsgView = nil;
    }
    if (self.sgift_avatar) {
        [self.sgift_avatar removeFromSuperview];
        self.sgift_avatar = nil;
    }
    if (self.rgift_avatar) {
        [self.rgift_avatar removeFromSuperview];
        self.rgift_avatar = nil;
    }
    if (self.rname_Label) {
        [self.rname_Label removeFromSuperview];
        self.rname_Label = nil;
    }
    if (self.sname_Label) {
        [self.sname_Label removeFromSuperview];
        self.sname_Label = nil;
    }
    if (self.sname_Label) {
        [self.sname_Label removeFromSuperview];
        self.sname_Label = nil;
    }
    
    [self configSubViews];
}
- (void)configSubViews {
    
    // 文本, @消息布局
    self.wordTxtMsgView = [[UIView alloc]initWithFrame:self.bounds];
    [self addSubview:self.wordTxtMsgView];
    self.wordTxtMsgView.userInteractionEnabled = NO;
    
    self.avatarImageView = [[UIImageView alloc]initWithFrame:CGRectZero];
    self.avatarImageView.layer.cornerRadius = 57/4;
    self.avatarImageView.clipsToBounds = YES;
    self.avatarImageView.layer.borderWidth= 1.0;
    self.avatarImageView.layer.borderColor = SPD_HEXCOlOR(@"f2ca4e").CGColor;
    [self.wordTxtMsgView addSubview:self.avatarImageView];
    [self.avatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(13);
        make.top.mas_equalTo(8);
        make.width.and.height.mas_equalTo(57 / 2);
    }];
    
    self.nicknameLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    self.nicknameLabel.font = [UIFont systemFontOfSize:14];
    [self.wordTxtMsgView addSubview:self.nicknameLabel];
    
    self.noticeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.noticeLabel.text = @"@";
    self.noticeLabel.textColor = SPD_HEXCOlOR(@"fae93b");
    self.noticeLabel.font = [UIFont boldSystemFontOfSize:20];
    [self.wordTxtMsgView addSubview:self.noticeLabel];
    [self.noticeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.nicknameLabel.mas_trailing).with.offset(6);
        make.top.mas_equalTo(12.25);
        make.width.and.height.mas_equalTo(20);
    }];
    
    self.nAvatarImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.nAvatarImageView.layer.cornerRadius = 57 / 4;
    self.nAvatarImageView.clipsToBounds = YES;
    self.nAvatarImageView.layer.borderWidth= 1.0;
    self.nAvatarImageView.layer.borderColor = SPD_HEXCOlOR(@"f2ca4e").CGColor;
    self.nAvatarImageView.userInteractionEnabled = YES;
    [self.wordTxtMsgView addSubview:self.nAvatarImageView];
    [self.nAvatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.noticeLabel.mas_trailing).with.offset(6);
        make.top.mas_equalTo(8);
        make.width.and.height.mas_equalTo(57 / 2);
    }];
    
    self.nNicknameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.nNicknameLabel.textColor = [UIColor whiteColor];
    self.nNicknameLabel.font = [UIFont systemFontOfSize:14];
    [self.wordTxtMsgView addSubview:self.nNicknameLabel];
    [self.nNicknameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.nAvatarImageView.mas_trailing).with.offset(6);
        make.top.mas_equalTo(14.75);
        make.width.mas_equalTo(75);
        make.height.mas_equalTo(15);
    }];
    
    self.contentLabel = [[UILabel alloc] init];
    self.contentLabel.font= [UIFont systemFontOfSize:13];
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.textColor = [UIColor whiteColor];
    [self.wordTxtMsgView addSubview:self.contentLabel];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.avatarImageView);
        make.top.mas_equalTo(self.avatarImageView.mas_bottom).with.offset(5);
        make.width.mas_lessThanOrEqualTo(kScreenW - 26 - 13);
    }];
    
    self.clanlabel = [[UILabel alloc] init];
    self.clanlabel.font= [UIFont systemFontOfSize:12];
    self.clanlabel.textColor = SPD_HEXCOlOR(@"ffebd9");
    self.clanlabel.userInteractionEnabled=YES;
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapLabel:)];
    [self addGestureRecognizer:tap];
    [self addSubview:self.clanlabel];
    [self.clanlabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-20);
        make.bottom.mas_equalTo(-3);
        make.height.mas_equalTo(20);
    }];
    
    
    
    // 礼物消息布局
    self.sendGiftMsgView = [[UIView alloc]initWithFrame:self.bounds];
    [self addSubview:self.sendGiftMsgView];
    self.sendGiftMsgView.userInteractionEnabled = NO;
    
    CGFloat leading = 28;
    
    //发送头像
    self.sgift_avatar = [[UIImageView alloc] init];
    [self.sendGiftMsgView addSubview:self.sgift_avatar];
    [self.sgift_avatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(leading);
        make.top.mas_equalTo(15);
        make.width.and.height.mas_equalTo(34);
    }];
    self.sgift_avatar.userInteractionEnabled = YES;
    self.sgift_avatar.layer.cornerRadius = 17;
    self.sgift_avatar.clipsToBounds = YES;
    
    self.sname_Label = [[UILabel alloc] init];
    [self.sendGiftMsgView addSubview:self.sname_Label];
    [self.sname_Label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.sgift_avatar.mas_trailing).with.offset(5);
        make.top.mas_equalTo(25);
        make.height.mas_equalTo(20);
    }];
    self.sname_Label.font = [UIFont systemFontOfSize:14];
    self.sname_Label.textColor = [UIColor whiteColor];
    
   self.fuction_Label = [[UILabel alloc] init];
    self.fuction_Label.text = SPDStringWithKey(@"送给", nil);
    self.fuction_Label.textColor = SPD_HEXCOlOR(@"fdcb18");
    self.fuction_Label.font = [UIFont systemFontOfSize:22];
    [self.sendGiftMsgView addSubview:self.fuction_Label];
    [self.fuction_Label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(leading);
        make.top.mas_equalTo(self.sgift_avatar.mas_bottom).with.offset(12);
        make.height.mas_equalTo(22);
    }];
    
    self.rgift_avatar = [[UIImageView alloc] init];
    [self.sendGiftMsgView addSubview:self.rgift_avatar];
    [self.rgift_avatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.fuction_Label.mas_trailing).with.offset(5);
        make.top.mas_equalTo(self.sgift_avatar.mas_bottom).with.offset(6);
        make.width.and.height.mas_equalTo(34);
    }];
    self.rgift_avatar.layer.cornerRadius = 17;
    self.rgift_avatar.clipsToBounds = YES;
    
    self.rname_Label = [[UILabel alloc] init];
    [self.sendGiftMsgView addSubview:self.rname_Label];
    [self.rname_Label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.rgift_avatar.mas_trailing).with.offset(5);
        make.top.mas_equalTo(self.sgift_avatar.mas_bottom).with.offset(15);
        make.trailing.mas_lessThanOrEqualTo(-70);
        make.height.mas_equalTo(20);
    }];
    self.rname_Label.font = [UIFont systemFontOfSize:14];
    self.rname_Label.textColor = [UIColor whiteColor];
    
    self.giftImageView = [[UIImageView alloc] init];
    self.giftImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.sendGiftMsgView addSubview:self.giftImageView];
    [self.giftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.rname_Label.mas_trailing).with.offset(9);
        make.top.mas_equalTo(self.sgift_avatar.mas_bottom).with.offset(5);
        make.width.and.height.mas_equalTo(40);
    }];
    
    self.magicResultLabel = [[UILabel alloc]init];
    [self.sendGiftMsgView addSubview:self.magicResultLabel];
    self.magicResultLabel.textColor = [UIColor whiteColor];
    self.magicResultLabel.font = [UIFont systemFontOfSize:13];
    self.magicResultLabel.hidden = YES;
    [self.magicResultLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.giftImageView.mas_trailing).with.offset(5);
        make.top.mas_equalTo(self.sgift_avatar.mas_bottom).with.offset(15);
    }];
    
}

-(void)handleTapLabel:(UITapGestureRecognizer *) gestureRecognizer{
    if (self.deleage && [self.deleage respondsToSelector:@selector(sphHomeclickedClanWithId:andClan_name:)]) {
        [self.deleage sphHomeclickedClanWithId:self.tapCurrentDict[@"clan_id"] andClan_name:self.tapCurrentDict[@"clan_name"]];
    }
}

- (void)setDict:(NSDictionary *)dict {
    self.hidden = NO;
    _dict = dict;
    NSLog(@"view set dict");
    if (![self.globalMutableArray containsObject:dict]) {
        NSLog(@"globalMutableArray 不包含这条消息!");
        [self.globalMutableArray addObject:dict];
        if (!_isShowing) {
            NSLog(@"IS NOT SHOWING 开始动画");
            [self startAnimationViewWithDict:dict];
        }else{
            NSLog(@"IS SHOWING -----");
        }
        
    }else{
        NSLog(@"已经包含这条消息");
    }
    
}

-(void)removeCurrentViewWithDict {
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        CGRect myframe = self.frame;
        myframe.origin.x = -kScreenW;
        self.frame = myframe;
        
    } completion:^(BOOL finished) {
        if (finished) {
            CGRect myframe = self.frame;
            myframe.origin.x = kScreenW;
            self.frame = myframe;
            self.isShowing = NO;
            [self.globalMutableArray removeObjectAtIndex:0];
            if (self.globalMutableArray.count>=1) {
                [self startAnimationViewWithDict:self.globalMutableArray[0]];
            }
        }else{
            CGRect myframe = self.frame;
            myframe.origin.x = kScreenW;
            self.frame = myframe;
            self.isShowing = NO;
            [self.globalMutableArray removeObjectAtIndex:0];
            if (self.globalMutableArray.count>=1) {
                [self startAnimationViewWithDict:self.globalMutableArray[0]];
            }
        }


    }];

}

- (void)startAnimationViewWithDict:(NSDictionary *)dict {
    
    if (dict[@"type"] == nil) {
        
        self.sendGiftMsgView.hidden = YES;
        self.wordTxtMsgView.hidden = NO;
        
        self.nicknameLabel.text = dict[@"nickname"];
        [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:dict[@"avatar"]]];
        
        // @相关
        NSString *mentionedType = dict[@"mentionedType"];
        NSArray *mentionedList = dict[@"mentionedList"];
        if (mentionedType && [mentionedType isEqualToString:@"2"] && mentionedList && mentionedList.count == 1) {
            self.noticeLabel.hidden = NO;
            self.nAvatarImageView.hidden = NO;
            self.nNicknameLabel.hidden = NO;
            NSDictionary *user = mentionedList[0];
            
            [self.nicknameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                
            }];
            [self.nicknameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.leading.mas_equalTo(self.avatarImageView.mas_trailing).with.offset(6);
                make.top.mas_equalTo(14.75);
                make.width.mas_lessThanOrEqualTo(75);
                make.height.mas_equalTo(15);
            }];
            self.nicknameLabel.textColor = [UIColor whiteColor];
            
            [self.nAvatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:user[@"avatar"]]];
            
            self.nNicknameLabel.text = user[@"nick_name"];
            
            NSString *content = [dict[@"content"] stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"@%@ ", user[@"nick_name"]] withString:@""];
            self.contentLabel.text = content;
            
        } else {
            self.noticeLabel.hidden = YES;
            self.nAvatarImageView.hidden = YES;
            self.nNicknameLabel.hidden = YES;
            
            self.nicknameLabel.textColor = SPD_HEXCOlOR(@"dac064");
            [self.nicknameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                
            }];
            [self.nicknameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo(self.avatarImageView.mas_trailing).with.offset(6);
                make.top.mas_equalTo(14.75);
                make.width.mas_equalTo(250);
                make.height.mas_equalTo(15);
            }];
            
            self.contentLabel.text = dict[@"content"];
        }
  
    }else if([dict[@"type"] isEqualToString:@"2"] || [dict[@"type"] isEqualToString:@"3"]) {
        self.sendGiftMsgView.hidden = NO;
        self.magicResultLabel.hidden = YES;
        self.wordTxtMsgView.hidden = YES;
        self.fuction_Label.text = SPDStringWithKey(@"给", nil);
        [self.sgift_avatar sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:dict[@"savatar"]]];
        [self.rgift_avatar sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:dict[@"ravatar"]]];
        
        self.sname_Label.text = dict[@"sender_nickname"];
        self.rname_Label.text = dict[@"receive_nickname"];
        
        [self.giftImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:dict[@"gift_url"]]];
    }else if([dict[@"type"] isEqualToString:@"magic"]) {
        self.sendGiftMsgView.hidden = NO;
        self.wordTxtMsgView.hidden = YES;
        self.magicResultLabel.hidden = NO;
        self.fuction_Label.text = SPDStringWithKey(@"向", nil);
        [self.sgift_avatar sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:dict[@"savatar"]]];
        [self.rgift_avatar sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:dict[@"ravatar"]]];
        self.sname_Label.text = dict[@"sender_nickname"];
        self.rname_Label.text = [NSString stringWithFormat:@"%@%@",dict[@"receive_nickname"],SPDStringWithKey(@"使用", nil)];
        if ([dict[@"result"] isEqualToString:@"success"]) {
            self.magicResultLabel.text = SPDStringWithKey(@"成功", nil);
        }else{
            self.magicResultLabel.text = SPDStringWithKey(@"失败", nil);
        }
        [self.giftImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:dict[@"magic_url"]]];
    }
    
    // 下划线
    if (!dict[@"clan_id"] || [dict[@"clan_id"] isEqualToString:@""]) {
        self.userInteractionEnabled = NO;
        self.clanlabel.hidden = YES;
    } else {
        NSString * clan =dict[@"clan_id"];
        if (clan.length >11) {
            self.userInteractionEnabled = YES;
        }else{
            self.userInteractionEnabled = NO;
        }
        self.clanlabel.hidden = NO;
        
        NSDictionary *attribtDic = @{NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
        NSMutableAttributedString *resultStr = [[NSMutableAttributedString alloc]init];
        NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@",dict[@"clan_name"]] attributes:attribtDic];
        [resultStr appendAttributedString:attribtStr];
        self.clanlabel.attributedText = resultStr;
    }
    
    if (self.frame.origin.x == 0  && self.globalMutableArray.count>= 2) {
        NSLog(@"removeCurrentViewWithDict %ld",self.globalMutableArray.count);
        [self removeCurrentViewWithDict];
    }else{
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            CGRect myframe = self.frame;
            myframe.origin.x = 0;
            self.frame = myframe;
            self.isShowing = YES;
            self.tapCurrentDict = dict;
        } completion:^(BOOL finished) {
            if (finished) {
                //self view change
                if (self.globalMutableArray.count >1) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self removeCurrentViewWithDict];
                    });
                    
                }else{
                    self.isShowing = NO;
                }
                
            }else {
                self.isShowing = NO;
                CGRect myframe = self.frame;
                myframe.origin.x = 0;
                myframe.size.width = kScreenW;
                self.frame = myframe;
            }
        }];
    }
    
}
-(NSMutableArray *)globalMutableArray{
    if (!_globalMutableArray) {
        _globalMutableArray = [NSMutableArray array];
    }
    return _globalMutableArray;
}

@end
