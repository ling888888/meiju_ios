//
//  SPDVoiceLiveGlobalScrollView.m
//  SimpleDate
//
//  Created by 李楠 on 17/5/31.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDVoiceLiveGlobalScrollView.h"
#import "SPDVoiceLiveGlobalMessageCell.h"
#import "SPDGlobalGiftMessageView.h"
#import "SPDVehicleAnimationView.h"

@interface SPDVoiceLiveGlobalScrollView ()

@property (nonatomic, strong) NSMutableSet *allViews;
@property (nonatomic, strong) NSMutableSet *animatingViews;
@property (nonatomic, strong) NSMutableDictionary *reuseableViews;
@property (nonatomic, assign) BOOL isEnterBackground;

@end

@implementation SPDVoiceLiveGlobalScrollView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.userInteractionEnabled = NO;
        self.clipsToBounds = YES;
        self.isEnterBackground = NO;
        self.hideAfterAnimation = NO;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    return self;
}

- (void)setDic:(NSDictionary *)dic {
    _dic = dic;
    if (!self.isEnterBackground) {
        [self.dataArray addObject:dic];
        if (self.dataArray.count == 1) {
            [self initViewWithDic:dic];
        }
    }
}

- (void)initViewWithDic:(NSDictionary *)dic {
    if (self.isRepeat && [SPDCommonTool getGlobalMessageSwitch]) {
        return;
    }
    
    self.superview.hidden = NO;
    if (!dic[@"type"]) {
        // 世界消息
        SPDVoiceLiveGlobalMessageCell *globalMessageCell;
        NSMutableSet *set = self.reuseableViews[NSStringFromClass([SPDVoiceLiveGlobalMessageCell class])];
        if (set.count) {
            globalMessageCell = [set anyObject];
            [set removeObject:globalMessageCell];
        } else {
            globalMessageCell = [[SPDVoiceLiveGlobalMessageCell alloc] init];
            [self.allViews addObject:globalMessageCell];
        }
        [self addSubview:globalMessageCell];
        globalMessageCell.dic = dic;
        [self beginScrollWithView:globalMessageCell];
        
    } else if ([dic[@"type"] integerValue] == 2 || [dic[@"type"] integerValue] == 3 || [dic[@"type"] isEqualToString:@"magic"]) {
        // 礼物, 魔法消息
        SPDGlobalGiftMessageView *giftMessageView;
        NSMutableSet *set = self.reuseableViews[NSStringFromClass([SPDGlobalGiftMessageView class])];
        if (set.count) {
            giftMessageView = [set anyObject];
            [set removeObject:giftMessageView];
        } else {
            giftMessageView = [[SPDGlobalGiftMessageView alloc] init];
            [self.allViews addObject:giftMessageView];
        }
        [self addSubview:giftMessageView];
        giftMessageView.dic = dic;
        [self beginScrollWithView:giftMessageView];
        
    } else if ([dic[@"type"] integerValue] == -1 || [dic[@"type"] integerValue] == -2) {
        // 座驾动效
        SPDVehicleAnimationView *vehicleAnimationView;
        NSMutableSet *set = self.reuseableViews[NSStringFromClass([SPDVehicleAnimationView class])];
        if (set.count) {
            vehicleAnimationView = [set anyObject];
            [set removeObject:vehicleAnimationView];
        } else {
            vehicleAnimationView = [[[NSBundle mainBundle] loadNibNamed:@"SPDVehicleAnimationView" owner:self options:nil] firstObject];
            [self.allViews addObject:vehicleAnimationView];
        }
        if ([dic[@"type"] integerValue] == -1) {
            vehicleAnimationView.frame = CGRectMake(self.bounds.size.width, 0, 200, 75);
        } else {
            vehicleAnimationView.frame = CGRectMake(self.bounds.size.width, 0, 300, 112.5);
        }
        vehicleAnimationView.dic = dic;
        [self addSubview:vehicleAnimationView];
        [self beginScrollWithView:vehicleAnimationView];
    }
    
    if ([self.delegate respondsToSelector:@selector(beginScrollWithDic:)]) {
        [self.delegate beginScrollWithDic:dic];
    }
}

- (void)beginScrollWithView:(UIView *)view {
    [self.animatingViews addObject:view];
    CGFloat distance = view.bounds.size.width + self.space;
    [UIView animateWithDuration:distance / self.speed delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        CGRect frame = view.frame;
        frame.origin.x = self.bounds.size.width - frame.size.width - self.space;
        view.frame = frame;
    } completion:^(BOOL finished) {
        if (finished && !self.isEnterBackground) {
            if (self.isRepeat) {
                [self finishFirstAnimation:view];
            } else {
                [self performSelector:@selector(finishFirstAnimation:) withObject:view afterDelay:0.5 inModes:@[NSRunLoopCommonModes]];
            }
        } else {
            [self resetView:view];
        }
    }];
}

- (void)finishFirstAnimation:(UIView *)view {
    [self continueScrollWithView:view];
    if (self.dataArray.count > (self.isRepeat ? 1 : 0)) {
        [self.dataArray removeObjectAtIndex:0];
    }
    if (self.dataArray.count > 0) {
        [self initViewWithDic:self.dataArray[0]];
    }
}

- (void)continueScrollWithView:(UIView *)view {
    CGFloat distance = self.bounds.size.width - self.space;
    [UIView animateWithDuration:distance / self.speed delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        CGRect frame = view.frame;
        frame.origin.x = -frame.size.width;
        view.frame = frame;
    } completion:^(BOOL finished) {
        [self resetView:view];
    }];
}

- (void)resetView:(UIView *)view {
    self.superview.hidden = self.hideAfterAnimation;
    
    if ([view isKindOfClass:[SPDVehicleAnimationView class]]) {
        SPDVehicleAnimationView *vaView = (SPDVehicleAnimationView *)view;
        [vaView.animationImageView stopAnimating];
    }
    CGRect frame = view.frame;
    frame.origin.x = self.bounds.size.width;
    view.frame = frame;
    [view removeFromSuperview];
    [self.reuseableViews[NSStringFromClass([view class])] addObject:view];
    [self.animatingViews removeObject:view];
    
    if ([self.delegate respondsToSelector:@selector(didResetView:)]) {
        [self.delegate didResetView:view];
    }
}

- (void)removeAnimaton {
    [self.dataArray removeAllObjects];
    for (UIView *view in self.allViews) {
        [view.layer removeAllAnimations];
    }
}

- (void)resumeAnimation {
    if (self.dic && !self.animatingViews.count) {
        [self.dataArray removeAllObjects];
        [self.dataArray addObject:self.dic];
        [self initViewWithDic:self.dataArray[0]];
    }
}

- (void)enterForeground {
    if (self.isRepeat) {
        [self resumeAnimation];
    }
    self.isEnterBackground = NO;
}

- (void)enterBackground {
    self.isEnterBackground = YES;
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    for (UIView *view in self.allViews) {
        [view.layer removeAllAnimations];
    }
}

#pragma mark - Getters

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return  _dataArray;
}

- (NSMutableSet *)allViews {
    if (!_allViews) {
        _allViews = [NSMutableSet set];
    }
    return _allViews;
}

- (NSMutableSet *)animatingViews {
    if (!_animatingViews) {
        _animatingViews = [NSMutableSet set];
    }
    return _animatingViews;
}

- (NSMutableDictionary *)reuseableViews {
    if (!_reuseableViews) {
        _reuseableViews = [NSMutableDictionary dictionary];
        [_reuseableViews setObject:[NSMutableSet set] forKey:NSStringFromClass([SPDVoiceLiveGlobalMessageCell class])];
        [_reuseableViews setObject:[NSMutableSet set] forKey:NSStringFromClass([SPDGlobalGiftMessageView class])];
        [_reuseableViews setObject:[NSMutableSet set] forKey:NSStringFromClass([SPDVehicleAnimationView class])];
    }
    return _reuseableViews;
}

@end
