//
//  AutoSwitch.m
//  SimpleDate
//
//  Created by ling Hou on 2019/12/16.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "AutoSwitch.h"

static const CGFloat kSwitchItemSpace = 3.0;

@interface AutoSwitch ()

@property (nonatomic, strong) UIImageView * coverImage;

@end

@implementation AutoSwitch

- (instancetype) initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews {
    self.layer.cornerRadius = self.frame.size.height/2.0f;
    self.clipsToBounds = YES;
    CGFloat width = self.bounds.size.height -  kSwitchItemSpace* 2;
    self.coverImage = [[UIImageView alloc]initWithFrame:CGRectMake(kSwitchItemSpace, kSwitchItemSpace, width, width)];
    [self addSubview:self.coverImage];
    self.coverImage.image = [UIImage imageNamed:@"ic_liaotianshi_faxiaoxi_shijiekaiguan"];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap)];
    [self addGestureRecognizer:tap];
}

- (void)setTintColor:(UIColor *)tintColor {
    _tintColor = tintColor;
    self.backgroundColor = _tintColor;
}

- (void)setCoverImageName:(NSString *)coverImageName {
    _coverImageName = coverImageName;
    self.coverImage.image = [UIImage imageNamed:_coverImageName];
}

- (void)setCoverImageSize:(CGSize)coverImageSize {
    _coverImageSize = coverImageSize;
    CGRect frame = self.coverImage.frame;
    frame.size.width = _coverImageSize.width;
    frame.size.height = _coverImageSize.height;
    self.coverImage.frame = frame;
}

- (void)handleTap {
    self.on = !self.on;
    if (self.delegate && [self.delegate respondsToSelector:@selector(autoSwitch:turnSwitch:)]) {
        [self.delegate autoSwitch:self turnSwitch:self.on];
    }
    CGFloat width = self.coverImage.frame.size.width;
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        CGRect frame = self.coverImage.frame;
        frame.origin.x = self.on ? (self.bounds.size.width - kSwitchItemSpace -width) : kSwitchItemSpace;
        self.coverImage.frame = frame;
    } completion:^(BOOL finished) {
        self.backgroundColor  = self.on ? (self.onTintColor?:[UIColor greenColor]) : (self.tintColor?:[UIColor redColor]);
    }];
}



@end
