//
//  RCDLiveInputView.m
//  RongChatRoomDemo
//
//  Created by 杜立召 on 16/8/3.
//  Copyright © 2016年 rongcloud. All rights reserved.
//

#import "RCDLiveInputView.h"
#import "RCDLiveKitUtility.h"
#import "RCDLiveKitCommonDefine.h"
#import <RongIMLib/RongIMLib.h>

typedef void (^RCTKAnimationCompletionBlock)(BOOL finished);

@interface RCDLiveInputView () <UITextViewDelegate,AutoSwitchDelegate>

@property(nonatomic, strong) NSMutableArray *inputContainerSubViewConstraints;


- (void)setAutoLayoutForSubViews;

- (void)switchInputBoxOrRecord;

- (void)voiceRecordButtonTouchDown:(UIButton *)sender;
- (void)voiceRecordButtonTouchUpInside:(UIButton *)sender;
- (void)voiceRecordButtonTouchDragExit:(UIButton *)sender;
- (void)voiceRecordButtonTouchDragEnter:(UIButton *)sender;
- (void)voiceRecordButtonTouchUpOutside:(UIButton *)sender;

- (void)rcInputBar_registerForNotifications;
- (void)rcInputBar_unregisterForNotifications;
- (void)rcInputBar_didReceiveKeyboardWillShowNotification:(NSNotification *)notification;
- (void)rcInputBar_didReceiveKeyboardWillHideNotification:(NSNotification *)notification;

@end

@implementation RCDLiveInputView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.inputContainerSubViewConstraints = [[NSMutableArray alloc] init];
        [self resetInputBar];
        self.currentPositionY = frame.origin.y;
        self.originalPositionY = frame.origin.y;
        self.inputTextview_height=36.0f;
        self.backgroundColor = SPD_HEXCOlOR(@"ffffff");;
        
    }
    return self;
}

- (void)resetInputBar {
    if (self.inputContainerSubViewConstraints.count > 0) {
        [self.inputContainerView removeConstraints:_inputContainerSubViewConstraints];
        [_inputContainerSubViewConstraints removeAllObjects];
    }
    if (self.sendGlobalMessageSwitch) {
        [self.sendGlobalMessageSwitch removeFromSuperview];
        self.sendGlobalMessageSwitch = nil;
    }

    if (self.inputTextView) {
        [self.inputTextView removeFromSuperview];
        self.inputTextView = nil;
    }
    if (self.emojiButton) {
        [self.emojiButton removeFromSuperview];
        self.emojiButton = nil;
    }
    
    if (self.inputContainerView) {
        [self.inputContainerView removeFromSuperview];
        self.inputContainerView = nil;
    }
    
    self.inputContainerView = [[UIView alloc] initWithFrame:self.bounds];
    [self addSubview:_inputContainerView];
    
    [self configInputContainerView];
    [self rcInputBar_registerForNotifications];
}

- (void)configInputContainerView {
    
    _sendGlobalMessageSwitch = [[AutoSwitch alloc]initWithFrame:CGRectMake(5, 17, 45, 24)];
    _sendGlobalMessageSwitch.delegate = self;
    _sendGlobalMessageSwitch.tintColor = [UIColor colorWithHexString:@"#E8E8E8"];
    _sendGlobalMessageSwitch.onTintColor = [UIColor colorWithHexString:@"#5591FF"];
    [self.inputContainerView addSubview:_sendGlobalMessageSwitch];
    
    _sendMessageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_sendMessageButton setFrame:CGRectZero];
    [_sendMessageButton setImage:[UIImage imageNamed:@"ic_msg_unsend"].adaptiveRtl forState:UIControlStateNormal];
    [_sendMessageButton addTarget:self
                           action:@selector(didTouchSendMessage:)
                 forControlEvents:UIControlEventTouchUpInside];
    [_sendMessageButton setExclusiveTouch:YES];
    _sendMessageButton.layer.cornerRadius = 4;
    _sendMessageButton.clipsToBounds = YES;
    [self.inputContainerView addSubview:_sendMessageButton];
    
    self.emojiButton = [[UIButton alloc] initWithFrame:CGRectZero];
    [_emojiButton setImage:SPD_Image_BYNAME(@"ic_liaotianshi_faxiaoxi_biaoqing") forState:UIControlStateNormal];
    [_emojiButton setExclusiveTouch:YES];
    [_emojiButton addTarget:self action:@selector(didTouchEmojiDown:) forControlEvents:UIControlEventTouchUpInside];
    self.inputContainerView.backgroundColor = [UIColor clearColor];
    [self.inputContainerView addSubview:_emojiButton];
    
    self.inputTextView = [[LDTextView alloc] initWithFrame:CGRectZero];
    _inputTextView.delegate = self;
    [_inputTextView setTextColor:SPD_HEXCOlOR(@"#1A1A1A")];
    [_inputTextView setReturnKeyType:UIReturnKeySend];
    _inputTextView.font = [UIFont systemFontOfSize:15];
    _inputTextView.placeholderFont = [UIFont systemFontOfSize:15];
    _inputTextView.placeholderColor = SPD_HEXCOlOR(@"e8e8e8");
    _inputTextView.enablesReturnKeyAutomatically = YES;
    _inputTextView.placeholder = SPDStringWithKey(@"点击左侧切换到世界消息模式", nil);
    _inputTextView.placeholderAlignCenter = YES;
    _inputTextView.backgroundColor = [UIColor clearColor];
    self.inputTextView.textContainerInset = UIEdgeInsetsMake(12, 0, 12, 0);
    [self.inputContainerView addSubview:_inputTextView];
    [self setAutoLayoutForSubViews];
    
}

- (void)setAutoLayoutForSubViews{
    [self.sendGlobalMessageSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(5);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(45);
        make.height.mas_equalTo(24);
    }];
    
    [self.inputTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(5 + 45 + 10);
        make.trailing.mas_equalTo(self.emojiButton.mas_leading).with.offset(-2);
        make.centerY.mas_equalTo(0);
        make.height.mas_equalTo(44);
    }];
    
    [self.emojiButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(self.sendMessageButton.mas_leading).with.offset(-22);
        make.centerY.mas_equalTo(0);
        make.width.and.height.mas_equalTo(28);
    }];
    
    [self.sendMessageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(0);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(20/17*50);
    }];
}


- (BOOL)canBecomeFirstResponder {
    return YES;
}
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    return NO; //隐藏系统默认的菜单项
}


#pragma mark -  buttons

-(void)didTouchSendMessage:(UIButton *)sender {
    if (self.inputTextView.hidden) {
        [self switchInputBoxOrRecord];
    }
    [self resetSendMessageBtn];
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTouchSendMessageButtonWithText:enableGlobalMessage:)]) {
        [self.delegate didTouchSendMessageButtonWithText:self.inputTextView.text enableGlobalMessage:self.enableWorldMessage];
    }
    
}

// 麦上表情按钮
- (void)didTouchMicEmojiDown:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSendMicEmojiButton:)]) {
        [self.delegate didSendMicEmojiButton:sender];
    }
}

-(void)resetSendMessageBtn {
//    [self.sendMessageButton setBackgroundImage:[UIImage imageNamed:[SPDCommonTool getArImageNameWithImage:@"msg_unsend"]] forState:UIControlStateNormal];
    [self.sendMessageButton setImage:[UIImage imageNamed:@"ic_msg_unsend"].adaptiveRtl forState:UIControlStateNormal];
}

#pragma mark - Notifications

- (void)rcInputBar_registerForNotifications {
    [self rcInputBar_unregisterForNotifications];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(rcInputBar_didReceiveKeyboardWillShowNotification:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(rcInputBar_didReceiveKeyboardWillHideNotification:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)rcInputBar_unregisterForNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)rcInputBar_didReceiveKeyboardWillShowNotification:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];
    CGRect keyboardBeginFrame = [userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    CGRect keyboardEndFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    if (!CGRectEqualToRect(keyboardBeginFrame, keyboardEndFrame)) {
        UIViewAnimationCurve animationCurve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
        NSInteger animationCurveOption = (animationCurve << 16);
        
        double animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
        [UIView animateWithDuration:animationDuration
                              delay:0.0
                            options:animationCurveOption
                         animations:^{
                             if ([self.delegate respondsToSelector:@selector(keyboardWillShowWithFrame:)]) {
                                 [self.delegate keyboardWillShowWithFrame:keyboardEndFrame];
                             }
                         }
                         completion:^(BOOL finished) {
                             
                         }];
    }
}

- (void)rcInputBar_didReceiveKeyboardWillHideNotification:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    CGRect keyboardBeginFrame = [userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    CGRect keyboardEndFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    if (!CGRectEqualToRect(keyboardBeginFrame, keyboardEndFrame)) {
        UIViewAnimationCurve animationCurve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
        NSInteger animationCurveOption = (animationCurve << 16);
        
        double animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
        [UIView animateWithDuration:animationDuration
                              delay:0.0
                            options:animationCurveOption
                         animations:^{
                             if (!CGRectEqualToRect(keyboardBeginFrame, keyboardEndFrame)) {
                                 if ([self.delegate respondsToSelector:@selector(keyboardWillHide)]) {
                                     [self.delegate keyboardWillHide];
                                 }
                             }
                         }
                         completion:^(BOOL finished) {
                             
                         }];
    }
}


- (void)didTouchEmojiDown:(UIButton *)sender {
    if (self.inputTextView.hidden) {
        [self switchInputBoxOrRecord];
    }
    [self.delegate didTouchEmojiButton:sender];
}

#pragma mark <UITextViewDelegate>

- (void)changeInputViewFrame:(NSString *)text textView:(UITextView *)textView range:(NSRange)range {

    
    if (![SPDCommonTool isEmpty:text]) {
//        [self.sendMessageButton setBackgroundImage:[UIImage imageNamed:[SPDCommonTool getArImageNameWithImage:@"msg_send"]] forState:UIControlStateNormal];
        [self.sendMessageButton setImage:[UIImage imageNamed:@"ic_msg_send"].adaptiveRtl  forState:UIControlStateNormal];
    }
    if (text.length == 0) {
        [self resetSendMessageBtn];
    }
    _inputTextview_height = 36.0f;
    if (_inputTextView.contentSize.height < 70 && _inputTextView.contentSize.height > 36.0f) {
        _inputTextview_height = _inputTextView.contentSize.height;
    }
    if (_inputTextView.contentSize.height >= 70) {
        _inputTextview_height = 70;
        
    }
    NSString *inputStr = _inputTextView.text;
    CGSize textViewSize=[self TextViewAutoCalculateRectWith:inputStr FontSize:16.0 MaxSize:CGSizeMake(_inputTextView.frame.size.width, 70)];
    
    CGSize textSize=[self TextViewAutoCalculateRectWith:inputStr FontSize:16.0 MaxSize:CGSizeMake(_inputTextView.frame.size.width, 70)];
    if (textViewSize.height<=36.0f&&range.location==0) {
        _inputTextview_height=36.0f;
    }
    else if(textViewSize.height>36.0f&&textViewSize.height<=55.0f)
    {
        _inputTextview_height=55.0f;
    }
    else if (textViewSize.height>55)
    {
        _inputTextview_height=70.0f;
    }
    
    if ([text isEqualToString:@""]&&range.location!=0) {
        if (textSize.width>=_inputTextView.frame.size.width&&range.length==1) {
            if (_inputTextView.contentSize.height < 70 && _inputTextView.contentSize.height > 36.0f) {
                _inputTextview_height = _inputTextView.contentSize.height;
                
            }
            if (_inputTextView.contentSize.height >= 70) {
                _inputTextview_height = 70;
                
            }
        }
        
        else
        {
            NSString *headString=[inputStr substringToIndex:range.location];
            NSString *lastString=[inputStr substringFromIndex:range.location+range.length];
            
            CGSize locationSize=[self TextViewAutoCalculateRectWith:[NSString stringWithFormat:@"%@%@",headString,lastString] FontSize:16.0 MaxSize:CGSizeMake(_inputTextView.frame.size.width, 70)];
            if (locationSize.height<=36.0) {
                _inputTextview_height=36.0;
                
            }
            if (locationSize.height>36.0&&locationSize.height<=55.0) {
                _inputTextview_height= 55.0;
                
            }
            if (locationSize.height>55.0) {
                _inputTextview_height=70.0;
                
            }
            
        }
        
    }
    float animateDuration = 0.5;
    [UIView animateWithDuration:animateDuration
                     animations:^{
//                         CGRect intputTextRect = self.inputTextView.frame;
//                         intputTextRect.size.height = _inputTextview_height;
//                         intputTextRect.origin.y = 7;
//                         [self.inputTextView setFrame:intputTextRect];
//                         self.inputTextview_height =
//                         _inputTextview_height;
//                         
//                         CGRect vRect = self.frame;
//                         vRect.size.height =
//                         50 + (_inputTextview_height - 36);
//                         vRect.origin.y = self.originalPositionY -
//                         (_inputTextview_height - 36
//                          );
//                         self.frame = vRect;
//                         self.currentPositionY = vRect.origin.y;

                     }];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    //判断加上输入的字符，是否超过界限
    NSString *str = [NSString stringWithFormat:@"%@%@", textView.text, text];
    if (str.length > 100)
    {
        textView.text = [str substringToIndex:100];
        BaseViewController *base = [BaseViewController new];
        [base showToast:SPDStringWithKey(@"字数有点多了哦", nil)];
        return NO;
    }else {
        [self.delegate inputTextView:textView shouldChangeTextInRange:range replacementText:text];
        if ([text isEqualToString:@"\n"]) {
            if ([self.delegate respondsToSelector:@selector(didTouchKeyboardReturnKey:text:)]) {
                NSString *_needToSendText = textView.text;
                NSString *_formatString =
                [_needToSendText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                if (0 == [_formatString length]) {
                    //                UIAlertView *notAllowSendSpace = [[UIAlertView alloc]
                    //                        initWithTitle:nil
                    //                              message:SPDStringWithKeyFromTable(@"whiteSpaceMessage", @"RongCloudKit", nil)
                    //                             delegate:self
                    //                    cancelButtonTitle:SPDStringWithKeyFromTable(@"OK", @"RongCloudKit", nil)
                    //                    otherButtonTitles:nil, nil];
                    //                [notAllowSendSpace show];
                } else {
                    [self.delegate didTouchKeyboardReturnKey:self text:[_needToSendText copy]];
                }
            }
            
            return NO;
        }
        
        [self changeInputViewFrame:text textView:textView range:range];
        return YES;
  
    }

    }

-(void)clearInputText{
    self.inputTextView.text = @"";
    
    _inputTextview_height = 36.0f;
    
    CGRect intputTextRect = self.inputTextView.frame;
    intputTextRect.size.height = _inputTextview_height;
    intputTextRect.origin.y = 7;
    [_inputTextView setFrame:intputTextRect];
    
    CGRect vRect = self.frame;
    vRect.size.height = Height_ChatSessionInputBar;
    vRect.origin.y = _originalPositionY;
    _currentPositionY = _originalPositionY;
    
    [self setFrame:vRect];
    
    CGRect rectFrame = self.inputContainerView.frame;
    rectFrame.size.height = vRect.size.height;
    self.inputContainerView.frame = rectFrame;
    
    [self.delegate chatSessionInputBarControlContentSizeChanged:vRect];
}

- (CGSize)TextViewAutoCalculateRectWith:(NSString*)text FontSize:(CGFloat)fontSize MaxSize:(CGSize)maxSize

{
    NSMutableParagraphStyle* paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode=NSLineBreakByWordWrapping;
    if (text) {
        CGSize labelSize = CGSizeZero;
        if (RCDLive_IOS_FSystenVersion < 7.0) {
            labelSize = RCDLive_RC_MULTILINE_TEXTSIZE_LIOS7(text, [UIFont systemFontOfSize:fontSize], maxSize, NSLineBreakByTruncatingTail);
        }
        else
        {
            labelSize = RCDLive_RC_MULTILINE_TEXTSIZE_GEIOS7(text, [UIFont systemFontOfSize:fontSize], maxSize);
        }
        //        NSDictionary* attributes =@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize],NSParagraphStyleAttributeName:paragraphStyle.copy};
        //        CGSize labelSize = [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine attributes:attributes context:nil].size;
        labelSize.height=ceil(labelSize.height);
        labelSize.width=ceil(labelSize.width);
        return labelSize;
    } else {
        return CGSizeZero;
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    // filter the space
}
- (void)textViewDidChange:(UITextView *)textView {
    CGRect line = [textView caretRectForPosition:textView.selectedTextRange.start];
    CGFloat overflow = line.size.height - (textView.contentOffset.y + textView.bounds.size.height -
                                           textView.contentInset.bottom - textView.contentInset.top);
    if (overflow > 0) {
        // We are at the bottom of the visible text and introduced a line feed,
        // scroll down (iOS 7 does not do it)
        // Scroll caret to visible area
        CGPoint offset = textView.contentOffset;
        //offset.y += overflow + 7; // leave 7 pixels margin
        // Cannot animate with setContentOffset:animated: or caret will not appear
        [UIView animateWithDuration:.2
                         animations:^{
                             [textView setContentOffset:offset];
                         }];
    }
    
    
    NSRange range;
    range.location = self.inputTextView.text.length;
    [self changeInputViewFrame:self.inputTextView.text textView:self.inputTextView range:range];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextViewDidChange:)]) {
        [self.delegate inputTextViewDidChange:textView];
    }
}

#pragma mark - AutoSwitchDelegate

- (void)autoSwitch:(AutoSwitch *)autoSwitch turnSwitch:(BOOL) on {
    self.enableWorldMessage = on;
    self.backgroundColor = on ? self.isLiveMode ? SPD_HEXCOlOR(@"F0E9FE"): SPD_HEXCOlOR(@"EEF4FF") : [UIColor whiteColor];
    if (self.isLiveMode) {
    }else{
        self.inputTextView.placeholder = on ? SPDStringWithKey(@"发送世界消息500金币/次", nil) : SPDStringWithKey(@"点击左侧切换到世界消息模式", nil);
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(didChangedAutoSwitchMode:)]) {
        [self.delegate didChangedAutoSwitchMode:on];
    }
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (UIView *)newLine {
    UILabel *line = [UILabel new];
    line.backgroundColor = [UIColor colorWithRed:221 / 255.0 green:221 / 255.0 blue:221 / 255.0 alpha:1];
    return line;
}

- (void)setHideSwitch:(BOOL)hideSwitch {
    _hideSwitch = hideSwitch;
    
    if (_hideSwitch) {
        self.sendGlobalMessageSwitch.hidden = YES;
        self.inputTextView.placeholder = SPDStringWithKey(@"想说点...", nil);
        [self.inputTextView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo(11.5);
        }];
    }
}

- (void)setIsLiveMode:(BOOL)isLiveMode {
    _isLiveMode = isLiveMode;
    [self setHideSwitch:NO];
    self.inputTextView.placeholder = SPDStringWithKey(@"想说点...", nil);
    self.sendGlobalMessageSwitch.coverImageName = @"ic_zhibo_danmu";
    self.sendGlobalMessageSwitch.tintColor = [SPD_HEXCOlOR(@"1A1A1A") colorWithAlphaComponent:0.2];
    self.sendGlobalMessageSwitch.onTintColor = SPD_HEXCOlOR(@"00D4A0");
    self.sendGlobalMessageSwitch.coverImageSize = CGSizeMake(28, 18);
    self.inputTextView.placeholderColor = SPD_HEXCOlOR(@"A4A4A4");
    self.inputTextView.placeholderFont = [UIFont mediumFontOfSize:15];
}

@end
