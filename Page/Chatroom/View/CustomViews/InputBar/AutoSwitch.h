//
//  AutoSwitch.h
//  SimpleDate
//
//  Created by ling Hou on 2019/12/16.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class AutoSwitch;
@protocol AutoSwitchDelegate <NSObject>

- (void)autoSwitch:(AutoSwitch *)autoSwitch turnSwitch:(BOOL) on;

@end

@interface AutoSwitch : UIView

@property (nonatomic, copy) NSString * coverImageName; // 

@property (nonatomic, strong) UIColor *onTintColor; // 打开的背景颜色

@property (nonatomic, strong) UIColor *tintColor;

@property (nonatomic, assign) CGSize coverImageSize;

@property (nonatomic, assign) BOOL on; // 是否是打开状态

@property (nonatomic, weak)id<AutoSwitchDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
