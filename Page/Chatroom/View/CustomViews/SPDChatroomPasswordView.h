//
//  SPDChatroomPasswordView.h
//  SimpleDate
//
//  Created by 李楠 on 2018/11/16.
//  Copyright © 2018 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, PasswordViewType) {
    PasswordViewTypeLock,
    PasswordViewTypeUnlock,
    PasswordViewTypeModify
};

@protocol SPDChatroomPasswordViewDelegate <NSObject>

@optional

- (void)didClickConfirmBtnWithType:(PasswordViewType)type password:(NSString *)password;
- (void)didClickCancelBtnWithType:(PasswordViewType)type;

@end

@interface SPDChatroomPasswordView : UIView

@property (nonatomic, assign) PasswordViewType type;
@property (nonatomic, weak) id<SPDChatroomPasswordViewDelegate> delegate;

- (void)show;

@end
