//
//  SPDChatroomRedPacketCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/9/3.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDChatroomRedPacketCell.h"
#import "ChatroomRedPacketModel.h"

@interface SPDChatroomRedPacketCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *goldLabel;

@end

@implementation SPDChatroomRedPacketCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(ChatroomRedPacketUserModel *)model {
    _model = model;
    
    [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _model.avatar]]];
    self.nickNameLabel.text = _model.nickName;
    self.goldLabel.text = [NSString stringWithFormat:@"%@", _model.gold];
}

@end
