//
//  SPDChatRoomEffectImageView.m
//  SimpleDate
//
//  Created by Monkey on 2017/7/12.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDChatRoomEffectImageView.h"
#import "SPDChatRoomSendPresentMessage.h"
#import "SPDWorldChatMessage.h"
#import "SVGA.h"

@interface SPDChatRoomEffectImageView ()<SVGAPlayerDelegate>

@property (nonatomic,strong)UIView *infoView;

@property (nonatomic,strong)UILabel *sender_userNameLabel;

@property (nonatomic,strong)UILabel *receiver_userNameLabel;

@property (nonatomic,strong)UIImageView *sender_avatarImageView;

@property (nonatomic,strong)UIImageView *receive_avatarImageView;

@property (nonatomic,strong)UIImageView *presentImageView;

@property (nonatomic,strong)UIImageView * contentImageView;

//@property (nonatomic,strong)UIImageView * giveImageView;
@property (nonatomic,strong)UILabel * giveLabel;

@property (nonatomic,strong)NSMutableArray * effectDataArray;

@property (nonatomic,assign)BOOL isAnimating;

@property (nonatomic, strong) UIImageView *numImageView;
@property (nonatomic, strong) SVGAPlayer *player;
@property (nonatomic, strong) SVGAParser *parser;

@end

@implementation SPDChatRoomEffectImageView


-(id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupUIWithFrame:frame];
    }
    return self;
}

-(void)setupUIWithFrame:(CGRect )frame {
    
    self.userInteractionEnabled = NO;
    
    self.contentImageView = [[UIImageView alloc]initWithFrame:frame];
    self.contentImageView.contentMode = UIViewContentModeBottom;
    [self addSubview:self.contentImageView];
    
    self.player = [[SVGAPlayer alloc] initWithFrame:self.bounds];
    self.player.contentMode = UIViewContentModeScaleAspectFit;
    self.player.loops = 1;
    self.player.clearsAfterStop = YES;
    self.player.delegate = self;
    [self addSubview:self.player];
    self.parser = [[SVGAParser alloc] init];
    
    self.infoView = [UIView new];
    [self addSubview:self.infoView];
    [self.infoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(54);
        make.centerX.mas_equalTo(0);
        make.centerY.mas_equalTo(-116);
    }];
    self.infoView.layer.cornerRadius = 27;
    self.infoView.clipsToBounds = YES;
    self.infoView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.55];
    self.infoView.userInteractionEnabled = NO;
    self.infoView.hidden = YES;
    
    
    self.sender_avatarImageView = [[UIImageView alloc]init];
    [self.infoView addSubview:self.sender_avatarImageView];
    [self.sender_avatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(7);
        make.centerY.mas_equalTo(0);
        make.width.and.height.mas_equalTo(32);
    }];
    
    self.sender_userNameLabel = [[UILabel alloc]init];
    self.sender_userNameLabel.numberOfLines = 2;
    self.sender_userNameLabel.textColor = SPD_HEXCOlOR(@"3191f1");
    self.sender_userNameLabel.font = [UIFont systemFontOfSize:16];
    [self.infoView addSubview:self.sender_userNameLabel];
    [self.sender_userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.sender_avatarImageView.mas_trailing).offset(5);
        make.centerY.mas_equalTo(0);
        make.width.mas_lessThanOrEqualTo(90);
    }];
    
    self.giveLabel = [UILabel new];
    self.giveLabel.text = SPDStringWithKey(@"送", nil);
    self.giveLabel.font = [UIFont systemFontOfSize:14];
    self.giveLabel.textColor = SPD_HEXCOlOR(@"fcfdff");
    self.giveLabel.textAlignment = NSTextAlignmentCenter;
    [self.infoView addSubview:self.giveLabel];
    [self.giveLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.sender_userNameLabel.mas_trailing).offset(3);
        make.centerY.mas_equalTo(0);
    }];
    
    self.receive_avatarImageView = [[UIImageView alloc]init];
    [self.infoView addSubview:self.receive_avatarImageView];
    [self.receive_avatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.giveLabel.mas_trailing).offset(8);
        make.height.and.width.mas_equalTo(32);
        make.centerY.mas_equalTo(0);
    }];
    
    self.receiver_userNameLabel = [[UILabel alloc]init];
    self.receiver_userNameLabel.font = [UIFont systemFontOfSize:16];
    self.receiver_userNameLabel.numberOfLines = 0;
    [self.infoView addSubview:self.receiver_userNameLabel];
    [self.receiver_userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.receive_avatarImageView.mas_trailing).offset(5);
        make.centerY.mas_equalTo(0);
        make.width.mas_lessThanOrEqualTo(100);
    }];
    
    self.presentImageView = [[UIImageView alloc]init];
    [self.infoView addSubview:self.presentImageView];
    [self.presentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.receiver_userNameLabel.mas_trailing).offset(5);
        make.centerY.mas_equalTo(0);
        make.width.and.height.mas_equalTo(36);
    }];
    
    self.numImageView = [[UIImageView alloc] init];
    [self addSubview:self.numImageView];
}

- (void)setMessage:(RCMessageContent *)message {
    dispatch_async(dispatch_get_main_queue(), ^{
        _message = message;
        [self.effectDataArray addObject:_message];
        if (!self.isAnimating) {
         [self showGiftImagesEffectsWithDictionary:_message];
        }
    });
}

- (void)showGiftImagesEffectsWithDictionary:(RCMessageContent *)message {
    self.isAnimating = YES;
    self.infoView.hidden = NO;
    [self configUIWithPositionMessage:message];
    [self playAnimationWithGiftId:[message valueForKeyPath:@"gift_id"]];
}

- (void)configUIWithPositionMessage:(RCMessageContent *)message{
    NSString * position ;
    SPDChatRoomSendPresentMessage * msg ;
    SPDWorldChatMessage * worldMsg ;
    NSArray * receiveUser;
    if ([message isKindOfClass:[SPDChatRoomSendPresentMessage class]]) {
         msg = (SPDChatRoomSendPresentMessage *)message;
        position = msg.position;
        receiveUser = msg.receiveUser;
    }else{
        worldMsg = (SPDWorldChatMessage *)message;
        position = @"big_center";
        receiveUser = worldMsg.receiveUser;
    }
    BOOL isBigCenter = [position isEqualToString:@"big_center"];
    [self.infoView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(isBigCenter ? 54 : 42);
    }];
    [self.sender_avatarImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.and.height.mas_equalTo(isBigCenter ? 32:24);
    }];
    [self.receive_avatarImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.and.height.mas_equalTo(isBigCenter ? 32:24);
    }];
    [self.presentImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.and.height.mas_equalTo(isBigCenter ? 37:25);
    }];
    self.sender_userNameLabel.font = [UIFont systemFontOfSize:isBigCenter ? 15:13];
    self.receiver_userNameLabel.font = [UIFont systemFontOfSize:isBigCenter ? 15:13];

    [self.sender_avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:message.senderUserInfo.portraitUri]];
        
    self.sender_userNameLabel.text = message.senderUserInfo.name;

    NSString * send_gender = msg ? msg.send_gender : worldMsg.gender;
    NSString * receive_gender = msg ? msg.receive_gender : worldMsg.receive_gender;
    self.sender_userNameLabel.textColor = [send_gender isEqualToString:@"male"] ? SPD_HEXCOlOR(@"3c91f1"):SPD_HEXCOlOR(@"fe69a1");
    [self.sender_userNameLabel sizeToFit];
    
    [self.receive_avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:msg?msg.receive_avatar:worldMsg.receive_avatar]];
    NSString * receiverName = msg ? msg.receive_nickname : worldMsg.receive_nickname;
    NSMutableString * receiver_name = [receiverName mutableCopy];
    self.receiver_userNameLabel.text= receiver_name;
    [self.receiver_userNameLabel sizeToFit];

    self.receiver_userNameLabel.textColor = [receive_gender isEqualToString:@"male"] ?SPD_HEXCOlOR(@"3c91f1"):SPD_HEXCOlOR(@"fe69a1");
        
    [self.presentImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",msg ? msg.present_url : worldMsg.present_url]]];
    [self layoutIfNeeded];
    
    CGFloat content_x = [position isEqualToString:@"big_center"] ? 0 : (self.frame.size.width - 99) / 2;
    CGFloat content_y = [position isEqualToString:@"big_center"] ? 0 : CGRectGetMaxY(self.infoView.frame)+ 30;
    CGFloat content_w = [position isEqualToString:@"big_center"] ? self.frame.size.width : 99 ;
    CGFloat content_h = [position isEqualToString:@"big_center"] ? self.frame.size.height : 103 ;
    CGFloat num_x = [position isEqualToString:@"big_center"] ? kScreenW - 20 - 115 : content_x + content_w + 6.5;
    CGFloat num_y = [position isEqualToString:@"big_center"] ? CGRectGetMaxY(self.infoView.frame) + 6 : content_y - 16;
    CGFloat num_w = [position isEqualToString:@"big_center"] ? 115: 65;
    CGFloat num_h = [position isEqualToString:@"big_center"] ? 115: 65;;
    CGRect contentFrame = self.contentImageView.frame;
    contentFrame.size.width = content_w;
    contentFrame.size.height = content_h;
    contentFrame.origin.x = content_x;
    contentFrame.origin.y = content_y;
    self.contentImageView.frame = contentFrame;
    self.player.frame = contentFrame;
    self.numImageView.frame = CGRectMake(num_x, num_y, num_w, num_h);
    NSString * num = msg ? msg.num : worldMsg.num;
    self.numImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"gift_num_b_%@", num]];
    
    // 判断群送礼物
    NSString * receive_user_type = msg? msg.receive_user_type : worldMsg.receive_user_type;
    NSString * curSel = msg ? msg.curSel : worldMsg.curSel;
    NSString * sumSel = msg ? msg.sumSel : worldMsg.sumSel;
    
    if (![receive_user_type isEqualToString:@"normal"] && receive_user_type.notEmpty) {
        NSArray * receiveIds = [receiveUser valueForKeyPath:@"@unionOfObjects._id"];
        if (([receiveIds containsObject:[SPDApiUser currentUser].userId] && ![message.senderUserInfo.userId isEqualToString:[SPDApiUser currentUser].userId]) && !([receive_user_type isEqualToString:@"all"] && [curSel isEqualToString:sumSel])) {
            
            self.receiver_userNameLabel.text = [SPDApiUser currentUser].nickName;
            self.receiver_userNameLabel.textColor = [[SPDApiUser currentUser].gender isEqualToString:@"male"]? SPD_HEXCOlOR(@"3c91f1"):SPD_HEXCOlOR(@"fe69a1") ;
            [self.receive_avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,[SPDApiUser currentUser].avatar]]];
            
        }else{
            self.receiver_userNameLabel.textColor = [UIColor whiteColor];
            if ([receive_user_type isEqualToString:@"all"]) {
                self.receiver_userNameLabel.text = [curSel isEqualToString:sumSel ] ? SPDStringWithKey(@"房间内每个人", nil): [NSString stringWithFormat:SPDLocalizedString(@"房间内%@人"),curSel];
            }else if ([receive_user_type isEqualToString:@"allfemale"]){
                self.receiver_userNameLabel.text = [curSel isEqualToString:sumSel ] ? SPDStringWithKey(@"房间内每个女用户", nil): [NSString stringWithFormat:SPDLocalizedString(@"房间内%@个女用户"),curSel];
            }else if ([receive_user_type isEqualToString:@"allmale"]){
                self.receiver_userNameLabel.text = [curSel isEqualToString:sumSel ] ? SPDStringWithKey(@"房间内每个男用户", nil): [NSString stringWithFormat:SPDLocalizedString(@"房间内%@个男用户"),curSel];
            }else if ([receive_user_type isEqualToString:@"mic"]){
                self.receiver_userNameLabel.text = [curSel isEqualToString:sumSel ] ? SPDStringWithKey(@"麦上每个人", nil): [NSString stringWithFormat:SPDStringWithKey(@"麦上%@人", nil),curSel];
            }else if ([receive_user_type isEqualToString:@"micfemale"]){
                self.receiver_userNameLabel.text = [curSel isEqualToString:sumSel ] ? SPDStringWithKey(@"麦上每个女用户", nil): [NSString stringWithFormat:SPDLocalizedString(@"麦上%@个女用户"),curSel];
            }else if ([receive_user_type isEqualToString:@"micmale"]){
                self.receiver_userNameLabel.text = [curSel isEqualToString:sumSel] ? SPDStringWithKey(@"麦上每个男用户", nil): [NSString stringWithFormat:SPDLocalizedString(@"麦上%@个男用户"),curSel];
            }
        }
    }
    
    //
    [self.infoView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(54);
        make.centerX.mas_equalTo(0);
        make.centerY.mas_equalTo(-116);
        make.trailing.equalTo(self.presentImageView.mas_trailing).offset(5);
    }];
    [self layoutIfNeeded];
}

- (void)showNumAnimation {
    CAKeyframeAnimation *animation;
    animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = 0.5;
    animation.fillMode = kCAFillModeForwards;
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(2.3, 2.3, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.05, 1.05, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    animation.keyTimes = @[@0.0, @0.7, @0.8, @0.9];
    self.numImageView.hidden = NO;
    [self.numImageView.layer addAnimation:animation forKey:nil];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.sender_avatarImageView.layer.cornerRadius = CGRectGetWidth(self.sender_avatarImageView.frame) / 2;
    self.sender_avatarImageView.clipsToBounds = YES;
    
    self.receive_avatarImageView.layer.cornerRadius = CGRectGetWidth(self.receive_avatarImageView.frame) / 2;
    self.receive_avatarImageView.clipsToBounds = YES;
}

#pragma mark - SVGA动效

- (void)playAnimationWithGiftId:(NSString *)giftId {
    [self.parser parseWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://%@famy.17jianyue.cn/svga/%@.svga", DEV ? @"dev-" : @"", giftId]]
              completionBlock:^(SVGAVideoEntity * _Nullable videoItem) {
                  if (videoItem != nil) {
                      self.player.videoItem = videoItem;
                      [self.player startAnimation];
                      [self showNumAnimation];
                  } else {
                      [self finishedAnimation];
                  }
              } failureBlock:^(NSError * _Nullable error) {
                  [self finishedAnimation];
              }];
}

- (void)finishedAnimation {
    self.infoView.hidden = YES;
    self.numImageView.hidden = YES;
    if (self.effectDataArray.count > 0) {
        [self.effectDataArray removeObjectAtIndex:0];
        if (self.effectDataArray.count > 0) {
            [self showGiftImagesEffectsWithDictionary:self.effectDataArray.firstObject];
        } else {
            self.isAnimating = NO;
        }
    } else {
        self.isAnimating = NO;
    }
}

#pragma mark - SVGAPlayerDelegate

- (void)svgaPlayerDidFinishedAnimation:(SVGAPlayer *)player {
    [self finishedAnimation];
}

#pragma mark - Getters

- (NSMutableArray *)effectDataArray {
    if (!_effectDataArray) {
        _effectDataArray = [NSMutableArray array];
    }
    return _effectDataArray;
}

@end
