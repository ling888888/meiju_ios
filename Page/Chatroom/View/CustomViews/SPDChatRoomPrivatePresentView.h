//
//  SPDChatRoomPrivatePresentView.h
//  SimpleDate
//
//  Created by Monkey on 2017/6/6.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPDChatRoomPrivatePresentView : UIView

@property (nonatomic,copy)NSDictionary *dict;

@end
