//
//  SPDChatroomRedPacketCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/9/3.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChatroomRedPacketUserModel;

@interface SPDChatroomRedPacketCell : UITableViewCell

@property (nonatomic, strong) ChatroomRedPacketUserModel *model;

@end
