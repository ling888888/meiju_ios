//
//  ChatroomMoreFunctionView.h
//  SimpleDate
//
//  Created by ling Hou on 2019/12/25.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class ChatroomMoreFunctionView;
@protocol ChatroomMoreFunctionViewDelegate <NSObject>

@optional
- (void)chatroomMoreFunctionView:(ChatroomMoreFunctionView *)view didClickedItem:(NSString *)type;

@end

@interface ChatroomMoreFunctionView : UIView

@property (nonatomic, assign) NSInteger positionLevel; // 权限值：0 游客，25 家族成员，75 副族长，150 族长
@property (nonatomic, weak) id<ChatroomMoreFunctionViewDelegate> delegate;
- (void)show ;

@end

NS_ASSUME_NONNULL_END
