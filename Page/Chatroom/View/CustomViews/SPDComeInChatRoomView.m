//
//  SPDComeInChatRoomView.m
//  SimpleDate
//
//  Created by Monkey on 2017/5/18.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDComeInChatRoomView.h"

#define kWidth 110
#define kHeight 60

@implementation SPDComeInChatRoomView

- (id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self configUI];
    }
    return self;
}

-(void)configUI {
    
    self.backgroundColor = SPD_HEXCOlOR(@"fe69a1");
    if (self.avatarImageView) {
        [self.avatarImageView removeFromSuperview];
        self.avatarImageView = nil;
    }
    if (self.nicknameLabel) {
        [self.nicknameLabel removeFromSuperview];
        self.nicknameLabel = nil;
    }
    
    [self configComeInChatRoomView];
}

-(void)configComeInChatRoomView {
    
    _avatarImageView = [[UIImageView alloc]initWithFrame:CGRectZero];
    [_avatarImageView setFrame:CGRectMake(5, 2.5, 20, 20)];
    _avatarImageView.layer.cornerRadius = 10;
    _avatarImageView.clipsToBounds =YES;
    [self addSubview:_avatarImageView];
    
    _nicknameLabel = [[UILabel alloc]init];
    [_nicknameLabel setTextColor:[UIColor whiteColor]];
    [_nicknameLabel setFrame:CGRectMake(5+CGRectGetMaxX(self.avatarImageView.frame), 2.5, self.bounds.size.width - (5+CGRectGetMaxX(self.avatarImageView.frame)) - 5, 20)];
    _nicknameLabel.font = [UIFont systemFontOfSize:10];
    [self addSubview:_nicknameLabel];
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(11, 25, 22, 35);
    label.text = @"骑着";
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont systemFontOfSize:10];
    [self addSubview:label];
    
    self.vehicleImageView = [[UIImageView alloc] init];
    self.vehicleImageView.frame = CGRectMake(11 + CGRectGetMaxX(label.frame), 25, 55, 35);
    self.vehicleImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:_vehicleImageView];
}

-(void)configViewWithNickName:(NSString *)nick_name andAvatar:(NSString *)avatar andVehicle:(NSString *)vehicle {
    [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:avatar]];
    if (_isHasVehicle) {
        [self.vehicleImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, [NSString stringWithFormat:@"%@?imageView2/2/w/110", vehicle]]]];
    }
    NSString * str = @"";
    if (nick_name.length >6) {
        str = [nick_name substringToIndex:5];
    }else{
        str = nick_name;
    }
    NSString *resultStr = [NSString stringWithFormat:@"%@%@",str,SPDStringWithKey(@"来了", nil)];
    _nicknameLabel.text = resultStr;
}

-(CGSize)getViewTextSize:(NSString *)nick_name {
    NSString * str = @"";
    if (nick_name.length >6) {
        str = [nick_name substringToIndex:5];
    }else{
        str = nick_name;
    }
    NSDictionary *attrs = @{NSFontAttributeName : [UIFont boldSystemFontOfSize:10]};
    NSString *resultStr = [NSString stringWithFormat:@"%@%@",str,SPDStringWithKey(@"来了", nil)];
    CGSize size=[resultStr sizeWithAttributes:attrs];
    return size;
}

-(void)startAnimationViewActionWithText: (NSString *)text andGender:(NSString *)gender andInputbarFrame:(CGRect) inputbarFrame{
    
    if (self.nicknameLabel.text != nil) {
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            CGRect comeinRect = self.frame;
            if (_isHasVehicle) {
                comeinRect.size.width = kWidth;
                comeinRect.size.height = kHeight;
            } else {
                CGSize size = [self getViewTextSize:text];
                comeinRect.size.width = size.width+30+5;
                comeinRect.size.height = 25;
            }
            if ([gender isEqualToString:@"female"]) {
                self.backgroundColor = SPD_HEXCOlOR(@"#fe69a1");
            }else{
                self.backgroundColor = SPD_HEXCOlOR(@"#3c91f1");
            }
            comeinRect.origin.x = kScreenW - comeinRect.size.width-8;
            comeinRect.origin.y = inputbarFrame.origin.y -comeinRect.size.height;
            self.frame = comeinRect;
            
            [_nicknameLabel setFrame:CGRectMake(5+CGRectGetMaxX(self.avatarImageView.frame), 2.5, self.bounds.size.width - (5+CGRectGetMaxX(self.avatarImageView.frame)) - 5, 20)];
        } completion:^(BOOL finished) {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self stopAnimationViewAction];
            });
        }];
    }
   
    
}

-(void)stopAnimationViewAction {
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        CGRect myframe = self.frame;
        myframe.origin.x = kScreenW ;
        self.frame = myframe;
        
    } completion:^(BOOL finished) {
        
    }];
}


@end
