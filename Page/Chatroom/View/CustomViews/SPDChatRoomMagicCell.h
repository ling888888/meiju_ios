//
//  SPDChatRoomMagicCell.h
//  SimpleDate
//
//  Created by 李楠 on 2017/9/27.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SPDMagicModel;

@interface SPDChatRoomMagicCell : UICollectionViewCell

@property (nonatomic, strong) SPDMagicModel *model;
@property (nonatomic, assign) NSInteger MagicLevel;

@end
