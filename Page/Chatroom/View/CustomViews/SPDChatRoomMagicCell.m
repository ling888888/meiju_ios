//
//  SPDChatRoomMagicCell.m
//  SimpleDate
//
//  Created by 李楠 on 2017/9/27.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDChatRoomMagicCell.h"
#import "SPDMagicModel.h"

@interface SPDChatRoomMagicCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;

@property (nonatomic, strong) NSMutableArray *levelImages;

@end

@implementation SPDChatRoomMagicCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(SPDMagicModel *)model {
    _model = model;
    
    [self.imageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, model.cover]]];
    self.imageView.layer.borderColor = [UIColor colorWithHexString:model.color].CGColor;
    self.countLabel.text = [NSString stringWithFormat:@"%@", model.num];
    self.MagicLevel = [model.magic_level integerValue];
    self.nameLabel.text = SPDStringWithKey(model.name, nil);
}

- (void)setMagicLevel:(NSInteger)magicLevel {
    if (_MagicLevel != magicLevel) {
        _MagicLevel = magicLevel;
        
        self.levelLabel.text = [NSString stringWithFormat:@"LV.%ld", magicLevel];
//        for (UIImageView *imageView in self.levelImages) {
//            [imageView removeFromSuperview];
//        }
//        [self.levelImages removeAllObjects];
//
//        NSInteger stage = (_MagicLevel - 1) / 5;
//        NSInteger count = (_MagicLevel - 1) % 5 + 1;
//        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"MagicLevel%ld", stage]];
//        CGFloat leadeing = (162 / 2 - count * 9 - (count - 1) * 4) / 2;
//        for (NSInteger i = 0; i < count; i++) {
//            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
//            [self.contentView addSubview:imageView];
//            [self.levelImages addObject:imageView];
//
//            [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.top.equalTo(self.imageView.mas_bottom).offset(9);
//                make.leading.mas_equalTo(leadeing);
//                make.width.and.height.mas_equalTo(9);
//            }];
//
//            leadeing = leadeing + 9 + 4;
//        }
    }
}

- (NSMutableArray *)levelImages {
    if (!_levelImages) {
        _levelImages = [NSMutableArray array];
    }
    return _levelImages;
}

@end
