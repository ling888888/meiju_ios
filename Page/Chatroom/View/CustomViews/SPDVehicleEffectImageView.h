//
//  SPDVehicleEffectImageView.h
//  SimpleDate
//
//  Created by 李楠 on 2017/11/13.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SPDJoinChatRoomMessage,LiveJoinMessage;

@interface SPDVehicleEffectImageView : UIImageView

@property (nonatomic, strong) SPDJoinChatRoomMessage *message;
@property (nonatomic, strong) LiveJoinMessage * liveJoinMessage;

+ (NSMutableArray *)getAnimationImagesWithVehicleId:(NSString *)vehicleId isCGImage:(BOOL)isCGImage;
- (void)startAnimatingWithVehicleId:(NSString *)vehicleId;

@end
