//
//  SPDChatRoomMagicEffectView.h
//  SimpleDate
//
//  Created by Monkey on 2017/9/27.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPDCRSendMagicMessage.h"

//展示施加魔法的动画效果
@interface SPDChatRoomMagicEffectView : UIView

@property (nonatomic,copy)NSDictionary * dataDict;

@property (nonatomic,strong)SPDCRSendMagicMessage *magicMsg;

@end
