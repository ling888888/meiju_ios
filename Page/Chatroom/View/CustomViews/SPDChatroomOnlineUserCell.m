//
//  SPDChatroomOnlineUserCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/12/27.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "SPDChatroomOnlineUserCell.h"
#import "SPDVoiceLiveUserModel.h"

@interface SPDChatroomOnlineUserCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *nobleAvatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *micImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *genderAgeBtn;
@property (weak, nonatomic) IBOutlet UIButton *levelBtn;
@property (weak, nonatomic) IBOutlet UIImageView *specialNumImageView;
@property (weak, nonatomic) IBOutlet UIImageView *vehicleImageView;

@property(nonatomic, strong) LY_PortraitView *portraitView;

@end

@implementation SPDChatroomOnlineUserCell

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
    }
    return _portraitView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
//    self.nobleAvatarImageView.hidden = YES;
    
    [self.nobleAvatarImageView addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(-5);
        make.leading.mas_equalTo(-5);
        make.bottom.mas_equalTo(5);
        make.trailing.mas_equalTo(5);
    }];
}

- (void)setModel:(SPDVoiceLiveUserModel *)model {
    _model = model;
    
    self.portraitView.portrait = model.portrait;
    self.micImageView.hidden = !_model.micIndex;
    self.nickNameLabel.text = _model.nick_name;
    if ([_model.gender isEqualToString:@"female"]) {
        [self.genderAgeBtn setBackgroundColor:[UIColor colorWithHexString:@"ff7eca"]];
        [self.genderAgeBtn setImage:[UIImage imageNamed:@"chatroomuser_female"] forState:UIControlStateNormal];
    } else if ([_model.gender isEqualToString:@"male"]) {
        [self.genderAgeBtn setBackgroundColor:[UIColor colorWithHexString:@"5db4fd"]];
        [self.genderAgeBtn setImage:[UIImage imageNamed:@"chatroomuser_male"] forState:UIControlStateNormal];
    }
    [self.genderAgeBtn setTitle:_model.age.stringValue forState:UIControlStateNormal];
    [SPDCommonTool configDataForLevelBtnWithLevel:[_model.level intValue] andUIButton:self.levelBtn];
    self.specialNumImageView.hidden = [SPDCommonTool isEmpty:_model.specialNum];
    self.vehicleImageView.hidden = [SPDCommonTool isEmpty:_model.driveVehicle];
    [self.vehicleImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _model.driveVehicle]]];
}

@end
