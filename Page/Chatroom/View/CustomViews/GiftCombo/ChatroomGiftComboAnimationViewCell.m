//
//  ChatroomGiftComboAnimationViewCell.m
//  SimpleDate
//
//  Created by 李楠 on 2020/3/2.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "ChatroomGiftComboAnimationViewCell.h"
#import "ChatroomGiftComboMessage.h"
#import <SVGAPlayer/SVGA.h>

@interface ChatroomGiftComboAnimationViewCell ()

@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *senderAvatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *senderNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *receiverAvatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *receiverNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *giftImageView;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet SVGAPlayer *svgaPlayer;

@property (nonatomic, strong) CAGradientLayer *gradientLayer;

@end

@implementation ChatroomGiftComboAnimationViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [[SVGAParser new] parseWithNamed:@"chatroom_gift_combo" inBundle:[NSBundle mainBundle] completionBlock:^(SVGAVideoEntity * _Nonnull videoItem) {
        self.svgaPlayer.videoItem = videoItem;
    } failureBlock:^(NSError * _Nonnull error) {
        
    }];
}

- (void)layoutSubviews {
    [super layoutSubviews];

    self.gradientLayer.frame = self.backgroundView.bounds;
}

#pragma mark - Public methods

- (void)updateCountWithMessage:(ChatroomGiftComboMessage *)message {
    self.countLabel.text = [NSString stringWithFormat:@"x%@", message.num];
    [self changeBackgroundColorWithCount:message.num.integerValue];
    
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = 0.5;
    animation.fillMode = kCAFillModeForwards;
    animation.values = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(2.0, 2.0, 1.0)],
                         [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.5, 0.5, 1.0)],
                         [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.2, 1.2, 1.0)],
                         [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.keyTimes = @[@0.0, @0.6, @0.9, @1.0];
    [self.countLabel.layer addAnimation:animation forKey:nil];
    [self.svgaPlayer startAnimation];
}

#pragma mark - Private methods

- (void)changeBackgroundColorWithCount:(NSInteger)count {
    if (count >= 297) {
        self.gradientLayer.colors = @[(__bridge id)[UIColor colorWithRed:110 / 255.0 green:86 / 255.0 blue:255 / 255.0 alpha:0.94].CGColor, (__bridge id)[UIColor colorWithRed:85 / 255.0 green:145 / 255.0 blue:255 / 255.0 alpha:0.94].CGColor];
    } else if (count >= 197) {
        self.gradientLayer.colors = @[(__bridge id)[UIColor colorWithRed:152 / 255.0 green:83 / 255.0 blue:255 / 255.0 alpha:0.94].CGColor, (__bridge id)[UIColor colorWithRed:221 / 255.0 green:143 / 255.0 blue:255 / 255.0 alpha:0.94].CGColor];
    } else if (count >= 97) {
        self.gradientLayer.colors = @[(__bridge id)[UIColor colorWithRed:255 / 255.0 green:181 / 255.0 blue:87 / 255.0 alpha:0.94].CGColor, (__bridge id)[UIColor colorWithRed:255 / 255.0 green:215 / 255.0 blue:103 / 255.0 alpha:0.94].CGColor];
    } else if (count >= 27) {
        self.gradientLayer.colors = @[(__bridge id)[UIColor colorWithRed:255 / 255.0 green:107 / 255.0 blue:147 / 255.0 alpha:0.94].CGColor, (__bridge id)[UIColor colorWithRed:255 / 255.0 green:149 / 255.0 blue:119 / 255.0 alpha:0.94].CGColor];
    } else {
        self.gradientLayer.colors = @[(__bridge id)[UIColor colorWithRed:26 / 255.0 green:26 / 255.0 blue:26 / 255.0 alpha:0.8].CGColor, (__bridge id)[UIColor colorWithRed:26 / 255.0 green:26 / 255.0 blue:26 / 255.0 alpha:0.8].CGColor];
    }
}

#pragma mark - Setters & Getters

- (void)setMessage:(ChatroomGiftComboMessage *)message {
    _message = message;
    
    [self.senderAvatarImageView fp_setImageWithURLString:message.senderAvatar];
    self.senderNameLabel.text = message.senderName;
    [self.giftImageView fp_setImageWithURLString:message.giftImg];
    self.countLabel.text = [NSString stringWithFormat:@"x%@", message.num];
    [self changeBackgroundColorWithCount:message.num.integerValue];
    
    if (!_message.receive_user_type.notEmpty || [_message.receive_user_type isEqualToString:@"normal"]) {
        [self.receiverAvatarImageView fp_setImageWithURLString:message.receiveAvatar];
        self.receiverNameLabel.text = message.receiveName;
    } else {
        NSArray *receiveUserIds = [_message.receiveUserId componentsSeparatedByString:@","];
        BOOL isSendAll = [_message.curSel isEqualToString:_message.sumSel];
        if (([receiveUserIds containsObject:[SPDApiUser currentUser].userId] && ![message.senderUserId isEqualToString:[SPDApiUser currentUser].userId]) && !([_message.receive_user_type isEqualToString:@"all"] && isSendAll)) {
            [self.receiverAvatarImageView fp_setImageWithURLString:[SPDApiUser currentUser].avatar];
            self.receiverNameLabel.text = [SPDApiUser currentUser].nickName;
        } else {
            if ([_message.receive_user_type isEqualToString:@"all"]) {
                [self.receiverAvatarImageView fp_setImageWithURLString:@"room.png"];
                self.receiverNameLabel.text = isSendAll ? SPDLocalizedString(@"房间内每个人") : [NSString stringWithFormat:SPDLocalizedString(@"房间内%@人"), _message.curSel];
            } else if ([_message.receive_user_type isEqualToString:@"allfemale"]) {
                [self.receiverAvatarImageView fp_setImageWithURLString:@"room.png"];
                self.receiverNameLabel.text = isSendAll ? SPDLocalizedString(@"房间内每个女用户") : [NSString stringWithFormat:SPDLocalizedString(@"房间内%@个女用户"), _message.curSel];
            } else if ([_message.receive_user_type isEqualToString:@"allmale"]) {
                [self.receiverAvatarImageView fp_setImageWithURLString:@"room.png"];
                self.receiverNameLabel.text = isSendAll ? SPDLocalizedString(@"房间内每个男用户") : [NSString stringWithFormat:SPDLocalizedString(@"房间内%@个男用户"), _message.curSel];
            } else if ([_message.receive_user_type isEqualToString:@"mic"]) {
                [self.receiverAvatarImageView fp_setImageWithURLString:@"mic.png"];
                self.receiverNameLabel.text = isSendAll ? SPDLocalizedString(@"麦上每个人") : [NSString stringWithFormat:SPDLocalizedString(@"麦上%@人"), _message.curSel];
            } else if ([_message.receive_user_type isEqualToString:@"micfemale"]) {
                [self.receiverAvatarImageView fp_setImageWithURLString:@"mic.png"];
                self.receiverNameLabel.text = isSendAll ? SPDLocalizedString(@"麦上每个女用户") : [NSString stringWithFormat:SPDLocalizedString(@"麦上%@个女用户"), _message.curSel];
            } else if ([_message.receive_user_type isEqualToString:@"micmale"]) {
                [self.receiverAvatarImageView fp_setImageWithURLString:@"mic.png"];
                self.receiverNameLabel.text = isSendAll ? SPDLocalizedString(@"麦上每个男用户") : [NSString stringWithFormat:SPDLocalizedString(@"麦上%@个男用户"), _message.curSel];
            }
        }
    }
}

- (CAGradientLayer *)gradientLayer {
    if (!_gradientLayer) {
        _gradientLayer = [CAGradientLayer layer];
        _gradientLayer.cornerRadius = 20;
        _gradientLayer.startPoint = CGPointMake(0, 0);
        _gradientLayer.endPoint = CGPointMake(1, 1);
        [self.backgroundView.layer insertSublayer:_gradientLayer atIndex:0];
    }
    return _gradientLayer;
}

@end
