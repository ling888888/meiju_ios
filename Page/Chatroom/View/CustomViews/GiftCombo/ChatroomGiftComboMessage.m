//
//  ChatroomGiftComboMessage.m
//  SimpleDate
//
//  Created by 李楠 on 2020/3/13.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "ChatroomGiftComboMessage.h"
#import "NSObject+Coding.h"
#import "SPDVoiceLiveUserModel.h"

@implementation ChatroomGiftComboMessage

+ (NSString *)getObjectName {
    return @"SPD:ChatRoomComboGiftMsg";
}

+ (RCMessagePersistent)persistentFlag {
    return MessagePersistent_ISCOUNTED;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        [self hl_decodeWithDeCoder:aDecoder];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [self hl_encodeWithCoder:aCoder];
}

- (NSData *)encode {
    NSMutableDictionary *dataDic = [NSMutableDictionary new];
    [dataDic setValue:self.clanId forKey:@"clanId"];
    [dataDic setValue:self.senderUserId forKey:@"senderUserId"];
    [dataDic setValue:self.senderAvatar forKey:@"senderAvatar"];
    [dataDic setValue:self.senderName forKey:@"senderName"];
    [dataDic setValue:self.receiveUserId forKey:@"receiveUserId"];
    [dataDic setValue:self.receiveAvatar forKey:@"receiveAvatar"];
    [dataDic setValue:self.receiveName forKey:@"receiveName"];
    [dataDic setValue:self.giftId forKey:@"giftId"];
    [dataDic setValue:self.giftImg forKey:@"giftImg"];
    [dataDic setValue:self.num forKey:@"num"];
    [dataDic setValue:self.curSel forKey:@"curSel"];
    [dataDic setValue:self.sumSel forKey:@"sumSel"];
    [dataDic setValue:self.receive_user_type forKey:@"receive_user_type"];
    
    if (self.senderUserInfo) {
        NSMutableDictionary *userInfoDic = [NSMutableDictionary new];
        if (self.senderUserInfo.name) {
            [userInfoDic setObject:self.senderUserInfo.name
                 forKeyedSubscript:@"name"];
        }
        if (self.senderUserInfo.portraitUri) {
            [userInfoDic setObject:self.senderUserInfo.portraitUri
                 forKeyedSubscript:@"icon"];
        }
        if (self.senderUserInfo.userId) {
            [userInfoDic setObject:self.senderUserInfo.userId
                 forKeyedSubscript:@"id"];
        }
        [dataDic setValue:userInfoDic forKey:@"user"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDic
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                   options:kNilOptions
                                                                     error:&error];
        
        if (dictionary) {
            [self setValuesForKeysWithDictionary:dictionary];
            [self decodeUserInfo:dictionary[@"user"]];
        }
    }
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    
}

@end
