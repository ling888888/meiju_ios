//
//  ChatroomGiftComboAnimationViewCell.h
//  SimpleDate
//
//  Created by 李楠 on 2020/3/2.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class ChatroomGiftComboMessage;

@interface ChatroomGiftComboAnimationViewCell : UIView

@property (nonatomic, strong) ChatroomGiftComboMessage *message;

- (void)updateCountWithMessage:(ChatroomGiftComboMessage *)message;

@end

NS_ASSUME_NONNULL_END
