//
//  ChatroomGiftComboMessage.h
//  SimpleDate
//
//  Created by 李楠 on 2020/3/13.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatroomGiftComboMessage : RCMessageContent

@property (nonatomic, copy) NSString *clanId;
@property (nonatomic, copy) NSString *senderUserId;
@property (nonatomic, copy) NSString *senderAvatar;
@property (nonatomic, copy) NSString *senderName;
@property (nonatomic, copy) NSString *receiveUserId;
@property (nonatomic, copy) NSString *receiveAvatar;
@property (nonatomic, copy) NSString *receiveName;
@property (nonatomic, copy) NSString *giftId;
@property (nonatomic, copy) NSString *giftImg;
@property (nonatomic, copy) NSNumber *num;

@property (nonatomic, copy) NSString *curSel; // 选中的人数
@property (nonatomic, copy) NSString *sumSel; // 总数
@property (nonatomic, copy) NSString *receive_user_type; // 接收方类型

@end

NS_ASSUME_NONNULL_END
