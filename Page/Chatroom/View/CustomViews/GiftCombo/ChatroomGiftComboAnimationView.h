//
//  ChatroomGiftComboAnimationView.h
//  SimpleDate
//
//  Created by 李楠 on 2020/3/10.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class ChatroomGiftComboMessage;

@interface ChatroomGiftComboAnimationView : UIView

- (void)showAnimationWithMessage:(ChatroomGiftComboMessage *)message;

@end

NS_ASSUME_NONNULL_END
