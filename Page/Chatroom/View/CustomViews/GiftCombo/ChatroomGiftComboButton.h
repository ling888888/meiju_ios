//
//  ChatroomGiftComboButton.h
//  SimpleDate
//
//  Created by 李楠 on 2020/3/11.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ChatroomGiftComboButtonDelegate <NSObject>

@optional

- (void)didClickGiftComboButton;
- (void)giftComboCountdownDidEnd;
- (void)giftComboSendDidEnd;

@end

@interface ChatroomGiftComboButton : UIView

@property (nonatomic, weak) id<ChatroomGiftComboButtonDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
