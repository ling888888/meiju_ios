//
//  ChatroomGiftComboButton.m
//  SimpleDate
//
//  Created by 李楠 on 2020/3/11.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "ChatroomGiftComboButton.h"

@interface ChatroomGiftComboButton ()<CAAnimationDelegate>

@property (weak, nonatomic) IBOutlet UIView *comboButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *comboButtonBottom;
@property (weak, nonatomic) IBOutlet UIImageView *comboImageView;

@property (nonatomic, assign) NSInteger comboCount;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) CAShapeLayer *shapeLayer;

@end

@implementation ChatroomGiftComboButton

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = CGRectMake(0, kScreenH - (30 + 17 + 58 + IphoneX_Bottom), kScreenW, 30 + 17 + 58 + IphoneX_Bottom);
    gradientLayer.startPoint = CGPointMake(0.5, 1);
    gradientLayer.endPoint = CGPointMake(0.5, 0);
    gradientLayer.colors = @[(__bridge id)[UIColor colorWithWhite:0 alpha:0.6].CGColor,
                            (__bridge id)[UIColor clearColor].CGColor];
    [self.layer addSublayer:gradientLayer];
    
    self.comboButtonBottom.constant = 17 + 58 + IphoneX_Bottom;
    NSString *currentLanguage = [SPDCommonTool getFamyLanguage];
    if ([currentLanguage isEqualToString:@"zh-Hans"]) {
        self.comboImageView.image = [UIImage imageNamed:@"chatroom_gift_combo_zh"];
    } else if ([currentLanguage isEqualToString:@"ar"]) {
        self.comboImageView.image = [UIImage imageNamed:@"chatroom_gift_combo_ar"];
    } else {
        self.comboImageView.image = [UIImage imageNamed:@"chatroom_gift_combo_en"];
    }
    
    [self shapeLayerAddAnimation];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *view = [super hitTest:point withEvent:event];
    if (view == self) {
        if (!self.timer) {
            [self removeFromSuperview];
        }
        return nil;
    } else {
        return view;
    }
}

- (void)removeFromSuperview {
    if (self.comboCount > 0 && [self.delegate respondsToSelector:@selector(giftComboSendDidEnd)]) {
        [self.delegate giftComboSendDidEnd];
    }
    
    [self releaseTimer];
    [self.shapeLayer removeAnimationForKey:@"strokeStart"];
    [super removeFromSuperview];
}

#pragma mark - Event response

- (IBAction)tapComboButton:(UITapGestureRecognizer *)sender {
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    animation.duration = 0.1;
    animation.toValue = @(0.5);
    animation.autoreverses = YES;
    [self.comboButton.layer addAnimation:animation forKey:nil];
    [self shapeLayerAddAnimation];
    
    if ([self.delegate respondsToSelector:@selector(didClickGiftComboButton)]) {
        [self.delegate didClickGiftComboButton];
    }
    self.comboCount++;
    //[MobClick event:@"FM7_19_10"];
}

- (IBAction)longPressComboButton:(UILongPressGestureRecognizer *)sender {
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
            [self initTimer];
            break;
        case UIGestureRecognizerStateEnded:
            [self releaseTimer];
            break;
        default:
            break;
    }
}

- (void)applicationDidEnterBackground:(NSNotification *)notification {
    [self removeFromSuperview];
}

#pragma mark - Private methods

- (void)shapeLayerAddAnimation {
    [self.shapeLayer removeAnimationForKey:@"strokeStart"];

    CABasicAnimation *shapeLayerAnimation = [CABasicAnimation animationWithKeyPath:@"strokeStart"];
    shapeLayerAnimation.duration = 3;
    shapeLayerAnimation.toValue = @(1);
    shapeLayerAnimation.fillMode = kCAFillModeForwards;
    shapeLayerAnimation.removedOnCompletion = NO;
    shapeLayerAnimation.delegate = self;
    [self.shapeLayer addAnimation:shapeLayerAnimation forKey:@"strokeStart"];
}

- (void)initTimer {
    if (!self.timer) {
        self.timer = [NSTimer timerWithTimeInterval:0.3 target:self selector:@selector(tapComboButton:) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
        [self.timer fire];
    }
}

- (void)releaseTimer {
    [self.timer invalidate];
    self.timer = nil;
}

#pragma mark - CAAnimationDelegate

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    if (flag) {
        if ([self.delegate respondsToSelector:@selector(giftComboCountdownDidEnd)]) {
            [self.delegate giftComboCountdownDidEnd];
        }
        [self removeFromSuperview];
    }
}

#pragma mark - Setters & Getters

- (CAShapeLayer *)shapeLayer {
    if (!_shapeLayer) {
        _shapeLayer = [CAShapeLayer new];
        _shapeLayer.strokeColor = [UIColor colorWithHexString:@"#FDD112"].CGColor;
        _shapeLayer.fillColor = [UIColor clearColor].CGColor;
        _shapeLayer.lineWidth = 6.5;
        _shapeLayer.lineCap = kCALineCapRound;
        _shapeLayer.strokeEnd = 0.99;
        _shapeLayer.path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(self.comboButton.bounds.size.width / 2, self.comboButton.bounds.size.height / 2) radius:(self.comboButton.bounds.size.width - 6.5) / 2 startAngle:-M_PI_2 endAngle:M_PI + M_PI_2 clockwise:YES].CGPath;
        [self.comboButton.layer addSublayer:_shapeLayer];
    }
    return _shapeLayer;
}

@end
