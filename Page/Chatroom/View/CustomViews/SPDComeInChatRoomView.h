//
//  SPDComeInChatRoomView.h
//  SimpleDate
//
//  Created by Monkey on 2017/5/18.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPDComeInChatRoomView : UIView

//头像
@property(nonatomic,strong)UIImageView *avatarImageView;

@property(nonatomic,strong)UILabel * nicknameLabel;

@property(nonatomic,strong)UIImageView *vehicleImageView;

//首先有没有座驾 默认是no
@property (nonatomic,assign)BOOL isHasVehicle;

-(void)configViewWithNickName:(NSString *)nick_name andAvatar:(NSString *)avatar andVehicle:(NSString *)vehicle ;

- (id)initWithFrame:(CGRect)frame;


-(CGSize)getViewTextSize:(NSString *)nick_name ;

-(void)startAnimationViewActionWithText: (NSString *)text andGender:(NSString *)gender andInputbarFrame:(CGRect) inputbarFrame ;

@end
