//
//  SPDVehicleAnimationView.m
//  SimpleDate
//
//  Created by 李楠 on 17/7/12.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDVehicleAnimationView.h"

@implementation SPDVehicleAnimationView

- (void)setDic:(NSDictionary *)dic {
    _dic = dic;
    
    if ([dic[@"type"] integerValue] == -1) {
        [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:dic[@"avatar"]]];
        self.nickNameLabel.text = dic[@"nick_name"];
        if ([dic[@"gender"] isEqualToString:@"female"]) {
            self.genderImageView.backgroundColor = [UIColor colorWithHexString:@"fe69a1"];
        } else {
            self.genderImageView.backgroundColor = [UIColor colorWithHexString:@"57a3f9"];
        }
    } else {
        self.avatarImageView.hidden = YES;
        self.nickNameLabel.hidden = YES;
        self.genderImageView.hidden = YES;
    }
    
    NSArray *animationImages = dic[@"images"];
    self.animationImageView.image = [animationImages firstObject];
    if (animationImages.count > 1) {
        self.animationImageView.animationImages = dic[@"images"];
        self.animationImageView.animationDuration = 0.25 * _animationImageView.animationImages.count;
        self.animationImageView.animationRepeatCount = 0;
        [self.animationImageView startAnimating];
    }
}

@end
