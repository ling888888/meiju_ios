//
//  ChatroomBottomBar.h
//  SimpleDate
//
//  Created by ling Hou on 2019/12/17.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SVGA.h"

NS_ASSUME_NONNULL_BEGIN

@class ChatroomBottomBar;
@protocol ChatroomBottomBarDelegate <NSObject>

@optional
- (void)chatroomBottomBar:(ChatroomBottomBar *) bottomBar didTouchButton:(UIButton *)sender;
- (void)didTapChatroomBottomBarSendGroupGift;

@end

@interface ChatroomBottomBar : UIView

@property (nonatomic, strong) UIButton * sendMessageButton;
@property (nonatomic, strong) SVGAPlayer * sendGroupGiftView;
@property (nonatomic, strong) UIButton * micButton;
@property (nonatomic, strong) UIButton * micEmojiButton;
@property (nonatomic, strong) UIButton * muteButton;
@property (nonatomic, strong) UIButton * functionButton;
@property (nonatomic, weak) id<ChatroomBottomBarDelegate>delegate;

- (void)resetButtonsLayout;

@end

NS_ASSUME_NONNULL_END
