//
//  SPDChatRoomGlobalView.m
//  SimpleDate
//
//  Created by Monkey on 2017/5/26.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDChatRoomGlobalView.h"
#import "UIImage+GIF.h"
#import "SPDCommonDefine.h"
#import "SPDCRSendMagicMessage.h"
#import "SPDWorldChatMessage.h"
#import "LY_PortraitView.h"

@interface SPDChatRoomGlobalView ()

//蓝色的世界消息

@property(nonatomic,strong)UIImageView *backGroundImage;

@property(nonatomic,strong)LY_PortraitView *portraitView; // 纯文本消息的

@property(nonatomic,strong)UILabel *nicknameLabel;

@property(nonatomic, strong) UILabel *noticeLabel;

@property(nonatomic,strong) LY_PortraitView *nAvatarImageView;

@property(nonatomic, strong) UILabel *nNicknameLabel;

@property(nonatomic,strong)UILabel *contentLabel;

@property(nonatomic,assign) BOOL isShowing;

@property(nonatomic,assign) BOOL isWaiting;

@property (nonatomic,strong)NSMutableArray *globalMutableArray;

@property (nonatomic,strong)UILabel * clanlabel;

@property (nonatomic,strong)UIView * wordTxtMsgView;
@property (nonatomic,strong)UIView * sendGiftMsgView;


@property (nonatomic, strong) LY_PortraitView * sPortraitView;
@property (nonatomic, strong) LY_PortraitView * rPortraitView;

@property (nonatomic,strong)UILabel *rname_Label;
@property (nonatomic,strong)UILabel *sname_Label;
@property (nonatomic,strong)UIImageView *giftImageView;

@property (nonatomic,strong)UILabel *fuction_Label; //送给 和 向 的label

@property (nonatomic,strong)UILabel *magicResultLabel; //魔法额成功失败

@property (nonatomic,strong)UILabel *magicDescLabel; //魔法额成功失败


@end

//魔法用了送礼的世界消息

@implementation SPDChatRoomGlobalView

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.originalFrame = frame;
        [self configUI];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    return self;
}

- (void)configUI {
    
    if (self.nicknameLabel) {
        [self.nicknameLabel removeFromSuperview];
        self.nicknameLabel = nil;
    }
    
    if (self.backGroundImage) {
        [self.backGroundImage removeFromSuperview];
        self.backGroundImage = nil;
    }
    
    if (self.contentLabel) {
        [self.contentLabel removeFromSuperview];
        self.contentLabel = nil;
    }
    
    if (self.clanlabel) {
        [self.clanlabel removeFromSuperview];
        self.clanlabel = nil;
    }
    
    if (self.wordTxtMsgView) {
        [self.wordTxtMsgView removeFromSuperview];
        self.wordTxtMsgView = nil;
    }
    
    if (self.sendGiftMsgView) {
        [self.sendGiftMsgView  removeFromSuperview];
        self.sendGiftMsgView = nil;
    }
    if (self.rname_Label) {
        [self.rname_Label  removeFromSuperview];
        self.rname_Label = nil;
    }
    if (self.sname_Label) {
        [self.sname_Label  removeFromSuperview];
        self.sname_Label = nil;
    }
    
    [self configSubViews];
}

- (void)configSubViews {
    
    self.backGroundImage = [[UIImageView alloc]initWithFrame:self.bounds];
    self.backGroundImage.image = [UIImage imageNamed:@"chatroom_global_message_bg"];
    [self addSubview:self.backGroundImage];
    
    // 文本, @消息布局
    self.wordTxtMsgView = [[UIView alloc]initWithFrame:self.bounds];
    [self addSubview:self.wordTxtMsgView];
    
    self.portraitView = [[LY_PortraitView alloc]init];
    UITapGestureRecognizer *tapAvatarImageView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAvatarImageView:)];
    [self.portraitView addGestureRecognizer:tapAvatarImageView];
    UILongPressGestureRecognizer *longPressAvatarImageView = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressAvatarImageView:)];
    [self.portraitView addGestureRecognizer:longPressAvatarImageView];
    self.portraitView.tag = 0;
    [self.wordTxtMsgView addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(39.0 / 2);
        make.top.mas_equalTo(14-8.5);
        make.width.and.height.mas_equalTo(34 * 1.5);
    }];
    
    self.nicknameLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    self.nicknameLabel.font = [UIFont systemFontOfSize:14];
    [self.wordTxtMsgView addSubview:self.nicknameLabel];
    
    self.noticeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.noticeLabel.text = @"@";
    self.noticeLabel.textColor = SPD_HEXCOlOR(@"fae93b");
    self.noticeLabel.font = [UIFont boldSystemFontOfSize:20];
    [self.wordTxtMsgView addSubview:self.noticeLabel];
    [self.noticeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.nicknameLabel.mas_trailing).with.offset(6);
        make.top.mas_equalTo(19.5);
        make.width.and.height.mas_equalTo(20);
    }];
    
    self.nAvatarImageView = [[LY_PortraitView alloc]init];
    self.nAvatarImageView.userInteractionEnabled = YES;
    self.nAvatarImageView.tag = 3;
    UITapGestureRecognizer *tapNAvatarImageView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAvatarImageView:)];
    [self.nAvatarImageView addGestureRecognizer:tapNAvatarImageView];
    UILongPressGestureRecognizer *longPressNAvatarImageView = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressAvatarImageView:)];
    [self.nAvatarImageView addGestureRecognizer:longPressNAvatarImageView];
    [self.wordTxtMsgView addSubview:self.nAvatarImageView];
    [self.nAvatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.noticeLabel.mas_trailing).with.offset(6);
        make.top.equalTo(self.portraitView.mas_top).offset(0);
        make.width.and.height.mas_equalTo(34*1.5);
    }];
    
    self.nNicknameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.nNicknameLabel.textColor = [UIColor whiteColor];
    self.nNicknameLabel.font = [UIFont systemFontOfSize:14];
    [self.wordTxtMsgView addSubview:self.nNicknameLabel];
    [self.nNicknameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.nAvatarImageView.mas_trailing).with.offset(6);
        make.top.mas_equalTo(22);
        make.width.mas_equalTo(75);
        make.height.mas_equalTo(15);
    }];
    
    self.contentLabel = [[UILabel alloc] init];
    self.contentLabel.font= [UIFont systemFontOfSize:13];
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.textColor = [UIColor whiteColor];
    [self.wordTxtMsgView addSubview:self.contentLabel];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.portraitView);
        make.top.mas_equalTo(self.portraitView.mas_bottom).with.offset(2);
        make.width.mas_lessThanOrEqualTo(kScreenW - 26 - 39);
    }];
    
    // 礼物消息布局
    self.sendGiftMsgView = [[UIView alloc]initWithFrame:self.bounds];
    [self addSubview:self.sendGiftMsgView];
    
    CGFloat leading = 28;
    
    self.sPortraitView = [[LY_PortraitView alloc]init];
    [self.sendGiftMsgView addSubview:self.sPortraitView];
    [self.sPortraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(leading);
        make.top.mas_equalTo(15-8.5);
        make.width.and.height.mas_equalTo(34*1.5);
    }];
    self.sPortraitView.userInteractionEnabled = YES;
    self.sPortraitView.tag = 1;
    UITapGestureRecognizer *sgift_Tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAvatarImageView:)];
    [self.sPortraitView addGestureRecognizer:sgift_Tap];
    UILongPressGestureRecognizer *longPressSAvatarImageView = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressAvatarImageView:)];
    [self.sPortraitView addGestureRecognizer:longPressSAvatarImageView];

    self.sname_Label = [[UILabel alloc] init];
    [self.sendGiftMsgView addSubview:self.sname_Label];
    [self.sname_Label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.sPortraitView.mas_trailing).with.offset(0);
        make.top.mas_equalTo(25);
        make.height.mas_equalTo(20);
    }];
    self.sname_Label.font = [UIFont systemFontOfSize:14];
    self.sname_Label.textColor = [UIColor whiteColor];
    
    self.fuction_Label = [[UILabel alloc] init];
    self.fuction_Label.text = SPDStringWithKey(@"送给", nil);
    self.fuction_Label.textColor = SPD_HEXCOlOR(@"fedd00");
    self.fuction_Label.font = [UIFont systemFontOfSize:22];
    [self.sendGiftMsgView addSubview:self.fuction_Label];
    [self.fuction_Label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(leading);
        make.top.mas_equalTo(self.sPortraitView.mas_bottom).with.offset(12);
        make.height.mas_equalTo(22);
    }];
    
    self.rPortraitView = [[LY_PortraitView alloc]init];
    [self.sendGiftMsgView addSubview:self.rPortraitView];
    [self.rPortraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.fuction_Label.mas_trailing).with.offset(10);
        make.top.mas_equalTo(self.sPortraitView.mas_bottom).with.offset(0);
        make.width.and.height.mas_equalTo(34 * 1.5);
    }];
    self.rPortraitView.userInteractionEnabled = YES;
    self.rPortraitView.tag = 2;
    UITapGestureRecognizer *rgift_avatarTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAvatarImageView:)];
    [self.rPortraitView addGestureRecognizer:rgift_avatarTap];
    UILongPressGestureRecognizer *longPressRAvatarImageView = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressAvatarImageView:)];
    [self.rPortraitView addGestureRecognizer:longPressRAvatarImageView];
    
    self.rname_Label = [[UILabel alloc] init];
    [self.sendGiftMsgView addSubview:self.rname_Label];
    [self.rname_Label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.rPortraitView.mas_trailing).with.offset(0);
        make.top.mas_equalTo(self.sPortraitView.mas_bottom).with.offset(15);
        make.trailing.mas_lessThanOrEqualTo(-112.5);
        make.height.mas_equalTo(20);
    }];
    self.rname_Label.font = [UIFont systemFontOfSize:14];
    self.rname_Label.textColor = [UIColor whiteColor];
    
    self.giftImageView = [[UIImageView alloc] init];
    [self.sendGiftMsgView addSubview:self.giftImageView];
    self.giftImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.giftImageView.clipsToBounds = YES;
    [self.giftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.rname_Label.mas_trailing).with.offset(9);
        make.top.mas_equalTo(self.sPortraitView.mas_bottom).with.offset(5);
        make.width.and.height.mas_equalTo(40);
    }];
    
    self.magicResultLabel = [[UILabel alloc]init];
    [self.sendGiftMsgView addSubview:self.magicResultLabel];
    self.magicResultLabel.hidden = YES;
    [self.magicResultLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.giftImageView.mas_trailing).with.offset(5);
        make.centerY.mas_equalTo(self.rname_Label.mas_centerY);
    }];
    
    self.magicDescLabel = [[UILabel alloc]init];
    self.magicDescLabel.textColor = [UIColor whiteColor];
    self.magicDescLabel.font = [UIFont systemFontOfSize:13];
    [self.sendGiftMsgView addSubview:self.magicDescLabel];
    self.magicDescLabel.hidden = YES;
    [self.magicDescLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(leading);
        make.height.mas_equalTo(14);
        make.top.equalTo(self.rname_Label.mas_bottom).offset(8);
    }];
    
    self.clanlabel = [[UILabel alloc] init];
    self.clanlabel.font= [UIFont systemFontOfSize:14];
    self.clanlabel.textColor = [UIColor whiteColor];
    self.clanlabel.userInteractionEnabled=YES;
    UITapGestureRecognizer *tapClanlabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClanlabel:)];
    [self.clanlabel addGestureRecognizer:tapClanlabel];
    [self addSubview:self.clanlabel];
    [self.clanlabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-15);
        make.bottom.mas_equalTo(-6);
        make.height.mas_equalTo(20);
    }];
}

- (void)tapClanlabel:(UITapGestureRecognizer *)sender {
    if (self.deleage && [self.deleage respondsToSelector:@selector(clickedClanWithId:andClan_name:)]) {
        if ([self.messageContent isKindOfClass:[SPDWorldChatMessage class]]) {
            SPDWorldChatMessage * message = (SPDWorldChatMessage *)self.messageContent;
            [self.deleage clickedClanWithId:message.send_ClanId andClan_name:message.send_ClanName];
        }else if ([self.messageContent isKindOfClass:[SPDCRSendMagicMessage class]]){
            SPDCRSendMagicMessage * message = (SPDCRSendMagicMessage *)self.messageContent;
            [self.deleage clickedClanWithId:message.clan_id andClan_name:message.clan_name];
        }
    }
}

- (void)tapAvatarImageView:(UITapGestureRecognizer *)tap {
    if (_globalMutableArray.count) {
        if (tap.view.tag == 0) {
            if (self.deleage && [self.deleage respondsToSelector:@selector(clickedAvatarImageView:)]) {
                SPDWorldChatMessage * message = (SPDWorldChatMessage *)_globalMutableArray[0];
                [self.deleage clickedAvatarImageView:message.senderUserInfo.userId];
            }
        }else if (tap.view.tag == 1){
            if (self.deleage && [self.deleage respondsToSelector:@selector(clickedAvatarImageView:)]) {
                SPDWorldChatMessage * message = (SPDWorldChatMessage *)_globalMutableArray[0];
                [self.deleage clickedAvatarImageView:message.senderUserInfo.userId];
            }
        }else if (tap.view.tag == 2){
            if ([_globalMutableArray[0] isKindOfClass:[SPDCRSendMagicMessage class]]) {
                SPDCRSendMagicMessage * message = (SPDCRSendMagicMessage *)_globalMutableArray[0];
                if (![message.receive_id isEqualToString:DEV?@"2877":@"5" ]) {
                    [self.deleage clickedAvatarImageView:message.receive_id];
                }
            }else if ([_globalMutableArray[0] isKindOfClass:[SPDWorldChatMessage class]]){
                SPDWorldChatMessage * message = (SPDWorldChatMessage *)_globalMutableArray[0];
                if (![message.receive_Id isEqualToString:DEV?@"2877":@"5" ]) {
                    [self.deleage clickedAvatarImageView:message.receive_Id];
                }
            }
        } else if (tap.view.tag == 3) {
            SPDWorldChatMessage * message = (SPDWorldChatMessage *)_globalMutableArray[0];
            [self.deleage clickedAvatarImageView:message.mentionedList[0][@"user_id"]];
            
        }
    }
    
}

- (void)longPressAvatarImageView:(UILongPressGestureRecognizer *)longPress {
    if (_globalMutableArray.count) {
        NSDictionary *userInfo;
        RCMessageContent *messageContent = _globalMutableArray[0]?:nil;
        if (messageContent && longPress.state == UIGestureRecognizerStateBegan) {
            if (longPress.view.tag == 0 || longPress.view.tag == 1) {
                SPDWorldChatMessage *worldMsg = (SPDWorldChatMessage *)messageContent;
                userInfo = @{@"user_id":worldMsg.senderUserInfo.userId?:@"", @"nick_name": worldMsg.senderUserInfo.name?:@"", @"avatar": worldMsg.senderUserInfo.portraitUri?:@"", @"gender": worldMsg.gender?:@"",Headwear:worldMsg.send_headwear?:@""};
            }
            else if (longPress.view.tag == 2){
                if ([messageContent isKindOfClass:[SPDWorldChatMessage class]]) {
                    SPDWorldChatMessage *worldMsg = (SPDWorldChatMessage *)messageContent;
                    userInfo = @{@"user_id": worldMsg.receive_Id?:@"", @"nick_name": worldMsg.receive_nickname?:@"", @"avatar": worldMsg.receive_avatar?:@"", @"gender": worldMsg.receive_gender?:@"",Headwear:worldMsg.receive_headwear?:@""};
                }else if ([messageContent isKindOfClass:[SPDCRSendMagicMessage class]]){
                    SPDCRSendMagicMessage *magicMsg = (SPDCRSendMagicMessage *)messageContent;
                    userInfo = @{@"user_id": magicMsg.receive_id?:@"", @"nick_name": magicMsg.receive_nickname?:@"", @"avatar": magicMsg.receive_avatar?:@"", @"gender": magicMsg.receive_gender?:@"",Headwear:magicMsg.receive_headwear?:@""};
                }
            } else if (longPress.view.tag == 3){
                SPDWorldChatMessage *worldMsg = (SPDWorldChatMessage *)messageContent;
                NSDictionary * dic = worldMsg.mentionedList[0];
                userInfo = @{@"user_id": dic[@"user_id"]?:@"", @"nick_name": dic[@"nick_name"]?:@"", @"avatar": dic[@"avatar"]?:@"", @"gender": dic[@"gender"]?:@"",@"headwear":dic[Headwear]?:@""};
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:CHATROOM_LONGPRESS object:nil userInfo:userInfo];
        }
    }
}

- (void)stopAnimation {

    self.isWaiting = NO;
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        self.center = CGPointMake(0, self.originalFrame.origin.y);
        self.transform = CGAffineTransformScale(self.transform, 0.1, 0.1);
        self.alpha = 0;
        
    } completion:^(BOOL finished) {
        self.transform = CGAffineTransformScale(self.transform, 10, 10);
        self.alpha = 1;
        [self resetFrame];
        if (finished) {
            
            if(self.globalMutableArray.count){
                [self.globalMutableArray removeObjectAtIndex:0];
            }
            if (self.globalMutableArray.count) {
                [self startAnimationViewWithMessage:_globalMutableArray[0]];
            } else {
                self.isShowing = NO;
            }
        } else {
            [self.globalMutableArray removeAllObjects];
            self.isShowing = NO;
        }
    }];
}

- (void)enterBackground {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(stopAnimation) object:nil];
    [self resetFrame];
    [self.globalMutableArray removeAllObjects];
    self.isShowing = NO;
    self.isWaiting = NO;
}

- (void)resetFrame {
    self.frame = self.originalFrame;
}

- (NSMutableArray *)globalMutableArray {
    if (!_globalMutableArray) {
        _globalMutableArray = [NSMutableArray array];
    }
    return _globalMutableArray;
}

- (void)setMessageContent:(RCMessageContent *)messageContent {
    self.hidden = NO;
    _messageContent = messageContent;
    [self.globalMutableArray addObject:_messageContent];
    if (!self.isShowing) {
        self.isShowing = YES;
        [self startAnimationViewWithMessage:_messageContent];
    } else {
        if (_isWaiting) {
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(stopAnimation) object:nil];
            [self stopAnimation];
        }
    }
}

- (void)startAnimationViewWithMessage:(RCMessageContent *)message {
    if ([message isKindOfClass:[SPDWorldChatMessage class]]) {
        SPDWorldChatMessage * wordMessage = (SPDWorldChatMessage *)message;
        if ([SPDCommonTool isEmpty:wordMessage.type] || [wordMessage.type isEqualToString:@"rocket"]) {
            [self configLayOutWithMessageTypeNil:wordMessage];
        }else if ([wordMessage.type isEqualToString:@"2"]){
            [self configLayOutWithMessageType2:wordMessage];
        }
    }else if ([message isKindOfClass:[SPDCRSendMagicMessage class]]){
        SPDCRSendMagicMessage * magicMessage = (SPDCRSendMagicMessage *)message;
        [self configMagicDataWithMessage:magicMessage];
    }
}

// 纯文字的世界消息
- (void) configLayOutWithMessageTypeNil:(SPDWorldChatMessage *)message {
    self.sendGiftMsgView.hidden = YES;
    self.wordTxtMsgView.hidden = NO;
    
    if ([message.type isEqualToString:@"rocket"]) {
        self.backGroundImage.image = [UIImage imageNamed:[SPDCommonTool getArImageNameWithImage:@"img_liaotianshi_shijiexiaoxi_huojian"]];
    }else{
        self.backGroundImage.image = [UIImage imageNamed:@"chatroom_global_message_bg"];
    }
    self.nicknameLabel.text = message.senderUserInfo.name?:@"";
    
    self.portraitView.portrait = message.portrait;
    
    // @相关
    NSString *mentionedType = message.mentionedType;
    NSArray *mentionedList = message.mentionedList;
    if (mentionedType && [mentionedType isEqualToString:@"2"] && mentionedList && mentionedList.count == 1) {
        self.noticeLabel.hidden = NO;
        self.nAvatarImageView.hidden = NO;
        self.nNicknameLabel.hidden = NO;
        NSDictionary *user = mentionedList[0];
        
        [self.nicknameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo(self.portraitView.mas_trailing).with.offset(0);
            make.top.mas_equalTo(22);
            make.width.mas_lessThanOrEqualTo(75);
            make.height.mas_equalTo(15);
        }];
        
        LY_Portrait * pp = [[LY_Portrait alloc]init];
        pp.url = user[@"avatar"];
        pp.headwearUrl = message.receiveHeadwearWebp;
        self.nAvatarImageView.portrait = pp;
        
        self.nNicknameLabel.text = user[@"nick_name"];
        
        NSString *content = [message.content stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"@%@ ", user[@"nick_name"]] withString:@""];
        self.contentLabel.text = content;
        
    } else {
        self.noticeLabel.hidden = YES;
        self.nAvatarImageView.hidden = YES;
        self.nNicknameLabel.hidden = YES;
        [self.nicknameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
        }];
        [self.nicknameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo(self.portraitView.mas_trailing).with.offset(6);
            make.top.mas_equalTo(22);
            make.width.mas_lessThanOrEqualTo(300);
            make.height.mas_equalTo(15);
        }];
        
        self.contentLabel.text = message.content;
    }
    
    NSDictionary *attribtDic = @{NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@",message.send_ClanName] attributes:attribtDic];
    self.clanlabel.attributedText = attribtStr;
    
    [self beginCompleteAnimationAction];
    
}

// type2 的世界消息
- (void) configLayOutWithMessageType2:(SPDWorldChatMessage *)message {
    self.sendGiftMsgView.hidden = NO;
    self.magicResultLabel.hidden = NO;
    self.wordTxtMsgView.hidden = YES;
    self.magicDescLabel.hidden = YES;
    
    self.sPortraitView.portrait = message.portrait;

    LY_Portrait * rportrait = [[LY_Portrait alloc]init];
    rportrait.url = message.receive_avatar;
    rportrait.headwearUrl = message.receiveHeadwearWebp;
    self.rPortraitView.portrait = rportrait;
    
    self.sname_Label.text = message.senderUserInfo.name?:@"";
    
    self.rname_Label.text = message.receive_nickname?:@"";
    
    self.fuction_Label.text = SPDStringWithKey(@"送给", nil);
    [self.giftImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:message.present_url]];
    self.magicResultLabel.textColor = [UIColor colorWithHexString:@"fedd00"];
    self.magicResultLabel.font = [UIFont systemFontOfSize:23];
    self.magicResultLabel.text = [NSString stringWithFormat:@"x%@", message.num?:@"1"];
    
    NSDictionary *attribtDic = @{NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@",message.send_ClanName?:@""] attributes:attribtDic];
    self.clanlabel.attributedText = attribtStr;
    
        if (message && message.receive_user_type.notEmpty) {
          self.rname_Label.textColor = [UIColor whiteColor];
          if ([message.receive_user_type isEqualToString:@"all"]) {
              self.rname_Label.text = [message.curSel isEqualToString:message.sumSel] ? SPDStringWithKey(@"房间内每个人", nil): [NSString stringWithFormat:SPDLocalizedString(@"房间内%@人"),message.curSel];
          }else if ([message.receive_user_type isEqualToString:@"allfemale"]){
              self.rname_Label.text = [message.curSel isEqualToString:message.sumSel] ? SPDStringWithKey(@"房间内每个女用户", nil): [NSString stringWithFormat:SPDLocalizedString(@"房间内%@个女用户"),message.curSel];
          }else if ([message.receive_user_type isEqualToString:@"allmale"]){
              self.rname_Label.text = [message.curSel isEqualToString:message.sumSel] ? SPDStringWithKey(@"房间内每个男用户", nil): [NSString stringWithFormat:SPDLocalizedString(@"房间内%@个男用户"),message.curSel];
          }else if ([message.receive_user_type isEqualToString:@"mic"]){
              self.rname_Label.text = [message.curSel isEqualToString:message.sumSel] ? SPDStringWithKey(@"麦上每个人", nil): [NSString stringWithFormat:SPDStringWithKey(@"麦上%@人", nil),message.curSel];
          }else if ([message.receive_user_type isEqualToString:@"micfemale"]){
              self.rname_Label.text = [message.curSel isEqualToString:message.sumSel] ? SPDStringWithKey(@"麦上每个女用户", nil): [NSString stringWithFormat:SPDLocalizedString(@"麦上%@个女用户"),message.curSel];
          }else if ([message.receive_user_type isEqualToString:@"micmale"]){
              self.rname_Label.text = [message.curSel isEqualToString:message.sumSel] ? SPDStringWithKey(@"麦上每个男用户", nil): [NSString stringWithFormat:SPDLocalizedString(@"麦上%@个男用户"),message.curSel];
          }
       }
    
    [self beginCompleteAnimationAction];
    
}

- (void)configMagicDataWithMessage:(SPDCRSendMagicMessage *)magicMsg {
    self.wordTxtMsgView.hidden = YES;
    self.sendGiftMsgView.hidden = NO;
    self.magicResultLabel.hidden = NO;
    self.magicDescLabel.hidden= YES; // 2020.6.5 隐藏
    
//    LY_Portrait * sportrait = [[LY_Portrait alloc]init];
//    sportrait.url = magicMsg.senderUserInfo.portraitUri;
//    sportrait.headwearUrl = magicMsg.senderHeadwearWebp;
    self.sPortraitView.portrait = magicMsg.portrait;
    
    LY_Portrait * rportrait = [[LY_Portrait alloc]init];
    rportrait.url = magicMsg.receive_avatar;
    rportrait.headwearUrl = magicMsg.receiveHeadwearWebp;
    self.rPortraitView.portrait = rportrait;

    self.sname_Label.text =magicMsg.senderUserInfo.name?:@"";
    
    self.fuction_Label.text = SPDStringWithKey(@"向", nil);
    self.rname_Label.text = [NSString stringWithFormat:@"%@%@",magicMsg.receive_nickname,SPDStringWithKey(@"使用", nil)];
    [self.giftImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:magicMsg.magic_url]];
    
    self.magicResultLabel.textColor = [UIColor whiteColor];
    self.magicResultLabel.font = [UIFont systemFontOfSize:13];
    if ([magicMsg.result isEqualToString:@"success"]) {
        self.magicResultLabel.text = SPDStringWithKey(@"成功", nil);
    }else{
        self.magicResultLabel.text = SPDStringWithKey(@"失败", nil);
    }
    
//    if ([magicMsg.result isEqualToString:@"success"]) {
//        NSMutableAttributedString * firstPart;
//        NSString * name = magicMsg.receive_nickname;
//        if (name.length > 5) {
//            firstPart = [[NSMutableAttributedString alloc] initWithString:[name substringToIndex:5]];
//        }else{
//            firstPart = [[NSMutableAttributedString alloc] initWithString:name];
//        }
//        NSDictionary * firstAttributes;
//        firstAttributes = @{ NSFontAttributeName:[UIFont boldSystemFontOfSize:11],NSForegroundColorAttributeName:[UIColor colorWithHexString:@"922790"],};
//        [firstPart setAttributes:firstAttributes range:NSMakeRange(0,firstPart.length)];
//        NSMutableAttributedString * secondPart = [[NSMutableAttributedString alloc] initWithString:[self configDescription:magicMsg]];
//        NSDictionary * secondeAttribute = @{ NSFontAttributeName:[UIFont boldSystemFontOfSize:11],NSForegroundColorAttributeName:[UIColor colorWithHexString:@"ffc12b"],};
//        [secondPart setAttributes:secondeAttribute range:NSMakeRange(0,secondPart.length)];
//        [firstPart appendAttributedString:secondPart];
//        self.magicDescLabel.attributedText = firstPart;
//        self.magicDescLabel.hidden = NO;
//    }else{
//        self.magicDescLabel.hidden = YES;
//    }
//
    NSDictionary *attribtDic = @{NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@",magicMsg.clan_name] attributes:attribtDic];
    self.clanlabel.attributedText = attribtStr;
    
    [self beginCompleteAnimationAction];
    
}

- (NSString *)configDescription:(SPDCRSendMagicMessage* )magicMsg{
    NSString * str ;
    NSString * effect = [magicMsg.magic_effect isEqualToString:@"inc"] ? @"增加":@"减少";
    if ([magicMsg.type isEqualToString:@"charm"] || [magicMsg.type isEqualToString:@"exp"]) {
        if ([magicMsg.type isEqualToString:@"charm"]) {
            NSString * temp =[NSString stringWithFormat:@"%@魅力值",effect];
            str =[NSString stringWithFormat:@"%@%@",SPDStringWithKey(temp, nil),magicMsg.magic_effect_value] ;
        }else{
            NSString * temp = [NSString stringWithFormat:@"%@经验值",effect];
            str =[NSString stringWithFormat:@"%@%@",SPDStringWithKey(temp, nil),magicMsg.magic_effect_value] ;
        }
    }else{
        if ([magicMsg.type isEqualToString:@"banned_talk"]) {
            str = [NSString stringWithFormat:@"%@%@s",SPDStringWithKey(@"被禁言", nil),magicMsg.magic_effect_value];
        }else if ([magicMsg.type isEqualToString:@"banned_mic"]){
            str = [NSString stringWithFormat:@"%@%@s",SPDStringWithKey(@"被封麦", nil),magicMsg.magic_effect_value];
        }else if ([magicMsg.type isEqualToString:@"kicked"]){
            str = [NSString stringWithFormat:@"%@s%@",magicMsg.magic_effect_value,SPDStringWithKey(@"内无法进入被踢出的房间", nil)];
        }else{
            str = @"被禁言";
        }
    }
    return str;
}
// 做动画
- (void)beginCompleteAnimationAction {
    [UIView animateWithDuration:0.7 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        CGRect myframe = self.frame;
        myframe.origin.x = 0;
        self.frame = myframe;
        
    } completion:^(BOOL finished) {
        if (finished) {
            if (self.globalMutableArray.count > 1) {
                [self stopAnimation];
            } else {
                self.isWaiting = YES;
                [self performSelector:@selector(stopAnimation) withObject:nil afterDelay:8];
            }
        } else {
            [self resetFrame];
            [self.globalMutableArray removeAllObjects];
            self.isShowing = NO;
        }
    }];
}


@end
