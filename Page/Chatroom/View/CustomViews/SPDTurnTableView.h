//
//  SPDTurnTableView.h
//  SimpleDate
//
//  Created by Monkey on 2018/12/14.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SPDTurnTableViewDelegate <NSObject>

- (void)TurnTableViewDidFinishWithIndex:(NSInteger)index;

@end

@interface SPDTurnTableView : UIView

@property (nonatomic,assign) NSInteger numberIndex;

@property (nonatomic,strong) UIButton * playButton;

@property (nonatomic,strong) UILabel * goldLabel;

@property (nonatomic,strong) UIImageView * rotateWheel;  // 转盘背景

@property (nonatomic,strong) NSMutableArray * dataArray;  // 存放奖励

@property (nonatomic, strong) NSNumber *gold;


@property (nonatomic,assign) id <SPDTurnTableViewDelegate> delegate;

@end
