//
//  ChatroomMoreFunctionCell.h
//  SimpleDate
//
//  Created by ling Hou on 2019/12/26.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatroomMoreFunctionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

NS_ASSUME_NONNULL_END
