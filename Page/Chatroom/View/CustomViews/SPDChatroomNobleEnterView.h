//
//  SPDChatroomNobleEnterView.h
//  SimpleDate
//
//  Created by 李楠 on 2018/10/16.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SPDJoinChatRoomMessage;

@interface SPDChatroomNobleEnterView : UIView
//1
@property (nonatomic, strong) RCMessageContent *message;

//@property (nonatomic, strong) LiveJoinMessage * liveJoinMessage;

@end
