//
//  SPDChatroomOnlineUserView.m
//  SimpleDate
//
//  Created by 李楠 on 2018/12/27.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "SPDChatroomOnlineUserView.h"
#import "SPDChatroomOnlineUserCell.h"
#import "SPDVoiceLiveUserModel.h"
#import "ZegoKitManager.h"

@interface SPDChatroomOnlineUserView ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *onlineNumberLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *collectionViewFlowLayout;

@end

@implementation SPDChatroomOnlineUserView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.contentView.frame = CGRectMake(0, kScreenH, kScreenW, kScreenH - 176);
    self.collectionViewFlowLayout.itemSize = CGSizeMake(kScreenW, 77);
    [self.collectionView registerNib:[UINib nibWithNibName:@"SPDChatroomOnlineUserCell" bundle:nil] forCellWithReuseIdentifier:@"SPDChatroomOnlineUserCell"];
}

- (void)show {
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        self.contentView.frame = CGRectMake(0, 184, kScreenW, kScreenH - 176);
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismiss {
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [UIColor clearColor];
        self.contentView.frame = CGRectMake(0, kScreenH, kScreenW, kScreenH - 176);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (IBAction)tap:(UITapGestureRecognizer *)sender {
    [self dismiss];
}

- (void)setDataArray:(NSMutableArray *)dataArray {
    _dataArray = dataArray;
    
    NSMutableArray *micArr = [NSMutableArray array];
    for (SPDVoiceLiveUserModel *model in _dataArray) {
        if ([ZegoManager getUserMicIndex:model._id]) {
            model.micIndex = [ZegoManager getUserMicIndex:model._id];
            [micArr addObject:model];
        } else {
            model.micIndex = nil;
        }
    }
    
    NSSortDescriptor *sortDescriptor1 = [NSSortDescriptor sortDescriptorWithKey:@"nobleValue" ascending:NO];
    NSSortDescriptor *sortDescriptor2 = [NSSortDescriptor sortDescriptorWithKey:@"level" ascending:NO];
    [_dataArray sortUsingDescriptors:@[sortDescriptor1, sortDescriptor2]];
    if (micArr.count) {
        [_dataArray removeObjectsInArray:micArr];
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"micIndex" ascending:YES];
        [micArr sortUsingDescriptors:@[sortDescriptor]];
        [_dataArray insertObjects:micArr atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, micArr.count)]];
    }
    
    self.onlineNumberLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"在线：%ld人", nil), _dataArray.count];
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SPDChatroomOnlineUserCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SPDChatroomOnlineUserCell" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(didSelectUserWithModel:)]) {
        [self.delegate didSelectUserWithModel:self.dataArray[indexPath.row]];
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ![touch.view isDescendantOfView:self.contentView];
}

@end
