//
//  SPDChatRoomEmojiCell.m
//  SimpleDate
//
//  Created by Monkey on 2019/5/28.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "SPDChatRoomEmojiCell.h"
#import "SPDEmojiModel.h"
@interface SPDChatRoomEmojiCell ()
@property (weak, nonatomic) IBOutlet UIImageView *emojiImageView;

@end

@implementation SPDChatRoomEmojiCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(SPDEmojiModel *)model {
    _model = model;
    [self.emojiImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,_model.image]]];
    self.titleLabel.text = _model.desc;
}

@end
