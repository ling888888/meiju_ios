//
//  SPDChatRoomEmojiView.h
//  SimpleDate
//
//  Created by Monkey on 2019/5/28.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class SPDEmojiModel,SPDChatRoomEmojiView;
@protocol SPDChatRoomEmojiViewDelegate <NSObject>

- (void)clickedChatRoomEmojiWith:(SPDEmojiModel *)model;
- (void)chatroomEmojiView:(SPDChatRoomEmojiView *)emojiView didClickedUpMicButton:(UIButton *)sender;
- (void)chatroomEmojiView:(SPDChatRoomEmojiView *)emojiView didClickedjoinClanButton:(UIButton *)sender;

@end

@interface SPDChatRoomEmojiView : UIView

@property (nonatomic, assign) NSInteger positionLevel; // 0 为游客

@property (nonatomic, assign) BOOL isHaveClan; // 当前用户是否具有家族

@property (nonatomic, weak)id<SPDChatRoomEmojiViewDelegate>delegate;

- (void)show;

- (void)hide;

@end

NS_ASSUME_NONNULL_END
