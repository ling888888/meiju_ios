//
//  SPDChatRoomEmojiView.m
//  SimpleDate
//
//  Created by Monkey on 2019/5/28.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "SPDChatRoomEmojiView.h"
#import "SPDChatRoomEmojiCell.h"
#import "SPDChatRoomEmojiLayout.h"
#import "SPDPresentFlowLayout.h"
#import "ZegoKitManager.h"
#import "SPDEmojiModel.h"
#import "CollectionPlaceholderCell.h"
#import "SPDVoiceLiveOnlineUserCell.h"

static const NSInteger tipsBarHeight = 40;
static const NSInteger tipsBarSpace = 5;
static const NSInteger bottomBarHeight = 317;

@interface SPDChatRoomEmojiView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate,UIGestureRecognizerDelegate,UIScrollViewDelegate>

@property (nonatomic, strong) UIView * bottomView;
@property (nonatomic, strong) UIView * tipsView;
@property (nonatomic, strong) UILabel * tipsLabel;
@property (nonatomic, strong) UIButton * tipsButton;
@property (nonatomic, copy)  NSString * currentFreeMicIndex; // 当前空余麦的index
@property (nonatomic, strong) UICollectionView * collectionView; // collection
@property (nonatomic, strong) NSMutableArray * dataArray;
@property (nonatomic, strong) UIPageControl * pageControl;

@end

@implementation SPDChatRoomEmojiView


- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self setUI];
    }
    return self;
}

- (void)setUI {
    
    [self addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(0);
        make.trailing.mas_equalTo(0);
        make.height.mas_equalTo(bottomBarHeight);
        make.top.mas_equalTo(kScreenH + tipsBarHeight + tipsBarSpace);
    }];
    [self addSubview:self.tipsView];
    [self.tipsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(10);
        make.trailing.mas_equalTo(-10);
        make.height.mas_equalTo(tipsBarHeight);
        make.bottom.bottom.equalTo(self.bottomView.mas_top).offset(tipsBarSpace);
    }];
    self.tipsView.layer.cornerRadius = 20;
    self.tipsView.clipsToBounds = YES;
    self.tipsView.hidden = YES;
    
    self.tipsLabel = [[UILabel alloc]init];
    self.tipsLabel.textColor = [UIColor whiteColor];
    self.tipsLabel.font = [UIFont systemFontOfSize:14];
    [_tipsView addSubview:self.tipsLabel];
    [self.tipsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15.5);
        make.centerY.mas_equalTo(0);
    }];
    
    self.tipsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.tipsButton setBackgroundColor:SPD_HEXCOlOR(@"#00D4A0")];
    [self.tipsButton addTarget:self action:@selector(touchTipsButton:) forControlEvents:UIControlEventTouchUpInside];
    [_tipsView addSubview:self.tipsButton];
    [self.tipsButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-10);
        make.height.mas_equalTo(28);
        make.width.mas_greaterThanOrEqualTo(80);
        make.centerY.mas_equalTo(0);
    }];
    self.tipsButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.tipsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.tipsButton.layer.cornerRadius = 14;
    self.tipsButton.clipsToBounds = YES;
    self.tipsButton.hidden = YES;
    
    [self.bottomView addSubview:self.collectionView];
    [self.bottomView addSubview:self.pageControl];
    
    self.dataArray = [NSMutableArray new];
    [self.collectionView registerNib:[UINib nibWithNibName:@"SPDChatRoomEmojiCell" bundle:nil] forCellWithReuseIdentifier:@"SPDChatRoomEmojiCell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"CollectionPlaceholderCell" bundle:nil] forCellWithReuseIdentifier:@"CollectionPlaceholderCell"];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handle)];
    tap.delegate = self;
    [self addGestureRecognizer:tap];
}

- (void)handle {
    [self hide];
}

- (void)show {
    [self requestData];
}

- (void)hide {
    [UIView animateWithDuration:0.3 animations:^{
        CGRect bottomFrame = self.bottomView.frame;
        bottomFrame.origin.y = kScreenH + 40 + 5;
        self.bottomView.frame = bottomFrame;
        CGRect tipsFrame = self.tipsView.frame;
        tipsFrame.origin.y = kScreenH;
        self.tipsView.frame = tipsFrame;
    } completion:^(BOOL finished) {
        self.tipsView.hidden = YES;
        [self removeFromSuperview];
    }];
}

- (void)requestData {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"chatroom.expression.list" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        NSArray * list = suceess[@"list"];
        [self.dataArray removeAllObjects];
        NSMutableArray * data =[NSMutableArray new];
        for (NSDictionary * dict in list) {
            SPDEmojiModel * model = [SPDEmojiModel initWithDictionary:dict];
            [data addObject:model];
        }
        self.dataArray = data ;
        [self.collectionView reloadData];
        self.pageControl.numberOfPages = self.dataArray.count /12;
        self.pageControl.currentPage = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            CGRect bottomFrame = self.bottomView.frame;
            bottomFrame.origin.y = kScreenH - bottomBarHeight ;
            self.bottomView.frame = bottomFrame;
            CGRect tipsViewFrame = self.tipsView.frame;
            tipsViewFrame.origin.y = kScreenH - bottomBarHeight - 4 - 40;
            self.tipsView.frame = tipsViewFrame;
        } completion:^(BOOL finished) {
            [self configTipsView];
        }];
        
    } bFailure:^(id  _Nullable failure) {
        [SPDCommonTool showWindowToast:failure[@"msg"]];
    }];
}

- (void)configTipsView {
    // 在麦上直接使用
    if (ZegoManager.selfMicIndex) {
        return;
    }
    self.tipsView.hidden = NO;
    if (self.positionLevel) {
        self.currentFreeMicIndex = [self checkIsHaveUseableMicWithStart:0 userId:[SPDApiUser currentUser].userId];
        self.tipsButton.hidden = NO;
        self.tipsLabel.text = SPDStringWithKey(@"上麦后可使用麦上表情哦!", nil);
        [self.tipsButton setBackgroundColor: self.currentFreeMicIndex ? SPD_HEXCOlOR(COMMON_PINK) :SPD_HEXCOlOR(@"#959595") ];
        [self.tipsButton setTitle: self.currentFreeMicIndex ?SPDStringWithKey(@"上麦", nil) :SPDStringWithKey(@"麦位已满", nil) forState:UIControlStateNormal];
        self.tipsButton.userInteractionEnabled = self.currentFreeMicIndex ? YES : NO ;
    } else {
       self.currentFreeMicIndex= [self checkIsHaveUseableMicWithStart:MicCount - PublicMicCount userId:[SPDApiUser currentUser].userId];
        self.tipsLabel.text = self.currentFreeMicIndex ? SPDStringWithKey(@"上麦后可使用麦上表情哦!", nil) : SPDStringWithKey(@"游客麦位已满", nil);
        self.tipsButton.hidden = (!self.currentFreeMicIndex && self.isHaveClan);
        [self.tipsButton setBackgroundColor:SPD_HEXCOlOR(COMMON_PINK)];
        [self.tipsButton setTitle: self.currentFreeMicIndex ? SPDStringWithKey(@"上麦", nil) : SPDStringWithKey(@"加入家族", nil) forState:UIControlStateNormal];
        self.tipsButton.userInteractionEnabled = YES;
    }
    
}

- (NSString *)checkIsHaveUseableMicWithStart:(NSInteger)start userId:(NSString *)userId {
    for (NSInteger index = start; index < MicCount; index++) {
        NSString *micIndex = [NSString stringWithFormat:@"%ld", index];
        if (!ZegoManager.micUsers[micIndex] && [ZegoManager.micStatus[micIndex] integerValue] != CannotUse && [ZegoManager.micStatus[micIndex] integerValue] != Freeze) {
            return micIndex;
        }
    }
    return nil;
}

- (void)touchTipsButton:(UIButton *) sender {
    [self hide];
    if (self.currentFreeMicIndex) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(chatroomEmojiView:didClickedUpMicButton:)]) {
            [self.delegate chatroomEmojiView:self didClickedUpMicButton:sender];
        }
    }else{
        if (self.delegate && [self.delegate respondsToSelector:@selector(chatroomEmojiView:didClickedjoinClanButton:)]) {
            [self.delegate chatroomEmojiView:self didClickedjoinClanButton:sender];
        }
    }
}

#pragma mark - collection delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.dataArray[indexPath.row] isKindOfClass:[SPDEmojiModel class]]) {
        SPDChatRoomEmojiCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SPDChatRoomEmojiCell" forIndexPath:indexPath];
        cell.model = self.dataArray[indexPath.row];
        return cell;
    }else{
        CollectionPlaceholderCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([CollectionPlaceholderCell class]) forIndexPath:indexPath];
        return cell;
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(kScreenW / 4 , 89);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (!ZegoManager.selfMicIndex) {
        return;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedChatRoomEmojiWith:)] && [self.dataArray[indexPath.row] isKindOfClass:[SPDEmojiModel class]]) {
        [self hide];
        [self.delegate clickedChatRoomEmojiWith:self.dataArray[indexPath.row]];
    }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    self.pageControl.currentPage = targetContentOffset->x / (self.collectionView.bounds.size.width);
}


#pragma mark - getter

- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[UIView alloc]initWithFrame:CGRectMake(0,kScreenH, kScreenW, bottomBarHeight)];
        _bottomView.backgroundColor = [UIColor colorWithHexString:@"#2B1B35"];
    }
    return _bottomView;
}

- (UIView *)tipsView {
    if (!_tipsView) {
        _tipsView = [[UIView alloc]init];
        _tipsView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.68];
    }
    return _tipsView;
}

- (UICollectionView *) collectionView {
    if (!_collectionView) {
        SPDChatRoomEmojiLayout * layout = [[SPDChatRoomEmojiLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, self.bottomView.bounds.size.width, self.bottomView.bounds.size.height - 50) collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor clearColor];
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.pagingEnabled = YES;
    }
    return _collectionView;
}

- (UIPageControl *)pageControl {
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc]initWithFrame:CGRectMake((kScreenW - 100)/2, CGRectGetMaxY(self.collectionView.frame), 100, 50)];
    }
    return _pageControl;
}

- (void)setDataArray:(NSMutableArray *)dataArray {
    while (dataArray.count % 12 != 0) {
        [dataArray addObject:@"emoji"];
    }
    _dataArray = dataArray;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (touch.view == self) {
        return YES;
    }
    return NO;
}

@end
