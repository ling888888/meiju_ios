//
//  SPDChatRoomEmojiCell.h
//  SimpleDate
//
//  Created by Monkey on 2019/5/28.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class SPDEmojiModel;
@interface SPDChatRoomEmojiCell : UICollectionViewCell

@property (nonatomic, strong) SPDEmojiModel * model;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

NS_ASSUME_NONNULL_END
