//
//  ChatroomBottomBar.m
//  SimpleDate
//
//  Created by ling Hou on 2019/12/17.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "ChatroomBottomBar.h"
#import "UIView+RGSize.h"
#import "ZegoKitManager.h"
@interface ChatroomBottomBar ()<SVGAPlayerDelegate>

@end

@implementation ChatroomBottomBar

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews {
    CGFloat tSpace = (kScreenW - 38)/2.0f;
    CGFloat space = kScreenW >= 375 ? ( (self.width - 38 - 10* 2 - 35 * 4 - 92 ) / 5 ) : (tSpace - 92 - 35)/3;
    self.sendGroupGiftView = [[SVGAPlayer alloc]init];
    [self addSubview:self.sendGroupGiftView];
    self.sendGroupGiftView.contentMode = UIViewContentModeScaleAspectFit;
    self.sendGroupGiftView.loops = 0;
    self.sendGroupGiftView.clearsAfterStop = YES;
    self.sendGroupGiftView.delegate = self;
    SVGAParser *parser = [[SVGAParser alloc] init];
    [parser parseWithNamed:@"chatroom_group_gift" inBundle:[NSBundle mainBundle] completionBlock:^(SVGAVideoEntity * _Nonnull videoItem) {
        self.sendGroupGiftView.videoItem = videoItem;
        [self.sendGroupGiftView startAnimation];
    } failureBlock:^(NSError * _Nonnull error) {
        
    }];
    [self.sendGroupGiftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.centerY.mas_equalTo(0);
        make.height.and.width.mas_equalTo(38);
    }];
    self.sendGroupGiftView.userInteractionEnabled = YES;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(touchSendGroupGift:)];
    [self.sendGroupGiftView addGestureRecognizer:tap];
    
    self.sendMessageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.sendMessageButton];
    self.sendMessageButton.tag = 0;
    [self.sendMessageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(kScreenW >= 375 ? 10: space);
        make.width.mas_equalTo(92);
        make.height.mas_equalTo(36);
        make.centerY.mas_equalTo(0);
    }];
    [self.sendMessageButton setTitle:SPDStringWithKey(@"想说点...", nil) forState:UIControlStateNormal];
    [self.sendMessageButton setTitleColor:SPD_HEXCOlOR(@"#B0B0B0") forState:UIControlStateNormal];
    self.sendMessageButton.titleLabel.font = [UIFont systemFontOfSize:13];
    [self.sendMessageButton setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.28]];
    [self.sendMessageButton setImage:[UIImage imageNamed:@"chatroom_send_msg"] forState:UIControlStateNormal];
    [self.sendMessageButton setImage:[UIImage imageNamed:@"chatroom_send_msg"] forState:UIControlStateSelected];
    self.sendMessageButton.layer.cornerRadius = 18;
    self.sendMessageButton.clipsToBounds = YES;
    [self.sendMessageButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 7, 0, 0)];
    [self.sendMessageButton addTarget:self action:@selector(onTouchSendMessageButton:) forControlEvents:UIControlEventTouchUpInside];
    self.sendMessageButton.mirrorEdgeInsets = YES;
    
    self.micEmojiButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.micEmojiButton.tag = 1;
    [self.micEmojiButton setImage:[UIImage imageNamed:@"ic_mic_emoji"] forState:UIControlStateNormal];
    self.micEmojiButton.selected = ZegoManager.selfMicIndex.length > 0  ? YES : NO;
    [self.micEmojiButton addTarget:self action:@selector(onTouchSendMessageButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.micEmojiButton];
    [self.micEmojiButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.sendMessageButton.mas_trailing).offset(space);
        make.centerY.mas_equalTo(0);
        make.height.and.width.mas_equalTo(35);
    }];
    
    CGFloat mSapce = ZegoManager.selfMicIndex ? (tSpace - 10 - 3 * 35)/ 3 : (tSpace - 2 * 35)/ 3;
    self.muteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.muteButton];
    self.muteButton.tag = 2;
    self.muteButton.selected = ZegoManager.speakerEnabled;
    [self.muteButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.sendGroupGiftView.mas_trailing).offset(mSapce);
        make.centerY.mas_equalTo(0);
        make.height.and.width.mas_equalTo(35);
    }];
    [self.muteButton setImage:[UIImage imageNamed:@"ic_loud_speak_nor"] forState:UIControlStateNormal];
    [self.muteButton setImage:[UIImage imageNamed:@"ic_loud_speak"] forState:UIControlStateSelected];
    [self.muteButton addTarget:self action:@selector(onTouchSendMessageButton:) forControlEvents:UIControlEventTouchUpInside];

    self.micButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.micButton];
    self.micButton.tag = 3;
    self.micButton.selected = ZegoManager.micSelected;
    self.micButton.enabled = ZegoManager.micEnabled;
    self.micButton.hidden = ZegoManager.selfMicIndex ? NO : YES;
    [self.micButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.muteButton.mas_trailing).offset(mSapce);
        make.centerY.mas_equalTo(0);
        make.height.and.width.mas_equalTo(35);
    }];
    [self.micButton setImage:[UIImage imageNamed:@"ic_mic_nor"] forState:UIControlStateNormal];
    [self.micButton setImage:[UIImage imageNamed:@"ic_mic"] forState:UIControlStateSelected];
    [self.micButton addTarget:self action:@selector(onTouchSendMessageButton:) forControlEvents:UIControlEventTouchUpInside];

    self.functionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.functionButton.tag = 4;
    [self.functionButton addTarget:self action:@selector(onTouchSendMessageButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.functionButton setImage:[UIImage imageNamed:@"chatroom_function"] forState:UIControlStateNormal];
    [self addSubview:self.functionButton];
    [self.functionButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(ZegoManager.selfMicIndex ?self.micButton.mas_trailing : self.muteButton.mas_trailing).offset(mSapce);
        make.centerY.mas_equalTo(0);
        make.height.and.width.mas_equalTo(35);
    }];
    
}

- (void)resetButtonsLayout {
    CGFloat tSpace = (kScreenW - 38)/2.0f;
    CGFloat mSapce = 0;
    if (ZegoManager.selfMicIndex) {
        mSapce = (tSpace - 10 - 3 * 35)/ 3;
    }else{
        mSapce = (tSpace - 2 * 35)/ 3;
    }
    [self.muteButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.sendGroupGiftView.mas_trailing).offset(mSapce);
        make.height.and.width.mas_equalTo(35);
        make.centerY.mas_equalTo(0);
    }];
    self.micButton.hidden = !ZegoManager.selfMicIndex;
    [self.micButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.muteButton.mas_trailing).offset(mSapce);
        make.height.and.width.mas_equalTo(35);
        make.centerY.mas_equalTo(0);
    }];
    [self.functionButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(ZegoManager.selfMicIndex ?self.micButton.mas_trailing : self.muteButton.mas_trailing).offset(mSapce);
        make.height.and.width.mas_equalTo(35);
        make.centerY.mas_equalTo(0);
    }];
    [self layoutIfNeeded];
    [self layoutSubviews];
    
}

- (void)onTouchSendMessageButton: (UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(chatroomBottomBar:didTouchButton:)]){
        [self.delegate chatroomBottomBar:self didTouchButton:sender];
    }
}

- (void)touchSendGroupGift: (UITapGestureRecognizer*)tap {
    if (self.delegate && self.delegate && [self.delegate respondsToSelector:@selector(didTapChatroomBottomBarSendGroupGift)]) {
        [self.delegate didTapChatroomBottomBarSendGroupGift];
    }
}

@end
