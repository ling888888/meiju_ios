//
//  SPDChatRoomUserInfoView.h
//  SimpleDate
//
//  Created by Monkey on 2017/6/5.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SPDMagicModel;
@class HomeModel;

@protocol SPDChatRoomUserInfoViewDelegate <NSObject>

@optional

- (void)didClickLookUserInfoBtn:(NSString *)userId;
- (void)didSelectSend:(NSString *)type userId:(NSString *)userId avater:(NSString *)avater nickName:(NSString *)nickName gender:(NSString *)gender; // 废弃
- (void)didSelectAdmin:(NSString *)admin userId:(NSString *)userId index:(NSInteger)index nickName:(NSString *)nickName;
- (void)didSelectSend:(NSString *)type userModel:(HomeModel *)userModel;

@end

@interface SPDChatRoomUserInfoView : UIView

@property (nonatomic, copy) NSString *clan_id;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, assign) NSInteger adminValue;
@property (nonatomic, assign) NSInteger positionLevel;
@property (nonatomic, assign) NSInteger micIndex;
@property (nonatomic, assign) NSInteger micStatus;
@property (nonatomic, assign) BOOL isInChatRoom;
@property (nonatomic, weak) id<SPDChatRoomUserInfoViewDelegate> delegate;

+ (instancetype)chatRoomUserInfoView;

- (void)show;

- (void)dismiss;

@end
