//
//  SPDChatRoomMagicEffectView.m
//  SimpleDate
//
//  Created by Monkey on 2017/9/27.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDChatRoomMagicEffectView.h"
#import "SPDConversationViewController.h"
#import "SVGA.h"

@interface SPDChatRoomMagicEffectView ()<SVGAPlayerDelegate>

@property (nonatomic,strong)UIView *infoView;//名称显示区域

@property (nonatomic,strong)UILabel *sender_userNameLabel;

@property (nonatomic,strong)UILabel *receiver_userNameLabel;

@property (nonatomic,strong)UIImageView *sender_avatarImageView;

@property (nonatomic,strong)UIImageView *receive_avatarImageView;

@property (nonatomic,strong)UIImageView *presentImageView; //magic icon

@property (nonatomic,strong)UIImageView * contentImageView; //动画展示区域

@property (nonatomic,strong)NSMutableArray * effectDataArray;

@property (nonatomic,strong)UILabel * resultLabel;

@property (nonatomic,strong)UILabel * magicDescLabel; //魔法效果：对方魅力值增加123

@property (nonatomic,assign)BOOL isAnimating;

@property (nonatomic, strong) SVGAPlayer *player;
@property (nonatomic, strong) SVGAParser *parser;

@end


@implementation SPDChatRoomMagicEffectView

-(id)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        [self setupUIWithFrame:frame];
        
    }
    return self;
}

-(void)setupUIWithFrame:(CGRect )frame {
    
    self.userInteractionEnabled = NO;
    
    self.infoView = [[UIView alloc]init];
    [self addSubview:self.infoView];
    [self.infoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.centerX.equalTo(self.infoView.superview);
        make.width.mas_equalTo(frame.size.width);
        make.height.mas_equalTo(52);
    }];
    self.infoView.layer.cornerRadius = 26;
    self.infoView.clipsToBounds = YES;
    self.infoView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.55];
    self.infoView.userInteractionEnabled = NO;
    self.infoView.hidden = YES;
    self.sender_avatarImageView = [[UIImageView alloc]init];
    [self.infoView addSubview:self.sender_avatarImageView];
    self.sender_avatarImageView.layer.cornerRadius = 16;
    self.sender_avatarImageView.clipsToBounds = YES;
    [self.sender_avatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.infoView.mas_leading).offset(13);
        make.top.mas_equalTo(4);
        make.size.mas_equalTo(CGSizeMake(32, 32));
    }];
    self.sender_userNameLabel = [[UILabel alloc]init];
    [self.infoView addSubview:self.sender_userNameLabel];
    self.sender_userNameLabel.font = [UIFont systemFontOfSize:13];
    [self.sender_userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.sender_avatarImageView.mas_trailing).with.offset(5);
        make.centerY.equalTo(self.sender_avatarImageView);
    }];
    self.receive_avatarImageView = [[UIImageView alloc]init];
    [self.infoView addSubview:self.receive_avatarImageView];
    self.receive_avatarImageView.layer.cornerRadius = 16;
    self.receive_avatarImageView.clipsToBounds = YES;
    [self.receive_avatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.sender_userNameLabel.mas_trailing).offset(5);
        make.top.mas_equalTo(4);
        make.size.mas_equalTo(CGSizeMake(32, 32));
    }];
    
    self.receiver_userNameLabel = [[UILabel alloc]init];
    [self.infoView addSubview:self.receiver_userNameLabel];
    self.receiver_userNameLabel.font = [UIFont systemFontOfSize:13];
    [self.receiver_userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.receive_avatarImageView.mas_trailing).with.offset(5);
        make.centerY.equalTo(self.receive_avatarImageView);
    }];
    
    self.presentImageView = [[UIImageView alloc]init];
    self.presentImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.infoView addSubview:self.presentImageView];
    [self.presentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.receiver_userNameLabel.mas_trailing).with.offset(5);
        make.top.mas_equalTo(self.mas_top).offset(0);
        make.size.mas_equalTo(CGSizeMake(58, 34));
    }];
    
    self.resultLabel = [[UILabel alloc]init];
    self.resultLabel.textColor =[UIColor whiteColor];
    self.resultLabel.font = [UIFont systemFontOfSize:13];
    [self.infoView addSubview:self.resultLabel];
    [self.resultLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.presentImageView.mas_trailing).offset(4);
        make.centerY.equalTo(self.receiver_userNameLabel);
    }];
    self.magicDescLabel = [[UILabel alloc]init];
    self.magicDescLabel.textColor =SPD_HEXCOlOR(@"ffc12b");
    self.magicDescLabel.font = [UIFont systemFontOfSize:11];
    self.magicDescLabel.textAlignment = NSTextAlignmentCenter;
    [self.infoView addSubview:self.magicDescLabel];
    [self.magicDescLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.receive_avatarImageView.mas_bottom).offset(3);
        make.leading.equalTo(self.mas_leading).offset(0);
        make.height.mas_greaterThanOrEqualTo(11);
        make.width.mas_equalTo(frame.size.width);
    }];
    
    self.contentImageView = [[UIImageView alloc]init];
    [self addSubview:self.contentImageView];
    self.contentImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.contentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        //        make.top.equalTo(self.infoView.mas_bottom).offset(5);
        make.top.equalTo(self.mas_top).offset(60);
        make.leading.equalTo(self.mas_leading).offset(0);
        make.trailing.equalTo(self.mas_trailing).offset(0);
        make.height.mas_equalTo(230);
    }];
    
    self.player = [[SVGAPlayer alloc] init];
    self.player.contentMode = UIViewContentModeScaleAspectFill;
    self.player.loops = 1;
    self.player.clearsAfterStop = YES;
    self.player.delegate = self;
    [self addSubview:self.player];
    [self.player mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(60);
        make.leading.equalTo(self.mas_leading).offset(0);
        make.trailing.equalTo(self.mas_trailing).offset(0);
        make.height.mas_equalTo(230);
    }];
    self.parser = [[SVGAParser alloc] init];
}

- (void)setMagicMsg:(SPDCRSendMagicMessage *)magicMsg {
    dispatch_async(dispatch_get_main_queue(), ^{
        _magicMsg = magicMsg;
        [self.effectDataArray addObject:magicMsg];
        if (!self.isAnimating) {
            [self showGiftImagesEffectsWithMsg:magicMsg];
        };
    });
}

- (void)showGiftImagesEffectsWithMsg:(SPDCRSendMagicMessage *)magicMsg {
    self.isAnimating = YES;
    if (isVideoStatus || [magicMsg.is_cost isEqualToString:@"false"]) {
        self.infoView.hidden = NO;
        [self configUIWithPositionMessage:magicMsg];
    } else {
        self.infoView.hidden = YES;
    }
    [self giftStartAnimationWithGiftId:magicMsg.magic_id andResult:magicMsg.result];
}

- (void)configUIWithPositionMessage:(SPDCRSendMagicMessage *)message {
    [self.sender_avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:message.senderUserInfo.portraitUri]];
    if (message.senderUserInfo.name.length > 5) {
        if ([SPDCommonTool currentVersionIsArbic]) {
            self.sender_userNameLabel.text= [NSString stringWithFormat:@"%@%@",SPDStringWithKey(@"向", nil),[message.senderUserInfo.name substringToIndex:5]];
            
        }else{
            self.sender_userNameLabel.text= [NSString stringWithFormat:@"%@%@",[message.senderUserInfo.name substringToIndex:5],SPDStringWithKey(@"向", nil)];
        }
    }else {
        if ([SPDCommonTool currentVersionIsArbic]) {
            self.sender_userNameLabel.text= [NSString stringWithFormat:@"%@%@",SPDStringWithKey(@"向", nil),message.senderUserInfo.name];
            
        }else {
            self.sender_userNameLabel.text= [NSString stringWithFormat:@"%@%@",message.senderUserInfo.name,SPDStringWithKey(@"向", nil)];
        }
    }
    [self.sender_userNameLabel sizeToFit];
    [self.receive_avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:message.receive_avatar]];
    
    if (message.receive_nickname.length > 5) {
        self.receiver_userNameLabel.text= [NSString stringWithFormat:@"%@%@",[message.receive_nickname substringToIndex:5],SPDStringWithKey(@"使用", nil)];
    }else {
        self.receiver_userNameLabel.text =[NSString stringWithFormat:@"%@%@", message.receive_nickname,SPDStringWithKey(@"使用", nil)];
    }
    [self.receiver_userNameLabel sizeToFit];
    
    [self.presentImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:message.magic_url]];
    
    //result label
    if ([message.result isEqualToString:@"success"]) {
        NSMutableAttributedString * firstPart;
        if (message.receive_nickname.length > 5) {
            firstPart = [[NSMutableAttributedString alloc] initWithString:[message.receive_nickname substringToIndex:5]];
        }else{
            firstPart = [[NSMutableAttributedString alloc] initWithString:message.receive_nickname];
        }
        NSDictionary * firstAttributes;
        if ([message.receive_gender isEqualToString:@"male"]) {
            firstAttributes = @{ NSFontAttributeName:[UIFont boldSystemFontOfSize:11],NSForegroundColorAttributeName:[UIColor colorWithHexString:@"3c91f1"],};
        }else{
            firstAttributes = @{ NSFontAttributeName:[UIFont boldSystemFontOfSize:11],NSForegroundColorAttributeName:[UIColor colorWithHexString:@"fe69a1"],};
        }
        [firstPart setAttributes:firstAttributes range:NSMakeRange(0,firstPart.length)];
        NSMutableAttributedString * secondPart = [[NSMutableAttributedString alloc] initWithString:[self configDescription]];
        NSDictionary * secondeAttribute = @{ NSFontAttributeName:[UIFont boldSystemFontOfSize:11],NSForegroundColorAttributeName:[UIColor colorWithHexString:@"ffc12b"],};
        [secondPart setAttributes:secondeAttribute range:NSMakeRange(0,secondPart.length)];
        [firstPart appendAttributedString:secondPart];
        self.magicDescLabel.attributedText = firstPart;
        
        self.magicDescLabel.hidden = NO;
        self.resultLabel.text = SPDStringWithKey(@"成功", nil);
    }else{
        self.resultLabel.text = SPDStringWithKey(@"失败", nil);
        self.magicDescLabel.hidden = YES;
    }
    if ([message.send_gender isEqualToString:@"female"]) {
        self.sender_userNameLabel.textColor = SPD_HEXCOlOR(@"fe69a1");
    }else{
        self.sender_userNameLabel.textColor = SPD_HEXCOlOR(@"3c91f1");
    }
    if ([message.receive_gender isEqualToString:@"female"]) {
        self.receiver_userNameLabel.textColor = SPD_HEXCOlOR(@"fe69a1");
    }else{
        self.receiver_userNameLabel.textColor = SPD_HEXCOlOR(@"3c91f1");
    }
    
    //reset autolayout
    
    [self layoutIfNeeded];
    //执行了 layout 之后CGRectGetMaxX 这个才有效
    if ([SPDCommonTool currentVersionIsArbic]) {
        [self.infoView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(CGRectGetMaxX(self.sender_avatarImageView.frame)+13);
        }];
    }else {
        [self.infoView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(CGRectGetMaxX(self.resultLabel.frame)+13);
        }];
    }
    [self.infoView layoutIfNeeded];
}


-(void)giftStartAnimationWithGiftId:(NSString *)gift_id andResult:(NSString *)result {
    
//    //检查沙河目录下有没有这个文件
//#warning message hl
//    NSString * giftImagePath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"/Library/Caches/magic_%@",result]];
//    if ([[NSFileManager defaultManager] fileExistsAtPath:giftImagePath]) {
//        //判断有多少个文件
//        NSArray *tempFileList = [[NSArray alloc] initWithArray:[[NSFileManager defaultManager] contentsOfDirectoryAtPath:giftImagePath error:nil]];
//        NSLog(@"document magic 下有 %ld",tempFileList.count);
//        int image_count = 0;
//        for (NSString * str in tempFileList) {
//            if ([str containsString:gift_id]) {
//                image_count++;
//            }
//        }
//        NSLog(@"image count is %d",image_count);
//        NSMutableArray *array = [NSMutableArray array];
//        for (NSInteger i = 0; i < image_count; i++) {
//            NSString *path = [giftImagePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_%ld.png",gift_id,i]];
////            NSLog(@"image path = %@",path);
//            if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
//                UIImage * img = [UIImage imageWithContentsOfFile:path];
//                [array addObject:img];
//            }
//        }
//        self.contentImageView.animationImages = array;
//        self.contentImageView.animationDuration = array.count * 0.1;
//        self.contentImageView.animationRepeatCount = 0;
//        [self.contentImageView startAnimating];
//        [self performSelector:@selector(giftStopAnimation) withObject:nil afterDelay:array.count * 0.1];
//    }
    
    [self.parser parseWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://%@famy.17jianyue.cn/svga/%@_%@.svga", DEV ? @"dev-" : @"", gift_id, result]]
              completionBlock:^(SVGAVideoEntity * _Nullable videoItem) {
                  if (videoItem != nil) {
                      self.player.videoItem = videoItem;
                      [self.player startAnimation];
                  } else {
                      [self giftStopAnimation];
                  }
              } failureBlock:^(NSError * _Nullable error) {
                  [self giftStopAnimation];
              }];
}

- (NSString *)configDescription{
    
    NSString * str ;
    NSString * effect = [self.magicMsg.magic_effect isEqualToString:@"inc"] ? @"增加":@"减少";
    if ([self.magicMsg.type isEqualToString:@"charm"] || [self.magicMsg.type isEqualToString:@"exp"]) {
        if ([self.magicMsg.type isEqualToString:@"charm"]) {
            NSString * temp =[NSString stringWithFormat:@"%@魅力值",effect];
            str =[NSString stringWithFormat:@"%@%@",SPDStringWithKey(temp, nil),self.magicMsg.magic_effect_value] ;
        }else{
            NSString * temp = [NSString stringWithFormat:@"%@经验值",effect];
            str =[NSString stringWithFormat:@"%@%@",SPDStringWithKey(temp, nil),self.magicMsg.magic_effect_value] ;
        }
    }else{
        if ([self.magicMsg.type isEqualToString:@"banned_talk"]) {
            str = [NSString stringWithFormat:@"%@%@s",SPDStringWithKey(@"被禁言", nil),self.magicMsg.magic_effect_value];
        }else if ([self.magicMsg.type isEqualToString:@"banned_mic"]){
            str = [NSString stringWithFormat:@"%@%@s",SPDStringWithKey(@"被封麦", nil),self.magicMsg.magic_effect_value];
        }else if ([self.magicMsg.type isEqualToString:@"kicked"]){
            str = [NSString stringWithFormat:@"%@s%@",self.magicMsg.magic_effect_value,SPDStringWithKey(@"内无法进入被踢出的房间", nil)];
        }else{
            str = @"被禁言";
        }
    }
    return str;
}

- (void)giftStopAnimation {
    self.infoView.hidden = YES;
    if (self.effectDataArray.count > 0) {
        [self.effectDataArray removeObjectAtIndex:0];
        if (self.effectDataArray.count > 0) {
            [self showGiftImagesEffectsWithMsg:self.effectDataArray.firstObject];
        } else {
            self.isAnimating = NO;
        }
    } else {
        self.isAnimating = NO;
    }
}

#pragma mark - SVGAPlayerDelegate

- (void)svgaPlayerDidFinishedAnimation:(SVGAPlayer *)player {
    [self giftStopAnimation];
}

#pragma mark - Getters

- (NSMutableArray *)effectDataArray {
    if (!_effectDataArray) {
        _effectDataArray = [NSMutableArray array];
    }
    return _effectDataArray;
}

@end

