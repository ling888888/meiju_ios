//
//  SPDChatRoomEffectImageView.h
//  SimpleDate
//
//  Created by Monkey on 2017/7/12.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SPDChatRoomSendPresentMessage,SPDWorldChatMessage;

@interface SPDChatRoomEffectImageView : UIView

@property (nonatomic, strong) RCMessageContent * message;

@end
