//
//  SPDChatroomTitleView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/1/3.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@class FamilyModel,SPDChatroomTitleView;

@protocol SPDChatroomTitleViewDelegate <NSObject>

@optional
- (void)chatroomTitleView:(SPDChatroomTitleView *)titleView didClickedFollowBtn:(UIButton *)sender;
- (void)didClickedFamilyInfo;

@end

@interface SPDChatroomTitleView : UIView

@property (nonatomic, strong) NSDictionary * familyDict;
@property (nonatomic, weak)id<SPDChatroomTitleViewDelegate>delegate;

- (void)followStatusChangedTo:(BOOL)isFollowing;

@end

NS_ASSUME_NONNULL_END
