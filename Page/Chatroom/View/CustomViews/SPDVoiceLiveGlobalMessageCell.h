//
//  SPDVoiceLiveGlobalMessageCell.h
//  SimpleDate
//
//  Created by 李楠 on 17/5/31.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPDVoiceLiveGlobalMessageCell : UIView

@property (nonatomic, copy) NSDictionary *dic;

@end
