//
//  KKGiftGroupUsersCell.m
//  SimpleDate
//
//  Created by ling Hou on 2019/12/30.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "KKGiftGroupUsersCell.h"
#import "SPDVoiceLiveUserModel.h"
#import "ZegoKitManager.h"

@interface KKGiftGroupUsersCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *onMicImageView;

@end

@implementation KKGiftGroupUsersCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setUserModel:(SPDVoiceLiveUserModel *)userModel {
    _userModel = userModel;
    [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _userModel.avatar]]];
    NSArray *micUserIds = [[ZegoManager.micUsers allValues] valueForKeyPath:@"@unionOfObjects._id"];
    self.onMicImageView.hidden = ![micUserIds containsObject:_userModel._id];
}

@end
