//
//  KKGiftGivingGiftCell.m
//  KaKa
//
//  Created by 李楠 on 2019/11/5.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "KKGiftGivingGiftCell.h"
#import "GiftListModel.h"

@interface KKGiftGivingGiftCell ()

@property (weak, nonatomic) IBOutlet UIImageView *currencyImageView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *charmLabel;
@property (weak, nonatomic) IBOutlet UIImageView *markerImageView;

@end

@implementation KKGiftGivingGiftCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(GiftListModel *)model {
    _model = model;
    if (_model._id.notEmpty) {
        self.hidden = NO;
        [self.giftImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,_model.cover]]];
        self.currencyImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_icon_8", _model.currencyType]];
        self.priceLabel.text = _model.cost.stringValue;
        self.charmLabel.text = _model.charm.stringValue;
      
        if ([_model.rewardType isEqualToString:@"gold"]) {
            self.markerImageView.hidden = NO;
            self.markerImageView.image = [UIImage imageNamed:@"gift_flag_gold"];
        } else if ([_model.rewardType isEqualToString:@"lottery"]) {
            self.markerImageView.hidden = NO;
            self.markerImageView.image = [UIImage imageNamed:@"gift_flag_lucky"];
        }
        if(![SPDCommonTool isEmpty:_model.limit_value]) {
             self.markerImageView.hidden = NO;
            self.markerImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"medal_%@", _model.limit_value]];
        }
       
        if (!_model.rewardType.notEmpty && !_model.limit_value.notEmpty) {
            self.markerImageView.hidden = YES;
        }
        
    } else {
        self.hidden = YES;
    }
}

- (void)setCellSelected:(BOOL)cellSelected {
    _cellSelected = cellSelected;
    self.layer.borderWidth = _cellSelected ? 1.5 : 0;
    if (_cellSelected) {
        [self heartAnimation];
    }
}

- (void)heartAnimation{
    float bigSize = 1.1;
    CABasicAnimation *heart = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    heart.duration = 0.5;
    heart.toValue = [NSNumber numberWithFloat:bigSize];
    heart.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    heart.autoreverses = YES;
    heart.repeatCount = FLT_MAX;
    [self.giftImageView.layer addAnimation:heart forKey:@"transform.scale"];
}

@end
