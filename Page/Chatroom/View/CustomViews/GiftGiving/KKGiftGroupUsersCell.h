//
//  KKGiftGroupUsersCell.h
//  SimpleDate
//
//  Created by ling Hou on 2019/12/30.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class SPDVoiceLiveUserModel;

@interface KKGiftGroupUsersCell : UICollectionViewCell

@property (nonatomic, strong) SPDVoiceLiveUserModel * userModel;
@property (weak, nonatomic) IBOutlet UIView *borderView;

@end

NS_ASSUME_NONNULL_END
