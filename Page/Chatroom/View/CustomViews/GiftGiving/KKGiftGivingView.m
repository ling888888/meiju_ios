//
//  KKGiftGivingView.m
//  KaKa
//
//  Created by 李楠 on 2019/11/5.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "KKGiftGivingView.h"
#import "KKGiftGivingTypeCell.h"
#import "KKGiftGivingGiftCell.h"
#import "SPDVoiceLiveUserModel.h"
#import "GiftListModel.h"
#import "SPDVehicleModel.h"
#import "PresentMessage.h"
#import "ZegoKitManager.h"
#import "KKGiftGivingVehicleCell.h"
#import "SPDMagicModel.h"
#import "MagicCVCell.h"
#import "KKGiftGroupUsersCell.h"
#import "SPDChatRoomSendPresentMessage.h"
#import "SPDWorldChatMessage.h"
#import "HomeModel.h"
#import "SPDConversationViewController.h"
#import "MarqueeLabel.h"
#import "NSString+XXWAddition.h"


@interface KKGiftGivingView ()<UIGestureRecognizerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewheight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descViewBottom;
@property (weak, nonatomic) IBOutlet UICollectionView *typeCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *giftCollectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIButton *numButton;
@property (weak, nonatomic) IBOutlet UIImageView *currencyImageView;
@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;
@property (weak, nonatomic) IBOutlet UIView *numBackgroundView;
@property (weak, nonatomic) IBOutlet UITableView *numTableView;
@property (weak, nonatomic) IBOutlet UIView *descView;
@property (weak, nonatomic) IBOutlet UIImageView *descIconImageView;
@property (weak, nonatomic) IBOutlet MarqueeLabel *descLabel;
@property (weak, nonatomic) IBOutlet UIView *groupUsersView;
@property (weak, nonatomic) IBOutlet UIButton *chooseGroupButton;
@property (weak, nonatomic) IBOutlet UILabel *groupUsersCountLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *groupUsersCollectionView;
@property (weak, nonatomic) IBOutlet UITableView *chooseGroupTypeTableView;
@property (weak, nonatomic) IBOutlet UIView *chooseGroupTypeBgView;
@property (weak, nonatomic) IBOutlet UIButton *sendGiftButton;


@property (nonatomic, strong) NSMutableArray *typeArray;
@property (nonatomic, assign) NSInteger giftSelectedIndex;
@property (nonatomic, strong) NSMutableArray<GiftListModel *> *goldGiftArray;
@property (nonatomic, strong) NSMutableArray<GiftListModel *> *diamondGiftArray;
@property (nonatomic, strong) NSMutableArray<GiftListModel *> *nobleGiftArray;
@property (nonatomic, strong) NSMutableArray<SPDVehicleModel *> *vehicleArray;
@property (nonatomic, strong) NSMutableArray<SPDMagicModel *> *magicArray;

@property (nonatomic, copy) NSString *num;
@property (nonatomic, copy) NSArray *numArray;
@property (nonatomic, copy) NSString *goldBalance;
@property (nonatomic, copy) NSString *diamondBalance;
@property (nonatomic, strong) NSMutableArray * allGroupTypesArray;
@property (nonatomic, strong) NSMutableArray * selectedUsersArray; // 选择的要送的人
@property (nonatomic, assign) NSInteger typeSelectedIndex;

@end

@implementation KKGiftGivingView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.contentViewheight.constant = 315 + IphoneX_Bottom + 10;
    self.contentViewBottom.constant = -self.contentViewheight.constant - 56 - 63;
    self.chooseGroupTypeTableView.tableFooterView = [UIView new];
    
    [self.typeCollectionView registerNib:[UINib nibWithNibName:@"KKGiftGivingTypeCell" bundle:nil] forCellWithReuseIdentifier:@"KKGiftGivingTypeCell"];
    [self.giftCollectionView registerNib:[UINib nibWithNibName:@"KKGiftGivingGiftCell" bundle:nil] forCellWithReuseIdentifier:@"KKGiftGivingGiftCell"];
    [self.giftCollectionView registerNib:[UINib nibWithNibName:@"KKGiftGivingVehicleCell" bundle:nil] forCellWithReuseIdentifier:@"KKGiftGivingVehicleCell"];
    [self.giftCollectionView registerNib:[UINib nibWithNibName:@"MagicCVCell" bundle:nil] forCellWithReuseIdentifier:@"MagicCVCell"];
    [self.groupUsersCollectionView registerNib:[UINib nibWithNibName:@"KKGiftGroupUsersCell" bundle:nil] forCellWithReuseIdentifier:@"KKGiftGroupUsersCell"];
    self.typeArray = [NSMutableArray arrayWithArray:@[SPDLocalizedString(@"金币"), SPDLocalizedString(@"钻石"),SPDLocalizedString(@"座驾"),SPDLocalizedString(@"贵族"),SPDLocalizedString(@"魔法")]];
    self.chooseGroupButton.selected = NO;
    self.num = @"1";
    self.numArray = @[@"1", @"7", @"17", @"77", @"177", @"777"];
    [self requestShowBacth];
    [self requestMyBalance];
}

- (void)dealloc {
    NSLog(@"dealloc");
}

#pragma mark - Event responses

- (IBAction)tapBackground:(UITapGestureRecognizer *)sender {
    [self hide];
}

- (IBAction)clickGiveButton:(UIButton *)sender {
    if (self.selectedUsersArray.count < 1 && self.isMutableGift) {
      [self showTips:SPDStringWithKey(@"请至少选择1个人", nil)];
      return;
    }
    if (self.giftSelectedIndex >= 0) {
        if (self.isMutableGift && self.selectedUsersArray.count == 1) {
            SPDVoiceLiveUserModel * model = self.selectedUsersArray[0];
            self.receiveGiftUserModel = [[HomeModel alloc]init];
            self.receiveGiftUserModel._id = model._id;
            self.receiveGiftUserModel.avatar = model.avatar;
            self.receiveGiftUserModel.nick_name = model.nick_name;
            self.receiveGiftUserModel.gender = model.gender;
        }
        switch (self.typeSelectedIndex) {
            case 0:
                [self requestGiftSendToWithModel:self.goldGiftArray[self.giftSelectedIndex]];
                break;
            case 1:
                [self requestGiftSendToWithModel:self.diamondGiftArray[self.giftSelectedIndex]];
                break;
            case 2:
                [self requestVehicleSendToWithModel:self.vehicleArray[self.giftSelectedIndex]];
               break;
            case 3:
                [self requestGiftSendToWithModel:self.nobleGiftArray[self.giftSelectedIndex]];
                break;
            case 4:{
                if (![[SPDApiUser currentUser].lang isEqualToString:self.receiveGiftUserModel.lang]) {
                    [self showTips:SPDStringWithKey(@"对方和你不在同一语言区，不能跨区使用魔法", nil)];
                    break;
                }
                [self requestMagicBuy:self.magicArray[self.giftSelectedIndex]];
                break;
            }
            default:
                break;
        }
        [self hide];
    }
}

- (IBAction)clickNumButton:(UIButton *)sender {
    self.numBackgroundView.hidden = NO;
    [self bringSubviewToFront:self.numBackgroundView];
}

- (IBAction)tapNumBackgroundView:(UITapGestureRecognizer *)sender {
     self.numBackgroundView.hidden = YES;
}

- (IBAction)tapChooseGroupBackgroundView:(UITapGestureRecognizer *)sender {
    self.chooseGroupTypeBgView.hidden = YES;
    self.chooseGroupButton.selected = !self.chooseGroupButton.selected;
}

- (IBAction)clickRechargeButton:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickRechargeButton)]) {
        [self.delegate didClickRechargeButton];
    }
}

- (IBAction)clickedChooseGroupsUsersButton:(UIButton *)sender {
    sender.selected = !sender.selected;
    // 弹出选择框
    if (sender.selected) {
        [self.chooseGroupTypeTableView reloadData];
        self.chooseGroupTypeBgView.hidden = NO;
        [self bringSubviewToFront:self.chooseGroupTypeBgView];
    }
}

#pragma mark - Private methods

- (void)requestShowBacth {
    [MBProgressHUD showHUDAddedTo:self animated:YES];
    NSDictionary *params = @{@"type": @"gift,vehicle,magic,noble_gift,jewelGift"};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:params] bURL:@"show.batch" bAnimated:NO bAnimatedViewController: nil bSuccessDoSomething:^(id  _Nullable data) {
        
        if (data[@"gift"]) {
            [self.goldGiftArray removeAllObjects];
            for (NSDictionary *dic in data[@"gift"]) {
                GiftListModel *model = [GiftListModel initWithDictionary:dic];
                model.currencyType = @"gold";
                [self.goldGiftArray addObject:model];
            }
            while (self.goldGiftArray.count % 8 != 0) {
                [self.goldGiftArray addObject:[GiftListModel new]];
            }
        }
        if (data[@"jewelGift"]) {
            [self.diamondGiftArray removeAllObjects];
            for (NSDictionary *dic in data[@"jewelGift"]) {
                GiftListModel *model = [GiftListModel initWithDictionary:dic];
                model.currencyType = @"diamond";
                [self.diamondGiftArray addObject:model];
            }
            while (self.diamondGiftArray.count % 8 != 0) {
                [self.diamondGiftArray addObject:[GiftListModel new]];
            }
        }
        if (data[@"noble_gift"]) {
            [self.nobleGiftArray removeAllObjects];
            for (NSDictionary *dict in data[@"noble_gift"]) {
                GiftListModel *model = [GiftListModel initWithDictionary:dict];
                model.currencyType = @"gold";
                [self.nobleGiftArray addObject:model];
            }
            while (self.nobleGiftArray.count % 8 != 0) {
                [self.nobleGiftArray addObject:[GiftListModel new]];
            }
        }
        if (data[@"vehicle"]) {
            [self.vehicleArray removeAllObjects];
            for (NSDictionary *dic in data[@"vehicle"]) {
                SPDVehicleModel *model = [SPDVehicleModel initWithDictionary:dic];
                [self.vehicleArray addObject:model];
            }
            while (self.vehicleArray.count % 8 != 0) {
                [self.vehicleArray addObject:[SPDVehicleModel new]];
            }
        }
        if (data[@"magic"]) {
            [self.magicArray removeAllObjects];
            for (NSDictionary *dic in data[@"magic"]) {
                SPDMagicModel *model = [SPDMagicModel initWithDictionary:dic];
                [self.magicArray addObject:model];
            }
            while (self.magicArray.count % 8 != 0) {
                [self.magicArray addObject:[SPDMagicModel new]];
            }
        }
        [self show];
        if(self.selectedIndex){
            [self setTypeSelectedIndex:self.selectedIndex];
            [self.typeCollectionView reloadData];
        }
    } bFailure:^(id  _Nullable failure) {
        [MBProgressHUD hideHUDForView:self animated:YES];
        [self removeFromSuperview];
    }];

}

- (void)requestMyBalance {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.balance" bAnimated:NO bAnimatedViewController:nil bSuccessDoSomething:^(id  _Nullable data) {
        self.goldBalance = [data[@"balance"] stringValue];
        self.diamondBalance = [data[@"jewelBalance"] stringValue];
        self.balanceLabel.text = self.typeSelectedIndex == 1 ? self.diamondBalance : self.goldBalance;
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)show {
    [MBProgressHUD hideHUDForView:self animated:YES];
    self.typeSelectedIndex = self.typeSelectedIndex;
    [self layoutIfNeeded];
    
    [UIView animateWithDuration:0.25 animations:^{
        self.contentViewBottom.constant = -10;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)hide {
    [UIView animateWithDuration:0.25 animations:^{
        self.contentViewBottom.constant = -self.contentViewheight.constant - 56 - 63;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)configDescriptionForDescView {
    NSString * desc;
    switch (self.typeSelectedIndex) {
        case 0:{
            GiftListModel * model = self.goldGiftArray[self.giftSelectedIndex];
            self.descView.hidden = !model.popdesc.notEmpty;
            desc = model.popdesc;
            self.descIconImageView.image = [UIImage imageNamed:@"gift_icon_desc"];
            break;
        }
        case 1:{
            GiftListModel * model = self.diamondGiftArray[self.giftSelectedIndex];
            self.descView.hidden = !model.popdesc.notEmpty;
            desc =  model.popdesc;
            self.descIconImageView.image = [UIImage imageNamed:@"gift_icon_desc"];
            break;
        }
        case 2:{
            SPDVehicleModel * model = self.vehicleArray[self.giftSelectedIndex];
            self.descView.hidden = !model.popdesc.notEmpty;
            desc =  model.popdesc;
            self.descIconImageView.image = [UIImage imageNamed:@"gift_icon_desc"];
            break;
        }
        case 3:{
           GiftListModel * model = self.nobleGiftArray[self.giftSelectedIndex];
           self.descView.hidden = !model.popdesc.notEmpty;
           desc =  model.popdesc;
           self.descIconImageView.image = [UIImage imageNamed:@"gift_icon_desc"];
           break;
        }
        case 4:{
            SPDMagicModel * model = self.magicArray[self.giftSelectedIndex];
            self.descView.hidden = !model.popdesc.notEmpty;
            desc =  model.popdesc;
            self.descIconImageView.image = [UIImage imageNamed:@"magic_icon_desc"];
            break;
        }
        default:
            break;
    }
    CGSize size = [NSString sizeWithString:desc andFont:[UIFont boldSystemFontOfSize:13] andMaxSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    self.descLabel.text = desc;
    if (size.width >= kScreenW - 100) {
        [self.descLabel restartLabel];
    }
}

- (NSString *)getReceiveUserName {
    if ([self.chooseGroupType isEqualToString:@"all"]) {
        return  (self.selectedUsersArray.count == self.groupsUsers.count ) ? SPDStringWithKey(@"房间内每个人", nil): [NSString stringWithFormat:SPDLocalizedString(@"房间内%@人"),@(self.selectedUsersArray.count)];
    }else if ([self.chooseGroupType isEqualToString:@"allfelmale"]){
        return  (self.selectedUsersArray.count == self.groupsUsers.count ) ? SPDStringWithKey(@"房间内每个女用户", nil): [NSString stringWithFormat:SPDLocalizedString(@"房间内%@个女用户"),@(self.selectedUsersArray.count)];
    }else if ([self.chooseGroupType isEqualToString:@"allmale"]){
        return  (self.selectedUsersArray.count == self.groupsUsers.count )? SPDStringWithKey(@"房间内每个男用户", nil): [NSString stringWithFormat:SPDLocalizedString(@"房间内%@个男用户"),@(self.selectedUsersArray.count)];
    }else if ([self.chooseGroupType isEqualToString:@"mic"]){
        return  (self.selectedUsersArray.count == self.groupsUsers.count )? SPDStringWithKey(@"麦上每个人", nil): [NSString stringWithFormat:SPDStringWithKey(@"麦上%@人", nil),@(self.selectedUsersArray.count)];
    }else if ([self.chooseGroupType isEqualToString:@"micfemale"]){
        return  (self.selectedUsersArray.count == self.groupsUsers.count )? SPDStringWithKey(@"麦上每个女用户", nil): [NSString stringWithFormat:SPDLocalizedString(@"麦上%@个女用户"),@(self.selectedUsersArray.count)];
    }else{
        return  (self.selectedUsersArray.count == self.groupsUsers.count ) ?SPDStringWithKey(@"麦上每个男用户", nil): [NSString stringWithFormat:SPDLocalizedString(@"麦上%@个男用户"),@(self.selectedUsersArray.count)];
    }
}

- (void)requestGiftSendToWithModel:(GiftListModel *)model {
    model.num = self.num;
    NSMutableDictionary * params = [@{@"num": model.num} mutableCopy];
    [params setValue:model._id forKey:@"gift_id"];
    NSString * url;
    if ((self.isMutableGift && self.selectedUsersArray.count == 1) || !self.isMutableGift) {
        [params setValue:self.receiveGiftUserModel._id forKey:@"user_id"];
        if (ZegoManager.clan_id.notEmpty && !isVideoStatus && !isInPrivateConversation) {
            [params setValue:ZegoManager.sceneType == SceneTypeRadioRoom ? @"radio_chatroom" : @"chatroom" forKey:@"scene_type"];
            if (ZegoManager.sceneType != SceneTypeRadioRoom) {
                [params setValue:ZegoManager.clan_id forKey:@"scene_id"];
            }
        }
        url = @"gift.send.to";
    }else{
        NSArray * userIds = [self.selectedUsersArray valueForKeyPath:@"@unionOfObjects._id"];
        [params setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:userIds options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding] forKey:@"user_id"];
        [params setValue:self.chooseGroupType forKey:@"receiveUserType"];
        [params setValue:[NSString stringWithFormat:@"%@",@(self.groupsUsers.count)]  forKey:@"sumSel"];
        [params setValue:ZegoManager.clan_id forKey:@"clan_id"];
        url = @"group.gift.send.to";
    }

    [RequestUtils commonPostRequestUtils:params bURL:url bAnimated:NO bAnimatedViewController:nil bSuccessDoSomething:^(id  _Nullable suceess) {
        if ((self.isMutableGift && self.selectedUsersArray.count == 1) || !self.isMutableGift) {
            [self sendPrivateGiftMessageWithModel:model];// 发私信消息
        }else{
            [self showTips:SPDStringWithKey(@"礼物发送成功", nil)];
        }
        if (ZegoManager.clan_id.notEmpty && !isVideoStatus && !isInPrivateConversation) {
            if ([self.delegate respondsToSelector:@selector(giftGivingView:prepareComboWithURL:params:)]) {
                [self.delegate giftGivingView:self prepareComboWithURL:url params:params];
            }
            model.send_msg_world = [suceess[@"send_msg_world"] boolValue];
            model.send_all_chatroom = [suceess[@"send_all_chatroom"] boolValue];
            [self sendChatRoomGiftMessageWithModel:model];
        }
    } bFailure:^(id  _Nullable error) {
        if ([error[@"code"] integerValue] ==  2002) {
            [self presentAlertWithTitle:SPDLocalizedString(@"余额不足，请充值") message:nil cancelTitle:SPDLocalizedString(@"取消") cancelHandler:nil actionTitle:SPDLocalizedString(@"充值") actionHandler:^(UIAlertAction * _Nonnull action) {
                if ([self.delegate respondsToSelector:@selector(didClickRechargeButton)]) {
                    [self.delegate didClickRechargeButton];
                }
            }];
        } else {
            [self showTips:error[@"msg"]];
        }
    }];
}

//送座驾扣费------
- (void)requestVehicleSendToWithModel:(SPDVehicleModel *)vehicle_Model {
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setValue:vehicle_Model.vehicle_id forKey:@"vehicle_id"];
     if (isVideoStatus) {// 在聊天室中 私聊某人在视频中
       [dic setValue:@"person" forKey:@"scene_type"];
     }
    if (ZegoManager.clan_id.notEmpty) {
        [dic setValue:ZegoManager.sceneType == SceneTypeRadioRoom ? @"radio_chatroom" : @"chatroom" forKey:@"scene_type"];
        [dic setValue:ZegoManager.clan_id forKey:@"scene_id"];
    }
    NSString * url;
    if (!self.isMutableGift || (self.isMutableGift && self.selectedUsersArray.count == 1)) {
        url = @"vehicle.buy";
        [dic setValue:self.receiveGiftUserModel._id forKey:@"user_id"];
    }else{
        url = @"group.vehicle.to";
        NSArray * userIds = [self.selectedUsersArray valueForKeyPath:@"@unionOfObjects._id"];
        if (userIds.count < 1) {
            [self showTips:SPDStringWithKey(@"请至少选择1个人", nil)];
            return;
        }
        [dic setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:userIds options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding] forKey:@"user_id"];
        [dic setValue:self.chooseGroupType forKey:@"receiveUserType"];
        [dic setValue:[NSString stringWithFormat:@"%@",@(self.groupsUsers.count)]  forKey:@"sumSel"];
        [dic setValue:ZegoManager.clan_id forKey:@"clan_id"];
    }
    [RequestUtils commonPostRequestUtils:dic bURL:url bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        [vehicle_Model setExpire_time:suceess[@"expire_time"]];
        if (!self.isMutableGift || (self.isMutableGift && self.selectedUsersArray.count == 0)) {
            [self sendPrivateVehicleMessageWithModel:vehicle_Model];
        }
        if (ZegoManager.clan_id.notEmpty) {
            if (ZegoManager.sceneType == SceneTypeRadioRoom) {
                [self sendRadioRoomVehicleMessageWithModel:vehicle_Model];
            } else {
                [self sendVehicleMessageInGlobalChatroom:vehicle_Model];
            }
        }
        
    } bFailure:^(id  _Nullable failure) {
        if ([failure[@"code"] integerValue] == 203 ) {
             if (isVideoStatus) {
                [self showTips:SPDStringWithKey(@"金币不足，请充值", nil)];
            }else{
                [self presentAlertWithTitle:SPDLocalizedString(@"余额不足，请充值") message:nil cancelTitle:SPDLocalizedString(@"取消") cancelHandler:nil actionTitle:SPDLocalizedString(@"充值") actionHandler:^(UIAlertAction * _Nonnull action) {
                    if ([self.delegate respondsToSelector:@selector(didClickRechargeButton)]) {
                        [self.delegate didClickRechargeButton];
                    }
                }];
            }
        }else{
            [self showTips:failure[@"msg"]];
        }
    }];
}

- (void)requestMagicBuy:(SPDMagicModel *)magicModel {
    if ([self.receiveGiftUserModel.user_id isEqualToString:[SPDApiUser currentUser].userId]) {
        [self showTips:SPDStringWithKey(@"不可以对自己使用魔法", nil)];
        return;
    }
    NSMutableDictionary * dict = [NSMutableDictionary new];
    [dict setObject:self.receiveGiftUserModel._id forKey:@"user_id"];
    [dict setObject:magicModel._id forKey:@"magic_id"];
    if (ZegoManager.clan_id) {
        [dict setObject:ZegoManager.clan_id forKey:@"chatroom_id"];
    }
    [RequestUtils commonPostRequestUtils:dict bURL:@"magic.send.to" bAnimated:YES bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        [self sendPrivateMagicMessageWithModel:magicModel andDict:suceess];
    } bFailure:^(id  _Nullable failure) {
        if ([failure isKindOfClass:[NSDictionary class]]) {
            if ([failure[@"code"] integerValue] == 2002) {
                if (isVideoStatus) {
                    [self showTips:SPDStringWithKey(@"金币不足，请充值", nil)];
                }else{
                   [self presentAlertWithTitle:SPDLocalizedString(@"余额不足，请充值") message:nil cancelTitle:SPDLocalizedString(@"取消") cancelHandler:nil actionTitle:SPDLocalizedString(@"充值") actionHandler:^(UIAlertAction * _Nonnull action) {
                       if ([self.delegate respondsToSelector:@selector(didClickRechargeButton)]) {
                           [self.delegate didClickRechargeButton];
                       }
                   }];
                }
            }else{
                [self showTips:failure[@"msg"]];
            }
        }
    }];
        
}

- (void)sendPrivateMagicMessageWithModel:(SPDMagicModel *)magicModel andDict:(NSDictionary *) dict {
    SPDPrivateMagicMessage *magicMsg = [SPDCommonTool getPrivateMagicMessageWithDict:dict andMagicModel:magicModel andReceiveId:self.receiveGiftUserModel._id] ;
    [[RCIM sharedRCIM] sendMessage:ConversationType_PRIVATE targetId:self.receiveGiftUserModel._id content:magicMsg pushContent:magicMsg.conversationDigest pushData:magicMsg.conversationDigest success:^(long messageId) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showTips:SPDStringWithKey(@"魔法使用成功", nil)];
            if (!isVideoStatus) {
                NSMutableDictionary * dict1 = [NSMutableDictionary dictionaryWithDictionary:dict];
                if ([magicModel.num integerValue] > 0) {
                    [dict1 setObject:@(0) forKey:@"is_cost"];//标志次魔法是否使用道具 yes
                }else{
                    [dict1 setObject:@(1) forKey:@"is_cost"];//标志次魔法是否使用道具 yes
                }
                //做动画
                if (ZegoManager.clan_id && self.delegate && [self.delegate respondsToSelector:@selector(giftGivingView:didSendMagicModel:andDict:)]) {
                    [self.delegate giftGivingView:self didSendMagicModel:magicModel andDict:dict1];
                }
                // 2. 如果是花钱的魔法 需要发送世界消息
                
            }
        });
    } error:^(RCErrorCode nErrorCode, long messageId) {
        
    }];
}

- (void)sendPrivateGiftMessageWithModel:(GiftListModel *)model {
    PresentMessage *message = [PresentMessage new];
    message.content = SPDLocalizedString(@"送给你一份礼物希望你会喜欢");
    message._id = model._id;
    message.giftName = model.name;
    message.imageName = model.cover;
    message.goldNum = [NSString stringWithFormat:@"%ld", model.cost.integerValue * model.num.integerValue];
    message.charamScore = [NSString stringWithFormat:@"%ld", model.charm.integerValue * model.num.integerValue];
    message.num = model.num;
    message.currencyType = [model.currencyType isEqualToString:@"gold"] ? 0 : 1;
    [[RCIM sharedRCIM] sendMessage:ConversationType_PRIVATE targetId:self.receiveGiftUserModel._id content:message pushContent:SPDLocalizedString(@"送给你一份礼物希望你会喜欢") pushData:SPDLocalizedString(@"送给你一份礼物希望你会喜欢") success:^(long messageId) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showTips:SPDLocalizedString(@"礼物发送成功")];
        });
    } error:^(RCErrorCode nErrorCode, long messageId) {
        
    }];
}

- (void)sendPrivateVehicleMessageWithModel:(SPDVehicleModel *)vehicle_Model {
    PresentMessage * msg = [PresentMessage messageWithContent:@"送你一辆座驾，快去车库装备吧" andVehicle_id:vehicle_Model.vehicle_id andGiftName:vehicle_Model.name andImageName:vehicle_Model.image andGoldNum:@"0" andCharamScore:@"0" andType:@"vehicle" andExpire_time:vehicle_Model.expire_time_str];
    [[RCIM sharedRCIM] sendMessage:ConversationType_PRIVATE targetId:self.receiveGiftUserModel.user_id.notEmpty ? self.receiveGiftUserModel.user_id :self.receiveGiftUserModel._id content:msg pushContent:SPDLocalizedString(@"送你一辆座驾，快去车库装备吧") pushData:SPDLocalizedString(@"送你一辆座驾，快去车库装备吧") success:^(long messageId) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[SPDCommonTool shareTool] checkExpWithGlodNum:[vehicle_Model.price integerValue] WithType:ExpVehicle];
            [self showTips:SPDStringWithKey(@"送座驾成功!", nil)];
        });
    } error:^(RCErrorCode nErrorCode, long messageId) {
        
    }];
}

 //送房间消息
- (void)sendChatRoomGiftMessageWithModel:(GiftListModel *)giftModel {
    NSString * targetId;
    RCMessageContent * message;
    if (ZegoManager.sceneType == SceneTypeClanRoom && giftModel.send_all_chatroom) {
       targetId = GLOBALCHATROOM;
       NSString * messageTxt = [NSString stringWithFormat:SPDStringWithKey(@"我送给%@一个%@", nil),@"xx",giftModel.name];
       SPDWorldChatMessage *rcWordMessage = [SPDWorldChatMessage SPDWorldChatMessageWithContent:messageTxt andsend_ClanId:ZegoManager.clan_id andsend_ClanName:@"" andGender:[SPDApiUser currentUser].gender];
       rcWordMessage.present_url = [NSString stringWithFormat:STATIC_DOMAIN_URL,giftModel.cover];
       rcWordMessage.gift_id = giftModel._id;
       rcWordMessage.type = @"3";
       rcWordMessage.send_msg_world = giftModel.send_msg_world ? @"true":@"false";
       rcWordMessage.num = giftModel.num;
       rcWordMessage.portrait = self.receiveGiftUserModel.portrait;
        if (!self.isMutableGift || (self.isMutableGift && self.selectedUsersArray.count == 1)) {
           rcWordMessage.receive_avatar = [NSString stringWithFormat:STATIC_DOMAIN_URL,self.receiveGiftUserModel.avatar];
           rcWordMessage.receive_gender = self.receiveGiftUserModel.gender;
           rcWordMessage.receive_Id = self.receiveGiftUserModel._id;
           rcWordMessage.receive_nickname = self.receiveGiftUserModel.nick_name;
           rcWordMessage.receive_user_type = @"normal";
           rcWordMessage.gender = [SPDApiUser currentUser].gender;
       }else{// 群送礼物 要区分
           rcWordMessage.receive_avatar = [NSString stringWithFormat:@"https://%@famy.17jianyue.cn/image/%@",DEV? @"dev-" : @"",([self.chooseGroupType containsString:@"mic"]? @"mic.png": @"room.png")];
          rcWordMessage.receive_gender = [SPDApiUser currentUser].gender;
          rcWordMessage.receive_Id = DEV ? @"2877" : @"5";
          rcWordMessage.receive_user_type = self.chooseGroupType;
          rcWordMessage.curSel = [NSString stringWithFormat:@"%@",@(self.selectedUsersArray.count)];
          rcWordMessage.sumSel = [NSString stringWithFormat:@"%@",@(self.groupsUsers.count)];
          rcWordMessage.receiveUser = [self.selectedUsersArray copy];
          rcWordMessage.receive_nickname = [self getReceiveUserName];
          rcWordMessage.gender = [SPDApiUser currentUser].gender;
       }
       rcWordMessage.senderUserInfo = [[RCIM sharedRCIM] currentUserInfo];
       message = rcWordMessage;
    }else{
        targetId = ZegoManager.clan_id;
        SPDChatRoomSendPresentMessage * msg = [SPDChatRoomSendPresentMessage new];
        if (!self.isMutableGift || (self.isMutableGift && self.selectedUsersArray.count == 1)) {
            msg.receive_avatar = [NSString stringWithFormat:STATIC_DOMAIN_URL,self.receiveGiftUserModel.avatar];
            msg.receive_gender = self.receiveGiftUserModel.gender;
            msg.receive_id = self.receiveGiftUserModel._id;
            msg.receive_nickname = self.receiveGiftUserModel.nick_name;
            msg.receive_user_type = @"normal";
        }else{// 群送礼物 要区分
            msg.receive_avatar = [NSString stringWithFormat:@"https://%@famy.17jianyue.cn/image/%@",DEV? @"dev-" : @"",([self.chooseGroupType containsString:@"mic"]? @"mic.png": @"room.png")];
            msg.receive_gender = [SPDApiUser currentUser].gender;
            msg.receive_id = DEV ? @"2877" : @"5";
            msg.receive_user_type = self.chooseGroupType;
            msg.curSel = [NSString stringWithFormat:@"%@",@(self.selectedUsersArray.count)];
            msg.sumSel = [NSString stringWithFormat:@"%@",@(self.groupsUsers.count)];
            msg.receiveUser = [self.selectedUsersArray copy];
            msg.receive_nickname = [self getReceiveUserName];
        }
        msg.position = giftModel.position;
        msg.gift_id = giftModel._id;
        msg.present_url = [NSString stringWithFormat:STATIC_DOMAIN_URL,giftModel.cover];
        msg.send_msg_world = giftModel.send_msg_world ? @"true": @"false";
        msg.send_all_chatroom = giftModel.send_all_chatroom ?@"true":@"false";
        msg.senderUserInfo = [RCIM sharedRCIM].currentUserInfo;
        msg.num = giftModel.num;
        msg.send_gender = [SPDApiUser currentUser].gender;
        message = msg;
    }
    [[RCIMClient sharedRCIMClient] sendMessage:ConversationType_CHATROOM targetId:targetId content:message pushContent:@"" pushData:@"" success:^(long messageId) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.delegate && [self.delegate respondsToSelector:@selector(giftGivingView:showEffectWithMessage:)]) {
                [self.delegate giftGivingView:self showEffectWithMessage:message];
            }
        });
    } error:^(RCErrorCode nErrorCode, long messageId) {

    }];
}

- (void)sendRadioRoomVehicleMessageWithModel:(SPDVehicleModel *)model {
    SPDChatRoomSendPresentMessage *message = [SPDChatRoomSendPresentMessage new];
    message.senderUserInfo = [RCIM sharedRCIM].currentUserInfo;
    message.send_gender = [SPDApiUser currentUser].gender;
    message.receive_id = self.receiveGiftUserModel._id;
    message.receive_avatar = [NSString stringWithFormat:STATIC_DOMAIN_URL, self.receiveGiftUserModel.avatar];
    message.receive_nickname = self.receiveGiftUserModel.nick_name;
    message.receive_gender = self.receiveGiftUserModel.gender;
    message.receive_user_type = @"normal";
    message.gift_id = model.vehicle_id;
    message.present_url = [NSString stringWithFormat:STATIC_DOMAIN_URL, model.image];
    message.num = @"1";
    [[RCIMClient sharedRCIMClient] sendMessage:ConversationType_CHATROOM targetId:ZegoManager.clan_id content:message pushContent:@"" pushData:@"" success:^(long messageId) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([self.delegate respondsToSelector:@selector(giftGivingView:showEffectWithMessage:)]) {
                [self.delegate giftGivingView:self showEffectWithMessage:message];
            }
        });
    } error:^(RCErrorCode nErrorCode, long messageId) {

    }];
}

- (void)sendVehicleMessageInGlobalChatroom:(SPDVehicleModel *)model {
   NSString * messageTxt = [NSString stringWithFormat:SPDStringWithKey(@"我送给%@一个%@", nil),self.receiveGiftUserModel.nick_name,model.name];
    SPDWorldChatMessage *rcWordMessage = [SPDWorldChatMessage SPDWorldChatMessageWithContent:messageTxt andsend_ClanId:ZegoManager.clan_id andsend_ClanName:ZegoManager.clan_name?:@"home" andGender:[SPDApiUser currentUser].gender];
    LY_Portrait * p = [LY_Portrait new];
    p.headwearUrl = self.sendGiftUserModel.portrait.headwearUrl;
    p.url = self.sendGiftUserModel.avatar;
    rcWordMessage.portrait = p;
   if (!self.isMutableGift || (self.isMutableGift && self.selectedUsersArray.count == 1)) {
       rcWordMessage.receive_avatar = [NSString stringWithFormat:STATIC_DOMAIN_URL,self.receiveGiftUserModel.avatar];
       rcWordMessage.receive_gender = self.receiveGiftUserModel.gender;
       rcWordMessage.receive_Id = self.receiveGiftUserModel._id;
       rcWordMessage.receive_nickname = self.receiveGiftUserModel.nick_name;
       rcWordMessage.receive_user_type = @"normal";
       if (self.isMutableGift) {
           SPDVoiceLiveUserModel * model = self.selectedUsersArray[0];
           rcWordMessage.receiveHeadwearWebp = model.portrait.headwearUrl;
       }else{
           rcWordMessage.receiveHeadwearWebp = self.receiveGiftUserModel.portrait.headwearUrl;
       }
   }else{// 群送礼物 要区分
       rcWordMessage.receive_avatar = [NSString stringWithFormat:@"https://%@famy.17jianyue.cn/image/%@",DEV? @"dev-" : @"",([self.chooseGroupType containsString:@"mic"]? @"mic.png": @"room.png")];
       rcWordMessage.receive_gender = [SPDApiUser currentUser].gender;
       rcWordMessage.receive_Id = DEV ? @"2877" : @"5";
       rcWordMessage.receive_user_type = self.chooseGroupType;
       rcWordMessage.curSel = [NSString stringWithFormat:@"%@",@(self.selectedUsersArray.count)];
       rcWordMessage.sumSel = [NSString stringWithFormat:@"%@",@(self.groupsUsers.count)];
       rcWordMessage.receiveUser = [self.selectedUsersArray copy];
       rcWordMessage.receive_nickname = [self getReceiveUserName];
   }
   rcWordMessage.present_url = [NSString stringWithFormat:STATIC_DOMAIN_URL,model.image];
   rcWordMessage.gift_id = model.vehicle_id;
   rcWordMessage.type = @"2";
   rcWordMessage.send_msg_world = @"false";
   rcWordMessage.senderUserInfo = [RCIM sharedRCIM].currentUserInfo;
    
    [[RCIM sharedRCIM] sendMessage:ConversationType_CHATROOM targetId:GLOBALCHATROOM content:rcWordMessage pushContent:@"" pushData:@"" success:^(long messageId) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.delegate && [self.delegate respondsToSelector:@selector(giftGivingView:didSendVehicleMessageInChatroom:)]) {
                [self.delegate giftGivingView:self didSendVehicleMessageInChatroom:rcWordMessage];
            }
        });
    } error:^(RCErrorCode nErrorCode, long messageId) {
        
    }];
    
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ![touch.view isDescendantOfView:self.contentView] && ![touch.view isDescendantOfView:self.numTableView] && ![touch.view isDescendantOfView:self.descView] && ![touch.view isDescendantOfView:self.groupUsersView] && ![touch.view isDescendantOfView:self.chooseGroupTypeTableView];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if (collectionView == self.typeCollectionView) {
        return self.typeArray.count;
    } else {
        return 1;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == self.typeCollectionView) {
        return 1;
    }else if (collectionView == self.groupUsersCollectionView){
        return self.groupsUsers.count;
    }
    else {
        switch (self.typeSelectedIndex) {
            case 0:
                return self.goldGiftArray.count;
            case 2:
                return self.vehicleArray.count;
            case 3:
                return self.nobleGiftArray.count;
            case 4:
            return self.magicArray.count;
            default:
                return self.diamondGiftArray.count;
        }
    }
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.typeCollectionView) {
        KKGiftGivingTypeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"KKGiftGivingTypeCell" forIndexPath:indexPath];
        cell.typeLabel.text = self.typeArray[indexPath.section];
        if (indexPath.section == self.typeSelectedIndex) {
            cell.typeLabel.font = [UIFont boldSystemFontOfSize:15];
            cell.typeLabel.textColor = [UIColor whiteColor];
        } else {
            cell.typeLabel.font = [UIFont boldSystemFontOfSize:14];
            cell.typeLabel.textColor = [UIColor colorWithHexString:@"#4E4E4E"];
        }
        cell.separatorView.hidden = (indexPath.section == self.typeArray.count - 1);
        return cell;
    }else if (collectionView == self.groupUsersCollectionView){
        KKGiftGroupUsersCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"KKGiftGroupUsersCell" forIndexPath:indexPath];
        cell.userModel = self.groupsUsers[indexPath.row];
        cell.borderView.layer.borderWidth = [self.selectedUsersArray containsObject:self.groupsUsers[indexPath.row]] ? 1:0;
        return cell;
    }
    else {
        switch (self.typeSelectedIndex) {
            case 0: {
                KKGiftGivingGiftCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"KKGiftGivingGiftCell" forIndexPath:indexPath];
                cell.model = self.goldGiftArray[indexPath.row];
                cell.cellSelected = (indexPath.row == self.giftSelectedIndex) ;
                return cell;
            }
            case 2: {
                KKGiftGivingVehicleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"KKGiftGivingVehicleCell" forIndexPath:indexPath];
                cell.vehicleModel = self.vehicleArray[indexPath.row];
                cell.cellSelected = (indexPath.row == self.giftSelectedIndex) ;
                return cell;
            }
            case 3: {
                KKGiftGivingGiftCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"KKGiftGivingGiftCell" forIndexPath:indexPath];
                cell.model = self.nobleGiftArray[indexPath.row];
                cell.cellSelected = (indexPath.row == self.giftSelectedIndex) ;
                return cell;
            }
            
            case 4: {
                MagicCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MagicCVCell" forIndexPath:indexPath];
                cell.magicModel = self.magicArray[indexPath.row];
                cell.cellSelected = (indexPath.row == self.giftSelectedIndex) ;
                return cell;
            }
            default: {
                KKGiftGivingGiftCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"KKGiftGivingGiftCell" forIndexPath:indexPath];
                cell.model = self.diamondGiftArray[indexPath.row];
                cell.cellSelected = (indexPath.row == self.giftSelectedIndex) ;
                return cell;
            }
        }
    }
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.typeCollectionView) {
        self.typeSelectedIndex = indexPath.section;
    }else if(collectionView == self.groupUsersCollectionView){
      SPDVoiceLiveUserModel * model = self.groupsUsers[indexPath.row];
        if ([self.selectedUsersArray containsObject:model]) {
            if (self.selectedUsersArray.count == 1) {
               [self showTips:SPDLocalizedString(@"请至少选择1个人")];
               return;
           }
            [self.selectedUsersArray removeObject:model];
        }else{
            [self.selectedUsersArray addObject:model];
        }
        self.groupUsersCountLabel.text = _selectedUsersArray.count == 0 ? @"" : [NSString stringWithFormat:@"%@",@(_selectedUsersArray.count)];
    } else {
        self.giftSelectedIndex = indexPath.row;
        [self configDescriptionForDescView];
    }
    [collectionView reloadData];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.typeCollectionView) {
        CGFloat width = [self.typeArray[indexPath.section] boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 40)
                                                                    options:NSStringDrawingUsesLineFragmentOrigin
                                                                 attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15]}
                                                                    context:nil].size.width;
        return CGSizeMake(ceilf(width) + 20.0f * 2 + 1.5f, 48.0f);
    }else if (collectionView == self.groupUsersCollectionView){
        return CGSizeMake(45, 52);
    }
    else {
        return CGSizeMake((collectionView.bounds.size.width - 1.5f * 2 - 3.0f * 3) / 4.0f, 100.0f);
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    if (scrollView == self.giftCollectionView) {
        NSInteger targetPage = targetContentOffset->x / scrollView.bounds.size.width;
        if (kIsMirroredLayout) {
            self.pageControl.currentPage = self.pageControl.numberOfPages - 1 - targetPage;
        } else {
            self.pageControl.currentPage = targetPage;
        }
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.numTableView) {
        return self.numArray.count;
    }else{
        return self.allGroupTypesArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.numTableView) {
        static NSString *identifier = @"NumCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            cell.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            cell.textLabel.font = [UIFont boldSystemFontOfSize:15];
            cell.indentationWidth = -118;
        }
        cell.textLabel.text = self.numArray[indexPath.row];
        cell.textLabel.textColor = [UIColor colorWithHexString:[cell.textLabel.text isEqualToString:self.num] ? @"00D4A0" : @"#FFFFFF"];
        if (indexPath.row == self.numArray.count - 1) {
            cell.separatorInset = UIEdgeInsetsMake(0, 118, 0, 0);
            cell.indentationLevel = 1;
        } else {
            cell.separatorInset = UIEdgeInsetsMake(0, 9, 0, 9);
            cell.indentationLevel = 0;
        }
        return cell;
    }else{
        static NSString *identifier = @"GroupCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            cell.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
            cell.indentationWidth = -140;
        }
        NSDictionary * dict = self.allGroupTypesArray[indexPath.row];
        cell.textLabel.text =  SPDLocalizedString(dict[@"title"]);
        cell.textLabel.textColor = [UIColor colorWithHexString:[dict[@"key"] isEqualToString:self.chooseGroupType] ? @"00D4A0" : @"#FFFFFF"];
        if (indexPath.row == self.allGroupTypesArray.count - 1) {
            cell.separatorInset = UIEdgeInsetsMake(0, 140, 0, 0);
            cell.indentationLevel = 1;
        } else {
            cell.separatorInset = UIEdgeInsetsMake(0, 9, 0, 9);
            cell.indentationLevel = 0;
        }
        return cell;
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.numTableView) {
        self.num = self.numArray[indexPath.row];
        [self.numTableView reloadData];
        [self.numButton setTitle:self.num forState:UIControlStateNormal];
        self.numBackgroundView.hidden = YES;
    }else{
        if (self.delegate && [self.delegate respondsToSelector:@selector(didChooseGroupUsersType:type:)]) {
            NSDictionary * dict = self.allGroupTypesArray[indexPath.row];
            self.chooseGroupType = dict[@"key"];
            [self.delegate didChooseGroupUsersType:self type:dict[@"key"]];
            self.chooseGroupTypeBgView.hidden = YES;
            self.chooseGroupButton.selected = !self.chooseGroupButton.selected;
            if ([self.chooseGroupType isEqualToString:@"all"]) {
                [self showTips:self.typeSelectedIndex == 2 ? SPDStringWithKey(@"你将送座驾给房间内所有人", nil) :SPDStringWithKey(@"你将送礼物给房间内所有人", nil)];
            }
        }
    }
}

#pragma mark - Getters and Setters

- (void)setTypeSelectedIndex:(NSInteger)typeSelectedIndex {
    _typeSelectedIndex = typeSelectedIndex;
    self.num = @"1";
    self.pageControl.currentPage = 0;
    [self collectionView:self.giftCollectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]]; // 默认选择第一个单元格
    [self.giftCollectionView reloadData];
    [self.giftCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    UIImage *mirrow = [UIImage imageNamed:@"gift_give_btn"].mirroredImage;
    [self.sendGiftButton setBackgroundImage:!kIsMirroredLayout  ? [UIImage imageNamed:@"gift_give_btn"]: mirrow  forState:UIControlStateNormal];
    self.sendGiftButton.mirrorBacgroundImage = YES;
    switch (self.typeSelectedIndex) {
        case 0:
            self.pageControl.numberOfPages = self.goldGiftArray.count == 0 ? 0 : (self.goldGiftArray.count - 1) / 8 + 1;
            self.numButton.hidden = NO;
            [self.numButton setTitle:self.num forState:UIControlStateNormal];
            self.currencyImageView.image = [UIImage imageNamed:@"ic_invitation_gold"];
            self.balanceLabel.text = self.goldBalance;
            break;
        case 1:
            self.pageControl.numberOfPages = self.diamondGiftArray.count == 0 ? 0 : (self.diamondGiftArray.count - 1) / 8 + 1;
            self.numButton.hidden = NO;
            [self.numButton setTitle:self.num forState:UIControlStateNormal];
            self.currencyImageView.image = [UIImage imageNamed:@"ic_diamond"];
            self.balanceLabel.text = self.diamondBalance;
            break;
        case 2:{
            self.pageControl.numberOfPages = self.vehicleArray.count == 0 ? 0 : (self.vehicleArray.count - 1) / 8 + 1;
            self.numButton.hidden = YES;
            [self.numButton setTitle:self.num forState:UIControlStateNormal];
            self.currencyImageView.image = [UIImage imageNamed:@"ic_invitation_gold"];
            self.balanceLabel.text = self.goldBalance;
            [self.sendGiftButton setBackgroundImage:[UIImage imageNamed:@"gift_give_btn_cicle"] forState:UIControlStateNormal];
            self.sendGiftButton.mirrorBacgroundImage = YES;
            break;
        }
        case 3:
            self.pageControl.numberOfPages = self.nobleGiftArray.count == 0 ? 0 : (self.nobleGiftArray.count - 1) / 8 + 1;
            self.numButton.hidden = NO;
            [self.numButton setTitle:self.num forState:UIControlStateNormal];
            self.currencyImageView.image = [UIImage imageNamed:@"ic_invitation_gold"];
            self.balanceLabel.text = self.goldBalance;
            break;
        
            
        default:
            self.pageControl.numberOfPages = self.magicArray.count == 0 ? 0 : (self.magicArray.count - 1) / 8 + 1;
            self.numButton.hidden = YES;
            [self.numButton setTitle:self.num forState:UIControlStateNormal];
            self.currencyImageView.image = [UIImage imageNamed:@"ic_invitation_gold"];
            self.balanceLabel.text = self.goldBalance;
            [self.sendGiftButton setBackgroundImage:[UIImage imageNamed:@"gift_give_btn_cicle"] forState:UIControlStateNormal];
            break;
    }
}

- (void)setIsMutableGift:(BOOL)isMutableGift {
    _isMutableGift = isMutableGift;
    [self.typeArray removeObject:SPDLocalizedString(@"魔法")];
    self.descViewBottom.constant = - 63;
    self.groupUsersView.hidden = NO;
}

- (void)setGroupsUsers:(NSMutableArray *)groupsUsers {
    _groupsUsers = groupsUsers;
    self.selectedUsersArray = [_groupsUsers mutableCopy];
    [self.groupUsersCollectionView reloadData];
}

- (void)setSelectedUsersArray:(NSMutableArray *)selectedUsersArray {
    _selectedUsersArray = selectedUsersArray;
    self.groupUsersCountLabel.text = _selectedUsersArray.count == 0 ? @"" : [NSString stringWithFormat:@"%@",@(_selectedUsersArray.count)];
}

- (void)setHideMagic:(BOOL)hideMagic {
    _hideMagic = hideMagic;
    
    [self.typeArray removeObject:SPDLocalizedString(@"魔法")];
}

- (NSMutableArray *)goldGiftArray {
    if (!_goldGiftArray) {
        _goldGiftArray = [NSMutableArray new];
    }
    return _goldGiftArray;
}

- (NSMutableArray *)diamondGiftArray {
    if (!_diamondGiftArray) {
        _diamondGiftArray = [NSMutableArray new];
    }
    return _diamondGiftArray;
}

- (NSMutableArray *)nobleGiftArray {
    if (!_nobleGiftArray) {
        _nobleGiftArray = [NSMutableArray new];
    }
    return _nobleGiftArray;
}

- (NSMutableArray *)vehicleArray {
    if(!_vehicleArray) {
        _vehicleArray = [NSMutableArray new];
    }
    return _vehicleArray;
}

- (NSMutableArray *)magicArray {
    if(!_magicArray) {
        _magicArray = [NSMutableArray new];
    }
    return _magicArray;
}

- (NSMutableArray *)allGroupTypesArray {
    if (!_allGroupTypesArray) {
        _allGroupTypesArray = [NSMutableArray arrayWithArray:@[@{
                                                                   @"title": @"麦上全部人",
                                                                   @"key": @"mic"},
                                                               @{
                                                                   @"title": @"房间内所有人",
                                                                   @"key": @"all"},
                                                               @{
                                                                   @"title": @"麦上全部女性",
                                                                   @"key": @"micfemale"},
                                                               @{
                                                                   @"title": @"麦上全部男性",
                                                                   @"key": @"micmale"},
                                                               @{
                                                                   @"title": @"房间内全部女性",
                                                                   @"key": @"allfemale"},
                                                               @{
                                                                   @"title": @"房间内全部男性",
                                                                   @"key": @"allmale"}]];
    }
    return _allGroupTypesArray;
}

@end
