//
//  KKGiftGivingTypeCell.h
//  KaKa
//
//  Created by 李楠 on 2019/11/5.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface KKGiftGivingTypeCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorView;

@end

NS_ASSUME_NONNULL_END
