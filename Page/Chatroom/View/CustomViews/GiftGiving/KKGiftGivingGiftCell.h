//
//  KKGiftGivingGiftCell.h
//  KaKa
//
//  Created by 李楠 on 2019/11/5.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class GiftListModel;

@interface KKGiftGivingGiftCell : UICollectionViewCell

@property (nonatomic, strong) GiftListModel *model;

@property (weak, nonatomic) IBOutlet UIImageView *giftImageView;

@property (nonatomic, assign) BOOL cellSelected;

@end

NS_ASSUME_NONNULL_END
