//
//  KKGiftGivingView.h
//  KaKa
//
//  Created by 李楠 on 2019/11/5.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class GiftListModel,KKGiftGivingView,SPDChatRoomSendPresentMessage,SPDVoiceLiveUserModel,HomeModel,SPDVehicleModel,SPDWorldChatMessage;

@protocol KKGiftGivingViewDelegate <NSObject>

@optional

- (void)didClickRechargeButton;
- (void)didChooseGroupUsersType:(KKGiftGivingView *)view type:(NSString *)type;
- (void)giftGivingView:(KKGiftGivingView *)view showEffectWithMessage:(RCMessageContent *)message;
- (void)giftGivingView:(KKGiftGivingView *)view didSendVehicleMessageInChatroom:(SPDWorldChatMessage *)message;
- (void)giftGivingView:(KKGiftGivingView *)view didSendMagicModel:(SPDMagicModel*)model andDict:(NSDictionary *)dict; // 在房间里需要发世界消息
- (void)giftGivingView:(KKGiftGivingView *)view prepareComboWithURL:(NSString *)URL params:(NSMutableDictionary *)params;

@end

@interface KKGiftGivingView : UIView

@property (nonatomic, weak) id<KKGiftGivingViewDelegate> delegate;

//@property (nonatomic, copy) NSString *userId;

@property (nonatomic, strong) HomeModel * receiveGiftUserModel; // 收礼物的model

@property (nonatomic, strong) HomeModel * sendGiftUserModel;// 送礼物的model

@property (nonatomic, strong) NSMutableArray * groupsUsers;

@property (nonatomic, assign) BOOL isMutableGift; // 是不是群送礼物

@property (nonatomic, strong) NSString * chooseGroupType; // all,mic,normal,allfemale,allmale,micfemale,micmale]房间、麦上,普通的,房间全部女性，房间全部男性，麦上全部女性，麦上全部男性


@property (nonatomic, assign) NSInteger selectedIndex;
@property (nonatomic, assign) BOOL hideMagic;


@end

NS_ASSUME_NONNULL_END
