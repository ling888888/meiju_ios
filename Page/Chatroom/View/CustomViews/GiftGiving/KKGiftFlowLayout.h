//
//  KKGiftFlowLayout.h
//  KaKa
//
//  Created by 李楠 on 2019/11/6.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface KKGiftFlowLayout : UICollectionViewFlowLayout

@end

NS_ASSUME_NONNULL_END
