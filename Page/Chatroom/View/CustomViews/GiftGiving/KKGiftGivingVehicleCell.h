//
//  KKGiftGivingVehicleCell.h
//  SimpleDate
//
//  Created by ling Hou on 2019/12/27.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class SPDVehicleModel;

@interface KKGiftGivingVehicleCell : UICollectionViewCell

@property (nonatomic, strong) SPDVehicleModel * vehicleModel;

@property (nonatomic, assign) BOOL cellSelected;

@end

NS_ASSUME_NONNULL_END
