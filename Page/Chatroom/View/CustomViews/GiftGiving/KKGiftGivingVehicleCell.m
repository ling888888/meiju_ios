//
//  KKGiftGivingVehicleCell.m
//  SimpleDate
//
//  Created by ling Hou on 2019/12/27.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "KKGiftGivingVehicleCell.h"
#import "SPDVehicleModel.h"

@interface KKGiftGivingVehicleCell ()
@property (weak, nonatomic) IBOutlet UIButton *timeButton;
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *priceButton;

@end

@implementation KKGiftGivingVehicleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setVehicleModel:(SPDVehicleModel *)vehicleModel {
    _vehicleModel = vehicleModel;
    if (_vehicleModel.vehicle_id.notEmpty) {
        self.hidden = NO;
        [self.timeButton setTitle:SPDStringWithKey(vehicleModel.valid_time, nil)  forState:UIControlStateNormal];
        [self.coverImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,vehicleModel.image]]];
        self.nameLabel.text = vehicleModel.name;
        [self.priceButton setTitle:[vehicleModel.price stringValue]  forState:UIControlStateNormal];
        [self.priceButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_icon_8", _vehicleModel.buy_type]] forState:UIControlStateNormal];
    }else{
        self.hidden = YES;
    }
}

- (void)setCellSelected:(BOOL)cellSelected {
    _cellSelected = cellSelected;
    self.layer.borderWidth = _cellSelected ? 1.5 : 0;
    if (_cellSelected) {
        [self heartAnimation];
    }
}

- (void)heartAnimation{
    float bigSize = 1.1;
    CABasicAnimation *heart = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    heart.duration = 0.5;
    heart.toValue = [NSNumber numberWithFloat:bigSize];
    heart.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    heart.autoreverses = YES;
    heart.repeatCount = FLT_MAX;
    [self.coverImageView.layer addAnimation:heart forKey:@"transform.scale"];
}

@end
