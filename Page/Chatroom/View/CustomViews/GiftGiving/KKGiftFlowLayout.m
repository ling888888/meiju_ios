//
//  KKGiftFlowLayout.m
//  KaKa
//
//  Created by 李楠 on 2019/11/6.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "KKGiftFlowLayout.h"

@interface KKGiftFlowLayout ()

@property (nonatomic, assign) NSUInteger rowCount;
@property (nonatomic, assign) NSUInteger cellCountPerRow;
@property (nonatomic, strong) NSMutableArray *allAttributes;

@end

@implementation KKGiftFlowLayout

- (void)prepareLayout {
    [super prepareLayout];
    
    self.rowCount = 2;
    self.cellCountPerRow = 4;
    self.allAttributes = [NSMutableArray new];
    
    for (NSInteger i = 0; i < self.collectionView.numberOfSections; i++) {
        NSMutableArray *attributesArray = [NSMutableArray new];
        NSUInteger items = [self.collectionView numberOfItemsInSection:i];
        
        for (NSInteger j = 0; j<items; j++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:j inSection:i];
            UICollectionViewLayoutAttributes *attributes = [self layoutAttributesForItemAtIndexPath:indexPath];
            [attributesArray addObject:attributes];
        }
        [self.allAttributes addObject:attributesArray];
    }
}

- (CGSize)collectionViewContentSize {
    return [super collectionViewContentSize];
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger item = indexPath.item;
    NSUInteger x;
    NSUInteger y;
    [self targetPositionWithItem:item resultX:&x resultY:&y];
    NSUInteger item2 = [self originItemAtX:x y:y];
    NSIndexPath *theNewIndexPath = [NSIndexPath indexPathForItem:item2 inSection:indexPath.section];
    UICollectionViewLayoutAttributes *theNewAttr = [super layoutAttributesForItemAtIndexPath:theNewIndexPath];
    theNewAttr.indexPath = indexPath;
    return theNewAttr;
}

- (NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSArray *attributes = [super layoutAttributesForElementsInRect:rect];
    NSMutableArray *tmp = [NSMutableArray array];
    for (UICollectionViewLayoutAttributes *attr in attributes) {
        for (NSMutableArray *attributes in self.allAttributes) {
            for (UICollectionViewLayoutAttributes *attr2 in attributes) {
                if (attr.indexPath.item == attr2.indexPath.item) {
                    [tmp addObject:attr2];
                    break;
                }
            }
        }
    }
    return tmp;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return YES;
}

- (void)targetPositionWithItem:(NSUInteger)item resultX:(NSUInteger *)x  resultY:(NSUInteger *)y {
    NSUInteger page = item / (self.cellCountPerRow * self.rowCount);
    NSUInteger theX = item % self.cellCountPerRow + page * self.cellCountPerRow;
    NSUInteger theY = item / self.cellCountPerRow - page * self.rowCount;
    if (x != NULL) {
        *x = theX;
    }
    if (y != NULL) {
        *y = theY;
    }
}

- (NSUInteger)originItemAtX:(NSUInteger)x y:(NSUInteger)y {
    NSUInteger item = x * self.rowCount + y;
    return item;
}

@end
