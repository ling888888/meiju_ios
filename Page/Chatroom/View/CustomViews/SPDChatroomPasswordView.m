//
//  SPDChatroomPasswordView.m
//  SimpleDate
//
//  Created by 李楠 on 2018/11/16.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "SPDChatroomPasswordView.h"

@interface SPDChatroomPasswordView ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *numLabel1;
@property (weak, nonatomic) IBOutlet UILabel *numLabel2;
@property (weak, nonatomic) IBOutlet UILabel *numLabel3;
@property (weak, nonatomic) IBOutlet UILabel *numLabel4;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UITextField *textField;

@end

@implementation SPDChatroomPasswordView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.alpha = 0;
}

- (void)show {
    switch (self.type) {
        case PasswordViewTypeLock:
            self.titleLabel.text = SPDStringWithKey(@"设置房间锁密码", nil);
            break;
        case PasswordViewTypeUnlock:
            self.titleLabel.text = SPDStringWithKey(@"输入房间锁密码", nil);
            break;
        case PasswordViewTypeModify:
            self.titleLabel.text = SPDStringWithKey(@"修改房间锁密码", nil);
            break;
        default:
            break;
    }
    [self.cancelBtn setTitle:SPDStringWithKey(@"取消", nil) forState:UIControlStateNormal];
    [self.confirmBtn setTitle:SPDStringWithKey(@"确定", nil) forState:UIControlStateNormal];
    [self.textField becomeFirstResponder];
    [UIView animateWithDuration:0.25 animations:^{
        self.alpha = 1;
    }];
}

- (void)dimiss {
    [self.textField resignFirstResponder];
    [UIView animateWithDuration:0.25 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        self.textField.text = @"";
        [self textFieldEdtingChanged:self.textField];
        [self removeFromSuperview];
    }];
}

- (IBAction)clickCancelBtn:(UIButton *)sender {
    [self dimiss];
    if ([self.delegate respondsToSelector:@selector(didClickCancelBtnWithType:)]) {
        [self.delegate didClickCancelBtnWithType:self.type];
    }
}

- (IBAction)clickConfirmBtn:(UIButton *)sender {
    [self dimiss];
    if ([self.delegate respondsToSelector:@selector(didClickConfirmBtnWithType:password:)]) {
        [self.delegate didClickConfirmBtnWithType:self.type password:self.textField.text];
    }
}

- (IBAction)textFieldEdtingChanged:(UITextField *)sender {
    if (sender.text.length >= 4) {
        sender.text = [sender.text substringToIndex:4];
        self.confirmBtn.enabled = YES;
    } else {
        self.confirmBtn.enabled = NO;
    }
    
    self.numLabel1.text = sender.text.length > 0 ? [sender.text substringWithRange:NSMakeRange(0, 1)] : @"";
    self.numLabel1.layer.borderColor = sender.text.length > 0 ? [UIColor colorWithHexString:COMMON_PINK].CGColor : [UIColor colorWithHexString:@"D2D2D2"].CGColor;
    self.numLabel2.text = sender.text.length > 1 ? [sender.text substringWithRange:NSMakeRange(1, 1)] : @"";
    self.numLabel2.layer.borderColor = sender.text.length > 1 ? [UIColor colorWithHexString:COMMON_PINK].CGColor : [UIColor colorWithHexString:@"D2D2D2"].CGColor;
    self.numLabel3.text = sender.text.length > 2 ? [sender.text substringWithRange:NSMakeRange(2, 1)] : @"";
    self.numLabel3.layer.borderColor = sender.text.length > 2 ? [UIColor colorWithHexString:COMMON_PINK].CGColor : [UIColor colorWithHexString:@"D2D2D2"].CGColor;
    self.numLabel4.text = sender.text.length > 3 ? [sender.text substringWithRange:NSMakeRange(3, 1)] : @"";
    self.numLabel4.layer.borderColor = sender.text.length > 3 ? [UIColor colorWithHexString:COMMON_PINK].CGColor : [UIColor colorWithHexString:@"D2D2D2"].CGColor;
}

@end
