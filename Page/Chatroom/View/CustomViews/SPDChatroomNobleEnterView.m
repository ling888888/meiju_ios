//
//  SPDChatroomNobleEnterView.m
//  SimpleDate
//
//  Created by 李楠 on 2018/10/16.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDChatroomNobleEnterView.h"
#import "SPDJoinChatRoomMessage.h"
#import "UIView+RGSize.h"
#import "LiveJoinMessage.h"
@interface SPDChatroomNobleEnterView ()

@property (nonatomic, strong) UIImageView *bgImageView;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) LY_PortraitView * avatarPortraitView;
@end

@implementation SPDChatroomNobleEnterView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initSubviews];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    return self;
}

- (void)initSubviews {
    self.bgImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    [self addSubview:self.bgImageView];
    
     CGFloat width = (self.height - 2) * 1.5;
     CGFloat y = (width - self.height)/2.0f;
     self.avatarPortraitView = [[LY_PortraitView alloc]initWithFrame:CGRectMake(-y, -y, width, width)];
     [self addSubview:self.avatarPortraitView];
    
    self.contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.avatarPortraitView.frame)+ 5 - y, 0, self.bounds.size.width - 40, self.bounds.size.height)];
    
    self.contentLabel.font = [UIFont systemFontOfSize:14];
    self.contentLabel.textColor = [UIColor whiteColor];
    self.contentLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.contentLabel];
    
}

- (void)setMessage:(RCMessageContent *)message {
    _message = message;
    [self.dataArray addObject:_message];
    if (self.dataArray.count == 1) {
        [self startAnimationViewWithMessage:_message];
    }
}

- (void)startAnimationViewWithMessage:(RCMessageContent *)message {
    
    self.avatarPortraitView.portrait = [message valueForKey:@"portrait"];
    NSString * name;
    if ([message isKindOfClass:[SPDJoinChatRoomMessage class]]) {
        name = message.senderUserInfo.name;
    }else{
        LY_Portrait * portrait = [message valueForKey:@"portrait"];
        portrait.url = [message valueForKey:@"avatar"];
        self.avatarPortraitView.portrait = portrait;
        name = [message valueForKey:@"userName"];
    }
    NSMutableAttributedString * firstPart = [[NSMutableAttributedString alloc] initWithString:name.notEmpty?name:message.senderUserInfo.name];
    [firstPart addAttributes:@{ NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:SPD_HEXCOlOR(@"fac578")} range:NSMakeRange(0, firstPart.length)];
    NSAttributedString * secondPart = [[NSAttributedString alloc]initWithString:@"进入房间".localized attributes:@{ NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:SPD_HEXCOlOR(@"ffffff")}];
    [firstPart appendAttributedString:[[NSAttributedString alloc]initWithString:@" "]];
    [firstPart appendAttributedString:secondPart];
    self.contentLabel.attributedText = firstPart;
    
    CGFloat nobleEnterRoomTextLength = [self.contentLabel.text sizeWithFont:[UIFont systemFontOfSize:14] andMaxSize:CGSizeMake(self.bounds.size.width - 27 - CGRectGetWidth(self.avatarPortraitView.frame), 14)].width;
    
    CGRect bgImageFrame = self.bgImageView.frame;
    bgImageFrame.size.width = nobleEnterRoomTextLength + 5 + 22 + 43 + 20;
    self.bgImageView.frame = bgImageFrame;
    
    UIImage * bgImage = [UIImage imageNamed:[NSString stringWithFormat:@"enter_%@",  [message valueForKey:@"noble"]]];
     self.bgImageView.image = [bgImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 22, 0, 30)resizingMode:UIImageResizingModeStretch];
    
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        CGRect frame = self.frame;
        frame.origin.x = 10;
        self.frame = frame;
    } completion:^(BOOL finished) {
        if (finished) {
            [self performSelector:@selector(stopAnimation) withObject:nil afterDelay:1 inModes:@[NSRunLoopCommonModes]];
        } else {
            [self resetFrame];
            [self.dataArray removeAllObjects];
        }
    }];
}

- (void)stopAnimation {
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        self.alpha = 1;
        [self resetFrame];
        if (finished) {
            if (self.dataArray.count) {
                [self.dataArray removeObjectAtIndex:0];
            }
            if (self.dataArray.count) {
                [self startAnimationViewWithMessage:self.dataArray[0]];
            }
        } else {
            [self.dataArray removeAllObjects];
        }
    }];
}

- (void)enterBackground {
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self resetFrame];
    [self.dataArray removeAllObjects];
}

- (void)resetFrame {
    CGRect frame = self.frame;
    frame.origin.x = kScreenW + 10;
    self.frame = frame;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

@end
