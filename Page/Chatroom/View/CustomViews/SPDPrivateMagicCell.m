//
//  SPDPrivateMagicCell.m
//  SimpleDate
//
//  Created by Monkey on 2017/10/24.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDPrivateMagicCell.h"
#import "SPDPrivateMagicMessage.h"

@interface SPDPrivateMagicCell()

@property (nonatomic,strong)UIView * magicNameTitleView;

@property (nonatomic,strong)UIImageView * magicBackgroundView;

@property (nonatomic,strong)UILabel * magicNameLabel;

@property (nonatomic,strong)UIImageView * magicImageView;

@property (nonatomic,strong)UILabel * magicDescLabel;

@property (nonatomic,strong)UIButton * resultButton; //报仇 和感谢

@property (nonatomic,assign)CGFloat width;

@end


@implementation SPDPrivateMagicCell


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    self.magicBackgroundView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, pixwn(533.0/2), 290/2)];
    [self.messageContentView addSubview:self.magicBackgroundView];
    
    self.resultButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.resultButton setTitle:@"sdsa" forState:UIControlStateNormal];
    [self.resultButton setTintColor:[UIColor whiteColor]];
    self.resultButton .titleLabel.font = [UIFont systemFontOfSize:13];
    [self.resultButton setBackgroundColor:SPD_HEXCOlOR(@"f96a6a")];
    self.resultButton.layer.cornerRadius = 11;
    self.resultButton.clipsToBounds = YES;
    self.resultButton.frame = CGRectMake((pixwn(533.0/2)-62)/2, 290/2-7.5-22, 62, 22);
    [self.resultButton addTarget:self action:@selector(resultButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.messageContentView addSubview:self.resultButton];
    [self.resultButton addTarget:self action:@selector(handleResultBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.magicDescLabel =[[UILabel alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(self.magicNameTitleView.frame)+19, pixwn(533.0/2)-57-10-10-15, 50)];
    self.magicDescLabel.textColor = [UIColor whiteColor];
    self.magicDescLabel.font = [UIFont systemFontOfSize:13];
    self.magicDescLabel.numberOfLines = 0;
    [self.messageContentView addSubview:self.magicDescLabel];
    
    self.magicImageView = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.magicDescLabel.frame)+10, self.magicDescLabel.frame.origin.y, 57, 57)];
    [self.magicImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:@""]];
    [self.messageContentView addSubview:self.magicImageView];
    
}

- (void)resultButtonClicked {
    
}

- (void)setDataModel:(RCMessageModel *)model {
    
    [super setDataModel:model];
    
    [self setAutoLayout];
    
}

- (void)setAutoLayout {
    
    if (self.messageDirection == MessageDirection_SEND) {
        CGRect contentFrame = self.messageContentView.frame;
        //原来给的 x=119  10  6  是融云的距离 40 = avatarwidth
        contentFrame.origin.x = kScreenW-10-6-40-pixwn(533.0/2);
        contentFrame.size.width = pixwn(533.0/2);
        contentFrame.size.height = 290/2;
        self.messageContentView.frame = contentFrame;
        
    }else{
        
    }
    
    RCMessageModel * model = self.model;
    SPDPrivateMagicMessage * msg = (SPDPrivateMagicMessage *)model.content;
    [self layoutWithMessage:msg];
}

- (void)layoutWithMessage:(SPDPrivateMagicMessage *)message {
    
    if (MessageDirection_SEND == self.messageDirection) {
        self.magicBackgroundView.image = [UIImage imageNamed:@"img_magic_send"];
    }else{
        self.magicBackgroundView.image = [UIImage imageNamed:@"img_magic_receive"];
    }
    
    CGRect magicNameTitleViewFrame = self.magicNameTitleView.frame;
    if (self.messageDirection == MessageDirection_RECEIVE) {
        magicNameTitleViewFrame.origin.x =pixwn(16);
    }else if(self.messageDirection == MessageDirection_SEND){
        magicNameTitleViewFrame.origin.x =pixwn(60+10);
    }
    self.magicNameTitleView.frame = magicNameTitleViewFrame;
    
    //等级和魔法
    self.magicNameLabel.text = [NSString stringWithFormat:@"%@",SPDStringWithKey(message.magic_name, nil)];
    [self.magicNameLabel sizeToFit];
    //再改变一下namelabel的高度
    CGRect nameLabelFrame = self.magicNameLabel.frame;
    nameLabelFrame.size.height = 13;
    self.magicNameLabel.frame = nameLabelFrame;
    
    [self handleMagicLevelWith:[message.magic_level integerValue]];
    
    [self.magicImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:message.magic_cover]];
    
    //看是不是 send
    if (self.messageDirection == MessageDirection_SEND) {
        
        if (message.senderUserInfo) {
            if ( message.magic_result && [message.magic_result isEqualToString:@"success"]) {
                NSString * effect; //inc des
                if ([message.magic_effect isEqualToString:@"inc"]) {
                    effect = @"增加";
                }else{
                    effect = @"减少";
                }
                if ([message.magic_type isEqualToString:@"charm"] || [message.magic_type isEqualToString:@"exp"]) {
                    NSString * str ;
                    if ([message.magic_type isEqualToString:@"charm"]) {
                        str = [NSString stringWithFormat:@"%@魅力值",effect];
                    }else{
                        str = [NSString stringWithFormat:@"%@经验值",effect];
                    }
                    self.magicDescLabel.text = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",SPDStringWithKey(@"你向对方使用", nil),SPDStringWithKey(message.magic_name, nil),SPDStringWithKey(@"魔法", nil),SPDStringWithKey(@"成功", nil),SPDStringWithKey(@"TA", nil), SPDStringWithKey(str, nil),message.magic_effect_value];
                    
                }else if ([message.magic_type isEqualToString:@"banned_mic"] || [message.magic_type isEqualToString:@"banned_talk"]){
                    self.magicDescLabel.text = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",SPDStringWithKey(@"你向对方使用", nil),SPDStringWithKey(message.magic_name, nil),SPDStringWithKey(@"魔法", nil),SPDStringWithKey(@"成功", nil),SPDStringWithKey(@"TA", nil),[self getEffectWithString:message.magic_type],[NSString stringWithFormat:@"%@s",message.magic_effect_value]];

                }else{
                    self.magicDescLabel.text = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",SPDStringWithKey(@"你向对方使用", nil),SPDStringWithKey(message.magic_name, nil),SPDStringWithKey(@"魔法", nil),SPDStringWithKey(@"成功", nil),SPDStringWithKey(@"TA", nil),message.magic_effect_value,[self getEffectWithString:message.magic_type]];
                }
                
            }else{
                self.magicDescLabel.text = [NSString stringWithFormat:@"%@%@%@%@",SPDStringWithKey(@"你向对方使用", nil),SPDStringWithKey(message.magic_name, nil),SPDStringWithKey(@"魔法", nil),SPDStringWithKey(@"但是失败了", nil)];
            }
        }
        
    }else if(self.messageDirection == MessageDirection_RECEIVE ){
        
        CGRect desframe = self.magicDescLabel.frame;
        desframe.origin.x = 16;
        self.magicDescLabel.frame = desframe;
        NSString * effect; //inc des
        if ([message.magic_effect isEqualToString:@"inc"]) {
            effect = @"增加";
        }else{
            effect = @"减少";
        }
        if (message.senderUserInfo &&  message.magic_result && [ message.magic_result isEqualToString:@"success"]) {
            NSString * str ;
            if ([message.magic_type isEqualToString:@"charm"]) {
                str = [NSString stringWithFormat:@"%@魅力值",effect];
            }else{
                str = [NSString stringWithFormat:@"%@经验值",effect];
            }
            
            if ([message.magic_type isEqualToString:@"charm"] || [message.magic_type isEqualToString:@"exp"]) {
                    self.magicDescLabel.text = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",SPDStringWithKey(@"对方向你使用", nil),SPDStringWithKey(message.magic_name, nil),SPDStringWithKey(@"魔法", nil),SPDStringWithKey(@"成功", nil),SPDStringWithKey(@"你", nil), SPDStringWithKey(str, nil),message.magic_effect_value];
            }else if ([message.magic_type isEqualToString:@"banned_mic"] || [message.magic_type isEqualToString:@"banned_talk"]){
                self.magicDescLabel.text = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",SPDStringWithKey(@"对方向你使用", nil),SPDStringWithKey(message.magic_name, nil),SPDStringWithKey(@"魔法", nil),SPDStringWithKey(@"成功", nil),SPDStringWithKey(@"你", nil),[self getEffectWithString:message.magic_type],[NSString stringWithFormat:@"%@s",message.magic_effect_value]];
            }else{
                self.magicDescLabel.text = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",SPDStringWithKey(@"对方向你使用", nil),SPDStringWithKey(message.magic_name, nil),SPDStringWithKey(@"魔法", nil),SPDStringWithKey(@"成功", nil),SPDStringWithKey(@"你", nil),message.magic_effect_value,[self getEffectWithString:message.magic_type]];
            }
            
        }else{
            self.magicDescLabel.text = [NSString stringWithFormat:@"%@%@%@%@",SPDStringWithKey(@"对方向你使用", nil),SPDStringWithKey(message.magic_name, nil),SPDStringWithKey(@"魔法", nil),SPDStringWithKey(@"但是失败了", nil)];
        }
    }
    [self.magicDescLabel sizeToFit];
    
    if (self.messageDirection == MessageDirection_SEND) {
        [self.resultButton setTitle:SPDStringWithKey(@"再来一次", nil) forState:UIControlStateNormal];
    }else{
        if (message.magic_power) {
            [self.resultButton setTitle:SPDStringWithKey(@"感谢", nil) forState:UIControlStateNormal];
        }else{
            [self.resultButton setTitle:SPDStringWithKey(@"报仇", nil) forState:UIControlStateNormal];
        }
    }
}

- (NSString *)getEffectWithString:(NSString *)str {
    
    NSString* effect;
    if ([str isEqualToString:@"charm"]) {
        effect = SPDStringWithKey(@"魅力值", nil);
    }else if ([str isEqualToString:@"exp"]){
        effect = SPDStringWithKey(@"经验值", nil);
    }else if ([str isEqualToString:@"banned_talk"]){
        effect = SPDStringWithKey(@"被禁言", nil);
    }else if ([str isEqualToString:@"banned_mic"]){
        effect = SPDStringWithKey(@"被封麦", nil);
    }else if ([str isEqualToString:@"kicked"]){
        effect = SPDStringWithKey(@"内无法进入被踢出的房间", nil);
    }
    return effect;
}

- (UIView *)magicNameTitleView {
    if (!_magicNameTitleView) {
        _magicNameTitleView = [[UIImageView alloc]initWithFrame:CGRectMake(0, pixwn(10), pixwn(182), 23)];
        _magicNameTitleView.backgroundColor = [UIColor whiteColor];
        _magicNameTitleView.layer.cornerRadius = 11;
        _magicNameTitleView.clipsToBounds = YES;
        self.magicNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(pixwn(5), (23-13)/2,100, 13)];
        self.magicNameLabel.textColor = SPD_HEXCOlOR(@"666666");
        self.magicNameLabel.font = [UIFont systemFontOfSize:13];
        [_magicNameTitleView addSubview:self.magicNameLabel];
        [self.messageContentView addSubview:_magicNameTitleView];
    }
    return _magicNameTitleView;
}

- (void)handleMagicLevelWith:(NSInteger )magicLevel {
    if (magicLevel == 0) {
        magicLevel = 1;
    }
    NSInteger b = (magicLevel-1)% 5; //星星的个数
    NSInteger image_level = (magicLevel -1)/5; //那一种星星
    CGFloat magicLevelWidth = pixwn(12);
    CGFloat magicLevelInterval = 5;
    CGFloat leading = CGRectGetMaxX(self.magicNameLabel.frame)+5;
    //防止重复添加 rmove
    for (UIView * view in self.magicNameTitleView.subviews) {
        if (view == self.magicNameLabel) {
            continue;
        }
        [view removeFromSuperview];
    }
    
    //最多有5个
    for (int i = 0; i < 5;i++) {
        UIImageView * img = [[UIImageView alloc]init];
        img.frame = CGRectMake(leading, (23-13)*1.0/2, magicLevelWidth,magicLevelWidth);
        [self.magicNameTitleView addSubview:img];
        if (i <= b) {
            img.image = [UIImage imageNamed:[NSString stringWithFormat:@"MagicLevel%ld",image_level]];
        }else{
            UIImage *emptyImage = [UIImage imageNamed:[NSString stringWithFormat:@"MagicLevelEmpty%ld", (magicLevel - 1) / 15]];
            img.image = emptyImage;
        }
        leading = leading + magicLevelWidth +magicLevelInterval;
    }
}

//升级SDK之后-
+(CGSize)sizeForMessageModel:(RCMessageModel *)model withCollectionViewWidth:(CGFloat)collectionViewWidth referenceExtraHeight:(CGFloat)extraHeight{
    
    return CGSizeMake(kScreenW, 290.0/2+extraHeight);
}

-(void)handleResultBtnClicked:(UIButton*)sender {
    [self.delegate didTapMessageCell:self.model];
}

@end
