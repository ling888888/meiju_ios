//
//  ChatroomMoreFunctionView.m
//  SimpleDate
//
//  Created by ling Hou on 2019/12/25.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "ChatroomMoreFunctionView.h"
#import "SPDPresentFlowLayout.h"
#import "ZegoKitManager.h"
#import "ChatroomMoreFunctionCell.h"

static const NSInteger titleHeight = 44;
static const NSInteger collectionHeight = 200;

@interface ChatroomMoreFunctionView ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIView * bottomView;
@property (nonatomic, strong) UICollectionView * collectionView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIPageControl * pageControl;
@property (nonatomic, strong) NSMutableArray * dataArray;
@end

@implementation ChatroomMoreFunctionView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews {
    self.bottomView = [[UIView alloc]init];
    self.bottomView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kScreenH);
        make.leading.and.trailing.mas_equalTo(0);
        make.height.mas_equalTo(collectionHeight + IphoneX_Bottom);
    }];
    [self layoutIfNeeded];

    self.titleLabel = [[UILabel alloc]init];
    [self.bottomView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(titleHeight);
    }];
    self.titleLabel.textColor = SPD_HEXCOlOR(@"#333333");
    self.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    self.titleLabel.text = SPDStringWithKey(@"更多", nil);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    SPDPresentFlowLayout * layout = [[SPDPresentFlowLayout alloc]init];
    layout.rowCount = 2;
    layout.cellCountPerRow = 4;
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.collectionView registerNib:[UINib nibWithNibName:@"ChatroomMoreFunctionCell" bundle:nil] forCellWithReuseIdentifier:@"ChatroomMoreFunctionCell"];
    [self.bottomView addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bottomView.mas_top).offset(titleHeight);
        make.leading.mas_equalTo(0);
        make.trailing.mas_equalTo(0);
    }];
    
    self.pageControl = [[UIPageControl alloc]initWithFrame:CGRectZero];
    [self.bottomView addSubview:self.pageControl];
    [self.pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(self.bottomView.mas_bottom).offset(0);
    }];
    

    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hide)];
    tap.delegate = self;
    [self addGestureRecognizer:tap];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bottomView.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(10,10)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bottomView.bounds;
    maskLayer.path = maskPath.CGPath;
    self.bottomView.layer.mask = maskLayer;
}

- (void)setPositionLevel:(NSInteger)positionLevel {
    _positionLevel = positionLevel;
    if (_positionLevel >= 150) {
        [self.dataArray insertObject:@{@"image": @"ic_liaotianshi_gengduo_zhuti",
                                    @"type": @"background",
                                    @"title": @"主题"} atIndex:0];
        if (ZegoManager.isLock) {
            [self.dataArray insertObject:@{@"image": @"ic_liaotianshi_gengduo_xiugaimima",
                                           @"type": @"changepwd",
                                           @"title": @"修改密码"} atIndex:0];
            [self.dataArray insertObject:@{@"image": @"ic_liaotianshi_gengduo_jiesuo",
                                        @"type": @"unlock",
                                        @"title": @"解锁房间"} atIndex:0];
        }else{
            [self.dataArray insertObject:@{@"image": @"ic_liaotianshi_gengduo_suofang",
                                        @"type": @"lock",
                                        @"title": @"锁房"} atIndex:0];
        }
    }
    self.pageControl.numberOfPages = self.dataArray.count;
    self.pageControl.hidden = (self.dataArray.count == 1);
    NSInteger line = self.dataArray.count <= 4  ? 1 : 2;
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(line * 100 + 5 * 2);
    }];
    [self.bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(line * 100 + 5 * 2 + titleHeight  + IphoneX_Bottom);
    }];
    UICollectionViewFlowLayout * commonFlowLayout = [UICollectionViewFlowLayout new];
    commonFlowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    SPDPresentFlowLayout * layout = [[SPDPresentFlowLayout alloc]init];
    layout.rowCount = 2;
    layout.cellCountPerRow = 4;
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.collectionView.collectionViewLayout = self.dataArray.count <= 4 ? commonFlowLayout : layout;
    [self.collectionView reloadData];
    [self.bottomView layoutIfNeeded];
}

- (void)show {
    
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [self.bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(kScreenH - self.bottomView.bounds.size.height);
        }];
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)hide {
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(kScreenH);
        }];
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ChatroomMoreFunctionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatroomMoreFunctionCell" forIndexPath:indexPath];
    NSDictionary * dict = self.dataArray[indexPath.row];
    cell.titleImageView.image = [UIImage imageNamed:dict[@"image"]];
    cell.titleLabel.text = SPDStringWithKey(dict[@"title"], nil);
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self hide];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSDictionary * dict = self.dataArray[indexPath.row];
        if (self.delegate && self.delegate && [self.delegate respondsToSelector:@selector(chatroomMoreFunctionView:didClickedItem:)]) {
            [self.delegate chatroomMoreFunctionView:self didClickedItem:dict[@"type"]];
        }
    });
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((kScreenW -  5* 5)/4.0f, 100.0f);
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 5;
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    return ![touch.view isDescendantOfView:self.bottomView];
}

- (NSMutableArray *)dataArray {
    if ( nil == _dataArray) {
        _dataArray = [NSMutableArray arrayWithArray:@[@{@"image": @"ic_liaotianshi_gengduo_zhaohuan",
                                                        @"type": @"clan_call",
                                                        @"title": @"召唤"
                                                        },
                                                      @{@"image": @"ic_liaotianshi_gengduo_choujiang",
                                                        @"type": @"turn_table",
                                                        @"title": @"抽奖转盘"
                                                        }]];
    }
    return _dataArray;
}

@end
