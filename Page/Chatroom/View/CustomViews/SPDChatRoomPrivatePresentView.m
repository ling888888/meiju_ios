//
//  SPDChatRoomPrivatePresentView.m
//  SimpleDate
//
//  Created by Monkey on 2017/6/6.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDChatRoomPrivatePresentView.h"

@interface SPDChatRoomPrivatePresentView ()

@property (nonatomic,strong)UILabel *sender_userNameLabel;
@property (nonatomic,strong)UILabel *receiver_userNameLabel;
@property (nonatomic,strong)UIImageView *sender_avatarImageView;
@property (nonatomic,strong)UIImageView *receive_avatarImageView;
@property (nonatomic,strong)UIImageView *presentImageView;
@property(nonatomic,assign)__block BOOL isShowing;
@property (nonatomic,strong)NSMutableArray *dataArray;
@property (nonatomic,strong)UIView *backView;
@property (nonatomic, strong) UIImageView *numImageView;

@end

@implementation SPDChatRoomPrivatePresentView

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self configUI];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    return self;
}

-(void)configUI {
    
//    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
//    self.layer.cornerRadius = 10;
//    self.clipsToBounds = YES;
    
    if (self.sender_userNameLabel) {
        [self.sender_userNameLabel removeFromSuperview];
        self.sender_userNameLabel= nil;
    }
    if (self.sender_avatarImageView) {
        [self.sender_avatarImageView removeFromSuperview];
        self.sender_avatarImageView= nil;

    }
    
    if (self.receiver_userNameLabel) {
        [self.receiver_userNameLabel removeFromSuperview];
        self.receiver_userNameLabel= nil;

    }
    if (self.receive_avatarImageView) {
        [self.receive_avatarImageView removeFromSuperview];
        self.receive_avatarImageView= nil;

    }
    if (self.presentImageView) {
        [self.presentImageView removeFromSuperview];
        self.presentImageView= nil;

    }
    
    if (self.backView) {
        [self.backView removeFromSuperview];
        self.backView = nil;
    }
    [self initSubViews];
    
}

#define SPDChatRoomPrivatePresentViewHeight 30.0f
-(void)initSubViews {
    
    self.backView = [[UIView alloc]initWithFrame:self.bounds];
    [self addSubview:self.backView];
    self.backView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
     self.backView.layer.cornerRadius = 15;
     self.backView.clipsToBounds = YES;
    
    self.sender_avatarImageView = [[UIImageView alloc]initWithFrame:CGRectMake(5, (SPDChatRoomPrivatePresentViewHeight- 37.0/2)/2, 37.0/2, 37.0/2)];
    self.sender_avatarImageView.layer.cornerRadius = 37.0/4;
    self.sender_avatarImageView.clipsToBounds = YES;
    [self addSubview:self.sender_avatarImageView];
    
    self.sender_userNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.sender_avatarImageView.frame)+3, 0, 20, 12)];
    [self.sender_userNameLabel setFrame:CGRectMake(CGRectGetMaxX(self.sender_avatarImageView.frame),(SPDChatRoomPrivatePresentViewHeight-12)/2, 60, 12)];
    self.sender_userNameLabel.textColor = SPD_HEXCOlOR(@"3191f1");
    self.sender_userNameLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:self.sender_userNameLabel];
    
    self.receive_avatarImageView = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.sender_userNameLabel.frame), self.sender_avatarImageView.frame.origin.y, self.sender_avatarImageView.frame.size.width, self.sender_avatarImageView.frame.size.height)];
    self.receive_avatarImageView.layer.cornerRadius = 37.0/4;
    self.receive_avatarImageView.clipsToBounds = YES;
    [self addSubview:self.receive_avatarImageView];
    NSLog(@"receive_avatarImageView name %@",NSStringFromCGRect(self.receive_avatarImageView.frame));

    self.receiver_userNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.receive_avatarImageView.frame)+10,(SPDChatRoomPrivatePresentViewHeight-12)/2, 60, 12)];
    self.receiver_userNameLabel.font = [UIFont systemFontOfSize:12];

    [self addSubview:self.receiver_userNameLabel];
    NSLog(@"receive name %@",NSStringFromCGRect(self.receiver_userNameLabel.frame));
    self.presentImageView =[[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.receiver_userNameLabel.frame), 1.5, 27, 27)];
    self.presentImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:self.presentImageView];

    self.numImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.presentImageView.frame) + 3, 1, 36, 25)];
    self.numImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:self.numImageView];
}

-(void)setDict:(NSDictionary *)dict {
    _dict = dict;
    [self.dataArray addObject:dict];
    
    if (!_isShowing) {
        [self startAnimationWithDict:dict];
    }
    
}

-(void)startAnimationWithDict:(NSDictionary *)dict {
    
    [self.sender_avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:dict[@"sender_avatar"]]];
    
    NSString * str= dict [@"sender_name"];
    NSMutableAttributedString * sender_attr_str = [[NSMutableAttributedString alloc]init];
    if (str.length > 4) {
        NSString *cutstr= [str substringToIndex:4];
        if ([dict[@"sender_gender"] isEqualToString:@"male"]) {
             NSAttributedString * attr_cut = [[NSAttributedString alloc]initWithString:cutstr attributes:@{NSForegroundColorAttributeName:SPD_HEXCOlOR(@"3c91f1")}];
            [sender_attr_str appendAttributedString:attr_cut];
        }else{
            NSAttributedString * attr_cut = [[NSAttributedString alloc]initWithString:cutstr attributes:@{NSForegroundColorAttributeName:SPD_HEXCOlOR(@"fe69a1")}];
            [sender_attr_str appendAttributedString:attr_cut];
        }
        
        
    }else {
        NSString *cutstr = str;
        if ([dict[@"sender_gender"] isEqualToString:@"male"]) {
            NSAttributedString * attr_cut = [[NSAttributedString alloc]initWithString:cutstr attributes:@{NSForegroundColorAttributeName:SPD_HEXCOlOR(@"3c91f1")}];
            [sender_attr_str appendAttributedString:attr_cut];
        }else{
            NSAttributedString * attr_cut = [[NSAttributedString alloc]initWithString:cutstr attributes:@{NSForegroundColorAttributeName:SPD_HEXCOlOR(@"fe69a1")}];
            [sender_attr_str appendAttributedString:attr_cut];
        }
    }
    
    NSAttributedString * another_str = [[NSAttributedString alloc]initWithString:SPDStringWithKey(@"送给", nil) attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [sender_attr_str appendAttributedString:another_str];
    self.sender_userNameLabel.attributedText = sender_attr_str;
    [self.sender_userNameLabel sizeToFit];
    CGRect sender_userNameLabel_frame = self.sender_userNameLabel.frame;
    sender_userNameLabel_frame.origin.x = CGRectGetMaxX(self.sender_avatarImageView.frame) + 3;
    self.sender_userNameLabel.frame = sender_userNameLabel_frame;
   
    //cgrect
    CGRect receive_avatar_frame = self.receive_avatarImageView.frame;
    receive_avatar_frame.origin.x= CGRectGetMaxX(self.sender_userNameLabel.frame)+3;
    self.receive_avatarImageView.frame = receive_avatar_frame;
    
    [self.receive_avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:dict[@"receive_avatar"]]];
    
    NSMutableString * receiver_name = dict[@"receive_name"];
    if (receiver_name.length > 4) {
     self.receiver_userNameLabel.text= [receiver_name substringToIndex:4];
    }else {
         self.receiver_userNameLabel.text= receiver_name;
    }
    [self.receiver_userNameLabel sizeToFit];
    
    //cgrect
    CGRect receiver_userNameLabel_frame = self.receiver_userNameLabel.frame;
    receiver_userNameLabel_frame.origin.x= CGRectGetMaxX(self.receive_avatarImageView.frame)+3;
    self.receiver_userNameLabel.frame = receiver_userNameLabel_frame;
    
    if ([dict[@"receive_gender"] isEqualToString:@"male"]) {
        self.receiver_userNameLabel.textColor = SPD_HEXCOlOR(@"3c91f1");
    }else{
        self.receiver_userNameLabel.textColor = SPD_HEXCOlOR(@"fe69a1");
    }
    
    [self.presentImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",dict[@"present_url"]]]];
    
    //cgrect
    CGRect presentImageView_frame = self.presentImageView.frame;
    presentImageView_frame.origin.x= CGRectGetMaxX(self.receiver_userNameLabel.frame)+3;
    self.presentImageView.frame = presentImageView_frame;
    
    self.numImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"gift_num_s_%@", dict[@"num"]]];
    CGRect numImageViewFrame = self.numImageView.frame;
    numImageViewFrame.origin.x = CGRectGetMaxX(self.presentImageView.frame) + 3;
    self.numImageView.frame = numImageViewFrame;
    
    //change self frame
    CGRect myframe = self.frame;
    myframe.size.width = CGRectGetMaxX(self.numImageView.frame) + 5;
    self.frame = myframe;
    
    CGRect backframe = self.backView.frame;
    backframe.size.width = myframe.size.width;
    self.backView .frame = backframe;
    
    [UIView animateWithDuration:6.0f delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        CGRect myframe = self.frame;
        myframe.origin.x = - self.frame.size.width;
        self.frame = myframe;
        self.isShowing =YES;
    } completion:^(BOOL finished) {
        
        [self clearData];
        
        if (finished) {
            //self view change
            CGRect myframe = self.frame;
            myframe.origin.x = kScreenW;
            myframe.size.width = 191;
            self.frame = myframe;
            self.isShowing = NO;
            [self.dataArray removeObjectAtIndex:0];
            if (self.dataArray.count > 0 ) {
                [self startAnimationWithDict:_dataArray[0]];
            }
        }else{
            self.isShowing = NO;
            [self.dataArray removeAllObjects];
            CGRect myframe = self.frame;
            myframe.origin.x = kScreenW;
            myframe.size.width = 191;
            self.frame = myframe;
        }

        
     }];
    
}

-(void)clearData {
   
    self.sender_userNameLabel.text = @"";
    self.receiver_userNameLabel.text = @"";
    self.sender_avatarImageView.image= [UIImage imageNamed:@""];
    self.receive_avatarImageView.image = [UIImage imageNamed:@""];
    self.presentImageView.image = [UIImage imageNamed:@""];
    
}


- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}


@end
