//
//  SPDVoiceLiveGlobalScrollView.h
//  SimpleDate
//
//  Created by 李楠 on 17/5/31.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SPDVoiceLiveGlobalScrollViewDelegate <NSObject>

@optional

- (void)beginScrollWithDic:(NSDictionary *)dic;
- (void)didResetView:(UIView *)view;

@end

@interface SPDVoiceLiveGlobalScrollView : UIView

@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, copy) NSDictionary *dic;
@property (nonatomic, assign) CGFloat space;
@property (nonatomic, assign) CGFloat speed;
@property (nonatomic, assign) BOOL isRepeat;
@property (nonatomic, assign) BOOL hideAfterAnimation;
@property (nonatomic, weak) id<SPDVoiceLiveGlobalScrollViewDelegate> delegate;

- (void)removeAnimaton;
- (void)resumeAnimation;

@end
