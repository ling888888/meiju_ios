//
//  SPDChatRoomAdminCell.m
//  SimpleDate
//
//  Created by 李楠 on 17/6/6.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDChatRoomAdminCell.h"

@interface SPDChatRoomAdminCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end

@implementation SPDChatRoomAdminCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setType:(NSString *)type {
    _type = type;
    
    self.textLabel.hidden = YES;
    if ([type isEqualToString:@"gift"] || [type isEqualToString:@"magic"]) {
        self.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"chatroomuser_%@", type]];
    } else if ([type isEqualToString:@"recruit"]) {
        self.imageView.image = [UIImage imageNamed:@"chatroomuser_recruit"];
        self.textLabel.hidden = NO;
        self.textLabel.text = SPDStringWithKey(@"招", nil);
    } else if ([type isEqualToString:SPDStringWithKey(@"上麦", nil)] || [type isEqualToString:SPDStringWithKey(@"帮他上麦", nil)]) {
        self.imageView.image = [UIImage imageNamed:@"chatroomuser_up"];
    } else if ([type isEqualToString:SPDStringWithKey(@"下麦旁听", nil)] || [type isEqualToString:SPDStringWithKey(@"设为旁听", nil)]) {
        self.imageView.image = [UIImage imageNamed:@"chatroomuser_down"];
    } else if ([type isEqualToString:SPDStringWithKey(@"禁言", nil)]) {
        self.imageView.image = [UIImage imageNamed:@"chatroomuser_forbid"];
    } else if ([type isEqualToString:SPDStringWithKey(@"解除禁言", nil)]) {
        self.imageView.image = [UIImage imageNamed:@"chatroomuser_release"];
    } else if ([type isEqualToString:SPDStringWithKey(@"禁麦", nil)]) {
        self.imageView.image = [UIImage imageNamed:@"chatroomuser_forbidmic"];
    } else if ([type isEqualToString:SPDStringWithKey(@"解除禁麦", nil)]) {
        self.imageView.image = [UIImage imageNamed:@"chatroomuser_releasemic"];
    } else if ([type isEqualToString:SPDStringWithKey(@"封麦", nil)]) {
        self.imageView.image = [UIImage imageNamed:@"chatroomuser_closemic"];
    } else if ([type isEqualToString:SPDStringWithKey(@"踢出房间", nil)]) {
        self.imageView.image = [UIImage imageNamed:@"chatroomuser_kickout"];
    }
}

@end
