//
//  SPDChatroomOnlineUserView.h
//  SimpleDate
//
//  Created by 李楠 on 2018/12/27.
//  Copyright © 2018 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SPDVoiceLiveUserModel;

@protocol SPDChatroomOnlineUserViewDelegate <NSObject>

@optional

- (void)didSelectUserWithModel:(SPDVoiceLiveUserModel *)model;

@end

NS_ASSUME_NONNULL_BEGIN

@interface SPDChatroomOnlineUserView : UIView

@property (nonatomic, weak) id<SPDChatroomOnlineUserViewDelegate> delegate;
@property (nonatomic, strong) NSMutableArray *dataArray;

- (void)show;

@end

NS_ASSUME_NONNULL_END
