//
//  SPDHomeChatRoomView.h
//  SimpleDate
//
//  Created by Monkey on 2017/6/7.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SPDHomeChatRoomViewDelegate<NSObject>

@optional
-(void)sphHomeclickedClanWithId:(NSString *)clan_id andClan_name:(NSString *)clan_name;

@end
@interface SPDHomeChatRoomView : UIView

@property (nonatomic,copy)NSDictionary *dict;
@property(nonatomic,weak)id<SPDHomeChatRoomViewDelegate> deleage;

@end
