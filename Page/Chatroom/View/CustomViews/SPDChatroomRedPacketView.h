//
//  SPDChatroomRedPacketView.h
//  SimpleDate
//
//  Created by 李楠 on 2018/9/3.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChatroomRedPacketModel;

@interface SPDChatroomRedPacketView : UIView

@property (nonatomic, strong) ChatroomRedPacketModel *model;

@end
