//
//  SPDVoiceLiveAudienceView.h
//  SimpleDate
//
//  Created by 李楠 on 17/5/18.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SPDVoiceLiveAudienceViewDelegate <NSObject>

- (void)didSelectAudience:(NSString *)userId index:(NSInteger)index;

@end

@interface SPDVoiceLiveAudienceView : UIView<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) NSArray *dataArray;
@property (assign, nonatomic) NSInteger index;
@property (weak, nonatomic) id<SPDVoiceLiveAudienceViewDelegate> delegate;

@end
