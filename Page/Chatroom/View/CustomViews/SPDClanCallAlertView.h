//
//  SPDClanCallAlertView.h
//  SimpleDate
//
//  Created by Monkey on 2017/6/22.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPDClanCallAlertView : UIView

@property (nonatomic,assign) BOOL is_Vip;

@property (weak, nonatomic) IBOutlet UIButton *UseMoneyToCallButton;

@property (weak, nonatomic) IBOutlet UIButton *isVipFreeCallButton;


@property(nonatomic) void (^vipButtonClicked)();

@property(nonatomic) void (^useMoneyButtonClicked)();

@property(nonatomic) void (^cancleButtonClicked)();

@end
