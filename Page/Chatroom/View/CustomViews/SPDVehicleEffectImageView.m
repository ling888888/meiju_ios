//
//  SPDVehicleEffectImageView.m
//  SimpleDate
//
//  Created by 李楠 on 2017/11/13.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDVehicleEffectImageView.h"
#import "SPDJoinChatRoomMessage.h"
#import "SVGA.h"
#import "LiveJoinMessage.h"

@interface SPDVehicleEffectImageView ()<CAAnimationDelegate, SVGAPlayerDelegate>

@property (nonatomic, strong) NSMutableArray *messageArray;
@property (nonatomic, assign) BOOL isShowing;
@property (nonatomic, strong) UIImageView *avatarImageView;
@property (nonatomic, strong) UILabel *nickNameLabel;
@property (nonatomic, strong) SPDJoinChatRoomMessage *currentMessage;
@property (nonatomic, strong) SVGAPlayer *player;
@property (nonatomic, strong) SVGAParser *parser;

@end

@implementation SPDVehicleEffectImageView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.contentMode = UIViewContentModeScaleAspectFit;
        self.hidden = YES;
        
        self.player = [[SVGAPlayer alloc] initWithFrame:self.bounds];
        self.player.contentMode = UIViewContentModeScaleAspectFit;
        self.player.loops = 1;
        self.player.clearsAfterStop = YES;
        self.player.delegate = self;
        [self addSubview:self.player];
        self.parser = [[SVGAParser alloc] init];
    }
    return self;
}

// 聊天室进场消息
- (void)setMessage:(SPDJoinChatRoomMessage *)message {
    _message = message;
    
    [self.messageArray addObject:message];
    if (!self.isShowing) {
        [self startAnimatingWithMessage:_message];
    }
}

// 直播间进场消息

- (void)setLiveJoinMessage:(LiveJoinMessage *)liveJoinMessage {
    _liveJoinMessage = liveJoinMessage;
    [self.messageArray addObject:_liveJoinMessage];
    if (!self.isShowing) {
        [self startAnimatingWithMessage:_liveJoinMessage];
    }
}

- (void)startAnimatingWithMessage:(RCMessageContent *)messageContent {
    self.isShowing = YES;
    if ([messageContent isKindOfClass:[SPDJoinChatRoomMessage class]]) {
        self.currentMessage = (SPDJoinChatRoomMessage *)messageContent;
        [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:self.currentMessage.senderUserInfo.portraitUri]];
        self.nickNameLabel.text = self.currentMessage.senderUserInfo.name;
        if ([self.currentMessage.gender isEqualToString:@"female"]) {
            self.nickNameLabel.textColor = [UIColor colorWithHexString:@"fe69a1"];
        } else {
            self.nickNameLabel.textColor = [UIColor colorWithHexString:@"57a3f9"];
        }
        [self startAnimatingWithVehicleId:self.currentMessage.vehicle_id];
    }else{
        self.currentMessage = (LiveJoinMessage *)messageContent;
        LiveJoinMessage * msg = (LiveJoinMessage *)messageContent;
        [self.avatarImageView fp_setImageWithURLString:msg.avatar];
        self.nickNameLabel.text = msg.userName;
        self.nickNameLabel.textColor = [UIColor colorWithHexString:@"ffffff"];
        [self startAnimatingWithVehicleId:msg.vehicleId];
    }
    
}


- (void)startAnimatingWithVehicleId:(NSString *)vehicleId {
    self.isShowing = YES;
    [self.parser parseWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://%@famy.17jianyue.cn/svga/%@.svga", DEV ? @"dev-" : @"", vehicleId]]
              completionBlock:^(SVGAVideoEntity * _Nullable videoItem) {
                  if (videoItem != nil) {
                      self.player.videoItem = videoItem;
                      [self.player startAnimation];
                      if (self.currentMessage.senderUserInfo || ![SPDCommonTool isEmpty:[self.currentMessage valueForKey:@"userName"]]) {
                          [self showInformation];
                      }
                  } else {
                      [self finishAnimating];
                  }
              } failureBlock:^(NSError * _Nullable error) {
                  [self finishAnimating];
              }];
}

- (void)showInformation {
    self.avatarImageView.hidden = NO;
    self.nickNameLabel.hidden = NO;
}

- (void)hideInformation {
    self.avatarImageView.hidden = YES;
    self.nickNameLabel.hidden = YES;
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    [self finishAnimating];
}

- (void)finishAnimating {
    [self hideInformation];
    if (self.messageArray.count) {
        [self.messageArray removeObjectAtIndex:0];
        if (self.messageArray.count) {
            [self startAnimatingWithMessage:self.messageArray[0]];
        } else {
            self.isShowing = NO;
        }
    } else {
        self.isShowing = NO;
    }
}

+ (NSMutableArray *)getAnimationImagesWithVehicleId:(NSString *)vehicleId isCGImage:(BOOL)isCGImage {
    NSString *vehicleImagePath = [NSHomeDirectory() stringByAppendingPathComponent:@"/Library/Caches/vehicle"];
    NSMutableArray *array = [NSMutableArray array];
    if ([[NSFileManager defaultManager] fileExistsAtPath:vehicleImagePath]) {
        NSArray *fileList = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:vehicleImagePath error:nil];
        NSInteger imageCount = 0;
        for (NSString *fileName in fileList) {
            if ([fileName containsString:vehicleId]) {
                imageCount++;
            }
        }
        for (NSInteger i = 0; i < imageCount; i++) {
            NSString *path = [vehicleImagePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_%ld.png", vehicleId, i]];
            UIImage *image = [UIImage imageWithContentsOfFile:path];
            if (image) {
                if (isCGImage) {
                    [array addObject:(id)image.CGImage];
                } else {
                    [array addObject:image];
                }
            }
        }
        return array;
    }
    return array;
}

#pragma mark - SVGAPlayerDelegate

- (void)svgaPlayerDidFinishedAnimation:(SVGAPlayer *)player {
    [self finishAnimating];
}

#pragma mark - Getters & Setters

- (NSMutableArray *)messageArray {
    if (!_messageArray) {
        _messageArray = [NSMutableArray array];
    }
    return _messageArray;
}

- (UIImageView *)avatarImageView {
    if (!_avatarImageView) {
        _avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 45, 45)];
        _avatarImageView.center = CGPointMake(self.bounds.size.width / 2 * 132.5 / 187.5, self.bounds.size.height / 2  * 162 / 224);
        _avatarImageView.layer.cornerRadius = 45 / 2;
        _avatarImageView.clipsToBounds = YES;
        _avatarImageView.hidden = YES;
        [self addSubview:_avatarImageView];
    }
    return _avatarImageView;
}

- (UILabel *)nickNameLabel {
    if (!_nickNameLabel) {
        _nickNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.avatarImageView.frame) + 10, self.avatarImageView.frame.origin.y, 200, self.avatarImageView.frame.size.height)];
        _nickNameLabel.textAlignment = NSTextAlignmentLeft;
        _nickNameLabel.font = [UIFont systemFontOfSize:16];
        _nickNameLabel.hidden = YES;
        [self addSubview:_nickNameLabel];
    }
    return _nickNameLabel;
}

- (void)setIsShowing:(BOOL)isShowing {
    _isShowing = isShowing;
    self.hidden = !_isShowing;
}

@end
