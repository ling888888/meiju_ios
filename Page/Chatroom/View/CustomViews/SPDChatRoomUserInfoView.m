//
//  SPDChatRoomUserInfoView.m
//  SimpleDate
//
//  Created by Monkey on 2017/6/5.
//  Copyright © 2017年 WYB. All rights reserved.
//


#import "SPDChatRoomUserInfoView.h"
#import "SPDChatRoomAdminCell.h"
#import "NSString+XXWAddition.h"
#import "SPDCommonDefine.h"
#import "HomeModel.h"
#import "LiveRoomUserInfoMedalView.h"
#import "PetView.h"
#import "PetModel.h"
#import "LookPetView.h"

@interface SPDChatRoomUserInfoView ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet UIView *selfView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *nobleDecorationImageView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *nobleAvatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *agentServiceImageView;
@property (weak, nonatomic) IBOutlet UIImageView *vehicleImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *ageBtn;
@property (weak, nonatomic) IBOutlet UIButton *levelBtn;
@property (weak, nonatomic) IBOutlet UIImageView *nobleMedalImageView;
@property (weak, nonatomic) IBOutlet UILabel *idLabel;
@property (weak, nonatomic) IBOutlet UIImageView *specialNumImageView;
@property (nonatomic, strong) LiveRoomUserInfoMedalView * medalView;
@property (weak, nonatomic) IBOutlet UICollectionView *sendCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *adminCollectionView;

@property (strong, nonatomic) NSMutableArray *adminArray;
@property (strong, nonatomic) NSMutableArray *sendArray;
@property (copy, nonatomic) NSString *nickName;
@property (copy, nonatomic) NSString *avatar;
@property (copy, nonatomic) NSString *gender;
@property (assign, nonatomic) CGFloat adminTop;
@property (nonatomic, strong) HomeModel * userModel;

@property(nonatomic, strong) LY_PortraitView *portraitView;

@property (nonatomic, strong) UIImageView *personImageView;
@property (nonatomic, strong) PetView *petView;

@end

@implementation SPDChatRoomUserInfoView

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
    }
    return _portraitView;
}

+ (instancetype)chatRoomUserInfoView {
    return [[self alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH)];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self loadSelfView];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self loadSelfView];
    }
    return self;
}

- (void)loadSelfView {
    [[NSBundle mainBundle] loadNibNamed:@"SPDChatRoomUserInfoView" owner:self options:nil];
    self.selfView.frame = CGRectMake(0, 0, kScreenW, kScreenH);
    [self addSubview:self.selfView];
    
    UITapGestureRecognizer *tapAvatarImageView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAvatarImageView:)];
    [self.avatarImageView addGestureRecognizer:tapAvatarImageView];
    UILongPressGestureRecognizer *longPressAvatarImageView = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressAvatarImageView:)];
    [self.avatarImageView addGestureRecognizer:longPressAvatarImageView];
    
    [self.sendCollectionView registerNib:[UINib nibWithNibName:@"SPDChatRoomAdminCell" bundle:nil] forCellWithReuseIdentifier:@"SPDChatRoomAdminCell"];
    [self.adminCollectionView registerNib:[UINib nibWithNibName:@"SPDChatRoomAdminCell" bundle:nil] forCellWithReuseIdentifier:@"SPDChatRoomAdminCell"];
    
    [self.avatarImageView addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.avatarImageView);
        make.width.mas_equalTo(self.avatarImageView).multipliedBy(1.5);
        make.height.mas_equalTo(self.avatarImageView).multipliedBy(1.5);
    }];
    
    // 勋章墙
    [self.contentView addSubview:self.medalView];
    [self.medalView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(5);
        make.trailing.mas_equalTo(-5);
        make.top.equalTo(self.idLabel.mas_bottom).offset(8);
        make.height.mas_equalTo(70.0f/365*(kScreenW - 10));
    }];
    
    [self.contentView addSubview:self.personImageView];
    [self.contentView addSubview:self.petView];
    [self.personImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(4);
        make.bottom.equalTo(self.avatarImageView.mas_bottom).offset(0);
        make.width.mas_equalTo(75);
        make.height.mas_equalTo(177);
    }];
    [self.petView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.personImageView.mas_trailing).offset(-20);
        make.bottom.equalTo(self.avatarImageView.mas_bottom).offset(0);
        make.size.mas_equalTo(CGSizeMake(97, 102/92.0 * 97));
    }];
}

- (void)setUserId:(NSString *)userId {
    _userId = userId;
    

    [MBProgressHUD showHUDAddedTo:self animated:YES];
    
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:@{@"user_id": _userId}] bURL:@"user.info" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable dataDict) {
        
        self.userModel = [HomeModel initWithDictionary:dataDict];
        self.avatar = dataDict[@"avatar"];
        
        self.medalView.name = self.userModel.nick_name;
        self.medalView.userId = userId;

        self.portraitView.portrait = self.userModel.portrait;
        self.nickName = dataDict[@"nick_name"];
        self.nickNameLabel.text = self.nickName;
        self.agentServiceImageView.hidden = ![dataDict[@"is_agent_gm"] boolValue];
        for (NSDictionary *dic in dataDict[@"vehicleList"]) {
            if ([dic[@"is_drive"] boolValue]) {
                [self.vehicleImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, dic[@"image"]]]];
            }
        }
        
        self.gender = dataDict[@"gender"];
        if ([self.gender isEqualToString:@"male"]) {
            self.ageBtn.backgroundColor = [UIColor colorWithHexString:@"3c91f1"];
            [self.ageBtn setImage:[UIImage imageNamed:@"chatroomuser_male"] forState:UIControlStateNormal];
        } else {
            self.ageBtn.backgroundColor = [UIColor colorWithHexString:@"fe69a1"];
            [self.ageBtn setImage:[UIImage imageNamed:@"chatroomuser_female"] forState:UIControlStateNormal];
        }
        [self.ageBtn setTitle:[NSString stringWithFormat:@"%@", dataDict[@"age"]] forState:UIControlStateNormal];
        BOOL isArabic = [SPDCommonTool currentVersionIsArbic];
        if (isArabic) {
            [self.ageBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
            [self.ageBtn setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 2.5)];
        }
        
        int level = 0;
        if (dataDict[@"level"] != nil) {
            NSString *levelSign = dataDict[@"level"][@"sign"];
            NSString *secretStr = [NSString stringWithFormat:@"_id=%@&user_level=%d&key=%@", self.userId, [dataDict[@"level"][@"user_level"] intValue], jianyue_secretKey];
            if ([secretStr.SHA256AndBase64 isEqualToString:levelSign]) {
                level = [dataDict[@"level"][@"user_level"] intValue];
            }
        }
        [SPDCommonTool configDataForLevelBtnWithLevel:level andUIButton:self.levelBtn];
        
        CGFloat offset = isArabic ? 39.5 : 0;
//        self.avatarImageView.layer.borderWidth = 2.0f;
        if (dataDict[@"packages"]) {
            if (![SPDCommonTool isEmpty:dataDict[@"packages"][Headwear]]) {
//                self.nobleAvatarImageView.hidden = NO;
//                self.avatarImageView.layer.borderWidth = 0;
                NSString * str = [NSString stringWithFormat:@"%@",dataDict[@"packages"][Headwear]];
                [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:str] options:SDWebImageDownloaderUseNSURLCache progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                    
                } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIImage *img = [UIImage imageWithData:data scale:2.0f];
//                        self.nobleAvatarImageView.image = img;
                    });
                }];
            }else{
//                self.nobleAvatarImageView.hidden = YES;
            }
            if (![SPDCommonTool isEmpty:dataDict[@"packages"][Medal]]) {
                self.nobleMedalImageView.hidden = NO;
                [self.nobleMedalImageView sd_setImageWithURL:[NSURL URLWithString:dataDict[@"packages"][Medal]]];
                offset = isArabic ? 54.5 : -54.5;
            }else{
                self.nobleMedalImageView.hidden = YES;
            }
        }
        
        offset = isArabic ? 54.5 : -54.5;
        if (offset != 0) {
            [self.nickNameLabel mas_updateConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(self.avatarImageView.mas_centerX).with.offset(offset);
            }];
        }
        
        self.idLabel.text = dataDict[@"specialNum"] ? : [NSString stringWithFormat:@"ID:%@", dataDict[@"invite_code"]];
        self.idLabel.textColor =  dataDict[@"specialNum"] ? [UIColor colorWithHexString:@"F7D411"] :SPD_HEXCOlOR(@"A6B0BD");
        self.specialNumImageView.hidden = !dataDict[@"specialNum"];
        if (!self.specialNumImageView.hidden) {
            [self.idLabel mas_updateConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(self.avatarImageView.mas_centerX).with.offset([SPDCommonTool currentVersionIsArbic] ? -15 : 15);
            }];
        }
        
        [self.sendArray addObject:@"gift"];
        if ([_userId isEqualToString:[SPDApiUser currentUser].userId]) {
            if (self.positionLevel == 0 && self.micIndex < 0) {
                self.adminTop = 71;
            } else {
                self.adminTop = 0;
                if (self.micIndex >= 0) {
                    [self.adminArray addObject:SPDStringWithKey(@"下麦旁听", nil)];
                } else {
                    [self.adminArray addObject:SPDStringWithKey(@"上麦", nil)];
                }
            }
            [MBProgressHUD hideHUDForView:self animated:YES];
            [self show];
        } else {
            [self.sendArray addObject:@"magic"];
            if ([SPDCommonTool isEmpty:dataDict[@"clan_id"]] && self.positionLevel > 0) {
                [self.sendArray addObject:@"recruit"];
            }
            
            if (self.adminValue <= 0 || (!self.isInChatRoom && self.micIndex < 0)) {
                self.adminTop = 71;
                [MBProgressHUD hideHUDForView:self animated:YES];
                [self show];
            } else {
                self.adminTop = 0;
                NSDictionary *dic = @{@"chatroomId": self.clan_id, @"userId": userId};
                [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"chatroom.banned.query" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
                    if (self.micIndex >= 0) {
                        [self.adminArray addObject:SPDStringWithKey(@"设为旁听", nil)];
                        
                        if (self.micStatus == 3) {
                            [self.adminArray addObject:SPDStringWithKey(@"解除禁麦", nil)];
                        } else {
                            [self.adminArray addObject:SPDStringWithKey(@"禁麦", nil)];
                        }
                    } else {
                        [self.adminArray addObject:SPDStringWithKey(@"帮他上麦", nil)];
                    }

                    if ([suceess[@"status"] isEqualToString:@"banned"]) {
                        [self.adminArray insertObject:SPDStringWithKey(@"解除禁言", nil) atIndex:1];
                    } else {
                        [self.adminArray insertObject:SPDStringWithKey(@"禁言", nil) atIndex:1];
                    }
                    
                    if (self.positionLevel >= 75) {
                        if (self.micIndex >= 0) {
                            [self.adminArray addObject:SPDStringWithKey(@"封麦", nil)];
                        }
                        [self.adminArray addObject:SPDStringWithKey(@"踢出房间", nil)];
                    }
                    [MBProgressHUD hideHUDForView:self animated:YES];
                    [self show];
                } bFailure:^(id  _Nullable failure) {
                    [MBProgressHUD hideHUDForView:self animated:YES];
                    [self removeFromSuperview];
                }];
            }
        }
    } bFailure:^(id  _Nullable failure) {
        [MBProgressHUD hideHUDForView:self animated:YES];
        [self removeFromSuperview];
    }];
}

- (void)requestData {
    [LY_Network getRequestWithURL:LY_Api.shuxing parameters:@{@"user_id":self.userId?:@""} success:^(id  _Nullable response) {
        if (response[@"decorate"]) {
            NSArray * arr = response[@"decorate"];
            for (NSDictionary * dic in arr) {
                if ([dic[@"position"] isEqualToString:@"pet"]) {
                    PetModel * model = [PetModel initWithDictionary:dic];
                    self.petView.pet = model;
                    self.personImageView.hidden = false;
                    self.petView.hidden = false;
                }
            }
        }
    } failture:^(NSError * _Nullable error) {
    }];
}

- (void)show {
    [self.sendCollectionView reloadData];
    [self.adminCollectionView reloadData];
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.selfView).with.offset(self.adminTop);
        }];
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self requestData];
    }];
}

- (void)dismiss {
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.contentView.frame = CGRectMake(0, self.frame.size.height, self.frame.size.width, self.contentView.bounds.size.height);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (IBAction)tap:(UITapGestureRecognizer *)sender {
    [self dismiss];
}

- (void)tapAvatarImageView:(UITapGestureRecognizer *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didClickLookUserInfoBtn:)]) {
        [self dismiss];
        [self.delegate didClickLookUserInfoBtn:self.userId];
    }
}

- (void)longPressAvatarImageView:(UILongPressGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateBegan) {
        [self dismiss];
        NSDictionary *userInfo = @{@"user_id": self.userId, @"nick_name": self.nickName, @"avatar": [NSString stringWithFormat:STATIC_DOMAIN_URL, self.avatar], @"gender": self.gender,Headwear:self.userModel.portrait.headwearUrl?:@""};
        [[NSNotificationCenter defaultCenter] postNotificationName:CHATROOM_LONGPRESS object:nil userInfo:userInfo];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if ([collectionView isEqual:self.sendCollectionView]) {
        return self.sendArray.count;
    } else {
        return self.adminArray.count;
    }
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SPDChatRoomAdminCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SPDChatRoomAdminCell" forIndexPath:indexPath];
    if ([collectionView isEqual:self.sendCollectionView]) {
        cell.type = self.sendArray[indexPath.row];
    } else if ([collectionView isEqual:self.adminCollectionView]) {
        cell.type = self.adminArray[indexPath.row];
    }
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([collectionView isEqual:self.sendCollectionView]) {
        return CGSizeMake(kScreenW / self.sendArray.count, 70);
    } else {
        return CGSizeMake(kScreenW / self.adminArray.count, 70);
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([collectionView isEqual:self.sendCollectionView]) {
        if ([self.sendArray[indexPath.row] isEqualToString:@"recruit"]) {
            [self dismiss];
            NSDictionary *dic = @{@"clanId": self.clan_id, @"sendUserId": [SPDApiUser currentUser].userId, @"receiverUserId": self.userId, @"type": @"invite"};
            [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"invite.join" isClan:YES bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
                [SPDCommonTool showWindowToast:SPDStringWithKey(@"邀请发送成功", nil)];
            } bFailure:^(id  _Nullable failure) {
                if ([failure isKindOfClass:[NSDictionary class]]) {
                    [SPDCommonTool showWindowToast:failure[@"msg"]];
                }
            }];
        } else {
            if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectSend:userModel:)]) {
                [self dismiss];
                [self.delegate didSelectSend:self.sendArray[indexPath.row] userModel:self.userModel];
            }
        }
    } else if ([collectionView isEqual:self.adminCollectionView]) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectAdmin:userId:index:nickName:)]) {
            [self dismiss];
            [self.delegate didSelectAdmin:self.adminArray[indexPath.row] userId:self.userId index:self.micIndex nickName:self.nickName];
        }
    }
}

- (void)handlePetTap:(UITapGestureRecognizer *)tap {
    [LY_Network getRequestWithURL:LY_Api.shuxing parameters:@{@"user_id":self.userId?:@""} success:^(id  _Nullable response) {
        if (response[@"decorate"]) {
            NSArray * arr = response[@"decorate"];
            for (NSDictionary * dic in arr) {
                if ([dic[@"position"] isEqualToString:@"pet"]) {
                    LookPetView * fuc = [LookPetView new];
                    [fuc setDict:[NSMutableDictionary dictionaryWithDictionary:dic]];
                    [fuc present];
                    return;
                }
            }
        }
    } failture:^(NSError * _Nullable error) {
        [self showTips:error.localizedDescription];
    }];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ![touch.view isDescendantOfView:self.contentView];
}

#pragma mark - Getters and Setters

- (NSMutableArray *)adminArray {
    if (!_adminArray) {
        _adminArray = [NSMutableArray array];
    }
    return _adminArray;
}

- (NSMutableArray *)sendArray {
    if (!_sendArray) {
        _sendArray = [NSMutableArray array];
    }
    return _sendArray;
}

- (LiveRoomUserInfoMedalView *)medalView {
    if (!_medalView) {
        _medalView = [[LiveRoomUserInfoMedalView alloc]init];
    }
    return _medalView;
}

- (UIImageView *)personImageView {
    if (!_personImageView) {
        _personImageView = [UIImageView new];
        _personImageView.image = [[SPDApiUser currentUser].gender isEqualToString:@"male"] ? [UIImage imageNamed:@"img_nanshneg"] : [UIImage imageNamed:@"img_nvsheng"];
        _personImageView.hidden = true;
    }
    return _personImageView;
}

- (PetView *)petView {
    if (!_petView) {
        _petView = [PetView new];
        [_petView setLevelBGColor:[UIColor colorWithHexString:@"#00D8C9"]];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handlePetTap:)];
        [_petView addGestureRecognizer:tap];
        _petView.hidden = YES;
    }
    return _petView;
}


@end
