//
//  SPDChatroomOnlineUserCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/12/27.
//  Copyright © 2018 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SPDVoiceLiveUserModel;

NS_ASSUME_NONNULL_BEGIN

@interface SPDChatroomOnlineUserCell : UICollectionViewCell

@property (strong, nonatomic) SPDVoiceLiveUserModel *model;

@end

NS_ASSUME_NONNULL_END
