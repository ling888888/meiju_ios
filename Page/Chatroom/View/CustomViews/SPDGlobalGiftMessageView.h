//
//  SPDGlobalGiftMessageView.h
//  SimpleDate
//
//  Created by 李楠 on 17/7/13.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPDGlobalGiftMessageView : UIView

@property (nonatomic, copy) NSDictionary *dic;

@end
