//
//  SPDGlobalGiftMessageView.m
//  SimpleDate
//
//  Created by 李楠 on 17/7/13.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDGlobalGiftMessageView.h"

@interface SPDGlobalGiftMessageView ()

//@property (nonatomic, strong) UIImageView *senderAvatar;
@property (nonatomic, strong) UILabel *senderNickName;
@property (nonatomic, strong) UILabel *sendLabel;
//@property (nonatomic, strong) UIImageView *receiverAvater;
@property (nonatomic, strong) UILabel *receiverNickName;
@property (nonatomic, strong) UILabel *useLabel;
@property (nonatomic, strong) UIImageView *giftImage;
@property (nonatomic, strong) UILabel *magicResultLabel;
//@property (nonatomic,strong)UIImageView * senderNobleAvatar;
//@property (nonatomic,strong)UIImageView * receiveNobleAvatar;
@property (nonatomic, strong) LY_PortraitView * portraitView; // send
@property (nonatomic, strong) LY_PortraitView * receivePortraitView; // receive

@end

@implementation SPDGlobalGiftMessageView

- (instancetype)init {
    if (self = [super init]) {
//        self.senderAvatar = [[UIImageView alloc] init];
//        self.senderAvatar.clipsToBounds = YES;
//        self.senderAvatar.layer.borderColor = [UIColor colorWithHexString:@"fcfdff"].CGColor;
//        [self addSubview:_senderAvatar];
//
//        self.senderNobleAvatar = [UIImageView new];
//        [self addSubview:self.senderNobleAvatar];
        
        self.portraitView = [[LY_PortraitView alloc]init];
        [self addSubview:self.portraitView];
        
        self.senderNickName = [[UILabel alloc] init];
        [self addSubview:_senderNickName];
        
        self.sendLabel = [[UILabel alloc] init];
        [self addSubview:_sendLabel];
        
//        self.receiverAvater = [[UIImageView alloc] init];
//        self.receiverAvater.clipsToBounds = YES;
//        self.receiverAvater.layer.borderColor = [UIColor colorWithHexString:@"fcfdff"].CGColor;
//        [self addSubview:_receiverAvater];
//
//        self.receiveNobleAvatar = [UIImageView new];
//        [self addSubview:self.receiveNobleAvatar];
        self.receivePortraitView = [[LY_PortraitView alloc]init];
        [self addSubview:_receivePortraitView];
        
        self.receiverNickName = [[UILabel alloc] init];
        [self addSubview:_receiverNickName];
        
        self.useLabel = [[UILabel alloc] init];
        self.useLabel.text = SPDStringWithKey(@"使用", nil);
        [self addSubview:_useLabel];
        
        self.giftImage = [[UIImageView alloc] init];
        self.giftImage.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_giftImage];
        
        self.magicResultLabel = [[UILabel alloc] init];
        [self addSubview:_magicResultLabel];
    }
    return self;
}

- (void)setDic:(NSDictionary *)dic {
    _dic = dic;
    
    CGFloat y = 5;
    CGFloat height = self.superview.bounds.size.height - y * 2;
//    CGFloat borderWidth = height > 30 ? 1 : 0;
    CGFloat space = height > 30 ? 10 : 5;
    CGFloat fontSize = height > 30 ? 14 : 11;
    NSString *textColor = height > 30 ? @"fcfdff" : nil;
    
//    [self.senderAvatar sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:dic[@"savatar"]]];
    self.portraitView.frame = CGRectMake(0, y, height, height);
    
    LY_Portrait * avatar = [[LY_Portrait alloc]init];
    avatar.headwearUrl = dic[@"send_headwear"];
    avatar.url = dic[@"savatar"];
    self.portraitView.portrait = avatar;
//    self.senderAvatar.layer.cornerRadius = height / 2;
//    self.senderAvatar.layer.borderWidth = borderWidth;
    
//    if (![SPDCommonTool isEmpty:dic[@"send_headwear"]]) {
//        self.senderNobleAvatar.bounds = CGRectMake(0, 0, height * 1.19, height * 1.19);
//        self.senderNobleAvatar.center = CGPointMake(self.senderAvatar.center.x, self.senderAvatar.center.y);
//        [self.senderNobleAvatar sd_setImageWithURL:[NSURL URLWithString:dic[@"send_headwear"]]];
//    }
    
    self.senderNickName.text = [NSString stringWithFormat:@"%@", dic[@"sender_nickname"]];
    self.senderNickName.font = [UIFont systemFontOfSize:fontSize];
    if (textColor) {
        self.senderNickName.textColor = [UIColor colorWithHexString:textColor];
    } else if ([dic[@"sgender"] isEqualToString:@"male"]) {
        self.senderNickName.textColor = [UIColor colorWithHexString:@"65adff"];
    } else {
        self.senderNickName.textColor = [UIColor colorWithHexString:@"fe69a1"];
    }
    [self.senderNickName sizeToFit];
    CGRect senderNickNameFrame = CGRectMake(CGRectGetMaxX(self.portraitView.frame) + space, y, 0, height);
    senderNickNameFrame.size.width = _senderNickName.bounds.size.width;
    self.senderNickName.frame = senderNickNameFrame;
    
    self.sendLabel.text = SPDStringWithKey([dic[@"type"] isEqualToString:@"magic"] ? @"向" : @"送给", nil);
    self.sendLabel.font = [UIFont systemFontOfSize:fontSize];
    self.sendLabel.textColor = [UIColor colorWithHexString:textColor ? : @"f8f8f8"];
    [self.sendLabel sizeToFit];
    CGRect sendLabelFrame = CGRectMake(CGRectGetMaxX(self.senderNickName.frame) + space, y, 0, height);
    sendLabelFrame.size.width = _sendLabel.bounds.size.width;
    self.sendLabel.frame = sendLabelFrame;
    
    self.receivePortraitView.frame = CGRectMake(CGRectGetMaxX(self.sendLabel.frame) + space, y, height, height);
    LY_Portrait * ravatar = [[LY_Portrait alloc]init];
    ravatar.headwearUrl = dic[@"receive_headwear"];
    ravatar.url = dic[@"ravatar"];
    self.receivePortraitView.portrait = ravatar;
    
//    [self.receiverAvater sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:dic[@"ravatar"]]];
//    self.receiverAvater.frame = CGRectMake(CGRectGetMaxX(self.sendLabel.frame) + space, y, height, height);
//    self.receiverAvater.layer.cornerRadius = height / 2;
//    self.receiverAvater.layer.borderWidth = borderWidth;
//
//    if (![SPDCommonTool isEmpty:dic[@"receive_headwear"]]) {
//        self.receiveNobleAvatar.bounds = CGRectMake(0, 0, height * 1.19, height * 1.19);
//        self.receiveNobleAvatar.center = CGPointMake(self.receiverAvater.center.x, self.receiverAvater.center.y);
//        [self.receiveNobleAvatar sd_setImageWithURL:[NSURL URLWithString:dic[@"receive_headwear"]]];
//    }
    
    self.receiverNickName.text = [NSString stringWithFormat:@"%@", dic[@"receive_nickname"]];
    self.receiverNickName.font = [UIFont systemFontOfSize:fontSize];
    if (textColor) {
        self.receiverNickName.textColor = [UIColor colorWithHexString:textColor];
    } else if ([dic[@"rgender"] isEqualToString:@"male"]) {
        self.receiverNickName.textColor = [UIColor colorWithHexString:@"65adff"];
    } else {
        self.receiverNickName.textColor = [UIColor colorWithHexString:@"fe69a1"];
    }
    [self.receiverNickName sizeToFit];
    CGRect receiverNickNameFrame = CGRectMake(CGRectGetMaxX(self.receivePortraitView.frame) + space, y, 0, height);
    receiverNickNameFrame.size.width = _receiverNickName.bounds.size.width;
    self.receiverNickName.frame = receiverNickNameFrame;
    
    if ([dic[@"type"] isEqualToString:@"magic"]) {
        self.useLabel.hidden = NO;
        self.useLabel.font = [UIFont systemFontOfSize:fontSize];
        self.useLabel.textColor = [UIColor colorWithHexString:textColor ? : @"f8f8f8"];
        [self.useLabel sizeToFit];
        CGRect useLabelFrame = CGRectMake(CGRectGetMaxX(self.receiverNickName.frame) + space, y, 0, height);
        useLabelFrame.size.width = _useLabel.bounds.size.width;
        self.useLabel.frame = useLabelFrame;
        
        [self.giftImage sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:dic[@"gift_url"]]];
        self.giftImage.frame = CGRectMake(CGRectGetMaxX(self.useLabel.frame) + space, 0, self.superview.bounds.size.height, self.superview.bounds.size.height);
        
        self.magicResultLabel.text = SPDStringWithKey([dic[@"result"] isEqualToString:@"success"] ? @"成功" : @"失败", nil);
        self.magicResultLabel.font = [UIFont systemFontOfSize:fontSize];
        self.magicResultLabel.textColor = [UIColor colorWithHexString:textColor ? : @"f8f8f8"];
        [self.magicResultLabel sizeToFit];
        CGRect magicResultLabelFrame = CGRectMake(CGRectGetMaxX(self.giftImage.frame) + space, y, 0, height);
        magicResultLabelFrame.size.width = _magicResultLabel.bounds.size.width;
        self.magicResultLabel.frame = magicResultLabelFrame;
        
        CGFloat selfWidth = height + space + _senderNickName.bounds.size.width + space + _sendLabel.bounds.size.width + space + height + space + _receiverNickName.bounds.size.width + space + _useLabel.bounds.size.width + space + self.superview.bounds.size.height + space + _magicResultLabel.bounds.size.width;
        self.frame = CGRectMake(self.superview.bounds.size.width, 0, selfWidth, self.superview.bounds.size.height);
    } else {
        self.useLabel.hidden = YES;
        
        [self.giftImage sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:dic[@"gift_url"]]];
        self.giftImage.frame = CGRectMake(CGRectGetMaxX(self.receiverNickName.frame) + space, 0, self.superview.bounds.size.height, self.superview.bounds.size.height);
        
        self.magicResultLabel.text = [NSString stringWithFormat:@"x%@", dic[@"num"]];
        self.magicResultLabel.font = [UIFont systemFontOfSize:fontSize];
        self.magicResultLabel.textColor = [UIColor colorWithHexString:textColor ? : @"f8f8f8"];
        [self.magicResultLabel sizeToFit];
        CGRect magicResultLabelFrame = CGRectMake(CGRectGetMaxX(self.giftImage.frame) + space, y, 0, height);
        magicResultLabelFrame.size.width = _magicResultLabel.bounds.size.width;
        self.magicResultLabel.frame = magicResultLabelFrame;
        
//        CGFloat selfWidth = height + space + _senderNickName.bounds.size.width + space + _sendLabel.bounds.size.width + space + height + space + _receiverNickName.bounds.size.width + space + self.superview.bounds.size.height;
        self.frame = CGRectMake(self.superview.bounds.size.width, 0, CGRectGetMaxX(self.magicResultLabel.frame), self.superview.bounds.size.height);
        NSString * receive_user_type = dic[@"receive_user_type"];
        NSString * curSel = dic[@"curSel"];
        NSString * sumSel = dic[@"sumSel"];
        if (receive_user_type.notEmpty) {
            self.receiverNickName.textColor = [UIColor whiteColor];
                 if ([receive_user_type isEqualToString:@"all"]) {
                     self.receiverNickName.text = [curSel isEqualToString:sumSel] ? SPDStringWithKey(@"房间内每个人", nil): [NSString stringWithFormat:SPDLocalizedString(@"房间内%@人"),curSel];
                 }else if ([receive_user_type isEqualToString:@"allfemale"]){
                     self.receiverNickName.text = [curSel isEqualToString:sumSel] ? SPDStringWithKey(@"房间内每个女用户", nil): [NSString stringWithFormat:SPDLocalizedString(@"房间内%@个女用户"),curSel];
                 }else if ([receive_user_type isEqualToString:@"allmale"]){
                     self.receiverNickName.text = [curSel isEqualToString:sumSel] ? SPDStringWithKey(@"房间内每个男用户", nil): [NSString stringWithFormat:SPDLocalizedString(@"房间内%@个男用户"),curSel];
                 }else if ([receive_user_type isEqualToString:@"mic"]){
                     self.receiverNickName.text = [curSel isEqualToString:sumSel] ? SPDStringWithKey(@"麦上每个人", nil): [NSString stringWithFormat:SPDStringWithKey(@"麦上%@人", nil),curSel];
                 }else if ([receive_user_type isEqualToString:@"micfemale"]){
                     self.receiverNickName.text = [curSel isEqualToString:sumSel] ? SPDStringWithKey(@"麦上每个女用户", nil): [NSString stringWithFormat:SPDLocalizedString(@"麦上%@个女用户"),curSel];
                 }else if ([receive_user_type isEqualToString:@"micmale"]){
                     self.receiverNickName.text = [curSel isEqualToString:sumSel] ? SPDStringWithKey(@"麦上每个男用户", nil): [NSString stringWithFormat:SPDLocalizedString(@"麦上%@个男用户"),curSel];
                 }
              }
           
    }
}

@end
