//
//  SPDChatroomTitleView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/1/3.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "SPDChatroomTitleView.h"
#import "MarqueeLabel.h"
#import "FamilyModel.h"
#import "NSString+XXWAddition.h"

@interface SPDChatroomTitleView ()

@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;
@property (weak, nonatomic) IBOutlet MarqueeLabel *clanNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *clanIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *clanLevelLabel;
@property (weak, nonatomic) IBOutlet UIButton *followButton;

@end

@implementation SPDChatroomTitleView

- (void)awakeFromNib {
    [super awakeFromNib];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handle)];
    [self addGestureRecognizer:tap];
    self.clanNameLabel.textAlignment = kIsMirroredLayout ? NSTextAlignmentRight : NSTextAlignmentLeft;

}

- (void)setFamilyDict:(NSDictionary *)familyDict {
    _familyDict = familyDict;
    if ([_familyDict[@"clan"][@"is_following"] boolValue] || ![familyDict[@"position"] isEqualToString:@"tourist"]) {
        [self followStatusChangedTo:YES];
    }else {
        [self followStatusChangedTo:NO];
    }
    [self.titleImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:_familyDict[@"clan"][@"avatar"]]];
    NSString * name = _familyDict[@"clan"][@"name"];
    self.clanNameLabel.text = name;
    CGSize size = [NSString sizeWithString:name andFont:[UIFont boldSystemFontOfSize:15] andMaxSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    if (size.width >= 96) {
        [self.clanNameLabel triggerScrollStart];
    }
    self.clanIdLabel.text = [NSString stringWithFormat:@"ID:%@",_familyDict[@"clan"][@"nick_id"]];
    self.clanLevelLabel.text = [NSString stringWithFormat:@"Lv.%@",_familyDict[@"clan"][@"clanLevel"]];
}

- (IBAction)followBtnClicked:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(chatroomTitleView:didClickedFollowBtn:)]) {
        [self.delegate chatroomTitleView:self didClickedFollowBtn:sender];
    }
}

- (void)handle {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didClickedFamilyInfo)]) {
        [self.delegate didClickedFamilyInfo];
    }
}

- (void)followStatusChangedTo:(BOOL)isFollowing {
    self.followButton.hidden = isFollowing;
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(isFollowing ? 180 : 212);
    }];
    [self layoutIfNeeded];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:kIsMirroredLayout ? (UIRectCornerTopLeft|UIRectCornerBottomLeft): (UIRectCornerTopRight|UIRectCornerBottomRight) cornerRadii:CGSizeMake(20,20)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
}

@end
