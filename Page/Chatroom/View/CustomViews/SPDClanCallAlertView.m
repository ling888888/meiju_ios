//
//  SPDClanCallAlertView.m
//  SimpleDate
//
//  Created by Monkey on 2017/6/22.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDClanCallAlertView.h"

@interface SPDClanCallAlertView ()
@property (weak, nonatomic) IBOutlet UIButton *ToBeVipForFreeButton;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;

@end

@implementation SPDClanCallAlertView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.layer.cornerRadius = 15;
    self.clipsToBounds = YES;
    
    self.titleLabel.text = SPDStringWithKey(@"家族召唤", nil);
    self.subTitleLabel.text = SPDStringWithKey(@"本次召唤将向当前在线的100位用户发送邀请", nil);
    [self.UseMoneyToCallButton setTitle:SPDStringWithKey(@"1000金币发送召唤", nil) forState:UIControlStateNormal];
    
}

-(void)setIs_Vip:(BOOL)is_Vip{
    _is_Vip = is_Vip;
    if (is_Vip) {
        self.UseMoneyToCallButton.hidden = YES;
        [self.ToBeVipForFreeButton setTitle:SPDStringWithKey(@"发送召唤", nil) forState:UIControlStateNormal];
    }else{
        self.UseMoneyToCallButton.hidden = NO;
        [self.ToBeVipForFreeButton setTitle:SPDStringWithKey(@"会员免费召唤", nil) forState:UIControlStateNormal];

    }
}
- (IBAction)cancleButtonClicked:(id)sender {
    if (self.cancleButtonClicked) {
        self.cancleButtonClicked();
    }
}
- (IBAction)useMoneyToClanCall:(id)sender {
    if (self.useMoneyButtonClicked) {
        self.useMoneyButtonClicked();
    }
}
- (IBAction)fristButtonClicked:(id)sender {
    if (self.vipButtonClicked) {
        self.vipButtonClicked();
    }
}

@end
