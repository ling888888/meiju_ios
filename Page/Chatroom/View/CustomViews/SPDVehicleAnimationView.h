//
//  SPDVehicleAnimationView.h
//  SimpleDate
//
//  Created by 李楠 on 17/7/12.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPDVehicleAnimationView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *animationImageView;
@property (weak, nonatomic) IBOutlet UIImageView *genderImageView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;

@property (nonatomic, copy) NSDictionary *dic;

@end
