//
//  SPDChatRoomAdminCell.h
//  SimpleDate
//
//  Created by 李楠 on 17/6/6.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPDChatRoomAdminCell : UICollectionViewCell

@property (nonatomic, copy) NSString *type;

@end
