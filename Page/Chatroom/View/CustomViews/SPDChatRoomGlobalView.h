//
//  SPDChatRoomGlobalView.h
//  SimpleDate
//
//  Created by Monkey on 2017/5/26.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPDCRSendMagicMessage.h"

@protocol SPDChatRoomGlobalViewDelegate <NSObject>

@optional

- (void)clickedClanWithId:(NSString *)clan_id andClan_name:(NSString *)clan_name;
- (void)clickedAvatarImageView:(NSString *)userId;

@end

@interface SPDChatRoomGlobalView : UIView

// nil 2 3 magic(魔法的世界消息)
@property (nonatomic,copy)NSDictionary *dict; // 废弃

@property(nonatomic,strong)RCMessageContent * messageContent; // ["SPDWorldChatMessage" ,@"SPDCRSendMagicMessage"]


@property(nonatomic,weak)id<SPDChatRoomGlobalViewDelegate> deleage;

@property (nonatomic, assign) CGRect originalFrame;

@end
