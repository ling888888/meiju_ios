//
//  SPDTurnTableView.m
//  SimpleDate
//
//  Created by Monkey on 2018/12/14.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDTurnTableView.h"
#import "SPDTurnTabelModel.h"

#define turnScale_W self.bounds.size.width/300
#define turnScale_H self.bounds.size.height/300

@implementation SPDTurnTableView

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initui];
    }
    return self;
}

- (void)initui {
    
//    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleRemove)];
//    [self addGestureRecognizer:tap];
//
    // 转盘
    self.rotateWheel = [[UIImageView alloc]initWithFrame:self.bounds];
    [self addSubview:self.rotateWheel];
    self.rotateWheel.image = [UIImage imageNamed:@"turntable_bg"];
    
    // 抽奖按钮
    self.playButton = [[UIButton alloc]initWithFrame:
                       CGRectMake(0,
                                  0,
                                  CGRectGetWidth(self.bounds)/3,
                                  CGRectGetHeight(self.bounds)/3)];
    self.playButton.center = CGPointMake(CGRectGetWidth(self.bounds)/2, CGRectGetWidth(self.bounds)/2);
    self.playButton.layer.cornerRadius = CGRectGetWidth(self.bounds)/3/2;
    [self addSubview:self.playButton];
    
    // 外围装饰背景图
    UIImageView * backImageView = [UIImageView new];
    backImageView.image = [UIImage imageNamed:@"turntable_border"];
    [self addSubview:backImageView];
    
    [backImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.rotateWheel.mas_centerX).with.offset(-1*turnScale_W);
        make.centerY.mas_equalTo(self.rotateWheel.mas_centerY).with.offset(-13*turnScale_H);
        make.size.mas_equalTo(CGSizeMake(306*turnScale_W, 335*turnScale_H));
    }];
    
//    UIButton * ruleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [self addSubview:ruleBtn];
//    [ruleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.size.mas_equalTo(CGSizeMake(41, 41));
//        make.right.equalTo(self.mas_right).offset(0);
//        make.top.equalTo(self.mas_top).offset(-20);
//    }];
//    [ruleBtn setBackgroundImage:[UIImage imageNamed:@"turntable_rule"] forState:UIControlStateNormal];
//    [ruleBtn addTarget:self action:@selector(handle) forControlEvents:UIControlEventTouchUpInside];
}

- (void)handle {
    // 跳转到网页
//    self.webView.hidden = NO;
   
}

//- (void)handleRemove {
//    if (self.webView.hidden) {
//        [self removeFromSuperview];
//    }else{
//        self.webView.hidden = YES;
//    }
//}

- (void)setGold:(NSNumber *)gold {
    _gold = gold;
    
    self.goldLabel.text = _gold.stringValue;
}

- (UILabel *)goldLabel{
    if (!_goldLabel) {
        _goldLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.playButton.bounds.size.width - 5, 30)];
        _goldLabel.center = CGPointMake(self.rotateWheel.center.x - 2.5, self.rotateWheel.center.y - 10);
        _goldLabel.textAlignment = NSTextAlignmentCenter;
        _goldLabel.font = [UIFont systemFontOfSize:15];
        _goldLabel.textColor = [UIColor colorWithHexString:@"E97900"];
        [self addSubview:_goldLabel];
    }
    return _goldLabel;
}

-(void)setDataArray:(NSMutableArray *)dataArray {
    _dataArray = dataArray;
    
    // 一般是8 个
    for (int i = 0; i < dataArray.count; i ++) {
        SPDTurnTabelModel * model = _dataArray[i];
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0,M_PI * CGRectGetHeight(self.bounds)/8,
                                                                  CGRectGetHeight(self.bounds)/2)];
        label.layer.anchorPoint = CGPointMake(0.5, 1);
        label.center = CGPointMake(CGRectGetHeight(self.bounds)/2, CGRectGetHeight(self.bounds)/2);
        label.text = [NSString stringWithFormat:@"%@",model.desc];
        CGFloat angle = M_PI * 2 / 8* i;
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:13];
        label.textColor = [UIColor whiteColor];
        label.transform = CGAffineTransformMakeRotation(angle);
        [self.rotateWheel addSubview:label];
        UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(35*turnScale_W,26, M_PI * CGRectGetHeight(self.bounds)/8 - 70*turnScale_W, 40)];
        [imageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,model.image]]];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [label addSubview:imageView];
        
    }
}

- (UIImageView *)rotateWheel {
    if (!_rotateWheel) {
        _rotateWheel = [[UIImageView alloc]init];
        [self addSubview:_rotateWheel];
    }
    return _rotateWheel;
}



@end
