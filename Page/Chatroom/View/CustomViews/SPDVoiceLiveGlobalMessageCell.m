//
//  SPDVoiceLiveGlobalMessageCell.m
//  SimpleDate
//
//  Created by 李楠 on 17/5/31.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDVoiceLiveGlobalMessageCell.h"

@interface SPDVoiceLiveGlobalMessageCell ()

@property (nonatomic, strong) UILabel *nickNameLabel;
@property (nonatomic, strong) UILabel *noticeLabel;
//@property (nonatomic, strong) UIImageView *nImageView;
@property (nonatomic, strong) UILabel *nNickNameLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) LY_PortraitView * portraitView;
@property (nonatomic, strong) LY_PortraitView * nPortraitView;

@end

@implementation SPDVoiceLiveGlobalMessageCell

- (instancetype)init {
    if (self = [super init]) {

        self.portraitView = [[LY_PortraitView alloc]init];
        [self addSubview:_portraitView];
        
        self.nickNameLabel = [[UILabel alloc] init];
        [self addSubview:_nickNameLabel];
        
        self.noticeLabel = [[UILabel alloc] init];
        self.noticeLabel.text = @"@";
        [self addSubview:_noticeLabel];
        
        self.nNickNameLabel = [[UILabel alloc] init];
        [self addSubview:_nNickNameLabel];

        self.contentLabel = [[UILabel alloc] init];
        [self addSubview:_contentLabel];
        
        self.nPortraitView = [[LY_PortraitView alloc]init];
        [self addSubview:self.nPortraitView];

    }
    return self;
}

- (void)setDic:(NSDictionary *)dic {
    _dic = dic;
    
    CGFloat y = 5;
    CGFloat height = self.superview.bounds.size.height - y * 2;
//    CGFloat borderWidth = height > 30 ? 1 : 0;
    CGFloat space = height > 30 ? 10 : 5;
    CGFloat fontSize = height > 30 ? 14 : 11;
    NSString *textColor = height > 30 ? @"fcfdff" : nil;

    self.portraitView.frame = CGRectMake(0, y, height, height);
    
    LY_Portrait* avatar = [[LY_Portrait alloc]init];
    avatar.headwearUrl = dic[@"sendHeadwearWebp"];
    avatar.url = dic[@"avatar"];
    self.portraitView.portrait = avatar;
    
    self.nickNameLabel.text = dic[@"nickname"];
    self.nickNameLabel.font = [UIFont systemFontOfSize:fontSize];
    if (textColor) {
        self.nickNameLabel.textColor = [UIColor colorWithHexString:textColor];
    } else if ([dic[@"gender"] isEqualToString:@"male"]) {
        self.nickNameLabel.textColor = [UIColor colorWithHexString:@"65adff"];
    } else if ([dic[@"gender"] isEqualToString:@"female"]) {
        self.nickNameLabel.textColor = [UIColor colorWithHexString:@"fe69a1"];
    } else {
        self.nickNameLabel.textColor = [UIColor colorWithHexString:@"f8f8f8"];
    }
    [self.nickNameLabel sizeToFit];
    
    CGRect nickNameLabelFrame = CGRectMake(CGRectGetMaxX(self.portraitView.frame) + space, y, 0, height);
    nickNameLabelFrame.size.width = _nickNameLabel.bounds.size.width;
    self.nickNameLabel.frame = nickNameLabelFrame;
    
    NSString *mentionedType = dic[@"mentionedType"];
    NSArray *mentionedList = dic[@"mentionedList"];
    if (mentionedType && [mentionedType isEqualToString:@"2"] && mentionedList && mentionedList.count == 1) {
        NSDictionary *user = mentionedList[0];
        
        self.noticeLabel.hidden = NO;
        self.noticeLabel.font = [UIFont systemFontOfSize:fontSize];
        self.noticeLabel.textColor = [UIColor colorWithHexString:textColor ? : @"f8f8f8"];
        [self.noticeLabel sizeToFit];
        CGRect noticeLabelFrame = CGRectMake(CGRectGetMaxX(self.nickNameLabel.frame) + space, y, 0, height);
        noticeLabelFrame.size.width = _noticeLabel.bounds.size.width;
        self.noticeLabel.frame = noticeLabelFrame;
        
        self.nPortraitView.frame = CGRectMake(CGRectGetMaxX(self.noticeLabel.frame) + space, y, height, height);
        
        LY_Portrait * nPortrait = [[LY_Portrait alloc]init];
        nPortrait.headwearUrl = dic[@"receiveHeadwearWebp"];
        nPortrait.url = user[@"avatar"];
        self.nPortraitView.portrait = nPortrait;

        self.nNickNameLabel.hidden = NO;
        self.nNickNameLabel.text = user[@"nick_name"];
        self.nNickNameLabel.font = [UIFont systemFontOfSize:fontSize];
        if (textColor) {
            self.nNickNameLabel.textColor = [UIColor colorWithHexString:textColor];
        } else if ([user[@"gender"] isEqualToString:@"male"]) {
            self.nNickNameLabel.textColor = [UIColor colorWithHexString:@"65adff"];
        } else if ([user[@"gender"] isEqualToString:@"female"]) {
            self.nNickNameLabel.textColor = [UIColor colorWithHexString:@"fe69a1"];
        } else {
            self.nNickNameLabel.textColor = [UIColor colorWithHexString:@"f8f8f8"];
        }
        [self.nNickNameLabel sizeToFit];
        CGRect nickNameLabelFrame = CGRectMake(CGRectGetMaxX(self.nPortraitView.frame) + space, y, 0, height);
        nickNameLabelFrame.size.width = _nNickNameLabel.bounds.size.width;
        self.nNickNameLabel.frame = nickNameLabelFrame;
        
        NSString *content = [dic[@"content"] stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"@%@ ", user[@"nick_name"]] withString:@""];
        self.contentLabel.text = [NSString stringWithFormat:@"%@", content];
        self.contentLabel.font = [UIFont systemFontOfSize:fontSize];
        self.contentLabel.textColor = [UIColor colorWithHexString:textColor ? : @"f8f8f8"];
        [self.contentLabel sizeToFit];
        CGRect contentLabelFrame = CGRectMake(CGRectGetMaxX(self.nNickNameLabel.frame) + space, y, 0, height);
        contentLabelFrame.size.width = _contentLabel.bounds.size.width;
        self.contentLabel.frame = contentLabelFrame;
        
        CGFloat selfWidth = height + space + _nickNameLabel.bounds.size.width + space + _noticeLabel.bounds.size.width + space + height + space + _nNickNameLabel.bounds.size.width + space + _contentLabel.bounds.size.width;
        self.frame = CGRectMake(self.superview.bounds.size.width, 0, selfWidth, self.superview.bounds.size.height);
    } else {
        self.noticeLabel.hidden = YES;
        self.nNickNameLabel.hidden = YES;
        
        self.contentLabel.text = [NSString stringWithFormat:@"%@", dic[@"content"]];
        self.contentLabel.font = [UIFont systemFontOfSize:fontSize];
        self.contentLabel.textColor = [UIColor colorWithHexString:textColor ? : @"f8f8f8"];
        [self.contentLabel sizeToFit];
        CGRect contentLabelFrame = CGRectMake(CGRectGetMaxX(self.nickNameLabel.frame) + space, y, 0, height);
        contentLabelFrame.size.width = _contentLabel.bounds.size.width;
        self.contentLabel.frame = contentLabelFrame;
        
        CGFloat selfWidth = height + space + _nickNameLabel.bounds.size.width + space + _contentLabel.bounds.size.width;
        self.frame = CGRectMake(self.superview.bounds.size.width, 0, selfWidth, self.superview.bounds.size.height);
    }
}

@end
