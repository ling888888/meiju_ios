//
//  ChatroomLockViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2018/11/19.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "ChatroomLockViewController.h"
#import "ChatroomLockTimeCell.h"
#import "ChatroomLockTitleCell.h"
#import "ChatroomLockListCell.h"
#import "ChatroomLockStateCell.h"
#import "ChatroomLockModel.h"
#import "ZegoKitManager.h"

@interface ChatroomLockViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, ChatroomLockListCellDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, copy) NSString *expireTime;
@property (nonatomic, copy) NSString *balance;

@end

@implementation ChatroomLockViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = SPDStringWithKey(@"房间锁", nil);
    [self.view addSubview:self.collectionView];
    [self requestMyBalance];
    [self requestChatroomLockList];
}

#pragma mark - Private methods

- (void)requestMyBalance {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.balance" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.balance = [NSString stringWithFormat:SPDStringWithKey(@"金币余额: %@", nil), suceess[@"balance"]];
        [self.collectionView reloadData];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestChatroomLockList {
    NSDictionary *dic = @{@"clanId": self.clanId};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"chatroom.lock.list" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        [self transformExpireTime:suceess[@"expireTime"]];
        for (NSDictionary *dic in suceess[@"list"]) {
            ChatroomLockModel *model = [ChatroomLockModel initWithDictionary:dic];
            [self.dataArray addObject:model];
        }
        [self.collectionView reloadData];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestChatroomLockBuyWithModel:(ChatroomLockModel *)model {
    NSDictionary *dic = @{@"clanId": self.clanId, @"lockId": model.lockId};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"chatroom.lock.buy" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        ZegoManager.canLock = YES;
        [self requestMyBalance];
        [self transformExpireTime:suceess[@"expireTime"]];
        [self showToast:SPDStringWithKey(@"房间锁购买成功", nil)];
        [self.collectionView reloadData];
    } bFailure:^(id  _Nullable failure) {
        if ([failure isKindOfClass:[NSDictionary class]]) {
            if ([failure[@"code"] integerValue] == 202) {
                [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                    [self pushToRechageController];
                }];
            } else if ([failure[@"code"] integerValue] == 2001) {
                [self showToast:SPDStringWithKey(@"创建家族后可以购买房间锁", nil)];
            } else {
                [self showToast:failure[@"msg"]];
            }
        }
    }];
}

- (void)transformExpireTime:(NSNumber *)expireTime {
    if (expireTime) {
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setTimeZone:[NSTimeZone localTimeZone]];
        [format setDateFormat:@"yyyy/MM/dd"];
        NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:expireTime.longLongValue / 1000];
        self.expireTime = [NSString stringWithFormat:SPDStringWithKey(@"%@到期", nil), [format stringFromDate:date]];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 4;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == 2) {
        return self.dataArray.count;
    } else {
        return 1;
    }
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0: {
            ChatroomLockTimeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatroomLockTimeCell" forIndexPath:indexPath];
            cell.expireTimeLabel.text = self.expireTime ? : SPDStringWithKey(@"还未购买房间锁", nil);
            return cell;
            break;
        }
        case 1: {
            ChatroomLockTitleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatroomLockTitleCell" forIndexPath:indexPath];
            cell.titleLabel.text = SPDStringWithKey(@"购买房间锁", nil);
            cell.balanceLabel.text = self.balance;
            return cell;
            break;
        }
        case 2: {
            ChatroomLockListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatroomLockListCell" forIndexPath:indexPath];
            cell.model = self.dataArray[indexPath.row];
            cell.delegate = self;
            return cell;
            break;
        }
        default: {
            ChatroomLockStateCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatroomLockStateCell" forIndexPath:indexPath];
            return cell;
            break;
        }
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            return CGSizeMake(kScreenW, 84);
            break;
        case 1:
            return CGSizeMake(kScreenW, 52);
            break;
        case 2:
            return CGSizeMake(kScreenW, 58);
            break;
        default:
            return CGSizeMake(kScreenW, 225);
            break;
    }
}

#pragma mark - ChatroomLockListCellDelegate

- (void)didClickBuyButtonWithModel:(ChatroomLockModel *)model {
    NSString *title = [NSString stringWithFormat:SPDStringWithKey(@"确定花费%@金币购买%@天房间锁", nil), model.price, model.day];
    [self presentCommonController:title messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
        [self requestChatroomLockBuyWithModel:model];
    }];
}

#pragma mark - Setters & Getters

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.minimumLineSpacing = 0;
        flowLayout.minimumInteritemSpacing = 0;
        _collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor colorWithHexString:@"FBFBFB"];
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.showsVerticalScrollIndicator = NO;
        [_collectionView registerNib:[UINib nibWithNibName:@"ChatroomLockTimeCell" bundle:nil] forCellWithReuseIdentifier:@"ChatroomLockTimeCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"ChatroomLockTitleCell" bundle:nil] forCellWithReuseIdentifier:@"ChatroomLockTitleCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"ChatroomLockListCell" bundle:nil] forCellWithReuseIdentifier:@"ChatroomLockListCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"ChatroomLockStateCell" bundle:nil] forCellWithReuseIdentifier:@"ChatroomLockStateCell"];
    }
    return _collectionView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

@end
