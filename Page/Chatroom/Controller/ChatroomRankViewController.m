//
//  ChatroomRankViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2018/11/23.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "ChatroomRankViewController.h"
#import "ChatroomRankFirstCell.h"
#import "ChatroomRankListCell.h"
#import "ChatroomRankModel.h"

@interface ChatroomRankViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *segmentedControlTop;
@property (weak, nonatomic) IBOutlet UILabel *emptyLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *myRankView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *nobleAvatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *genderAgeBtn;
@property (weak, nonatomic) IBOutlet UIButton *levelBtn;
@property (weak, nonatomic) IBOutlet UILabel *goldLabel;

@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) NSMutableArray *hoursRankArr;
@property (nonatomic, strong) NSMutableArray *daysRankArr;
@property (nonatomic, strong) ChatroomRankModel *myRankModel;
@property (nonatomic, strong) ChatroomRankModel *myHoursRankModel;
@property (nonatomic, strong) ChatroomRankModel *myDaysRankModel;

@end

@implementation ChatroomRankViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureSubviews];
    [self requestChatroomRankGold];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
}

#pragma mark - Private methods

- (void)configureSubviews {
    self.navigationItem.title = SPDStringWithKey(@"家族贡献榜", nil);
    [self.segmentedControl setTitle:SPDStringWithKey(@"前24小时", nil) forSegmentAtIndex:0];
    [self.segmentedControl setTitle:SPDStringWithKey(@"前7天", nil) forSegmentAtIndex:1];
    self.segmentedControlTop.constant = NavHeight + 8;
    [self.collectionView registerNib:[UINib nibWithNibName:@"ChatroomRankFirstCell" bundle:nil] forCellWithReuseIdentifier:@"ChatroomRankFirstCell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"ChatroomRankListCell" bundle:nil] forCellWithReuseIdentifier:@"ChatroomRankListCell"];
    if ([SPDCommonTool currentVersionIsArbic]) {
        [self.genderAgeBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        [self.genderAgeBtn setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 2.5)];
    }
}

- (void)requestChatroomRankGold {
    NSDictionary *dic = @{@"clanId": self.clanId, @"type": self.segmentedControl.selectedSegmentIndex == 0 ? @"24Hours" : @"7Days"};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"chatroom.rank.gold" bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        NSInteger rank = 1;
        for (NSDictionary *dic in suceess[@"list"]) {
            ChatroomRankModel *model = [ChatroomRankModel initWithDictionary:dic];
            model.rank = @(rank);
            if (rank == 1) {
                model.totalGold = suceess[@"totalGold"];
                model.updateTime = suceess[@"updateTime"];
            }
            [self.dataArr addObject:model];
            rank++;
        }
        self.myRankModel = [ChatroomRankModel initWithDictionary:suceess[@"mine"]];
        [self reloadData];
    } bFailure:^(id  _Nullable failure) {

    }];
}

- (void)reloadData {
    if (self.dataArr.count <= 0) {
        self.collectionView.hidden = YES;
        self.myRankView.hidden = YES;
        
        self.emptyLabel.text = SPDStringWithKey(self.segmentedControl.selectedSegmentIndex == 0 ? @"24小时内家族还未产生贡献" : @"7天内家族还未产生贡献", nil);
    } else {
        self.collectionView.hidden = NO;
        self.myRankView.hidden = NO;

        self.collectionView.backgroundColor = [UIColor whiteColor];
        [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
        [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, self.myRankModel.avatar]]];
        if (![SPDCommonTool isEmpty:self.myRankModel.noble]) {
            self.nobleAvatarImageView.hidden = NO;
            self.nobleAvatarImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"avatar_%@_50", self.myRankModel.noble]];
        } else {
            self.nobleAvatarImageView.hidden = YES;
        }
        self.nickNameLabel.text = self.myRankModel.nickName;
        if ([self.myRankModel.gender isEqualToString:@"female"]) {
            [self.genderAgeBtn setBackgroundColor:[UIColor colorWithHexString:@"ff7eca"]];
            [self.genderAgeBtn setImage:[UIImage imageNamed:@"chatroomuser_female"] forState:UIControlStateNormal];
        } else if ([self.myRankModel.gender isEqualToString:@"male"]) {
            [self.genderAgeBtn setBackgroundColor:[UIColor colorWithHexString:@"5db4fd"]];
            [self.genderAgeBtn setImage:[UIImage imageNamed:@"chatroomuser_male"] forState:UIControlStateNormal];
        }
        [self.genderAgeBtn setTitle:self.myRankModel.age.stringValue forState:UIControlStateNormal];
        [SPDCommonTool configDataForLevelBtnWithLevel:[self.myRankModel.level intValue] andUIButton:self.levelBtn];
        self.goldLabel.text = self.myRankModel.goldStr;
    }
}

#pragma mark - Event response

- (IBAction)segmentedControlValueChanged:(UISegmentedControl *)sender {
    if (self.myRankModel) {
        [self reloadData];
    } else {
        [self requestChatroomRankGold];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return  self.segmentedControl.selectedSegmentIndex == 0 ? self.hoursRankArr.count : self.daysRankArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        ChatroomRankFirstCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatroomRankFirstCell" forIndexPath:indexPath];
        cell.model = self.dataArr[indexPath.row];
        return cell;
    } else {
        ChatroomRankListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatroomRankListCell" forIndexPath:indexPath];
        cell.model = self.dataArr[indexPath.row];
        return cell;
    }
    
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    ChatroomRankModel *model = self.dataArr[indexPath.row];
    [self pushToDetailTableViewController:self userId:model.userId];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return CGSizeMake(kScreenW, 224);
    } else {
        return CGSizeMake(kScreenW, 70);
    }
}

#pragma mark - Setter & getters

- (NSMutableArray *)dataArr {
    return self.segmentedControl.selectedSegmentIndex == 0 ? self.hoursRankArr : self.daysRankArr;
}

- (NSMutableArray *)hoursRankArr {
    if (!_hoursRankArr) {
        _hoursRankArr = [NSMutableArray array];
    }
    return _hoursRankArr;
}

- (NSMutableArray *)daysRankArr {
    if (!_daysRankArr) {
        _daysRankArr = [NSMutableArray array];
    }
    return _daysRankArr;
}

- (void)setMyRankModel:(ChatroomRankModel *)myRankModel {
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        self.myHoursRankModel = myRankModel;
    } else {
        self.myDaysRankModel = myRankModel;
    }
}

- (ChatroomRankModel *)myRankModel {
    return self.segmentedControl.selectedSegmentIndex == 0 ? self.myHoursRankModel : self.myDaysRankModel;
}

@end
