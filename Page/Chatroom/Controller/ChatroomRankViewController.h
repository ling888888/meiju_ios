//
//  ChatroomRankViewController.h
//  SimpleDate
//
//  Created by 李楠 on 2018/11/23.
//  Copyright © 2018 WYB. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChatroomRankViewController : BaseViewController

@property (nonatomic, copy) NSString *clanId;

@end

NS_ASSUME_NONNULL_END
