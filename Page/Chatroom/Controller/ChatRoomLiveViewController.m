//
//  ChatRoomLiveViewController.m
//  SimpleDate
//
//  Created by Monkey on 2017/5/12.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "ChatRoomLiveViewController.h"
#import "ZegoKitManager.h"
#import "RCDLiveMessageCell.h"
#import "RCDLiveGiftMessageCell.h"
#import "RCDLiveGiftMessage.h"
#import "RCDLiveTipMessageCell.h"
#import "RCDLiveMessageModel.h"
#import "RCDLiveCollectionViewHeader.h"
#import "RCDLiveKitUtility.h"
#import "RCDLiveKitCommonDefine.h"
#import <RongIMLib/RongIMLib.h>
#import <objc/runtime.h>
#import "RCDLiveTipMessageCell.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
//#import "RCDLivePortraitViewCell.h"
#import "SPDApiUser.h"
#import "SPDVoiceLiveOnlineUserCell.h"
#import "SPDFamilyMemberModel.h"
#import "RCDMSChatRoomCell.h"
#import "SPDVoiceLiveUserModel.h"
#import "SPDVoiceLiveAudienceView.h"
#import "SPDJoinChatRoomMessage.h"
#import "SPDComeInChatRoomView.h"
#import "SPDBannedSpeakMessage.h"
#import "SPDBannedSpeakCell.h"
#import "SPDChatRoomGlobalView.h"
#import "UIImage+GIF.h"
#import "SPDVoiceLiveGlobalScrollView.h"
#import "SPDWorldChatMessage.h"
#import "NSString+XXWAddition.h"
#import "SPDChatRoomUserInfoView.h"
#import "SPDChatRoomSendPresentMessage.h"
#import "SPDChatRoomPrivatePresentView.h"
#import "MWPhotoBrowser.h"
#import "PresentView.h"
#import "SPDTextMessage.h"
#import "SPDClanCallMessage.h"
#import "FDAlertView.h"
#import "SPDClanCallAlertView.h"
#import "DBUtil.h"
#import "SPDChatRoomEffectImageView.h"
#import "SPDKeywordMatcher.h"
#import "ZTWAInfoShareView.h"
#import "SPDUMShareUtils.h"
#import "CRSendGiftMessage.h"
#import "CRSendVehicleMessage.h"
#import "CRWorldMsgMessage.h"
#import "SPDCRSendMagicMessage.h"
#import "SPDChatRoomMagicEffectView.h"
#import "SPDVehicleEffectImageView.h"
#import "SPDUserOnlineTitleView.h"
#import "SPDFamilyInfoController.h"
#import "ChatroomBgImageController.h"
#import "AppManager.h"
#import "SPDChatroomRedPacketView.h"
#import "ChatroomRedPacketModel.h"
#import "SPDChatroomRedPacketMessage.h"
#import "ChatroomRedPacketMsgCell.h"
#import "SPDNobleGreetingMessage.h"
#import "NobleGreetingMsgCell.h"
#import "SPDChatroomNobleEnterView.h"
#import "JXPopoverView.h"
#import "ChatroomLockViewController.h"
#import "ChatroomRankViewController.h"
#import "SDCycleScrollView.h"

//输入框的高度
#define MinHeight_InputView 50.0f
#define kBounds [UIScreen mainScreen].bounds.size

#define collectionOriginalY   (NavHeight + 175.0f - 64)

@interface ChatRoomLiveViewController () <
UICollectionViewDelegate, UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout, RCDLiveMessageCellDelegate, UIGestureRecognizerDelegate,
UIScrollViewDelegate, UINavigationControllerDelegate,RCTKInputBarControlDelegate,RCConnectionStatusChangeDelegate,UIAlertViewDelegate,RCIMClientReceiveMessageDelegate, SPDVoiceLiveAudienceViewDelegate,RCDMSChatRoomCellDelegate,SPDChatRoomGlobalViewDelegate,SPDChatRoomUserInfoViewDelegate,MWPhotoBrowserDelegate, ZegoAudioRoomDelegate, ZegoAudioLivePublisherDelegate, ZTWAInfoShareViewDelegate, ZegoKitManagerDelegate, ChatroomBgImageControllerDelegate, ChatroomRedPacketMsgCellDelegate, SPDFamilyInfoControllerDelegate, SDCycleScrollViewDelegate>

@property (nonatomic,strong) ZegoAudioRoomApi *api;
@property (nonatomic,copy) AFNetworkReachabilityManager *networkReachabilityManager;

@property (nonatomic,strong) UICollectionView *micCollectionView;
@property (nonatomic,strong) UICollectionView *onlineUserConllectionView;
@property (nonatomic,strong) UIButton *micBtn;
@property (nonatomic,strong) UIButton *muteBtn;
@property (nonatomic,strong) UIButton *publicMicBtn;
@property (nonatomic,copy) NSString *position; // 职位描述["tourist","chief","deputychief","member"],游客，族长，副族长，成员
@property (nonatomic,copy)NSString *myTagPosition;//用来做发消息的标签的 没有 superadmin
@property (nonatomic,assign) NSInteger positionLevel; // 权限值：0 游客，25 家族成员，75 副族长，150 族长
@property (nonatomic,strong) NSMutableArray *familyMembers;
@property (nonatomic,strong) NSMutableDictionary *onlineUsers;
@property (nonatomic,strong) NSTimer *timer;
@property (nonatomic,strong) NSMutableArray *speekers;
@property (nonatomic,strong) NSMutableDictionary *audiences;
@property (nonatomic,strong) NSArray *sortedAudiences;
@property (nonatomic,strong) NSMutableDictionary *tourists;
@property (nonatomic,strong) UILabel *rankTotalGoldLabel;
@property (nonatomic,assign) NSInteger onlineNumber;
@property (nonatomic,strong) UILabel *onlineNumberLabel;
@property (nonatomic,assign) BOOL isDisconnected;
@property (nonatomic,assign) BOOL isRequestMicStatus;
@property (nonatomic,assign) BOOL isRequestPosition;
@property (nonatomic,strong) SPDVoiceLiveGlobalScrollView *globalScrollView; // 跑马灯
@property (nonatomic,strong) SPDVoiceLiveGlobalScrollView *vehicleAnimationView; // 座驾动效
@property (nonatomic,copy) NSDictionary *wordDict;
@property (nonatomic,strong) NSMutableArray *albumArray;
@property (nonatomic,copy) NSDictionary *notice_userInfo;
@property (nonatomic,strong) UIImageView *backImageView;
@property (nonatomic,copy) NSString *specialNum;
@property (nonatomic,assign) BOOL is_agent_gm;
@property (nonatomic,strong) UIImageView *backgroundImageView;
@property (nonatomic,assign) BOOL isEnableZegoKick;
@property (nonatomic,assign) BOOL isOpenLawLessCheck;
@property (nonatomic,copy) NSString *noble;
@property (nonatomic,assign) BOOL isHaveClan;
@property (nonatomic,strong) SPDChatroomNobleEnterView *nobleEnterView;
@property (nonatomic,strong) NSNumber *rankTotalGold;
@property (nonatomic,strong) SDCycleScrollView *activityScrollView;
/**
 *  是否需要滚动到底部
 */
@property(nonatomic, assign) BOOL isNeedScrollToButtom;
/**
 *  底部显示未读消息view
 */
@property (nonatomic, strong) UIView *unreadButtonView;
@property(nonatomic, strong) UILabel *unReadNewMessageLabel;

@property (nonatomic, assign) NSInteger unreadNewMsgCount;

/**
 *  是否正在加载消息
 */
@property(nonatomic) BOOL isLoading;

@property (nonatomic ,strong) SPDComeInChatRoomView *comeInChatRoomView;

@property (nonatomic,assign)BOOL enableSendGlobal;

@property (nonatomic,strong)SPDChatRoomGlobalView *globalNotifyView;

@property (nonatomic,strong)UITapGestureRecognizer *tap;

@property (nonatomic,copy)  NSString *sendPresent_nickname;
@property (nonatomic,copy)  NSString *sendPresent_avatar;
@property (nonatomic,copy)  NSString *sendPresent_gender;
@property (nonatomic,copy)  NSString *sendPresent_userId;

@property (nonatomic,strong)SPDChatRoomPrivatePresentView * privatePresentView;


@property (nonatomic,assign) NSTimeInterval joinRoomInterval;

@property (nonatomic,copy) NSString * charmRankStr;

@property (nonatomic,copy) NSString * richeRankStr;

@property (nonatomic,copy) NSString * userLevelStr; //用户等级

@property (nonatomic,strong) UIButton *clanCallButton;

@property (nonatomic,assign)BOOL is_vip;

@property(nonatomic,strong)FDAlertView *alert;

@property (nonatomic,strong) SPDChatRoomEffectImageView * effectsView; //礼物的动效

@property (nonatomic,strong)SPDChatRoomMagicEffectView * magicAnimationView; //魔法的动效

@property (nonatomic,strong) SPDVehicleEffectImageView * vehicleEffectView;

@property (nonatomic,strong) SPDClanCallAlertView * calnCallAlertView;

@property (nonatomic,strong) SPDKeywordMatcher * keyword_match;

@property (nonatomic,strong) KeywordMap * keymap;

@property (nonatomic, strong) ZTWAInfoShareView *shareView;

@property (nonatomic,strong) SPDUserOnlineTitleView * titleView;

@property (nonatomic,strong) NSMutableDictionary * familyInfoDict;

@property (nonatomic,strong) UIButton * lotteryButton;

@end


/**
 *  文本cell标示
 */
static NSString *const rctextCellIndentifier = @"rctextCellIndentifier";

/**
 *  小灰条提示cell标示
 */
static NSString *const RCDLiveTipMessageCellIndentifier = @"RCDLiveTipMessageCellIndentifier";

/**
 *  礼物cell标示
 */
static NSString *const RCDLiveGiftMessageCellIndentifier = @"RCDLiveGiftMessageCellIndentifier";

static NSString *const SPDBannedSpeakCellIndentifier = @"SPDBannedSpeakCellIndentifier";

static NSString *const SPDVoiceLiveOnlineUserCellForMic = @"SPDVoiceLiveOnlineUserCellForMic";

static NSString *const SPDVoiceLiveOnlineUserCellIndentifier = @"SPDVoiceLiveOnlineUserCellIndentifier";

@implementation ChatRoomLiveViewController


- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self rcInit];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self rcInit];
    }
    return self;
}

- (SPDUserOnlineTitleView *)titleView {
    if (!_titleView) {
        _titleView = [[[NSBundle mainBundle] loadNibNamed:@"SPDUserOnlineTitleView" owner:self options:nil] lastObject];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleRightBarBtnClicked)];
        [_titleView addGestureRecognizer:tap];
    }
    return _titleView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    ZegoManager.popIndex = self.navigationController.viewControllers.count;
    
    self.isRequestMicStatus = NO;
    
    self.charmRankStr = @"80";
    self.richeRankStr = @"80";
    self.userLevelStr = @"0";
    self.conversationDataRepository = [[NSMutableArray alloc] init];
    
    //    [self startMonitor];
    
    [self configNavigationBar];
    
    [self joinInChatRoom];
    
    [self initializedSubViews];
    
    [self requestUserInfoData];
    
    [self requestSysConfig];
    
    [self requestUserDetail];
    
    
    [self loadLastGlobalMessage];
    
//    [SPDCommonTool requestAnimationImagesWithType:@"vehicle"];
//    [SPDCommonTool requestAnimationImagesWithType:@"gift"];
//    [SPDCommonTool requestAnimationImagesWithType:@"magic_success"];
//    [SPDCommonTool requestAnimationImagesWithType:@"magic_fail"];

    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"word" ofType:@"plist"];
    self.wordDict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    
    self.joinRoomInterval = [[NSDate date] timeIntervalSince1970] *1000;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    
    [self.conversationMessageCollectionView reloadData];
    
    [self.globalScrollView resumeAnimation];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyBoardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyBoardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    if (ZegoManager.streams.count) {
        [self initTimer];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

-(void)keyBoardWillShow {
    
    self.tap  = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapCollection:)];
    [self.conversationMessageCollectionView addGestureRecognizer:self.tap];
    
}

-(void)keyBoardWillHide {
    [self.conversationMessageCollectionView removeGestureRecognizer:self.tap];
    
}

-(void)handleTapCollection:(UITapGestureRecognizer *)tap {
    [self.inputBar setInputBarStatus:RCDLiveBottomBarDefaultStatus];
}

-(void)handleChatRoomGlobalMsg:(NSNotification *)notify {
    __block RCMessage *rcMessage = notify.object;
    if ([rcMessage.content isKindOfClass:[SPDWorldChatMessage class]]) {
        SPDWorldChatMessage * msg = (SPDWorldChatMessage *)rcMessage.content;
        RCUserInfo *usrInfo = msg.senderUserInfo;
        dispatch_async(dispatch_get_main_queue(), ^{
            if (msg.senderUserInfo && msg.type == nil) {
                NSDictionary * dict = @{
                                        @"user_id":usrInfo.userId ,
                                        @"nickname":usrInfo.name  ? :@"",
                                        @"avatar":usrInfo.portraitUri  ? :@"",
                                        @"content":msg.content  ? :@"",
                                        @"gender":msg.gender  ? :@"",
                                        @"clan_id":msg.send_ClanId ? :@"",
                                        @"clan_name":msg.send_ClanName ? :@"",
                                        @"mentionedType":msg.mentionedType ? : @"",
                                        @"mentionedList":msg.mentionedList ? : @[]
                                        };
                if (self.globalScrollView.dataArray.count) {
                    [self.globalNotifyView setDict:dict];
                }
                [self.globalScrollView setDic:dict];
                
            }else if([msg.type isEqualToString:@"2"] && msg.senderUserInfo){
                NSDictionary *dict = @{
                                       @"user_id":usrInfo.userId ? : @"",
                                       @"savatar":usrInfo.portraitUri  ? :@"",
                                       @"sender_nickname":usrInfo.name  ? :@"",
                                       @"sgender":msg.gender  ? :@"",
                                       @"ruser_id":msg.receive_Id,
                                       @"ravatar":msg.receive_avatar  ? :@"",
                                       @"receive_nickname":msg.receive_nickname  ? :@"",
                                       @"rgender":msg.receive_gender  ? :@"",
                                       @"clan_id":msg.send_ClanId  ? :@"",
                                       @"clan_name":msg.send_ClanName  ? :@"",
                                       @"gift_url" :msg.present_url  ? :@"",
                                       @"type" :msg.type,
                                       @"num": msg.num ? : @"1"
                                       };
                if (self.globalScrollView.dataArray.count) {
                    [self.globalNotifyView setDict:dict];
                }
                [self.globalScrollView setDic:dict];
                
            }else if ([msg.type isEqualToString:@"3"]){
                [self showAllWorldSendGiftMessage:msg];
            }
        });
    } else if ([rcMessage.content isMemberOfClass:[SPDCRSendMagicMessage class]]){
        SPDCRSendMagicMessage *magicMsg = (SPDCRSendMagicMessage *)rcMessage.content;
        RCUserInfo *usrInfo = magicMsg.senderUserInfo;
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDictionary *  dict = @{
                                     @"user_id":usrInfo.userId ? : @"",
                                     @"savatar":usrInfo.portraitUri ? : @"",
                                     @"sender_nickname":usrInfo.name ? : @"",
                                     @"sgender":magicMsg.send_gender ? : @"",
                                     @"ruser_id":magicMsg.receive_id ? : @"",
                                     @"ravatar":magicMsg.receive_avatar ? : @"",
                                     @"receive_nickname":magicMsg.receive_nickname ? : @"",
                                     @"rgender":magicMsg.receive_gender ? : @"",
                                     @"clan_id":magicMsg.clan_id ? : @"",
                                     @"clan_name":magicMsg.clan_name ? : @"",
                                     @"gift_url":magicMsg.magic_url ? : @"",
                                     @"result":magicMsg.result ? : @"",
                                     @"magic_type":magicMsg.type ? :@"",
                                     @"magic_effect_value": magicMsg.magic_effect_value ?:@"",
                                     @"magic_effect":magicMsg.magic_effect ?: @"inc",
                                     @"type":@"magic"
                                     };
            if ([magicMsg.is_cost isEqualToString:@"true"]) {
                //花费金币
                if (self.globalScrollView.dataArray.count) {
                    [self.globalNotifyView setDict:dict];
                }
                [self.globalScrollView setDic:dict];
            }
            if ([magicMsg.clan_id isEqualToString:self.targetId]) {
                self.magicAnimationView.magicMsg = magicMsg;
            }
        });
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
    
    [self releaseTimer];
}

- (void)loadLastGlobalMessage {
    if ([[SPDRCIMDataSource shareInstance].lastGlobalMessage isKindOfClass:[SPDWorldChatMessage class]]) {
        SPDWorldChatMessage * msg = (SPDWorldChatMessage *)[SPDRCIMDataSource shareInstance].lastGlobalMessage;
        RCUserInfo *usrInfo = msg.senderUserInfo;
        dispatch_async(dispatch_get_main_queue(), ^{
            if (msg.senderUserInfo && msg.type == nil) {
                NSDictionary * dict = @{
                                        @"user_id":usrInfo.userId?:@"",
                                        @"nickname":usrInfo.name  ? :@"",
                                        @"avatar":usrInfo.portraitUri  ? :@"",
                                        @"content":msg.content  ? :@"",
                                        @"gender":msg.gender  ? :@"",
                                        @"clan_id":msg.send_ClanId?:@"",
                                        @"clan_name":msg.send_ClanName ? :@"",
                                        @"mentionedType":msg.mentionedType ? : @"",
                                        @"mentionedList":msg.mentionedList ? : @[]
                                        };
                [self.globalScrollView setDic:dict];
            } else if([msg.type isEqualToString:@"2"] || [msg.type isEqualToString:@"3"]){
                NSDictionary *dict = @{
                                       @"user_id":usrInfo.userId?:@"",
                                       @"savatar":usrInfo.portraitUri  ? :@"",
                                       @"sender_nickname":usrInfo.name  ? :@"",
                                       @"sgender":msg.gender  ? :@"",
                                       @"ruser_id":msg.receive_Id  ? :@"",
                                       @"ravatar":msg.receive_avatar  ? :@"",
                                       @"receive_nickname":msg.receive_nickname  ? :@"",
                                       @"rgender":msg.receive_gender  ? :@"",
                                       @"clan_id":msg.send_ClanId  ? :@"",
                                       @"clan_name":msg.send_ClanName  ? :@"",
                                       @"gift_url" :msg.present_url  ? :@"",
                                       @"type" :msg.type,
                                       @"num": msg.num ? : @"1"
                                       };
                [self.globalScrollView setDic:dict];
            }
        });
    } else if ([[SPDRCIMDataSource shareInstance].lastGlobalMessage isMemberOfClass:[SPDCRSendMagicMessage class]]){
        SPDCRSendMagicMessage *magicMsg = (SPDCRSendMagicMessage *)[SPDRCIMDataSource shareInstance].lastGlobalMessage;
        RCUserInfo *usrInfo = magicMsg.senderUserInfo;
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDictionary *  dict = @{
                                     @"user_id":usrInfo.userId ? : @"",
                                     @"savatar":usrInfo.portraitUri ? : @"",
                                     @"sender_nickname":usrInfo.name ? : @"",
                                     @"sgender":magicMsg.send_gender ? : @"",
                                     @"ruser_id":magicMsg.receive_id ? : @"",
                                     @"ravatar":magicMsg.receive_avatar ? : @"",
                                     @"receive_nickname":magicMsg.receive_nickname ? : @"",
                                     @"rgender":magicMsg.receive_gender ? : @"",
                                     @"clan_id":magicMsg.clan_id ? : @"",
                                     @"clan_name":magicMsg.clan_name ? : @"",
                                     @"gift_url":magicMsg.magic_url ? : @"",
                                     @"result":magicMsg.result ? : @"",
                                     @"type":@"magic"
                                     };
            [self.globalScrollView setDic:dict];
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self loadUnkonwnGlobalMessage];
        });
    }
}

- (void)loadUnkonwnGlobalMessage {
    if ([SPDRCIMDataSource shareInstance].lastGlobalMessage.senderUserInfo) {
        RCUserInfo *usrInfo = [SPDRCIMDataSource shareInstance].lastGlobalMessage.senderUserInfo;
        NSDictionary * dict = @{
                                @"user_id":usrInfo.userId,
                                @"nickname":usrInfo.name,
                                @"avatar":usrInfo.portraitUri,
                                @"content":SPDStringWithKey(@"当前版本无法查看此消息，请升级至最新版本。", nil),
                                @"gender":@"",
                                @"clan_id":@"",
                                @"clan_name":@""
                                };
        [self.globalScrollView setDic:dict];
    }
}

- (void)configNavigationBar {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_chatroom_back_btn"] style:UIBarButtonItemStylePlain target:self action:@selector(clickBackBtn)];
    self.navigationItem.titleView = self.titleView;
    
    UIButton *memberBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 30)];
    [memberBtn setImage:[UIImage imageNamed:@"family_member"] forState:UIControlStateNormal];
    [memberBtn addTarget:self action:@selector(handleRightBarBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:memberBtn];
    self.navigationItem.rightBarButtonItems = @[rightBarButtonItem];
}

-(void)handleRightBarBtnClicked {
    
    [self.inputBar setInputBarStatus:RCDLiveBottomBarDefaultStatus];
    
    if (!self.isRequestPosition) {
      SPDFamilyInfoController * vc =[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([SPDFamilyInfoController class])] ;
        vc.infoDict = self.familyInfoDict;
        vc.shareDict = self.shareDict;
        vc.delegate = self;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(void)handlesubscribeBtnBtnClicked {

}

-(void)rcInit {
    self.conversationDataRepository = [[NSMutableArray alloc] init];
    self.conversationMessageCollectionView = nil;
    self.defaultHistoryMessageCountOfChatRoom = CHATROOM_MESSAGECOUNT;
    [self registerNotification];
}

-(void)rcRemove {
    [self removeNotification];
}

/**
 *  注册监听Notification
 */
- (void)registerNotification {
    //注册接收消息
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleChatRoomReceiveMessageNotification:)
     name:SPDLiveKitChatRoomMessageNotification
     object:nil];
    [[NSNotificationCenter defaultCenter ] addObserver:self selector:@selector(handleSendUserPresentSuccess:) name:CHATROOM_SENDPRESENT object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleChatRoomGlobalMsg:) name:SPDLiveKitChatRoomGlobalNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSendPrivateMagicMsg:) name:SENDMAGICSUCCESS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleChatRoomLongPress:) name:CHATROOM_LONGPRESS object:nil];
}

-(void)removeNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SPDLiveKitChatRoomMessageNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CHATROOM_SENDPRESENT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SPDLiveKitChatRoomGlobalNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CHATROOM_LONGPRESS object:nil];
}

#pragma mark - 加入/退出聊天室

- (void)joinInChatRoom {
    self.targetId = self.clan_id;
    [[RCIM sharedRCIM] setDisableMessageAlertSound:YES];
    
    if (![self.clan_id isEqualToString:ZegoManager.clan_id]) {
        [ZegoManager leaveRoom];
    }
    
    ZegoManager.sceneType = SceneTypeClanRoom;
    ZegoManager.isBackground = NO;
    ZegoManager.delegate = self;
    NSString *clan_id = ZegoManager.clan_id;
    ZegoManager.clan_id = self.clan_id;
    self.api = ZegoManager.api;
    [self.api setUserStateUpdate:YES];
    [self.api setAudioRoomDelegate:self];
    [self.api setAudioPublisherDelegate:self];
    [self.api enableSpeaker:ZegoManager.speakerEnabled];
    [self.api enableMic:ZegoManager.micSelected];
    [ZegoAudioRoomApi setUserID:[SPDApiUser currentUser].userId userName:[SPDApiUser currentUser].nickName];
    [self.api setManualPlay:NO];
    [self.api loginRoom:self.clan_id completionBlock:^(int errorCode) {
        if (errorCode == 0) {
            // 加入聊天室成功
            self.isDisconnected = NO;
            [self requestUsersInfo:@[] isUpdate:NO];

            if (![self.clan_id isEqualToString:clan_id]) {
                [self sendJoinChatRoomMessage];
            }
        } else {
            [self showToast:SPDStringWithKey(@"加入聊天室失败，请稍后重试", nil)];
            [self exit];
            
            //[MobClick event:@"joinZegoFailed"];
        }
    }];
    [DBUtil recordClanHistory:_clan_id];
}

- (void)clickBackBtn {
    [self.inputBar setInputBarStatus:RCDLiveBottomBarDefaultStatus];
    self.backImageView.hidden = NO;
}

- (void)tapBackImageView {
    self.backImageView.hidden = YES;
}

- (void)back {
    [self rcRemove];
    if (ZegoManager.popIndex > 0) {
        [self.backImageView removeFromSuperview];
        [self.navigationController popToViewController:self.navigationController.viewControllers[ZegoManager.popIndex - 2] animated:NO];
        ZegoManager.popIndex = 0;
    }
}

- (void)keep {
    [self back];
    [self.api setAudioRoomDelegate:ZegoManager];
    [self.api setAudioPublisherDelegate:ZegoManager];
    ZegoManager.isBackground = YES;
}

- (void)exit {
    [self back];
    [ZegoManager leaveRoom];
}

#pragma mark - ZegoKitManagerDelegate

- (void)micStatusDidChanged:(id)data {
    [self dealMicStatusData:data checkOffline:NO];
}

- (void)kickOut {
    [self back];
}

#pragma mark - ZegoAudioRoomDelegate

- (void)onKickOut:(int)reason roomID:(NSString *)roomID {
    
}

- (void)onDisconnect:(int)errorCode roomID:(NSString *)roomID {
    self.isDisconnected = YES;
    [self showToast:SPDStringWithKey(@"网络连接已断开,请重新进入", nil)];
    [self exit];
}

- (void)onUserUpdate:(NSArray<ZegoUserState *> *)userList updateType:(ZegoUserUpdateType)type {
    if (type == ZEGO_UPDATE_TOTAL) {
        
        [self requestUsersInfo:userList isUpdate:NO];
        
    } else if (type == ZEGO_UPDATE_INCREASE) {
        
        NSMutableArray *arrNewUsers = [NSMutableArray array];
        NSMutableArray *arrLeftUsers = [NSMutableArray array];
        for (ZegoUserState *user in userList) {
            if (user.updateFlag == ZEGO_USER_ADD) {
                [arrNewUsers addObject:user];
            }  else if (user.updateFlag == ZEGO_USER_DELETE) {
                [arrLeftUsers addObject:user];
            }
        }
        
        if (arrNewUsers.count) {
            [self requestUsersInfo:arrNewUsers isUpdate:YES];
        }
        if (arrLeftUsers.count) {
            for (ZegoUserState *user in arrLeftUsers) {
                NSString *userId = user.userID;
                NSString *index = [ZegoManager getUserMicIndex:userId];
                if (index) {
                    [self requestMicStatus];
                }
                [self.audiences removeObjectForKey:userId];
                [self.onlineUsers removeObjectForKey:userId];
            }
            
            self.onlineNumber = self.onlineUsers.count;
            dispatch_async(dispatch_get_main_queue(), ^{
                if (!_isRequestMicStatus) {
                    self.sortedAudiences = [self sortAudiences];
                    [self.onlineUserConllectionView reloadData];
                }
            });
        }
    }
}

- (void)onStreamUpdated:(ZegoAudioStreamType)type stream:(ZegoAudioStream*)stream {
    if (type == ZEGO_AUDIO_STREAM_ADD) {
        [self addStream:stream];
    } else {
        [self deleteStreamWithUserID:stream.userID];
    }
}

#pragma mark - ZegoAudioLivePublisherDelegate

- (void)onPublishStateUpdate:(int)stateCode streamID:(NSString *)streamID streamInfo:(NSDictionary *)info {
    if (stateCode == 0) {
        ZegoAudioStream *stream = [[ZegoAudioStream alloc] init];
        stream.streamID = streamID;
        stream.userID = [SPDApiUser currentUser].userId;
        [self addStream:stream];
    }
}

#pragma mark - 语音动效

- (void)addStream:(ZegoAudioStream *)stream {
//    NSLog(@"[stream] add %@, %@", stream.streamID, stream.userID);
    [ZegoManager.streams setObject:stream forKey:stream.userID];
    [self initTimer];
}

- (void)deleteStreamWithUserID:(NSString *)userID {
//    NSLog(@"[stream] delete %@", userID);
    [ZegoManager.streams removeObjectForKey:userID];
    [self.speekers removeObject:userID];
    if (ZegoManager.streams.count == 0) {
        NSLog(@"[stream] stop %@", userID);
        [self releaseTimer];
    }
}

- (void)initTimer {
    if (!self.timer) {
//        NSLog(@"[stream] start");
        self.timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(getSoundLevel) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
        [self.timer fire];
    }
}

- (void)releaseTimer {
    [self.timer invalidate];
    self.timer = nil;
}

- (void)getSoundLevel {
    for (ZegoAudioStream *stream in [ZegoManager.streams allValues]) {
        if ([stream.userID isEqualToString:[SPDApiUser currentUser].userId]) {
//            NSLog(@"[stream] self %lf", [self.api getCaptureSoundLevel]);
            if ([self.api getCaptureSoundLevel] > 0.5) {
                [self updateMicStatus:Speeking userId:stream.userID];
            } else {
                [self updateMicStatus:Mute userId:stream.userID];
            };
        } else {
//            NSLog(@"[stream] other %lf", [self.api getSoundLevelOfStream:stream.streamID]);
            if ([self.api getSoundLevelOfStream:stream.streamID] > 0.5) {
                [self updateMicStatus:Speeking userId:stream.userID];
            } else {
                [self updateMicStatus:Mute userId:stream.userID];
            };
        }
    }
}

- (void)updateMicStatus:(MicStatus)status userId:(NSString *)userId {
    if (status == Speeking) {
        [self.speekers addObject:userId];
    } else {
        [self.speekers removeObject:userId];
    }
    NSString *index = [ZegoManager getUserMicIndex:userId];
    if (index) {
        SPDVoiceLiveOnlineUserCell *cell = [self getCellWithIndex:index.integerValue];
        if (status == Speeking && cell.micStatus != Mute && cell.micStatus != Forbidden) {
            cell.animating = YES;
        } else {
            cell.animating = NO;
        }
    }
}

- (void)startMonitor {
    [self.networkReachabilityManager startMonitoring];
    __weak __typeof(self)weakSelf = self;
    [self.networkReachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        if (status == AFNetworkReachabilityStatusReachableViaWWAN || status == AFNetworkReachabilityStatusReachableViaWiFi) {
            if (strongSelf.isDisconnected) {
                
            }
        }
    }];
}

#pragma mark - chatroom

/**
 *  注册cell
 *
 *  @param cellClass  cell类型
 *  @param identifier cell标示
 */
- (void)registerClass:(Class)cellClass forCellWithReuseIdentifier:(NSString *)identifier {
    [self.conversationMessageCollectionView registerClass:cellClass
                               forCellWithReuseIdentifier:identifier];
}

/**
 *  初始化页面控件
 */
- (void)initializedSubViews {
    
    //聊天背景
    self.backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    self.backgroundImageView.image = [UIImage imageNamed:@"img_chatroom_bg"];
    self.backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:self.backgroundImageView];
    
    //聊天区
    if(self.contentView == nil){
        CGRect contentViewFrame = CGRectMake(0, collectionOriginalY, kScreenW, kScreenH-collectionOriginalY);
        self.contentView.backgroundColor = RCDLive_RGBCOLOR(235, 235, 235);
        self.contentView = [[UIView alloc]initWithFrame:contentViewFrame];
        [self.view addSubview:self.contentView];
    }
    
    // 排行榜
    UIButton *rankBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *rankTotalImage = [UIImage imageNamed:@"chatroom_rank_total"].adaptiveRtl;
    [rankBtn setBackgroundImage:[rankTotalImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10) resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
    [rankBtn addTarget:self action:@selector(pushToChatroomRankViewController) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:rankBtn];
    [rankBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(0);
        make.top.mas_equalTo(NavHeight + 4.5);
        make.height.mas_equalTo(21);
    }];

    UIImageView *rankIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"chatroom_rank_icon"]];
    [rankBtn addSubview:rankIconImageView];
    [rankIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(8);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(16);
        make.height.mas_equalTo(15);
    }];

    self.rankTotalGoldLabel = [[UILabel alloc] init];
    self.rankTotalGoldLabel.text = @"0";
    self.rankTotalGoldLabel.font = [UIFont systemFontOfSize:12];
    self.rankTotalGoldLabel.textColor = [UIColor colorWithHexString:@"FFD800"];
    [rankBtn addSubview:self.rankTotalGoldLabel];
    [self.rankTotalGoldLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(rankIconImageView.mas_trailing).with.offset(7);
        make.centerY.mas_equalTo(0);
    }];

    UIImageView *rankArrowImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"chatroom_rank_arrow"].adaptiveRtl];
    [rankBtn addSubview:rankArrowImageView];
    [rankArrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.rankTotalGoldLabel.mas_trailing).with.offset(5);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(6);
        make.height.mas_equalTo(9);
        make.trailing.mas_equalTo(-15);
    }];
    
    // 在线人数区
    self.onlineNumberLabel = [[UILabel alloc] init];
    self.onlineNumberLabel.clipsToBounds = YES;
    self.onlineNumberLabel.layer.cornerRadius = 21 / 2.0;
    self.onlineNumberLabel.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.35];
    self.onlineNumberLabel.font = [UIFont systemFontOfSize:13];
    self.onlineNumberLabel.textColor = [UIColor colorWithHexString:COMMON_PINK];
//    self.onlineNumberLabel.textAlignment = NSTextAlignmentCenter;
    self.onlineNumberLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"  在线：%ld人  ", nil), 0];
    [self.view addSubview:_onlineNumberLabel];
    [self.onlineNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(rankBtn.mas_trailing).with.offset(5);
        make.top.mas_equalTo(NavHeight + 4.5);
        make.height.mas_equalTo(21);
    }];
    
    self.muteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.muteBtn.selected = ZegoManager.speakerEnabled;
    [self.muteBtn setImage:[UIImage imageNamed:@"ic_loud_speak_nor"] forState:UIControlStateNormal];
    [self.muteBtn setImage:[UIImage imageNamed:@"ic_loud_speak"] forState:UIControlStateSelected];
    [self.muteBtn addTarget:self action:@selector(onTouchMuteButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.muteBtn];
    [self.muteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.onlineNumberLabel.mas_trailing).with.offset(10);
        make.top.equalTo(self.view).with.offset(NavHeight + 4.5);
        make.width.mas_equalTo(34);
        make.height.mas_equalTo(21);
    }];
    
    self.micBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.micBtn.enabled = ZegoManager.micEnabled;
    self.micBtn.selected = ZegoManager.micSelected;
    [self.micBtn setImage:[UIImage imageNamed:@"ic_mic_nor"] forState:UIControlStateNormal];
    [self.micBtn setImage:[UIImage imageNamed:@"ic_mic"] forState:UIControlStateSelected];
    [self.micBtn addTarget:self action:@selector(onTouchMicButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_micBtn];
    [self.micBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.muteBtn.mas_trailing).with.offset(10);
        make.top.equalTo(self.view).with.offset(NavHeight + 4.5);
        make.width.mas_equalTo(34);
        make.height.mas_equalTo(21);
    }];
    
    self.clanCallButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.clanCallButton.backgroundColor = [UIColor colorWithHexString:COMMON_PINK];
    self.clanCallButton.clipsToBounds = YES;
    self.clanCallButton.layer.cornerRadius = 21 / 2;
    self.clanCallButton.layer.borderWidth = 1;
    self.clanCallButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.clanCallButton.titleLabel.font = [UIFont systemFontOfSize:12];
    [self.clanCallButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.clanCallButton setTitle:SPDStringWithKey(@"   召唤   ", nil) forState:UIControlStateNormal];
    [self.clanCallButton addTarget:self action:@selector(onTouchClanCallButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.clanCallButton];
    [self.clanCallButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-13);
        make.top.mas_equalTo(NavHeight + 4.5);
        make.height.mas_equalTo(21);
        make.leading.mas_greaterThanOrEqualTo(self.micBtn.mas_trailing).with.offset(10);
    }];
    
    // 世界消息滚动区
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(13, NavHeight + 30 + 51 + 9, 14, 12)];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"global_notification" ofType:@"gif"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    UIImage *image = [UIImage sd_animatedGIFWithData:data];
    imageView.image = image;
    [self.view addSubview:imageView];
    
    self.globalScrollView = [[SPDVoiceLiveGlobalScrollView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imageView.frame) + 5, NavHeight + 30 + 51, kScreenW - CGRectGetMaxX(imageView.frame) - 5, 30)];
    self.globalScrollView.space = self.globalScrollView.bounds.size.width;
    self.globalScrollView.speed = 50;
    self.globalScrollView.isRepeat = YES;
    [self.view addSubview:_globalScrollView];
    
    // 公共麦按钮
    self.publicMicBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.publicMicBtn setImage:[UIImage imageNamed:@"ic_public_mic"] forState:UIControlStateNormal];
    [self.publicMicBtn addTarget:self action:@selector(usePublicMic) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.publicMicBtn];
    [self.publicMicBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(self.view).with.offset(-19);
        make.bottom.equalTo(self.view).with.offset(-50 - 30.5);
        make.width.and.height.equalTo(@39);
    }];
    
    // 分享麦按钮
    UIButton *shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [shareBtn setImage:[UIImage imageNamed:@"chatroom_share"] forState:UIControlStateNormal];
//    [shareBtn addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
    [shareBtn setImage:[UIImage imageNamed:@"chatroom_signin"] forState:UIControlStateNormal];
    [shareBtn addTarget:self action:@selector(signIn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:shareBtn];
    [shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(self.view).with.offset(-19);
        make.bottom.equalTo(self.publicMicBtn.mas_top).with.offset(-15);
        make.width.and.height.equalTo(@39);
    }];
    
    
    // 活动按钮
    [self.view addSubview:self.activityScrollView];
    [self.activityScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-19);
        make.bottom.mas_equalTo(shareBtn.mas_top).with.offset(-15);
        make.width.mas_equalTo(39);
        make.height.mas_equalTo(39);
    }];
    
    //聊天消息区
    if (nil == self.conversationMessageCollectionView) {
        UICollectionViewFlowLayout *customFlowLayout = [[UICollectionViewFlowLayout alloc] init];
        customFlowLayout.minimumLineSpacing = 0;
        customFlowLayout.sectionInset = UIEdgeInsetsMake(10.0f, 0.0f,5.0f, 0.0f);
        customFlowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        CGRect _conversationViewFrame = self.view.bounds;
        _conversationViewFrame.origin.y = 0;
        _conversationViewFrame.size.height = kScreenH-collectionOriginalY-MinHeight_InputView;
        _conversationViewFrame.size.width = kScreenW;
        self.conversationMessageCollectionView =
        [[UICollectionView alloc] initWithFrame:_conversationViewFrame
                           collectionViewLayout:customFlowLayout];
        [self.conversationMessageCollectionView
         setBackgroundColor:[UIColor clearColor]];
        self.conversationMessageCollectionView.showsHorizontalScrollIndicator = NO;
        self.conversationMessageCollectionView.alwaysBounceVertical = YES;
        self.conversationMessageCollectionView.dataSource = self;
        self.conversationMessageCollectionView.delegate = self;
        [self.contentView addSubview:self.conversationMessageCollectionView];
    }
    
    [self.contentView addSubview:self.globalNotifyView];
    [self.contentView addSubview:self.nobleEnterView];
    
    //座驾进场效果展示
    self.vehicleEffectView = [[SPDVehicleEffectImageView alloc] initWithFrame:CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height - 50)];
    [self.contentView addSubview:self.vehicleEffectView];
    
    //礼物效果展示
    self.effectsView = [[SPDChatRoomEffectImageView alloc]initWithFrame:CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height-49)];//-49 是为了让出输入框的高度
    [self.contentView addSubview:self.effectsView];
    
    //魔法效果的展示
    [self.contentView addSubview:self.magicAnimationView];
    
    //输入区
    if(self.inputBar == nil){
        float inputBarOriginY =self.conversationMessageCollectionView.bounds.size.height;
        float inputBarOriginX = self.conversationMessageCollectionView.frame.origin.x;
        float inputBarSizeWidth = self.contentView.frame.size.width;
        float inputBarSizeHeight = MinHeight_InputView;
        self.inputBar = [[RCDLiveInputBar alloc]initWithFrame:CGRectMake(inputBarOriginX, inputBarOriginY,inputBarSizeWidth,inputBarSizeHeight)];
        self.inputBar.delegate = self;
        self.inputBar.backgroundColor = [UIColor whiteColor];
        //        self.inputBar.hidden = YES;
        [self.contentView addSubview:self.inputBar];
    }
    
    // 座驾动效展示
    self.vehicleAnimationView = [[SPDVoiceLiveGlobalScrollView alloc] initWithFrame:CGRectMake(0, self.inputBar.frame.origin.y - 75, self.contentView.frame.size.width, 75)];
    self.vehicleAnimationView.space = (self.vehicleAnimationView.bounds.size.width / 2) - (200 / 2);
    self.vehicleAnimationView.speed = self.vehicleAnimationView.bounds.size.width;
    self.vehicleAnimationView.isRepeat = NO;
    [self.contentView addSubview:_vehicleAnimationView];
    
    [self.contentView addSubview:self.comeInChatRoomView];
    [self.view addSubview:self.privatePresentView];
    [self registerClass:[RCDMSChatRoomCell class]forCellWithReuseIdentifier:rctextCellIndentifier];
    [self registerClass:[SPDBannedSpeakCell class] forCellWithReuseIdentifier:SPDBannedSpeakCellIndentifier];
    [self registerClass:[ChatroomRedPacketMsgCell class] forCellWithReuseIdentifier:@"ChatroomRedPacketMsgCell"];
    [self registerClass:[NobleGreetingMsgCell class] forCellWithReuseIdentifier:@"NobleGreetingMsgCell"];
}

/**
 *  未读消息View
 *
 *  @return <#return value description#>
 */
- (UIView *)unreadButtonView {
    if (!_unreadButtonView) {
        _unreadButtonView = [[UIView alloc]initWithFrame:CGRectMake((self.view.frame.size.width - 80)/2, self.view.frame.size.height - MinHeight_InputView - 30, 80, 30)];
        _unreadButtonView.userInteractionEnabled = YES;
        _unreadButtonView.backgroundColor = RCDLive_HEXCOLOR(0xffffff);
        _unreadButtonView.alpha = 0.7;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tabUnreadMsgCountIcon:)];
        [_unreadButtonView addGestureRecognizer:tap];
        _unreadButtonView.hidden = YES;
        [self.view addSubview:_unreadButtonView];
        _unreadButtonView.layer.cornerRadius = 4;
    }
    return _unreadButtonView;
}

/**
 *  底部新消息文字
 *
 *  @return return value description
 */
- (UILabel *)unReadNewMessageLabel {
    if (!_unReadNewMessageLabel) {
        _unReadNewMessageLabel = [[UILabel alloc]initWithFrame:_unreadButtonView.bounds];
        _unReadNewMessageLabel.backgroundColor = [UIColor clearColor];
        _unReadNewMessageLabel.font = [UIFont systemFontOfSize:12.0f];
        _unReadNewMessageLabel.textAlignment = NSTextAlignmentCenter;
        _unReadNewMessageLabel.textColor = RCDLive_HEXCOLOR(0xff4e00);
        [self.unreadButtonView addSubview:_unReadNewMessageLabel];
    }
    return _unReadNewMessageLabel;
    
}

/**
 *  更新底部新消息提示显示状态
 */
- (void)updateUnreadMsgCountLabel{
    if (self.unreadNewMsgCount == 0) {
        self.unreadButtonView.hidden = YES;
    }
    else{
        self.unreadButtonView.hidden = NO;
        self.unReadNewMessageLabel.text = SPDStringWithKey(@"底部有新消息", nil);
    }
}

/**
 *  检查是否更新新消息提醒
 */
- (void) checkVisiableCell{
    NSIndexPath *lastPath = [self getLastIndexPathForVisibleItems];
    if (lastPath.row >= self.conversationDataRepository.count - self.unreadNewMsgCount || lastPath == nil || [self isAtTheBottomOfTableView] ) {
        self.unreadNewMsgCount = 0;
        [self updateUnreadMsgCountLabel];
    }
}

/**
 *  获取显示的最后一条消息的indexPath
 *
 *  @return indexPath
 */
- (NSIndexPath *)getLastIndexPathForVisibleItems
{
    NSArray *visiblePaths = [self.conversationMessageCollectionView indexPathsForVisibleItems];
    if (visiblePaths.count == 0) {
        return nil;
    }else if(visiblePaths.count == 1) {
        return (NSIndexPath *)[visiblePaths firstObject];
    }
    NSArray *sortedIndexPaths = [visiblePaths sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSIndexPath *path1 = (NSIndexPath *)obj1;
        NSIndexPath *path2 = (NSIndexPath *)obj2;
        return [path1 compare:path2];
    }];
    return (NSIndexPath *)[sortedIndexPaths lastObject];
}

/**
 *  点击未读提醒滚动到底部
 *
 *  @param gesture gesture description
 */
- (void)tabUnreadMsgCountIcon:(UIGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateEnded) {
        [self scrollToBottomAnimated:YES];
    }
}

-(void)showInputBar:(id)sender{
    self.inputBar.hidden = NO;
    [self.inputBar setInputBarStatus:RCDLiveBottomBarKeyboardStatus];
}

#pragma mark - 聊天室背景图片 & 聊天室锁

- (void)showPopoverView:(UIButton *)sender {
    JXPopoverView *popoverView = [JXPopoverView popoverView];
    popoverView.style = PopoverViewStyleDark;
    
    NSMutableArray *actions = [NSMutableArray array];
    if (ZegoManager.chatroomLock) {
        if (ZegoManager.isLock) {
            JXPopoverAction *unlockAction = [JXPopoverAction actionWithImage:[UIImage imageNamed:@"chatroom_lock_unlock"] title:SPDStringWithKey(@"解锁", nil) handler:^(JXPopoverAction *action) {
                [ZegoManager requestChatroomLockUseWithType:@"unlock" password:@""];
            }];
            [actions addObject:unlockAction];
            JXPopoverAction *modifyAction = [JXPopoverAction actionWithImage:[UIImage imageNamed:@"chatroom_lock_modify"] title:SPDStringWithKey(@"修改密码", nil) handler:^(JXPopoverAction *action) {
                [ZegoManager showPasswordViewWithType:PasswordViewTypeModify];
            }];
            [actions addObject:modifyAction];
        } else {
            JXPopoverAction *lockAction = [JXPopoverAction actionWithImage:[UIImage imageNamed:@"chatroom_lock_lock"] title:SPDStringWithKey(@"上锁", nil) handler:^(JXPopoverAction *action) {
                if (ZegoManager.canLock) {
                    [ZegoManager showPasswordViewWithType:PasswordViewTypeLock];
                } else {
                    [self presentCommonController:SPDStringWithKey(@"房间上锁需要购买房间锁", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                        [self pushToChatroomLockViewController];
                    }];
                }
            }];
            [actions addObject:lockAction];
        }
    }
    JXPopoverAction *bgAction = [JXPopoverAction actionWithImage:[UIImage imageNamed:@"ic_chatroom_bg_btn"] title:SPDStringWithKey(@"主题", nil) handler:^(JXPopoverAction *action) {
        [self pushToChatroomBgImageController];
    }];
    [actions addObject:bgAction];
    
    [popoverView showToView:sender withActions:actions];
}

- (void)pushToChatroomLockViewController {
    ChatroomLockViewController *vc = [[ChatroomLockViewController alloc] init];
    vc.clanId = self.clan_id;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)pushToChatroomBgImageController {
    ChatroomBgImageController *vc = [[ChatroomBgImageController alloc] init];
    vc.clanId = self.clan_id;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)pushToChatroomRankViewController {
    ChatroomRankViewController *vc = [[ChatroomRankViewController alloc] init];
    vc.clanId = self.clan_id;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - ChatroomBgImageControllerDelegate

- (void)backgroundDidChangeToImage:(NSString *)image {
    [self.backgroundImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, image]] placeholderImage:self.backgroundImageView.image];
}

#pragma mark - 分享

- (void)share {
    if (self.shareDict) {
        [self.inputBar setInputBarStatus:RCDLiveBottomBarDefaultStatus];
        [self.shareView setShareDict:self.shareDict];
        [self.shareView show];
    }
}

- (void)infoShareView:(ZTWAInfoShareView *)infoShareView shareToPlatformType:(SPDUMShareType)sharePlatformType {
    NSString *shareUrl = self.shareDict[@"share_url"];
    NSString *sharetitle = self.shareDict[@"title"];
    NSString *shareContent = self.shareDict[@"content"];
    NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:self.shareDict[@"thumbnail"]]];
    UIImage *image = [[UIImage alloc] initWithData:data];
    
//    [[SPDUMShareUtils shareInstance] shareToPlatform:sharePlatformType WithShareUrl:shareUrl andshareTitle:sharetitle andshareContent:shareContent andImageData:image andShareSourece:@"chatroom" andSPDUMShareSuccess:^(id shareSuccess) {
//        [self.shareView dismiss];
//    } presentedController:self andSPDUMShareFailure:^(id shareFailure) {
//        
//    }];
}

- (void)clickedInviteCount {
    NSString * str = [NSString stringWithFormat:INVITE_DETAIL, [SPDApiUser currentUser].userId, [SPDCommonTool getFamyLanguage]];
    [self loadWebViewController:self title:SPDStringWithKey(@"邀请", nil) url:str];
}

#pragma mark - 签到

- (void)openLottery {
    
    //[MobClick event:@"clickActivityeBtn"];
}

- (void)signIn {
    if (self.positionLevel > 0 && self.positionLevel < 250) {
        [self loadWebViewController:self title:@"" url:[NSString stringWithFormat:CLAN_SIGNIN_URL, [SPDApiUser currentUser].lang, [SPDApiUser currentUser].userId, self.clan_id, [[AppManager sharedInstance] getDeviceId]]];
    } else {
        [self showToast:SPDStringWithKey(@"无家族请创建／加入家族后签到领金币，有家族请回到自己家族签到领金币", nil)];
    }
}

#pragma mark - 数据请求

- (void)requestSysConfig {
    NSDictionary *dic = @{@"ios_version": LOCAL_VERSION_SHORT};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"sys.config" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable dict) {
        NSDictionary *config = dict[@"config"];
        self.isEnableZegoKick = [config[@"isEnableZegoKick"] isEqualToString:@"on"];
        self.isOpenLawLessCheck = [config[@"isOpenLawLessCheck"] isEqualToString:@"on"];
        ZegoManager.chatroomLock = [config[@"chatroomLock"] isEqualToString:@"on"];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestUserInfoData {
    self.isRequestPosition = YES;
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.info.detail" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        
        self.shareDict = suceess[@"share"];
        self.shareView.invite_code = suceess[@"invite_code"];
        NSString * myclan_invite_code = [[NSUserDefaults standardUserDefaults] objectForKey:MYINVITECOUNT];
        if (myclan_invite_code) {
            self.shareView.invite_count = [myclan_invite_code intValue];
        }else{
            self.shareView.invite_count = 0;
        }
        //刷新用户在融云的信息
        
        RCUserInfo * currentUserInfo =[[RCIM sharedRCIM] currentUserInfo];
        currentUserInfo.portraitUri = [NSString stringWithFormat:STATIC_DOMAIN_URL,suceess[@"avatar"]];
        currentUserInfo.name = suceess[@"nick_name"];
        [[RCIM sharedRCIM] refreshUserInfoCache:currentUserInfo withUserId:currentUserInfo.userId];

        self.specialNum = suceess[@"specialNum"];
        self.is_agent_gm = [suceess[@"is_agent_gm"] boolValue];
        self.noble = suceess[@"is_noble"][@"noble_type"];
        [self requestFamilyDetail];
    } bFailure:^(id  _Nullable failure) {
        [self requestFamilyDetail];
    }];
}

-(void)requestUserDetail {
    
    NSDictionary * dict = @{
                            @"user_id":[SPDApiUser currentUser].userId,
                            };
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dict] bURL:@"user.detail" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        
        NSDictionary * charmDict = suceess[@"charm"];
        NSDictionary * richeDict = suceess[@"riche"];
        
        if ([suceess[@"is_vip"] isKindOfClass:[NSNull class]]) {
            self.is_vip = false;
        }else{
            self.is_vip = [suceess[@"is_vip"] boolValue];
        }
        
        if (charmDict != nil) {
            int charmRank = [charmDict[@"rank"] intValue];
            NSString * charmSign = charmDict[@"sign"];
            NSString *secretStr = [NSString stringWithFormat:@"_id=%@&rank=%d&key=%@", [SPDApiUser currentUser].userId,charmRank ,jianyue_secretKey];
            if ([secretStr.SHA256AndBase64 isEqualToString:charmSign]) {
                self.charmRankStr = [SPDCommonTool getCharmRankNumber:charmRank];
            }
        }
        if (richeDict != nil) {
            int richeRank = [richeDict[@"rank"] intValue];
            NSString * richeSign = richeDict[@"sign"];
            NSString *secretStr = [NSString stringWithFormat:@"_id=%@&rank=%d&key=%@", [SPDApiUser currentUser].userId, richeRank,jianyue_secretKey];
            if ([secretStr.SHA256AndBase64 isEqualToString:richeSign]) {
                self.richeRankStr = [SPDCommonTool getRichRankNumber:richeRank];
            }
            
        }
        if (suceess[@"level"] != nil) {
            NSString * levelSign = suceess[@"level"][@"sign"];
            NSString *secretStr = [NSString stringWithFormat:@"_id=%@&user_level=%d&key=%@",[SPDApiUser currentUser].userId,[suceess[@"level"][@"user_level"] intValue] ,jianyue_secretKey];
            if ([secretStr.SHA256AndBase64 isEqualToString:levelSign]) {
                self.userLevelStr = suceess[@"level"][@"user_level"];
            }
        }
        
        self.isHaveClan = ![SPDCommonTool isEmpty:suceess[@"clan_id"]];
    } bFailure:^(id  _Nullable failure) {
        
    }];
    
}

- (void)requestFamilyDetail {
    self.isRequestPosition = YES;
    NSDictionary *dic = @{@"clan_id": _clan_id, @"user_id": [SPDApiUser currentUser].userId};
    
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"detail" isClan:YES bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        
        self.familyInfoDict = [NSMutableDictionary dictionaryWithDictionary:suceess];
        
        ZegoManager.clan_cover = suceess[@"clan"][@"avatar"];
        ZegoManager.isLock = [suceess[@"clan"][@"isLock"] boolValue];
        ZegoManager.canLock = [suceess[@"clan"][@"canLock"] boolValue];
        NSString *backgroundImage = suceess[@"clan"][@"background_image"];
        [self.backgroundImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, backgroundImage]] placeholderImage:backgroundImage ? self.backgroundImageView.image : [UIImage imageNamed:@"img_chatroom_bg"]];
        self.titleView.nickNameLabel.text = suceess[@"clan"][@"name"];
        self.titleView.level = suceess[@"clan"][@"clanLevel"];
        self.titleView.statusLabel.text = [NSString stringWithFormat:@"ID: %@", suceess[@"clan"][@"nick_id"]];
        self.titleView.tasks = suceess[@"clan"][@"tasks"];
        self.position = suceess[@"position"];
        self.clan_name = suceess[@"clan"][@"name"];
        self.myTagPosition = suceess[@"position"];
        self.rankTotalGold = suceess[@"clan"][@"totalGold"];
        for (NSDictionary *dic in suceess[@"clan"][@"super_admin"]) {
            NSString *secretStr = [NSString stringWithFormat:@"user_id=%@&key=%@", dic[@"user_id"], jianyue_secretKey];
            if ([secretStr.SHA256AndBase64 isEqualToString:dic[@"sign"]]) {
                SPDFamilyMemberModel *model = [SPDFamilyMemberModel initWithDictionary:dic];
                model.position = @"superadmin";
                [self.familyMembers addObject:model];
                if ([model.user_id isEqualToString:[SPDApiUser currentUser].userId]) {
                    self.position = @"superadmin";
                }
            }
        }
        for (NSDictionary *dic in suceess[@"clan"][@"members"]) {
            SPDFamilyMemberModel *model = [SPDFamilyMemberModel initWithDictionary:dic];
            model.position = @"member";
            [self.familyMembers addObject:model];
        }
        for (NSDictionary *dic in suceess[@"clan"][@"deputy_chiefs"]) {
            SPDFamilyMemberModel *model = [SPDFamilyMemberModel initWithDictionary:dic];
            model.position = @"deputychief";
            [self.familyMembers addObject:model];
        }
        SPDFamilyMemberModel *model = [SPDFamilyMemberModel initWithDictionary:suceess[@"clan"][@"chief"]];
        model.position = @"chief";
        [self.familyMembers addObject:model];
        self.isRequestPosition = NO;
        [self handleInsertGreeting];
        
    } bFailure:^(id  _Nullable failure) {
        self.isRequestPosition = NO;
    }];
}

- (void)handleInsertGreeting {
    if ([self.position isEqualToString:@"chief"] ) {
        return;
    }
    
    SPDFamilyMemberModel *chiefModel = [SPDFamilyMemberModel initWithDictionary:self.familyInfoDict[@"clan"][@"chief"]];
    NSString * greeting = self.familyInfoDict[@"clan"][@"greeting"];
    NSString *greetingStr = (greeting && ![greeting isEqualToString:@"默认欢迎语"]) ? greeting : SPDStringWithKey(@"欢迎来我们家族一起玩耍，有很多小伙伴在等着你喔～", nil);
    NSString *content = [NSString stringWithFormat:@"@%@ %@", [SPDApiUser currentUser].nickName, greetingStr];
    RCUserInfo *userInfo = [[RCUserInfo alloc] initWithUserId:chiefModel.user_id name:chiefModel.nick_name portrait:chiefModel.avatar];
    
    RCMessageContent *messageContent;
    if (self.noble) {
        SPDNobleGreetingMessage *message = [[SPDNobleGreetingMessage alloc] init];
        message.content = content;
        message.noble = self.noble;
        message.gender = chiefModel.gender;
        message.senderUserInfo = userInfo;
        CGSize textSize = [RCDMSChatRoomCell getMessageCellSize:content withWidth:kScreenW - 119.5];
        message.textSize = textSize;
        messageContent = message;
    } else {
        SPDTextMessage *message = [[SPDTextMessage alloc]init];
        message.content = content;
        message.is_greetings = @"true";
        message.gender = chiefModel.gender;
        message.age = [NSString stringWithFormat:@"%@", chiefModel.age];
        message.senderUserInfo = userInfo;
        message.isHaveClan = YES;
        LY_Portrait *portrait = [[LY_Portrait alloc] init];
        portrait.url = [LY_User currentUser].portrait;
        portrait.headwearUrl = [LY_User currentUser].headwearWebp;
        message.portrait = portrait;
        messageContent = message;
    }
    RCMessage *message = [[RCMessage alloc] initWithType:self.conversationType
                                                targetId:self.targetId
                                               direction:MessageDirection_SEND
                                               messageId:-1
                                                 content:messageContent];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self appendAndDisplayMessage:message];
    });
}

- (void)requestClanFollow {
    NSDictionary *dic = @{@"user_id": [SPDApiUser currentUser].userId, @"clan_id": self.clan_id, @"action": @"follow"};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"follow" isClan:YES bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        [self isFollowingDidChange:YES];
        [self showToast:SPDStringWithKey(@"关注家族成功", nil)];
    } bFailure:^(id  _Nullable failure) {
        if ([failure isKindOfClass:[NSDictionary class]]) {
            [self showToast:failure[@"msg"]];
        }
    }];
}

- (void)requestUsersInfo:(NSArray *)users isUpdate:(BOOL)isUpdate {
    NSMutableArray *userIds = [NSMutableArray array];
    for (ZegoUserState *user in users) {
        [userIds addObject:user.userID];
    }
    if (!isUpdate) {
        [userIds addObject:[SPDApiUser currentUser].userId];
    }
    NSMutableDictionary *dict = [@{@"clan_id": self.clan_id} mutableCopy];
    [dict setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:userIds options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding] forKey:@"user_ids"];
    [RequestUtils commonGetRequestUtils:dict bURL:@"chatroom.users" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        if (!isUpdate && userIds.count > 1) {
            [self.onlineUsers removeAllObjects];
            [self.audiences removeAllObjects];
        }
        BOOL isOnMic = NO;
        for (NSDictionary *dic in suceess[@"users"]) {
            SPDVoiceLiveUserModel *model = [SPDVoiceLiveUserModel initWithDictionary:dic];
            [self.onlineUsers setObject:model forKey:model._id];
            if (![ZegoManager getUserMicIndex:model._id]) {
                [self.audiences setObject:model forKey:model._id];
            } else {
                isOnMic = YES;
            }
        }
        if (isUpdate) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (!_isRequestMicStatus) {
                    self.onlineNumber = self.onlineUsers.count;
                    self.sortedAudiences = [self sortAudiences];
                    if (isOnMic) {
                        [self.micCollectionView reloadData];
                    }
                    [self.onlineUserConllectionView reloadData];
                }
            });
        } else {
            [self requestMicStatusAndCheckOffline:(userIds.count > 1)];
            if (self.onlineUsers.count >= 10) {
                [self requestChatroomRedPacketTrigger];
            }
        }
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestMicStatus {
    [self requestMicStatusAndCheckOffline:NO];
}

- (void)requestMicStatusAndCheckOffline:(BOOL)checkOffline {
    NSDictionary *dic = @{@"chatroom_id": _clan_id};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"chatroom.status" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        [self dealMicStatusData:suceess checkOffline:checkOffline];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)dealMicStatusData:(id _Nullable)data checkOffline:(BOOL)checkOffline {
    self.isRequestMicStatus = YES;
    NSDictionary *oldMicUsers = [ZegoManager.micUsers copy];
    [ZegoManager.micUsers removeAllObjects];
    for (NSDictionary *dic in data[@"micstatus"]) {
        NSString *index = [NSString stringWithFormat:@"%@", dic[@"index"]];
        [ZegoManager.micStatus setValue:dic[@"status"] forKey:index];
        NSDictionary *userDic = dic[@"user"];
        if ([dic[@"status"] intValue] != 0 && [dic[@"status"] intValue] != 4 && userDic.count) {
            SPDVoiceLiveUserModel *model = [SPDVoiceLiveUserModel initWithDictionary:userDic];
            [ZegoManager.micUsers setObject:model forKey:index];
            [self.audiences removeObjectForKey:model._id];
            if ([model._id isEqualToString:[SPDApiUser currentUser].userId]) {
                [self.api startPublish];
                if (!oldMicUsers[index]) {
                    [self showToast:SPDStringWithKey(@"您已经上麦，可以开始讲话", nil)];
                }
                ZegoManager.selfMicIndex = index;
                if ([ZegoManager.micStatus[index] intValue] == 3) {
                    [self enableMic:NO];
                    ZegoManager.selfEndStatus = @"3";
                } else {
                    [self enableMic:YES];
                    ZegoManager.selfEndStatus = @"0";
                    if ([ZegoManager.micStatus[index] intValue] == 2) {
                        [self.api enableMic:NO];
                        self.micBtn.selected = NO;
                        ZegoManager.micSelected = NO;
                    }
                }
            } else if (checkOffline && !_onlineUsers[model._id]) {
                [self stopUseMic:model._id index:index.integerValue isCheck:YES];
            }
        } else {
            SPDVoiceLiveUserModel *model = oldMicUsers[index];
            if (model) {
                if (![ZegoManager getUserMicIndex:model._id] && _onlineUsers[model._id]) {
                    if ([model._id isEqualToString:[SPDApiUser currentUser].userId]) {
                        [self.api stopPublish];
                        [self enableMic:NO];
                        ZegoManager.selfMicIndex = nil;
                        [self deleteStreamWithUserID:[SPDApiUser currentUser].userId];
                    }
                    [self.audiences setObject:model forKey:model._id];
                }
            }
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        self.onlineNumber = self.onlineUsers.count;
        self.sortedAudiences = [self sortAudiences];
        [self.micCollectionView reloadData];
        [self.onlineUserConllectionView reloadData];
        self.isRequestMicStatus = NO;
        if (self.positionLevel >= 75 && self.isOpenLawLessCheck) {
            [ZegoManager startCheckStreams];
        }
    });
}

- (NSArray *)sortAudiences {
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"level" ascending:NO];
    return [[self.audiences allValues] sortedArrayUsingDescriptors:@[sortDescriptor]];
}

- (void)requestChatroomRedPacketTrigger {
    NSDictionary *dic = @{@"clanId": self.clan_id};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"chatroom.redPacket.trigger" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestChatroomRedPacketInfoWithMsg:(SPDChatroomRedPacketMessage *)message {
    NSDictionary *dic = @{@"redPacketId": message.redPacketId};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"chatroom.redPacket.info" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        ChatroomRedPacketModel *model = [ChatroomRedPacketModel initWithDictionary:suceess];
        model.redPacketId = message.redPacketId;
        
        if (![self.view viewWithTag:904]) {
            SPDChatroomRedPacketView *view = [[[NSBundle mainBundle] loadNibNamed:@"SPDChatroomRedPacketView" owner:self options:nil] firstObject];
            view.frame = self.view.bounds;
            view.tag = 904;
            view.model = model;
            [self.view addSubview:view];
        }
    } bFailure:^(id  _Nullable failure) {
        if ([failure isKindOfClass:[NSDictionary class]]) {
            [self showToast:failure[@"msg"]];
        }
    }];
}

#pragma mark - 长按头像@

- (void)handleChatRoomLongPress:(NSNotification *)noti {
    if (self.notice_userInfo && self.notice_userInfo.count) {
        self.notice_userInfo = noti.userInfo;
        self.inputBar.chatSessionInputBarControl.inputTextView.text = [NSString stringWithFormat:@"@%@ ", _notice_userInfo[@"nick_name"]];
    } else {
        self.notice_userInfo = noti.userInfo;
        self.inputBar.chatSessionInputBarControl.inputTextView.text = [self.inputBar.chatSessionInputBarControl.inputTextView.text stringByAppendingFormat:@"@%@ ", _notice_userInfo[@"nick_name"]];
    }
    [self.inputBar.chatSessionInputBarControl.inputTextView becomeFirstResponder];
}

#pragma mark - 输入框事件

- (void)onInputTextViewDidChange:(UITextView *)inputTextView {
    if (self.notice_userInfo && self.notice_userInfo.count) {
        if (![inputTextView.text containsString:[NSString stringWithFormat:@"@%@ ", _notice_userInfo[@"nick_name"]]]) {
            self.notice_userInfo = nil;
        }
    }
}

/**
 *  点击键盘回车或者emoji表情面板的发送按钮执行的方法
 *
 *  @param text  输入框的内容
 */
- (void)onTouchSendButton:(NSString *)text{
    
    if (text.length <= 100) {
        
        if (self.enableSendGlobal) {
            NSMutableString * msgText ;
            msgText =[[NSMutableString alloc]initWithString:text];
            NSArray* arr = [self.wordDict allKeys];
            for (NSString * keys in arr) {
                text=[text stringByReplacingOccurrencesOfString: keys withString:@"***"];
            }
            SPDWorldChatMessage *rcWordMessage = [SPDWorldChatMessage SPDWorldChatMessageWithContent:[NSString stringWithFormat:@"%@",text] andsend_ClanId:self.clan_id andsend_ClanName:self.clan_name andGender:[SPDApiUser currentUser].gender];
            //扣费成功 之后 发送世界消息
            if (self.notice_userInfo && self.notice_userInfo.count) {
                rcWordMessage.mentionedType = @"2";
                rcWordMessage.mentionedList = @[self.notice_userInfo];
            }
            [self requestSeverToSendGlobalMessage:rcWordMessage];
        }else{
            //判断包不包含关键词
            
            BOOL isBool = [self.keyword_match match:text withKeywordMap:self.keymap];
            
            SPDTextMessage *rcTextMessage  = [self getTextMessageWithText:text];
            if (self.notice_userInfo && self.notice_userInfo.count) {
                rcTextMessage.mentionedType = @"2";
                rcTextMessage.mentionedList = @[self.notice_userInfo];
            }
            self.notice_userInfo = nil;
            rcTextMessage.specialNum = self.specialNum ? : @"";
            rcTextMessage.is_agent_gm = self.is_agent_gm;
            rcTextMessage.noble = self.noble ? : @"";
            rcTextMessage.isHaveClan = self.isHaveClan;
            LY_Portrait *portrait = [[LY_Portrait alloc] init];
            portrait.url = [LY_User currentUser].portrait;
            portrait.headwearUrl = [LY_User currentUser].headwearWebp;
            rcTextMessage.portrait = portrait;
//            RCUserInfo *userInfo = [RCIM sharedRCIM].currentUserInfo;
//            NSString * userInfoName = userInfo.name;
//            if (userInfoName.length >8) {
//                userInfoName = [userInfoName substringToIndex:8];
//            }
//            userInfo.name = userInfoName;
//            [[RCIM sharedRCIM] setCurrentUserInfo:userInfo];
            if (isBool) {
                rcTextMessage.senderUserInfo = [RCIM sharedRCIM].currentUserInfo;
                RCMessage *message = [[RCMessage alloc] initWithType:self.conversationType
                                                            targetId:self.targetId
                                                           direction:MessageDirection_SEND
                                                           messageId:-1
                                                             content:rcTextMessage];
                if (!rcTextMessage.senderUserInfo) {
                    return;
                }
                [self appendAndDisplayMessage:message];
                [self.inputBar clearInputView];
                
            }else{
                [self sendMessage:rcTextMessage pushContent:nil];
            }
        }
        
    }else if(text.length >100) {
        [self showToast:SPDStringWithKey(@"输入字数太多了哦！", nil)];
    }
    
    [self.inputBar.chatSessionInputBarControl resetSendMessageBtn];
    if (IS_IPAD) {
        [self.inputBar setInputBarStatus:RCDLiveBottomBarDefaultStatus];
    }
    
}

-(SPDTextMessage *)getTextMessageWithText:(NSString *)text {
    SPDTextMessage * txtMsg = [[SPDTextMessage alloc]init];
    txtMsg.content = text;
    txtMsg.clan_level = [SPDCommonTool getTextMsgClanMarkWithPosition:self.myTagPosition];
    txtMsg.charm_level = self.charmRankStr;
    NSInteger level = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"%@%@",MYCURRENTLEVEL,[SPDApiUser currentUser].userId]];
    if ([self.userLevelStr integerValue] < level) {
        self.userLevelStr = [NSString stringWithFormat:@"%@",@(level)];
    }
    txtMsg.rich_level = self.richeRankStr;
    txtMsg.user_level = self.userLevelStr;
    txtMsg.age = [SPDApiUser currentUser].age.stringValue;
    txtMsg.gender = [SPDApiUser currentUser].gender;
    LY_Portrait *portrait = [[LY_Portrait alloc] init];
    portrait.url = [LY_User currentUser].portrait;
    portrait.headwearUrl = [LY_User currentUser].headwearWebp;
    txtMsg.portrait = portrait;
    return txtMsg;
}


#pragma mark - 家族召唤
//家族的召唤
-(void)onTouchClanCallButton:(UIButton *)sender {
    
    NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
    NSString *error=[userDefault objectForKey:@"errorTime"];
    if (error==nil) {
        [self popsendCallAlertView];
    }else{
        double  timeInterval=[error doubleValue];
        NSTimeInterval current=[[NSDate date] timeIntervalSince1970];
        if (current-timeInterval<=90) {
            [self showToast:SPDStringWithKey(@"90s之后再试试哦！", nil)];
        }else if (current-timeInterval>90){
            [self popsendCallAlertView];
        }
        
    }
    
}

-(void)popsendCallAlertView {
    _calnCallAlertView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([SPDClanCallAlertView class]) owner:self options:nil]lastObject];
    if (self.is_vip) {
        _calnCallAlertView.frame  = CGRectMake(0, 0, pixwn(275), 221);
    }else{
        _calnCallAlertView.frame  = CGRectMake(0, 0, pixwn(275), 255);
    }
    _calnCallAlertView.is_Vip = self.is_vip;
    __weak typeof(self) weakSelf = self;
    [_calnCallAlertView setCancleButtonClicked:^{
        [weakSelf.alert hide];
    }];
    [_calnCallAlertView setVipButtonClicked:^{
        [weakSelf.alert hide];
        [weakSelf handleVipClicked];
    }];
    [_calnCallAlertView setUseMoneyButtonClicked:^{
        [weakSelf.alert hide];
        weakSelf.clanCallButton.userInteractionEnabled = NO;
        [weakSelf sendClanCallRequestWithNum:1000];
    }];
    self.alert.contentView = _calnCallAlertView;
    [self.alert show];
}

-(void)handleVipClicked{
    
    if (self.is_vip) {
        self.clanCallButton.userInteractionEnabled = NO;
        [self sendClanCallRequestWithNum:0];
    }else{
        [self pushToVipViewController:self WithIsVip:self.is_vip];
    }
}

-(void)sendClanCallRequestWithNum:(int)num {
    
    NSDictionary * dict = @{
                            @"user_id":[SPDApiUser currentUser].userId,
                            @"clan_id":self.clan_id,
                            @"num":@(num)};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dict] bURL:@"call" isClan:YES bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        [self showMessageToast:SPDStringWithKey(@"召唤成功,帅哥美女正在来的路上", nil)];
        self.clanCallButton.userInteractionEnabled = YES;
    } bFailure:^(id  _Nullable failure) {
        self.clanCallButton.userInteractionEnabled = YES;
        if ([failure[@"code"] integerValue] == 203) {
            [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil)messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
            } confirmTitle:SPDStringWithKey(@"充值", nil) confirmAction:^{
                [self pushToRechageController];
            }];
        }else {
            [self showToast:failure[@"msg"]];
        }
    }];
    
    
}

#pragma mark - 静音
//静音的切换 -- 听不到别人说话
-(void)onTouchMuteButton:(UIButton *)sender {
    sender.selected = !sender.selected;
    [ZegoManager enableSpeaker:sender.selected];
}

- (void)onTouchMicButton:(UIButton *)sender {
    sender.selected = !sender.selected;
    if ([self.api enableMic:sender.selected]) {
        ZegoManager.micSelected = sender.selected;
        NSString *index = [ZegoManager getUserMicIndex:[SPDApiUser currentUser].userId];
        if (index) {
            if (sender.selected) {
                [self uploadAdminMessage:@"解除静音" userId:[SPDApiUser currentUser].userId index:index.integerValue status:@"1" isCheck:NO];
            } else {
                [self uploadAdminMessage:@"静音" userId:[SPDApiUser currentUser].userId index:index.integerValue status:@"2" isCheck:NO];
            }
        }
    }
}

//发送消息
-(void)onTouchSendMessageButton:(NSString *)text {
    
    if (![SPDCommonTool isEmpty:text]) {
        [self onTouchSendButton:text];
    }
    
}

-(void)onTouchSendGlobalMessageButton {
    
    self.enableSendGlobal = ! self.enableSendGlobal;
//    if (self.enableSendGlobal) {
//        [self.inputBar.chatSessionInputBarControl.sendGlobalMessageButton setBackgroundImage:[UIImage imageNamed:@"img_world_msg"] forState:UIControlStateNormal];
//        self.inputBar.chatSessionInputBarControl.inputTextView.placeholder=SPDStringWithKey(@"每次发送需要消耗五百金币", nil);
//    }else{
//        [self.inputBar.chatSessionInputBarControl.sendGlobalMessageButton setBackgroundImage:[UIImage imageNamed:@"chatroom_sendmsg"] forState:UIControlStateNormal];
//        self.inputBar.chatSessionInputBarControl.inputTextView.placeholder=SPDStringWithKey(@"点击房间切换到世界喊话", nil);
//
//    }
}

-(void)requestSeverToSendGlobalMessage:(RCMessageContent *)messageContent {
    
    NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
    NSString *error=[userDefault objectForKey:@"errorTime"];
    double  timeInterval=[error doubleValue];
    NSTimeInterval current=[[NSDate date] timeIntervalSince1970];
    
    if (error == nil || current-timeInterval > 90) {
        NSDictionary *dic = @{@"userid":[SPDApiUser currentUser].userId,
                              @"scene_id":self.clan_id
                              };
        [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"chatroom.global.message" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
            
            [self sendGlobalMessage:messageContent pushContent:nil];
            self.notice_userInfo = nil;
            
            //cheak level
            [[SPDCommonTool shareTool] checkExpToWhetherToUpgrage];
            
        } bFailure:^(id  _Nullable failure) {
            [self failureToRequestChatRoomGlobalMsgApiWith:failure];
        }];
        
    }else{
        [self showMessageToast:SPDStringWithKey(@"90s之后再试试哦！", nil)];
    }
    
}

-(void)failureToRequestChatRoomGlobalMsgApiWith:(id)failure {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([failure[@"code"] integerValue] == 202) {
            [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
            } confirmTitle:SPDStringWithKey(@"充值", nil) confirmAction:^{
                [self pushToRechageController];
            }];
        }else {
            [self showToast:failure[@"msg"]];
        }
    });
    
}


#pragma mark send globalmessage

- (void)sendGlobalMessage:(RCMessageContent *)messageContent
              pushContent:(NSString *)pushContent {
    
    messageContent.senderUserInfo = [RCIM sharedRCIM].currentUserInfo;
    if (messageContent == nil) {
        return;
    }
    if (messageContent.senderUserInfo.userId == nil || messageContent.senderUserInfo.name == nil || messageContent.senderUserInfo.portraitUri == nil) {
        //向服务器发送消息
        [SPDCommonTool postiOSMethodToRecoredRCall:[NSString stringWithFormat:@"sendGlobalMessage - userinfo - nil"]];
        [self showToast:@"magic userinfo nil"];
        return;
    }
    
    [[RCIMClient sharedRCIMClient] sendMessage:self.conversationType targetId:GLOBALCHATROOM content:messageContent pushContent:pushContent pushData:nil success:^(long messageId) {
        __weak typeof(&*self) __weakself = self;
        if ([messageContent isKindOfClass:[SPDWorldChatMessage class]]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [__weakself.inputBar clearInputView];
                RCUserInfo *usrInfo = [RCIM sharedRCIM].currentUserInfo;
                SPDWorldChatMessage *worldMsg = (SPDWorldChatMessage *)messageContent;
                NSDictionary * dict ;
                if (worldMsg.type == nil ) {
                    dict = @{
                             @"user_id":usrInfo.userId,
                             @"nickname":usrInfo.name?:@"",
                             @"avatar":usrInfo.portraitUri ?:@"",
                             @"content":worldMsg.content?:@"",
                             @"gender":worldMsg.gender?:@"",
                             @"clan_id":worldMsg.send_ClanId?:@"",
                             @"clan_name":worldMsg.send_ClanName?:@"",
                             @"mentionedType":worldMsg.mentionedType ? : @"",
                             @"mentionedList":worldMsg.mentionedList ? : @[]
                             };
                    __weakself.globalNotifyView.dict = dict;
                    __weakself.globalScrollView.dic = dict;
                    NSLog(@"向服务器发送世界消息验证");
                    [self handleCRWorldMsg];
                    
                }else if([worldMsg.type isEqualToString:@"2"]){
                    dict = @{
                             @"user_id":usrInfo.userId?:@"",
                             @"savatar":usrInfo.portraitUri?:@"",
                             @"sender_nickname":usrInfo.name?:@"",
                             @"sgender":worldMsg.gender?:@"",
                             @"ruser_id":worldMsg.receive_Id?:@"",
                             @"ravatar":worldMsg.receive_avatar?:@"",
                             @"receive_nickname":worldMsg.receive_nickname?:@"",
                             @"rgender":worldMsg.receive_gender?:@"",
                             @"clan_id":worldMsg.send_ClanId?:@"",
                             @"clan_name":worldMsg.send_ClanName?:@"",
                             @"gift_url" :worldMsg.present_url?:@"",
                             @"type" :worldMsg.type?:@"",
                             @"num": worldMsg.num ? : @"1"
                             };
                    __weakself.globalNotifyView.dict = dict;
                    __weakself.globalScrollView.dic = dict;
                    
                }else if ([worldMsg.type isEqualToString:@"3"]){
                    [self showAllWorldSendGiftMessage:worldMsg];
                }
                [SPDRCIMDataSource shareInstance].lastGlobalMessage = worldMsg;
                
            });
        }
        else if ([messageContent isMemberOfClass:[SPDCRSendMagicMessage class]]){
            SPDCRSendMagicMessage *magicMsg = (SPDCRSendMagicMessage *)messageContent;
            RCUserInfo *usrInfo = [RCIM sharedRCIM].currentUserInfo;
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDictionary *  dict = @{
                                         @"user_id":usrInfo.userId?:@"",
                                         @"savatar":usrInfo.portraitUri?:@"",
                                         @"sender_nickname":usrInfo.name?:@"",
                                         @"sgender":magicMsg.send_gender?:@"",
                                         @"ruser_id":magicMsg.receive_id?:@"",
                                         @"ravatar":magicMsg.receive_avatar?:@"",
                                         @"receive_nickname":magicMsg.receive_nickname?:@"",
                                         @"rgender":magicMsg.receive_gender?:@"",
                                         @"clan_id":magicMsg.clan_id ?:@"",
                                         @"clan_name":magicMsg.clan_name?:@"",
                                         @"gift_url" :magicMsg.magic_url?:@"",
                                         @"result":magicMsg.result ? : @"",
                                         @"magic_type":magicMsg.type ? :@"",
                                         @"magic_effect_value": magicMsg.magic_effect_value ?:@"",
                                         @"type" :@"magic",
                                         @"magic_effect":magicMsg.magic_effect
                                         };
               
                [SPDRCIMDataSource shareInstance].lastGlobalMessage = magicMsg;
                //发送世界消息 魔法消息实在世界发送的 接到的人判断是不是在本聊天室 如果是在本聊天室 那么播放动画
                NSLog(@"发送魔法世界消息成功");
                if ([magicMsg.is_cost isEqualToString:@"true"]) {
                    //花费金币
                    __weakself.globalNotifyView.dict = dict;
                    __weakself.globalScrollView.dic = dict;
                }
                __weakself.magicAnimationView.magicMsg = magicMsg;
            });
        }
        
    } error:^(RCErrorCode nErrorCode, long messageId) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SPDCommonTool showWindowToast:[NSString stringWithFormat:@"%ld",nErrorCode]];
        });
    }];
    
}

-(void)showAllWorldSendGiftMessage:(SPDWorldChatMessage *)worldMsg {
    
    if (worldMsg.senderUserInfo && isAlreadyDownloadGiftImagesEffect) {
//        self.effectsView.message = wo
//        __weak typeof(&*self) __blockSelf = self;
//        RCUserInfo *usr = worldMsg.senderUserInfo;
//        dispatch_async(dispatch_get_main_queue(), ^{
//            NSDictionary *dict = @{
//                                   @"sender_name":usr.name,
//                                   @"sender_avatar":usr.portraitUri,
//                                   @"sender_gender":worldMsg.gender,
//                                   @"receive_name":worldMsg.receive_nickname,
//                                   @"receive_avatar":worldMsg.receive_avatar,
//                                   @"receive_gender":worldMsg.receive_gender,
//                                   @"present_url":worldMsg.present_url,
//                                   @"gift_id":worldMsg.gift_id,
//#warning message - 可能还要加一个 position
//                                   @"position":@"big_center",
//                                   @"num": worldMsg.num ? : @"1"
//                                   };
//            __blockSelf.effectsView.dataDict = dict;
//
//        });
        
    }
}


#pragma mark RCInputBarControlDelegate

/**
 *  根据inputBar 回调来修改页面布局，inputBar frame 变化会触发这个方法
 *
 *  @param frame    输入框即将占用的大小
 *  @param duration 时间
 *  @param curve
 */
- (void)onInputBarControlContentSizeChanged:(CGRect)frame withAnimationDuration:(CGFloat)duration andAnimationCurve:(UIViewAnimationCurve)curve{
    
    //根据输入框改变y
    CGRect comeinframe = self.comeInChatRoomView.frame;
    comeinframe.origin.y = frame.origin.y -comeinframe.size.height;
    
    CGRect vehicleFrame = self.vehicleAnimationView.frame;
    vehicleFrame.origin.y = frame.origin.y - vehicleFrame.size.height;
    
    CGRect collectionViewRect = self.contentView.frame;
    collectionViewRect.size.height = kScreenH -collectionOriginalY -frame.size.height+50;
    CGRect collectionRect = self.conversationMessageCollectionView.frame;
    collectionRect.size.height = collectionViewRect.size.height-50;
    
    [UIView animateWithDuration:duration animations:^{
        [UIView setAnimationCurve:curve];
        [self.comeInChatRoomView setFrame:comeinframe];
        [self.vehicleAnimationView setFrame:vehicleFrame];
        [self.contentView setFrame:collectionViewRect];
        [self.conversationMessageCollectionView setFrame:collectionRect];
        [UIView commitAnimations];
    }];
    CGRect inputbarRect = self.inputBar.frame;
    inputbarRect.origin.y = collectionViewRect.size.height-50;
    [self.inputBar setFrame:inputbarRect];
    [self.view bringSubviewToFront:self.inputBar];
    [self scrollToBottomAnimated:NO];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if ([collectionView isEqual:self.micCollectionView]) {
        return 5;
    } else if ([collectionView isEqual:self.onlineUserConllectionView]) {
        return self.sortedAudiences.count;
    } else {
        return self.conversationDataRepository.count;
    }
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([collectionView isEqual:self.micCollectionView]) {
        SPDVoiceLiveOnlineUserCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:SPDVoiceLiveOnlineUserCellForMic forIndexPath:indexPath];
        if (indexPath.row < 3) {
            cell.publicMicImage.hidden = YES;
        } else {
            cell.publicMicImage.hidden = NO;
        }
        NSString *key = [NSString stringWithFormat:@"%ld", indexPath.row];
        cell.micStatus = [ZegoManager.micStatus[key] intValue];
        SPDVoiceLiveUserModel *model = ZegoManager.micUsers[key];
        cell.model = model;
        if (model && cell.micStatus != Mute && cell.micStatus != Forbidden) {
            for (NSString *userId in self.speekers) {
                if ([model._id isEqualToString:userId]) {
                    cell.animating = YES;
                    return cell;
                }
            }
        }
        cell.animating = NO;
        return cell;
    } else if ([collectionView isEqual:self.onlineUserConllectionView]) {
        SPDVoiceLiveOnlineUserCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:SPDVoiceLiveOnlineUserCellIndentifier forIndexPath:indexPath];
        SPDVoiceLiveUserModel *model = self.sortedAudiences[indexPath.row];
        [cell.userIcon sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, model.avatar]]];
        cell.micStatusImage.hidden = YES;
        cell.publicMicImage.hidden = YES;
        cell.model = model;
        cell.animating = NO;
        return cell;
    } else {
        RCDLiveMessageModel *model =
        [self.conversationDataRepository objectAtIndex:indexPath.row];
        RCMessageContent *messageContent = model.content;
        //文本消息
        if ([messageContent isMemberOfClass:[SPDTextMessage class]]  || [messageContent isMemberOfClass:[RCTextMessage class]]) {
            RCDMSChatRoomCell *__cell = [collectionView dequeueReusableCellWithReuseIdentifier:rctextCellIndentifier forIndexPath:indexPath];
            __cell.isFullScreenMode = NO;
            [__cell setDataModel:model];
            [__cell setDelegate:self];
            [__cell setCellDelegate:self];
            return __cell;
        } else if ([messageContent isMemberOfClass:[SPDChatroomRedPacketMessage class]]) {
            ChatroomRedPacketMsgCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatroomRedPacketMsgCell" forIndexPath:indexPath];
            cell.delegate = self;
            cell.message = (SPDChatroomRedPacketMessage *)messageContent;
            return cell;
        } else if ([messageContent isMemberOfClass:[SPDNobleGreetingMessage class]]) {
            NobleGreetingMsgCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NobleGreetingMsgCell" forIndexPath:indexPath];
            cell.delegate = self;
            cell.message = (SPDNobleGreetingMessage *)messageContent;
            return cell;
        } else {
            SPDBannedSpeakCell * __cell =[collectionView dequeueReusableCellWithReuseIdentifier:SPDBannedSpeakCellIndentifier forIndexPath:indexPath];
            [__cell setDataModel:model];
            return __cell;
        }
    }
    return nil;
}

#pragma mark <UICollectionViewDelegateFlowLayout>

/**
 *  cell的大小
 *
 *  @return
 */
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([collectionView isEqual:self.micCollectionView] || [collectionView isEqual:self.onlineUserConllectionView]) {
        return CGSizeMake(45, 45);
    } else {
        RCDLiveMessageModel *model =
        [self.conversationDataRepository objectAtIndex:indexPath.row];
        if (model.cellSize.height > 0) {
            return model.cellSize;
        }
        RCMessageContent *messageContent = model.content;
        if ([messageContent isMemberOfClass:[SPDTextMessage class]] || [messageContent isMemberOfClass:[RCTextMessage class]]) {
            model.cellSize = [self sizeForItem:collectionView atIndexPath:indexPath];
            
        } else if ([messageContent isMemberOfClass:[SPDBannedSpeakMessage class]]){
            return CGSizeMake(kScreenW, 30) ;
        } else if ([messageContent isMemberOfClass:[SPDChatroomRedPacketMessage class]]) {
            return CGSizeMake(kScreenW, 121);
        } else if ([messageContent isMemberOfClass:[SPDNobleGreetingMessage class]]) {
            SPDNobleGreetingMessage *message = (SPDNobleGreetingMessage *)messageContent;
            return CGSizeMake(kScreenW, message.textSize.height + 25 + 15);
        }
        return model.cellSize;
    }
}

/**
 *  计算不同消息的具体尺寸
 *
 *  @return
 */
- (CGSize)sizeForItem:(UICollectionView *)collectionView
          atIndexPath:(NSIndexPath *)indexPath {
    CGFloat __width = CGRectGetWidth(collectionView.frame);
    RCDLiveMessageModel *model =
    [self.conversationDataRepository objectAtIndex:indexPath.row];
    RCMessageContent *messageContent = model.content;
    CGFloat __height = 0.0f;
    NSString *localizedMessage;
    if ([messageContent isMemberOfClass:[RCInformationNotificationMessage class]]) {
        RCInformationNotificationMessage *notification = (RCInformationNotificationMessage *)messageContent;
        localizedMessage = [RCDLiveKitUtility formatMessage:notification];
        CGSize __labelSize = [RCDMSChatRoomCell getMessageCellSize:localizedMessage withWidth:kScreenW - 90];
        __height = __height + __labelSize.height+ 25 +15;
        return CGSizeMake(__width,__height);

    }else if ([messageContent isMemberOfClass:[SPDTextMessage class]]){
        SPDTextMessage *textMeesage = (SPDTextMessage *)messageContent;
        return CGSizeMake(__width, __height + textMeesage.contentHeight + 5);
    }
    return CGSizeZero;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeZero;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([collectionView isEqual:self.micCollectionView] || [collectionView isEqual:self.onlineUserConllectionView]) {
        if (!_position || !_familyMembers.count) {
            if (!_isRequestPosition) {
                [self requestFamilyDetail];
            }
            return;
        }
        [self.inputBar setInputBarStatus:RCDLiveBottomBarDefaultStatus];
        
        SPDVoiceLiveOnlineUserCell *cell = (SPDVoiceLiveOnlineUserCell *)[collectionView cellForItemAtIndexPath:indexPath];
        if ([collectionView isEqual:self.micCollectionView] && !cell.model) {
            NSString *userId = @"n";
            NSInteger index = indexPath.row;
            MicStatus status = [[ZegoManager.micStatus valueForKey:[NSString stringWithFormat:@"%ld", index]] intValue];
            
            if (status == Freeze) {
                return;
            }
            
            if ([self.position isEqualToString:@"tourist"]) {
                if (![ZegoManager getUserMicIndex:[SPDApiUser currentUser].userId]) {
                    if (index < 3 || status == CannotUse) {
                        [self showToast:SPDStringWithKey(@"联系家族成员可上麦", nil)];
                    } else {
                        [self beginUseMic:[SPDApiUser currentUser].userId index:index];
                    }
                }
                return;
            }
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            
            if (status == CannotUse) {
                if (self.positionLevel >= 75) {
                    UIAlertAction *openMic = [UIAlertAction actionWithTitle:SPDStringWithKey(@"解封此麦位", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self openMic:index];
                    }];
                    [alertController addAction:openMic];
                    
                    if (![ZegoManager getUserMicIndex:[SPDApiUser currentUser].userId]) {
                        UIAlertAction *beginUseMic = [UIAlertAction actionWithTitle:SPDStringWithKey(@"上麦", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self beginUseMic:[SPDApiUser currentUser].userId index:index];
                        }];
                        [alertController addAction:beginUseMic];
                    }
                    
                    UIAlertAction *cancel = [UIAlertAction actionWithTitle:SPDStringWithKey(@"取消", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    [alertController addAction:cancel];
                    
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                return;
            }
            
            UIAlertAction *helpTouristUseMic = [UIAlertAction actionWithTitle:SPDStringWithKey(@"帮访客上麦", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self helpTouristUseMic:index];
            }];
            [alertController addAction:helpTouristUseMic];
            
            if (self.positionLevel >= 75) {
                UIAlertAction *closeMic = [UIAlertAction actionWithTitle:SPDStringWithKey(@"封闭此麦位", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self closeMic:userId index:index];
                }];
                [alertController addAction:closeMic];
            }
            
            if (status == Forbidden) {
                UIAlertAction *releaseForbidMic = [UIAlertAction actionWithTitle:SPDStringWithKey(@"解除禁麦", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self releaseForbidMic:userId index:index];
                }];
                [alertController addAction:releaseForbidMic];
            } else {
                UIAlertAction *forbidMic = [UIAlertAction actionWithTitle:SPDStringWithKey(@"禁麦", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self forbidMic:userId index:index];
                }];
                [alertController addAction:forbidMic];
            }
            
            if (![ZegoManager getUserMicIndex:[SPDApiUser currentUser].userId]) {
                UIAlertAction *beginUseMic = [UIAlertAction actionWithTitle:SPDStringWithKey(@"上麦", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self beginUseMic:[SPDApiUser currentUser].userId index:index];
                }];
                [alertController addAction:beginUseMic];
            }
            
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:SPDStringWithKey(@"取消", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            [alertController addAction:cancel];
            
            [self presentViewController:alertController animated:YES completion:nil];
        } else {
            NSString *userId = cell.model._id;
            NSInteger adminValue;
            if (cell.model.position) {
                adminValue = self.positionLevel - cell.model.positionLevel;
            } else {
                adminValue = self.positionLevel - [self getUserPosition:userId];
            }
            [self selectAudience:userId adminValue:adminValue];
        }
    }
}

#pragma mark - ChatroomRedPacketMsgCellDelegate

- (void)didClickRedPacketWithMessage:(SPDChatroomRedPacketMessage *)message {
    [self requestChatroomRedPacketInfoWithMsg:message];
}

#pragma mark - SPDVoiceLiveAudienceViewDelegate

- (void)didSelectAudience:(NSString *)userId index:(NSInteger)index {
    [self beginUseMic:userId index:index];
}

#pragma mark - SPDChatRoomUserInfoViewDelegate

- (void)didClickLookUserInfoBtn:(NSString *)userId {
    [self lookUserInfo:userId];
}

- (void)didSelectSend:(NSString *)type userId:(NSString *)userId avater:(NSString *)avater nickName:(NSString *)nickName gender:(NSString *)gender {
    self.sendPresent_nickname = nickName;
    self.sendPresent_avatar = [NSString stringWithFormat:STATIC_DOMAIN_URL, avater];
    self.sendPresent_gender = gender;
    self.sendPresent_userId = userId;
    [self sendWithType:type userId:userId];
}

- (void)didSelectAdmin:(NSString *)admin userId:(NSString *)userId index:(NSInteger)index nickName:(NSString *)nickName {
    if ([admin isEqualToString:SPDStringWithKey(@"上麦", nil)] || [admin isEqualToString:SPDStringWithKey(@"帮他上麦", nil)]) {
        if (_audiences[userId]) {
            [self checkIsHaveUseableMicWithStart:0 userId:userId];
        } else {
            [self showToast:SPDStringWithKey(@"该用户已离开直播间", nil)];
        }
    } else if ([admin isEqualToString:SPDStringWithKey(@"下麦旁听", nil)] || [admin isEqualToString:SPDStringWithKey(@"设为旁听", nil)]) {
        [self stopUseMic:userId index:index isCheck:NO];
    } else if ([admin isEqualToString:SPDStringWithKey(@"禁言", nil)]) {
        [self shutup:userId];
    } else if ([admin isEqualToString:SPDStringWithKey(@"解除禁言", nil)]) {
        [self releaseShutup:userId];
    } else if ([admin isEqualToString:SPDStringWithKey(@"禁麦", nil)]) {
        [self forbidMic:userId index:index];
    } else if ([admin isEqualToString:SPDStringWithKey(@"解除禁麦", nil)]) {
        [self releaseForbidMic:userId index:index];
    } else if ([admin isEqualToString:SPDStringWithKey(@"封麦", nil)]) {
        [self closeMic:userId index:index];
    } else if ([admin isEqualToString:SPDStringWithKey(@"踢出房间", nil)]) {
        [self kickOutRoom:userId nickName:nickName];
    }
}

#pragma mark - SPDFamilyInfoControllerDelegate

- (void)isFollowingDidChange:(BOOL)isFollowing {
    if (isFollowing) {
        self.navigationItem.rightBarButtonItems = [self.navigationItem.rightBarButtonItems subarrayWithRange:NSMakeRange(0, 1)];
    } else {
        UIButton *followBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 34, 30)];
        [followBtn setImage:[UIImage imageNamed:@"chatroom_follow_btn"] forState:UIControlStateNormal];
        [followBtn addTarget:self action:@selector(requestClanFollow) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:followBtn];
        self.navigationItem.rightBarButtonItems = @[self.navigationItem.rightBarButtonItems.firstObject, rightBarButtonItem];
    }
}

#pragma mark - Browser

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser*)photoBrowser{
    return self.albumArray.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser*)photoBrowser photoAtIndex:(NSUInteger)index{
    NSString *imageUrl = [NSString stringWithFormat:STATIC_DOMAIN_URL, self.albumArray[index][@"res_id"]];
    MWPhoto *photo = [MWPhoto photoWithURL:[NSURL URLWithString:imageUrl]];
    return photo;
}

#pragma mark - SDCycleScrollViewDelegate

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    switch (index) {
        case 0:
            [self openLottery];
            break;
        case 1: {
            NSString *url = [NSString stringWithFormat:@"https://%@share.famy.ly/view/andriodlotter.html?user_id=%@&lang=%@", DEV ? @"dev-" : @"", [SPDApiUser currentUser].userId, [SPDApiUser currentUser].lang];
            [self loadWebViewController:self title:@"" url:url];
            break;
        }
        default:
            break;
    }
}

#pragma mark - 权限相关

// 用户信息弹窗
- (void)selectAudience:(NSString *)userId adminValue:(NSInteger)adminValue {
    SPDChatRoomUserInfoView *userInfoView = [SPDChatRoomUserInfoView chatRoomUserInfoView];
    userInfoView.adminValue = adminValue;
    userInfoView.positionLevel = _positionLevel;
    NSString *micIndex = [ZegoManager getUserMicIndex:userId];
    if (micIndex) {
        userInfoView.micIndex = micIndex.integerValue;
        userInfoView.micStatus = [ZegoManager.micStatus[micIndex] integerValue];
    } else {
        userInfoView.micIndex = -1;
        userInfoView.micStatus = -1;
    }
    if (_onlineUsers[userId]) {
        userInfoView.isInChatRoom = YES;
    } else {
        userInfoView.isInChatRoom = NO;
    }
    userInfoView.userId = userId;
    userInfoView.delegate = self;
    userInfoView.clan_id = self.clan_id;
    [self.inputBar setInputBarStatus:RCDLiveBottomBarDefaultStatus];
    [self.view addSubview:userInfoView];
}

// 公共麦
- (void)usePublicMic {
    if ([ZegoManager getUserMicIndex:[SPDApiUser currentUser].userId]) {
        [self showToast:SPDStringWithKey(@"您已经在麦上了", nil)];
    } else {
        if (self.positionLevel) {
            [self checkIsHaveUseableMicWithStart:0 userId:[SPDApiUser currentUser].userId];
        } else {
            [self checkIsHaveUseableMicWithStart:3 userId:[SPDApiUser currentUser].userId];
        }
    }
}

// 检查是否有可用麦位
- (void)checkIsHaveUseableMicWithStart:(NSInteger)start userId:(NSString *)userId {
    for (NSInteger index = start; index < 5; index++) {
        NSString * micIndex = [NSString stringWithFormat:@"%ld", index];
        if (!ZegoManager.micUsers[micIndex] && [ZegoManager.micStatus[micIndex] integerValue] != CannotUse && [ZegoManager.micStatus[micIndex] integerValue] != Freeze) {
            [self beginUseMic:userId index:index];
            return;
        }
    }
    if (start) {
        [self showToast:SPDStringWithKey(@"无可用公共麦位", nil)];
    } else {
        [self showToast:SPDStringWithKey(@"无可用麦位", nil)];
    }
}

// 帮助访客上麦
- (void)helpTouristUseMic:(NSInteger)index {
    SPDVoiceLiveAudienceView *view = [[[NSBundle mainBundle] loadNibNamed:@"SPDVoiceLiveAudienceView" owner:self options:nil] firstObject];
    view.delegate = self;
    view.dataArray = self.tourists.allValues;
    view.index = index;
    view.frame = CGRectMake(0, 0, kScreenW, kScreenH);
    [self.view addSubview:view];
}

// 封麦
- (void)closeMic:(NSString *)userId index:(NSInteger)index {
    [self uploadAdminMessage:@"封麦" userId:userId index:index status:@"4" isCheck:NO];
}

// 解除封麦
- (void)openMic:(NSInteger)index {
    [self uploadAdminMessage:@"解除封麦" userId:@"n" index:index status:@"0" isCheck:NO];
}

// 禁麦
- (void)forbidMic:(NSString *)userId index:(NSInteger)index {
    [self uploadAdminMessage:@"禁麦" userId:userId index:index status:@"3" isCheck:NO];
}

// 解除禁麦
- (void)releaseForbidMic:(NSString *)userId index:(NSInteger)index {
    NSString *status;
    if ([userId isEqualToString:@"n"]) {
        status = @"0";
    } else {
        status = @"1";
    }
    [self uploadAdminMessage:@"解除禁麦" userId:userId index:index status:status isCheck:NO];
}

// 上麦
- (void)beginUseMic:(NSString *)userId index:(NSInteger)index {
    if ([userId isEqualToString:[SPDApiUser currentUser].userId]) {
        NSString *status;
        if ([[ZegoManager.micStatus valueForKey:[NSString stringWithFormat:@"%ld", index]] intValue] != Forbidden) {
            status = @"1";
        } else {
            status = @"3";
        }
        [self uploadAdminMessage:@"上麦" userId:userId index:index status:status isCheck:NO];
    } else {
        [self sendAdminMessage:@"帮访客上麦" userId:userId index:index];
    }
}

// 下麦
- (void)stopUseMic:(NSString *)userId index:(NSInteger)index isCheck:(BOOL)isCheck {
    NSString *status;
    if ([[ZegoManager.micStatus valueForKey:[NSString stringWithFormat:@"%ld", index]] intValue] != Forbidden) {
        status = @"0";
    } else {
        status = @"3";
    }
    [self uploadAdminMessage:@"下麦" userId:userId index:index status:status isCheck:isCheck];
}

// 禁言
- (void)shutup:(NSString *)userId {
    [self sendRequestWithUrl:@"chatroom.banned" andUserId:userId];
}

// 解除禁言
- (void)releaseShutup:(NSString *)userId {
    [self sendRequestWithUrl:@"chatroom.banned.remove" andUserId:userId];
}

// 踢出房间
- (void)kickOutRoom:(NSString *)userId nickName:(NSString *)nickName {
    [self kickOutRoomRequest:userId nickName:nickName];
}

// 看资料
- (void)lookUserInfo:(NSString *)userId {
    [self pushToDetailTableViewController:self userId:userId];
}

// 送礼物、座驾、魔法弹窗
- (void)sendWithType:(NSString *)type userId:(NSString *)userId {
     
    NSUserDefaults *userDefault= [NSUserDefaults standardUserDefaults];
    NSString *error = [userDefault objectForKey:@"errorTime"];
    if (error == nil) {
        [self showPresentViewWithTargetId:userId type:type];
    }else{
        double  timeInterval=[error doubleValue];
        NSTimeInterval current=[[NSDate date] timeIntervalSince1970];
        if (current-timeInterval<=90) {
            [self showToast:SPDStringWithKey(@"90s之后再试试哦！", nil)];
        }else if (current-timeInterval>90){
            [self showPresentViewWithTargetId:userId type:type];
        }
    }
    
}

#pragma mark - 礼物座驾魔法 回调

// 成功的给某人送了一个礼物 现在通知全聊天室的人
-(void)handleSendUserPresentSuccess:(NSNotification *)notify {
    
    if ([notify.object isKindOfClass:[GiftListModel class]]) {
        
        GiftListModel * giftModel = notify.object;
        NSString * receive_id = notify.userInfo[@"user_id"]; //user_id 接收礼物的id
        if (!receive_id) {return;}
        [self handleCRSendGiftMessage:giftModel WithUserId:receive_id];

        if (giftModel.send_all_chatroom) {
            //发送世界消息
            NSString * messageTxt = [NSString stringWithFormat:SPDStringWithKey(@"我送给%@一个%@", nil),self.sendPresent_nickname,giftModel.name];
            SPDWorldChatMessage *rcWordMessage = [SPDWorldChatMessage SPDWorldChatMessageWithContent:messageTxt andsend_ClanId:self.clan_id andsend_ClanName:self.clan_name andGender:[SPDApiUser currentUser].gender];
            rcWordMessage.receive_avatar = self.sendPresent_avatar;
            rcWordMessage.receive_nickname = self.sendPresent_nickname;
            rcWordMessage.receive_gender = self.sendPresent_gender;
//            rcWordMessage.receive_Id = self.sendPresent_userId;
            rcWordMessage.receive_Id = receive_id;
            rcWordMessage.present_url = [NSString stringWithFormat:STATIC_DOMAIN_URL,giftModel.cover];
            rcWordMessage.gift_id = giftModel._id;
            rcWordMessage.type = @"3";
            if (giftModel.send_msg_world) {
                rcWordMessage.send_msg_world = @"true";
            }else{
                rcWordMessage.send_msg_world = @"false";
            }
            rcWordMessage.num = giftModel.num;
            
            [self sendGlobalMessage:rcWordMessage pushContent:@"hhhh" ];
            
            if (giftModel.send_msg_world) {
                NSString * messageTxt = [NSString stringWithFormat:SPDStringWithKey(@"我送给%@一个%@", nil),self.sendPresent_nickname,giftModel.name];
                SPDWorldChatMessage *rcWordMessage = [SPDWorldChatMessage SPDWorldChatMessageWithContent:messageTxt andsend_ClanId:self.clan_id andsend_ClanName:self.clan_name andGender:[SPDApiUser currentUser].gender];
                rcWordMessage.receive_avatar = self.sendPresent_avatar;
                rcWordMessage.receive_nickname = self.sendPresent_nickname;
                rcWordMessage.receive_gender = self.sendPresent_gender;
//                rcWordMessage.receive_Id = self.sendPresent_userId;
                rcWordMessage.receive_Id = receive_id;
                rcWordMessage.present_url = [NSString stringWithFormat:STATIC_DOMAIN_URL,giftModel.cover];
                rcWordMessage.gift_id = giftModel._id;
                rcWordMessage.type = @"2";
                if (giftModel.send_msg_world) {
                    rcWordMessage.send_msg_world = @"true";
                }else{
                    rcWordMessage.send_msg_world = @"false";
                }
                rcWordMessage.num = giftModel.num;
                [self sendGlobalMessage:rcWordMessage pushContent:@"hhhh"];
                
            }
            
        }else{
            NSString * cover = [NSString stringWithFormat:STATIC_DOMAIN_URL,giftModel.cover];
            SPDChatRoomSendPresentMessage * msg = [SPDChatRoomSendPresentMessage messageWithAvatar:self.sendPresent_avatar andNickname:self.sendPresent_nickname andGender:self.sendPresent_gender andPresent:cover andSenderGender:[SPDApiUser currentUser].gender andPosition:giftModel.position] ;
            msg.gift_id = giftModel._id;
            if (giftModel.send_msg_world) {
                msg.send_msg_world = @"true";
            }else{
                msg.send_msg_world = @"false";
            }
            if (giftModel.send_all_chatroom) {
                msg.send_all_chatroom = @"true";
            }else{
                msg.send_all_chatroom = @"false";
            }
            msg.senderUserInfo = [RCIM sharedRCIM].currentUserInfo;
            msg.num = giftModel.num;
            [[RCIMClient sharedRCIMClient] sendMessage:self.conversationType targetId:self.targetId content:msg pushContent:nil pushData:nil success:^(long messageId){
                NSLog(@"--聊天室送礼物成功消息成功");
                if ([giftModel.position isEqualToString:@"scroll"]) {

                    [self showScrollGiftImage:msg];

                }else if ([giftModel.position isEqualToString:@"big_center"] || [msg.position isEqualToString:@"small_center"]){
                    if (isAlreadyDownloadGiftImagesEffect) {
                        [self showCenterImageEffect:msg];
                        if (giftModel.send_msg_world && !giftModel.send_all_chatroom) {
                            //发送世界消息
                            NSString * messageTxt = [NSString stringWithFormat:SPDStringWithKey(@"我送给%@一个%@", nil),self.sendPresent_nickname,giftModel.name];
                            SPDWorldChatMessage *rcWordMessage = [SPDWorldChatMessage SPDWorldChatMessageWithContent:messageTxt andsend_ClanId:self.clan_id andsend_ClanName:self.clan_name andGender:[SPDApiUser currentUser].gender];
                            rcWordMessage.receive_avatar = self.sendPresent_avatar;
                            rcWordMessage.receive_nickname = self.sendPresent_nickname;
                            rcWordMessage.receive_gender = self.sendPresent_gender;
                            rcWordMessage.receive_Id = self.sendPresent_userId;
                            rcWordMessage.present_url = [NSString stringWithFormat:STATIC_DOMAIN_URL,giftModel.cover];
                            rcWordMessage.gift_id = giftModel._id;
                            rcWordMessage.type = @"2";
                            if (giftModel.send_msg_world) {
                                rcWordMessage.send_msg_world = @"true";
                            }else{
                                rcWordMessage.send_msg_world = @"false";
                            }
                            rcWordMessage.num = giftModel.num;
                            [self sendGlobalMessage:rcWordMessage pushContent:@"hhhh"];

                        }
                    }
                }

            } error:nil];
        }
        
    }else if ([notify.object isKindOfClass:[SPDVehicleModel class]]) {
        NSLog(@"在聊天室送了一个座驾");
        SPDVehicleModel * vehicle_model = notify.object;
        [self handleCRSendVehicleMessage:vehicle_model];
        NSString * messageTxt = [NSString stringWithFormat:SPDStringWithKey(@"我送给%@一个%@", nil),self.sendPresent_nickname,vehicle_model.name];
        SPDWorldChatMessage *rcWordMessage = [SPDWorldChatMessage SPDWorldChatMessageWithContent:messageTxt andsend_ClanId:self.clan_id andsend_ClanName:self.clan_name andGender:[SPDApiUser currentUser].gender];
        rcWordMessage.receive_avatar = self.sendPresent_avatar;
        rcWordMessage.receive_nickname = self.sendPresent_nickname;
        rcWordMessage.receive_gender = self.sendPresent_gender;
        rcWordMessage.receive_Id = self.sendPresent_userId;
        rcWordMessage.present_url = [NSString stringWithFormat:STATIC_DOMAIN_URL,vehicle_model.image];
        rcWordMessage.gift_id = vehicle_model.vehicle_id;
        rcWordMessage.type = @"2";
        rcWordMessage.send_msg_world = @"false";
        [self sendGlobalMessage:rcWordMessage pushContent:@"hhhh"];
        
    }else if ([notify.object isKindOfClass:[SPDMagicModel class]]) {
        SPDMagicModel * model = notify.object;
        NSDictionary * msgDict = notify.userInfo; //传递的结果的成功还是失败 以及 魔法产生的效果 比如说 魅力值加123人
        NSLog(@"我在聊天室送了一个%@",model.name);
        if (isAlreadyDownloadMagicImagesEffect) {
            NSLog(@"图片已经下载完成");
            [self generateSendMagicMessageWithModel:model andNSDict:msgDict];
        }
    }
    
}

- (void)generateSendMagicMessageWithModel:(SPDMagicModel *)model andNSDict:(NSDictionary *)dict {
        SPDCRSendMagicMessage * message = [[SPDCRSendMagicMessage alloc]init];
        message.senderUserInfo = [RCIM sharedRCIM].currentUserInfo;
        message.receive_id =self.sendPresent_userId;
        message.receive_avatar = self.sendPresent_avatar;
        message.receive_gender = self.sendPresent_gender;
        message.receive_nickname = self.sendPresent_nickname;
        message.send_gender = [SPDApiUser currentUser].gender;
        message.magic_id = model._id;
        message.magic_url = [NSString stringWithFormat:STATIC_DOMAIN_URL,model.cover];
        message.magic_effect = dict[@"effect"] ? : @"des";
        message.result = dict[@"result"] ? : @"fail";
        message.type = dict[@"type"] ? : @"charm";
        message.magic_effect_value = dict[@"value"] ? : @"";
        message.clan_id = self.clan_id;
        message.clan_name = self.clan_name;
        message.is_cost = [dict[@"is_cost"] boolValue] ? @"true":@"false";
        //在发送一条世界消息
        [self sendGlobalMessage:message pushContent:@""];
    
}


#pragma mark 要给服务器做统计消息处理

- (void)handleCRSendGiftMessage:(GiftListModel* )giftmodel WithUserId:(NSString* )user_id{
    
    if (![SPDApiUser currentUser].userId || !user_id) {
        [self showToast:@"handleCRSendGiftMessage sendPresent_userId null"];
        return;
    }
    CRSendGiftMessage * msg = [CRSendGiftMessage cRSendGiftMessageWithfromUserId:[SPDApiUser currentUser].userId toUserId:user_id ClanId:self.clan_id and_id:giftmodel._id andNum:[NSString stringWithFormat:@"%@",giftmodel.cost]];
    [self  sendMessage:msg pushContent:@""];
    
}

- (void)handleCRSendVehicleMessage:(SPDVehicleModel *)model {
    CRSendVehicleMessage * msg = [CRSendVehicleMessage cRSendVehicleWithfromUserId:[SPDApiUser currentUser].userId toUserId:self.sendPresent_userId ClanId:self.clan_id and_id:model.vehicle_id andNum:[NSString stringWithFormat:@"%@",model.price]];
    [self  sendMessage:msg pushContent:@""];
}

- (void)handleCRWorldMsg{
    
    CRWorldMsgMessage * msg = [CRWorldMsgMessage cRWorldMsgMessageWithUserId:[SPDApiUser currentUser].userId andClan_id:self.clan_id];
    [self sendMessage:msg pushContent:@""];
    
}

-(void)showScrollGiftImage:(SPDChatRoomSendPresentMessage *)msg {
    if (msg.senderUserInfo && msg.send_gender) {
        __weak typeof(&*self) __blockSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDictionary *dict = @{
                                   @"sender_name":msg.senderUserInfo.name ?:@"",
                                   @"sender_avatar":msg.senderUserInfo.portraitUri?:@"",
                                   @"sender_gender":msg.send_gender?:@"",
                                   @"receive_name":msg.receive_nickname?:@"",
                                   @"receive_avatar":msg.receive_avatar?:@"",
                                   @"receive_gender":msg.receive_gender?:@"",
                                   @"present_url":msg.present_url?:@"",
                                   @"num": msg.num ? : @"1"
                                   };
            
            __blockSelf.privatePresentView.dict = dict;
        });
    }
}

-(void)showCenterImageEffect:(SPDChatRoomSendPresentMessage *)message {
    
    if (message.senderUserInfo && message.send_gender && isAlreadyDownloadGiftImagesEffect) {
        self.effectsView.message = message;
//
//        __weak typeof(&*self) __blockSelf = self;
//        RCUserInfo *usr = message.senderUserInfo;
//        dispatch_async(dispatch_get_main_queue(), ^{
//            NSDictionary *dict = @{
//                                   @"sender_name":usr.name?:@"",
//                                   @"sender_avatar":usr.portraitUri?:@"",
//                                   @"sender_gender":message.send_gender?:@"",
//                                   @"receive_name":message.receive_nickname?:@"",
//                                   @"receive_avatar":message.receive_avatar?:@"",
//                                   @"receive_gender":message.receive_gender?:@"",
//                                   @"present_url":message.present_url?:@"",
//                                   @"gift_id":message.gift_id,
//                                   @"position":message.position?:@"",
//                                   @"num": message.num ? : @"1"
//                                   };
//            __blockSelf.effectsView.dataDict = dict;
//
//        });
//
    }
}


- (void)sendRequestWithUrl:(NSString *)url andUserId:(NSString *)userId {
    
    NSDictionary *dict = @{@"userId":userId,
                           @"chatroomId":self.clan_id};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dict] bURL:url bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        NSString *messageText ;
        NSString * minute;
        NSString *nickname;
        nickname = suceess[@"nickname"];
        
        if (nickname.length>7) {
            nickname = [nickname substringToIndex:6];
        }
        NSString *mynickname = [SPDApiUser currentUser].nickName;
        if (mynickname.length >7) {
            mynickname = [mynickname substringToIndex:6];
        }
        NSDictionary *dict = @{
                               @"userId":userId,
                               @"nickname":nickname
                               };
        double delayInterval = [suceess[@"minute"] doubleValue] * 60;
        if ([url isEqualToString:@"chatroom.banned"]) {
            minute  = [suceess[@"minute"] stringValue];
            messageText= [NSString stringWithFormat:SPDStringWithKey(@"管理员把%@禁言%@分钟", nil), nickname, minute];
            [self performSelector:@selector(delayAutoUnBanned:) withObject:dict afterDelay:delayInterval];
        }else if([url isEqualToString:@"chatroom.banned.remove"]){
            messageText= [NSString stringWithFormat:SPDStringWithKey(@"%@给%@解除禁言", nil),mynickname,nickname];
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(delayAutoUnBanned:) object:dict];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            SPDBannedSpeakMessage *message = [SPDBannedSpeakMessage bannedSpeakMessageWith:messageText];
            [self sendMessage:message pushContent:nil];
        });
        
    } bFailure:^(id  _Nullable failure) {
        if ([failure isKindOfClass:[NSDictionary class]]) {
            [self showToast:failure[@"msg"]];
        }
    }];
}

- (void)delayAutoUnBanned:(NSDictionary *)dict {
    
    NSString *messageText = [NSString stringWithFormat:SPDStringWithKey(@"禁言时间到，%@自动解除禁言", nil),dict[@"nickname"]];
    dispatch_async(dispatch_get_main_queue(), ^{
        SPDBannedSpeakMessage *message = [SPDBannedSpeakMessage bannedSpeakMessageWith:messageText];
        [self sendMessage:message pushContent:nil];
    });
}

- (void)uploadAdminMessage:(NSString *)type userId:(NSString *)userId index:(NSInteger)index status:(NSString *)status isCheck:(BOOL)isCheck {
    NSMutableDictionary *dic = [@{@"chatroom_id": _clan_id, @"index": [NSString stringWithFormat:@"%ld", index], @"change": status, @"is_check": @(isCheck)} mutableCopy];
    if (![userId isEqualToString:@"n"] && ![type isEqualToString:@"下麦"] && ![type isEqualToString:@"封麦"]) {
        [dic setValue:userId forKey:@"user_id"];
    }
    [RequestUtils commonPostRequestUtils:dic bURL:@"chatroom.change" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        [self requestMicStatus];
        [self sendAdminMessage:type userId:userId index:index];
    } bFailure:^(id  _Nullable failure) {
//        [self showToast:SPDStringWithKey(@"操作失败，请稍后重试", nil)];
        if ([failure isKindOfClass:[NSDictionary class]]) {
            [self showToast:failure[@"msg"]];
        }
    }];
}

- (void)kickOutRoomRequest:(NSString *)userId nickName:(NSString *)nickName {
    NSDictionary *dic = @{@"chatroom_id": _clan_id, @"user_id": userId, @"time": [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]]};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"chatroom.kickout" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        [self sendAdminMessage:@"踢人" userId:userId index:-1];
        if (self.isEnableZegoKick) {
            [self performSelector:@selector(checkKickOut:) withObject:userId afterDelay:3 inModes:@[NSRunLoopCommonModes]];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *msgText= [NSString stringWithFormat:SPDStringWithKey(@"管理员把%@踢出房间", nil), nickName];
            SPDBannedSpeakMessage *msg = [SPDBannedSpeakMessage bannedSpeakMessageWith:msgText];
            [self sendMessage:msg pushContent:nil];
        });
    } bFailure:^(id  _Nullable failure) {
        if ([failure isKindOfClass:[NSDictionary class]]) {
            [self showToast:failure[@"msg"]];
        }
    }];
}

- (void)checkKickOut:(NSString *)userId {
    NSString *index = [ZegoManager getUserMicIndex:userId];
    if (index) {
        [self stopUseMic:userId index:index.integerValue isCheck:NO];
    }
    if (self.onlineUsers[userId]) {
        [ZegoManager kickOutUser:userId];
    }
}

- (void)sendAdminMessage:(NSString *)type userId:(NSString *)userId index:(NSInteger)index {
    NSString *msg = [NSString stringWithFormat:@"%@,%ld,%@", type, index, userId];
    [self.api sendRoomMessage:msg type:ZEGO_TEXT category:ZEGO_SYSTEM completion:^(int errorCode, NSString *roomId, unsigned long long messageId) {
        if (errorCode != 0) {
            [self showToast:SPDStringWithKey(@"操作失败，请稍后重试", nil)];
        }
    }];
}

- (SPDVoiceLiveOnlineUserCell *)getCellWithIndex:(NSInteger)index {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    SPDVoiceLiveOnlineUserCell *cell = (SPDVoiceLiveOnlineUserCell *)[_micCollectionView cellForItemAtIndexPath:indexPath];
    return cell;
}

- (void)enableMic:(BOOL)bEnable {
    [ZegoManager enableMic:bEnable];
    self.micBtn.enabled = bEnable;
    self.micBtn.selected = bEnable;
}

- (NSInteger)getUserPosition:(NSString *)userId {
    SPDVoiceLiveUserModel *model = self.onlineUsers[userId];
    for (SPDFamilyMemberModel *fModel in self.familyMembers) {
        if ([userId isEqualToString:fModel.user_id]) {
            if (model) {
                model.position = fModel.position;
            }
            return [self getPositionLevel:fModel.position];
        }
    }
    if (model) {
        model.position = @"tourist";
    }
    return 0;
}

- (NSInteger)getPositionLevel:(NSString *)position {
    NSInteger positionLevel;
    if ([position isEqualToString:@"member"]) {
        positionLevel = 25;
    } else if ([position isEqualToString:@"deputychief"]) {
        positionLevel = 75;
    } else if ([position isEqualToString:@"chief"]) {
        positionLevel = 150;
    } else if ([position isEqualToString:@"superadmin"]) {
        positionLevel = 250;
    } else {
        positionLevel = 0;
    }
    return positionLevel;
}

#pragma mark - RCMessage
/*!
 发送消息(除图片消息外的所有消息)
 
 @param messageContent 消息的内容
 @param pushContent    接收方离线时需要显示的远程推送内容
 
 @discussion 当接收方离线并允许远程推送时，会收到远程推送。
 远程推送中包含两部分内容，一是pushContent，用于显示；二是pushData，用于携带不显示的数据。
 
 SDK内置的消息类型，如果您将pushContent置为nil，会使用默认的推送格式进行远程推送。
 自定义类型的消息，需要您自己设置pushContent来定义推送内容，否则将不会进行远程推送。
 
 如果您需要设置发送的pushData，可以使用RCIM的发送消息接口。
 */
- (void)sendMessage:(RCMessageContent *)messageContent
        pushContent:(NSString *)pushContent {
    if (_targetId == nil) {
        NSLog(@"sendMessage target nil");
        return;
    }
    messageContent.senderUserInfo = [RCIM sharedRCIM].currentUserInfo;
    if (messageContent == nil || messageContent.senderUserInfo == nil) {
        return;
    }
    [[RCIMClient sharedRCIMClient] sendMessage:self.conversationType targetId:self.targetId content:messageContent pushContent:pushContent pushData:nil success:^(long messageId) {
        __weak typeof(&*self) __weakself = self;
        
        if ([messageContent isMemberOfClass:[SPDTextMessage class]]|| [messageContent isMemberOfClass:[SPDBannedSpeakMessage class]]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                RCMessage *message = [[RCMessage alloc] initWithType:__weakself.conversationType
                                                            targetId:__weakself.targetId
                                                           direction:MessageDirection_SEND
                                                           messageId:messageId
                                                             content:messageContent];
                if ([message.content isMemberOfClass:[RCDLiveGiftMessage class]] ) {
                    message.messageId = -1;//插入消息时如果id是-1不判断是否存在
                }
                [__weakself appendAndDisplayMessage:message];
                [__weakself.inputBar clearInputView];
            });
            
        }else if ([messageContent isMemberOfClass:[SPDJoinChatRoomMessage class]]){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self handleUserComeInChatRoomWithUserInfo:messageContent.senderUserInfo andMessage:messageContent];
            });
            
        }
        else if ([messageContent isMemberOfClass:[SPDCRSendMagicMessage class]]) {
            
        }
        
    } error:^(RCErrorCode nErrorCode, long messageId) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (nErrorCode  == 23408) {
                [self showMessageToast:SPDStringWithKey(@"您已经被禁言了哦!", nil)];
            }else if(nErrorCode == 23406){
                [self rejoinChatRoom];
                [self showMessageToast:@"Not In CHATROOM"];
                //                [self joinInChatRoom];
            }
        });
        
    }];
    
}

-(void)rejoinChatRoom {
    //一个小时之后断线重新连接
    [[RCIMClient sharedRCIMClient] joinChatRoom:self.clan_id messageCount:CHATROOM_MESSAGECOUNT success:^{
        
        [[RCIMClient sharedRCIMClient] joinChatRoom:GLOBALCHATROOM messageCount:-1 success:^{
            
        } error:^(RCErrorCode status) {
            
        }];
        
    } error:^(RCErrorCode status) {
        
        if (status == KICKED_FROM_CHATROOM) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showToast:SPDStringWithKey(@"你已被踢出房间，暂时无法进入此房间", nil)];
            });
        }
        
    }];
    
}

#pragma mark - 发送融云自定义进入聊天室消息

- (void)sendJoinChatRoomMessage {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.vehicle.list" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        for (NSDictionary *dic in suceess[@"list"]) {
            if ([dic[@"is_drive"] boolValue]) {
                SPDJoinChatRoomMessage *joinMessage = [SPDJoinChatRoomMessage joinChatRoomMessageWithContent:@"hi" andGender:[SPDApiUser currentUser].gender andVehicle:@"" andVehicle_id:dic[@"vehicle_id"] andGrade:dic[@"grade"] andImage:dic[@"image"]];
                joinMessage.senderUserInfo = [RCIM sharedRCIM].currentUserInfo;
                joinMessage.noble = self.noble ? : @"";
                [self sendMessage:joinMessage pushContent:@""];
                return;
            }
        }
        [self sendNoVehicleMessage];
    } bFailure:^(id  _Nullable failure) {
        [self sendNoVehicleMessage];
    }];
}

- (void)sendNoVehicleMessage {
    SPDJoinChatRoomMessage *joinMessage = [SPDJoinChatRoomMessage joinChatRoomMessageWithContent:@"hi" andGender:[SPDApiUser currentUser].gender andVehicle:@"" andVehicle_id:@"" andGrade:@"" andImage:@""];
    joinMessage.senderUserInfo = [RCIM sharedRCIM].currentUserInfo;
    joinMessage.noble = self.noble ? : @"";
    [self sendMessage:joinMessage pushContent:@""];
}

#pragma mark - 接受到聊天室消息的通知

- (void)handleChatRoomReceiveMessageNotification:(NSNotification *) notification {
    
    __block RCMessage *rcMessage = notification.object;
    RCDLiveMessageModel *model = [[RCDLiveMessageModel alloc] initWithMessage:rcMessage];
    NSDictionary *leftDic = notification.userInfo;
    if (leftDic && [leftDic[@"left"] isEqual:@(0)]) {
        self.isNeedScrollToButtom = YES;
    }
    if (model.conversationType == model.conversationType &&[model.targetId isEqual:self.targetId]){
        if ([rcMessage.content isKindOfClass:[SPDJoinChatRoomMessage class]]) {
            dispatch_async(dispatch_get_main_queue(), ^{
//                if (rcMessage.sentTime - self.joinRoomInterval > 0) {
                if ([[NSDate date] timeIntervalSince1970] * 1000 - self.joinRoomInterval > 2500.0) {
                    [self handleUserComeInChatRoomWithUserInfo:rcMessage.content.senderUserInfo andMessage:rcMessage.content];
                }
            });
        }else if ([rcMessage.content isKindOfClass:[SPDChatRoomSendPresentMessage class]]){
            NSLog(@"在聊天室qq 送给 cc 一个私聊礼物哦！");
            SPDChatRoomSendPresentMessage *msg = (SPDChatRoomSendPresentMessage *)rcMessage.content;
            NSTimeInterval  current = [[NSDate date] timeIntervalSince1970] *1000;
            if (rcMessage.content.senderUserInfo && current- self.joinRoomInterval >2000.0) {
                //判断msg 的高低版本
                if (msg.gift_id != nil) {
                    if ([msg.position isEqualToString:@"big_center"]|| [msg.position isEqualToString:@"small_center"]) {
                        [self showCenterImageEffect:msg];
                    }else{
                        [self showScrollGiftImage:msg];
                    }
                }else{
                    [self showScrollGiftImage:msg];
                }
            }
        }else if ([rcMessage.content isKindOfClass:[SPDCRSendMagicMessage class]]){
            SPDCRSendMagicMessage * msg = (SPDCRSendMagicMessage *)rcMessage.content;
            NSTimeInterval  current = [[NSDate date] timeIntervalSince1970] *1000;
            if (rcMessage.content.senderUserInfo && current- self.joinRoomInterval >2000.0 && isAlreadyDownloadMagicImagesEffect) {
                self.magicAnimationView.magicMsg = msg;
            }
        }else  if ([rcMessage.content isKindOfClass:[SPDTextMessage class]] || [rcMessage.content isKindOfClass:[RCTextMessage class]] || [rcMessage.content isMemberOfClass:[SPDBannedSpeakMessage class]] || [rcMessage.content isMemberOfClass:[SPDChatroomRedPacketMessage class]]){
            __weak typeof(&*self) __blockSelf = self;
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (rcMessage) {
                    [__blockSelf appendAndDisplayMessage:rcMessage];
                    UIMenuController *menu = [UIMenuController sharedMenuController];
                    menu.menuVisible=NO;
                    //如果消息不在最底部，收到消息之后不滚动到底部，加到列表中只记录未读数
                    if (![self isAtTheBottomOfTableView]) {
                        self.unreadNewMsgCount ++ ;
                        [self updateUnreadMsgCountLabel];
                    }
                }
            });
        }
        
    }
    
}

#pragma mark - 世界消息deleage - rejoin
- (void)clickedClanWithId:(NSString *)clan_id andClan_name:(NSString *)clan_name {
    if (![clan_id isEqualToString:self.clan_id] && clan_id.length >= 10) {
        [self joinChatroomWithClanId:clan_id];
    }
}

- (void)reloadDataWithClanId:(NSString *)clanId {
    [self.conversationDataRepository removeAllObjects];
    [self.onlineUsers removeAllObjects];
    [self.audiences removeAllObjects];
    self.sortedAudiences = @[];
    [self.familyMembers removeAllObjects];
    [self.speekers removeAllObjects];
    [self enableMic:NO];
    self.muteBtn.selected = YES;
    self.joinRoomInterval = [[NSDate date] timeIntervalSince1970] * 1000;
    self.conversationType = ConversationType_CHATROOM ;
    self.clan_id = clanId;
    [self joinInChatRoom];
    [self requestFamilyDetail];
    [self.conversationMessageCollectionView reloadData];
    [self.micCollectionView reloadData];
    [self.onlineUserConllectionView reloadData];
}

- (void)clickedAvatarImageView:(NSString *)userId {
    [self clicledAvatarWithUserId:userId];
}


#pragma mark - 用户进入房间动效
- (void)handleUserComeInChatRoomWithUserInfo:(RCUserInfo *)userinfo andMessage:(RCMessageContent *)message {
    SPDJoinChatRoomMessage *joinMsg = (SPDJoinChatRoomMessage *)message;
    if (![SPDCommonTool isEmpty:joinMsg.vehicle_id] && [joinMsg.grade integerValue] == 1) {
        if (isAlreadyDownloadVehicleImagesEffect) {
            NSMutableArray *images = [SPDVehicleEffectImageView getAnimationImagesWithVehicleId:joinMsg.vehicle_id isCGImage:NO];
            if (images.count) {
                NSDictionary *dic = @{@"type": @"-1", @"nick_name": userinfo.name, @"avatar": userinfo.portraitUri, @"gender": joinMsg.gender, @"images": images};
                self.vehicleAnimationView.dic = dic;
            }
        }
    } else if (![SPDCommonTool isEmpty:joinMsg.vehicle_id] && [joinMsg.grade integerValue] == 2) {
        if (isAlreadyDownloadVehicleImagesEffect) {
            self.vehicleEffectView.message = joinMsg;
        }
    } else {
        if ([SPDCommonTool isEmpty:joinMsg.vehicle_id]) {
            self.comeInChatRoomView.isHasVehicle = NO;
        } else {
            self.comeInChatRoomView.isHasVehicle = YES;
        }
        [self.comeInChatRoomView configViewWithNickName:userinfo.name andAvatar:userinfo.portraitUri andVehicle:joinMsg.image];
        [self.comeInChatRoomView startAnimationViewActionWithText:userinfo.name andGender:joinMsg.gender andInputbarFrame:self.inputBar.frame];
    }
    if (![SPDCommonTool isEmpty:joinMsg.noble]) {
        self.nobleEnterView.message = joinMsg;
    }
}

#pragma mark - 消息处理
/**
 *  将消息加入本地数组
 *
 *  @return
 */
- (void)appendAndDisplayMessage:(RCMessage *)rcMessage {
    if (!rcMessage || !rcMessage.content || !rcMessage.content.senderUserInfo) {
        return;
    }
    RCDLiveMessageModel *model = [[RCDLiveMessageModel alloc] initWithMessage:rcMessage];
    if([rcMessage.content isMemberOfClass:[RCDLiveGiftMessage class]]){
        model.messageId = -1;
    }
    if ([self appendMessageModel:model]) {
        if (self.conversationDataRepository.count>0) {
            NSIndexPath *indexPath =
            [NSIndexPath indexPathForItem:self.conversationDataRepository.count - 1
                                inSection:0];
            if ([self.conversationMessageCollectionView numberOfItemsInSection:0] !=
                self.conversationDataRepository.count - 1) {
                return;
            }
            [self.conversationMessageCollectionView
             insertItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
            if ([self isAtTheBottomOfTableView] || self.isNeedScrollToButtom) {
                [self scrollToBottomAnimated:YES];
                self.isNeedScrollToButtom=NO;
            }
            
        }
    }
}

/**
 *  如果当前会话没有这个消息id，把消息加入本地数组
 *
 *  @return
 */
- (BOOL)appendMessageModel:(RCDLiveMessageModel *)model {
    long newId = model.messageId;
    for (RCDLiveMessageModel *__item in self.conversationDataRepository) {
        /*
         * 当id为－1时，不检查是否重复，直接插入
         * 该场景用于插入临时提示。
         */
        if (newId == -1) {
            break;
        }
        if (newId == __item.messageId) {
            return NO;
        }
    }
    if (!model.content) {
        return NO;
    }
    //这里可以根据消息类型来决定是否显示，如果不希望显示直接return NO
    
    //数量不可能无限制的大，这里限制收到消息过多时，就对显示消息数量进行限制。
    //用户可以手动下拉更多消息，查看更多历史消息。
    if (self.conversationDataRepository.count>100) {
        //                NSRange range = NSMakeRange(0, 1);
        RCDLiveMessageModel *message = self.conversationDataRepository[0];
        [[RCIMClient sharedRCIMClient]deleteMessages:@[@(message.messageId)]];
        [self.conversationDataRepository removeObjectAtIndex:0];
        [self.conversationMessageCollectionView reloadData];
    }
    
    [self.conversationDataRepository addObject:model];
    return YES;
}

/**
 *  判断消息是否在collectionView的底部
 *
 *  @return 是否在底部
 */
- (BOOL)isAtTheBottomOfTableView {
    if (self.conversationMessageCollectionView.contentSize.height <= self.conversationMessageCollectionView.frame.size.height) {
        return YES;
    }
    if(self.conversationMessageCollectionView.contentOffset.y +200 >= (self.conversationMessageCollectionView.contentSize.height - self.conversationMessageCollectionView.frame.size.height)) {
        return YES;
    }else{
        return NO;
    }
}

#pragma mark <UIScrollViewDelegate>
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
    [self.inputBar setInputBarStatus:RCDLiveBottomBarDefaultStatus];
}

/**
 *  滚动条滚动时显示正在加载loading
 *
 *  @param scrollView
 */
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // 是否显示右下未读icon
    if (self.unreadNewMsgCount != 0) {
        [self checkVisiableCell];
    }
    
    if (scrollView.contentOffset.y < -5.0f) {
        //        [self.collectionViewHeader startAnimating];
    } else {
        //        [self.collectionViewHeader stopAnimating];
        _isLoading = NO;
    }
}

/**
 *  滚动结束加载消息 （聊天室消息还没存储，所以暂时还没有此功能）
 *
 *  @param scrollView scrollView description
 *  @param decelerate decelerate description
 */
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
                  willDecelerate:(BOOL)decelerate {
    if (scrollView.contentOffset.y < -15.0f && !_isLoading) {
        _isLoading = YES;
    }
}
/**
 *  消息滚动到底部
 *
 *  @param animated 是否开启动画效果
 */
- (void)scrollToBottomAnimated:(BOOL)animated {
    if ([self.conversationMessageCollectionView numberOfSections] == 0) {
        return;
    }
    NSUInteger finalRow = MAX(0, [self.conversationMessageCollectionView numberOfItemsInSection:0] - 1);
    if (0 == finalRow) {
        return;
    }
    NSIndexPath *finalIndexPath =
    [NSIndexPath indexPathForItem:finalRow inSection:0];
    [self.conversationMessageCollectionView scrollToItemAtIndexPath:finalIndexPath
                                                   atScrollPosition:UICollectionViewScrollPositionTop
                                                           animated:animated];
}


#pragma mark - SPDcell delegate

- (void)clicledAvatarWithUserId:(NSString *)userId {
    if (!_position || !_familyMembers.count) {
        if (!_isRequestPosition) {
            [self requestFamilyDetail];
        }
        return;
    }
    SPDVoiceLiveUserModel *model = self.onlineUsers[userId];
    NSInteger adminValue;
    if (model.position) {
        adminValue = self.positionLevel - model.positionLevel;
    } else {
        adminValue = self.positionLevel - [self getUserPosition:userId];
    }
    [self selectAudience:userId adminValue:adminValue];
}

#pragma mark - setter and getter

- (NSMutableArray *)familyMembers {
    if (!_familyMembers) {
        _familyMembers = [NSMutableArray array];
    }
    return _familyMembers;
}

- (NSMutableDictionary *)onlineUsers {
    if (!_onlineUsers) {
        _onlineUsers = [NSMutableDictionary dictionary];
    }
    return _onlineUsers;
}

- (NSMutableArray *)speekers {
    if (!_speekers) {
        _speekers = [NSMutableArray array];
    }
    return _speekers;
}

- (NSMutableDictionary *)audiences {
    if (!_audiences) {
        _audiences = [NSMutableDictionary dictionary];
    }
    return _audiences;
}

- (NSMutableDictionary *)tourists {
    _tourists = [NSMutableDictionary dictionaryWithDictionary:self.audiences];
    for (SPDFamilyMemberModel *model in self.familyMembers) {
        [_tourists removeObjectForKey:model.user_id];
    }
    return _tourists;
}

- (AFNetworkReachabilityManager *)networkReachabilityManager{
    if (!_networkReachabilityManager) {
        _networkReachabilityManager = [AFNetworkReachabilityManager sharedManager];
    }
    return _networkReachabilityManager;
}

- (UICollectionView *)micCollectionView {
    if (!_micCollectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(45, 45);
        flowLayout.minimumLineSpacing = 0;
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.sectionInset = UIEdgeInsetsMake(3.0f, 0.0f, 3.0f, 0.0f);
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _micCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _micCollectionView.dataSource = self;
        _micCollectionView.delegate = self;
        _micCollectionView.backgroundColor = [UIColor clearColor];
        _micCollectionView.showsHorizontalScrollIndicator = NO;
        [_micCollectionView registerNib:[UINib nibWithNibName:@"SPDVoiceLiveOnlineUserCell" bundle:nil] forCellWithReuseIdentifier:SPDVoiceLiveOnlineUserCellForMic];
        [self.view addSubview:_micCollectionView];
        [_micCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(self.view).with.offset(8);
            make.top.equalTo(self.view).with.offset(NavHeight + 30);
            make.width.mas_equalTo(5 * 45);
            make.height.mas_equalTo(51);
        }];
    }
    return _micCollectionView;
}

- (UICollectionView *)onlineUserConllectionView {
    if (!_onlineUserConllectionView) {
        //在线成员区
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(45, 45);
        flowLayout.minimumLineSpacing = 0;
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.sectionInset = UIEdgeInsetsMake(3.0f, 0.0f, 3.0f, 0.0f);
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _onlineUserConllectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _onlineUserConllectionView.dataSource = self;
        _onlineUserConllectionView.delegate = self;
        _onlineUserConllectionView.backgroundColor = [UIColor clearColor];
        _onlineUserConllectionView.showsHorizontalScrollIndicator = NO;
        [_onlineUserConllectionView registerNib:[UINib nibWithNibName:@"SPDVoiceLiveOnlineUserCell" bundle:nil] forCellWithReuseIdentifier:SPDVoiceLiveOnlineUserCellIndentifier];
        [self.view addSubview:_onlineUserConllectionView];
        [_onlineUserConllectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(self.micCollectionView.mas_trailing);
            make.trailing.mas_equalTo(0);
            make.top.equalTo(self.view).with.offset(NavHeight + 30);
            make.height.mas_equalTo(51);
        }];
    }
    return _onlineUserConllectionView;
}

- (void)setRankTotalGold:(NSNumber *)rankTotalGold {
    _rankTotalGold = rankTotalGold;
    
    if (_rankTotalGold.integerValue >= 1000000) {
        self.rankTotalGoldLabel.text = [NSString stringWithFormat:@"%.1lfM", floor(_rankTotalGold.integerValue / 1000000.0 * 10) / 10];
    } else if (_rankTotalGold.integerValue >= 1000) {
        self.rankTotalGoldLabel.text = [NSString stringWithFormat:@"%.1lfK", floor(_rankTotalGold.integerValue / 1000.0 * 10) / 10];
    } else {
        self.rankTotalGoldLabel.text = _rankTotalGold.stringValue;
    }
}

- (void)setOnlineNumber:(NSInteger)onlineNumber {
    _onlineNumber = onlineNumber;
    self.onlineNumberLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"  在线：%ld人  ", nil), _onlineNumber];
}

- (void)setPosition:(NSString *)position {
    _position = position;
    self.positionLevel = [self getPositionLevel:_position];
    if (self.positionLevel == 150) {
        UIButton *moreBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 30)];
        [moreBtn setImage:[UIImage imageNamed:@"chatroom_more"] forState:UIControlStateNormal];
        [moreBtn addTarget:self action:@selector(showPopoverView:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:moreBtn];
        self.navigationItem.rightBarButtonItems = @[self.navigationItem.rightBarButtonItems.firstObject, rightBarButtonItem];
    } else if (self.positionLevel == 0 && ![self.familyInfoDict[@"clan"][@"is_following"] boolValue]) {
        UIButton *followBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 34, 30)];
        [followBtn setImage:[UIImage imageNamed:@"chatroom_follow_btn"] forState:UIControlStateNormal];
        [followBtn addTarget:self action:@selector(requestClanFollow) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:followBtn];
        self.navigationItem.rightBarButtonItems = @[self.navigationItem.rightBarButtonItems.firstObject, rightBarButtonItem];
    }
}

-(SPDComeInChatRoomView *) comeInChatRoomView{
    if (!_comeInChatRoomView) {
        _comeInChatRoomView = [[SPDComeInChatRoomView alloc]initWithFrame:CGRectMake(kScreenW,self.inputBar.frame.origin.y-25, 100, 25)];
        _comeInChatRoomView.layer.cornerRadius = 6;
        _comeInChatRoomView.clipsToBounds =YES;
        _comeInChatRoomView.backgroundColor = SPD_HEXCOlOR(@"fe69a1");
    }
    return _comeInChatRoomView;
}

-(SPDChatRoomGlobalView *)globalNotifyView{
    if (!_globalNotifyView) {
        _globalNotifyView = [[SPDChatRoomGlobalView alloc]initWithFrame:CGRectMake(kScreenW, 0, kScreenW-26, 117)];
        _globalNotifyView.deleage = self;
        _globalNotifyView.hidden = YES;
    }
    return _globalNotifyView;
}

- (SPDChatroomNobleEnterView *)nobleEnterView {
    if (!_nobleEnterView) {
        _nobleEnterView = [[SPDChatroomNobleEnterView alloc] initWithFrame:CGRectMake(kScreenW + 10, 0, 271, 30)];
    }
    return _nobleEnterView;
}

-(SPDChatRoomPrivatePresentView *)privatePresentView{
    if (!_privatePresentView) {
        _privatePresentView = [[SPDChatRoomPrivatePresentView alloc]initWithFrame:CGRectMake(kScreenW, 282, 191, 30)];
    }
    return _privatePresentView;
}

- (SPDChatRoomMagicEffectView *)magicAnimationView {
    if (!_magicAnimationView) {
        _magicAnimationView = [[SPDChatRoomMagicEffectView alloc]initWithFrame:CGRectMake(19, 12, kScreenW-38,(115+460)/2 )];
    }
    return _magicAnimationView;
}

- (SDCycleScrollView *)activityScrollView {
    if (!_activityScrollView) {
        _activityScrollView = [[SDCycleScrollView alloc] init];
        _activityScrollView.backgroundColor = [UIColor clearColor];
        _activityScrollView.delegate = self;
        _activityScrollView.localizationImageNamesGroup = @[@"ic_chatroom_activity", @"chatroom_lottery"];
        _activityScrollView.bannerImageViewContentMode = UIViewContentModeScaleAspectFit;
        _activityScrollView.showPageControl = YES;
        _activityScrollView.pageControlDotSize = CGSizeMake(3, 3);
        _activityScrollView.pageControlBottomOffset = -15;
        _activityScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleAnimated;
        _activityScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    }
    return _activityScrollView;
}

-(FDAlertView *)alert{
    if (!_alert) {
        //清空性别
        _alert = [[FDAlertView alloc] init];
    }
    
    return _alert;
}

- (SPDKeywordMatcher *)keyword_match {
    if (!_keyword_match) {
        _keyword_match = [[SPDKeywordMatcher alloc]init];
    }
    return _keyword_match;
}

- (KeywordMap *)keymap {
    if (!_keymap) {
        _keymap = [self.keyword_match convert:[self.wordDict allKeys]];
    }
    return _keymap;
}

- (ZTWAInfoShareView *)shareView {
    if (!_shareView) {
        _shareView=[ZTWAInfoShareView shareView];
        _shareView.delegate=self;
    }
    return _shareView;
}

- (UIImageView *)backImageView {
    if (!_backImageView) {
        _backImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
        _backImageView.image = [UIImage imageNamed:@"ic_chatroom_back_image"];
        _backImageView.hidden = YES;
        _backImageView.userInteractionEnabled = YES;
        [[UIApplication sharedApplication].keyWindow addSubview:_backImageView];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapBackImageView)];
        [_backImageView addGestureRecognizer:tap];
        
        UIButton *keepBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        keepBtn.frame = CGRectMake(0, 0, 90, 90);
        keepBtn.center = CGPointMake(_backImageView.bounds.size.width / 2, _backImageView.bounds.size.height * 1 / 3);
        [keepBtn setBackgroundImage:[UIImage imageNamed:@"chatroom_keep_btn"] forState:UIControlStateNormal];
        [keepBtn addTarget:self action:@selector(keep) forControlEvents:UIControlEventTouchUpInside];
        [_backImageView addSubview:keepBtn];
        
        UILabel *keepLabel = [[UILabel alloc] init];
        keepLabel.frame = CGRectMake(0, CGRectGetMaxY(keepBtn.frame) + 11, _backImageView.bounds.size.width, 21);
        keepLabel.font = [UIFont systemFontOfSize:19];
        keepLabel.textColor = [UIColor whiteColor];
        keepLabel.textAlignment = NSTextAlignmentCenter;
        keepLabel.text = SPDStringWithKey(@"保留", nil);
        [_backImageView addSubview:keepLabel];
        
        UIButton *exitBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        exitBtn.frame = CGRectMake(0, 0, 90, 90);
        exitBtn.center = CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height * 2 / 3);
        [exitBtn setBackgroundImage:[UIImage imageNamed:@"chatroom_exit_btn"] forState:UIControlStateNormal];
        [exitBtn addTarget:self action:@selector(exit) forControlEvents:UIControlEventTouchUpInside];
        [_backImageView addSubview:exitBtn];
        
        UILabel *exitLabel = [[UILabel alloc] init];
        exitLabel.frame = CGRectMake(0, CGRectGetMaxY(exitBtn.frame) + 11, _backImageView.bounds.size.width, 21);
        exitLabel.font = [UIFont systemFontOfSize:19];
        exitLabel.textColor = [UIColor whiteColor];
        exitLabel.textAlignment = NSTextAlignmentCenter;
        exitLabel.text = SPDStringWithKey(@"退出", nil);
        [_backImageView addSubview:exitLabel];
    }
    return _backImageView;
}

- (void)dealloc {
    NSLog(@"dealloc");
}

@end
