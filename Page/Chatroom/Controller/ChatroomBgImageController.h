//
//  ChatroomBgImageController.h
//  SimpleDate
//
//  Created by 李楠 on 2018/8/20.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "BaseViewController.h"

@protocol ChatroomBgImageControllerDelegate <NSObject>

@optional

- (void)backgroundDidChangeToImage:(NSString *)image;

@end

@interface ChatroomBgImageController : BaseViewController

@property (nonatomic, copy) NSString *clanId;
@property (nonatomic, weak) id<ChatroomBgImageControllerDelegate> delegate;

@end
