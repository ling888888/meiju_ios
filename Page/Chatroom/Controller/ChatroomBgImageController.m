//
//  ChatroomBgImageController.m
//  SimpleDate
//
//  Created by 李楠 on 2018/8/20.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "ChatroomBgImageController.h"
#import "ChatroomBgImageCell.h"
#import "ChatroomBgImageModel.h"

@interface ChatroomBgImageController ()<UICollectionViewDataSource, UICollectionViewDelegate, ChatroomBgImageCellDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) ChatroomBgImageModel *usedModel;
@property (nonatomic, assign) BOOL isRequestingUse;

@end

@implementation ChatroomBgImageController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar vhl_setBackgroundColor:[UIColor whiteColor]];
    self.navigationItem.title = SPDStringWithKey(@"主题", nil);
    [self.view addSubview:self.collectionView];
    [self requestChatroomBackgroundList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Request

- (void)requestChatroomBackgroundList {
    NSDictionary *dic = @{@"clanId": self.clanId};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"chatroom.background.list" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        for (NSDictionary *dic in suceess[@"list"]) {
            ChatroomBgImageModel *model = [ChatroomBgImageModel initWithDictionary:dic];
            if (model.isUse) {
                self.usedModel = model;
            }
            [self.dataArray addObject:model];
        }
        [self.collectionView reloadData];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestChatroomBackgroundBuyWithModel:(ChatroomBgImageModel *)model {
    NSDictionary *dic = @{@"clanId": self.clanId, @"backgroundId": model.backgroundId};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"chatroom.background.buy" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        NSString *title = SPDStringWithKey(@"购买成功，确定使用当前主题？", nil);
        [self presentCommonController:title messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
            if (!model.isUse) {
                [self requestChatroomBackgroundUseWithModel:model];
            }
        }];
        model.isOwn = YES;
        model.expireTime = suceess[@"expireTime"];
        [self.collectionView reloadData];
    } bFailure:^(id  _Nullable failure) {
        if ([failure isKindOfClass:[NSDictionary class]]) {
            if ([failure[@"code"] integerValue] == 202) {
                [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                    [self pushToRechageController];
                }];
            } else {
                [self showToast:failure[@"msg"]];
            }
        }
    }];
}

- (void)requestChatroomBackgroundUseWithModel:(ChatroomBgImageModel *)model {
    self.isRequestingUse = YES;
    NSDictionary *dic = @{@"clanId": self.clanId, @"backgroundId": model.backgroundId};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"chatroom.background.use" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        [self showToast:SPDStringWithKey(@"房间主题更改成功", nil)];
        self.usedModel.isUse = NO;
        model.isUse = YES;
        self.usedModel = model;
        [self.collectionView reloadData];
        if ([self.delegate respondsToSelector:@selector(backgroundDidChangeToImage:)]) {
            [self.delegate backgroundDidChangeToImage:model.image];
        }
        self.isRequestingUse = NO;
    } bFailure:^(id  _Nullable failure) {
        self.isRequestingUse = NO;
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ChatroomBgImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatroomBgImageCell" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    cell.delegate = self;
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    ChatroomBgImageModel *model = self.dataArray[indexPath.row];
    if (model.isOwn) {
        if (!model.isUse && !self.isRequestingUse) {
            [self requestChatroomBackgroundUseWithModel:model];
        }
    } else {
        [self showToast:SPDStringWithKey(@"还未拥有背景，购买后可使用", nil)];
    }
}

#pragma mark - ChatroomBgImageCellDelegate

- (void)didClickBuyBgImageWithModel:(ChatroomBgImageModel *)model {
    NSString *title;
    if (model.isOwn) {
        title = [NSString stringWithFormat:SPDStringWithKey(@"确定花费%@金币续费房间主题？", nil), model.renewPrice];
    } else {
        title = [NSString stringWithFormat:SPDStringWithKey(@"确定花费%@金币购买%@天房间主题？", nil), model.price, model.day];
    }
    [self presentCommonController:title messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
        [self requestChatroomBackgroundBuyWithModel:model];
    }];
}

#pragma mark - Setters and Getters

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        CGFloat width = (kScreenW - 11 - 8 - 4) / 2;
        flowLayout.itemSize = CGSizeMake(width, ((width - 5) * 220 / 169) + 60);
        flowLayout.minimumLineSpacing = 0;
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.sectionInset = UIEdgeInsetsMake(5, 11, 4, 4);
        _collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.showsVerticalScrollIndicator = NO;
        [_collectionView registerNib:[UINib nibWithNibName:@"ChatroomBgImageCell" bundle:nil] forCellWithReuseIdentifier:@"ChatroomBgImageCell"];
    }
    return _collectionView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

@end
