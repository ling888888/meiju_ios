//
//  SPDFamilyInfoAvatarCell.h
//  SimpleDate
//
//  Created by Monkey on 2018/3/22.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPDFamilyMemberModel.h"

@interface SPDFamilyInfoAvatarCell : UICollectionViewCell

@property (nonatomic , strong) SPDFamilyMemberModel *mymodel;

@end
