//
//  NoblePrivilegeCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/9/11.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NoblePrivilegeModel;

@interface NoblePrivilegeCell : UICollectionViewCell

@property (nonatomic, strong) NoblePrivilegeModel *model;

@end
