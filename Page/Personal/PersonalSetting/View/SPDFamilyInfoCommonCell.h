//
//  SPDFamilyInfoCommonCell.h
//  SimpleDate
//
//  Created by Monkey on 2018/3/22.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPDFamilyMemberModel.h"

@protocol SPDFamilyInfoCommonCellDelegate <NSObject>

- (void)clickedUserAvatarWithModel: (SPDFamilyMemberModel *)model;

- (void)clickedViewAllWithType: (NSString  *)type;


@end

@interface SPDFamilyInfoCommonCell : BaseTableViewCell

@property(nonatomic , copy) NSString * type; // apply member

@property(nonatomic , strong) NSMutableArray * dataArray;

@property (weak, nonatomic) IBOutlet UIImageView *titleImgView;

@property (weak, nonatomic) IBOutlet UILabel *titleLable;

@property(nonatomic , weak) id<SPDFamilyInfoCommonCellDelegate> delegate ;

@end
