//
//  SPDFamilySwitchCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/10/9.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SPDFamilySwitchCellDelegate <NSObject>

@optional

- (void)didClickActionSwitch:(UISwitch *)sender;

@end

@interface SPDFamilySwitchCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UISwitch *actionSwitch;

@property (nonatomic, weak) id<SPDFamilySwitchCellDelegate> delegate;

@end
