//
//  SPDFamilyTipsEditCell.h
//  SimpleDate
//
//  Created by Monkey on 2018/3/23.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPDFamilyTipsEditCell : BaseTableViewCell

@property (nonatomic , copy) NSDictionary * dict;

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *height;


@end
