//
//  SPDFamilyInfoHeaderCell.m
//  SimpleDate
//
//  Created by Monkey on 2018/3/22.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDFamilyInfoHeaderCell.h"

@interface SPDFamilyInfoHeaderCell()
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *nick_idLabel;

@property (weak, nonatomic) IBOutlet UIImageView *backImageView;

@property (nonatomic,strong)UIView * blackAlphaView;

@property (weak, nonatomic) IBOutlet UIButton *countryBtn;

@property (weak, nonatomic) IBOutlet UILabel *descLable;
@property (weak, nonatomic) IBOutlet UIView *editInfoBtn;
@property (weak, nonatomic) IBOutlet UILabel *editLabel;
@property (weak, nonatomic) IBOutlet UIButton *followBtn;
@property (weak, nonatomic) IBOutlet UIImageView *starImageView1;
@property (weak, nonatomic) IBOutlet UIImageView *starImageView2;
@property (weak, nonatomic) IBOutlet UIImageView *starImageView3;
@property (weak, nonatomic) IBOutlet UIImageView *starImageView4;
@property (weak, nonatomic) IBOutlet UIImageView *starImageView5;
@property (weak, nonatomic) IBOutlet UIImageView *starImageView6;
@property (weak, nonatomic) IBOutlet UIImageView *starImageView7;
@property (weak, nonatomic) IBOutlet UILabel *currentLevelLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *expProgressView;
@property (weak, nonatomic) IBOutlet UILabel *expLabel;
@property (weak, nonatomic) IBOutlet UILabel *nextLevelLabel;

@end

@implementation SPDFamilyInfoHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.avatar.layer.cornerRadius = 47;
    self.avatar.clipsToBounds = YES;
    self.avatar.layer.borderColor = [UIColor whiteColor].CGColor;
    self.avatar.layer.borderWidth = 1;
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(jumpEdit)];
    [self.editInfoBtn addGestureRecognizer:tap];
    
    self.editLabel.text = SPDStringWithKey(@"编辑", nil);
    for (UIView *view in self.expProgressView.subviews) {
        view.layer.masksToBounds = YES;
        view.layer.cornerRadius = 5 / 2.0;
    }
}

- (void)jumpEdit {
    if (self.delegate && [self.delegate respondsToSelector:@selector(familyHeaderCellClickEditInfoBtn)]) {
        [self.delegate familyHeaderCellClickEditInfoBtn];
    }
}

- (void)setDict:(NSMutableDictionary *)dict{
    _dict = dict;
    [self.avatar sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:dict[@"clan"][@"avatar"]]];
 
    [self.backImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:dict[@"clan"][@"avatar"]]];
    self.backImageView.image = [SPDCommonTool coreBlurImage:self.backImageView.image withBlurNumber:9];
    self.blackAlphaView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    self.nameLabel.text = dict[@"clan"][@"name"];
    self.nick_idLabel.text = [NSString stringWithFormat:@"%@ID:%@",SPDStringWithKey(@"家族", nil),dict[@"clan"][@"nick_id"]];
    self.descLable.text = dict[@"clan"][@"desc"];
    if (!dict[@"clan"][@"country"]) {
        [self.countryBtn setTitle:SPDStringWithKey(@"世界", nil) forState:UIControlStateNormal];
    }else{
        [self.countryBtn setTitle:dict[@"clan"][@"country_name"] forState:UIControlStateNormal];
    }
    [self.countryBtn setImage:[UIImage imageNamed:@"ic_clan_loac"] forState:UIControlStateNormal];
    self.editInfoBtn.hidden = ![dict[@"position"] isEqualToString:@"chief"];

    if ([SPDCommonTool currentVersionIsArbic]) {
        [self.countryBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
        [self.countryBtn setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 10)];
    }else{
        [self.countryBtn setImageEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
    }
    
    NSMutableArray *starImageViews = [@[self.starImageView7, self.starImageView1, self.starImageView2, self.starImageView3, self.starImageView4, self.starImageView5, self.starImageView6] mutableCopy];
    for (UIImageView *imageView in starImageViews) {
        imageView.image = [UIImage imageNamed:@"ic_clan_star_s"];
    }
    for (NSNumber *index in _dict[@"clan"][@"tasks"]) {
        UIImageView *imageView = starImageViews[index.integerValue];
        imageView.image = [UIImage imageNamed:@"ic_clan_star_hl_s"];
    }
    
    NSInteger clanLevel = [_dict[@"clan"][@"clanLevel"] integerValue];
    if ([dict[@"position"] isEqualToString:@"tourist"]) {
        self.expProgressView.hidden = YES;
        self.expLabel.hidden = YES;
        self.nextLevelLabel.hidden = YES;
        self.currentLevelLabel.text = [NSString stringWithFormat:@"LV.%ld",  clanLevel];
    } else {
        self.currentLevelLabel.hidden = YES;
        self.followBtn.hidden = YES;
        NSInteger levelExp = [_dict[@"clan"][@"levelExp"] integerValue];
        NSInteger currentExp = [_dict[@"clan"][@"currentExp"] integerValue];
        NSInteger nextExp = [_dict[@"clan"][@"nextExp"] integerValue];
        [self.expProgressView setProgress:(levelExp - currentExp) * 1.0 / (nextExp - currentExp) animated:NO];
        self.expLabel.text = [NSString stringWithFormat:@"%ld/%ld", levelExp - currentExp, nextExp - currentExp];
        self.nextLevelLabel.text = [NSString stringWithFormat:@"LV.%ld",  clanLevel + 1];
    }
}

- (void)setIsFollowing:(BOOL)isFollowing {
    _isFollowing = isFollowing;
    
    if (_isFollowing) {
        self.followBtn.backgroundColor = [UIColor clearColor];
        self.followBtn.layer.borderWidth = 1;
        [self.followBtn setTitle:SPDStringWithKey(@"取消关注", nil) forState:UIControlStateNormal];
    } else {
        self.followBtn.backgroundColor = [UIColor colorWithHexString:COMMON_PINK];
        self.followBtn.layer.borderWidth = 0;
        [self.followBtn setTitle:SPDStringWithKey(@"关注", nil) forState:UIControlStateNormal];
    }
}

- (IBAction)clickFollowBtn:(UIButton *)sender {
    NSDictionary *dic = @{@"user_id": [SPDApiUser currentUser].userId, @"clan_id": self.dict[@"clan"][@"_id"], @"action": self.isFollowing ? @"cancel" : @"follow"};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"follow" isClan:YES bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        self.isFollowing = !self.isFollowing;
        [SPDCommonTool showWindowToast:SPDStringWithKey(self.isFollowing ? @"关注家族成功" : @"已取消关注", nil)];
        if (self.delegate && [self.delegate respondsToSelector:@selector(familyHeaderCellIsFollowingDidChange:)]) {
            [self.delegate familyHeaderCellIsFollowingDidChange:self.isFollowing];
        }
    } bFailure:^(id  _Nullable failure) {
        if ([failure isKindOfClass:[NSDictionary class]]) {
            [SPDCommonTool showWindowToast:failure[@"msg"]];
        }
    }];
}

- (IBAction)clickSignInButton:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(familyInfoHeaderCellDidClickSignInButton)]) {
        [self.delegate familyInfoHeaderCellDidClickSignInButton];
    }
}

- (UIView *)blackAlphaView {
    if (!_blackAlphaView) {
        _blackAlphaView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenW, self.backImageView.bounds.size.height)];
        [self.backImageView addSubview:_blackAlphaView];
    }
    return _blackAlphaView;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
