//
//  FamilyMemberListCell.m
//  SimpleDate
//
//  Created by 李楠 on 2019/2/3.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "FamilyMemberListCell.h"
#import "SPDFamilyMemberModel.h"

@interface FamilyMemberListCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *tagImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *loginTimeBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loginTimeBtnTrailing;
@property (weak, nonatomic) IBOutlet UIButton *genderAgeBtn;
@property (weak, nonatomic) IBOutlet UIButton *promoteBtn;
@property (weak, nonatomic) IBOutlet UIButton *kickoutBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *kickoutBtnBottom;

@end

@implementation FamilyMemberListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.kickoutBtn setTitle:SPDStringWithKey(@"踢出", nil) forState:UIControlStateNormal];
    if ([SPDCommonTool currentVersionIsArbic]) {
        [self.genderAgeBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        [self.genderAgeBtn setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 2.5)];
        [self.loginTimeBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 4, 0, 0)];
        [self.loginTimeBtn setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 2)];
    }
}

- (void)setModel:(SPDFamilyMemberModel *)model {
    _model = model;
    
    [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:_model.avatar]];
    self.avatarImageView.layer.borderColor = [UIColor colorWithHexString:[self.position isEqualToString:@"deputychief"] ? @"8FC3EB" : @"D7C1AD"].CGColor;
    self.tagImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"clan_%@_tag", _model.position]];
    self.nickNameLabel.text = _model.nick_name;
    [self.loginTimeBtn setTitle:_model.last_login_time forState:UIControlStateNormal];
    if ([_model.gender isEqualToString:@"female"]) {
        [self.genderAgeBtn setBackgroundColor:[UIColor colorWithHexString:@"ff7eca"]];
        [self.genderAgeBtn setImage:[UIImage imageNamed:@"chatroomuser_female"] forState:UIControlStateNormal];
    } else if ([_model.gender isEqualToString:@"male"]) {
        [self.genderAgeBtn setBackgroundColor:[UIColor colorWithHexString:@"5db4fd"]];
        [self.genderAgeBtn setImage:[UIImage imageNamed:@"chatroomuser_male"] forState:UIControlStateNormal];
    }
    [self.genderAgeBtn setTitle:_model.age.stringValue forState:UIControlStateNormal];
    
    if ([self.position isEqualToString:@"chief"]) {
        if ([_model.position isEqualToString:@"deputychief"]) {
            [self.promoteBtn setTitle:SPDStringWithKey(@"降级", nil) forState:UIControlStateNormal];
        } else {
            [self.promoteBtn setTitle:SPDStringWithKey(@"晋升", nil) forState:UIControlStateNormal];
        }
    } else if ([self.position isEqualToString:@"deputychief"]) {
        self.promoteBtn.hidden = YES;
        if ([_model.position isEqualToString:@"deputychief"]) {
            self.kickoutBtn.hidden = YES;
        } else {
            self.kickoutBtnBottom.constant = 36;
        }
    } else {
        self.promoteBtn.hidden = YES;
        self.kickoutBtn.hidden = YES;
        self.loginTimeBtnTrailing.constant = 20;
    }
}

- (IBAction)clickPromoteBtn:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickActionWithModel:url:)]) {
        if ([_model.position isEqualToString:@"deputychief"]) {
            [self.delegate didClickActionWithModel:self.model url:@"demote"];
        } else {
            [self.delegate didClickActionWithModel:self.model url:@"promotion"];
        }
    }
}

- (IBAction)clickKickoutBtn:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickActionWithModel:url:)]) {
        [self.delegate didClickActionWithModel:self.model url:@"kick"];
    }
}

@end
