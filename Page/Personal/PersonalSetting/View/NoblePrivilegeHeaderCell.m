//
//  NoblePrivilegeHeaderCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/9/11.

#import "NoblePrivilegeHeaderCell.h"
#import "NobleModel.h"
#import "YYWebImage.h"

@interface NoblePrivilegeHeaderCell ()

@property (weak, nonatomic) IBOutlet YYAnimatedImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *privilegeLabel;
@property (weak, nonatomic) IBOutlet UILabel *privilegeCountLabel;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *privilegeBgView;

@end

@implementation NoblePrivilegeHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.privilegeLabel.text = SPDStringWithKey(@"贵族特权", nil);
}

- (void)setModel:(NobleModel *)model {
    _model = model;
    
    self.imageView.image = [[YYImage alloc]initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_model.icon_active]]];
    self.nameLabel.text = _model.name;
    self.statusLabel.text = SPDStringWithKey(_model.isOwned ? @"已激活" : @"暂未激活", nil);
    self.privilegeCountLabel.text = [NSString stringWithFormat:@"(%ld/%ld)", _model.privileges.count, _model.privilegeArray.count];
}

@end
