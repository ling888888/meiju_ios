//
//  SPDCustomSliderView.h
//  SimpleDate
//
//  Created by 侯玲 on 2019/1/16.
//  Copyright © 2019年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPDCustomSliderView : UIView

- (void)updateSliderLengthWith:(CGFloat)sliderLength;

@end
