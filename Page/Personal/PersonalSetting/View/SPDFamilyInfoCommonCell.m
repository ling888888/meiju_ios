//
//  SPDFamilyInfoCommonCell.m
//  SimpleDate
//
//  Created by Monkey on 2018/3/22.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDFamilyInfoCommonCell.h"
#import "SPDFamilyInfoAvatarCell.h"

@interface SPDFamilyInfoCommonCell ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UIImageView *tapImageView;
@property (weak, nonatomic) IBOutlet UILabel *noOneLabel;

@end

@implementation SPDFamilyInfoCommonCell



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"SPDFamilyInfoAvatarCell" bundle:nil] forCellWithReuseIdentifier:@"SPDFamilyInfoAvatarCell"];
    UICollectionViewFlowLayout *layout= [[UICollectionViewFlowLayout alloc] init];
    layout.minimumInteritemSpacing = 13;
    self.collectionView.collectionViewLayout = layout;
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = CGSizeMake(54, 54);
    
    self.tapImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleImageViewTap:)];
    [self.tapImageView addGestureRecognizer:tap];
    
}

- (void)setDataArray:(NSMutableArray *)dataArray {
    _dataArray = dataArray;
    if ([self.type isEqualToString:@"apply"]) {
        self.tapImageView.hidden = dataArray.count == 0 ? YES : NO;
        self.noOneLabel.hidden = dataArray.count == 0 ? NO : YES;
        self.noOneLabel.text = SPDStringWithKey(@"空空如也", nil);
    }
    [self.collectionView reloadData];
}

- (void)handleImageViewTap:(UITapGestureRecognizer *)tap {
    [self.delegate clickedViewAllWithType:self.type];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SPDFamilyInfoAvatarCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SPDFamilyInfoAvatarCell" forIndexPath:indexPath];
    cell.mymodel = self.dataArray[indexPath.row];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    SPDFamilyMemberModel * model = self.dataArray[indexPath.row];
    [self.delegate clickedUserAvatarWithModel:model];
}

- (void)setType:(NSString *)type{
    _type = type;
    NSString * imgName = [type isEqualToString:@"apply"]?@"ic_family_apply" :@"ic_family_member_detail";
    self.titleImgView.image = [UIImage imageNamed:imgName];
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
