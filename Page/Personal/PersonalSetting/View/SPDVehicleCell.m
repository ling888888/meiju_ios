//
//  SPDVehicleCell.m
//  SimpleDate
//
//  Created by 李楠 on 17/7/10.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDVehicleCell.h"
#import "SPDVehicleModel.h"

@implementation SPDVehicleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.moreVehicleTip.text = SPDStringWithKey(@"更多商品\n敬请期待", nil);
    self.isDriveImage.image = [UIImage imageNamed:@"vehicle_drive"].adaptiveRtl;
    
    NSDictionary *attribtDic = @{NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc] initWithString:SPDStringWithKey(@"点击试驾", nil) attributes:attribtDic];
    [self.previewButton setAttributedTitle:attribtStr forState:UIControlStateNormal];
}

- (void)setModel:(SPDVehicleModel *)model {
    _model = model;
    
    [self.imageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _model.image]]];
    self.nameLabel.text = _model.name;
    
    if (_isMine) {
        self.validTimeBtn.hidden = YES;
        self.priceButton.hidden = YES;
        self.tagImageView.hidden = YES;
        self.previewButton.hidden = YES;
        self.descLabel.hidden = YES;
        
        self.isDriveImage.hidden = !_model.is_drive;
        self.expireTimeLabel.hidden = _model.type.integerValue == 5;
        self.expireTimeLabel.text = [_model.expire_time_str substringFromIndex:5];
        if (_model.is_drive) {
            [self.equipButton setTitle:SPDStringWithKey(@"装备中", nil) forState:UIControlStateNormal];
        } else {
            [self.equipButton setTitle:SPDStringWithKey(@"装备", nil) forState:UIControlStateNormal];
        }
        [self.buyButton setTitle:SPDStringWithKey(@"续费", nil) forState:UIControlStateNormal];
    } else {
        self.isDriveImage.hidden = YES;
        self.expireTimeLabel.hidden = YES;
        self.equipButton.hidden = YES;
        
        self.validTimeBtn.hidden = _model.type.integerValue == 5;
        [self.validTimeBtn setTitle:SPDStringWithKey(_model.valid_time, nil) forState:UIControlStateNormal];
        self.previewButton.hidden = !_model.grade.integerValue;
        if (_model.type.integerValue) {
            self.buyButton.hidden = YES;
            self.priceButton.hidden = YES;
            self.tagImageView.hidden = NO;
            self.descLabel.hidden = NO;

            switch (_model.type.integerValue) {
                case 1:
                    self.tagImageView.image = [UIImage imageNamed:[SPDCommonTool getArImageNameWithImage:@"vehicle_exclusive"]];
                    break;
                case 2:
                    self.tagImageView.image = [UIImage imageNamed:[SPDCommonTool getArImageNameWithImage:@"vehicle_activity"]];
                    break;
                case 4:
                    self.tagImageView.image = [UIImage imageNamed:[SPDCommonTool getArImageNameWithImage:@"vehicle_vip"]];
                    break;
                case 5:
                    self.tagImageView.image = [UIImage imageNamed:[SPDCommonTool getArImageNameWithImage:@"vehicle_aristocracy"]];
                    break;
                default:
                    self.tagImageView.hidden = YES;
                    break;
            }
            self.descLabel.text = _model.desc;
            self.descLabel.textColor = [UIColor colorWithHexString:_model.desc_color];
        } else {
            self.tagImageView.hidden = YES;
            self.descLabel.hidden = YES;
            self.buyButton.hidden = NO;
            self.priceButton.hidden = NO;
            
            [self.priceButton setTitle:[NSString stringWithFormat:@"%@%@", _model.price, SPDStringWithKey(@"金币", nil)] forState:UIControlStateNormal];
            [self.buyButton setTitle:SPDStringWithKey(@"购买", nil) forState:UIControlStateNormal];
        }
    }
}

- (IBAction)buyVehicle:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didClickBuyButton:)]) {
        [self.delegate didClickBuyButton:_model];
    }
}

- (IBAction)equipVehicle:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didClickEquipButton:)]) {
        [self.delegate didClickEquipButton:_model];
    }
}

- (IBAction)previewVehicle:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didClickPreviewButton:)]) {
        [self.delegate didClickPreviewButton:_model];
    }
}

@end
