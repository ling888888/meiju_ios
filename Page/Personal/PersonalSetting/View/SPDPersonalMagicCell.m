//
//  SPDPersonalMagicCell.m
//  SimpleDate
//
//  Created by 李楠 on 2017/9/29.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDPersonalMagicCell.h"
#import "SPDMagicModel.h"
#import "MagicPropHelpView.h"
#import "SPDApiUser.h"

@interface SPDPersonalMagicCell ()

@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UIImageView *levelImageView1;
@property (weak, nonatomic) IBOutlet UIImageView *levelImageView2;
@property (weak, nonatomic) IBOutlet UIImageView *levelImageView3;
@property (weak, nonatomic) IBOutlet UIImageView *levelImageView4;
@property (weak, nonatomic) IBOutlet UIImageView *levelImageView5;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel;
@property (weak, nonatomic) IBOutlet UILabel *upgradeMoneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *magicalImpactLabel;
@property (weak, nonatomic) IBOutlet UILabel *AdvancedImpactLabel;
@property (weak, nonatomic) IBOutlet UILabel *propCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *upgrade1Btn;
@property (weak, nonatomic) IBOutlet UIButton *upgrade10Btn;
@property (weak, nonatomic) IBOutlet UIButton *upgrade100Btn;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentEffectLabel;
@property (weak, nonatomic) IBOutlet UILabel *nextEffectLabel;

@end

@implementation SPDPersonalMagicCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    for (UIView *view in self.progressView.subviews) {
        view.layer.masksToBounds = YES;
        view.layer.cornerRadius = 11 / 2.0;
    }
    [self.upgrade1Btn setTitle:SPDStringWithKey(@"升級1次", nil) forState:UIControlStateNormal];
    [self.upgrade10Btn setTitle:SPDStringWithKey(@"升級10次", nil) forState:UIControlStateNormal];
    [self.upgrade100Btn setTitle:SPDStringWithKey(@"升級100次", nil) forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(SPDMagicModel *)model {
    _model = model;
    
    [self.coverImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, model.cover]]];
    self.nameLabel.text = SPDStringWithKey(model.name, nil);
    
    NSInteger magicLevel = model.magic_level.integerValue;
    self.levelLabel.text = [NSString stringWithFormat:@"LV.%ld", magicLevel];
//    NSInteger count = (magicLevel - 1) % 5 + 1;
//    UIImage *levelImage = [UIImage imageNamed:[NSString stringWithFormat:@"MagicLevel%ld", (magicLevel - 1) / 5]];
//    UIImage *emptyImage = [UIImage imageNamed:[NSString stringWithFormat:@"MagicLevelEmpty%ld", (magicLevel - 1) / 15]];
//    self.levelImageView1.image = levelImage;
//    self.levelImageView2.image = count >= 2 ? levelImage : emptyImage;
//    self.levelImageView3.image = count >= 3 ? levelImage : emptyImage;
//    self.levelImageView4.image = count >= 4 ? levelImage : emptyImage;
//    self.levelImageView5.image = count >= 5 ? levelImage : emptyImage;
    
    NSString *current = [NSString stringWithFormat:@"%@_current", model._id];
    self.currentEffectLabel.text = [NSString stringWithFormat:SPDStringWithKey(current, nil), model.current_effect];
    NSString *next = [NSString stringWithFormat:@"%@_next", model._id];
    self.nextEffectLabel.text = [NSString stringWithFormat:SPDStringWithKey(next, nil), model.next_effect];
    
    CGFloat now_exp = model.now_exp.floatValue - model.level_exp.floatValue;
    CGFloat all_exp = model.all_exp.floatValue - model.level_exp.floatValue;
    [self.progressView setProgress:now_exp / all_exp animated:NO];
    self.progressLabel.text = [NSString stringWithFormat:@"%ld/%ld", model.now_exp.integerValue - model.level_exp.integerValue, model.all_exp.integerValue - model.level_exp.integerValue];
    
    self.upgradeMoneyLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"魔法升级：每次升级魔法需要消耗%@金币", nil), model.cost];
    self.magicalImpactLabel.text = model.info;
    self.AdvancedImpactLabel.text = model.level_up_info;
    self.propCountLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"拥有道具: %@", nil), model.num];
    self.upgrade1Btn.enabled = (magicLevel < 75);
    self.upgrade10Btn.enabled = (magicLevel < 75);
    self.upgrade100Btn.enabled = (magicLevel < 75);
}

- (IBAction)clickPropHelpBtn:(UIButton *)sender {
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    MagicPropHelpView *view = [[[NSBundle mainBundle] loadNibNamed:@"MagicPropHelpView" owner:self options:nil] firstObject];
    view.frame = window.bounds;
    [view.bgView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(25 + 17 + 20 + self.helpHeight + 20);
    }];
    view.contentLabel.text = self.propHelp;
    [window addSubview:view];
}

- (IBAction)clickUpgradeBtn:(UIButton *)sender {
//    NSInteger magic_level = self.model.magic_level.integerValue;
//    if (magic_level > 18 && magic_level % 10 == 9) {
//        NSInteger personal_level = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"%@%@", MYCURRENTLEVEL, [SPDApiUser currentUser].userId]];
//        if (personal_level < self.model.magic_level.integerValue - 9) {
//            [[self viewController] showToast:[NSString stringWithFormat:SPDStringWithKey(@"请将个人等级升至%ld等级, 再来升级", nil), self.model.magic_level.integerValue - 9]];
//            return;
//        }
//    }

//    if (self.model.num.integerValue) {
//        [self requestMagicUpgrade];
//    } else {
//        [[self viewController] presentCommonController:[NSString stringWithFormat:SPDStringWithKey(@"确定花费%@金币升级此魔法?", nil), self.model.cost] messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
//            [self requestMagicUpgrade];
//        }];
//    }
    NSInteger num = sender.tag - 1000;
    NSString *cost = [NSString stringWithFormat:@"%ld", self.model.cost.integerValue * num];
    [[self viewController] presentCommonController:[NSString stringWithFormat:SPDStringWithKey(@"确定花费%@金币升级此魔法?", nil), cost] messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
        [self requestMagicUpgradeWithNum:num];
    }];
    switch (num) {
        case 1:
            //[MobClick event:@"clickMagicUpgradeOne"];
            break;
        case 10:
            //[MobClick event:@"clickMagicUpgradeTen"];
            break;
        case 100:
            //[MobClick event:@"clickMagicUpgradeOnehundred"];
            break;
        default:
            break;
    }
}

- (void)requestMagicUpgradeWithNum:(NSInteger)num {
    self.upgrade1Btn.enabled = NO;
    self.upgrade10Btn.enabled = NO;
    self.upgrade100Btn.enabled = NO;
    BaseViewController *vc = [self viewController];
    
    NSDictionary *dic = @{@"_id": self.model._id, @"num": [NSString stringWithFormat:@"%ld", num]};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"my.magic.upgrade" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        if (![self.model.magic_level isEqualToNumber:suceess[@"level"]]) {
            [vc showToast:SPDStringWithKey(@"升级成功，继续加油～", nil)];
        }
        self.model.magic_level = suceess[@"level"];
        self.model.now_exp = suceess[@"exp"];
        self.model.level_exp = suceess[@"level_exp"];
        self.model.all_exp = suceess[@"level_exp_end"];
        self.model.num = suceess[@"num"];
        self.model.current_effect = suceess[@"current_effect"];
        self.model.next_effect = suceess[@"next_effect"];
        self.model = self.model;
        [self showUpgradeAnimationWithExp:suceess[@"upgrade_exp"] rate:suceess[@"crit_rate"]];
        self.upgrade1Btn.enabled = YES;
        self.upgrade10Btn.enabled = YES;
        self.upgrade100Btn.enabled = YES;
    } bFailure:^(id  _Nullable failure) {
        if ([failure isKindOfClass:[NSDictionary class]]) {
            if ([failure[@"code"] integerValue] == 2001) {
                [vc presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"充值", nil) confirmAction:^{
                    [vc pushToRechageController];
                }];
            } else if ([failure[@"code"] integerValue] == 2002) {
                [vc showToast:[NSString stringWithFormat:SPDStringWithKey(@"升级下一等级需要个人等级达到%@级", nil), failure[@"data"][@"personrank"]]];
            } else {
                [vc showToast:failure[@"msg"]];
            }
        }
        self.upgrade1Btn.enabled = YES;
        self.upgrade10Btn.enabled = YES;
        self.upgrade100Btn.enabled = YES;
    }];
}

- (void)showUpgradeAnimationWithExp:(NSNumber *)exp rate:(NSNumber *)rate {
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_magic_upgrade_bg"]];
    imageView.center = self.progressView.center;
    [self.contentView addSubview:imageView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:imageView.bounds];
    if (rate.integerValue > 1) {
        label.text = [NSString stringWithFormat:@"%@ X %@", exp, rate];
    } else {
        label.text = [NSString stringWithFormat:@"%@", exp];
    }
    label.textColor = [UIColor colorWithHexString:@"ffa508"];
    label.font = [UIFont systemFontOfSize:18];
    label.textAlignment = NSTextAlignmentCenter;
    [imageView addSubview:label];
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        imageView.center = CGPointMake(self.progressView.center.x, self.progressView.center.y - 50);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            imageView.center = CGPointMake(self.progressView.center.x, self.progressView.center.y - 90);
            imageView.alpha = 0;
        } completion:^(BOOL finished) {
            [imageView removeFromSuperview];
        }];
    }];
}

- (BaseViewController *)viewController {
    id object = [self nextResponder];
    while (![object isKindOfClass:[BaseViewController class]] && object != nil) {
        object = [object nextResponder];
    }
    return object;
}

@end
