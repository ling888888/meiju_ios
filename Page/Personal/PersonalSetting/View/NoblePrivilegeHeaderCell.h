//
//  NoblePrivilegeHeaderCell.h
//  SimpleDate
//

#import <UIKit/UIKit.h>

@class NobleModel;

@interface NoblePrivilegeHeaderCell : UICollectionViewCell

@property (nonatomic, strong) NobleModel *model;

@end
