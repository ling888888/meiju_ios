//
//  MagicPropHelpView.m
//  SimpleDate
//
//  Created by 李楠 on 2017/9/30.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "MagicPropHelpView.h"

@implementation MagicPropHelpView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.titleLabel.text = SPDStringWithKey(@"获取魔法道具方式", nil);
    [self animating];
}

- (IBAction)close:(UIButton *)sender {
    [self removeFromSuperview];
}

- (IBAction)tap:(UITapGestureRecognizer *)sender {
    [self removeFromSuperview];
}

- (void)animating {
    CAKeyframeAnimation * animation;
    animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = 0.30;
    animation.removedOnCompletion = YES;
    animation.fillMode = kCAFillModeForwards;
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1, 1.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    [_bgView.layer addAnimation:animation forKey:nil];
    
    [UIView animateWithDuration:0.30 animations:^{
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.25];
    }];
}

@end
