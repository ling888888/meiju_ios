//
//  NobleNameCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/9/11.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NobleModel;

@interface NobleNameCell : UICollectionViewCell

@property (nonatomic, strong) NobleModel *model;

@end
