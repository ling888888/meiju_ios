//
//  NobleNameCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/9/11.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "NobleNameCell.h"
#import "NobleModel.h"

@interface NobleNameCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIView *selectedView;

@end

@implementation NobleNameCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(NobleModel *)model {
    _model = model;
    
    self.nameLabel.text = _model.name;
    self.nameLabel.textColor = [UIColor colorWithHexString:_model.isSelected ? @"EBC075" : @"BBBBBB"];
    self.nameLabel.font = [UIFont boldSystemFontOfSize:_model.isSelected ? 16 : 14];
    self.selectedView.hidden = !_model.isSelected;
}

@end
