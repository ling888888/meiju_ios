//
//  SPDFamilySwitchCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/10/9.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDFamilySwitchCell.h"

@interface SPDFamilySwitchCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;

@end

@implementation SPDFamilySwitchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.titleLabel.text = SPDStringWithKey(@"管理权限", nil);
    self.descLabel.text = SPDStringWithKey(@"开启后，申请加入家族需要族长，副族长同意", nil);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickActionSwitch:(UISwitch *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickActionSwitch:)]) {
        [self.delegate didClickActionSwitch:sender];
    }
}

@end
