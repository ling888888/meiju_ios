//
//  SPDPersonalMagicCell.h
//  SimpleDate
//
//  Created by 李楠 on 2017/9/29.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SPDMagicModel;

@interface SPDPersonalMagicCell : UITableViewCell

@property (nonatomic, strong) SPDMagicModel *model;
@property (nonatomic, copy) NSString *propHelp;
@property (nonatomic, assign) CGFloat helpHeight;

@end
