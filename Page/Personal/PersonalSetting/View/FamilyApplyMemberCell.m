//
//  FamilyApplyMemberCell.m
//  SimpleDate
//
//  Created by Monkey on 2017/6/13.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "FamilyApplyMemberCell.h"


@interface FamilyApplyMemberCell ()
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *ageGenderBtn;
@property (weak, nonatomic) IBOutlet UIButton *refuseBtn;
@property (weak, nonatomic) IBOutlet UIButton *agreeBtn;

@end

@implementation FamilyApplyMemberCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.avatarImageView.layer.cornerRadius = 20;
    self.avatarImageView.clipsToBounds = YES;
    self.avatarImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAvatar)];
    [self.avatarImageView addGestureRecognizer:tap];
    
    if (kScreenH <= 568) {
        self.ageGenderBtn.titleLabel.font = [UIFont systemFontOfSize:10];
    }
    [self.agreeBtn setTitle:SPDStringWithKey(@"同意", nil) forState:UIControlStateNormal];
    [self.refuseBtn setTitle:SPDStringWithKey(@"拒绝", nil) forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)agreeBtnClicked:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(applyCellAgreeBtnClicked:)]) {
        [self.delegate applyCellAgreeBtnClicked:self.applyModel];
    }
}
- (IBAction)refuseBtnClicked:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(applyCellRefuseBtnClicked:)]) {
        [self.delegate applyCellRefuseBtnClicked:self.applyModel];
    }
}

-(void)setApplyModel:(SPDFamilyMemberModel *)applyModel{
    _applyModel = applyModel;
    
    [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:applyModel.avatar]];
    
    if (applyModel.nick_name.length >5) {
        self.nickNameLabel.text = [applyModel.nick_name substringToIndex:4];
    }else{
        self.nickNameLabel.text = applyModel.nick_name;
    }
    
    if ([applyModel.gender isEqualToString:@"female"]) {
        [self.ageGenderBtn setImage:[UIImage imageNamed:@"ic_family_female"] forState:UIControlStateNormal];
    }else{
        [self.ageGenderBtn setImage:[UIImage imageNamed:@"ic_family_male"] forState:UIControlStateNormal];
    }
    [self.ageGenderBtn setTitle:[NSString stringWithFormat:@"%@",applyModel.age] forState:UIControlStateNormal];
    [self.ageGenderBtn setImagePosition:HHImagePositionLeft spacing:5.0f];

}
-(void)tapAvatar {
    if (self.delegate && [self.delegate respondsToSelector:@selector(applyCellAvatarClicked:)]) {
        [self.delegate applyCellAvatarClicked:self.applyModel];
    }
}


@end
