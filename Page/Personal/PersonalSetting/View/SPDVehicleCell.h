//
//  SPDVehicleCell.h
//  SimpleDate
//
//  Created by 李楠 on 17/7/10.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SPDVehicleModel;

@protocol SPDVehicleCellDelegate <NSObject>

- (void)didClickBuyButton:(SPDVehicleModel *)model;
- (void)didClickEquipButton:(SPDVehicleModel *)model;
- (void)didClickPreviewButton:(SPDVehicleModel *)model;

@end

@interface SPDVehicleCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;
@property (weak, nonatomic) IBOutlet UIButton *equipButton;
@property (weak, nonatomic) IBOutlet UIImageView *isDriveImage;
@property (weak, nonatomic) IBOutlet UILabel *expireTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *validTimeBtn;
@property (weak, nonatomic) IBOutlet UIButton *priceButton;
@property (weak, nonatomic) IBOutlet UILabel *moreVehicleTip;
@property (weak, nonatomic) IBOutlet UIImageView *tagImageView;
@property (weak, nonatomic) IBOutlet UIButton *previewButton;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;

@property (nonatomic, assign) BOOL isMine;
@property (nonatomic, strong) SPDVehicleModel *model;
@property (nonatomic, weak) id<SPDVehicleCellDelegate> delegate;

@end
