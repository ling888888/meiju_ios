//
//  SPDFamilyTipsEditCell.m
//  SimpleDate
//
//  Created by Monkey on 2018/3/23.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDFamilyTipsEditCell.h"

@interface SPDFamilyTipsEditCell ()

@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;


@end

@implementation SPDFamilyTipsEditCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.textView.returnKeyType = UIReturnKeyDone;
    self.textView.contentInset = UIEdgeInsetsMake(8, 0, 0, 0);
}

- (void)setDict:(NSDictionary *)dict {
    _dict = dict;
    NSString * position = dict[@"position"];
    NSString * name = [position isEqualToString:@"chief"] ? @"edit_hi_icon" : @"ic_say_hi_icon";
    
    self.titleImageView.image = [UIImage imageNamed:name];
    self.textView.editable = [position isEqualToString:@"chief"];
    NSString * greeting = dict[@"clan"][@"greeting"];
    self.textView.text =  (greeting && ![greeting isEqualToString:@"默认欢迎语"]) ? greeting: SPDStringWithKey(@"欢迎来我们家族一起玩耍，有很多小伙伴在等着你喔～", nil);
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
}

@end
