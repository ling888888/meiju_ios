//
//  SPDCustomSliderView.m
//  SimpleDate
//
//  Created by 侯玲 on 2019/1/16.
//  Copyright © 2019年 WYB. All rights reserved.
//

#import "SPDCustomSliderView.h"

CGFloat const rightCustomImageViewHeight = 10.f;
CGFloat const rightCustomImageViewWidth = 10.f;

@interface SPDCustomSliderView()

@property (nonatomic, strong) UIImageView *bottomImageView;

@property (nonatomic, strong) UIImageView *topImageView;

@property (nonatomic, strong) UIImageView *rightCustomImageView;

@end

@implementation SPDCustomSliderView

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
        [self setupConstrain];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.bottomImageView];
    [self addSubview:self.topImageView];
    [self addSubview:self.rightCustomImageView];
}

- (void)setupConstrain {
    [self.bottomImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
        make.leading.trailing.equalTo(self);
        make.height.mas_equalTo(2.f);
    }];
    
    [self.topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.top.height.equalTo(self.bottomImageView);
    }];
    
    [self.rightCustomImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(rightCustomImageViewWidth, rightCustomImageViewHeight));
        make.leading.equalTo(self.topImageView.mas_trailing);
        make.centerY.equalTo(self);
    }];
}

- (void)updateSliderLengthWith:(CGFloat)sliderLength {
    sliderLength = sliderLength > self.frame.size.width ? self.frame.size.width : sliderLength;
    [self.topImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(sliderLength);
    }];
}

- (UIImageView *)bottomImageView {
    if (nil == _bottomImageView) {
        _bottomImageView = [[UIImageView alloc] init];
        _bottomImageView.backgroundColor = [UIColor colorWithHexString:@"B1AFB5"];
    }
    return _bottomImageView;
}

- (UIImageView *)topImageView {
    if (nil == _topImageView) {
        _topImageView = [[UIImageView alloc] init];
        _topImageView.backgroundColor = [UIColor colorWithHexString:@"#EBC075"];
    }
    return _topImageView;
}

- (UIImageView *)rightCustomImageView {
    if (nil == _rightCustomImageView) {
        _rightCustomImageView = [[UIImageView alloc] init];
        _rightCustomImageView.image = [UIImage imageNamed:@"noble_slider_icon"];
    }
    return _rightCustomImageView;
}

@end
