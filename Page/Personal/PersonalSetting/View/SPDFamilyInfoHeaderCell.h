//
//  SPDFamilyInfoHeaderCell.h
//  SimpleDate
//
//  Created by Monkey on 2018/3/22.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SPDFamilyInfoHeaderCellDelegate <NSObject>

@optional

- (void)familyHeaderCellClickEditInfoBtn;
- (void)familyHeaderCellIsFollowingDidChange:(BOOL)isFollowing;
- (void)familyInfoHeaderCellDidClickSignInButton;

@end

@interface SPDFamilyInfoHeaderCell : BaseTableViewCell

@property (nonatomic, strong)NSMutableDictionary * dict;

@property (nonatomic, weak)id<SPDFamilyInfoHeaderCellDelegate> delegate;

@property (nonatomic, assign) BOOL isFollowing;

@end
