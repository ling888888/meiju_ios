//
//  FamilyMemberHeaderCell.m
//  SimpleDate
//
//  Created by 李楠 on 2019/2/2.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "FamilyMemberHeaderCell.h"
#import "SPDFamilyMemberModel.h"

@interface FamilyMemberHeaderCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *genderAgeBtn;
@property (weak, nonatomic) IBOutlet UIButton *loginTimeBtn;

@end

@implementation FamilyMemberHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    if ([SPDCommonTool currentVersionIsArbic]) {
        [self.genderAgeBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        [self.genderAgeBtn setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 2.5)];
        [self.loginTimeBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 4, 0, 0)];
        [self.loginTimeBtn setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 2)];
    }
}

- (void)setModel:(SPDFamilyMemberModel *)model {
    _model = model;
    
    [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:_model.avatar]];
    self.nickNameLabel.text = _model.nick_name;
    if ([_model.gender isEqualToString:@"female"]) {
        [self.genderAgeBtn setBackgroundColor:[UIColor colorWithHexString:@"ff7eca"]];
        [self.genderAgeBtn setImage:[UIImage imageNamed:@"chatroomuser_female"] forState:UIControlStateNormal];
    } else if ([_model.gender isEqualToString:@"male"]) {
        [self.genderAgeBtn setBackgroundColor:[UIColor colorWithHexString:@"5db4fd"]];
        [self.genderAgeBtn setImage:[UIImage imageNamed:@"chatroomuser_male"] forState:UIControlStateNormal];
    }
    [self.genderAgeBtn setTitle:_model.age.stringValue forState:UIControlStateNormal];
    [self.loginTimeBtn setTitle:_model.last_login_time forState:UIControlStateNormal];
}

@end
