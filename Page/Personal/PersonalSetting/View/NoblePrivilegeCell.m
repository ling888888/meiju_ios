//
//  NoblePrivilegeCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/9/11.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "NoblePrivilegeCell.h"
#import "NoblePrivilegeModel.h"

@interface NoblePrivilegeCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end

@implementation NoblePrivilegeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(NoblePrivilegeModel *)model {
    _model = model;
    
    NSString *url = _model.isOwned ? _model.icon_active : _model.icon_inactive;
    [self.imageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, url]]];
    self.nameLabel.text = _model.name;
    self.nameLabel.textColor = _model.isOwned ? SPD_HEXCOlOR(@"EBC075"):SPD_HEXCOlOR(@"C8C8C8");
}

@end
