//
//  SPDFamilyInfoAvatarCell.m
//  SimpleDate
//
//  Created by Monkey on 2018/3/22.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDFamilyInfoAvatarCell.h"


@interface SPDFamilyInfoAvatarCell()
@property (weak, nonatomic) IBOutlet UIImageView *cicleImageView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;

@end

@implementation SPDFamilyInfoAvatarCell


- (void)awakeFromNib {
    [super awakeFromNib];
   
    self.avatarImageView.layer.cornerRadius = 27;
    self.avatarImageView.clipsToBounds = YES;
    
}

- (void)setMymodel:(SPDFamilyMemberModel *)mymodel {
    _mymodel = mymodel;                                          //   members chief cicle_members
    NSString * name = [NSString stringWithFormat:@"clan_%@_tag",mymodel.position];
    self.cicleImageView.image = [UIImage imageNamed:name];
    [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:mymodel.avatar]];
    if ([mymodel.position isEqualToString:@"chief"]) {
        self.avatarImageView.layer.borderColor = [UIColor colorWithHexString:@"ffcf28"].CGColor;
        self.avatarImageView.layer.borderWidth = 2;
    }else if ([mymodel.position isEqualToString:@"deputychief"]){
        self.avatarImageView.layer.borderColor = [UIColor colorWithHexString:@"c2c2c2"].CGColor;
        self.avatarImageView.layer.borderWidth = 2;
    }else if ([mymodel.position isEqualToString:@"member"]){
        self.avatarImageView.layer.borderColor = [UIColor colorWithHexString:@"decbc0"].CGColor;
        self.avatarImageView.layer.borderWidth = 2;
    }
}

@end
