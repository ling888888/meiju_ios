//
//  FamilyMemberHeaderCell.h
//  SimpleDate
//
//  Created by 李楠 on 2019/2/2.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SPDFamilyMemberModel;

NS_ASSUME_NONNULL_BEGIN

@interface FamilyMemberHeaderCell : UICollectionViewCell

@property (nonatomic, strong) SPDFamilyMemberModel *model;

@end

NS_ASSUME_NONNULL_END
