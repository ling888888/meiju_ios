//
//  FamilyMemberListCell.h
//  SimpleDate
//
//  Created by 李楠 on 2019/2/3.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SPDFamilyMemberModel;

@protocol FamilyMemberListCellDelegate <NSObject>

@optional

- (void)didClickActionWithModel:(SPDFamilyMemberModel *)model url:(NSString *)url;

@end

NS_ASSUME_NONNULL_BEGIN

@interface FamilyMemberListCell : UICollectionViewCell

@property (nonatomic, copy) NSString *position;
@property (nonatomic, strong) SPDFamilyMemberModel *model;
@property (nonatomic, weak) id<FamilyMemberListCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
