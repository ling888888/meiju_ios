//
//  FamilyApplyMemberCell.h
//  SimpleDate
//
//  Created by Monkey on 2017/6/13.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPDFamilyMemberModel.h"

@protocol FamilyApplyMemberCellDelegate <NSObject>

@optional
-(void)applyCellRefuseBtnClicked:(SPDFamilyMemberModel *)model;

-(void)applyCellAgreeBtnClicked:(SPDFamilyMemberModel *)model;

-(void)applyCellAvatarClicked:(SPDFamilyMemberModel *)model;

@end

@interface FamilyApplyMemberCell : BaseTableViewCell

@property (nonatomic,strong)SPDFamilyMemberModel * applyModel;

@property (nonatomic,weak)id<FamilyApplyMemberCellDelegate>delegate;

@end
