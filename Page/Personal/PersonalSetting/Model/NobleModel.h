//
//  NobleModel.h
//  SimpleDate
//
//  Created by 李楠 on 2018/9/12.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "CommonModel.h"

@interface NobleModel : CommonModel

@property (nonatomic, copy) NSString *_id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *icon_active;
@property (nonatomic, copy) NSString *icon_inactive;
@property (nonatomic, strong) NSArray *privileges;
@property (nonatomic, strong) NSMutableArray *privilegeArray;
@property (nonatomic, strong) NSNumber *active_price;
@property (nonatomic, strong) NSNumber *renew_price;
@property (nonatomic, assign) BOOL isOwned;
@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, assign) NSInteger noble_value; // 当前爵位的贵族值

@end
