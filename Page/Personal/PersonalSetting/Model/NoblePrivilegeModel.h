//
//  NoblePrivilegeModel.h
//  SimpleDate
//
//  Created by 李楠 on 2018/9/12.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "CommonModel.h"

@interface NoblePrivilegeModel : CommonModel

@property (nonatomic, copy) NSString *_id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *icon_active;
@property (nonatomic, copy) NSString *icon_inactive;
@property (nonatomic, assign) BOOL isOwned;

@end
