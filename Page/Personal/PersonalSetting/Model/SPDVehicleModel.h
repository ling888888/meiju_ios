//
//  SPDVehicleModel.h
//  SimpleDate
//
//  Created by 李楠 on 17/7/11.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPDVehicleModel : CommonModel

@property (nonatomic, copy) NSString *vehicle_id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *desc;
@property (nonatomic, copy) NSString *popdesc;// topbar描述
@property (nonatomic, copy) NSNumber *price;
@property (nonatomic, strong) NSNumber *grade;
@property (nonatomic, strong) NSNumber *expire_time;
@property (nonatomic, copy) NSString *expire_time_str;
@property (nonatomic, copy) NSString *valid_time;
@property (nonatomic, assign) BOOL is_drive;
@property (nonatomic, copy) NSNumber *type;
@property (nonatomic, copy) NSString *desc_color;
@property (nonatomic, copy) NSString * buy_type;

@end
