//
//  FamilyModel.m
//  SimpleDate
//
//  Created by Monkey on 2017/4/27.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "FamilyModel.h"

@implementation FamilyModel

- (void)setNick_name:(NSString *)nick_name {
    _nick_name = nick_name;
    
    self.name = nick_name;
}

- (void)setClan_info:(NSString *)clan_info {
    _clan_info = clan_info;
    
    self.desc = clan_info;
}

@end
