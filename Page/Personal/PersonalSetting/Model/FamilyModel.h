//
//  FamilyModel.h
//  SimpleDate
//
//  Created by Monkey on 2017/4/27.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FamilyModel : CommonModel

@property(nonatomic,copy)NSString *_id;

@property(nonatomic,copy)NSString *name;

@property(nonatomic,copy)NSString *desc;

@property(nonatomic,copy)NSString *avatar;

@property(nonatomic,strong)NSDictionary *chiefs;

@property(nonatomic,copy)NSNumber *limit;

@property(nonatomic,copy)NSNumber *members_num;

@property(nonatomic,assign)BOOL myclan;

@property(nonatomic,assign)BOOL is_review;

@property(nonatomic,copy)NSString *local_avatar;

@property(nonatomic,copy)NSString *nick_name;

@property(nonatomic,copy)NSString *clan_info;

@property (nonatomic, copy) NSNumber *liveness_num;
@property (nonatomic, copy) NSString *liveness_color;
@property (nonatomic, copy) NSString *lang;
@property (nonatomic, copy) NSString *flag;
@property (nonatomic, strong) NSArray *tasks;
@property (nonatomic, copy) NSString *agentServiceImage;
@property (nonatomic, assign) BOOL isStickClan;
@property (nonatomic, assign) BOOL isLock;
@property (nonatomic, copy) NSNumber *clanLevel;
@property (nonatomic, copy) NSString *nick_id;
@property (nonatomic, copy) NSNumber * onlineNum; //在线人数

@end
