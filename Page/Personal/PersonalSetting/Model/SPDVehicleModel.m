//
//  SPDVehicleModel.m
//  SimpleDate
//
//  Created by 李楠 on 17/7/11.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDVehicleModel.h"

@implementation SPDVehicleModel

- (void)setExpire_time:(NSNumber *)expire_time {
    _expire_time = expire_time;
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setTimeZone:[NSTimeZone localTimeZone]];
    [format setDateFormat:@"yyyy/MM/dd"];
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:[expire_time longLongValue] / 1000];
    self.expire_time_str = [NSString stringWithFormat:SPDStringWithKey(@"%@到期", nil), [format stringFromDate:date]];
}

@end
