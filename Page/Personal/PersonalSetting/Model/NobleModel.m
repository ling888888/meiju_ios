//
//  NobleModel.m
//  SimpleDate
//
//  Created by 李楠 on 2018/9/12.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "NobleModel.h"

@implementation NobleModel

- (NSMutableArray *)privilegeArray {
    if (!_privilegeArray) {
        _privilegeArray = [NSMutableArray array];
    }
    return _privilegeArray;
}

@end
