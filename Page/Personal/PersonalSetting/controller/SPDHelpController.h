//
//  SPDHelpController.h
//  SimpleDate
//
//  Created by Monkey on 2017/4/19.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "BaseViewController.h"

/***
 
 可以加载一些网页
 
***/

@interface SPDHelpController : BaseViewController

@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *titleText;
@property (nonatomic, copy) NSArray * listArray;

@end
