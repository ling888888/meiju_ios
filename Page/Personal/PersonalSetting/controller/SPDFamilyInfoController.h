//
//  SPDFamilyInfoController.h
//  SimpleDate
//
//  Created by Monkey on 2018/3/22.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "BaseViewController.h"

@protocol SPDFamilyInfoControllerDelegate <NSObject>

@optional

- (void)isFollowingDidChange:(BOOL)isFollowing;

@end

@interface SPDFamilyInfoController : BaseViewController

@property (nonatomic,copy) NSMutableDictionary * infoDict;

@property (nonatomic,copy) NSDictionary * shareDict; //分享

@property (nonatomic,copy) NSString * clan_id;

@property (nonatomic, weak) id<SPDFamilyInfoControllerDelegate> delegate;

@end
