//
//  SPDFamilyInfoController.m
//  SimpleDate
//
//  Created by Monkey on 2018/3/22.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDFamilyInfoController.h"
#import "ZTWAInfoShareView.h"
#import "SPDFamilyMemberModel.h"
#import "SPDFamilyInfoHeaderCell.h"
#import "SPDFamilyInfoCommonCell.h"
#import "SPDUMShareUtils.h"
#import "SPDFamilyTipsEditCell.h"
#import "SPDFamilyApplyController.h"
#import "SPDEditClanViewController.h"
#import "SPDKeywordMatcher.h"
#import "SPDFamilySwitchCell.h"
#import "FamilyMemberViewController.h"
#import "AppManager.h"

@interface SPDFamilyInfoController ()<UITableViewDelegate,UITableViewDataSource,ZTWAInfoShareViewDelegate,SPDFamilyInfoCommonCellDelegate,UITextViewDelegate,SPDFamilyInfoHeaderCellDelegate, SPDFamilySwitchCellDelegate>

@property (weak, nonatomic) IBOutlet UIButton *inviteBtn;

@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (weak, nonatomic) IBOutlet UIButton *joinBtn;
@property (weak, nonatomic) IBOutlet UIButton *contactBtn;
@property (nonatomic, strong) ZTWAInfoShareView *shareView;
@property (nonatomic, strong) NSMutableArray *familyMembersArrays;
@property (nonatomic, strong) NSMutableArray *deputy_chiefsArray;
@property (nonatomic, strong) SPDFamilyMemberModel *chiefModel;
@property (nonatomic, strong) NSMutableArray * applysArray;
@property (nonatomic, assign) NSInteger limit;
@property (nonatomic, copy)  NSString * position ;
@property (nonatomic, strong) SPDKeywordMatcher *keywordMatcher;
@property (nonatomic, strong) KeywordMap *keywordMap;
@property (nonatomic, strong) NSMutableDictionary *wordDict;
@property (nonatomic, assign) BOOL editGreeting   ;
@property (nonatomic, assign) NSInteger dissolve_cost ;//解散家族需要多少钱
@property (nonatomic, assign) BOOL applySwitch;
@property (nonatomic, assign) BOOL isFollowing;

@end

@implementation SPDFamilyInfoController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = SPDStringWithKey(@"家族详情", nil);
    
    if (@available(iOS 11.0, *)) {
        self.myTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [self configNavigationBar];
    
    [self.contactBtn setTitle:SPDStringWithKey(@"联系族长", nil) forState:UIControlStateNormal];
    [self.inviteBtn setTitle:SPDStringWithKey(@"邀请好友", nil) forState:UIControlStateNormal];
    self.joinBtn.layer.cornerRadius = 16;
    self.joinBtn.clipsToBounds = YES;
    self.contactBtn.layer.cornerRadius = 16;
    self.contactBtn.clipsToBounds = YES;
    self.inviteBtn.layer.cornerRadius = 16;
    self.inviteBtn.clipsToBounds = YES;
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    [self.myTableView registerNib:[UINib nibWithNibName:@"SPDFamilySwitchCell" bundle:nil] forCellReuseIdentifier:@"SPDFamilySwitchCell"];
    
    self.dissolve_cost = 0;
    
    [self handleClanData:self.infoDict];

  
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    [self requestClanDetailData];
    
}

- (void)configNavigationBar {
    UIBarButtonItem *left = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ic_arrow_left"].adaptiveRtl style:UIBarButtonItemStylePlain target:self action:@selector(clickBackBtn)];
    self.navigationItem.leftBarButtonItem = left;
}

- (void)clickBackBtn {
    if (self.editGreeting) {
        [self presentCommonController:SPDStringWithKey(@"确定保存编辑的内容吗？", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
            [self.navigationController popViewControllerAnimated:YES];
        } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
            [self uploadClanGreetingTitleToSever];
        }];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)handleClanData: (NSMutableDictionary *)dict{
    //先整理数据
    self.position = dict[@"position"];
    self.clan_id = dict[@"clan"][@"_id"];
    self.dissolve_cost = [dict[@"clan"][@"dissolve_cost"] integerValue];
    NSString * title = ![self.position isEqualToString:@"tourist"]?@"退出家族":@"加入家族";
    if([self.position isEqualToString:@"chief"]){ title = @"解散家族" ;}
    [self.joinBtn setTitle:SPDStringWithKey(title, nil) forState:UIControlStateNormal];

    self.chiefModel = [SPDFamilyMemberModel initWithDictionary:dict[@"clan"][@"chief"]];
    [self.chiefModel setPosition:@"chief"];
    
    [self.deputy_chiefsArray removeAllObjects];
    [self.applysArray removeAllObjects];
    [self.familyMembersArrays removeAllObjects];
    NSArray * deputy_chief = dict[@"clan"][@"deputy_chiefs"];
    for (NSDictionary * dic in   deputy_chief) {
        SPDFamilyMemberModel *model = [SPDFamilyMemberModel initWithDictionary:dic];
        [model setPosition:@"deputychief"];
        [self.deputy_chiefsArray  addObject:model];
    }
    
    NSArray * members = dict[@"clan"][@"members"];
    for (NSDictionary * dic in   members) {
        SPDFamilyMemberModel *model = [SPDFamilyMemberModel initWithDictionary:dic];
        [model setPosition:@"member"];
        [self.familyMembersArrays  addObject:model];
    }
    
    NSArray * applys = dict[@"clan"][@"apply"];
    for (NSDictionary * dic in applys) {
        SPDFamilyMemberModel *model = [SPDFamilyMemberModel initWithDictionary:dic];
        [model setPosition:@"tourist"];
        [self.applysArray  addObject:model] ;
    }
    
    self.limit = [dict[@"clan"][@"limit"] integerValue];
    self.applySwitch = [dict[@"clan"][@"apply_switch"] boolValue];
    self.isFollowing = [dict[@"clan"][@"is_following"] boolValue];
    
    [self requestUserInfoData];
    
    [self.myTableView reloadData];
    
}

-(void)requestUserInfoData{
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.info.detail" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        NSLog(@"请求个人资料成功");
        NSDictionary *myInfoDict = [NSDictionary dictionaryWithDictionary:suceess];
        //给分享弹出框邀请码赋值！
//        self.shareView.invite_code=myInfoDict[@"invite_code"];
        self.shareDict = myInfoDict [@"share"];
        self.shareView.shareDict = myInfoDict[@"share"];
        
    } bFailure:nil];
    
}


#pragma  mark table

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        SPDFamilyInfoHeaderCell * cell = [SPDFamilyInfoHeaderCell cellWithTableView:tableView];
        cell.dict = self.infoDict;
        cell.delegate = self;
        cell.isFollowing = self.isFollowing;
        return cell;
    }else if (indexPath.section == 1){
        SPDFamilyTipsEditCell * cell = [SPDFamilyTipsEditCell cellWithTableView:tableView];
        cell.dict = self.infoDict;
        [cell.textView setDelegate:self];
        cell.selected = NO;
        return cell;
    }else if (indexPath.section == 2){
        SPDFamilyInfoCommonCell * cell = [SPDFamilyInfoCommonCell cellWithTableView:tableView];
        cell.titleLable.text = [NSString stringWithFormat:@"%@(%@/%@)",SPDStringWithKey(@"家族成员", nil),@(self.familyMembersArrays.count+self.deputy_chiefsArray.count+1),@(self.limit)];
        cell.type = @"member";
        cell.dataArray = [NSMutableArray array];
        [cell.dataArray addObject:self.chiefModel];
        [cell.dataArray addObjectsFromArray:self.deputy_chiefsArray];
        [cell.dataArray addObjectsFromArray:self.familyMembersArrays];
        cell.delegate = self;
        return cell;

    } else if (indexPath.section == 3) {
        SPDFamilyInfoCommonCell * cell = [SPDFamilyInfoCommonCell cellWithTableView:tableView];
        cell.type = @"apply";
        cell.dataArray = self.applysArray;
        cell.titleLable.text = [NSString stringWithFormat:@"%@(%@)",SPDStringWithKey(@"申请列表", nil),@(self.applysArray.count)];
        cell.delegate = self;
        return cell;

    } else {
        SPDFamilySwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SPDFamilySwitchCell" forIndexPath:indexPath];
        cell.actionSwitch.on = self.applySwitch;
        cell.delegate = self;
        return cell;
    }
    return [[UITableViewCell alloc]init];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.position isEqualToString:@"chief"] || [self.position isEqualToString:@"deputychief"] ? 5 : 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 8;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            return 283;
            break;
        case 1:
           return UITableViewAutomaticDimension;
            break;
        case 2:
          return  120;
            break;
        case 3:
          return  120;
            break;
        case 4:
            return 70;
            break;
        default:
            return  120;
            break;
    }
}

- (IBAction)joinBtnClicked:(id)sender {
    if ([self.position isEqualToString:@"tourist"]) {
        
        [self presentCommonController:SPDStringWithKey(@"确定申请加入该家族吗?", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
            
        } confirmTitle:SPDStringWithKey(@"确认", nil) confirmAction:^{
            [self applyJoinClan];
        }];
    }else if([self.position isEqualToString:@"member"] || [self.position isEqualToString:@"deputychief"]){
        [self presentCommonController:SPDStringWithKey(@"确定退出本家族吗?", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
        } confirmTitle:SPDStringWithKey(@"确认", nil)confirmAction:^{
            [self exictClan];
        }];
    }else if ([self.position isEqualToString:@"chief"]){
        if(self.familyMembersArrays.count > 0){
            [self showMessageToast:SPDStringWithKey(@"清除所有成员才可解散家族", nil)];
        }else{
            [self presentCommonController:[NSString stringWithFormat:SPDStringWithKey(@"确定花费%@金币解散家族", nil),@(self.dissolve_cost)] messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
            } confirmTitle:SPDStringWithKey(@"确认", nil)confirmAction:^{
                [self deleteClan];
            }];
        }
    }
}

-(void)applyJoinClan{
    
    NSDictionary *dict =@{@"user_id":[SPDApiUser currentUser].userId,
                          @"clan_id":self.clan_id };
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dict] bURL:@"apply" isClan:YES bAnimated:YES bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        [self showMessageToast:SPDStringWithKey(@"申请成功", nil)];
    } bFailure:^(id  _Nullable failure) {
        [self showToast:failure[@"msg"]];
    }];
    
}

-(void)exictClan {
    NSDictionary *dict =@{@"user_id":[SPDApiUser currentUser].userId,
                          @"clan_id":self.clan_id};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dict] bURL:@"exit" isClan:YES bAnimated:YES bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        [self showMessageToast:SPDStringWithKey(@"退出成功", nil)];
        [self requestClanDetailData];
    } bFailure:^(id  _Nullable failure) {
        [self showToast:failure[@"msg"]];
    }];
}

-(void)deleteClan {
    NSDictionary *dict =@{@"user_id":[SPDApiUser currentUser].userId,
                          @"clan_id":self.clan_id};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dict] bURL:@"dissolve" isClan:YES bAnimated:YES bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        [self showMessageToast:SPDStringWithKey(@"解散家族成功", nil)];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popToRootViewControllerAnimated:YES];
        });
    } bFailure:^(id  _Nullable failure) {
        if ([failure[@"code"]integerValue] == 203) {
            [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
            } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                [self pushToRechageController];
            }];
        }else{
            [self showToast:failure[@"msg"]];
        }
    }];
}


- (IBAction)contactBtnClicked:(id)sender {
    [self pushToDetailTableViewController:self userId:self.chiefModel.user_id];
}

- (IBAction)inviteBtnClicked:(id)sender {
    [self.shareView show];
}

- (void)clickedInviteCount {
    NSLog(@"点击了click");
    NSString * str = [NSString stringWithFormat:INVITE_DETAIL,[SPDApiUser currentUser].userId,[SPDCommonTool getFamyLanguage]];
    [self loadWebViewController:self title:SPDStringWithKey(@"邀请", nil) url:str];
}

- (void)infoShareView:(ZTWAInfoShareView *)infoShareView shareToPlatformType:(SPDUMShareType)sharePlatformType {
    
    NSString *shareUrl=self.shareDict[@"share_url"];
    NSString *sharetitle=self.shareDict[@"title"];
    NSString *shareContent=self.shareDict[@"content"];
    NSData *data=[[NSData alloc]initWithContentsOfURL:[NSURL URLWithString:self.shareDict[@"thumbnail"]]];
    UIImage *image=[[UIImage alloc] initWithData:data];
    
//    [[SPDUMShareUtils shareInstance] shareToPlatform:sharePlatformType WithShareUrl:shareUrl andshareTitle:sharetitle andshareContent:shareContent andImageData:image andShareSourece:@"clan" andSPDUMShareSuccess:^(id shareSuccess) {
//        
//        NSLog(@"分享成功～");
//        [SPDCommonTool showWindowToast:SPDStringWithKey(@"分享成功", nil)];
//        [self.shareView dismiss];
//        
//    } presentedController:self andSPDUMShareFailure:^(id shareFailure) {
//        
//        
//    }];
    
}

#pragma mark - SPDFamilySwitchCellDelegate

- (void)didClickActionSwitch:(UISwitch *)sender {
    NSString *title = SPDStringWithKey(sender.on ? @"开启后，用户申请加入家族需要族长或副族长同意才可成为家族成员" : @"关闭后，用户申请加入家族后可自动成为家族成员", nil);
    [self presentCommonController:title messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
        [sender setOn:!sender.on animated:YES];
    } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
        [self requestApplySwitchWithSwitch:sender];
    }];
}

- (void)requestApplySwitchWithSwitch:(UISwitch *)sender {
    NSDictionary *dic = @{@"user_id": [SPDApiUser currentUser].userId, @"clan_id": self.clan_id, @"switch": sender.on ? @"on" : @"off"};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"apply.switch" isClan:YES bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.applySwitch = sender.on;
        [self showToast:SPDStringWithKey(sender.on ? @"开启成功" : @"关闭成功", nil)];
    } bFailure:^(id  _Nullable failure) {
        [sender setOn:!sender.on animated:YES];
        if ([failure isKindOfClass:[NSDictionary class]]) {
            [self showToast:failure[@"msg"]];
        }
    }];
}

#pragma mark cell delegate

- (void)clickedUserAvatarWithModel: (SPDFamilyMemberModel *)model{
    [self pushToDetailTableViewController:self userId:model.user_id];

}

- (void)familyHeaderCellClickEditInfoBtn {
    SPDEditClanViewController *vc = [[SPDEditClanViewController alloc] init];
    vc.clan_id = self.clan_id;
    vc.clanInfo = self.infoDict[@"clan"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)familyHeaderCellIsFollowingDidChange:(BOOL)isFollowing {
    self.isFollowing = isFollowing;
    if ([self.delegate respondsToSelector:@selector(isFollowingDidChange:)]) {
        [self.delegate isFollowingDidChange:isFollowing];
    }
}

- (void)familyInfoHeaderCellDidClickSignInButton {
    if ([self.position isEqualToString:@"tourist"]) {
        [self showToast:SPDStringWithKey(@"无家族请创建／加入家族后签到领金币，有家族请回到自己家族签到领金币", nil)];
    } else {
        [self loadWebViewController:self title:@"" url:[NSString stringWithFormat:CLAN_SIGNIN_URL, [SPDApiUser currentUser].lang, [SPDApiUser currentUser].userId, self.clan_id, [[AppManager sharedInstance] getDeviceId]]];
    }
}

- (void)clickedViewAllWithType: (NSString  *)type {
    if ([type isEqualToString:@"member"]) {
        FamilyMemberViewController *vc = [[FamilyMemberViewController alloc] init];
        vc.clanInfo = self.infoDict;
        [self.navigationController pushViewController:vc animated:YES];
    }else if([type isEqualToString:@"apply"] && self.applysArray.count !=0){
        SPDFamilyApplyController *  apply =  [[SPDFamilyApplyController alloc]init];
        apply.dataArray = self.applysArray;
        apply.clan_id = self.clan_id;
        [self.navigationController pushViewController:apply animated:YES];
    }
}

-(void)requestClanDetailData {
    
    if (!self.clan_id) {
        return;
    }
    NSDictionary * dict = @{
                            @"user_id":[SPDApiUser currentUser].userId,
                            @"clan_id":self.clan_id
                            };
    
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dict] bURL:@"detail" isClan:YES bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        
        self.infoDict = suceess;
        [self handleClanData:suceess];
        
    } bFailure:^(id  _Nullable failure) {
        
    }];
    
}

#pragma mark text view

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]){ //判断输入的字是否是回车，即按下return
            [self presentCommonController:SPDStringWithKey(@"确定保存编辑的内容吗？", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
        
            } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                [self uploadClanGreetingTitleToSever];
            }];
        return NO; //这里返回NO，就代表return键值失效，即页面上按下return，不会出现换行，如果为yes，则输入页面会换行
    }
    
    return YES;
}



-(void)textViewDidChange:(UITextView *)textView{
    CGSize size = [textView sizeThatFits:CGSizeMake(CGRectGetWidth(self.view.frame), CGFLOAT_MAX)];
    CGFloat height = size.height;
    [self.myTableView beginUpdates];
    SPDFamilyTipsEditCell * cell = [self.myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    if (height < 49) {
        cell.height.constant = 49+10;
    }else{
        cell.height.constant = height+10;
    }
    [self.myTableView endUpdates];

     self.editGreeting = YES;
}

- (void)uploadClanGreetingTitleToSever {
    
    SPDFamilyTipsEditCell * cell = [self.myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    NSString * greeting = cell.textView.text;
//    [self showToast:SPDStringWithKey(@"请输入家族宣言", nil)];
    if ([SPDCommonTool isEmpty:greeting]) {
        [self showToast:SPDStringWithKey(@"欢迎语不能为空", nil)];
        return;
    }
    // 敏感度
    if ([self.keywordMatcher match:greeting withKeywordMap:self.keywordMap]) {
        [self showMessageToast:SPDStringWithKey(@"家族名称/宣言/欢迎语有敏感词，请重新输入", nil)];
        return;
    }
    //判断长度
    if ([[SPDCommonTool getFamyLanguage] containsString:@"zh"]) {
        if (greeting.length > 40) { [self showMessageToast:SPDStringWithKey(@"请输入家族欢迎语(60个字以内)", nil)];return;}
    }else{
        if (greeting.length > 60) {[self showMessageToast:SPDStringWithKey(@"请输入家族欢迎语(60个字以内)", nil)];return;}
    }
    
    NSMutableDictionary * dict = [NSMutableDictionary new];
    [dict setObject:greeting forKey:@"greeting"];
    [dict setObject:[SPDApiUser currentUser].userId forKey:@"user_id"];
    [dict setObject:self.clan_id forKey:@"clan_id"];
    
    [RequestUtils commonPostRequestUtils:dict bURL:@"greeting" isClan:YES bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.editGreeting = NO;
        [self showToast:SPDStringWithKey(@"保存成功", nil)];
        SPDFamilyTipsEditCell * cell = [self.myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
        [cell.textView resignFirstResponder];
    } bFailure:^(id  _Nullable failure) {
        if ([failure isKindOfClass:[NSDictionary class]]) {
            [self showToast:failure[@"msg"]];
        }
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
        SPDFamilyTipsEditCell * cell = [self.myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
        [cell.textView resignFirstResponder];
}


-(ZTWAInfoShareView *)shareView{
    if (!_shareView) {
        _shareView=[ZTWAInfoShareView shareView];
        NSNumber *invite_count = [[NSUserDefaults standardUserDefaults] objectForKey:MYINVITECOUNT] ;
        if (invite_count) {
            _shareView.invite_count = [invite_count intValue];
        }else{
//          _shareView.invite_count =  [self requestClanInviteCountAPI];
        }
        _shareView.delegate = self;
    }
    return _shareView;
}

-(NSMutableArray *)familyMembersArrays{
    if (!_familyMembersArrays) {
        _familyMembersArrays = [NSMutableArray array];
    }
    return _familyMembersArrays;
}

-(NSMutableArray *)deputy_chiefsArray{
    if (!_deputy_chiefsArray) {
        _deputy_chiefsArray = [NSMutableArray array];
    }
    return _deputy_chiefsArray;
}

-(NSMutableArray *)applysArray{
    if (!_applysArray) {
        _applysArray = [NSMutableArray array];
    }
    return _applysArray;
}

- (SPDKeywordMatcher *)keywordMatcher {
    if (!_keywordMatcher) {
        _keywordMatcher = [[SPDKeywordMatcher alloc]init];
    }
    return _keywordMatcher;
}

- (KeywordMap *)keywordMap {
    if (!_keywordMap) {
        _keywordMap = [self.keywordMatcher convert:[self.wordDict allKeys]];
    }
    return _keywordMap;
}

- (NSMutableDictionary *)wordDict {
    if (!_wordDict) {
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"word" ofType:@"plist"];
        _wordDict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    }
    return _wordDict;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
