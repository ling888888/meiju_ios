//
//  SPDMagicViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2017/9/29.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDMagicViewController.h"
#import "SPDPersonalMagicCell.h"
#import "SPDMagicModel.h"
#import "SPDHelpController.h"

@interface SPDMagicViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *magicArray;
@property (nonatomic, copy) NSString *propHelp;
@property (nonatomic, assign) CGFloat helpHeight;

@end

@implementation SPDMagicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = SPDStringWithKey(@"我的魔法", nil);
    [self.navigationController.navigationBar vhl_setBackgroundColor:[UIColor whiteColor]];
    UIBarButtonItem *helpBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"family_help"] style:UIBarButtonItemStylePlain target:self action:@selector(clickHelpBtn)];
    self.navigationItem.rightBarButtonItem = helpBtn;
    
    [self.view addSubview:self.tableView];
    [self requestData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)clickHelpBtn {
    SPDHelpController *vc = [[SPDHelpController alloc] init];
    vc.url = [NSString stringWithFormat:@"https://clan.famy.ly/magic_helper.html?lang=%@", [SPDCommonTool getFamyLanguage]];
    vc.titleText = SPDStringWithKey(@"魔法等级帮助", nil);
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)requestData {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.magic.list" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        for (NSDictionary *dic in suceess[@"magics"]) {
            SPDMagicModel *model = [SPDMagicModel initWithDictionary:dic];
            [self.magicArray addObject:model];
        }
        self.propHelp = suceess[@"magic_help"];
        [self.tableView reloadData];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.magicArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SPDPersonalMagicCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SPDPersonalMagicCell" forIndexPath:indexPath];
    cell.model = self.magicArray[indexPath.row];
    cell.propHelp = self.propHelp;
    cell.helpHeight = self.helpHeight;
    return cell;
}

#pragma mark - setter / getter

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH) style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.rowHeight = 372;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [UIView new];
        [_tableView registerNib:[UINib nibWithNibName:@"SPDPersonalMagicCell" bundle:nil] forCellReuseIdentifier:@"SPDPersonalMagicCell"];
    }
    return _tableView;
}

- (NSMutableArray *)magicArray {
    if (!_magicArray) {
        _magicArray = [NSMutableArray array];
    }
    return _magicArray;
}

- (void)setPropHelp:(NSString *)propHelp {
    if (propHelp && ![propHelp isEqual:[NSNull null]]) {
        _propHelp = propHelp;
        if (propHelp.length == 0) {
            return;
        }
        self.helpHeight =  [_propHelp boundingRectWithSize:CGSizeMake(238, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:12]} context:nil].size.height;
    }
}

@end
