//
//  SPDFamilyApplyController.m
//  SimpleDate
//
//  Created by Monkey on 2018/3/26.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SPDFamilyApplyController.h"
#import "FamilyApplyMemberCell.h"

@interface SPDFamilyApplyController ()<UITableViewDelegate,UITableViewDataSource,FamilyApplyMemberCellDelegate>

@property (nonatomic, strong) UITableView * myTableView;

@end

@implementation SPDFamilyApplyController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = SPDStringWithKey(@"申请列表", nil);
    [self.view addSubview:self.myTableView];

    //确定要清空
    UIButton *clearBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    clearBtn.frame = CGRectMake(0, 0, 25, 20);
    [clearBtn setImage:[UIImage imageNamed:@"clear_message"] forState:UIControlStateNormal];
    [clearBtn addTarget:self action:@selector(handleClear) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *clearItem = [[UIBarButtonItem alloc]initWithCustomView:clearBtn];
    self.navigationItem.rightBarButtonItem = clearItem;
    
}

- (void)handleClear {
    if (self.dataArray.count <= 0) {
        return;
    }
    [self presentAlertController:SPDStringWithKey(@"确定要清空申请列表吗？", nil) cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
        
    } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
        [self.dataArray removeAllObjects];
        [self.myTableView reloadData];
    }];
   
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SPDFamilyMemberModel * applys = [self.dataArray objectAtIndex:indexPath.row];
    FamilyApplyMemberCell *applyCell = [FamilyApplyMemberCell cellWithTableView:tableView];
    applyCell.applyModel = applys;
    applyCell.delegate = self;
    return applyCell;
}

#pragma mark -- cell delegate

-(void)applyCellRefuseBtnClicked:(SPDFamilyMemberModel *)model {
    
    [self presentCommonController:SPDStringWithKey(@"确定拒绝该用户申请吗?", nil) messageString:@" " cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
        
    } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
      
        [self chiefUser:model WithUrl:@"apply.refuse"];
    }];
}

-(void)applyCellAgreeBtnClicked:(SPDFamilyMemberModel *)model {
    [self presentCommonController:SPDStringWithKey(@"确定同意该用户申请吗?", nil) messageString:@" " cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
        
    } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
        [self chiefUser:model WithUrl:@"apply.agree"];
    }];
}

-(void)applyCellAvatarClicked:(SPDFamilyMemberModel *)model {
    [self pushToDetailTableViewController:self userId:model.user_id];
}

-(void)chiefUser:(SPDFamilyMemberModel  *)model WithUrl:(NSString *)url{
    
    NSMutableDictionary * dict = [NSMutableDictionary new];
    [dict setValue:[SPDApiUser currentUser].userId forKey:@"user_id"];
    [dict setValue:model.user_id forKey:@"apply_user_id"];
    [dict setValue:self.clan_id forKey:@"clan_id"];

    [RequestUtils commonPostRequestUtils:dict bURL:url isClan:YES bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        
        if ([url isEqualToString:@"kick"]) {
            [self showToast:SPDStringWithKey(@"踢出成功", nil)];
        }else if ([url isEqualToString:@"promotion"]){
            [self showMessageToast:SPDStringWithKey(@"晋升成功", nil)];
        }else if ([url isEqualToString:@"demote"]){
            [self showMessageToast:SPDStringWithKey(@"降级成功", nil)];
        }else if ([url isEqualToString:@"apply.refuse"]) {
            [self showMessageToast:SPDStringWithKey(@"拒绝成功", nil)];
        }else if ([url isEqualToString:@"apply.agree"]){
            [self showMessageToast:SPDStringWithKey(@"同意成功", nil)];
        }
        [self.dataArray removeObject:model];
        [self.myTableView reloadData];
        
    } bFailure:^(id  _Nullable failure) {
        [self showToast:failure[@"msg"]];
    }];
}

- (UITableView *)myTableView {
    if (!_myTableView) {
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, NavHeight, kScreenW, kScreenH-NavHeight)];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.tableFooterView = [[UIView alloc]init];
    }
    return _myTableView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
