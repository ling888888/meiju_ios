//
//  NobleBillListViewController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/6/2.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "NobleBillListViewController.h"
#import "BillModel.h"
#import "BillCell.h"

@interface NobleBillListViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView * goldTableView;
@property (nonatomic, assign) NSInteger goldPage;
@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation NobleBillListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"贵族值明细".localized;
    [self.view addSubview:self.goldTableView];
    [self requestBillList];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:1];
    [self.navigationController.navigationBar vhl_setBackgroundColor:SPD_HEXCOlOR(@"ffffff")];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
}


- (void)requestBillList {
    NSDictionary *dic = @{@"page": @(_goldPage)};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"noble.bill.list" bAnimated:NO  bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if (_goldPage == 0) {
            [self.dataArray removeAllObjects];
        }
        NSArray *bills = suceess[@"bills"];
        for (NSDictionary *dic in bills) {
            if (![dic[@"status"] isEqualToString:@"process"]) {
                BillModel *model = [BillModel initWithDictionary:dic];
                [self.dataArray addObject:model];
            }
        }
        [self.goldTableView reloadData];
        self.goldPage++;
        
        self.goldTableView.mj_header.hidden = NO;
        [self.goldTableView.mj_header endRefreshing];
        if (self.goldTableView.mj_footer.state == MJRefreshStateRefreshing) {
            [self.goldTableView.mj_footer endRefreshingWithCompletionBlock:^{
                self.goldTableView.mj_footer.hidden = !bills.count;
            }];
        } else {
            self.goldTableView.mj_footer.hidden = !bills.count;
        }
    } bFailure:^(id  _Nullable failure) {
        self.goldTableView.mj_header.hidden = NO;
        [self.goldTableView.mj_header endRefreshing];
        [self.goldTableView.mj_footer endRefreshing];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BillCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BillCell" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 66;
}

- (UITableView *)goldTableView {
    if (!_goldTableView) {
        _goldTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _goldTableView.dataSource = self;
        _goldTableView.rowHeight = 66;
        _goldTableView.tableFooterView = [UIView new];
        [_goldTableView registerNib:[UINib nibWithNibName:@"BillCell" bundle:nil] forCellReuseIdentifier:@"BillCell"];
        
        __weak typeof(self) weakSelf = self;
        _goldTableView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            weakSelf.goldPage = 0;
            [weakSelf requestBillList];
        }];
        _goldTableView.mj_header.hidden = YES;
        _goldTableView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            [weakSelf requestBillList];
        }];
        _goldTableView.mj_footer.hidden = YES;
    }
    return _goldTableView;
}
- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

@end
