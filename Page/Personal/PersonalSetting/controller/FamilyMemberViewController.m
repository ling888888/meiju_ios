//
//  FamilyMemberViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2019/2/2.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "FamilyMemberViewController.h"
#import "SPDFamilyMemberModel.h"
#import "FamilyMemberHeaderCell.h"
#import "FamilyMemberTitleCell.h"
#import "FamilyMemberListCell.h"

@interface FamilyMemberViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, FamilyMemberListCellDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, copy) NSString *position;
@property (nonatomic, copy) NSString *clanId;
@property (nonatomic, strong) SPDFamilyMemberModel *chiefModel;
@property (nonatomic, assign) NSInteger deputyChiefLimit;
@property (nonatomic, strong) NSMutableArray *deputyChiefArr;
@property (nonatomic, assign) NSInteger memberLimit;
@property (nonatomic, strong) NSMutableArray *memberArr;

@end

@implementation FamilyMemberViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = SPDStringWithKey(@"家族详情", nil);
    self.view.backgroundColor = [UIColor colorWithHexString:COMMON_PINK];
    [self.view addSubview:self.collectionView];
    if (@available(iOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
}

- (void)requestClanDetail {
    NSDictionary *dic = @{@"user_id":[SPDApiUser currentUser].userId, @"clan_id": self.clanId};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"detail" isClan:YES bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.clanInfo = suceess;
        [self.collectionView reloadData];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 5;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == 2) {
        return self.deputyChiefArr.count;
    } else if (section == 4) {
        return self.memberArr.count;
    } else {
        return 1;
    }
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        FamilyMemberHeaderCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FamilyMemberHeaderCell" forIndexPath:indexPath];
        cell.model = self.chiefModel;
        return cell;
    } else if (indexPath.section == 1 || indexPath.section == 3) {
        FamilyMemberTitleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FamilyMemberTitleCell" forIndexPath:indexPath];
        cell.titleLabel.text = indexPath.section == 1 ? [NSString stringWithFormat:@"%@(%ld/%ld)", SPDStringWithKey(@"副族长", nil), self.deputyChiefArr.count, self.deputyChiefLimit] : [NSString stringWithFormat:@"%@(%ld/%ld)", SPDStringWithKey(@"家族成员", nil), self.memberArr.count, self.memberLimit];
        return cell;
    } else {
        FamilyMemberListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FamilyMemberListCell" forIndexPath:indexPath];
        cell.delegate = self;
        cell.position = self.position;
        cell.model = indexPath.section == 2 ? self.deputyChiefArr[indexPath.row] : self.memberArr[indexPath.row];
        return cell;
    }
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        [self pushToDetailTableViewController:self userId:self.chiefModel.user_id];
    } else if (indexPath.section == 2) {
        SPDFamilyMemberModel *model = self.deputyChiefArr[indexPath.row];
        [self pushToDetailTableViewController:self userId:model.user_id];
    } else if (indexPath.section == 4) {
        SPDFamilyMemberModel *model = self.memberArr[indexPath.row];
        [self pushToDetailTableViewController:self userId:model.user_id];
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return CGSizeMake(kScreenW, 183);
    } else if (indexPath.section == 1 || indexPath.section == 3) {
        return CGSizeMake(kScreenW, 44);
    } else {
        return CGSizeMake(kScreenW, 92);
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (section == 3) {
        return UIEdgeInsetsMake(5.0f, 0.0f, 0.0f, 0.0f);
    } else {
        return UIEdgeInsetsZero;
    }
}

#pragma mark - FamilyMemberListCellDelegate

- (void)didClickActionWithModel:(SPDFamilyMemberModel *)model url:(NSString *)url {
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:@{@"user_id": [SPDApiUser currentUser].userId, @"clan_id": self.clanId}];
    NSString *tips;
    if ([url isEqualToString:@"promotion"]) {
        [dic setObject:model.user_id forKey:@"p_user_id"];
        tips = SPDStringWithKey(@"晋升成功", nil);
    } else if ([url isEqualToString:@"demote"]) {
        [dic setObject:model.user_id forKey:@"d_user_id"];
        tips = SPDStringWithKey(@"降级成功", nil);
    } else if ([url isEqualToString:@"kick"]) {
        [dic setObject:model.user_id forKey:@"kick_user_id"];
        tips = SPDStringWithKey(@"踢出成功", nil);
    }
    [RequestUtils commonPostRequestUtils:dic bURL:url isClan:YES bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        [self showToast:tips];
        [self requestClanDetail];
    } bFailure:^(id  _Nullable failure) {
        [self showToast:failure[@"msg"]];
    }];
}

#pragma mark - Setters and Getters

- (void)setClanInfo:(NSDictionary *)clanInfo {
    _clanInfo = clanInfo;
    
    self.position = _clanInfo[@"position"];
    self.clanId = _clanInfo[@"clan"][@"_id"];
    self.chiefModel = [SPDFamilyMemberModel initWithDictionary:_clanInfo[@"clan"][@"chief"]];
    self.chiefModel.position = @"chief";
    self.deputyChiefLimit = [_clanInfo[@"clan"][@"deputChiefLimit"] integerValue];
    [self.deputyChiefArr removeAllObjects];
    for (NSDictionary *dic in _clanInfo[@"clan"][@"deputy_chiefs"]) {
        SPDFamilyMemberModel *model = [SPDFamilyMemberModel initWithDictionary:dic];
        model.position = @"deputychief";
        [self.deputyChiefArr addObject:model];
    }
    self.memberLimit = [_clanInfo[@"clan"][@"limit"] integerValue];
    [self.memberArr removeAllObjects];
    for (NSDictionary *dic in _clanInfo[@"clan"][@"members"]) {
        SPDFamilyMemberModel *model = [SPDFamilyMemberModel initWithDictionary:dic];
        model.position = @"member";
        [self.memberArr addObject:model];
    }
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.minimumLineSpacing = 0;
        flowLayout.minimumInteritemSpacing = 0;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, NavHeight, kScreenW, kScreenH - NavHeight) collectionViewLayout:flowLayout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor colorWithHexString:@"f5f5f5"];
        _collectionView.showsVerticalScrollIndicator = NO;
        [_collectionView registerNib:[UINib nibWithNibName:@"FamilyMemberHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"FamilyMemberHeaderCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"FamilyMemberTitleCell" bundle:nil] forCellWithReuseIdentifier:@"FamilyMemberTitleCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"FamilyMemberListCell" bundle:nil] forCellWithReuseIdentifier:@"FamilyMemberListCell"];
    }
    return _collectionView;
}

- (NSMutableArray *)deputyChiefArr {
    if (!_deputyChiefArr) {
        _deputyChiefArr = [NSMutableArray array];
    }
    return _deputyChiefArr;
}

- (NSMutableArray *)memberArr {
    if (!_memberArr) {
        _memberArr = [NSMutableArray array];
    }
    return _memberArr;
}
@end
