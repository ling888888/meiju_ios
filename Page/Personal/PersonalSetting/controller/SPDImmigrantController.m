//
//  SPDImmigrantController.m
//  SimpleDate
//
//  Created by Monkey on 2017/12/14.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDImmigrantController.h"
#import "SPDApiUser.h"
#import "AppDelegate.h"
#import "ZegoKitManager.h"

@interface SPDImmigrantController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * titleArray;//中文区
@property (nonatomic, strong) NSMutableArray * strArray; // ar fr
@property (nonatomic, strong) NSMutableDictionary *languageDic;
@property (nonatomic, strong) NSIndexPath * lastpath;
@property (nonatomic, strong) NSString * currentLanguageArea;
@property (nonatomic, copy)  NSNumber * charge; //换区需要花的金币

@end

@implementation SPDImmigrantController

-(NSMutableArray *)titleArray{
    if (!_titleArray) {
        _titleArray = [NSMutableArray new];
    }
    return _titleArray;
}

-(NSMutableArray *)strArray{
    if (!_strArray) {
        _strArray = [NSMutableArray new];
    }
    return _strArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = SPDStringWithKey(@"我要移民", nil);
    self.currentLanguageArea = [SPDApiUser currentUser].lang;
    if (!self.currentLanguageArea) {
        [self showToast:@"您还没有语言区，请重新登录再进行更改！"];
    }else{
        [self.view addSubview:self.tableView];
        [self requestLanguageArea];
    }
    
//    UIBarButtonItem * right = [[UIBarButtonItem alloc]initWithTitle:SPDStringWithKey(@"移民规则", nil) style:UIBarButtonItemStylePlain target:self action:@selector(handleRightBtnClicked:)];
//    self.navigationItem.rightBarButtonItem = right;
    
}

//- (void)handleRightBtnClicked:(UIButton *)btn {
//    UIStoryboard * home = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    SPDImmigrantRulesController * vc = [home instantiateViewControllerWithIdentifier:@"SPDImmigrantRulesController"];
//    [self.navigationController pushViewController:vc animated:YES];
//}

- (void)requestLanguageArea {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"lang" bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        NSArray * array = suceess[@"list"];
        [self.titleArray removeAllObjects];
        [self.strArray removeAllObjects];
        for (NSDictionary * dict in  array) {
            [self.titleArray addObject:dict[@"name"]];
            [self.strArray addObject:dict[@"lang"]];
            [self.languageDic setObject:dict[@"language"] forKey:dict[@"lang"]];
        }
        self.charge = suceess[@"charge"];
        [self.tableView reloadData];
    } bFailure:^(id  _Nullable failure) {
        [self.tableView reloadData];
    }];
}

-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc]init];
    }
    return _tableView;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.titleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    cell.textLabel.text = self.titleArray[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if ([self.strArray[indexPath.row] isEqualToString:self.currentLanguageArea]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        self.lastpath = indexPath;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ( indexPath != self.lastpath) {
        //先置换一下ui
        NSInteger lastRow = (self.lastpath !=nil) ? (self.lastpath.row):-1;
        if (indexPath.row != lastRow) {
            NSString*title = [NSString stringWithFormat:SPDStringWithKey(@"确定花费%@金币移民?", nil),self.charge];
            [SPDCommonTool presentAutoController:[SPDCommonTool getWindowTopViewController] title:title titleColor:@"" titleFontSize:0 messageString:@"" messageColor:@"" messageFontSize:0 cancelTitle:SPDStringWithKey(@"取消",nil) cancelAction:^{
            } confirmTitle:SPDStringWithKey(@"确定",nil) confirmAction:^{
                [self chooseImmigrateWithStr:self.strArray[indexPath.row]];
//                [tableView deselectRowAtIndexPath:self.lastpath animated:YES];
            }];
        }
        
    }
}

- (void)chooseImmigrateWithStr:(NSString *)lang {
        //调用init 方法
        NSMutableDictionary * dict = [NSMutableDictionary new];
        [dict setValue:@(0) forKey:@"init"];
        [dict setValue:@"setting" forKey:@"source"];
        [dict setValue:lang forKey:@"lang"];
        NSLog(@"init dict %@",dict);
        [RequestUtils commonPostUrlRequestUtils:dict bURL:@"lang" RequestType:SPDAPIUrl bAnimated:YES bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
            NSLog(@"设置语言分区成功～");
            
            [self changeSelectCellStyle:lang];
            [self switchGlobalChatroomWithLanguage:lang];
            [[SPDLanguageTool sharedInstance] setNewLanguage:self.languageDic[lang]];
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"launch_lastupdate"];
            [SPDCommonTool requestAnimationImagesWithType:@"launch"];
            
        } bFailure:^(id  _Nullable failure) {
            NSLog(@"设置语言分区失败～");
            if ([failure isKindOfClass:[NSDictionary class]]) {
                if ([failure[@"code"] integerValue] == 2004) {
                    [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
                        [self.tableView reloadData];
                    } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                        [self pushToRechageController];
                    }];
                }else{
                    [self showToast:failure[@"msg"] ];
                }
            }
        }];
}

- (void)changeSelectCellStyle: (NSString *)lang {
    NSUInteger index = [self.strArray indexOfObject:lang];
    NSIndexPath *path = [NSIndexPath indexPathForRow:index inSection:0];
    ;
    UITableViewCell * lastcell = [self.tableView cellForRowAtIndexPath:self.lastpath];
    lastcell.accessoryType = UITableViewCellAccessoryNone;
    UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:path];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    self.lastpath = path;
//    [self.tableView deselectRowAtIndexPath:path animated:YES];
}

- (void)switchGlobalChatroomWithLanguage:(NSString *)lang {
    [ZegoManager leaveRoom];
    [[RCIMClient sharedRCIMClient] quitChatRoom:GLOBALCHATROOM success:^{
        
    } error:^(RCErrorCode status) {
        
    }];
    [DBUtil deleteClanHistory];
    [SPDRCIMDataSource shareInstance].lastGlobalMessage = nil;
    [SPDApiUser currentUser].lang = lang;
    [[SPDApiUser currentUser] saveToDefault];
    [[RCIMClient sharedRCIMClient] joinChatRoom:GLOBALCHATROOM messageCount:1 success:^{
        [[RCIMClient sharedRCIMClient] setConversationNotificationStatus:ConversationType_CHATROOM targetId:GLOBALCHATROOM isBlocked:YES success:^(RCConversationNotificationStatus nStatus) {
            
        } error:^(RCErrorCode status) {
            
        }];
    } error:^(RCErrorCode status) {
        
    }];
}

//重新渲染一下
-(void)resetRootViewController
{
    AppDelegate *appDelegate =
    (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([SPDApiUser currentUser].isLoginToLocal) {
//        UIStoryboard *SB = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        SPDTabBarController *tabBarController = [SB instantiateViewControllerWithIdentifier:@"SPDTabBarController"];
        LY_TabBarController *tabBarController = [[LY_TabBarController alloc] init];
        appDelegate.window.rootViewController = tabBarController;
    }else{
        UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        appDelegate.window.rootViewController = [loginStoryboard instantiateInitialViewController];
    }
    
    
}

- (NSMutableDictionary *)languageDic {
    if (!_languageDic) {
        _languageDic = [NSMutableDictionary dictionary];
    }
    return _languageDic;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
