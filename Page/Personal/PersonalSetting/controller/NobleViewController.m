//
//  NobleViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2018/9/11.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "NobleViewController.h"
#import "NobleNameCell.h"
#import "NoblePrivilegeHeaderCell.h"
#import "NoblePrivilegeCell.h"
#import "NobleModel.h"
#import "NoblePrivilegeModel.h"
#import "SPDCustomSliderView.h"
#import "UIViewController+Additions.h"
#import "UINavigationBar+LY.h"
#import "NobleBillListViewController.h"
#import "YYImage.h"
#import "HomeModel.h"
#import "YYWebImage.h"

@interface NobleViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
//@property (weak, nonatomic) IBOutlet UIImageView *avatarFrameImageView;
@property (weak, nonatomic) IBOutlet UILabel *nobleTotalValueLabel; // 贵族值
@property (weak, nonatomic) IBOutlet UILabel *descLabel; // 今日成长值 和 今日消耗值
@property (weak, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (weak, nonatomic) IBOutlet UICollectionView *nameCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *privilegeCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *singlePriceLabel;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;

@property (nonatomic, strong) NSDictionary *myNobleDic;
@property (nonatomic, strong) NSMutableArray *nobleArray;
@property (nonatomic, strong) NobleModel *ownededModel;
@property (nonatomic, strong) NobleModel *selectedModel;
@property (nonatomic, assign) BOOL haveSetMyNoble;
@property (nonatomic, assign) NSInteger currentNobleValue; // 当前贵族值
@property (nonatomic, strong) SPDCustomSliderView *customSliderView;
@property (weak, nonatomic) IBOutlet UIView *rechargeView;

@property(nonatomic, strong) LY_PortraitView *portraitView;

@property(nonatomic, strong) HomeModel *model;

@end

@implementation NobleViewController

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
    }
    return _portraitView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = SPDStringWithKey(@"贵族中心", nil);
    self.topConstant.constant = 11 + naviBarHeight;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"family_help"] style:UIBarButtonItemStylePlain target:self action:@selector(clickRightBarButtonItem:)];

    [self.nameCollectionView registerNib:[UINib nibWithNibName:@"NobleNameCell" bundle:nil] forCellWithReuseIdentifier:@"NobleNameCell"];
    [self.privilegeCollectionView registerNib:[UINib nibWithNibName:@"NoblePrivilegeHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"NoblePrivilegeHeaderCell"];
    [self.privilegeCollectionView registerNib:[UINib nibWithNibName:@"NoblePrivilegeCell" bundle:nil] forCellWithReuseIdentifier:@"NoblePrivilegeCell"];
    [self.privilegeCollectionView setContentInset:UIEdgeInsetsMake(0, 0, 80, 0)];
    [self.buyButton setTitle:SPDStringWithKey(@"充值", nil) forState:UIControlStateNormal];
    [self.view addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(70);
        make.top.mas_equalTo(NavHeight + 5);
        make.leading.mas_equalTo(5);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:0];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.tintColor = [UIColor colorWithHexString:@"ffffff"];
    self.navigationController.navigationBar.titleTextAttributes = @{
        NSFontAttributeName:[UIFont mediumFontOfSize:18],
        NSForegroundColorAttributeName:[UIColor colorWithHexString:@"ffffff"]
    };
    [self requestMyInfoDetail];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:1];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    self.navigationController.navigationBar.tintColor = [UIColor colorWithHexString:@"#1A1A1A"];
    self.navigationController.navigationBar.titleTextAttributes = @{
        NSFontAttributeName:[UIFont mediumFontOfSize:18],
        NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#1A1A1A"]
    };
}

#pragma mark - Event response

- (void)clickRightBarButtonItem:(UIBarButtonItem *)sender {
    NSString *url = [NSString stringWithFormat:@"https://%@share.famy.ly/usertitlegz.html?language=%@", DEV ? @"dev-" : @"", [SPDCommonTool getFamyLanguage]];
    [self loadWebViewController:self title:@"" url:url];
}

- (IBAction)clickBuyButton:(UIButton *)sender {
    [self pushToRechageController];
//    [self requestNoblePriceWithModel:self.selectedModel];
}

- (IBAction)nobleBillList:(UIButton *)sender {
    [self.navigationController pushViewController:[NobleBillListViewController new] animated:YES];
}

#pragma mark - Private methods

- (void)requestMyInfoDetail {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.info.detail" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.model = [HomeModel initWithDictionary:suceess];
        self.myNobleDic = suceess[@"is_noble"];
        [self refreshMyNoble];
        [self requestNoblePrivilegeList];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestNoblePrivilegeList {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionary] bURL:@"noble.privilege.list" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        [self.nobleArray removeAllObjects];
        for (NSDictionary *dic in suceess[@"noble"]) {
            NobleModel *model = [NobleModel initWithDictionary:dic];
            for (NSDictionary *dic in suceess[@"privilege"]) {
                NoblePrivilegeModel *privilegeModel = [NoblePrivilegeModel initWithDictionary:dic];
                [model.privilegeArray addObject:privilegeModel];
            }
            for (NSString *_id in model.privileges) {
                for (NoblePrivilegeModel *privilegeModel in model.privilegeArray) {
                    if ([privilegeModel._id isEqualToString:_id]) {
                        privilegeModel.isOwned = YES;
                        break;
                    }
                }
            }
            if ([model._id isEqualToString:self.myNobleDic[@"noble_type"]]) {
                model.isOwned = YES;
                self.ownededModel = model;
            }
            [self.nobleArray addObject:model];
        }
        [self collectionView:self.nameCollectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        // 设置贵族值
        self.currentNobleValue = [suceess[@"nobleValueInfo"][@"noble_total_value"] integerValue] ? : 0;
        self.nobleTotalValueLabel.text = [NSString stringWithFormat:@"%@:%ld", SPDStringWithKey(@"贵族值", nil), self.currentNobleValue];
        NSInteger nobleTodayValue = [suceess[@"nobleValueInfo"][@"noble_today_value"] integerValue];
        NSString *nobleTodayValueStr = nobleTodayValue >= 100000 ? [SPDCommonTool transformIntegerToBriefStr:nobleTodayValue] : [NSString stringWithFormat:@"%ld", nobleTodayValue];
        self.descLabel.text = [NSString stringWithFormat:@"%@:%@ %@:%@", SPDStringWithKey(@"今日成长值", nil), nobleTodayValueStr, SPDStringWithKey(@"今日消耗值", nil), suceess[@"nobleValueInfo"][@"noble_consume_value"] ? : @"0"];
        
        // 设置贵族值进度条
        for (UIView *view in self.myScrollView.subviews) {
            [view removeFromSuperview];
        }
        self.myScrollView.contentSize = CGSizeMake((self.nobleArray.count + 1) * (kScreenW / 3), 44);
        if (!self.customSliderView.superview) {
            [self.myScrollView addSubview:self.customSliderView];
        }
        
        BOOL currentVersionIsArabic = [SPDCommonTool currentVersionIsArbic];
        CGFloat leading = 5;
        NSInteger sliderIndex = -1;
        for (NSInteger index = 0; index < self.nobleArray.count + 1; index++) {
            YYAnimatedImageView *imageView = [[YYAnimatedImageView alloc] init];
            [self.myScrollView addSubview:imageView];
            [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(0);
                make.left.mas_equalTo(currentVersionIsArabic ? self.myScrollView.contentSize.width - leading - 25 : leading);
                make.height.mas_equalTo(21);
                make.width.mas_equalTo(21);
            }];
            
            UILabel * label = [[UILabel alloc]init];
            label.font = [UIFont systemFontOfSize:15];
            [self.myScrollView addSubview:label];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(imageView.mas_bottom).offset(4);
                make.centerX.mas_equalTo(imageView.mas_centerX);
            }];
            leading += kScreenW / 3;
            
            if (index == 0) {
                imageView.image = [UIImage imageNamed:@"img_default_noble"];
                label.text = @"0";
                label.textColor = [UIColor colorWithHexString:@"EBC075"];
            } else {
#warning  todo 
                NobleModel *model = self.nobleArray[index - 1];
                
//                imageView.image = [[YYImage alloc]initWithData: [NSData dataWithContentsOfURL:[NSURL URLWithString:]]];
                imageView.yy_imageURL = [NSURL URLWithString:model.icon_active];
                label.text = [NSString stringWithFormat:@"%ld", model.noble_value];
                label.textColor = self.currentNobleValue >= model.noble_value ? [UIColor colorWithHexString:@"#EBC075"]: SPD_HEXCOlOR(@"BBBBBB");
                if (self.currentNobleValue > model.noble_value) {
                    sliderIndex = index - 1;
                }
            }
        }
        
        CGFloat sliderTopViewLength = 0;
        if (sliderIndex == self.nobleArray.count - 1) {
            sliderTopViewLength = self.customSliderView.frame.size.width - 30;
        } else if (sliderIndex == -1) {
            NobleModel *rightModel = self.nobleArray[sliderIndex + 1];
            sliderTopViewLength += self.currentNobleValue * 1.f / rightModel.noble_value * (kScreenW / 3.f - 25);
        } else {
            sliderTopViewLength += (sliderIndex + 1) * kScreenW / 3.f;
            NobleModel *leftModel = self.nobleArray[sliderIndex];
            NobleModel *rightModel = self.nobleArray[sliderIndex + 1];
            sliderTopViewLength += (self.currentNobleValue - leftModel.noble_value) * 1.f / (rightModel.noble_value - leftModel.noble_value) * (kScreenW / 3.f - 25);
        }
        [self.customSliderView updateSliderLengthWith:sliderTopViewLength];
        
        CGFloat offsetX;
        if (currentVersionIsArabic) {
            offsetX = self.myScrollView.contentSize.width - sliderTopViewLength - 30 - self.myScrollView.bounds.size.width / 2;
        } else {
            offsetX = sliderTopViewLength + 30 - self.myScrollView.bounds.size.width / 2;
        }
        if (offsetX < 0) {
            offsetX = 0;
        } else if (offsetX > self.myScrollView.contentSize.width - self.myScrollView.bounds.size.width) {
            offsetX = self.myScrollView.contentSize.width - self.myScrollView.bounds.size.width;
        }
        [self.myScrollView setContentOffset:CGPointMake(offsetX, 0)];
        
        if (!self.myNobleDic) {
            self.singlePriceLabel.text = SPDStringWithKey(@"暂未获得贵族身份，加油充值吧～", nil);
        } else {
            NobleModel *kingModel = self.nobleArray[self.nobleArray.count - 1];
            if (self.currentNobleValue >= kingModel.noble_value) {
                self.singlePriceLabel.text = SPDStringWithKey(@"继续充值保持国王身份吧～", nil);
            } else {
                NobleModel *model = self.nobleArray[sliderIndex + 1];
                if (self.currentNobleValue / model.noble_value == 1) {
                    NobleModel *realModel = self.nobleArray[sliderIndex + 2];
                    self.singlePriceLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"再增加%@贵族值可升至%@", nil), suceess[@"nobleValueInfo"][@"nextlevel_need_gold"] ? : @"0", realModel.name];
                } else {
                    self.singlePriceLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"再增加%@贵族值可升至%@", nil), suceess[@"nobleValueInfo"][@"nextlevel_need_gold"] ? : @"0", model.name];
                }
            }
        }
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestNoblePriceWithModel:(NobleModel *)model {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionary] bURL:@"noble.price" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        for (NSDictionary *dic in suceess[@"noble"]) {
            if ([dic[@"_id"] isEqualToString:model._id]) {
                if ([dic[@"buy_type"] isEqualToString:@"none"]) {
                    [self showToast:SPDStringWithKey(@"你已拥有高级贵族，不能再开通低等级贵族", nil)];
                } else {
                    NSString *title = [NSString stringWithFormat:SPDStringWithKey(@"确定花费%@金币开通%@贵族？", nil), dic[@"price"], model.name];
                    [self presentCommonController:title messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                        [self requestNobleBuyWithModel:model];
                    }];
                }
            }
        }
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestNobleBuyWithModel:(NobleModel *)model {
    self.buyButton.enabled = NO;
    NSDictionary *dic = @{@"type": model._id};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"noble.buy" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        [self showToast:SPDStringWithKey(@"开通贵族成功", nil)];
        self.ownededModel.isOwned = NO;
        model.isOwned = YES;
        self.ownededModel = model;
        self.myNobleDic = @{@"noble_type": model._id, @"noble_expire_time": suceess[@"noble_expire_time"]};
        [self refreshMyNoble];
        [self refreshSelectedNoble];
        self.buyButton.enabled = YES;
    } bFailure:^(id  _Nullable failure) {
        self.buyButton.enabled = YES;
        if ([failure isKindOfClass:[NSDictionary class]]) {
            if ([failure[@"code"] integerValue] == 2003) {
                [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"充值", nil) confirmAction:^{
                    [self pushToRechageController];
                }];
            } else {
                [self showToast:failure[@"msg"]];
            }
        }
    }];
}

- (void)refreshMyNoble {
    if (self.model) {
        self.portraitView.portrait = self.model.portrait;
    }
}

- (void)refreshSelectedNoble {
    [self.nameCollectionView reloadData];
    [self.privilegeCollectionView reloadData];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if ([collectionView isEqual:self.nameCollectionView]) {
        return 1;
    } else {
        return 2;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if ([collectionView isEqual:self.nameCollectionView]) {
        return self.nobleArray.count;
    } else {
        if (section == 0) {
            return 1;
        } else {
            return self.selectedModel.privilegeArray.count;
        }
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([collectionView isEqual:self.nameCollectionView]) {
        NobleNameCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NobleNameCell" forIndexPath:indexPath];
        cell.model = self.nobleArray[indexPath.row];
        return cell;
    } else {
        if (indexPath.section == 0) {
            NoblePrivilegeHeaderCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NoblePrivilegeHeaderCell" forIndexPath:indexPath];
            cell.model = self.selectedModel;
            return cell;
        } else {
            NoblePrivilegeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NoblePrivilegeCell" forIndexPath:indexPath];
            cell.model = self.selectedModel.privilegeArray[indexPath.row];
            return cell;
        }
    }
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([collectionView isEqual:self.nameCollectionView]) {
        NobleModel *model = self.nobleArray[indexPath.row];
        if (![model isEqual:self.selectedModel]) {
            self.selectedModel.isSelected = NO;
            model.isSelected = YES;
            self.selectedModel = model;
            [self refreshSelectedNoble];
        }
    } else if (indexPath.section == 1) {
        NoblePrivilegeModel *model = self.selectedModel.privilegeArray[indexPath.row];
        if (model.isOwned) {
            NSString *url = [NSString stringWithFormat:@"https://%@share.famy.ly/usertitle.html?title=%@&permiss=%@&language=%@", DEV ? @"dev-" : @"", self.selectedModel._id, model._id, [SPDCommonTool getFamyLanguage]];
            [self loadWebViewController:self title:@"" url:url];
        }
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([collectionView isEqual:self.nameCollectionView]) {
        return CGSizeMake(kScreenW / 6, 50);
    } else {
        if (indexPath.section == 0) {
            return CGSizeMake(kScreenW, 320);
        } else {
            return CGSizeMake(kScreenW / 3, 80);
        }
    }
}

#pragma mark - Setters and Getters

- (NSMutableArray *)nobleArray {
    if (!_nobleArray) {
        _nobleArray = [NSMutableArray array];
    }
    return _nobleArray;
}

- (SPDCustomSliderView *)customSliderView {
    if (!_customSliderView) {
        CGFloat x = [SPDCommonTool currentVersionIsArbic] ? 25 : 30;
        _customSliderView = [[SPDCustomSliderView alloc] initWithFrame:CGRectMake(x, 0, self.myScrollView.contentSize.width - 30 - 25, 20)];
    }
    return _customSliderView;
}

@end
