//
//  SPDFamilyApplyController.h
//  SimpleDate
//
//  Created by Monkey on 2018/3/26.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "BaseViewController.h"
#import "SPDFamilyMemberModel.h"


@interface SPDFamilyApplyController : BaseViewController

@property (nonatomic,strong) NSMutableArray * dataArray ;

@property (nonatomic, copy) NSString * clan_id;


@end
