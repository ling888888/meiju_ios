//
//  SPDVehicleViewController.h
//  SimpleDate
//
//  Created by 李楠 on 17/7/11.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SPDVehicleViewControllerDelegate <NSObject>

@optional

- (void)showEquipVehicleGuidance;

@end

@interface SPDVehicleViewController : BaseViewController

@property (nonatomic, assign) BOOL isMine;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, weak) id<SPDVehicleViewControllerDelegate> delegate;

@end
