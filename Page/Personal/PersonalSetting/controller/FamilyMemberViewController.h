//
//  FamilyMemberViewController.h
//  SimpleDate
//
//  Created by 李楠 on 2019/2/2.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FamilyMemberViewController : BaseViewController

@property (nonatomic, strong) NSDictionary *clanInfo;

@end

NS_ASSUME_NONNULL_END
