//
//  SPDVehicleViewController.m
//  SimpleDate
//
//  Created by 李楠 on 17/7/11.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDVehicleViewController.h"
#import "SPDVehicleCell.h"
#import "SPDVehicleModel.h"
#import "SPDApiUser.h"
#import "PresentMessage.h"
#import "SPDVehicleEffectImageView.h"
#import "SPDVoiceLiveGlobalScrollView.h"

@interface SPDVehicleViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, SPDVehicleCellDelegate, SPDVehicleViewControllerDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *vehicleArray;
@property (nonatomic, strong) SPDVehicleEffectImageView *vehicleEffectView;
@property (nonatomic, strong) SPDVoiceLiveGlobalScrollView *vehicleAnimationView;

@end

@implementation SPDVehicleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initSubviews];
    if (!_isMine) {
        [self requestData];
//        [SPDCommonTool requestAnimationImagesWithType:@"vehicle"];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.title = SPDStringWithKey(@"座驾", nil);
    if (_isMine) {
        [self requestData];
    }
}

- (void)initSubviews {
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor colorWithHexString:@"f5f5f5"];
    
    if (_isMine) {
        UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:SPDStringWithKey(@"购买", nil) style:UIBarButtonItemStylePlain target:self action:@selector(rightBarButtonItemClicked)];
        self.navigationItem.rightBarButtonItem = rightBarButtonItem;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 64, kScreenW, kScreenH - 64)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.image = [UIImage imageNamed:@"vehicle_none"].adaptiveRtl;
        [self.view addSubview:imageView];
    }
    
    [self.view addSubview:self.collectionView];
}

- (void)rightBarButtonItemClicked {
    SPDVehicleViewController *vc = [[SPDVehicleViewController alloc] init];
    vc.isMine = NO;
    vc.user_id = [SPDApiUser currentUser].userId;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)requestData {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:_isMine ? @"my.vehicle.list" : @"vehicle.list" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        [self.vehicleArray removeAllObjects];
        for (NSDictionary *dic in suceess[@"list"]) {
            SPDVehicleModel *model = [SPDVehicleModel initWithDictionary:dic];
            [self.vehicleArray addObject:model];
        }
        if (self.vehicleArray.count == 0 && _isMine) {
            self.collectionView.backgroundColor = [UIColor clearColor];
        } else {
            self.collectionView.backgroundColor = [UIColor colorWithHexString:@"f5f5f5"];
        }
        [self.collectionView reloadData];
        
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (_isMine) {
        return self.vehicleArray.count;
    } else {
        return self.vehicleArray.count + 1;
    }
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SPDVehicleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SPDVehicleCell" forIndexPath:indexPath];
    if (indexPath.row == _vehicleArray.count) {
        cell.moreVehicleTip.hidden = NO;
    } else {
        cell.isMine = _isMine;
        cell.delegate = self;
        SPDVehicleModel *model = _vehicleArray[indexPath.row];
        cell.model = model;
        cell.moreVehicleTip.hidden = YES;
    }
    return cell;
}

#pragma mark - SPDVehicleCellDelegate

- (void)didClickBuyButton:(SPDVehicleModel *)model {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *error = [userDefault objectForKey:@"errorTime"];
    if (!error) {
        [self buyVehicle:model];
    } else {
        double timeInterval = [error doubleValue];
        NSTimeInterval current = [[NSDate date] timeIntervalSince1970];
        if (current - timeInterval <= 90) {
            [self showToast:SPDStringWithKey(@"90s之后再试试哦！", nil)];
        } else {
            [self buyVehicle:model];
        }
    }
}

- (void)buyVehicle:(SPDVehicleModel *)model {
    NSString *alert;
    if (_isMine) {
        alert = [NSString stringWithFormat:SPDStringWithKey(@"确定花费%@金币再次购买此座驾7天", nil), model.price];
    } else {
        alert = [NSString stringWithFormat:SPDStringWithKey(@"确定花费%@金币购买此座驾7天", nil), model.price];
    }
    [self presentCommonController:alert messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
        [self requestVehicleBuy:model];
    }];
}

- (void)requestVehicleBuy:(SPDVehicleModel *)model {
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setValue:self.user_id forKey:@"user_id"];
    [dic setValue:model.vehicle_id forKey:@"vehicle_id"];
    [RequestUtils commonPostRequestUtils:dic bURL:@"vehicle.buy" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        
        // check level
        [[SPDCommonTool shareTool] checkExpToWhetherToUpgrage];
        
        if (_isMine) {
            [self requestData];
            [self showToast:SPDStringWithKey(@"续费成功", nil)];
        } else {
            if ([self.user_id isEqualToString:[SPDApiUser currentUser].userId]) {
                [self showToast:SPDStringWithKey(@"购买成功", nil)];
            } else {
                [self showToast:SPDStringWithKey(@"赠送成功", nil)];
                model.expire_time = suceess[@"expire_time"];
                [self sendMessageWithVehicle:model];
            }

            if ([suceess[@"is_firstbuy"] boolValue]) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(showEquipVehicleGuidance)]) {
                    [self.delegate showEquipVehicleGuidance];
                }
            }
            
            [self.navigationController popViewControllerAnimated:YES];
        }
    } bFailure:^(id  _Nullable failure) {
        if ([failure[@"code"] integerValue] == 203) {
            [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"充值", nil) confirmAction:^{
                [self pushToRechageController];
            }];
        } else {
            [self showToast:failure[@"msg"]];
        }
    }];
}

- (void)sendMessageWithVehicle:(SPDVehicleModel *)model {
    PresentMessage *msg = [PresentMessage messageWithContent:SPDStringWithKey(@"送你一辆座驾，快去车库装备吧", nil) andVehicle_id:model.vehicle_id andGiftName:model.name andImageName:model.image andGoldNum:@"0" andCharamScore:@"0" andType:@"vehicle" andExpire_time:model.expire_time_str];
    [[RCIM sharedRCIM] sendMessage:ConversationType_PRIVATE targetId:self.user_id content:msg pushContent:SPDStringWithKey(@"送你一辆座驾，快去车库装备吧", nil) pushData:SPDStringWithKey(@"送你一辆座驾，快去车库装备吧", nil) success:^(long messageId) {
        
    } error:^(RCErrorCode nErrorCode, long messageId) {

    }];
}

- (void)didClickEquipButton:(SPDVehicleModel *)model {
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    if (model.is_drive) {
        [self presentCommonController:SPDStringWithKey(@"确定不再驾驶该车辆", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
            [self requestVehicleDrive:dic];
        }];
    } else {
        [self presentCommonController:SPDStringWithKey(@"确定驾驶该车辆", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
            [dic setValue:model.vehicle_id forKey:@"vehicle_id"];
            [self requestVehicleDrive:dic];
        }];
    }
}

- (void)requestVehicleDrive:(NSMutableDictionary *)dic {
    [RequestUtils commonPostRequestUtils:dic bURL:@"my.vehicle.drive" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        [self requestData];
        if (dic.count) {
            [self showToast:SPDStringWithKey(@"驾驶成功", nil)];
        }
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)didClickPreviewButton:(SPDVehicleModel *)model {
    if (isAlreadyDownloadVehicleImagesEffect) {
        if (model.grade.integerValue == 1) {
            NSMutableArray *images = [SPDVehicleEffectImageView getAnimationImagesWithVehicleId:model.vehicle_id isCGImage:NO];
            if (images.count) {
                NSDictionary *dic = @{@"type": @"-2", @"images": images};
                self.vehicleAnimationView.dic = dic;
            }
        } else if (model.grade.integerValue == 2) {
            [self.vehicleEffectView startAnimatingWithVehicleId:model.vehicle_id];
        }
    } else {
        [self showToast:SPDStringWithKey(@"正在获取座驾特效，请稍等", nil)];
    }
}

#pragma mark - SPDVehicleViewControllerDelegate

- (void)showEquipVehicleGuidance {
    if (!self.vehicleArray.count) {
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH)];
        bgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImageView:)];
        [bgView addGestureRecognizer:tap];
    
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"first_buy_vehicle"].adaptiveRtl];
        [bgView addSubview:imageView];
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo(kScreenW / 2 - 2.5 - 58);
            make.top.mas_equalTo(135);
            make.height.mas_equalTo(82);
            make.width.mas_equalTo(224);
        }];
        
        UILabel *equipLabel = [[UILabel alloc] init];
        equipLabel.backgroundColor = [UIColor colorWithHexString:COMMON_PINK];
        equipLabel.text = SPDStringWithKey(@"装备", nil);
        equipLabel.textColor = [UIColor whiteColor];
        equipLabel.font = [UIFont systemFontOfSize:12];
        equipLabel.textAlignment = NSTextAlignmentCenter;
        equipLabel.clipsToBounds = YES;
        equipLabel.layer.cornerRadius = 10;
        [imageView addSubview:equipLabel];
        [equipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
            make.height.mas_equalTo(20);
            make.width.mas_equalTo(48);
        }];
        
        UILabel *tipsLabel = [[UILabel alloc] init];
        tipsLabel.text = SPDStringWithKey(@"购买成功\n快去装备吧", nil);
        tipsLabel.textColor = [UIColor whiteColor];
        tipsLabel.font = [UIFont systemFontOfSize:16];
        tipsLabel.numberOfLines = 0;
        [imageView addSubview:tipsLabel];
        [tipsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.trailing.mas_equalTo(10);
            make.top.mas_equalTo(0);
            make.height.mas_equalTo(50);
            make.width.mas_equalTo(150);
        }];

        [[UIApplication sharedApplication].keyWindow addSubview:bgView];
    }
}

- (void)tapImageView:(UITapGestureRecognizer *)tap {
    [tap.view removeFromSuperview];
    if (self.vehicleArray.count) {
        [self didClickEquipButton:self.vehicleArray[0]];
    }
}

#pragma mark - setter / getter

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake((kScreenW - 5 - 13 * 2) / 2, 180);
        flowLayout.minimumLineSpacing = 5;
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.sectionInset = UIEdgeInsetsMake(5.0f, 13.0f, 5.0f, 13.0f);
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, NavHeight, kScreenW, kScreenH - NavHeight) collectionViewLayout:flowLayout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor colorWithHexString:@"f5f5f5"];
        _collectionView.showsVerticalScrollIndicator = NO;
        [_collectionView registerNib:[UINib nibWithNibName:@"SPDVehicleCell" bundle:nil] forCellWithReuseIdentifier:@"SPDVehicleCell"];
    }
    return _collectionView;
}

- (NSMutableArray *)vehicleArray {
    if (!_vehicleArray) {
        _vehicleArray = [NSMutableArray array];
    }
    return _vehicleArray;
}

- (SPDVehicleEffectImageView *)vehicleEffectView {
    if (!_vehicleEffectView) {
        _vehicleEffectView = [[SPDVehicleEffectImageView alloc] initWithFrame:self.view.bounds];
        _vehicleEffectView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
        _vehicleEffectView.userInteractionEnabled = YES;
        [self.view addSubview:_vehicleEffectView];
    }
    return _vehicleEffectView;
}

- (SPDVoiceLiveGlobalScrollView *)vehicleAnimationView {
    if (!_vehicleAnimationView) {
        UIView *view = [[UIView alloc] initWithFrame:self.view.bounds];
        view.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
        [self.view addSubview:view];
        
        _vehicleAnimationView = [[SPDVoiceLiveGlobalScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 112.5)];
        _vehicleAnimationView.center = view.center;
        _vehicleAnimationView.space = (self.vehicleAnimationView.bounds.size.width / 2) - (300 / 2);
        _vehicleAnimationView.speed = self.vehicleAnimationView.bounds.size.width;
        _vehicleAnimationView.isRepeat = NO;
        _vehicleAnimationView.hideAfterAnimation = YES;
        [view addSubview:_vehicleAnimationView];
    }
    return _vehicleAnimationView;
}

@end
