//
//  SPDHelpController.m
//  SimpleDate
//
//  Created by Monkey on 2017/4/19.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDHelpController.h"
#import <WebKit/WebKit.h>
#import "SPDImmigrantController.h"
#import "SPDVehicleViewController.h"
#import "ZTWAInfoShareView.h"
#import "SPDUMShareUtils.h"

@interface SPDHelpController ()<WKNavigationDelegate, ZTWAInfoShareViewDelegate>

@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, assign) BOOL isRelation;
@property (nonatomic, strong) ZTWAInfoShareView *shareView;

@end

@implementation SPDHelpController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_arrow_left"].adaptiveRtl style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    if (self.isRelation) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"family_help"] style:UIBarButtonItemStylePlain target:self action:@selector(clickHelpBtn)];
    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //统一导航栏标题颜色
    self.navigationController.navigationBar.titleTextAttributes = @{
        NSFontAttributeName:[UIFont mediumFontOfSize:18],
        NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#1A1A1A"]
    };
}

- (void)setUrl:(NSString *)url {
    _url = url;
    [self loadMyUrl];
}

- (void)back {
    if (self.webView.canGoBack && !self.isRelation) {
        [self.webView goBack];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)clickHelpBtn {
  
}

- (void)loadMyUrl {
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url]];
    [self.webView loadRequest:request];
//    if ([self.url containsString:My_Relation_URL]) {
//        //判断有没有徒弟
//        NSString * userDefaultTitle = self.listArray.count >0 ? @"haveShowedMentorRelationUrl":@"haveShowedNOMentorRelationUrl";
//        NSString *  flag = self.listArray.count >0 ? @"mentor":@"nomentor";
//        //判断是不是第一次加载 没有师徒关系的网页
//        if (! [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@%@",userDefaultTitle,[SPDApiUser currentUser].userId]]) {
//            NSString * myUrl = [NSString stringWithFormat:@"%@&flag=%@",self.url,flag];
//            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:myUrl]];
//            [self.webView loadRequest:request];
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"%@%@",userDefaultTitle,[SPDApiUser currentUser].userId]];
//        }else{
//            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url]];
//            [self.webView loadRequest:request];
//        }
//
//    }else{
//
//    }
    
}


- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    NSString *requestStr = navigationAction.request.URL.absoluteString;
    if ([requestStr containsString:@"jianyue://invitation"]) {
        [self.shareView show];
    }else if ([requestStr containsString:@"jianyue://get_user_detail"]){
        //跳转到用户详情
        NSRange range = [requestStr rangeOfString:@"="];
        NSString *user_id = [requestStr substringFromIndex:range.location+1];
        NSLog(@"user_id==%@",user_id);
        [self pushToDetailTableViewController:self userId:user_id];
    }else if ([requestStr containsString:@"jianyue://invite_detail?user_id"]) {
        // 跳转到用户详情
        NSRange range = [requestStr rangeOfString:@"="];
        NSString *user_id = [requestStr substringFromIndex:range.location+1];
        [self pushToDetailTableViewController:self userId:user_id];
    } else if ([requestStr containsString:@"jianyue://chatroom"]) {
        // 跳转到聊天室
        NSRange range = [requestStr rangeOfString:@"="];
        NSString *clan_id = [requestStr substringFromIndex:range.location+1];
        [self joinChatroomWithClanId:clan_id];
    } else if ([requestStr containsString:@"jianyue://back"]) {
        // 返回
        [self.navigationController popViewControllerAnimated:YES];
    } else if ([requestStr containsString:@"jianyue://i_want_to"]) {
        // 跳转到移民
        UIStoryboard * home = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SPDImmigrantController * vc = [home instantiateViewControllerWithIdentifier:@"SPDImmigrantController"];
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([requestStr containsString:@"jianyue://reservation?rescode=2007"]){
        //福彩余额不足
        [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
        } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
            [self pushToRechageController];
        }];
    }else if ([requestStr containsString:@"jianyue://some"]){
        [SPDCommonTool showWindowToast:@"点击预约按钮"];
    }else if ([requestStr containsString:@"jianyue://jumpiOS"]){
        //跳转到任意 Storyboard 页面
        NSRange range = [requestStr rangeOfString:@"="];
        NSString *controllerName = [requestStr substringFromIndex:range.location + 1];
        id obj = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:controllerName];
        if (!obj) {
            return;
        }// 防止不存在这个页面崩溃
        if ([obj isKindOfClass:[SPDVehicleViewController class]]) {
            SPDVehicleViewController *vc = obj;
            vc.user_id = [SPDApiUser currentUser].userId;
            [self.navigationController pushViewController:vc animated:YES];
        } else {
            [self.navigationController pushViewController:obj animated:YES];
        }
    }else if ([requestStr containsString:@"jianyue://jumpClassiOS"]){
        //跳转到任意 非Storyboard 页面
        NSRange range = [requestStr rangeOfString:@"="];
        NSString *controllerName = [requestStr substringFromIndex:range.location+1];
        Class class = NSClassFromString(controllerName);
        id obj = [[class alloc] init];
        [self.navigationController pushViewController:obj animated:YES];
    } else if ([requestStr containsString:@"jianyue://contact_service"]) {
        // 联系客服
        NSRange range = [requestStr rangeOfString:@"="];
        NSString *userId = [requestStr substringFromIndex:range.location + 1];
        [self pushToConversationViewControllerWithTargetId:userId];
    }else if ([requestStr containsString:@"jianyue://invitation"]) {
//        [self dealShare];//出现分享按钮
    }else if ([requestStr containsString:@"jianyue://jumpiOS"]){
        //跳转到任意 Storyboard 页面
        NSRange range = [requestStr rangeOfString:@"="];
        NSString *controllerName = [requestStr substringFromIndex:range.location + 1];
        id obj = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:controllerName];
        if ([obj isKindOfClass:[SPDVehicleViewController class]]) {
            SPDVehicleViewController *vc = obj;
            vc.user_id = [SPDApiUser currentUser].userId;
            [self.navigationController pushViewController:vc animated:YES];
        } else {
            [self.navigationController pushViewController:obj animated:YES];
        }
    }else if ([requestStr containsString:@"jianyue://jumpClassiOS"]){
        //跳转到任意 非Storyboard 页面
        NSRange range = [requestStr rangeOfString:@"="];
        NSString *controllerName = [requestStr substringFromIndex:range.location+1];
        Class class = NSClassFromString(controllerName);
        id obj = [[class alloc] init];
        [self.navigationController pushViewController:obj animated:YES];
    } else if ([requestStr containsString:@"jianyue://clandetail"]) {
        
    }
    decisionHandler(WKNavigationActionPolicyAllow);
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation {
    [webView evaluateJavaScript:@"document.title" completionHandler:^(id _Nullable title, NSError * _Nullable error) {
        self.navigationItem.title = title;
    }];
}

//- (void)setTitleText:(NSString *)titleText {
//    self.navigationItem.title = titleText;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    //清楚webview的缓存
    _webView = nil;
    [self cleanCacheAndCookie];
    
}


/**清除缓存和cookie*/
- (void)cleanCacheAndCookie {
    
//    NSHTTPCookie *cookie;
//    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
//    for (cookie in [storage cookies]){
//        [storage deleteCookie:cookie];
//        NSLog(@"cookie--- %@",cookie);
//    } //删除cookie 会导致网络请求cookie丢失
    NSURLCache * cache = [NSURLCache sharedURLCache];
    [cache removeAllCachedResponses];
    [cache setDiskCapacity:0];
    [cache setMemoryCapacity:0];
    
}

#pragma maek - ZTWAInfoShareViewDelegate

- (void)infoShareView:(ZTWAInfoShareView *)infoShareView shareToPlatformType:(SPDUMShareType)sharePlatformType {
    NSString *shareUrl = infoShareView.shareDict[@"share_url"];
    NSString *sharetitle = infoShareView.shareDict[@"title"];
    NSString *shareContent = infoShareView.shareDict[@"content"];
    NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:infoShareView.shareDict[@"thumbnail"]]];
    UIImage *image = [[UIImage alloc] initWithData:data];
    
//    [[SPDUMShareUtils shareInstance] shareToPlatform:sharePlatformType WithShareUrl:shareUrl andshareTitle:sharetitle andshareContent:shareContent andImageData:image andShareSourece :@"help" andSPDUMShareSuccess:^(id shareSuccess) {
//        [SPDCommonTool showWindowToast:SPDStringWithKey(@"分享成功", nil)];
//        [self.shareView dismiss];
//    } presentedController:self andSPDUMShareFailure:^(id shareFailure) {
//        
//    }];
}

#pragma maek - Setters & getters

- (WKWebView *)webView {
    if (!_webView) {
        _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, NavHeight, kScreenW, kScreenH - NavHeight)];
        _webView.navigationDelegate = self;
        if (@available(iOS 11.0, *)) {
            _webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
        [self.view addSubview:_webView];
    }
    return _webView;
}

- (BOOL)isRelation {
    return [self.url containsString:@"relation.famy.ly/index.html"];
}

- (ZTWAInfoShareView *)shareView {
    if (!_shareView) {
        _shareView = [ZTWAInfoShareView shareView];
        _shareView.delegate = self;
    }
    return _shareView;
}
@end
