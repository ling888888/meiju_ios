//
//  SPDBlackListViewController.m
//  SimpleDate
//
//  Created by Monkey on 2017/8/1.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDBlackListViewController.h"
#import "HomeModel.h"
#import "MyCollectionTableViewCell.h"


@interface SPDBlackListViewController ()

@property (weak, nonatomic) IBOutlet UITableView *myTableView;

@property (nonatomic,strong)NSMutableArray * blackListArray;

@end

@implementation SPDBlackListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.myTableView.tableFooterView = [[UIView alloc]init];
    
    [self configNavigationBar];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self requestBlackListData];

}

- (void)configNavigationBar {
    self.navigationItem.title = SPDStringWithKey(@"黑名单", nil);
    
    UIBarButtonItem * item = [[UIBarButtonItem alloc]initWithTitle:SPDStringWithKey(@"一键移出", nil) style:UIBarButtonItemStylePlain target:self action:@selector(handleRemoveAllBlackList)];
    self.navigationItem.rightBarButtonItem = item;
}

- (void)handleRemoveAllBlackList {
    
    if (self.blackListArray.count) {
        
        [self presentAutoController:SPDStringWithKey(@"确定要移出全部黑名单用户吗?", nil) titleColor:@"" titleFontSize:15 messageString:SPDStringWithKey(@"一键移出将取消对黑名单用户的拉黑", nil) messageColor:@"#999999"messageFontSize:14 cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
            
        } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
            
            NSMutableArray * users_idArray = [NSMutableArray array] ;
            for (HomeModel *model in self.blackListArray) {
                [users_idArray addObject:model._id];
            }
            NSString * users_id = [users_idArray componentsJoinedByString:@","];
            [self removeUserFromBlackList:users_id andType:YES];
            
        }];
    
    }
}

- (void)requestBlackListData {
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:@(0) forKey:@"page"];
    
    [RequestUtils commonGetRequestUtils:dict bURL:@"my.blacklist" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id _Nullable dict){
        
        [self.blackListArray removeAllObjects];
        NSDictionary *users=dict[@"users"];
        for (NSDictionary *dic in users) {
            HomeModel *model=[HomeModel initWithDictionary:dic];
            [self.blackListArray addObject:model];
        }
        [self.myTableView reloadData];
    }
    bFailure:^(id _Nullable failure) {
                                   
    }];
}

- (void)removeUserFromBlackList: (NSString *) user_id andType:(BOOL)isBatch{
    NSDictionary * dict;
    NSString * url ;
    if(!isBatch){
        dict = @{@"user_id":user_id};
        url = @"my.blacklist.remove";
    }else{
        dict = @{@"user_ids":user_id};
        url = @"my.blacklist.remove.batch";
    }
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dict] bURL:url bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {

        if (isBatch) {
            [self showMessageToast:SPDStringWithKey(@"一键移出成功！", nil)];
        }else{
            [self showMessageToast:SPDStringWithKey(@"移出黑名单成功,可以与对方视频语音、聊天了", nil)];
        }

        [self requestBlackListData];
        
    } bFailure:^(id  _Nullable failure) {
        [self showMessageToast:failure[@"msg"]];
    }];
    
}



- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:SPDStringWithKey(@"移出", nil) handler:^(UITableViewRowAction *_Nonnull action, NSIndexPath *_Nonnull indexPath) {
        
        HomeModel *item = self.blackListArray[indexPath.row];
        [self removeUserFromBlackList:item._id andType:NO];

    }];
    
    //    return @[ deleteAction, likeAction ];
    return @[ deleteAction];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self tableView:tableView heightForRowAtIndexPath:indexPath];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.blackListArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    HomeModel *item = self.blackListArray[indexPath.row];
    MyCollectionTableViewCell *cell = [MyCollectionTableViewCell cellWithTableView:tableView];
    [cell blindCellFromModel:item];
    cell.type = BlackListType;
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    HomeModel * model = self.blackListArray[indexPath.row];
    [self pushToDetailTableViewController:self userId:model._id];
}

- (NSMutableArray *)blackListArray {
    if (!_blackListArray) {
        _blackListArray = [NSMutableArray array];
    }
    return _blackListArray;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
