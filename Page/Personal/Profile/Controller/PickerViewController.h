//
//  PickerViewController.h
//  SimpleDate
//
//  Created by xu.juvenile on 16/3/9.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "BaseViewController.h"

typedef enum {
    PickerTypeBirthDay,
    PickerTypeCity,
    PickerTypeWeight,
    PickerTypeHeight,
    PickerTypeIncome,
    PickerTypeDateTime,
    PickerTypeAreaCode,
    PickerTypeGender
}PickerType;


@interface PickerViewController : BaseViewController<UIPickerViewDelegate,UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *ensureButton;
@property (weak, nonatomic) IBOutlet UIPickerView *selectPicker;

@property (nonatomic,copy) NSArray *pickerArray;
@property (nonatomic,assign) PickerType pickerType;
@property(nonatomic, copy) void (^ensureBtnClickBlock)(NSString *myText);
@property(nonatomic, copy) void (^ensureAreaCodeBtnClickBlock)(NSString *area ,NSString * code);

- (void)presentInViewController:(UIViewController *)viewController complection:(void(^)())complection;
- (void)dismiss;

@end
