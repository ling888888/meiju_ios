//
//  PickerViewController.m
//  SimpleDate
//
//  Created by xu.juvenile on 16/3/9.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "PickerViewController.h"

#define Key_Division @"Division"
#define Key_DivisionCode @"DivisionCode"
#define Key_DivisionName @"DivisionName"
#define Key_DivisionSub @"DivisionSub"
#define Key_DivisionVersion @"DivisionVersion"
#define KDistrictSelectNotification @"KDistrictSelectNotification"
#define KDistrictSelectDistrict @"KDistrictSelectDistrict"

@interface PickerViewController ()
@property (nonatomic, copy) NSString *myText;
@property (nonatomic, copy) NSString *myArea;

/**
 *  时间选择器
 */
@property (nonatomic, strong) NSMutableArray *multYears;
@property (nonatomic, strong) NSMutableArray *multMouths;
@property (nonatomic, strong) NSMutableArray *multDays;
/*
 * 时间选择器
 */

@property (nonatomic, strong) NSMutableArray *hours;
@property (nonatomic, strong) NSMutableArray *minutes;
/**
 *  地区选择器
 */
@property (nonatomic, copy) NSDictionary *districtDict;
@property (nonatomic, copy) NSArray *provinceArray;
@property (nonatomic, copy) NSArray *cityArray;
@property (nonatomic, copy) NSArray *districtArray;


@property (nonatomic, copy) NSString *provinceStr;
@property (nonatomic, copy) NSString *cityStr;
@property (nonatomic, copy) NSString *districtStr;

@property (nonatomic, copy) NSString *hoursStr;
@property (nonatomic, copy) NSString *minuteStr;

    //区号选择器
@property (nonatomic, strong) NSMutableArray *paArray;
@property (nonatomic, strong) NSMutableArray *areaArray;
@property (nonatomic, strong) NSMutableArray *codeArray;

@property (weak, nonatomic) IBOutlet UIButton *cancleBtn;
@property (weak, nonatomic) IBOutlet UIButton *ensureBtn;

@end

@implementation PickerViewController

- (NSArray *)pickerArray
{
    if (!_pickerArray) {
        _pickerArray = [NSArray array];
    }
    return _pickerArray;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    [self.cancleBtn setTitle:SPDStringWithKey(@"取消", nil) forState:UIControlStateNormal];
    [self.ensureBtn setTitle:SPDStringWithKey(@"确认", nil) forState:UIControlStateNormal];

    if (self.pickerType == PickerTypeBirthDay) {
        [self setupCalendarType];
        self.myText = @"1990-01-01";
    }
    else if (self.pickerType == PickerTypeCity) {
        [self setupProviceType];
        self.myText = @"北京市 北京市 东城区";
    }else if (self.pickerType == PickerTypeDateTime){
        [self setupDateTime];
    }else if (self.pickerType == PickerTypeAreaCode){
        [self setupAreaCode];
    }
    else {
        self.myText = self.pickerArray[0];
    }
}
    
- (void)setupAreaCode {
    
    self.paArray = [NSMutableArray array];
    self.areaArray = [NSMutableArray array];
    self.codeArray = [NSMutableArray array];
    
    self.paArray =[NSMutableArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Areacode" ofType:@"plist"]];
    self.myArea = @"Albania";
    self.myText = @"+355";
    [self.selectPicker selectRow:0 inComponent:0 animated:YES];
    [self.selectPicker selectRow:0 inComponent:1 animated:YES];


}

- (void)setupDateTime{
    //24小时
    self.hours  = [NSMutableArray array];
    self.minutes  = [NSMutableArray array];
    
    for (NSInteger i = 0; i < 24; i++) {
        if (i<10) {
            [self.hours addObject:[NSString stringWithFormat:@"0%zi",i]];
        }else{
          [self.hours addObject:[NSString stringWithFormat:@"%zi", i]];
        }
        
    }
    for (NSInteger i = 0; i < 61; i++) {
        if (i<10) {
            [self.minutes addObject:[NSString stringWithFormat:@"0%zi",i]];
        }else{
            [self.minutes addObject:[NSString stringWithFormat:@"%zi", i]];
        }
    }
    
    self.hoursStr = @"00";
    self.minuteStr = @"00";
    self.myText = [NSString stringWithFormat:@"%@:%@",self.hoursStr,self.minuteStr];
}

- (void)setupCalendarType
{
    self.multYears = [NSMutableArray array];
    self.multMouths = [NSMutableArray array];
    self.multDays = [NSMutableArray array];
    
    for (NSInteger i = 1960; i < 2008; i++) {
        [self.multYears addObject:[NSString stringWithFormat:@"%zi", i]];
    }
    
    for (NSInteger i = 1; i < 13; i++) {
        [self.multMouths addObject:[NSString stringWithFormat:@"%zi", i]];
    }
    
    for (NSInteger i = 1; i < 32; i++) {
        [self.multDays addObject:[NSString stringWithFormat:@"%zi", i]];
    }
    
    //设置pickerView显示指定日期 1990-01-01
    [self.selectPicker selectRow:30 inComponent:0 animated:YES]; //18岁
    [self.selectPicker selectRow:0 inComponent:1 animated:YES];
    [self.selectPicker selectRow:0 inComponent:2 animated:YES];
}

- (void)setupProviceType
{
    self.districtDict = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"districtMGE" ofType:@"plist"]];
    self.provinceArray = self.districtDict[Key_Division];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIPickerViewDelegate && UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (self.pickerType == PickerTypeBirthDay || self.pickerType == PickerTypeCity ) {
        return 3;
    }else if (self.pickerType == PickerTypeDateTime ||self.pickerType == PickerTypeAreaCode){
        return 2;
    }
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (self.pickerType == PickerTypeBirthDay) {
        switch (component) {
            case 0: {
                return self.multYears.count;
                break;
            }
            case 1: {
                return self.multMouths.count;
                break;
            }
            case 2: {
                return self.multDays.count;
                break;
            }
        }
    }
    else if (self.pickerType == PickerTypeCity) {
        if (component == 0) {
            return self.provinceArray.count;
        }
        else if (component == 1) {
            if ([pickerView selectedRowInComponent:0] == -1) {
                return 0;
            }
            else {
                NSDictionary *ProvinceDict = self.provinceArray[[pickerView selectedRowInComponent:0]];
                self.cityArray = [ProvinceDict objectForKey:Key_DivisionSub];
                return [self.cityArray count];
            }
        }
        else {
            if ([pickerView selectedRowInComponent:1] == -1) {
                return 0;
            }
            else {
                NSDictionary *CityDict = self.cityArray[[pickerView selectedRowInComponent:1]];
                self.districtArray = [CityDict objectForKey:Key_DivisionSub];
                return self.districtArray.count;
            }
        }
    }else if (self.pickerType == PickerTypeDateTime){
        if (component == 0) {
            return self.hours.count;
        }else if (component == 1){
            return self.minutes.count;
        }
    }else if (self.pickerType == PickerTypeAreaCode){
        if (component == 0) {
            for (NSArray * arr in self.paArray) {
                for (NSArray *array in arr) {
                    [self.areaArray addObject:array];
                }
            }
            return self.areaArray.count;
        }else if (component == 1){
            for (NSArray * arr in self.paArray) {
                for (NSArray *array in arr) {
                    [self.areaArray addObject:array];
                }
            }
            return self.areaArray.count;
        }
    }

    return self.pickerArray.count;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 30;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{

    if (!view) {
        view = [[UIView alloc] init];
    }

    CGFloat textWidth = self.view.frame.size.width;

    //三列须指定textLabel宽度否则错位
    if (self.pickerType == PickerTypeBirthDay||self.pickerType == PickerTypeCity) {
        textWidth = 120;
    }else if (self.pickerType == PickerTypeDateTime || self.pickerType == PickerTypeAreaCode){
        CGFloat screenWidth = SCREEN_WIDTH;
        textWidth = screenWidth/2;
    }
    UILabel *text = [[UILabel alloc]
        initWithFrame:CGRectMake(0, 0, textWidth, 20)];

    text.textAlignment = NSTextAlignmentCenter;

    if (self.pickerType == PickerTypeWeight) {
        text.text = [NSString stringWithFormat:@"%@ kg", self.pickerArray[row]];
    }
    else if (self.pickerType == PickerTypeBirthDay) {
        switch (component) {
            case 0:
                text.text = [self.multYears[row] stringByAppendingString:@""];
                break;
            case 1:
                text.text = [self.multMouths[row] stringByAppendingString:@""];
                break;
            case 2:
                text.text = [self.multDays[row] stringByAppendingString:@""];
                break;
        }
    }
    else if (self.pickerType == PickerTypeHeight) {
        text.text = [NSString stringWithFormat:@"%@ cm", self.pickerArray[row]];
    }
    else if (self.pickerType == PickerTypeCity) {
        switch (component) {
            case 0: {
                NSDictionary *ProvinceDict = self.provinceArray[row];
                text.text = [ProvinceDict objectForKey:Key_DivisionName];
                break;
            }
            case 1: {
                NSDictionary *CityDict = self.cityArray[row];
                text.text = [CityDict objectForKey:Key_DivisionName];
                break;
            }
            case 2: {
                NSDictionary *DistrictDict = self.districtArray[row];
                text.text = [DistrictDict objectForKey:Key_DivisionName];
                break;
            }
            default:
                break;
        }
    }else if (self.pickerType == PickerTypeDateTime){
        if (component == 0) {
            text.text = self.hours[row];
        }else{
            text.text = self.minutes[row];
        }
        
    }else if (self.pickerType == PickerTypeAreaCode) {
        switch (component) {
            case 0: {
                NSArray *array = self.areaArray[row];
                text.text = array[0];
                break;
            }
            case 1: {
                NSArray *array = self.areaArray[row];
                text.text = array[1];
                break;
            }
            default:
            break;
        }
    } else if (self.pickerType == PickerTypeGender) {
        text.text = [self.pickerArray[row] isEqualToString:@"female"] ? SPDStringWithKey(@"女", nil) : SPDStringWithKey(@"男", nil);
    }
    else{
        text.text = self.pickerArray[row];
    }

    [view addSubview:text];

    return view;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if (self.pickerType == PickerTypeBirthDay) {

        NSString *strDate = [NSString stringWithFormat:@"%@-%@-%@",
                                                       self.multYears[[pickerView selectedRowInComponent:0]],
                                                       self.multMouths[[pickerView selectedRowInComponent:1]],
                                                       self.multDays[[pickerView selectedRowInComponent:2]]];

        //二次转换
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        [formatter setDateFormat:@"yyyy-MM-dd"];
//        NSDate *date = [formatter dateFromString:strDate];
        NSString  * month = @"";
        if ([self.multMouths[[pickerView selectedRowInComponent:1]] integerValue] < 10) {
           month=[NSString stringWithFormat:@"0%@",self.multMouths[[pickerView selectedRowInComponent:1]]];
        }else{
            month=[NSString stringWithFormat:@"%@",self.multMouths[[pickerView selectedRowInComponent:1]]];
        }
        NSString *day=@"";
        if ([self.multDays[[pickerView selectedRowInComponent:2]] integerValue] < 10) {
            day=[NSString stringWithFormat:@"0%@",self.multDays[[pickerView selectedRowInComponent:2]]];
        }else{
            day=[NSString stringWithFormat:@"%@",self.multDays[[pickerView selectedRowInComponent:2]]];
        }
        NSString *birtday=[NSString stringWithFormat:@"%@-%@-%@", self.multYears[[pickerView selectedRowInComponent:0]],month,day];
        NSLog(@"NSDateComponents= %@",birtday);
        self.myText = birtday;
    }else if (self.pickerType == PickerTypeCity){
        if (component == 0) {
            [pickerView reloadComponent:1];
            
            [pickerView reloadComponent:2];//***
            [self getSelectDistrictName];
        } else if (component == 1) {
            [pickerView reloadComponent:2];//***
            [self getSelectDistrictName];
        } else {
            [self getSelectDistrictName];
        }
        
    }else if (self.pickerType == PickerTypeDateTime){
        if (component == 0) {
            self.hoursStr = [NSString stringWithFormat:@"%@",self.hours[[pickerView selectedRowInComponent:0]]];
        }else{
            self.minuteStr = [NSString stringWithFormat:@"%@",self.hours[[pickerView selectedRowInComponent:1]]];
        }
        self.myText = [NSString stringWithFormat:@"%@ %@",self.hoursStr,self.minuteStr];
    }else if (self.pickerType == PickerTypeAreaCode) {
        if (component == 0) {
            [self.selectPicker selectRow:row inComponent:1 animated:YES];

        }else{
            [self.selectPicker selectRow:row inComponent:0 animated:YES];
        }
        self.myArea = self.areaArray[row][0];
        self.myText = self.areaArray[row][1];
    }
    else {
        self.myText = self.pickerArray[row];
    }
    
}

#pragma mark - 按钮跟视图显现方法

- (IBAction)cancelBtnClick:(UIButton *)sender
{
    [self dismiss];
}

- (void)dismiss
{
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

- (IBAction)ensureBtnClick:(UIButton *)sender
{
    if (self.pickerType == PickerTypeAreaCode) {
        self.ensureAreaCodeBtnClickBlock(self.myArea, self.myText);
    }else{
        self.ensureBtnClickBlock(self.myText);
    }
    NSLog(@"ensureBtnClickBlockText=%@", self.myText);
    [self dismiss];
}

- (void)presentInViewController:(UIViewController *)viewController complection:(void (^)())complection
{
    [viewController.view addSubview:self.view];
    [viewController addChildViewController:self];
    complection();
}

#pragma mark - 地区选择结果

- (void)getSelectDistrictName
{
    NSDictionary *ProvinceDict = self.provinceArray[[self.selectPicker selectedRowInComponent:0]];
    self.provinceStr = [ProvinceDict objectForKey:Key_DivisionName];


    NSArray *array = [ProvinceDict objectForKey:Key_DivisionSub];
    if ([self.selectPicker selectedRowInComponent:1] > array.count - 1) {
        return;
    }
    NSDictionary *CityDict = [[ProvinceDict objectForKey:Key_DivisionSub] objectAtIndex:[self.selectPicker selectedRowInComponent:1]];
    self.cityStr = [CityDict objectForKey:Key_DivisionName];
    self.districtStr = [self.districtArray[[self.selectPicker selectedRowInComponent:2]] objectForKey:Key_DivisionName];

    self.myText = [NSString stringWithFormat:@"%@ %@ %@", self.provinceStr, self.cityStr, self.districtStr];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
