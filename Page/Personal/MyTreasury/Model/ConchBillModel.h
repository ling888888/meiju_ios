//
//  ConchBillModel.h
//  SimpleDate
//
//  Created by 李楠 on 2020/4/10.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "CommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ConchBillModel : CommonModel

@property (nonatomic, copy) NSNumber *tradeStatus;
@property (nonatomic, copy) NSNumber *conchNumber;
@property (nonatomic, copy) NSString *time;
@property (nonatomic, copy) NSString *type;

@end

NS_ASSUME_NONNULL_END
