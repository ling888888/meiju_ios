//
//  ConchBillModel.m
//  SimpleDate
//
//  Created by 李楠 on 2020/4/10.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "ConchBillModel.h"

@implementation ConchBillModel

- (void)setTime:(NSString *)time {
    NSDateFormatter *formater = [NSDateFormatter new];
    [formater setTimeZone:[NSTimeZone localTimeZone]];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:time.longLongValue / 1000];
    _time = [formater stringFromDate:date];
}

@end
