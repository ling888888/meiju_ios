//
//  MyWalletListModel.h
//  SimpleDate
//
//  Created by 李楠 on 2020/4/14.
//

#import "CommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyWalletListModel : CommonModel

@property (nonatomic, copy) NSString *productId;
@property (nonatomic, copy) NSString *number;
@property (nonatomic, copy) NSString *price;

@end

NS_ASSUME_NONNULL_END
