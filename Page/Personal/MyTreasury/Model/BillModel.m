//
//  BillModel.m
//  SimpleDate
//
//  Created by 李楠 on 2019/2/21.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "BillModel.h"

@implementation BillModel

- (void)setCreate_time:(NSNumber *)create_time {
    _create_time = create_time;
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setTimeZone:[NSTimeZone localTimeZone]];
    [format setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:[_create_time longLongValue] / 1000];
    self.time = [format stringFromDate:date];
}

@end
