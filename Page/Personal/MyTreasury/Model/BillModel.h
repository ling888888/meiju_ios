//
//  BillModel.h
//  SimpleDate
//
//  Created by 李楠 on 2019/2/21.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "CommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BillModel : CommonModel

@property (nonatomic, copy) NSString *_id;
@property (nonatomic, copy) NSString *type; //["add","del"][收入，支出] 贵族账单列表 和正常的不一样
@property (nonatomic, copy) NSString *status;
@property (nonatomic, strong) NSNumber *sum;
@property (nonatomic, strong) NSNumber *create_time;
@property (nonatomic, copy) NSString *time;
@property (nonatomic, copy) NSString *currencyType;
@property (nonatomic, copy) NSString * sub_title; //{ '充值': 'recharge', '每日消耗': 'outgo', '管理员充值': 'admin_recharge', '管理员扣款': 'admin_outgo', '购买会员': 'vip' } 贵族账单列表

@end

NS_ASSUME_NONNULL_END
