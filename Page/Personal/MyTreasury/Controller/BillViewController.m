//
//  BillViewController.m
//  SimpleDate
//
//  Created by 程龙军 on 15/12/15.
//  Copyright © 2015年 WYB. All rights reserved.
//

#import "BillViewController.h"
#import "LazyPageScrollView.h"
#import "BillCell.h"
#import "BillModel.h"
#import "ConchBillModel.h"

#define LazyPageScrollViewTabHeight 58

@interface BillViewController ()<UITableViewDataSource>

@property (nonatomic, strong) LazyPageScrollView *lazyPageScrollView;
@property (nonatomic, strong) UITableView *goldTableView;
@property (nonatomic, strong) UITableView *diamondTableView;
@property (nonatomic, strong) LazyPageScrollView *conchPageScrollView;
@property (nonatomic, strong) UITableView *conchIncomeTableView;
@property (nonatomic, strong) UITableView *conchOutcomeTableView;
@property (nonatomic, assign) NSInteger goldPage;
@property (nonatomic, assign) NSInteger diamondPage;
@property (nonatomic, assign) NSInteger conchIncomePage;
@property (nonatomic, assign) NSInteger conchOutcomePage;
@property (nonatomic, strong) NSMutableArray *goldArr;
@property (nonatomic, strong) NSMutableArray *diamondArr;
@property (nonatomic, strong) NSMutableArray *conchIncomeArr;
@property (nonatomic, strong) NSMutableArray *conchOutcomeArr;

@end

@implementation BillViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.navigationController.navigationBar vhl_setBackgroundColor:[UIColor whiteColor]];
//    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.title = SPDStringWithKey(@"账单", nil);
    [self.view addSubview:self.lazyPageScrollView];
    [self requestBillListWithCurrencyType:@"gold"];
    [self requestBillListWithCurrencyType:@"jewel"];
    self.conchIncomePage = 1;
    [self requestConchBillType:@"0"];
    self.conchOutcomePage = 1;
    [self requestConchBillType:@"1"];
}

- (void)requestBillListWithCurrencyType:(NSString *)currencyType {
    UITableView *tableView;
    NSInteger page;
    NSMutableArray *dataArr;
    if ([currencyType isEqualToString:@"gold"]) {
        tableView = self.goldTableView;
        page = self.goldPage;
        dataArr = self.goldArr;
    } else {
        tableView = self.diamondTableView;
        page = self.diamondPage;
        dataArr = self.diamondArr;
    }
    NSDictionary *dic = @{@"currency_type": currencyType, @"page": @(page)};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"bill.list" bAnimated:NO  bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if (page == 0) {
            [dataArr removeAllObjects];
        }
        NSArray *bills = suceess[@"bills"];
        for (NSDictionary *dic in bills) {
            if (![dic[@"status"] isEqualToString:@"process"]) {
                BillModel *model = [BillModel initWithDictionary:dic];
                model.currencyType = currencyType;
                [dataArr addObject:model];
            }
        }
        [tableView reloadData];
        if ([currencyType isEqualToString:@"gold"]) {
            self.goldPage++;
        } else {
            self.diamondPage++;
        }
        
        tableView.mj_header.hidden = NO;
        [tableView.mj_header endRefreshing];
        if (tableView.mj_footer.state == MJRefreshStateRefreshing) {
            [tableView.mj_footer endRefreshingWithCompletionBlock:^{
                tableView.mj_footer.hidden = !bills.count;
            }];
        } else {
            tableView.mj_footer.hidden = !bills.count;
        }
    } bFailure:^(id  _Nullable failure) {
        tableView.mj_header.hidden = NO;
        [tableView.mj_header endRefreshing];
        [tableView.mj_footer endRefreshing];
    }];
}

- (void)requestConchBillType:(NSString *)type {
    UITableView *tableView;
    NSInteger page;
    NSMutableArray *dataArr;
    if ([type isEqualToString:@"0"]) {
        tableView = self.conchIncomeTableView;
        page = self.conchIncomePage;
        dataArr = self.conchIncomeArr;
    } else {
        tableView = self.conchOutcomeTableView;
        page = self.conchOutcomePage;
        dataArr = self.conchOutcomeArr;
    }
    NSDictionary *params = @{@"type": type, @"pageNumber": @(page)};
    [RequestUtils GET:URL_NewServer(@"sys/conch.bill") parameters:params success:^(id  _Nullable suceess) {
        if (page == 1) {
            [dataArr removeAllObjects];
        }
        NSArray *rows = suceess[@"rows"];
        for (NSDictionary *dic in rows) {
            ConchBillModel *model = [ConchBillModel initWithDictionary:dic];
            model.type = type;
            [dataArr addObject:model];
        }
        [tableView reloadData];
        if ([type isEqualToString:@"0"]) {
            self.conchIncomePage++;
        } else {
            self.conchOutcomePage++;
        }
        
        tableView.mj_header.hidden = NO;
        [tableView.mj_header endRefreshing];
        if (tableView.mj_footer.state == MJRefreshStateRefreshing) {
            [tableView.mj_footer endRefreshingWithCompletionBlock:^{
                tableView.mj_footer.hidden = !rows.count;
            }];
        } else {
            tableView.mj_footer.hidden = !rows.count;
        }
    } failure:^(id  _Nullable failure) {
        tableView.mj_header.hidden = NO;
        [tableView.mj_header endRefreshing];
        [tableView.mj_footer endRefreshing];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.goldTableView) {
        return self.goldArr.count;
    } else if (tableView == self.diamondTableView) {
        return self.diamondArr.count;
    } else if (tableView == self.conchIncomeTableView) {
        return self.conchIncomeArr.count;
    } else {
        return self.conchOutcomeArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BillCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BillCell" forIndexPath:indexPath];
    if (tableView == self.goldTableView) {
        cell.model = self.goldArr[indexPath.row];
    } else if (tableView == self.diamondTableView) {
        cell.model = self.diamondArr[indexPath.row];
    } else if (tableView == self.conchIncomeTableView) {
        cell.conchBillModel = self.conchIncomeArr[indexPath.row];
    } else {
        cell.conchBillModel = self.conchOutcomeArr[indexPath.row];
    }
    return cell;
}

#pragma mark - setters & getters

- (LazyPageScrollView *)lazyPageScrollView {
    if (!_lazyPageScrollView) {
        _lazyPageScrollView = [[LazyPageScrollView alloc] initWithFrame:CGRectMake(0, NavHeight, kScreenW, kScreenH - NavHeight)];
        _lazyPageScrollView.backgroundColor = [UIColor whiteColor];
        [_lazyPageScrollView initTab:YES Gap:0 TabHeight:LazyPageScrollViewTabHeight VerticalDistance:1 BkColor:[UIColor whiteColor]];
        [_lazyPageScrollView setTitleStyle:[UIFont systemFontOfSize:15] SelFont:[UIFont systemFontOfSize:15] Color:[UIColor colorWithHexString:@"333333"] SelColor:[UIColor colorWithHexString:@"333333"]];
        [_lazyPageScrollView enableBreakLine:YES Width:1 TopMargin:16 BottomMargin:16 Color:[UIColor colorWithHexString:@"DDDDDD"]];
        [_lazyPageScrollView enableTabBottomLine:YES LineHeight:2 LineColor:[UIColor colorWithHexString:COMMON_PINK] LineBottomGap:0 ExtraWidth:60];
        [_lazyPageScrollView addTab:SPDStringWithKey(@"金币", nil) View:self.goldTableView Info:nil];
        [_lazyPageScrollView addTab:SPDStringWithKey(@"钻石", nil) View:self.diamondTableView Info:nil];
        [_lazyPageScrollView addTab:SPDStringWithKey(@"贝壳", nil) View:self.conchPageScrollView Info:nil];
        [_lazyPageScrollView generate:nil];
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, LazyPageScrollViewTabHeight, kScreenW, 1)];
        view.backgroundColor = [UIColor colorWithHexString:@"DDDDDD"];
        [_lazyPageScrollView addSubview:view];
    }
    return _lazyPageScrollView;
}

- (UITableView *)goldTableView {
    if (!_goldTableView) {
        _goldTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH - NavHeight - LazyPageScrollViewTabHeight) style:UITableViewStylePlain];
        _goldTableView.dataSource = self;
        _goldTableView.rowHeight = 66;
        _goldTableView.tableFooterView = [UIView new];
        [_goldTableView registerNib:[UINib nibWithNibName:@"BillCell" bundle:nil] forCellReuseIdentifier:@"BillCell"];
        
        __weak typeof(self) weakSelf = self;
        _goldTableView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            weakSelf.goldPage = 0;
            [weakSelf requestBillListWithCurrencyType:@"gold"];
        }];
        _goldTableView.mj_header.hidden = YES;
        _goldTableView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            [weakSelf requestBillListWithCurrencyType:@"gold"];
        }];
        _goldTableView.mj_footer.hidden = YES;
    }
    return _goldTableView;
}

- (UITableView *)diamondTableView {
    if (!_diamondTableView) {
        _diamondTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH - NavHeight - LazyPageScrollViewTabHeight) style:UITableViewStylePlain];
        _diamondTableView.dataSource = self;
        _diamondTableView.rowHeight = 66;
        _diamondTableView.tableFooterView = [UIView new];
        [_diamondTableView registerNib:[UINib nibWithNibName:@"BillCell" bundle:nil] forCellReuseIdentifier:@"BillCell"];
        
        __weak typeof(self) weakSelf = self;
        _diamondTableView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            weakSelf.diamondPage = 0;
            [weakSelf requestBillListWithCurrencyType:@"jewel"];
        }];
        _diamondTableView.mj_header.hidden = YES;
        _diamondTableView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            [weakSelf requestBillListWithCurrencyType:@"jewel"];
        }];
        _diamondTableView.mj_footer.hidden = YES;
    }
    return _diamondTableView;
}

- (LazyPageScrollView *)conchPageScrollView {
    if (!_conchPageScrollView) {
        _conchPageScrollView = [[LazyPageScrollView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH - NavHeight - LazyPageScrollViewTabHeight)];
        _conchPageScrollView.backgroundColor = [UIColor whiteColor];
        [_conchPageScrollView initTab:YES Gap:0 TabHeight:LazyPageScrollViewTabHeight VerticalDistance:1 BkColor:[UIColor whiteColor]];
        [_conchPageScrollView setTitleStyle:[UIFont systemFontOfSize:15] SelFont:[UIFont systemFontOfSize:15] Color:[UIColor colorWithHexString:@"333333"] SelColor:[UIColor colorWithHexString:@"333333"]];
        [_conchPageScrollView enableBreakLine:YES Width:1 TopMargin:16 BottomMargin:16 Color:[UIColor colorWithHexString:@"DDDDDD"]];
        [_conchPageScrollView enableTabBottomLine:YES LineHeight:2 LineColor:[UIColor colorWithHexString:COMMON_PINK] LineBottomGap:0 ExtraWidth:60];
        [_conchPageScrollView addTab:SPDStringWithKey(@"收入", nil) View:self.conchIncomeTableView Info:nil];
        [_conchPageScrollView addTab:SPDStringWithKey(@"支出", nil) View:self.conchOutcomeTableView Info:nil];
        [_conchPageScrollView generate:nil];
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, LazyPageScrollViewTabHeight, kScreenW, 1)];
        view.backgroundColor = [UIColor colorWithHexString:@"DDDDDD"];
        [_conchPageScrollView addSubview:view];
    }
    return _conchPageScrollView;
}

- (UITableView *)conchIncomeTableView {
    if (!_conchIncomeTableView) {
        _conchIncomeTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH - NavHeight - LazyPageScrollViewTabHeight * 2) style:UITableViewStylePlain];
        _conchIncomeTableView.dataSource = self;
        _conchIncomeTableView.rowHeight = 66;
        _conchIncomeTableView.tableFooterView = [UIView new];
        [_conchIncomeTableView registerNib:[UINib nibWithNibName:@"BillCell" bundle:nil] forCellReuseIdentifier:@"BillCell"];
        
        __weak typeof(self) weakSelf = self;
        _conchIncomeTableView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            weakSelf.conchIncomePage = 1;
            [weakSelf requestConchBillType:@"0"];
        }];
        _conchIncomeTableView.mj_header.hidden = YES;
        _conchIncomeTableView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            [weakSelf requestConchBillType:@"0"];
        }];
        _conchIncomeTableView.mj_footer.hidden = YES;
    }
    return _conchIncomeTableView;
}

- (UITableView *)conchOutcomeTableView {
    if (!_conchOutcomeTableView) {
        _conchOutcomeTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH - NavHeight - LazyPageScrollViewTabHeight * 2) style:UITableViewStylePlain];
        _conchOutcomeTableView.dataSource = self;
        _conchOutcomeTableView.rowHeight = 66;
        _conchOutcomeTableView.tableFooterView = [UIView new];
        [_conchOutcomeTableView registerNib:[UINib nibWithNibName:@"BillCell" bundle:nil] forCellReuseIdentifier:@"BillCell"];
        
        __weak typeof(self) weakSelf = self;
        _conchOutcomeTableView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            weakSelf.conchOutcomePage = 1;
            [weakSelf requestConchBillType:@"1"];
        }];
        _conchOutcomeTableView.mj_header.hidden = YES;
        _conchOutcomeTableView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            [weakSelf requestConchBillType:@"1"];
        }];
        _conchOutcomeTableView.mj_footer.hidden = YES;
    }
    return _conchOutcomeTableView;
}

- (NSMutableArray *)goldArr {
    if (!_goldArr) {
        _goldArr = [NSMutableArray new];
    }
    return _goldArr;
}

- (NSMutableArray *)diamondArr {
    if (!_diamondArr) {
        _diamondArr = [NSMutableArray new];
    }
    return _diamondArr;
}

- (NSMutableArray *)conchIncomeArr {
    if (!_conchIncomeArr) {
        _conchIncomeArr = [NSMutableArray new];
    }
    return _conchIncomeArr;
}

- (NSMutableArray *)conchOutcomeArr {
    if (!_conchOutcomeArr) {
        _conchOutcomeArr = [NSMutableArray new];
    }
    return _conchOutcomeArr;
}

@end
