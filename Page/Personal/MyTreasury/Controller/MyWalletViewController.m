//
//  MyWalletViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2020/4/10.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "MyWalletViewController.h"
#import "BillViewController.h"
#import <WebKit/WebKit.h>

@interface MyWalletViewController ()<WKNavigationDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UILabel *goldLabel;
@property (weak, nonatomic) IBOutlet UILabel *diamondLabel;
@property (weak, nonatomic) IBOutlet UILabel *conchLabel;

@property (nonatomic, strong) UIButton *billButton;
@property (nonatomic, strong) CAGradientLayer *goldGradientLayer;
@property (nonatomic, strong) CAGradientLayer *diamondGradientLayer;
@property (nonatomic, strong) CAGradientLayer *conchGradientLayer;
@property (nonatomic, strong) WKWebView *webView;

@end

@implementation MyWalletViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = SPDStringWithKey(@"我的钱包", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.billButton];
    
    [self configureSubviews];
    [self requestMyBalance];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:0.0];
    self.navigationController.navigationBar.titleTextAttributes = @{
        NSFontAttributeName:[UIFont mediumFontOfSize:18],
        NSForegroundColorAttributeName:[UIColor colorWithHexString:@"ffffff"]
    };
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.tintColor = SPD_HEXCOlOR(@"1a1a1a");
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{
        NSFontAttributeName:[UIFont mediumFontOfSize:18],
        NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#1A1A1A"]
    };
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    self.goldGradientLayer.frame = self.goldLabel.superview.bounds;
    self.diamondGradientLayer.frame = self.diamondLabel.superview.bounds;
    self.conchGradientLayer.frame = self.conchLabel.superview.bounds;
    [CATransaction commit];
}

#pragma mark - Private methods

- (void)configureSubviews {
    self.topConstraint.constant = StatusBarHeight + 53.5;
    
    self.goldGradientLayer = [self createCAGradientLayer];
    [self.goldLabel.superview.layer insertSublayer:self.goldGradientLayer atIndex:0];

    self.diamondGradientLayer = [self createCAGradientLayer];
    [self.diamondLabel.superview.layer insertSublayer:self.diamondGradientLayer atIndex:0];

    self.conchGradientLayer = [self createCAGradientLayer];
    [self.conchLabel.superview.layer insertSublayer:self.conchGradientLayer atIndex:0];
    
    [self.view addSubview:self.webView];
}

- (CAGradientLayer *)createCAGradientLayer {
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.cornerRadius = 13;
    if (kIsMirroredLayout) {
        gradientLayer.startPoint = CGPointMake(1, 0.5);
        gradientLayer.endPoint = CGPointMake(0, 0.5);
    } else {
        gradientLayer.startPoint = CGPointMake(0, 0.5);
        gradientLayer.endPoint = CGPointMake(1, 0.5);
    }
    gradientLayer.colors = @[(__bridge id)[UIColor colorWithWhite:1 alpha:0.3].CGColor, (__bridge id)[UIColor colorWithWhite:1 alpha:0].CGColor];
    return gradientLayer;
}

- (void)requestMyBalance {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.balance" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.goldLabel.text = [suceess[@"balance"] stringValue];
        self.diamondLabel.text = [suceess[@"jewelBalance"] stringValue];
        self.conchLabel.text = [suceess[@"conch"] stringValue];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

#pragma mark - Event responses

- (void)didClickBillButton:(UIButton *)sender {
    [self.navigationController pushViewController:[BillViewController new] animated:YES];
}

#pragma mark - WKNavigationDelegate

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    NSString *requestURL = navigationAction.request.URL.absoluteString;
    if ([requestURL containsString:@"famy.ly/mobilePayResult.html"]) {
        [self requestMyBalance];
    }
    decisionHandler(WKNavigationActionPolicyAllow);
}

#pragma mark - Setters & Getters

- (WKWebView *)webView {
    if (!_webView) {
        _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, StatusBarHeight + 162, kScreenW, kScreenH - (StatusBarHeight + 162))];
        _webView.backgroundColor = [UIColor whiteColor];
        _webView.navigationDelegate = self;
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://%@share.famy.ly/view/ar/payNew.html?user_id=%@&lang=%@", DEV ? @"dev-" : @"", [SPDApiUser currentUser].userId, [SPDCommonTool getFamyLanguage]]]];
        [_webView loadRequest:request];
    }
    return _webView;
}

- (UIButton *)billButton {
    if (!_billButton) {
        _billButton = [UIButton buttonWithType:UIButtonTypeSystem];
        _billButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_billButton setTitle:SPDLocalizedString(@"账单") forState:UIControlStateNormal];
        [_billButton addTarget:self action:@selector(didClickBillButton:) forControlEvents:UIControlEventTouchUpInside];
        [_billButton sizeToFit];
    }
    return _billButton;
}

@end
