//
//  BillCell.m
//  SimpleDate
//
//  Created by 程龙军 on 15/12/15.
//  Copyright © 2015年 WYB. All rights reserved.
//

#import "BillCell.h"
#import "BillModel.h"
#import "ConchBillModel.h"

@interface BillCell ()

@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIImageView *currencyImageView;
@property (weak, nonatomic) IBOutlet UILabel *sumLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sumLabelLeading;

@end

@implementation BillCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setModel:(BillModel *)model {
    _model = model;
    
    if (!_model.sub_title.notEmpty) {
        if ([_model.type isEqualToString:@"recharge"]) {
            self.typeLabel.text = SPDStringWithKey(@"充值", nil);
        } else if ([_model.type isEqualToString:@"income"]) {
            self.typeLabel.text = SPDStringWithKey(@"收入", nil);
        } else if ([_model.type isEqualToString:@"outgo"]) {
            self.typeLabel.text = SPDStringWithKey(@"支出", nil);
        }
        if ([_model.currencyType isEqualToString:@"gold"]) {
            self.currencyImageView.image = [UIImage imageNamed:@"ic_invitation_gold"];
        } else if ([_model.currencyType isEqualToString:@"jewel"]) {
            self.currencyImageView.image = [UIImage imageNamed:@"ic_diamond"];
        }
        self.sumLabel.text = _model.sum.stringValue;
    }else{
        self.currencyImageView.hidden = YES;
        self.sumLabelLeading.constant = -16;
        if ([_model.sub_title isEqualToString:@"recharge"]) {
            self.typeLabel.text = SPDStringWithKey(@"充值", nil);
        } else if ([_model.sub_title isEqualToString:@"outgo"]) {
            self.typeLabel.text = SPDStringWithKey(@"每日消耗", nil);
        } else if ([_model.sub_title isEqualToString:@"admin_recharge"]) {
            self.typeLabel.text = SPDStringWithKey(@"管理员充值", nil);
        } else if ([_model.sub_title isEqualToString:@"admin_outgo"]) {
            self.typeLabel.text = SPDStringWithKey(@"管理员扣款", nil);
        } else if ([_model.sub_title isEqualToString:@"vip"]) {
            self.typeLabel.text = SPDStringWithKey(@"购买会员", nil);
        } else if ([_model.sub_title isEqualToString:@"outgo_decorate_pet"]) {
            self.typeLabel.text = SPDStringWithKey(@"开宝箱", nil);
        }
        self.sumLabel.text = [NSString stringWithFormat:@"%@%@",[_model.type isEqualToString:@"add"]?@"+":@"-",_model.sum.stringValue];

    }
    
    if ([_model.status isEqualToString:@"process"]) {
        self.statusLabel.text = SPDStringWithKey(@"处理中", nil);
    } else if ([_model.status isEqualToString:@"done"]) {
        self.statusLabel.text = SPDStringWithKey(@"已完成", nil);
    } else if ([_model.status isEqualToString:@"cancel"]) {
        self.statusLabel.text = SPDStringWithKey(@"已取消", nil);
    }
    self.timeLabel.text = _model.time;
}

- (void)setConchBillModel:(ConchBillModel *)conchBillModel {
    _conchBillModel = conchBillModel;
    
    switch (_conchBillModel.type.integerValue) {
        case 0:
            self.typeLabel.text = SPDStringWithKey(@"收入", nil);
            break;
        default:
            self.typeLabel.text = SPDStringWithKey(@"支出", nil);
            break;
    }
    switch (_conchBillModel.tradeStatus.integerValue) {
        case 1:
            self.statusLabel.text = SPDStringWithKey(@"已完成", nil);
            break;
        default:
            self.statusLabel.text = SPDStringWithKey(@"已退款", nil);
            break;
    }
    self.currencyImageView.image = [UIImage imageNamed:@"ic_zhibo_liwuhe_beike_xiao"];
    self.sumLabel.text = _conchBillModel.conchNumber.stringValue;
    self.timeLabel.text = _conchBillModel.time;
}

@end
