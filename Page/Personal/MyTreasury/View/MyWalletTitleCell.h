//
//  MyWalletTitleCell.h
//  SimpleDate
//
//  Created by 李楠 on 2020/4/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyWalletTitleCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *cautionLabel;

@end

NS_ASSUME_NONNULL_END
