//
//  MyWalletBottomCell.h
//  SimpleDate
//
//  Created by 李楠 on 2020/4/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyWalletBottomCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton *feedbackButton;

@end

NS_ASSUME_NONNULL_END
