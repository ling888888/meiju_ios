//
//  MyWalletConchListCell.m
//  SimpleDate
//
//  Created by 李楠 on 2020/4/14.
//

#import "MyWalletConchListCell.h"
#import "MyWalletListModel.h"

@interface MyWalletConchListCell ()

@property (weak, nonatomic) IBOutlet UILabel *conchLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@end

@implementation MyWalletConchListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.layer.shadowColor = [UIColor colorWithRed:153 / 255.0 green:153 / 255.0 blue:153 / 255.0 alpha:0.09].CGColor;
    self.layer.shadowOffset = CGSizeMake(0, 0);
    self.layer.shadowOpacity = 1;
    self.layer.shadowRadius = 6;
}

- (void)setModel:(MyWalletListModel *)model {
    _model = model;
    
    self.conchLabel.text = _model.number;
    self.priceLabel.text = _model.price;
}

@end
