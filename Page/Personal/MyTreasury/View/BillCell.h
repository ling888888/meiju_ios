//
//  BillCell.h
//  SimpleDate
//
//  Created by 程龙军 on 15/12/15.
//  Copyright © 2015年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BillModel, ConchBillModel;

@interface BillCell : UITableViewCell

@property (nonatomic, strong) BillModel *model;
@property (nonatomic, strong) ConchBillModel *conchBillModel;

@end
