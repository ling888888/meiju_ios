//
//  MyWalletGoldListCell.h
//  SimpleDate
//
//  Created by 李楠 on 2020/4/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class MyWalletListModel;

@interface MyWalletGoldListCell : UICollectionViewCell

@property (nonatomic, strong) MyWalletListModel *model;

@end

NS_ASSUME_NONNULL_END
