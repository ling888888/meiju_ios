//
//  presentReceivcCVCell.m
//  SimpleDate
//
//  Created by 侯玲 on 16/10/11.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "presentReceivcCVCell.h"

@interface presentReceivcCVCell ()
@property (weak, nonatomic) IBOutlet UIImageView *presentImageView;
@property (weak, nonatomic) IBOutlet UILabel *presentCountLabel;


@end

@implementation presentReceivcCVCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


-(void)setModel:(GiftListModel *)model{
    _model = model;
    if (!_model._id.notEmpty) {
        _presentImageView.image = [UIImage imageNamed:@"ic_caifubang_zanwuliwu"];
        _model.count = @(1);
        _presentImageView.contentMode = UIViewContentModeCenter;
    }else{
        _presentImageView.contentMode = UIViewContentModeScaleAspectFit;
        [_presentImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,model.cover]]];
    }
    self.presentCountLabel.hidden = ([model.count intValue] == 1);
    _presentCountLabel.text = [NSString stringWithFormat:@"x%@", [model.count stringValue]];
}

@end
