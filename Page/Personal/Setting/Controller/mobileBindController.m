//
//  mobileBindController.m
//  SimpleDate
//
//  Created by 侯玲 on 16/8/31.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "mobileBindController.h"
#import "SPDHashGenerator.h"
#import "PickerViewController.h"
#import "SelectAreaCodeController.h"

@interface mobileBindController ()<UITextFieldDelegate,SelectAreaCodeControllerDelegate>
{
    NSTimer* timer;
}
@property (weak, nonatomic) IBOutlet UITextField *mobieTextField;
@property (weak, nonatomic) IBOutlet UITextField *verifyCodeTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *getVerifyCodeBtn;
@property(nonatomic,copy)NSString *phoneNumber;
@property(nonatomic,copy)NSString *veriftyCode;
@property(nonatomic,copy)NSString *password;
@property(nonatomic,assign)int second;
@property (weak, nonatomic) IBOutlet UILabel *lajilabel;
@property (weak, nonatomic) IBOutlet UIButton *comfirmBtn;

@property (weak, nonatomic) IBOutlet UIButton *codeBtn;
@property (weak, nonatomic) IBOutlet UILabel *countryLabel;
@property(nonatomic, copy) NSString *lac; //区号

@end

@implementation mobileBindController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //默认选择
    self.lac = @"966";
    self.countryLabel.text = @"Saudi Arabia";
    [self.codeBtn setTitle:self.lac forState:UIControlStateNormal];
    self.mobieTextField.placeholder = SPDStringWithKey(@"请输入您的手机号", nil);
    self.verifyCodeTextField.placeholder = SPDStringWithKey(@"请输入验证码", nil);
    [self.getVerifyCodeBtn setTitle:SPDStringWithKey(@"获取验证码", nil) forState:UIControlStateNormal];
    self.passwordTextField.placeholder = SPDStringWithKey(@"设置您的密码(6-16位)", nil);
    
    [self requestSignupAreaCode];
    
  
     NSDictionary *attribtDic = @{NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
     NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:@"其他问题？".localized attributes:attribtDic];
     //赋值
     self.lajilabel.attributedText = attribtStr;
    self.lajilabel.hidden = !self.isResetPwd;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gongzitaidi)];
    self.lajilabel.userInteractionEnabled = YES;
    [self.lajilabel addGestureRecognizer:tap];
}

- (void)gongzitaidi {
    [self pushToConversationViewControllerWithTargetId:@"13"];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.second=60;
    self.timeLabel.hidden=NO;
    [self configureNav];
    [self checkViewControllerType];
    
}
-(void)configureNav{
    UIBarButtonItem *leftBarItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ic_arrow_left"].adaptiveRtl style:UIBarButtonItemStylePlain target:self action:@selector(dealLeftBarItemClicked:)];
    self.navigationItem.leftBarButtonItem = leftBarItem;
}

-(void)dealLeftBarItemClicked:(UIBarButtonItem *)item{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)requestSignupAreaCode {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"signup.areaCode" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if (suceess[@"areaCode"] && suceess[@"areaName"]) {
            self.lac = [suceess[@"areaCode"] isKindOfClass:[NSString class]] ? suceess[@"areaCode"] : [suceess[@"areaCode"] stringValue];
            self.countryLabel.text = suceess[@"areaName"];
            [self.codeBtn setTitle:self.lac forState:UIControlStateNormal];
        }
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (IBAction)lacButtonclicked:(id)sender {
   
//    PickerViewController *pickerViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PickerViewController"];
//    [pickerViewController setPickerType:PickerTypeAreaCode];
//    [pickerViewController setEnsureAreaCodeBtnClickBlock:^(NSString *area,NSString *code){
//        self.lac = [code substringFromIndex:1];
//        self.countryLabel.text = area;
//        [self.codeBtn setTitle:self.lac forState:UIControlStateNormal];
//    }];
//    [pickerViewController presentInViewController:self complection:^{
//
//    }];
    SelectAreaCodeController * vc = [SelectAreaCodeController new];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
        
}

-(void)checkViewControllerType{
    
    if (self.isResetPwd) {
        self.title=SPDStringWithKey(@"修改密码", nil);
        [self.comfirmBtn setTitle:SPDStringWithKey(@"修改密码", nil)forState:UIControlStateNormal];
    }else if (self.isBindMobile){
        self.title=SPDStringWithKey(@"绑定手机", nil);
        [self.comfirmBtn setTitle:SPDStringWithKey(@"绑定手机", nil)forState:UIControlStateNormal];
    }
    
}
//获取验证码
- (IBAction)getVerifyCodeTapped:(id)sender {
    if ([SPDCommonTool isLegalPhoneNumber:self.mobieTextField.text]) {
        [self.getVerifyCodeBtn setEnabled:NO];
        self.getVerifyCodeBtn.alpha           = 0.5;
        self.getVerifyCodeBtn.titleLabel.text = SPDStringWithKey(@"获取中", nil);
        self.phoneNumber = self.mobieTextField.text;
        // 手机号码合法
        if (self.isResetPwd){
            [self requestVerifyCodeWithUrlStr:@"password.reset.sms"];
        }else if (self.isBindMobile){
            [self requestVerifyCodeWithUrlStr:@"bindmobile.sms"];
        }
    }else{
        [self showToast:@"Please enter the correct phone number"];
    }
}

-(void)textFieldresignFirstResponder{
    [self.mobieTextField resignFirstResponder];
    [self.verifyCodeTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}
- (IBAction)comfirmBtnTapped:(id)sender {
    [self.comfirmBtn setEnabled:NO];
    self.phoneNumber  = self.mobieTextField.text;
    self.veriftyCode = self.verifyCodeTextField.text;
    self.password     = self.passwordTextField.text;
    [self cheakPassWord];
    [self.comfirmBtn setEnabled:YES];
}

-(void)requestVerifyCodeWithUrlStr:(NSString *)urlStr{
    NSDictionary *param=@{
                          @"mobile":self.phoneNumber,
                          @"lac":self.lac
                          };
    NSLog(@"%@",self.phoneNumber);
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:param] bURL:urlStr bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                 target:self
                                               selector:@selector(timerFireMethod:)
                                               userInfo:nil
                                                repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        
        self.timeLabel.enabled         = NO;
        self.timeLabel.text = [NSString stringWithFormat:@"%d", self.second];
        self.timeLabel.hidden          = NO;
        

    } bFailure:^(id  _Nullable failure) {
        [self showMessageToast:failure[@"msg"]];
        self.getVerifyCodeBtn.enabled=YES;
        self.getVerifyCodeBtn.alpha=1.0;
    }];
    
}

#pragma mark - timer
-(void)timerFireMethod:(NSTimer *)timer{
    if (self.second == 1) {
        [timer invalidate];
        self.timeLabel.hidden = YES;
        self.second = 60;
        [self.getVerifyCodeBtn setTitle:SPDStringWithKey(@"点击重新获取", nil)forState:UIControlStateNormal];
        [self.getVerifyCodeBtn setBackgroundColor:[UIColor colorWithHexString:@"ff6f8a"]];
        self.getVerifyCodeBtn.titleLabel.font = [UIFont systemFontOfSize:13.0];
        [self.getVerifyCodeBtn setEnabled:YES];
        self.getVerifyCodeBtn.alpha = 1.0;
    } else {
        self.getVerifyCodeBtn.enabled         = NO;
        self.second--;
        NSString* title = [NSString stringWithFormat:@"%d", self.second];
        self.timeLabel.text=title;
    }

}


-(BOOL)isPhoneNumberLegal:(NSString *)phoneNumber {
    if (phoneNumber.length != 11) {
        UIAlertController *alert     = [UIAlertController alertControllerWithTitle:SPDStringWithKey(@"输入信息错误", nil)message:SPDStringWithKey(@"请检查手机号后重新输入", nil)preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:SPDStringWithKey(@"确定", nil)style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    NSString *regex        = @"^1[3|4|5|7|8][0-9]\\d{8}$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL isMatch           = [predicate evaluateWithObject:phoneNumber];
    if (!isMatch) {
        UIAlertController *alert     = [UIAlertController alertControllerWithTitle:SPDStringWithKey(@"输入信息错误", nil)message:SPDStringWithKey(@"请检查手机号后重新输入", nil)preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:SPDStringWithKey(@"确定", nil)style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    return YES;
}
-(void)cheakPassWord{
    if (self.password.length == 0) {
        UIAlertController *alert     = [UIAlertController alertControllerWithTitle:SPDStringWithKey(@"输入错误", nil)message:SPDStringWithKey(@"密码不能为空", nil)preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:SPDStringWithKey(@"确定", nil)style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    if (self.password.length <=5) {
        UIAlertController *alert     = [UIAlertController alertControllerWithTitle:SPDStringWithKey(@"密码位数不足", nil)message:SPDStringWithKey(@"请输入6位及以上的密码", nil)preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:SPDStringWithKey(@"确定", nil)style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    if (self.password.length>=6&&self.password.length<=16) {
        if (self.isResetPwd) {
            [self uploadDataWithString:@"password.reset"];
        }
        if(self.isBindMobile){
            [self uploadDataWithString:@"bindmobile"];

        }
    }
    
}

-(void)uploadDataWithString:(NSString *)str{
    
    NSDictionary *param=@{
                          @"mobile":self.phoneNumber,
                          @"verify_code":self.veriftyCode,
                          @"password":[SPDHashGenerator md5:self.password],
                          @"lac":self.lac
                          };
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:param] bURL:str bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        
        if (self.isResetPwd) {
            [self showMessageToast:SPDStringWithKey(@"修改密码成功", nil)];
  
        }else{
            [self showMessageToast:SPDStringWithKey(@"绑定成功，可用手机登录", nil)];
        }
        [self performSelector:@selector(popToView) withObject:nil afterDelay:1.5];

    } bFailure:^(id  _Nullable failure) {
        
        [self showMessageToast:failure[@"msg"]];
        
    }];
}
//返回上级页面
-(void)popToView{
    
    [self.navigationController popViewControllerAnimated:YES];

}
-(BOOL)isPhoneNumberAndVerificationCodeLegal:(NSString *)phoneNumber veriCode:(NSString *)veriCode {
    if (phoneNumber.length != 11 || veriCode.length != 6) {
        UIAlertController *alert     = [UIAlertController alertControllerWithTitle:SPDStringWithKey(@"输入信息错误", nil)message:SPDStringWithKey(@"请检查手机号和验证码后重新输入", nil)preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:SPDStringWithKey(@"确定", nil)style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    NSString *phoneRegex           = @"^1[3|4|5|7|8][0-9]\\d{8}$";
    NSPredicate *phonePredicate    = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    BOOL isPhoneNumberMatch        = [phonePredicate evaluateWithObject:phoneNumber];
    
    NSString *veriCodeRegex        = @"^\\d{6}$";
    NSPredicate *veriCodePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", veriCodeRegex];
    BOOL isVeriCodeMatch           = [veriCodePredicate evaluateWithObject:veriCode];
    
    if (!isPhoneNumberMatch || !isVeriCodeMatch) {
        UIAlertController *alert     = [UIAlertController alertControllerWithTitle:SPDStringWithKey(@"输入信息错误", nil)message:SPDStringWithKey(@"请检查手机号和验证码后重新输入", nil)preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:SPDStringWithKey(@"确定", nil)style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    return YES;
}



# pragma mark - UITextFieldDelegate

- (BOOL) textFieldShouldReturn:(UITextField*)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    textField.textColor = [UIColor blackColor];
    return YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [timer invalidate];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - SelectAreaCodeControllerDelegate

- (void)areaCodeSelect:(NSString *)area lac:(NSString *)lac {
    self.lac = [lac substringFromIndex:1] ;
    [self.codeBtn setTitle:self.lac forState:UIControlStateNormal];
    self.countryLabel.text = area;
}


@end
