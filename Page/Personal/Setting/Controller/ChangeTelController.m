//
//  ChangeTelController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/15.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "ChangeTelController.h"
#import "PickerViewController.h"
#import "VerifyOriginalMobileController.h"

@interface ChangeTelController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topCONS;
@property (weak, nonatomic) IBOutlet UILabel *countrynamelabel;
@property (weak, nonatomic) IBOutlet UILabel *laclabel;
@property (weak, nonatomic) IBOutlet UILabel *tellabel;
@property (weak, nonatomic) IBOutlet UITextField *textfield;
@property (weak, nonatomic) IBOutlet UIButton *nextbtn;

@end

//  千万别说命名怎么这么恶心 因为工资太低。。。。能力更低。。。。
// gg

@implementation ChangeTelController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"更换手机号".localized;
    self.textfield.placeholder = @"请输入您的手机号码".localized;
    self.topCONS.constant = naviBarHeight + 10;
    [self.textfield addTarget:self action:@selector(textFieldTextDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self requestareacode];
}

// 工资太低不会转换大小写。。。
- (void)requestareacode {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"signup.areaCode" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if (suceess[@"areaCode"] && suceess[@"areaName"]) {
            self.laclabel.text = suceess[@"areaCode"];
            self.countrynamelabel.text = suceess[@"areaName"];
        }
    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (IBAction)nextbtn:(UIButton *)sender {
    [RequestUtils commonPostRequestUtils:[@{@"lac":self.laclabel.text ,@"mobile":self.textfield.text} mutableCopy]  bURL:@"change.verify" bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        // 跳转到下一个页面
        VerifyOriginalMobileController * vc = [VerifyOriginalMobileController new];
        vc.mobile = self.textfield.text;
        vc.lac  = self.laclabel.text;
        vc.type = @"1";
        [self.navigationController pushViewController:vc animated:YES];
        
    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (IBAction)changelac:(id)sender {
    PickerViewController *pickerViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PickerViewController"];
    [pickerViewController setPickerType:PickerTypeAreaCode];
    [pickerViewController setEnsureAreaCodeBtnClickBlock:^(NSString *area,NSString *code){
        self.laclabel.text = [code substringFromIndex:1];
        self.countrynamelabel.text = area;
    }];
    [pickerViewController presentInViewController:self complection:^{
        
    }];
}

//监听改变方法
- (void)textFieldTextDidChange:(UITextField *)textChange{
    self.nextbtn.userInteractionEnabled = textChange.text.notEmpty;
    [self.nextbtn setBackgroundColor:[UIColor colorWithHexString:textChange.text.notEmpty ? COMMON_PINK :@"E8E7EA"]];
}

@end
