//
//  securityViewController.m
//  SimpleDate
//
//  Created by 侯玲 on 16/8/29.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "securityViewController.h"
#import "BindingCell.h"
#import "BindingMobileCell.h"
#import "mobileBindController.h"
#import "SPDUMShareUtils.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "MobileCell.h"
#import "VerifyOriginalMobileController.h"
#import "WXManager.h"

@interface securityViewController ()<UITableViewDataSource,UITableViewDelegate,BindingCellDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, copy)  NSString *login_type;
@property (nonatomic, copy)  NSString *mobile;
@property (nonatomic, copy)  NSDictionary *dict;

@end

@implementation securityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = SPDStringWithKey(@"账号安全", nil);
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self cheackloginType];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hanleWXAuthSuccess:) name:@"WXAuthSuccess" object:nil];
}

-(void)cheackloginType{
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"account.security" bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable dict) {
        self.dict = dict;
        _login_type = dict[@"login_type"];
        _mobile = dict[@"mobile"];
        [self.view addSubview:self.tableView];
        [self.tableView reloadData];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return indexPath.section == 0 ? 66 : 132;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ( section == 0 ) {
        if ([_login_type isEqualToString:@"mobile"] || [self.dict[@"mobile_bind"] boolValue]) {
            return 2;
        }else{
            return 1;
        }
    }else{
        return 1;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell;
    if (indexPath.section==1) {
        BindingCell * cell = [BindingCell cellWithTableView:tableView];
        cell.delegate=self;
        cell.dict = self.dict;
        return cell;
    }else if(indexPath.section==0){
        cell = [[UITableViewCell alloc]init];
        if ([self.dict[@"mobile_bind"] boolValue]) {
            if (indexPath.row==0) {
                MobileCell* cell = [MobileCell cellWithTableView:tableView];
                NSString*mobileStr = [_mobile stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"FAMY"];
                cell.mLabel.text = [NSString stringWithFormat:@"%@%@",SPDStringWithKey(@"已绑定", nil),mobileStr];
                return cell;
            }else if (indexPath.row == 1){
                cell=[[UITableViewCell alloc]init];
                cell.textLabel.text = SPDStringWithKey(@"修改密码", nil);
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
        }else{
            BindingMobileCell *cell = [BindingMobileCell cellWithTableView:tableView];
            cell.clickBindMobileButtonBlock = ^{
                [self performSegueWithIdentifier:@"showToBind" sender:self];
            };
            return cell;
        }
        
    }
    return cell;
}

- (NSString *)reversalString:(NSString *)originString{
    NSString *resultStr = @"";
    for (NSInteger i = originString.length -1; i >= 0; i--) {
      NSString *indexStr = [originString substringWithRange:NSMakeRange(i, 1)];
      resultStr = [resultStr stringByAppendingString:indexStr];
    }
  return resultStr;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 1) {
        [self performSegueWithIdentifier:@"showToPwd" sender:self];
    }
    if (indexPath.section == 0 && indexPath.row == 0 && [self.dict[@"mobile_bind"] boolValue]){
        VerifyOriginalMobileController * vc = [VerifyOriginalMobileController new];
        vc.mobile = self.dict[@"mobile"];
        vc.lac = self.dict[@"lac"];
        vc.type = @"0";
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - cell Delegate

-(void)qqBindButtonClickedWithState:(NSString *)state{
    if ([state isEqualToString:@"bind"]) {
        [self requestAuthWithFacebook];
    }else if ([state isEqualToString:@"unbind"]){
        [self unbindWithPlatformName:@"facebook"];
    }
}
-(void)wechatBindButtonClickedWithState:(NSString *)state{
    if ([state isEqualToString:@"bind"]) {
        [[WXManager sharedInstance] sendLoginReq];
    }else if ([state isEqualToString:@"unbind"]){
        [self unbindWithPlatformName:@"wechat"];
    }
}
-(void)weiboBindButtonClickedWithState:(NSString *)state{
    if ([state isEqualToString:@"bind"]) {
        [[WXManager sharedInstance] sendLoginReq];
    }else if ([state isEqualToString:@"unbind"]){
        [self unbindWithPlatformName:@"wechat"];
    }
}
#pragma mark - WeeXinn

- (void)hanleWXAuthSuccess:(NSNotification *)notify {
    [RequestUtils SpecialGetRequestUtils:[@{@"appid":WXAppId,@"secret":WXAppSecret,@"code":notify.userInfo[@"code"],@"grant_type":@"authorization_code"} mutableCopy] bURL:@"https://api.weixin.qq.com/sns/oauth2/access_token" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        
        NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:suceess options:NSJSONReadingAllowFragments error:nil];
        [self bindWithPlatformName:@"wechat" andOpenid:dict[@"openid"] andNick_name:@"wechat" andUnionId:dict[@"unionid"] andAccess_token:dict[@"access_token"]];
    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}


//4-13 union_id
-(void)bindWithPlatformName:(NSString *)platformName andOpenid:(NSString *)openid andNick_name:(NSString *)nick_name andUnionId:(NSString *)unionid andAccess_token:(NSString *)access_token {

    NSDictionary *dict=@{
                         @"platform":platformName,
                         @"nick_name":nick_name,
                         @"openid":openid,
                         @"access_token":access_token,
                         @"unionid":unionid
                         };
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dict] bURL:@"bindplatform" bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if ([platformName isEqualToString:@"facebook"]) {
            [self showMessageToast:SPDStringWithKey(@"绑定成功，可用Facebook登录", nil)];

        }else if([platformName isEqualToString:@"wechat"]){
            [self showMessageToast:SPDStringWithKey(@"绑定成功，可用Wechat登录", nil)];
        }else if([platformName isEqualToString:@"twitter"]){
            [self showMessageToast:SPDStringWithKey(@"绑定成功，可用Twitter登录", nil)];
        }
        [self cheackloginType];
    } bFailure:^(id  _Nullable failure) {
        [self showMessageToast:failure[@"msg"]];
    }];
}

//请求解绑
-(void)unbindWithPlatformName:(NSString *)platform{
    
    NSDictionary *dict=@{@"platform": platform};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dict] bURL:@"unbind" bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        [self showMessageToast:SPDStringWithKey(@"解绑成功", nil)];
        [self performSelector:@selector(cheackloginType) withObject:nil afterDelay:1.0];
    } bFailure:^(id  _Nullable failure) {
        [self showToast:failure[@"msg"]];
    }];
    
}

#pragma mark - 绑定facebook

- (void)requestAuthWithFacebook {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithPermissions:@[@"public_profile"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult * _Nullable result, NSError * _Nullable error) {
        if (error) {
            //             NSLog(@"Process error");
        } else if (result.isCancelled) {
            //             NSLog(@"Cancelled");
        } else {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [self handleFacebookUserInfoWithFBSDKLoginManagerLoginResult:result];
        }
    }];
}

- (void)handleFacebookUserInfoWithFBSDKLoginManagerLoginResult:(FBSDKLoginManagerLoginResult *)result {
    
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  
                                  initWithGraphPath:result.token.userID
                                  
                                  parameters:@{@"fields": @"id,name,picture,gender"}
                                  
                                  HTTPMethod:@"GET"];
    
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id user_result,NSError *error) {
        // Handle the result
        if (!error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self bindWithPlatformName:@"facebook" andOpenid:user_result[@"id"] andNick_name:user_result[@"name"] andUnionId:@"facebook-unionid" andAccess_token:result.token.tokenString];
        }
    }];
    
}


#pragma mark - segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"showToBind"]) {
        mobileBindController *mobile=segue.destinationViewController;
        mobile.hidesBottomBarWhenPushed=YES;
        mobile.isBindMobile=YES;
    }
    if ([segue.identifier isEqualToString:@"showToPwd"]) {
        mobileBindController *mobile=segue.destinationViewController;
        mobile.hidesBottomBarWhenPushed=YES;
        mobile.isResetPwd=YES;
    }
    
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, kScreenW, kScreenH) style:UITableViewStyleGrouped];
        CGRect frame=CGRectMake(0, 0, 0, CGFLOAT_MIN);
        _tableView.tableHeaderView=[[UIView alloc]initWithFrame:frame];
        _tableView.tableFooterView=[[UIView alloc]init];
        _tableView.delegate=self;
        _tableView.dataSource=self;
    }
    return _tableView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
