//
//  SettingTableController.m
//  SimpleDate
//
//  Created by 程龙军 on 15/12/9.
//  Copyright © 2015年 WYB. All rights reserved.
//

#import "SettingTableController.h"
#import "SettingCell.h"
#import "SPDBlackListViewController.h"

@interface SettingTableController ()<UITableViewDataSource, UITableViewDelegate, SettingCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *personalTableView;
@property (nonatomic, copy) NSArray *sections;
@property (nonatomic, assign) BOOL strangerMsgSwitch;
@property (nonatomic, assign) BOOL canModifyStrangerMsgSwitch;

@end

@implementation SettingTableController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar vhl_setBackgroundColor:[UIColor whiteColor]];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = SPDStringWithKey(@"设置", nil);
    [self.personalTableView registerNib:[UINib nibWithNibName:@"SettingCell" bundle:nil] forCellReuseIdentifier:@"SettingCell"];
    
    NSArray *section1Settings = @[SPDStringWithKey(@"账号安全", nil),
                                  SPDStringWithKey(@"选择语言", nil),
                                  SPDStringWithKey(@"我要移民", nil)];
    
    NSArray *section2Settings = @[SPDStringWithKey(@"阻止非好友私信消息", nil),
                                  SPDStringWithKey(@"世界消息勿扰", nil),
                                  SPDStringWithKey(@"系统消息勿扰", nil),
                                  SPDStringWithKey(@"勿扰模式", nil),
                                  SPDStringWithKey(@"隐身", nil),
                                  SPDStringWithKey(@"黑名单", nil)];
    
    NSArray *section3Settings = @[SPDStringWithKey(@"关于我们", nil),
                                  SPDStringWithKey(@"清空缓存", nil),
                                  SPDStringWithKey(@"意见反馈", nil)];
    
    NSArray *section4Settings = @[SPDStringWithKey(@"退出账号", nil)];
    self.sections = @[section1Settings, section2Settings, section3Settings, section4Settings];
    [self requestMySwitchGet];
}

#pragma mark - Requests

- (void)requestMySwitchGet {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionary] bURL:@"my.switch" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.strangerMsgSwitch = [suceess[@"strangerMsgSwitch"] isEqualToString:@"on"];
        self.canModifyStrangerMsgSwitch = [suceess[@"isModify"] isEqualToString:@"on"];
        [self.personalTableView reloadData];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestMySwitchPostWithSwitch:(UISwitch *)sender name:(NSString *)name {
    self.strangerMsgSwitch = sender.on;
    NSDictionary *dic = @{@"name": name, @"status": sender.on ? @"on" : @"off"};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"my.switch" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        
    } bFailure:^(id  _Nullable failure) {
        self.strangerMsgSwitch = !sender.on;
        [sender setOn:!sender.on animated:YES];
    }];
}

- (void)requestMySysmsgWithSwitch:(UISwitch *)sender {
    self.sysMsgSwitch = sender.on;
    NSDictionary *dic = @{@"msg_status": sender.on ? @"off" : @"on"};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"my.sysmsg" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id _Nullable suceess) {

    } bFailure:^(id  _Nullable failure) {
        self.sysMsgSwitch = !sender.on;
        [sender setOn:!sender.on animated:YES];
    }];
    
}

- (void)requestMyVisibleWithSwitch:(UISwitch *)sender {
    self.visibleSwitch = sender.on;
    NSDictionary *dic = @{@"visual_status": sender.on ? @"invisible" : @"visible"};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"my.visible" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id _Nullable suceess) {
        
    } bFailure:^(id  _Nullable failure) {
        self.visibleSwitch = !sender.on;
        [sender setOn:!sender.on animated:YES];
    }];
}

- (void)requestLogout {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"logout" bAnimated:nil bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *settings = self.sections[section];
    return settings.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SettingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingCell" forIndexPath:indexPath];
    NSArray *settings = self.sections[indexPath.section];
    cell.delegate = self;
    cell.nameLabel.text = settings[indexPath.row];
    
    cell.arrowImageView.hidden = YES;
    cell.actionSwitch.hidden = YES;
    cell.descLabel.hidden = YES;
    switch (indexPath.section) {
        case 1:
            switch (indexPath.row) {
                case 0:
                    cell.actionSwitch.hidden = NO;
                    cell.actionSwitch.on = self.strangerMsgSwitch;
                    break;
                case 1:
                    cell.actionSwitch.hidden = NO;
                    cell.actionSwitch.on = [SPDCommonTool getGlobalMessageSwitch];
                    break;
                case 2:
                    cell.actionSwitch.hidden = NO;
                    cell.actionSwitch.on = self.sysMsgSwitch;
                    break;
                case 4:
                    cell.actionSwitch.hidden = NO;
                    cell.actionSwitch.on = self.visibleSwitch;
                    break;
                default:
                    cell.arrowImageView.hidden = NO;
                    break;
            }
            break;
        case 2:
            switch (indexPath.row) {
                case 1:
                    cell.descLabel.hidden = NO;
                    cell.descLabel.text = [NSString stringWithFormat:@"%.2fM", [[SDImageCache sharedImageCache] getSize] / 1024.0 / 1024.0];
                    break;
                default:
                    cell.arrowImageView.hidden = NO;
                    break;
            }
            break;
        default:
            cell.arrowImageView.hidden = NO;
            break;
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                    [self performSegueWithIdentifier:@"jumpToSecurity" sender:self];
                    break;
                case 1:
                    [self performSegueWithIdentifier:@"jumpToLanguage" sender:self];
                    break;
                case 2: {
                    NSString *url = [NSString stringWithFormat:IMMIGRANT_RULE_URL, [SPDCommonTool getFamyLanguage]];
                    [self loadWebViewController:self title:SPDStringWithKey(@"移民规则", nil) url:url];
                    break;
                }
                default:
                    break;
            }
            break;
        case 1:
            switch (indexPath.row) {
                case 3:
                    [self performSegueWithIdentifier:@"PushToIgnore" sender:self];
                    break;
                case 5: {
                    SPDBlackListViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SPDBlackListViewController"];
                    [self.navigationController pushViewController:vc animated:YES];
                    break;
                }
                default:
                    break;
            }
            break;
        case 2:
            switch (indexPath.row) {
                case 0:
                    [self jumpToSafari:[NSURL URLWithString:URL_OfficialWebsite]];
                    break;
                case 1: {
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    [[NSURLCache sharedURLCache] removeAllCachedResponses];
                    [[SDImageCache sharedImageCache] clearDiskOnCompletion:^{
                        [self.personalTableView reloadData];
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    }];
                    break;
                }
                case 2:
                    [self performSegueWithIdentifier:@"ToSuggestViewController" sender:self];
                    break;
                default:
                    break;
            }
            break;
        case 3: {
            [self presentAlertWithTitle:@"提醒".localized message:@"确认退出？".localized cancelTitle:@"取消".localized cancelHandler:^(UIAlertAction * _Nonnull action) {
                
            } actionTitle:@"确定".localized actionHandler:^(UIAlertAction * _Nonnull action) {
                [self requestLogout];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"LOGOUT" object:nil];
            }];
            break;
        }
        default:
            break;
    }
}

#pragma mark - SettingCellDelegate

- (void)actionSwitch:(UISwitch *)sender valueChangedWithName:(NSString *)name {
    if ([name isEqualToString:SPDStringWithKey(@"阻止非好友私信消息", nil)]) {
        if (self.canModifyStrangerMsgSwitch) {
            [self requestMySwitchPostWithSwitch:sender name:@"strangerMsgSwitch"];
        } else {
            [self showToast:SPDStringWithKey(@"主播身份不可更改接收非好友消息开关", nil)];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                sender.on = !sender.on;
            });
        }
    } else if ([name isEqualToString:SPDStringWithKey(@"世界消息勿扰", nil)]) {
        [SPDCommonTool setGlobalMessageSwitch:sender.on];
    } else if ([name isEqualToString:SPDStringWithKey(@"系统消息勿扰", nil)]) {
        [self requestMySysmsgWithSwitch:sender];
    } else if ([name isEqualToString:SPDStringWithKey(@"隐身", nil)]) {
        [self requestMyVisibleWithSwitch:sender];
    }
}

@end
