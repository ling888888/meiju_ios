//
//  VerifyOriginalMobileController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/14.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "VerifyOriginalMobileController.h"
#import "ChangeTelController.h"

@interface VerifyOriginalMobileController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topCons;
@property (weak, nonatomic) IBOutlet UILabel *telLabel;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *timerBtn;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UILabel *lajiLabel;

@property (nonatomic, strong) NSTimer * timer;
@property (nonatomic, assign) int second;


@end

@implementation VerifyOriginalMobileController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.lajiLabel.text = [self.type isEqualToString:@"0"]? @"为了保障您的帐号安全，需验证您的身份，验证成功后可设置新的登录密码。".localized:@"可通过短信验证码更换手机号码".localized;
    self.second = 60;
    self.textField.delegate = self;
    self.navigationItem.title = [self.type isEqualToString:@"0"]? @"验证手机号码".localized:@"输入短信验证码".localized;
    self.topCons.constant = naviBarHeight + 10;
    self.textField.placeholder = @"请输入您收到的验证码".localized;
    self.telLabel.text = [NSString stringWithFormat:@"+%@%@",_lac,_mobile];
    [self.timerBtn setBackgroundColor:SPD_HEXCOlOR(COMMON_PINK)];
    [self.timerBtn setTitle:[self.type isEqualToString:@"0"] ?@"获取验证码".localized:@"获取验证码".localized forState:UIControlStateNormal];
    [self.nextButton setTitle:[self.type isEqualToString:@"0"] ?@"下一步".localized:@"完成".localized forState:UIControlStateNormal];
    [self.nextButton setBackgroundColor:SPD_HEXCOlOR(@"#E8E7EA")];
    self.nextButton.userInteractionEnabled = NO;
    [self.textField addTarget:self action:@selector(textFieldTextDidChange:) forControlEvents:UIControlEventEditingChanged];
}

- (IBAction)nextBtnClicked:(UIButton *)sender {
    NSMutableDictionary * dict = [NSMutableDictionary new];
    [dict setValue:_mobile forKey:@"mobile"];
    [dict setValue:_lac forKey:@"lac"];
    [dict setValue:self.textField.text forKey:@"verify_code"];
    [dict setValue:[self.type isEqualToString:@"1"] ? @"2":@"1" forKey:@"type"];
    [RequestUtils commonPostRequestUtils:dict bURL:@"change.phone" bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if ([self.type isEqualToString:@"0"] ) {
            [self.navigationController pushViewController:[ChangeTelController new] animated:YES];
        }else{
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (IBAction)timerBtnClicked:(id)sender {
    NSMutableDictionary * dict = [NSMutableDictionary new];
    [dict setValue:_mobile forKey:@"mobile"];
    [dict setValue:_lac forKey:@"lac"];
    [RequestUtils commonPostRequestUtils:dict bURL:@"change.sms" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        [self.timer fire];
    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}

//监听改变方法
- (void)textFieldTextDidChange:(UITextField *)textChange{
    self.nextButton.userInteractionEnabled = textChange.text.notEmpty;
    [self.nextButton setBackgroundColor:[UIColor colorWithHexString:textChange.text.notEmpty ? COMMON_PINK :@"E8E7EA"]];
}

- (NSTimer *)timer {
    if (!_timer) {
        _timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(time:) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
    }
    return _timer;
}

- (void)time:(NSTimer *)timer {
    if (self.second > 0) {
        self.timerBtn.userInteractionEnabled = NO;
        [self.timerBtn setTitle:[NSString stringWithFormat:@"重新获取(%@s)".localized,@(self.second)] forState:UIControlStateNormal];
        [self.timerBtn setBackgroundColor:SPD_HEXCOlOR(@"#D3D2D5")];
        self.second --;
    }else{
        [self.timer invalidate];
        self.timer = nil;
        self.second = 60;
        [self.timerBtn setTitle:@"点击重新获取".localized forState:UIControlStateNormal];
        [self.timerBtn setBackgroundColor:SPD_HEXCOlOR(COMMON_PINK)];
        self.timerBtn.userInteractionEnabled = YES;
    }
    
}

@end
