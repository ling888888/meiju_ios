//
//  IgnoreViewController.m
//  SimpleDate
//
//  Created by xu.juvenile on 16/4/6.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "IgnoreViewController.h"
#import "PickerViewController.h"

@interface IgnoreViewController ()
@property (weak, nonatomic) IBOutlet UILabel *pushToSettingLabel;

@property (weak, nonatomic) IBOutlet UILabel *userNotifiationLabel;
@property (weak, nonatomic) IBOutlet UILabel *wheatherIgnorTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *tipsLabel;

@end

@implementation IgnoreViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.title = SPDStringWithKey(@"勿扰模式", nil);
    self.wheatherIgnorTitleLabel.text = SPDStringWithKey(@"是否开启勿扰模式", nil);
    self.tipsLabel.text = SPDStringWithKey(@"请在iPhone的“设置”-“通知”里进行修改。", nil);
    self.pushToSettingLabel.text = SPDStringWithKey(@"点我跳转", nil);

    if ([[UIApplication sharedApplication] currentUserNotificationSettings].types == UIUserNotificationTypeNone) {
        [self.userNotifiationLabel setText:SPDStringWithKey(@"已开启", nil)];
    }
    else {
        [self.userNotifiationLabel setText:SPDStringWithKey(@"已关闭", nil)];
    }

    NSDictionary *underlineAttribute = @{ NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle) };
    self.pushToSettingLabel.attributedText = [[NSAttributedString alloc] initWithString:self.pushToSettingLabel.text attributes:underlineAttribute];
    
    [self.pushToSettingLabel setUserInteractionEnabled:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pushToSetting:(UITapGestureRecognizer *)sender
{
    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=NOTIFICATIONS_ID"]];

    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];

    dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)1 * NSEC_PER_SEC);
    dispatch_after(delayTime, dispatch_get_main_queue(), ^{
      [self.navigationController popViewControllerAnimated:NO];
    });
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
