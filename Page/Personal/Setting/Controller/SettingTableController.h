//
//  SettingTableController.h
//  SimpleDate
//
//  Created by 程龙军 on 15/12/9.
//  Copyright © 2015年 WYB. All rights reserved.
//

#import "BaseViewController.h"

@interface SettingTableController : BaseViewController

@property (nonatomic, assign) BOOL sysMsgSwitch;
@property (nonatomic, assign) BOOL visibleSwitch;

@end
