//
//  mobileBindController.h
//  SimpleDate
//
//  Created by 侯玲 on 16/8/31.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface mobileBindController : BaseViewController

@property(nonatomic,assign)BOOL isResetPwd;

@property(nonatomic,assign)BOOL isBindMobile;

@end
