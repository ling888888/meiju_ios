//
//  VerifyOriginalMobileController.h
//  SimpleDate
//
//  Created by ling Hou on 2020/4/14.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VerifyOriginalMobileController : UIViewController

@property (nonatomic, copy) NSString * type; // 0 （验证原先的手机号） 1 完成操作
@property (nonatomic, copy) NSString * mobile;
@property (nonatomic, copy) NSString * lac;

@end

NS_ASSUME_NONNULL_END
