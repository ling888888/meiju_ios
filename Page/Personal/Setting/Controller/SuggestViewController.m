//
//  SuggestViewController.m
//  SimpleDate
//
//  Created by liuweiqing on 16/2/1.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "SuggestViewController.h"
#import "MBProgressHUD.h"
#import "SPDApiUser.h"


@interface SuggestViewController ()

@property (weak, nonatomic) IBOutlet UITextView *suggestTextView;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@end

@implementation SuggestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = SPDStringWithKey(@"意见反馈", nil);
    [self.submitButton setTitle:SPDStringWithKey(@"提交", nil) forState:UIControlStateNormal];
    
}

- (IBAction)submitBtnClick:(UIButton *)sender {
    [self submitButtonClick];
}

#pragma mark --event response
- (void)submitButtonClick {
    
    if (self.suggestTextView.text.length <= 0) {
        UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"提醒".localized message:@"建议内容不能为空".localized preferredStyle:UIAlertControllerStyleAlert];
        [alertC addAction:[UIAlertAction actionWithTitle:@"确定".localized style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alertC animated:true completion:nil];
    } else {
        
        [self uploadSuggestionData];
    }
}

-(void)uploadSuggestionData{
    
    NSDictionary *dict=@{
                         @"content": self.suggestTextView.text
                         };
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dict] bURL:@"feedback" bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        
        [self.navigationController popViewControllerAnimated:YES];
        
    } bFailure:^(id  _Nullable failure) {
        UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"提醒".localized message:failure[@"msg"] preferredStyle:UIAlertControllerStyleAlert];
        [alertC addAction:[UIAlertAction actionWithTitle:@"确定".localized style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alertC animated:true completion:nil];
    }];
}



@end
