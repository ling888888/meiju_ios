//
//  SPDChangeLanguage.m
//  SimpleDate
//
//  Created by Monkey on 2017/11/10.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDChangeLanguage.h"
#import "SPDLanguageTool.h"

@interface SPDChangeLanguage ()<UITableViewDataSource, UITableViewDelegate>
    
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *languageArray;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;
    
@end

@implementation SPDChangeLanguage

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = SPDStringWithKey(@"选择语言", nil);
    [self.view addSubview:self.tableView];
    
}
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
#pragma mark - UITableViewDataSource
    
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.languageArray.count;
}
    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    NSString *language = self.languageArray[indexPath.row];
    cell.textLabel.text = [SPDLanguageTool sharedInstance].allLanguages[language];
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    cell.textLabel.textColor = SPD_HEXCOlOR(@"333333");
    
    if ([language isEqualToString:[SPDLanguageTool sharedInstance].currentLanguage]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        self.selectedIndexPath = indexPath;
    } else {
        cell.accessoryType = UITableViewCellSelectionStyleNone;
    }
    return cell;
}
    
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath != self.selectedIndexPath) {
        UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:self.selectedIndexPath];
        selectedCell.accessoryType = UITableViewCellAccessoryNone;
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        self.selectedIndexPath = indexPath;

        [[SPDLanguageTool sharedInstance] setNewLanguage:self.languageArray[indexPath.row]];
    }
}
    
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45;
}

#pragma mark - setters and getters
    
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc]init];
    }
    return _tableView;
}

- (NSArray *)languageArray {
    if (!_languageArray) {
        _languageArray = [SPDLanguageTool sharedInstance].allLanguages.allKeys;
    }
    return _languageArray;
}

@end
