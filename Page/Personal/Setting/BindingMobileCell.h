//
//  BindingMobileCell.h
//  SimpleDate
//
//  Created by 侯玲 on 16/8/31.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BindingMobileCell : BaseTableViewCell

@property (nonatomic,copy)void (^clickBindMobileButtonBlock)();

@end
