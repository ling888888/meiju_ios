//
//  BindingCell.h
//  SimpleDate
//
//  Created by 侯玲 on 16/8/29.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BindingCellDelegate <NSObject>

-(void)qqBindButtonClickedWithState:(NSString *)state;
-(void)wechatBindButtonClickedWithState:(NSString *)state;
-(void)weiboBindButtonClickedWithState:(NSString *)state;


@end

@interface BindingCell : BaseTableViewCell

@property(nonatomic,weak)id<BindingCellDelegate> delegate;

@property (nonatomic, copy) NSDictionary * dict;

@end
