//
//  BindingCell.m
//  SimpleDate
//
//  Created by 侯玲 on 16/8/29.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "BindingCell.h"

@interface BindingCell ()
@property (weak, nonatomic) IBOutlet UIImageView *qqImg;
@property (weak, nonatomic) IBOutlet UIImageView *wechatImg;
@property (weak, nonatomic) IBOutlet UIImageView *weiboImg;

@property (weak, nonatomic) IBOutlet UIButton *qqBtn;
@property (weak, nonatomic) IBOutlet UIButton *wechatBtn;
@property (weak, nonatomic) IBOutlet UIButton *weiboBtn;

@property (weak, nonatomic) IBOutlet UILabel *qqNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *wechatNameLbel;
@property (weak, nonatomic) IBOutlet UILabel *weiboNameLabel;


@end

@implementation BindingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)setDict:(NSDictionary *)dict {
    _dict = dict;
    self.qqImg.image = [_dict[@"facebook_bind"] boolValue] ? SPD_Image_BYNAME(@"ic_fb_bind"):SPD_Image_BYNAME(@"ic_fb_unbind");
    [self.qqBtn setTitle:[_dict[@"facebook_bind"] boolValue] ? SPDStringWithKey(@"已绑定", nil):SPDStringWithKey(@"绑定", nil) forState:UIControlStateNormal];
    [self.qqBtn setBackgroundColor:[_dict[@"facebook_bind"] boolValue]?SPD_HEXCOlOR(@"#D2D1D4"):SPD_HEXCOlOR(COMMON_PINK)];
    
    self.wechatImg.image = [_dict[@"twitter_bind"] boolValue] ? SPD_Image_BYNAME(@"ic_tw_bind"):SPD_Image_BYNAME(@"ic_tw_unbind");
    [self.wechatBtn setTitle:[_dict[@"twitter_bind"] boolValue] ? SPDStringWithKey(@"已绑定", nil):SPDStringWithKey(@"绑定", nil) forState:UIControlStateNormal];
    [self.wechatBtn setBackgroundColor:[_dict[@"twitter_bind"] boolValue]?SPD_HEXCOlOR(@"#D2D1D4"):SPD_HEXCOlOR(COMMON_PINK)];
    
    self.weiboImg.image = [_dict[@"wechat_bind"] boolValue] ? SPD_Image_BYNAME(@"ic_wechat_bind"):SPD_Image_BYNAME(@"ic_wechat_unbind");
    [self.weiboBtn setTitle:[_dict[@"wechat_bind"] boolValue] ? SPDStringWithKey(@"已绑定", nil):SPDStringWithKey(@"绑定", nil) forState:UIControlStateNormal];
    [self.weiboBtn setBackgroundColor:[_dict[@"wechat_bind"] boolValue]?SPD_HEXCOlOR(@"#D2D1D4"):SPD_HEXCOlOR(COMMON_PINK)];
}

- (IBAction)qqbindBtnTapped:(id)sender {
    [self.delegate qqBindButtonClickedWithState:[self.dict[@"facebook_bind"] boolValue] ?@"unbind":@"bind"];
}

- (IBAction)wechatbindBtnTapped:(id)sender {
    [self.delegate wechatBindButtonClickedWithState:[self.dict[@"twitter_bind"] boolValue] ?@"unbind":@"bind"];
}

- (IBAction)weibobinBtnTapped:(id)sender {
    [self.delegate wechatBindButtonClickedWithState:[self.dict[@"wechat_bind"] boolValue] ?@"unbind":@"bind"];
}

@end
