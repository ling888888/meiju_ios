//
//  presentReceivcCVCell.h
//  SimpleDate
//
//  Created by 侯玲 on 16/10/11.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GiftListModel.h"

@interface presentReceivcCVCell : UICollectionViewCell

@property(nonatomic,copy)GiftListModel *model;

@end
