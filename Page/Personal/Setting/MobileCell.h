//
//  MobileCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/4/14.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MobileCell : BaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *mLabel;

@end

NS_ASSUME_NONNULL_END
