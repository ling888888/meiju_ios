//
//  SettingCell.h
//  SimpleDate
//
//  Created by xu.juvenile on 16/3/7.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SettingCellDelegate <NSObject>

@optional

- (void)actionSwitch:(UISwitch *)sender valueChangedWithName:(NSString *)name;

@end

@interface SettingCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (weak, nonatomic) IBOutlet UISwitch *actionSwitch;

@property (nonatomic, assign) id<SettingCellDelegate> delegate;

@end
