//
//  MyCollectionTableViewCell.m
//  SimpleDate
//
//  Created by xu.juvenile on 2016/5/9.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "MyCollectionTableViewCell.h"
#import "UIButton+HHImagePosition.h"
#import "HomeModel.h"


@interface MyCollectionTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *genderImg;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *addressImg;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@end

@implementation MyCollectionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.myHeadView setClipsToBounds:YES];
}

- (void)setType:(MyCollectionTableViewCellType)type {
    _type = type;
    
    if (_type == BlackListType) {
        [self.addressBtn setHidden:YES];
    }else{
        [self.addressBtn setHidden:NO];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)blindCellFromModel:(HomeModel*)userItem
{
    [self.myHeadView sd_setImageShowingActivityIndicatorWithURL:[self getImageUrlStringFromPath:userItem.avatar]];
    [self.myNameLabel setText:userItem.nick_name];
    [self.myNameLabel sizeToFit];
    
    if (![SPDCommonTool isEmpty:userItem.personal_desc]) {
        [self.user_descLabel setText:[NSString stringWithFormat:@" %@%@",SPDStringWithKey(@"个性签名:", nil),userItem.personal_desc]];
    }else{
        
        [self.user_descLabel setText:[NSString stringWithFormat:@"%@%@",SPDStringWithKey(@"个性签名:", nil),SPDStringWithKey(@"这个人很懒,什么也没留下", nil)]];
    }
    //性别和年龄
    self.ageLabel.text = [userItem.age stringValue];
    if ([userItem.gender isEqualToString:@"male"]) {
        self.genderImg.image = [UIImage imageNamed:@"ic_male_set"];
    }else{
        self.genderImg.image = [UIImage imageNamed:@"female_set"];
    }
    
    //地址
    NSString *str=[SPDCommonTool getStringFromCityWholeText:userItem.city];
    self.addressImg.image = [UIImage imageNamed:@"city_set"];
    self.addressLabel.text = str;
    
}

@end
