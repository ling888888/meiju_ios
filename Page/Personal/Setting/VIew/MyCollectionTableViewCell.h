//
//  MyCollectionTableViewCell.h
//  SimpleDate
//
//  Created by xu.juvenile on 2016/5/9.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeModel.h"

typedef enum {
    FriendType,
    BlackListType
}MyCollectionTableViewCellType;

@interface MyCollectionTableViewCell : BaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *myHeadView;
@property (weak, nonatomic) IBOutlet UILabel *myNameLabel;
@property (weak, nonatomic) IBOutlet UITextView *mySignInfo;
@property (weak, nonatomic) IBOutlet UILabel *user_descLabel;
@property (weak, nonatomic) IBOutlet UIButton *genderAgeBtn;
@property (weak, nonatomic) IBOutlet UIButton *addressBtn;

@property (nonatomic,assign)MyCollectionTableViewCellType type;

-(void)blindCellFromModel:(HomeModel *)userItem;

@end
