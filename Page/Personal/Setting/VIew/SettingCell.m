
//
//  SettingCell.m
//  SimpleDate
//
//  Created by xu.juvenile on 16/3/7.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "SettingCell.h"

@implementation SettingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.arrowImageView.image = [UIImage imageNamed:@"icon_right_arrow"].adaptiveRtl;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (IBAction)actionSwitchValueChanged:(UISwitch *)sender {
    if ([self.delegate respondsToSelector:@selector(actionSwitch:valueChangedWithName:)]) {
        [self.delegate actionSwitch:sender valueChangedWithName:self.nameLabel.text];
    }
}

@end
