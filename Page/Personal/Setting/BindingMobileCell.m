//
//  BindingMobileCell.m
//  SimpleDate
//
//  Created by 侯玲 on 16/8/31.
//  Copyright © 2016年 WYB. All rights reserved.
//

#import "BindingMobileCell.h"

@interface BindingMobileCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *bindBtn;

@end

@implementation BindingMobileCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.titleLabel.text = SPDStringWithKey(@"手机绑定", nil);
    [self.bindBtn setTitle:SPDStringWithKey(@"绑定", nil) forState:UIControlStateNormal];
}

- (IBAction)bindMobileBtnClicked:(id)sender {
    
    self.clickBindMobileButtonBlock();
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
