//
//  Mine.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/2.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Mine : NSObject

/// 用户id
@property(nonatomic, copy) NSString *_id;

/// 靓号
@property(nonatomic, copy) NSString *specialNum;

/// 头像
@property(nonatomic, copy) NSString *avatar;

/// 昵称
@property(nonatomic, copy) NSString *nick_name;

/// 生日
@property(nonatomic, copy) NSString *birthday;

/// 年龄
@property(nonatomic, copy) NSString *age;

/// 性别
@property(nonatomic, copy) NSString *gender;

/// 简介
@property(nonatomic, copy) NSString *synopsis;

/// 邀请码
@property(nonatomic, copy) NSString *inviteCode;

/// 用户等级
@property(nonatomic, assign) int level;

/// 是否是客服助手
@property(nonatomic, assign) BOOL isServicer;

/// 是否是主播
//@property(nonatomic, assign) BOOL isAnchor;
@property(nonatomic, copy) NSString *liveId;

@end

NS_ASSUME_NONNULL_END
