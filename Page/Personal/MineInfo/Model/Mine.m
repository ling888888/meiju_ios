//
//  Mine.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/2.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "Mine.h"
#import "YYModel.h"

@implementation Mine

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"isSpecialNum":@"if_special",
             @"inviteCode":@"invite_code",
             @"level":@"level.user_level",
             @"isServicer":@"is_agent_gm",
             @"synopsis":@"personal_desc",
             };
}

@end
