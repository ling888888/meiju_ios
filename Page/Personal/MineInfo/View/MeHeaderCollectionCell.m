//
//  MeHeaderCollectionCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "MeHeaderCollectionCell.h"
#import "MeItemCollectionCell.h"
#import "HomeModel.h"
#import "LY_HPTagView.h"
#import "LY_IDView.h"

@interface MeHeaderCollectionCell ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) LY_PortraitView *portraitView;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * IDLabel;
@property (nonatomic, strong) UILabel * fuzhiImageView;
@property (nonatomic, copy) NSArray * dataArray;
@property (nonatomic, copy) LY_HPTagView * tagView;
// ID按钮
@property(nonatomic, strong) LY_IDView *idView;

// 拷贝按钮
@property(nonatomic, strong) UIButton *copyIdBtn;

@end

@implementation MeHeaderCollectionCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews {
    
    [self addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(111);
        make.leading.mas_equalTo(5);
        make.top.mas_equalTo(naviBarHeight - 10);
    }];
    
    [self addSubview:self.nameLabel];
    self.nameLabel.text = [SPDApiUser currentUser].nickName;
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.portraitView.mas_trailing).offset(8);
        make.top.equalTo(self.portraitView.mas_top).offset(16);
        make.trailing.mas_equalTo(-10);
    }];
    
    [self addSubview:self.bottomCollectionView];
    [self.bottomCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(16);
        make.top.equalTo(self.portraitView.mas_bottom).offset(10);
        make.trailing.mas_equalTo(-16);
        make.height.mas_equalTo(106);
    }];
    
    [self addSubview:self.tagView];
    [self.tagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.nameLabel.mas_leading).offset(0);
        make.top.equalTo(self.nameLabel.mas_bottom).offset(9);
        make.height.mas_equalTo(18);
//        make.trailing.mas_equalTo(0);
    }];
    [self addSubview:self.idView];
    [self addSubview:self.copyIdBtn];
    [self.idView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.tagView.mas_bottom).offset(11);
        make.leading.equalTo(self.tagView.mas_leading).offset(0);
        make.height.mas_equalTo(15);
    }];
    [self.copyIdBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.idView.mas_centerY).offset(0);
        make.leading.equalTo(self.idView.mas_trailing).offset(5);
        make.width.and.height.mas_equalTo(20);
    }];
}

- (void)setModel:(HomeModel *)model {
    _model = model;
    
    self.portraitView.portrait = model.portrait;
    self.nameLabel.text =_model.nick_name?:[SPDApiUser currentUser].nickName;
    [self.bottomCollectionView reloadData];
    //tagView
    self.tagView.gender = _model.gender;
    self.tagView.level = [_model.level integerValue];
    self.tagView.isAnchor = _model.liveId.notEmpty;
    self.tagView.customTag = _model.customTag;
    if (_model.medal.notEmpty) {
        self.tagView.isNoble = YES;
        self.tagView.nobleImgUrlStr = _model.medal;
    }
    self.tagView.regalRank = _model.riche.notEmpty ? [_model.riche[@"rank"] integerValue]:-1;
    self.tagView.charmRank = _model.charm.notEmpty ? [_model.charm[@"rank"] integerValue]:-1;
    self.tagView.isVip = _model.is_vip;
    self.tagView.isSpecialNum = NO;
    self.tagView.isUserService = _model.is_agent_gm;
    [self.tagView refresh];
    
    if (_model.specialNum.notEmpty) {
        [self.idView setID:_model.specialNum isSpecialNum:true];
    } else {
        [self.idView setID:_model.invite_code?:@"900000" isSpecialNum:false];
    }
}

- (void)handleTapAvatar:(UITapGestureRecognizer *)tap {
    if (self.delegate && [self.delegate respondsToSelector:@selector(meHeaderCollectionCellDidClickedAvatarWithUserId:)]) {
        [self.delegate meHeaderCollectionCellDidClickedAvatarWithUserId:_model.user_id];
    }
}

#pragma mark - UICollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MeItemCollectionCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MeItemCollectionCell" forIndexPath:indexPath];
    [cell updateIcon:self.dataArray[indexPath.row][@"icon"] title:self.dataArray[indexPath.row][@"name"]];
    return cell;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 40, 0, 40);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((kScreenW - 32 - 80)/2, 86);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate && [self.delegate respondsToSelector:@selector(meHeaderCollectionCellDidClickedWithType:)]) {
        [self.delegate meHeaderCollectionCellDidClickedWithType:self.dataArray[indexPath.row][@"type"]];
    }
}

/// 复制ID事件
- (void)copyIDAction {
    [[UIPasteboard generalPasteboard] setString:self.idView.value];
    [LY_HUD showToastTips:@"复制成功".localized];
}


#pragma mark - getters

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [LY_PortraitView new];
        _portraitView.contentMode = UIViewContentModeScaleAspectFill;
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapAvatar:)];
        [_portraitView addGestureRecognizer:tap];
    }
    return _portraitView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.font = [UIFont mediumFontOfSize:18];
        _nameLabel.textColor = SPD_HEXCOlOR(@"F8F8F8");
        _nameLabel.text = [SPDApiUser currentUser].userId;
    }
    return _nameLabel;
}

- (UICollectionView *)bottomCollectionView {
    if (!_bottomCollectionView) {
        UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout new];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        _bottomCollectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _bottomCollectionView.delegate = self;
        _bottomCollectionView.dataSource = self;
        _bottomCollectionView.backgroundColor = [UIColor whiteColor];
        _bottomCollectionView.scrollEnabled = NO;
        _bottomCollectionView.layer.cornerRadius = 10;
        [_bottomCollectionView registerClass:[MeItemCollectionCell class] forCellWithReuseIdentifier:@"MeItemCollectionCell"];
        _bottomCollectionView.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
        _bottomCollectionView.layer.shadowColor = [UIColor colorWithRed:47/255.0 green:45/255.0 blue:51/255.0 alpha:0.08].CGColor;
        _bottomCollectionView.layer.shadowOffset = CGSizeMake(0,5);
        _bottomCollectionView.layer.shadowOpacity = 1;
        _bottomCollectionView.layer.shadowRadius = 10;
    }
    return _bottomCollectionView;
}

- (NSArray *)dataArray {
    if (!_dataArray) {
        _dataArray = @[@{
                           @"type": @"wallet",
                           @"icon": @"ic_wode_wodeqianbao",
                           @"name": @"我的钱包"
                        },
                       @{
                          @"type": @"order",
                          @"icon": @"ic_wode_woyaozhibo",
                          @"name": @"我的订单"
                       }
        ];
    }
    return _dataArray;
}

- (LY_HPTagView *)tagView {
    if (!_tagView) {
        _tagView = [LY_HPTagView new];
    }
    return _tagView;
}

- (LY_IDView *)idView {
    if (!_idView) {
        _idView = [[LY_IDView alloc] init];
        _idView.normalIDColor = [UIColor colorWithHexString:@"ffffff"];
        _idView.normalIDFont = [UIFont mediumFontOfSize:12];
        _idView.specialIDFont = [UIFont mediumFontOfSize:12];
        [_idView addTarget:self action:@selector(copyIDAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _idView;
}

- (UIButton *)copyIdBtn {
    if (!_copyIdBtn) {
        _copyIdBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_copyIdBtn setImage:[UIImage imageNamed:@"ic_wode_fuzhi"] forState:UIControlStateNormal];
        [_copyIdBtn addTarget:self action:@selector(copyIDAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _copyIdBtn;
}
@end

