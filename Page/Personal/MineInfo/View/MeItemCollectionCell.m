//
//  MeItemCollectionCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "MeItemCollectionCell.h"

@interface MeItemCollectionCell ()

@property (nonatomic, strong) UILabel * nameLabel;
@end

@implementation MeItemCollectionCell


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubView];
    }
    return self;
}


- (void)initSubView {
    self.coverImageView = [[UIImageView alloc]init];
    [self.contentView addSubview:self.coverImageView];
    [self.coverImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.centerX.mas_equalTo(0);
        make.width.and.height.mas_equalTo(45);
    }];
    
    self.nameLabel = [UILabel new];
    self.nameLabel.textColor = SPD_HEXCOlOR(@"#1A1A1A");
    self.nameLabel.font = [UIFont systemFontOfSize:12];
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.coverImageView.mas_bottom).offset(5);
        make.leading.mas_equalTo(5);
        make.trailing.mas_equalTo(-5);
    }];
    
    self.levelLabel = [UILabel new];
    self.levelLabel.hidden = YES;
    self.levelLabel.text = @"1";
    [self.contentView addSubview:self.levelLabel];
    self.levelLabel.textColor = SPD_HEXCOlOR(@"#1A1A1A");
    self.levelLabel.font = [UIFont boldSystemFontOfSize:12];
    [self.levelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.coverImageView.mas_centerX).offset(0);
        make.centerY.equalTo(self.coverImageView.mas_centerY).offset(0);
    }];
}

- (void)updateIcon:(NSString *)icon title:(NSString *)title {
    self.coverImageView.image = [UIImage imageNamed:icon];
    self.nameLabel.text = title.localized;
    self.levelLabel.hidden = YES;
}

- (void)setIconWidth:(CGFloat)iconWidth {
    _iconWidth = iconWidth;
    [self.coverImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.and.height.mas_equalTo(_iconWidth);
    }];
    [self layoutIfNeeded];
}

- (void)setNameColor:(NSString *)nameColor {
    _nameColor = nameColor;
    self.nameLabel.textColor = SPD_HEXCOlOR(_nameColor);
}

- (void)setNameFont:(CGFloat)nameFont {
    _nameFont = nameFont;
    self.nameLabel.font = [UIFont mediumFontOfSize:_nameFont];
}

@end
