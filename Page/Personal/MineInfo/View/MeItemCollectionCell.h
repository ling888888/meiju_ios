//
//  MeItemCollectionCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MeItemCollectionCell : UICollectionViewCell

@property (nonatomic, assign) CGFloat iconWidth;
@property (nonatomic, assign) CGFloat nameFont;
@property (nonatomic, copy) NSString * nameColor;

@property (nonatomic, strong) UILabel * levelLabel;
@property (nonatomic, strong) UIImageView * coverImageView;


- (void)updateIcon:(NSString *)icon title:(NSString *)title;


@end

NS_ASSUME_NONNULL_END
