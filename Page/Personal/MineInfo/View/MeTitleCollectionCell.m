//
//  MeTitleCollectionCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "MeTitleCollectionCell.h"

@interface MeTitleCollectionCell ()

@property (nonatomic, strong) UIView * flagView;
@property (nonatomic, strong) UILabel * titleLabel;

@end

@implementation MeTitleCollectionCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews {
    
    self.flagView = [UIView new];
    self.flagView.backgroundColor = SPD_HEXCOlOR(COMMON_PINK);
    [self.contentView addSubview:self.flagView];
    [self.flagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(20);
        make.width.mas_equalTo(2);
        make.height.mas_equalTo(13);
        make.centerY.mas_equalTo(0);

    }];
    
    self.titleLabel = [UILabel new];
    self.titleLabel.textColor = SPD_HEXCOlOR(@"#1A1A1A");
    self.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.flagView.mas_trailing).offset(10);
        make.centerY.mas_equalTo(0);
    }];
}

- (void)setTitle:(NSString *)title {
    _title = title;
    self.titleLabel.text = _title;
}

@end
