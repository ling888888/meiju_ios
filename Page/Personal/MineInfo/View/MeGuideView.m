//
//  MeGuideView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/5/8.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "MeGuideView.h"

@interface MeGuideView ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leading;
@property (weak, nonatomic) IBOutlet UIImageView *arraow1;
@property (weak, nonatomic) IBOutlet UIImageView *bg1;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIImageView *bg2;
@property (weak, nonatomic) IBOutlet UIImageView *img2;
@property (weak, nonatomic) IBOutlet UIImageView *img1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet UIImageView *arrow2;
@property (weak, nonatomic) IBOutlet UIImageView *bg3;
@property (weak, nonatomic) IBOutlet UILabel *label3;
@property (weak, nonatomic) IBOutlet UIImageView *arrow3;
@property (weak, nonatomic) IBOutlet UIImageView *img3;
@property (weak, nonatomic) IBOutlet UIButton *btn3;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topcons;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lastLeading;

@end

@implementation MeGuideView

- (void)awakeFromNib{
    [super awakeFromNib];
    
    self.top1.constant = statusBarHeight >=44 ? 217 - 15:193-15;
    self.leading.constant = (ceil((kScreenW -32)/4.0f) - 45)/2 + 16 - 8;
    self.lastLeading.constant = (ceil((kScreenW -32)/4.0f) - 45)/2 + 16 - 8;
    self.topcons.constant = 539;
    
    [self bringSubviewToFront:self.label2];
    [self bringSubviewToFront:self.btn2];
}

- (IBAction)btn1:(id)sender {
    self.process = @"2";
}

- (IBAction)btn2:(id)sender {
    self.process = @"3";
}
- (IBAction)tap:(id)sender {
    if ([self.process isEqualToString:@"1"]) {
        self.process = @"2";
    }else if ([self.process isEqualToString:@"2"]){
        self.process = @"3";
    }else{
        [self removeFromSuperview];
    }
}

- (void)setProcess:(NSString *)process {
    _process = process;
    self.img1.hidden = ![_process isEqualToString:@"1"];
    self.arraow1.hidden = ![_process isEqualToString:@"1"];
    self.bg1.hidden = ![_process isEqualToString:@"1"];
    self.label1.hidden = ![_process isEqualToString:@"1"];
    self.btn1.hidden = ![_process isEqualToString:@"1"];
    
    self.img2.hidden = ![_process isEqualToString:@"2"];
    self.arrow2.hidden = ![_process isEqualToString:@"2"];
    self.bg2.hidden = ![_process isEqualToString:@"2"];
    self.label2.hidden = ![_process isEqualToString:@"2"];
    self.btn2.hidden = ![_process isEqualToString:@"2"];
    
    self.img3.hidden = ![_process isEqualToString:@"3"];
    self.arrow3.hidden = ![_process isEqualToString:@"3"];
    self.bg3.hidden = ![_process isEqualToString:@"3"];
    self.label3.hidden = ![_process isEqualToString:@"3"];
    self.btn3.hidden = ![_process isEqualToString:@"3"];
    
}
- (IBAction)btnr:(id)sender {
    [self removeFromSuperview];
}

@end
