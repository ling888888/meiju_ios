//
//  MeHeaderCollectionCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class HomeModel;

@protocol MeHeaderCollectionCellDelegate <NSObject>

@optional
- (void)meHeaderCollectionCellDidClickedWithType:(NSString *)type;
- (void)meHeaderCollectionCellDidClickedAvatarWithUserId:(NSString *)userId;
@end

@interface MeHeaderCollectionCell : UICollectionViewCell

@property (nonatomic, strong) UICollectionView * bottomCollectionView;

@property (nonatomic, strong) HomeModel * model;

@property (nonatomic, weak)id<MeHeaderCollectionCellDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
