//
//  MeViewController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "MeViewController.h"
#import "MeHeaderCollectionCell.h"
#import "HomeModel.h"
#import "SettingTableController.h"
#import "MeTitleCollectionCell.h"
#import "MeItemCollectionCell.h"
#import "SPDImmigrantController.h"
#import "SpecialNumViewController.h"
#import "StoreNewRecommandController.h"
#import "NobleViewController.h"
#import "SPDVehicleViewController.h"
#import "SPDHelpController.h"
#import "SPDMagicViewController.h"
#import "SPDInvitationController.h"
#import "CallSettingsViewController.h"
#import "SettingTableController.h"
#import "LY_LiveFiringViewController.h"
#import "LY_RechargeViewController.h"
#import "SPDWeekSignInView.h"
#import "ChatroomWealthController.h"
#import "FollowContainerController.h"
#import "DetailsUserFansController.h"
#import "MeGuideView.h"
#import "LY_HomePageViewController.h"
#import "LiveApplyAnchorSubmitController.h"
#import "LiveApplyAnchorConditionsViewController.h"
#import "MedalListController.h"

@interface MeViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,MeHeaderCollectionCellDelegate,SPDWeekSignInViewDelegate>

@property (nonatomic, strong) UICollectionView * collectionView;
// 系统消息开关
@property(nonatomic, assign) BOOL systemMsgSwitch;

// 可视开关
@property(nonatomic, assign) BOOL visibleSwitch;

@property (nonatomic, strong) HomeModel * model;

@property (nonatomic, strong) UIImageView * bgImageView;

@property (nonatomic, strong) NSMutableDictionary * signDict;

@property (nonatomic, copy) NSArray * dataArray;

@end

@implementation MeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (@available(iOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ic_wode_shezhi"] style:UIBarButtonItemStylePlain target:self action:@selector(handlSettings:)];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.bgImageView];
    [self.view addSubview:self.collectionView];
    [self showGuide];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:0.0];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
    [self requestSignIn];
    [self setupData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:1.0];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
}

- (void)showGuide {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"MeGuideView"]) {
        MeGuideView * view = [[[NSBundle mainBundle]loadNibNamed:@"MeGuideView" owner:self options:nil]firstObject];
        view.frame = self.view.bounds;
        view.process = @"1";
        [[UIApplication sharedApplication].keyWindow addSubview:view];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MeGuideView"];
    }
}

#pragma mark - response

- (void)handlSettings:(UIBarButtonItem *)item {
    SettingTableController *settingVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SettingTableController"];
    settingVC.sysMsgSwitch = self.systemMsgSwitch;
    settingVC.visibleSwitch = self.visibleSwitch;
    [self.navigationController pushViewController:settingVC animated:true];
}

#pragma mark - request

- (void)setupData {
    [LY_Network getRequestWithURL:LY_Api.mine_info parameters:nil success:^(id  _Nullable response) {
        self.model = [HomeModel initWithDictionary:response];
        [self.collectionView reloadData];
        self.systemMsgSwitch = ![[response objectForKey:@"sysmsg_status"] isEqualToString:@"on"];
        self.visibleSwitch = ![[response objectForKey:@"visual_status"] isEqualToString:@"visible"];
        NSDictionary *userInfo = @{
            @"_id" : response[@"_id"],
            @"avatar" : response[@"avatar"],
            @"ry_token" : [SPDApiUser currentUser].ryToken,
            @"nick_name" : response[@"nick_name"],
            @"if_special": @([response[@"if_special"] boolValue]),
            @"gender":response[@"gender"],
            @"age":@([response[@"age"] intValue]),
            @"lang":response[@"lang"]?:@"ar",
            @"share":response[@"share"]
        };
        [[SPDApiUser currentUser] saveUserInfoToDefault:userInfo];
        
        
        [LY_User currentUser].portrait = self.model.portrait.url;
        [LY_User currentUser].headwearWebp = self.model.portrait.headwearUrl;
        
        
        //
        BOOL is_vip=[response[@"is_vip"]boolValue];
        if (is_vip) {
            [[NSUserDefaults standardUserDefaults] setObject:@"true" forKey:ISVIP];
        }else{
            [[NSUserDefaults standardUserDefaults] setObject:@"false" forKey:ISVIP];
        }
        //保存下
        NSInteger noble_value = [response[@"noble_value"] integerValue];
        [[NSUserDefaults standardUserDefaults] setInteger:noble_value forKey:[NSString stringWithFormat:@"noble:%@",[SPDApiUser currentUser].userId]];
        //刷新用户在融云的信息
        RCUserInfo * currentUserInfo =[[RCIM sharedRCIM] currentUserInfo];
        currentUserInfo.portraitUri = [NSString stringWithFormat:STATIC_DOMAIN_URL,response[@"avatar"]];
        currentUserInfo.name = response[@"nick_name"];
        [[RCIM sharedRCIM] refreshUserInfoCache:currentUserInfo withUserId:currentUserInfo.userId];
        
        // 做引导
        
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

- (void)requestSignIn {
    __weak typeof(self) weakSelf = self;
    [LY_Network getRequestWithURL:LY_Api.mine_signIn parameters:nil success:^(id  _Nullable response) {
        weakSelf.signDict = response;
        [self.collectionView reloadData];
    } failture:^(NSError * _Nullable error) {
        
    }];
}

#pragma mark - delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 5;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSArray *array = self.dataArray[section];
    return array.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        MeHeaderCollectionCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MeHeaderCollectionCell" forIndexPath:indexPath];
        cell.delegate = self;
        cell.model = self.model;
        return cell;
    }else if (indexPath.section == 2 || indexPath.section == 4){
        MeItemCollectionCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MeItemCollectionCell" forIndexPath:indexPath];
        cell.iconWidth = 37;
        cell.nameFont = 12;
        cell.nameColor = @"1A1A1A";
        NSArray * array = self.dataArray[indexPath.section];
        NSString * type = array[indexPath.row][@"type"];
        [cell updateIcon:array[indexPath.row][@"icon"] title:array[indexPath.row][@"name"]];
        if ([type isEqualToString:@"level"]) {
            cell.levelLabel.text = [self.model.level stringValue];
            cell.levelLabel.hidden = NO;
        }else if ([type isEqualToString:@"sign"]){
            BOOL sign = [self.signDict[@"check"] isEqualToString:@"true"];
            NSString * icon = sign ? @"ic_wode_qiandao":array[indexPath.row][@"icon"];
            NSString * name = sign ? @"已签到".localized: array[indexPath.row][@"name"];
            [cell updateIcon:icon title:name];
            cell.nameColor = sign ? @"1A1A1A":@"00D4A0";
        }
        return cell;
    }
    else{
        MeTitleCollectionCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MeTitleCollectionCell" forIndexPath:indexPath];
        cell.title = (indexPath.section == 1)? @"增值服务".localized : @"常用功能".localized;
        return cell;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            return CGSizeMake(kScreenW,  20 + 106 + 163 - 64 + NavHeight);
            break;
        case 2:
            return CGSizeMake((kScreenW - 18*2)/4.0f, 73);
            break;
        case 4:
            return CGSizeMake((kScreenW - 18*2)/4.0f, 73);
            break;
        default:
            return CGSizeMake(kScreenW, 24);
            break;
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    switch (section) {
        case 2:
            return UIEdgeInsetsMake(0, 18, 0, 18);
            break;
        case 3:
            return UIEdgeInsetsMake(30, 0, 10, 0);
            break;
        case 4:
            return UIEdgeInsetsMake(0, 18, 0, 18);
            break;
        default:
            return UIEdgeInsetsZero;
            break;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2 || indexPath.section == 4) {
        NSString * type = self.dataArray[indexPath.section][indexPath.row][@"type"];
        if ([type isEqualToString:@"store"]) {
            [self.navigationController pushViewController:[StoreNewRecommandController new] animated:YES];
        }else if ([type isEqualToString:@"lianghao"]){
            [self.navigationController pushViewController:[SpecialNumViewController new] animated:YES];
        }else if ([type isEqualToString:@"vehicle"]){
            [SPDCommonTool requestAccountPropertyAPIWithType:@"personal" viewController:self alertBindFailed:^(id alertFailed) {
                SPDVehicleViewController *vc = [[SPDVehicleViewController alloc] init];
                vc.isMine = YES;
                vc.user_id = [SPDApiUser currentUser].userId;
                [self.navigationController pushViewController:vc animated:YES];
            }];
        }else if ([type isEqualToString:@"magic"]){
            SPDMagicViewController *vc = [[SPDMagicViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }else if ([type isEqualToString:@"level"]){
            NSString * str = [NSString stringWithFormat:RANKLEVEL,[SPDApiUser currentUser].userId,[SPDCommonTool getFamyLanguage]];
            SPDHelpController *helpController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SPDHelpController"];
            helpController.url = str;
            helpController.titleText = SPDStringWithKey(@"我的等级", nil);
            [self.navigationController pushViewController:helpController animated:true];
        }else if ([type isEqualToString:@"sign"]){
            SPDWeekSignInView * view = [[[NSBundle mainBundle] loadNibNamed:@"SPDWeekSignInView" owner:self options:nil]lastObject];
            view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
            view.dataDict = [self.signDict copy];
            view.delegate = self;
            [[UIApplication sharedApplication].keyWindow addSubview:view];
        }else if ([type isEqualToString:@"lang"]){
            [self loadWebViewController:self title:@"" url:[NSString stringWithFormat:IMMIGRANT_RULE_URL,[SPDCommonTool getFamyLanguage]]];
        }else if ([type isEqualToString:@"set"]){
            CallSettingsViewController *vc = [CallSettingsViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }else if ([type isEqualToString:@"invite"]){
            SPDInvitationController *vc = [[SPDInvitationController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }else if ([type isEqualToString:@"medal"]){
            MedalListController * medalVC = [MedalListController new];
            medalVC.userId = [SPDApiUser currentUser].userId;
            [self.navigationController pushViewController:medalVC animated:YES];
        }
    }
}

- (void)meHeaderCollectionCellDidClickedWithType:(NSString *)type {
    if ([type isEqualToString:@"wallet"]) {
        [SPDCommonTool requestAccountPropertyAPIWithType:@"personal" viewController:self alertBindFailed:^(id alertFailed) {
            [SPDCommonTool pushToRechageController:self];
        }];
    }else if ([type isEqualToString:@"live"]){
        if (self.model._id.notEmpty) {
            if (self.model.liveId.notEmpty) {
                [self.navigationController pushViewController:[LY_LiveFiringViewController new] animated:true];
            }else{
                [self.navigationController pushViewController:[LiveApplyAnchorConditionsViewController new] animated:YES];
            }
        }
    }else if ([type isEqualToString:@"noble"]){
        NobleViewController *vc = [[NobleViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([type isEqualToString:@"info"]){
        LY_HomePageViewController *homePageVC = [[LY_HomePageViewController alloc] initWithUserId:[SPDApiUser currentUser].userId];
        [self.navigationController pushViewController:homePageVC animated:true];
    }
}

- (void)meHeaderCollectionCellDidClickedAvatarWithUserId:(NSString *)userId {
    LY_HomePageViewController *homePageVC = [[LY_HomePageViewController alloc] initWithUserId:[SPDApiUser currentUser].userId];
    [self.navigationController pushViewController:homePageVC animated:true];
}

#pragma mark - SPDWeekSignInViewDelegate

- (void)weekSignInViewSignInComplete {
    [self requestSignIn];
}

#pragma mark - scrollview

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat yOffset = scrollView.contentOffset.y;
    CGFloat originHeight =  106 + 163 - 64 + NavHeight  - 53 ;
    if(yOffset < 0){
        CGFloat totalOffset = originHeight + ABS(yOffset);
        CGFloat f = totalOffset / originHeight;
        CGRect headerImageViewFrame = self.bgImageView.frame;
        headerImageViewFrame.size.height = totalOffset;
        headerImageViewFrame.size.width = kScreenW * f;
        self.bgImageView.frame = headerImageViewFrame;
    }else{
        CGRect headerImageViewFrame = self.bgImageView.frame;
        headerImageViewFrame.origin.y = -scrollView.contentOffset.y;
        headerImageViewFrame.size.height = originHeight;
        headerImageViewFrame.size.width = kScreenW;
        self.bgImageView.frame = headerImageViewFrame;
    }
    
}

#pragma mark - getters

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout new];
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 5;
        _collectionView = [[UICollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerClass:[MeHeaderCollectionCell class] forCellWithReuseIdentifier:@"MeHeaderCollectionCell"];
        [_collectionView registerClass:[MeTitleCollectionCell class] forCellWithReuseIdentifier:@"MeTitleCollectionCell"];
        [_collectionView registerClass:[MeItemCollectionCell class] forCellWithReuseIdentifier:@"MeItemCollectionCell"];
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.backgroundColor = [UIColor clearColor];
    }
    return _collectionView;
}

- (UIImageView *)bgImageView {
    if (!_bgImageView) {
        _bgImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kScreenW, 149 + naviBarHeight)];
        _bgImageView.image = [UIImage imageNamed:@"img_wode_bg"];
        _bgImageView.frame = CGRectMake(0, 0, kScreenW,  149 + naviBarHeight);
    }
    return _bgImageView;
}

- (NSMutableDictionary *)signDict {
    if (!_signDict) {
        _signDict = [NSMutableDictionary new];
    }
    return _signDict;
}

- (NSArray *)dataArray{
    if (!_dataArray) {
        _dataArray = @[@[@"1"],@[@"常用功能"],@[@{
            @"type": @"lianghao",
            @"icon": @"ic_wode_lianghaoid",
            @"name": @"靓号ID"
        },
        @{
            @"type": @"zhujiao",
            @"icon": @"ic_zhujiaozhuanshu",
            @"name": @"助教专属"
        },
        @{
            @"type": @"vehicle",
            @"icon": @"ic_wode_zuojia",
            @"name": @"座驾"
        },
                                            //        @{
                                            //           @"type": @"magic",
                                            //           @"icon": @"ic_wode_wodemofa",
                                            //           @"name": @"我的魔法"
                                            //        },
    @{
                                        @"type": @"level",
                                        @"icon": @"ic_wode_wodedengji",
                                        @"name": @"我的等级"
    }
//                                            ,@{
//        @"type": @"medal",
//        @"icon": @"ic_wode_wodexunzhang",
//        @"name": @"我的勋章墙"
//    }
        ],@[@"更多服务"],@[@{
        @"type": @"shezhi",
        @"icon": @"ic_wode_tonghuashezhi",
        @"name": @"设置"
    },@{
        @"type": @"kefu",
        @"icon": @"ic_kefu",
        @"name": @"联系客服"
    }
//                    ,
//                    @{
//                        @"type": @"set",
//                        @"icon": @"ic_wode_tonghuashezhi",
//                        @"name": @"通话设置"
//                    },
//                    @{
//                        @"type": @"invite",
//                        @"icon": @"ic_wode_yaoqing",
//                        @"name": @"邀请"
//                    }
    ]];
}
return _dataArray;
}


@end
