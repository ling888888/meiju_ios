//
//  LY_HomePageViewController.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/30.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_BaseViewController.h"
#import "LY_HPBaseInfoView.h"
#import "LY_HPLiveInfoView.h"
#import "LY_HPFamilyView.h"
#import "LY_HPGuardListView.h"
#import "LY_HPAlbumView.h"
#import "LY_HPSocialView.h"
#import "LY_HPBottomView.h"
#import "LY_HPGuideView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_HomePageViewController : LY_BaseViewController

/// 查寻资料用户ID
@property(nonatomic, copy, readonly) NSString *userId;

/// 查看资料是：自己信息、其他人信息
@property(nonatomic, assign, readonly) LY_HomePageType type;

/// 初始化方法
/// @param userId 查寻资料用户ID
- (instancetype)initWithUserId:(NSString *)userId;



@end

NS_ASSUME_NONNULL_END
