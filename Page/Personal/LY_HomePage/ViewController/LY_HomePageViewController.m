//
//  LY_HomePageViewController.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/30.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_HomePageViewController.h"
#import "ZegoKitManager.h"
#import "SPDConversationViewController.h"
#import "ReportViewController.h"
#import "LY_HPMedalView.h"
#import "MedalModel.h"
#import "LY_EditInfoViewController.h"
#import "LY_HPPWView.h"

@interface LY_HomePageViewController () <UIScrollViewDelegate, LY_HPBottomViewDelegate>

@property(nonatomic, strong) UIButton *trailingNaviItemBtn;

@property(nonatomic, strong) UIButton *replaceBackgroundImgBtn;

@property(nonatomic, strong) UIButton *editInformationBtn;

@property(nonatomic, strong) UIScrollView *scrollView;

@property(nonatomic, strong) UIView *containerView;

@property(nonatomic, strong) LY_HPBaseInfoView *baseInfoView;

@property(nonatomic, strong) LY_HPLiveInfoView *liveInfoView;

@property(nonatomic, strong) LY_HPFamilyView *familyView;

@property(nonatomic, strong) LY_HPGuardListView *guardListView;

@property(nonatomic, strong) LY_HPAlbumView *albumView;

@property(nonatomic, strong) LY_HPSocialView *socialView;

@property(nonatomic, strong) LY_HPBottomView *bottomView;

@property(nonatomic, strong) LY_HomePageInfo *userInfo;

@property (nonatomic, strong) LY_HPMedalView * medalView;

@property (nonatomic, strong) LY_HPPWView * zhujiaoView;

@property(nonatomic, assign) CGFloat navigationBarAlpha;

@property (nonatomic, strong) NSMutableArray * medalArray;

@end

@implementation LY_HomePageViewController

- (UIButton *)trailingNaviItemBtn {
    if (!_trailingNaviItemBtn) {
        _trailingNaviItemBtn = [[UIButton alloc] init];
        [_trailingNaviItemBtn addTarget:self action:@selector(trailingNaviItemBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _trailingNaviItemBtn;
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.alwaysBounceVertical = true;
        _scrollView.delegate = self;
        
    }
    return _scrollView;
}

- (UIView *)containerView {
    if (!_containerView) {
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor whiteColor];
    }
    return _containerView;
}

- (LY_HPBaseInfoView *)baseInfoView {
    if (!_baseInfoView) {
        _baseInfoView = [[LY_HPBaseInfoView alloc] initWithType:self.type];
        _baseInfoView.backgroundColor = [UIColor whiteColor];
    }
    return _baseInfoView;
}

- (LY_HPLiveInfoView *)liveInfoView {
    if (!_liveInfoView) {
        _liveInfoView = [[LY_HPLiveInfoView alloc] initWithType:self.type];
        _liveInfoView.backgroundColor = [UIColor whiteColor];
    }
    return _liveInfoView;
}

- (LY_HPFamilyView *)familyView {
    if (!_familyView) {
        _familyView = [[LY_HPFamilyView alloc] initWithType:self.type];
        _familyView.backgroundColor = [UIColor whiteColor];
    }
    return _familyView;
}

- (LY_HPMedalView *)medalView {
    if (!_medalView) {
        _medalView = [[LY_HPMedalView alloc] initWithType:self.type];
        _medalView.backgroundColor = [UIColor whiteColor];
        _medalView.userId = self.userId;
        _medalView.hidden = YES;
    }
    return _medalView;
}

- (NSMutableArray *)medalArray {
    if (!_medalArray) {
        _medalArray = [NSMutableArray new];
    }
    return _medalArray;
}

- (LY_HPGuardListView *)guardListView {
    if (!_guardListView) {
        _guardListView = [[LY_HPGuardListView alloc] initWithType:self.type];
        _guardListView.backgroundColor = [UIColor whiteColor];
    }
    return _guardListView;
}

- (LY_HPAlbumView *)albumView {
    if (!_albumView) {
        _albumView = [[LY_HPAlbumView alloc] initWithType:self.type];
        _albumView.backgroundColor = [UIColor whiteColor];
    }
    return _albumView;
}

- (LY_HPSocialView *)socialView {
    if (!_socialView) {
        _socialView = [[LY_HPSocialView alloc] initWithType:self.type];
        _socialView.backgroundColor = [UIColor whiteColor];
    }
    return _socialView;
}

- (LY_HPBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[LY_HPBottomView alloc] initWithType:self.type];
        _bottomView.delegate = self;
    }
    return _bottomView;
}

- (LY_HPPWView *)zhujiaoView {
    if (!_zhujiaoView) {
        _zhujiaoView = [[LY_HPPWView alloc] initWithType:self.type];
//        _zhujiaoView.delegate = self;
    }
    return _zhujiaoView;
}

- (instancetype)initWithUserId:(NSString *)userId {
    self = [super init];
    if (self) {
        _userId = userId;
        // 判断是否是查看自己的资料
        if ([userId isEqualToString:[SPDApiUser currentUser].userId]) {
            _type = LY_HomePageTypeMine;
        } else {
            _type = LY_HomePageTypeOthers;
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavigation];
    
    [self setupUI];
    
    [self setupUIFrame];
    
    // 判断是否是查看自己资料
    if (self.type == LY_HomePageTypeMine) {
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isShowedHPGuide"]) {
            LY_HPGuideView *guideView = [[NSBundle mainBundle] loadNibNamed:@"LY_HPGuideView" owner:self options:nil].firstObject;
            
            [self.view addSubview:guideView];
            [guideView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(self.view);
            }];
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isShowedHPGuide"];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar vhl_setBackgroundColor:[UIColor whiteColor]];
    [self updateNavigationBarAndStatusStyleForNavigationBarAlpha:self.navigationBarAlpha];
    
    [self setupData];
    
    [self requestList]; // 勋章
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar vhl_setBackgroundColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:1.0];
}

- (void)setupNavigation {
    
    // 关闭scrollView自动偏移
    if (@available(iOS 11.0, *)) {
        self.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    // 判断是否是查看自己资料
    if (self.type == LY_HomePageTypeMine) {
        //[MobClick event:@"FM5_2_1"];
        UIButton * btn = [[UIButton alloc]init];
        [btn setImage:[UIImage imageNamed:@"ic_yonghuziliaoye_bianji"] forState:UIControlStateNormal];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
        [btn addTarget:self action:@selector(handleInfo:) forControlEvents:UIControlEventTouchUpInside];
    } else {
        //[MobClick event:@"FM5_3"];
        [self.trailingNaviItemBtn setImage:[UIImage imageNamed:@"ic_gerenzhuye_gengduo"] forState:UIControlStateNormal];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.trailingNaviItemBtn];
    }
}

- (void)handleInfo:(UIButton *)sender {
    LY_EditInfoViewController *editInfoVC = [[LY_EditInfoViewController alloc] init];
    [self.navigationController pushViewController:editInfoVC animated:true];
}

- (void)setupUI {
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.containerView];
    [self.containerView addSubview:self.baseInfoView];
    [self.containerView addSubview:self.zhujiaoView];
//    [self.containerView addSubview:self.liveInfoView];
    [self.containerView addSubview:self.familyView];
//    [self.containerView addSubview:self.guardListView];
//    [self.containerView addSubview:self.medalView];
//    [self.containerView addSubview:self.albumView];
    [self.containerView addSubview:self.socialView];
    [self.view addSubview:self.bottomView];
}

- (void)setupUIFrame {
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.scrollView);
        make.width.mas_equalTo(kScreenW);
//        make.height.mas_equalTo(kScreenH + 500);
    }];
    [self.baseInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.mas_equalTo(self.containerView);
    }];
//    [self.zhujiaoView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.leading.trailing.mas_equalTo(self.baseInfoView);
//    }];
    [self.zhujiaoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.baseInfoView.mas_bottom);
        make.leading.trailing.mas_equalTo(self.containerView);
    }];
    [self.familyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.zhujiaoView.mas_bottom);
        make.leading.trailing.mas_equalTo(self.containerView);
    }];
//    [self.guardListView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.familyView.mas_bottom);
//        make.leading.trailing.mas_equalTo(self.containerView);
//    }];
    
//    [self.medalView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.guardListView.mas_bottom);
//        make.leading.trailing.mas_equalTo(self.containerView);
//    }];
//
//    [self.albumView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.guardListView.mas_bottom);
//        make.leading.trailing.mas_equalTo(self.containerView);
//    }];
    [self.socialView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.familyView.mas_bottom);
        make.leading.trailing.mas_equalTo(self.containerView);
        make.bottom.mas_equalTo(-80);
    }];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.mas_equalTo(self.view);
        make.height.mas_equalTo(72.5);
        make.bottom.mas_equalTo(-10);
    }];
}

- (void)setupData {
    __weak typeof(self) weakSelf = self;
    if (!self.userId.notEmpty) {
        [LY_HUD showToastTips:@"userId不能为空"];
        [self.navigationController popViewControllerAnimated:true];
        return;
    }
    NSDictionary *params = @{@"user_id":self.userId};
    [LY_Network getRequestWithURL:LY_Api.user_detail parameters:params success:^(id  _Nullable response) {
        LY_HomePageInfo *userInfo = [LY_HomePageInfo yy_modelWithDictionary:response];
        weakSelf.userInfo = userInfo;
        
        weakSelf.baseInfoView.userInfo = userInfo;
        weakSelf.liveInfoView.userInfo = userInfo;
        weakSelf.familyView.userInfo = userInfo;
        weakSelf.guardListView.userInfo = userInfo;
        weakSelf.albumView.userInfo = userInfo;
        weakSelf.medalView.userInfo = userInfo;

        if (self.type == LY_HomePageTypeOthers) {
            if (userInfo.albumList.count == 0) {
                [weakSelf.socialView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.top.mas_equalTo(self.guardListView.mas_bottom);
                }];
            } else {
                [weakSelf.socialView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.top.mas_equalTo(self.albumView.mas_bottom);
                }];
            }
        }
        
        weakSelf.socialView.userInfo = userInfo;
        weakSelf.bottomView.userInfo = userInfo;
        
        if (userInfo.homepageFalling.notEmpty) {
            [self showNobleAnimation:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:userInfo.homepageFalling]]]]];
        }
        weakSelf.scrollView.showEmptyTipView = false;
        weakSelf.containerView.hidden = false;
        weakSelf.bottomView.hidden = false;
        weakSelf.trailingNaviItemBtn.hidden = false;
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
        
        weakSelf.scrollView.tipImageName = @"img_zhibo_wuwangluo";
        weakSelf.scrollView.tipText = @"无法连接到网络".localized;
        weakSelf.scrollView.tipButtonTitle = @"刷新页面".localized;
        weakSelf.scrollView.tipButton.backgroundColor = [UIColor colorWithHexString:@"#1A1A1A"];
        [weakSelf.scrollView.tipButton addTarget:self action:@selector(notInterNetTipButtonAction) forControlEvents:UIControlEventTouchUpInside];
        weakSelf.scrollView.showEmptyTipView = true;
        weakSelf.containerView.hidden = true;
        weakSelf.bottomView.hidden = true;
        weakSelf.trailingNaviItemBtn.hidden = true;
        [weakSelf updateNavigationBarAndStatusStyleForNavigationBarAlpha:1.0];
    }];
}

- (void)requestList {
//    [RequestUtils GET:URL_Server(@"user.medal") parameters:@{@"user_id":self.userId} success:^(id  _Nullable suceess) {
//        [self.medalArray removeAllObjects];
//        for (NSDictionary * dic in suceess[@"medal"]) {
//            MedalModel * model = [MedalModel initWithDictionary:dic];
//            [self.medalArray  addObject:model];
//        }
//       self.medalView.dataArray = self.medalArray;
//       self.medalView.hidden = !(self.medalArray.count > 0);
//       if (self.medalArray.count != 0) {
//           [self.albumView mas_remakeConstraints:^(MASConstraintMaker *make) {
//               make.top.equalTo(self.medalView.mas_bottom);
//               make.leading.trailing.mas_equalTo(self.containerView);
//           }];
//       }
//
//    } failure:^(id  _Nullable failure) {
//        [self showTips:failure[@"msg"]];
//        [self.albumView mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(self.guardListView.mas_bottom);
//            make.leading.trailing.mas_equalTo(self.containerView);
//        }];
//    }];
}

- (void)showNobleAnimation:(UIImage *)image {
    CAEmitterLayer *emitterLayer = [[CAEmitterLayer alloc] init];
    emitterLayer.emitterPosition = CGPointMake(kScreenW / 2, -17);
    emitterLayer.emitterShape = kCAEmitterLayerLine;
    emitterLayer.emitterSize = CGSizeMake(kScreenW, 0);
    emitterLayer.renderMode = kCAEmitterLayerSurface;
    for (CALayer *layer in self.view.layer.sublayers) {
        if ([layer isKindOfClass:[CAEmitterLayer class]]) {
            [layer removeFromSuperlayer];
        }
    }
    [self.view.layer addSublayer:emitterLayer];
    
    CAEmitterCell *emitterCell = [CAEmitterCell emitterCell];
    emitterCell.birthRate = 2.5;
    emitterCell.contents = (id)image.CGImage;
    emitterCell.velocity = 100.0;
    emitterCell.lifetime = 50;//粒子存活时间
    emitterCell.emissionLongitude = M_PI;
    emitterCell.scale = 0.3;//缩放比例
    emitterCell.scaleRange = 0.2;//缩放比例
    emitterCell.yAcceleration = 2;//y轴方向的加速度，落叶下飘只需要y轴正向加速度。
    emitterCell.spin = 1.0;//粒子旋转速度
    emitterCell.spinRange = 2.0;//粒子旋转速度范围
    emitterLayer.emitterCells = [NSArray arrayWithObject:emitterCell];
}

/// 关注该主播
- (void)liveFollowRequest {
    //[MobClick event:@"FM5_3_1"];
    NSDictionary *params = @{@"userId":self.userId};
    [LY_Network postRequestWithURL:LY_Api.live_follow parameters:params success:^(id  _Nullable response) {
        self.userInfo.isFollowLiver = true;
        [self.bottomView refresh];
        [LY_HUD showToastTips:@"关注成功".localized];
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

/// 取消关注该主播
- (void)liveUnfollowRequest {
    NSDictionary *params = @{@"userId":self.userId};
    [LY_Network postRequestWithURL:LY_Api.live_unfollow parameters:params success:^(id  _Nullable response) {
        self.userInfo.isFollowLiver = false;
        [self.bottomView refresh];
        [LY_HUD showToastTips:@"取消关注".localized];
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

/// 发送添加好友请求
- (void)addFriendRequest {
    //[MobClick event:@"FM5_3_9"];
    NSDictionary *params = @{@"to":self.userId,
                             @"type":@"request",
    };
    [LY_Network postRequestWithURL:LY_Api.friend parameters:params success:^(id  _Nullable response) {
        // 请求已经发出
        [LY_HUD showToastTips:@"已发送好友请求，请耐心等待".localized];
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

/// 删除好友
- (void)deleteFriendRequest {
    NSDictionary *params = @{@"to":self.userId,
                             @"type":@"delete",
    };
    [LY_Network postRequestWithURL:LY_Api.friend parameters:params success:^(id  _Nullable response) {
        self.userInfo.isFriend = false;
        [self.bottomView refresh];
        [LY_HUD showToastTips:@"删除好友成功".localized];
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

/// 添加黑名单
- (void)addBlackListRequest {
    NSDictionary *params = @{@"user_id":self.userId};
    [LY_Network postRequestWithURL:LY_Api.blackList_add parameters:params success:^(id  _Nullable response) {
        self.userInfo.isBlock = true;
        self.userInfo.isFriend = false;
        [self.bottomView refresh];
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

- (void)notInterNetTipButtonAction {
    [self setupData];
}

/// 移除黑名单
- (void)removeBlackListRequest {
    NSDictionary *params = @{@"user_id":self.userId};
    [LY_Network postRequestWithURL:LY_Api.blackList_remove parameters:params success:^(id  _Nullable response) {
        self.userInfo.isBlock = false;
        [self.bottomView refresh];
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

/// 右侧导航按钮点击事件
- (void)trailingNaviItemBtnAction {
    if (self.type == LY_HomePageTypeMine) {
        // 更换背景图片
        
    } else {
        // 更多功能
        //[MobClick event:@"FM5_3_10"];
        LYPSActionSheetView *actionSheetView = [[LYPSActionSheetView alloc] init];
        if (self.userInfo.isFriend) {
            [actionSheetView addAction:[LYPSystemAlertAction actionWithTitle:@"删除好友".localized style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
                [self deleteFriendRequest];
            }]];
        } else {
            [actionSheetView addAction:[LYPSystemAlertAction actionWithTitle:@"加为好友".localized style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
                [self addFriendRequest];
            }]];
        }
        if (self.userInfo.isAnchor) {
            if (self.userInfo.isFollowLiver) {
                [actionSheetView addAction:[LYPSystemAlertAction actionWithTitle:@"取消关注".localized style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
                    [self liveUnfollowRequest];
                }]];
            } else {
                [actionSheetView addAction:[LYPSystemAlertAction actionWithTitle:@"关注主播".localized style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
                    [self liveFollowRequest];
                }]];
            }
        }
        if (self.userInfo.isBlock) {
            [actionSheetView addAction:[LYPSystemAlertAction actionWithTitle:@"取消拉黑".localized style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
                [self removeBlackListRequest];
            }]];
        } else {
            [actionSheetView addAction:[LYPSystemAlertAction actionWithTitle:@"拉黑".localized style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
                [self addBlackListRequest];
            }]];
        }
        [actionSheetView addAction:[LYPSystemAlertAction actionWithTitle:@"举报".localized style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
            ReportViewController *reportVC = [[ReportViewController alloc] init];
            reportVC.userId = self.userId;
            [self.navigationController pushViewController:reportVC animated:true];
        }]];
        [actionSheetView present];
    }
}

// MARK: - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat navigationBarAlpha = scrollView.contentOffset.y / 100;
    
    [self updateNavigationBarAndStatusStyleForNavigationBarAlpha:navigationBarAlpha];
    
    if (scrollView.contentOffset.y > 210) {
        self.title = self.userInfo.nick_name;
    } else {
        self.title = nil;
    }
//    NSLog(@"scrollView.contentOffset.y:%f", scrollView.contentOffset.y);
}

- (void)updateNavigationBarAndStatusStyleForNavigationBarAlpha:(CGFloat)navigationBarAlpha {
    self.navigationBarAlpha = navigationBarAlpha;
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:navigationBarAlpha];
    if (navigationBarAlpha > 0.8) {
        [self.trailingNaviItemBtn setImage:[UIImage imageNamed:@"ic_gerenzhuye_gengduo_hei"] forState:UIControlStateNormal];
        [self updateBackBtnImage:[UIImage imageNamed:@"ic_tongyong_fanhui_hei"]];
        self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    } else {
        [self.trailingNaviItemBtn setImage:[UIImage imageNamed:@"ic_gerenzhuye_gengduo"] forState:UIControlStateNormal];
        [self updateBackBtnImage:[UIImage imageNamed:@"ic_arrow_left"]];
        self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    }
}

// MARK: - LY_HPBottomViewDelegate

- (void)bottomView:(LY_HPBottomView *)bottomView didClickChatBtn:(UIButton *)chatBtn {
    SPDConversationViewController *vc = [[SPDConversationViewController alloc] initWithConversationType:ConversationType_PRIVATE targetId:self.userId];
    [self.navigationController pushViewController:vc animated:YES];
    //[MobClick event:@"FM5_3_8"];
}

- (void)bottomView:(LY_HPBottomView *)bottomView didClickFriendsBtn:(UIButton *)friendsBtn {
    [self addFriendRequest];
}

- (void)bottomView:(LY_HPBottomView *)bottomView didClickVideoBtn:(UIButton *)videoBtn {
    if (self.userInfo.videoEnable) {
        [ZegoManager pauseAudio];
        [[RCCall sharedRCCall] startSingleCall:self.userId mediaType:RCCallMediaVideo];
        //[MobClick event:@"FM5_3_6"];
    }
}

- (void)bottomView:(LY_HPBottomView *)bottomView didClickAudioBtn:(UIButton *)audioBtn {
    if (self.userInfo.audioEnable) {
        [ZegoManager pauseAudio];
        [[RCCall sharedRCCall] startSingleCall:self.userId mediaType:RCCallMediaAudio];
        //[MobClick event:@"FM5_3_7"];
    }
}

- (void)bottomView:(LY_HPBottomView *)bottomView didClickFollowBtn:(UIButton *)followBtn {
    [self liveFollowRequest];
}

@end
