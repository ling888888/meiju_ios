//
//  LY_HPMedalView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/18.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_HPMedalView.h"
#import "LY_Button.h"
#import "LY_HPMedalCollectionCell.h"
#import "MedalLevelViewController.h"
#import "MedalListController.h"
#import "MedalModel.h"

@interface LY_HPMedalView ()<UICollectionViewDelegate , UICollectionViewDataSource>

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) LY_Button *moreBtn;
@property (nonatomic, strong) UICollectionView * collectionView;
@end

@implementation LY_HPMedalView

- (instancetype)initWithType:(LY_HomePageType)type {
    self = [super initWithType:type];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.titleLabel];
    [self addSubview:self.moreBtn];
    [self addSubview:self.collectionView];
}

- (void)setupUIFrame {
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(10);
    }];
    [self.moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-15);
        make.height.mas_equalTo(35);
        make.centerY.mas_equalTo(self.titleLabel);
    }];
    CGFloat width = (kScreenW - 6*15)/5.0f;
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.trailing.mas_equalTo(0);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(10);
        make.height.mas_equalTo(width);
        make.bottom.mas_equalTo(-10);
    }];
}

- (void)moreBtnAction {
    MedalListController * medalVC = [MedalListController new];
    medalVC.userId = self.userId;
    medalVC.name = self.userInfo.nick_name;
    [self.VC.navigationController pushViewController:medalVC animated:YES];
}

- (void)setDataArray:(NSMutableArray *)dataArray {
    _dataArray = dataArray;
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count >= 5 ? 5 : self.dataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LY_HPMedalCollectionCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LY_HPMedalCollectionCell" forIndexPath:indexPath];
    cell.medalModel = self.dataArray[indexPath.row];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    MedalModel * model = self.dataArray[indexPath.row];
    if (model.items.count == 0) {
        return;
    }
    MedalLevelViewController * vc = [MedalLevelViewController new];
    vc.userId = self.userInfo._id;
    vc.currentIndex = model.level;
    vc.medalArray = [model.items mutableCopy];
    [self.VC presentViewController:vc animated:YES completion:nil];
}

#pragma mark - getters

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _titleLabel.font = [UIFont mediumFontOfSize:15];
        _titleLabel.text = @"勋章墙".localized;
    }
    return _titleLabel;
}

- (LY_Button *)moreBtn {
    if (!_moreBtn) {
        _moreBtn = [[LY_Button alloc] initWithImageTitleSpace:5];
        _moreBtn.title = @"更多".localized;
        _moreBtn.image = [UIImage imageNamed:@"ic_zhibo_fensituan_gengduo"].adaptiveRtl;
        _moreBtn.titleColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _moreBtn.titleFont = [UIFont mediumFontOfSize:15];
        [_moreBtn addTarget:self action:@selector(moreBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _moreBtn;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout new];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumLineSpacing = 15;
        layout.minimumInteritemSpacing = 15;
        CGFloat width = (kScreenW - 6*15)/5.0f;
        layout.itemSize = CGSizeMake(width, width);
        layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 15);
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [_collectionView registerClass:[LY_HPMedalCollectionCell class] forCellWithReuseIdentifier:@"LY_HPMedalCollectionCell"];
    }
    return _collectionView;
}


@end
