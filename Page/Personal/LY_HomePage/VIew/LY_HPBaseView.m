//
//  LY_HPBaseView.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/9.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_HPBaseView.h"

@implementation LY_HPBaseView

- (instancetype)initWithType:(LY_HomePageType)type {
    self = [super init];
    if (self) {
        _type = type;
    }
    return self;
}

- (void)setUserInfo:(LY_HomePageInfo *)userInfo {
    _userInfo = userInfo;
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
