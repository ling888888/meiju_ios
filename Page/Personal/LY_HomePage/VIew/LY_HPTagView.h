//
//  LY_HPTagView.h
//  SimpleDate
//
//  Created by jianyue on 2020/5/8.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LY_CustomTag.h"
#import "LY_FansTagView.h"
#import "KKPaddingLabel.h"

NS_ASSUME_NONNULL_BEGIN

@class LY_CustomTag;

typedef enum : NSUInteger {
    LY_HPTagViewTypeMine, // 我的
    LY_HPTagViewTypeAnchor, // 主播
    LY_HPTagViewTypeAudience, // 观众
} LY_HPTagViewType;

@interface LY_HPTagView : UIView



// 性别
@property(nonatomic, copy) NSString *gender;

// 等级
@property(nonatomic, assign) NSInteger level;

// 主播
@property(nonatomic, assign) BOOL isAnchor;

// 贵族
@property(nonatomic, assign) BOOL isNoble;

// 贵族
@property(nonatomic, strong) NSString *nobleImgUrlStr;

// 土豪排行榜
@property(nonatomic, assign) NSInteger regalRank;

// 魅力排行榜
@property(nonatomic, assign) NSInteger charmRank;

// 会员
@property(nonatomic, assign) BOOL isVip;

// 靓号
@property(nonatomic, assign) BOOL isSpecialNum;

// 客服助手
@property(nonatomic, assign) BOOL isUserService;

// 自定义标签
@property(nonatomic, strong) LY_CustomTag *customTag;

// 粉丝标签
@property(nonatomic, strong) LY_FansTag *fansTag;

// 消费等级
@property (nonatomic, copy) NSString * wealthLevel;

// 标签类型
@property(nonatomic, assign) LY_HPTagViewType type;

// 直播间的管理员管理员
@property (nonatomic, assign) BOOL isAdmin;

- (instancetype)initWithType:(LY_HPTagViewType)type;

- (void)refresh;

@end




NS_ASSUME_NONNULL_END
