//
//  LY_HPFamilyView.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_HPFamilyView.h"
#import "ZegoKitManager.h"

@interface LY_HPFamilyView ()

@property(nonatomic, strong) UILabel *titleLabel;

@property(nonatomic, strong) UIButton *familyBtn;

@end

@implementation LY_HPFamilyView

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _titleLabel.font = [UIFont mediumFontOfSize:15];
        _titleLabel.text = @"家族".localized;
    }
    return _titleLabel;
}

- (UIButton *)familyBtn {
    if (!_familyBtn) {
        _familyBtn = [[UIButton alloc] init];
        UIImage *norBgImg = [UIImage imageWithUIColor:[[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.2]];
        UIImage *selBgImg = [UIImage imageWithUIColor:[UIColor colorWithHexString:@"#00D4A0"]];
        [_familyBtn setBackgroundImage:norBgImg forState:UIControlStateNormal];
        [_familyBtn setBackgroundImage:selBgImg forState:UIControlStateSelected];
        [_familyBtn setTitle:@"暂无家族".localized forState:UIControlStateNormal];
        _familyBtn.titleLabel.font = [UIFont mediumFontOfSize:14];
        _familyBtn.layer.cornerRadius = 15;
        _familyBtn.layer.masksToBounds = true;
        _familyBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 13, 0, 13);
        [_familyBtn addTarget:self action:@selector(familyBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _familyBtn;
}

- (instancetype)initWithType:(LY_HomePageType)type {
    self = [super initWithType:type];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
        [self setupDefaultData];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.titleLabel];
    [self addSubview:self.familyBtn];
}

- (void)setupUIFrame {
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(10);
    }];
    [self.familyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(9);
        make.height.mas_equalTo(30);
        make.bottom.mas_equalTo(-10);
    }];
}

- (void)setupDefaultData {
    self.familyBtn.selected = false;
}

- (void)setUserInfo:(LY_HomePageInfo *)userInfo {
    [super setUserInfo:userInfo];
    
    if (userInfo.clanId.notEmpty && !userInfo.isCountryClan) {
        self.familyBtn.selected = true;
        [self.familyBtn setTitle:userInfo.clanName forState:UIControlStateSelected];
        
        // 判断身份
        if ([userInfo.clanIdentity isEqualToString:@"chief"]) {
            if (kIsMirroredLayout) {
                self.familyBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -5);
                self.familyBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
            } else {
                self.familyBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
                self.familyBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -5);
            }
            [self.familyBtn setImage:[UIImage imageNamed:@"ic_gerenzhuye_jiazuzuzhang"] forState:UIControlStateSelected];
        } else if ([userInfo.clanIdentity isEqualToString:@"deputychief"]) {
            if (kIsMirroredLayout) {
                self.familyBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -5);
                self.familyBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
            } else {
                self.familyBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
                self.familyBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -5);
            }
            [self.familyBtn setImage:[UIImage imageNamed:@"ic_gerenzhuye_jiazufuzuzhang"] forState:UIControlStateSelected];
        } else {
            self.familyBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            self.familyBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            [self.familyBtn setImage:nil forState:UIControlStateSelected];
        }
    } else {
        self.familyBtn.selected = false;
    }
}

-(void)familyBtnAction {
    if (self.type == LY_HomePageTypeMine) {
        //[MobClick event:@"FM5_2_5"];
    } else {
        //[MobClick event:@"FM5_3_11"];
    }
    
    // 判断语言区
    if ([[SPDApiUser currentUser].lang isEqualToString:self.userInfo.lang]) {
        // 进入家族
        [ZegoManager joinRoomFrom:self.VC clanId:self.userInfo.clanId];
    } else {
        [LY_HUD showToastTips:@"对方和你不在同一语言区，不能跨区进家族".localized];
    }
}

@end
