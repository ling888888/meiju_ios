//
//  LY_HPTagView.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/8.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_HPTagView.h"
#import "SVGAImageView.h"

#import "LiveRoomWealthLevelView.h"

@interface LY_HPTagView ()


@property(nonatomic, copy) NSString *gender_private;
@property(nonatomic, assign) NSInteger level_private;
@property(nonatomic, assign) BOOL isAnchor_private;
@property(nonatomic, assign) BOOL isNoble_private;
@property(nonatomic, strong) NSString *nobleImgUrlStr_private;
@property(nonatomic, assign) NSInteger regalRank_private;
@property(nonatomic, assign) NSInteger charmRank_private;
@property(nonatomic, assign) BOOL isVip_private;
@property(nonatomic, assign) BOOL isSpecialNum_private;
@property(nonatomic, assign) BOOL isUserService_private;
@property(nonatomic, strong) LY_CustomTag *customTag_private;
@property(nonatomic, strong) LY_FansTag *fansTag_private;

@property(nonatomic, copy) NSString *wealthLevel_private;

@property(nonatomic, assign) BOOL isNeedRefresh;


// 性别
@property(nonatomic, strong) UIButton *genderBtn;

// 等级
@property(nonatomic, strong) UIButton *levelBtn;

// 主播
@property(nonatomic, strong) KKPaddingLabel *anchorLabel;

// 管理员
@property(nonatomic, strong) KKPaddingLabel *managerLabel;

// 贵族
@property(nonatomic, strong) UIImageView *nobleImgView;

// 土豪排行榜
@property(nonatomic, strong) UIImageView *regalImgView;

// 魅力排行榜
@property(nonatomic, strong) UIImageView *charmImgView;

// 会员
@property(nonatomic, strong) SVGAImageView *vipImgView;

// 靓号
@property(nonatomic, strong) UIImageView *specialNumImgView;

// 客服助手
@property(nonatomic, strong) UIImageView *userServiceImgView;

// 自定义标签
@property(nonatomic, strong) UIImageView *customTagView;

// 粉丝标签
@property(nonatomic, strong) LY_FansTagView *fansTagView;

// 消费等级标签
@property(nonatomic, strong) LiveRoomWealthLevelView *wealthLevelView;

@property(nonatomic, strong) UIView *trailingView;

@end



@implementation LY_HPTagView

- (UIView *)trailingView {
    if (!_trailingView) {
        _trailingView = [[UIView alloc] init];
        _trailingView.backgroundColor = [UIColor clearColor];
    }
    return _trailingView;
}

- (UIButton *)genderBtn {
    if (!_genderBtn) {
        _genderBtn = [[UIButton alloc] init];
        _genderBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _genderBtn;
}

- (UIButton *)levelBtn {
    if (!_levelBtn) {
        _levelBtn = [[UIButton alloc] initWithFrame:CGRectMake(17, 0.5, 27.5, 11)];
        [_levelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _levelBtn.titleLabel.font = [UIFont mediumFontOfSize:10];
    }
    return _levelBtn;
}

- (KKPaddingLabel *)anchorLabel {
    if (!_anchorLabel) {
        _anchorLabel = [[KKPaddingLabel alloc] init];
        _anchorLabel.textColor = [UIColor whiteColor];
        _anchorLabel.text = @"主播".localized;
        _anchorLabel.textAlignment = NSTextAlignmentCenter;
        _anchorLabel.font = [UIFont systemFontOfSize:9];
        _anchorLabel.backgroundColor = [UIColor colorWithHexString:@"#6A2CF7"];
        _anchorLabel.padding = UIEdgeInsetsMake(0, 5, 0, 5);
        _anchorLabel.layer.masksToBounds = true;
    }
    return _anchorLabel;
}

- (KKPaddingLabel *)managerLabel {
    if (!_managerLabel) {
        _managerLabel = [[KKPaddingLabel alloc] init];
        _managerLabel.textColor = [UIColor whiteColor];
        _managerLabel.text = @"管理员".localized;
        _managerLabel.textAlignment = NSTextAlignmentCenter;
        _managerLabel.font = [UIFont systemFontOfSize:9];
        _managerLabel.backgroundColor = [UIColor colorWithHexString:COMMON_PINK];
        _managerLabel.padding = UIEdgeInsetsMake(0, 5, 0, 5);
        _managerLabel.layer.masksToBounds = true;
    }
    return _managerLabel;
}

- (UIImageView *)nobleImgView {
    if (!_nobleImgView) {
        _nobleImgView = [[UIImageView alloc] init];
        _nobleImgView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _nobleImgView;
}

- (UIImageView *)regalImgView {
    if (!_regalImgView) {
        _regalImgView = [[UIImageView alloc] init];
        
    }
    return _regalImgView;
}

- (UIImageView *)charmImgView {
    if (!_charmImgView) {
        _charmImgView = [[UIImageView alloc] init];
        
    }
    return _charmImgView;
}

- (SVGAImageView *)vipImgView {
    if (!_vipImgView) {
        _vipImgView = [[SVGAImageView alloc] init];
        _vipImgView.contentMode = UIViewContentModeScaleAspectFit;
        _vipImgView.autoPlay = true;
        _vipImgView.imageName = @"vip_member";
//        _svImgView.delegate = self;
    }
    return _vipImgView;
}

- (UIImageView *)specialNumImgView {
    if (!_specialNumImgView) {
        _specialNumImgView = [[UIImageView alloc] init];
        _specialNumImgView.contentMode = UIViewContentModeScaleAspectFit;
        _specialNumImgView.image = [UIImage imageNamed:@"specialNum_icon_13"];
    }
    return _specialNumImgView;
}

- (UIImageView *)userServiceImgView {
    if (!_userServiceImgView) {
        _userServiceImgView = [[UIImageView alloc] init];
        _userServiceImgView.contentMode = UIViewContentModeScaleAspectFill;
        _userServiceImgView.image = [UIImage imageNamed:@"ic_kefuzhushou"];
    }
    return _userServiceImgView;
}

- (UIImageView *)customTagView {
    if (!_customTagView) {
        _customTagView = [[UIImageView alloc] init];
        _customTagView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _customTagView;
}

- (LY_FansTagView *)fansTagView {
    if (!_fansTagView) {
        _fansTagView = [[LY_FansTagView alloc] init];
    }
    return _fansTagView;
}

- (LiveRoomWealthLevelView *)wealthLevelView {
    if (!_wealthLevelView) {
        _wealthLevelView = [[LiveRoomWealthLevelView alloc] init];
    }
    return _wealthLevelView;
}

- (instancetype)initWithType:(LY_HPTagViewType)type
{
    self = [super initWithFrame:CGRectZero];
    if (self) {
        self.type = type;
        
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.type = LY_HPTagViewTypeMine;
        
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
//    self.anchorLabel.cornerRadius = rect.size.height / 2;
//    self.genderBtn.layer.cornerRadius = rect.size.height / 2;
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.anchorLabel.cornerRadius = self.frame.size.height / 2;
    self.managerLabel.cornerRadius = self.frame.size.height / 2;
    self.genderBtn.layer.cornerRadius = self.frame.size.height / 2;
}

- (void)setupUI {
    self.regalRank_private = -1;
    self.charmRank_private = -1;
    self.isNeedRefresh = true;
    
    // 判断tag类型
    if (self.type == LY_HPTagViewTypeMine) {
        // 我的样式
        
    }
}

- (void)setupUIFrame {
    // 判断tag类型
    if (self.type == LY_HPTagViewTypeMine) {
        // 我的样式
    }
}

- (void)setGender:(NSString *)gender {
    _gender = gender;
    
    // 判断性别男、女
    if ([gender isEqualToString:@"male"]) {
        self.genderBtn.backgroundColor = [UIColor colorWithHexString:@"3c91f1"];
        [self.genderBtn setImage:[UIImage imageNamed:@"chatroomuser_male"] forState:UIControlStateNormal];
    } else {
        self.genderBtn.backgroundColor = [UIColor colorWithHexString:@"fe69a1"];
        [self.genderBtn setImage:[UIImage imageNamed:@"chatroomuser_female"] forState:UIControlStateNormal];
    }
}

- (void)setLevel:(NSInteger)level {
    _level = level;
    
    [SPDCommonTool configDataForLevelBtnWithLevel:(int)level andUIButton:self.levelBtn];
}

- (void)setIsAnchor:(BOOL)isAnchor {
    _isAnchor = isAnchor;
    if (_anchorLabel) {
        self.anchorLabel.hidden = !isAnchor;
    }
    if (self.isAnchor != self.isAnchor_private) {
        self.isAnchor_private = self.isAnchor;
        self.isNeedRefresh = true;
    }
}

- (void)setIsAdmin:(BOOL)isAdmin {
    if (_isAdmin != isAdmin) {
        self.isNeedRefresh = true;
    }
    _isAdmin = isAdmin;
    _managerLabel.hidden = !_isAdmin;
}

- (void)setIsNoble:(BOOL)isNoble {
    _isNoble = isNoble;
    if (_nobleImgView) {
        self.nobleImgView.hidden = !isNoble;
    }
    if (self.isNoble != self.isNoble_private) {
        self.isNoble_private = self.isNoble;
        self.isNeedRefresh = true;
    }
}

- (void)setNobleImgUrlStr:(NSString *)nobleImgUrlStr {
    _nobleImgUrlStr = nobleImgUrlStr;
    if (nobleImgUrlStr.notEmpty) {
//        [self.nobleImgView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:nobleImgUrlStr]]];
        [self.nobleImgView fp_setImageWithURLString:nobleImgUrlStr];
    }
}

- (void)setRegalRank:(NSInteger)regalRank {
    _regalRank = regalRank;
    if (_regalImgView) {
        self.regalImgView.hidden = regalRank == -1;
    }
    if (self.regalRank != self.regalRank_private) {
        self.regalRank_private = self.regalRank;
        self.isNeedRefresh = true;
    }
    if (self.regalRank < 10 && self.regalRank >= 0) {
        if (self.regalRank == 0) {
            // 第一名
            self.regalImgView.image = [UIImage imageNamed:@"rich_first_tag"];
        } else if (self.regalRank == 1) {
            // 第二名
            self.regalImgView.image = [UIImage imageNamed:@"rich_second_tag"];
        } else if (self.regalRank == 2) {
         // 第三名
         self.regalImgView.image = [UIImage imageNamed:@"rich_third_tag"];
        } else {
         // 4 - 9名
         self.regalImgView.image = [UIImage imageNamed:@"rich_fourth_tag"];
        }
    }
}

- (void)setCharmRank:(NSInteger)charmRank {
    _charmRank = charmRank;
    if (_charmImgView) {
        self.charmImgView.hidden = charmRank == -1;
    }
    if (self.charmRank != self.charmRank_private) {
        self.charmRank_private = self.charmRank;
        self.isNeedRefresh = true;
    }
    if (self.charmRank < 10 && self.charmRank >= 0) {
        if (self.charmRank == 0) {
            // 第一名
            self.charmImgView.image = [UIImage imageNamed:@"charm_first_tag"];
        } else if (self.charmRank == 1) {
            // 第二名
            self.charmImgView.image = [UIImage imageNamed:@"charm_second_tag"];
        } else if (self.charmRank == 2) {
            // 第三名
            self.charmImgView.image = [UIImage imageNamed:@"charm_third_tag"];
        } else {
            // 4 - 9名
            self.charmImgView.image = [UIImage imageNamed:@"charm_fourth_tag"];
        }
    }
}

- (void)setIsVip:(BOOL)isVip {
    _isVip = isVip;
    if (_vipImgView) {
        self.vipImgView.hidden = !isVip;
    }
    if (self.isVip != self.isVip_private) {
        self.isVip_private = self.isVip;
        self.isNeedRefresh = true;
    }
}

- (void)setIsSpecialNum:(BOOL)isSpecialNum {
    _isSpecialNum = isSpecialNum;
    if (_specialNumImgView) {
        self.specialNumImgView.hidden = !isSpecialNum;
    }
    if (self.isSpecialNum != self.isSpecialNum_private) {
        self.isSpecialNum_private = self.isSpecialNum;
        self.isNeedRefresh = true;
    }
}

- (void)setIsUserService:(BOOL)isUserService {
    _isUserService = isUserService;
    if (_userServiceImgView) {
        self.userServiceImgView.hidden = !isUserService;
    }
    if (self.isUserService != self.isUserService_private) {
        self.isUserService_private = self.isUserService;
        self.isNeedRefresh = true;
    }
}

- (void)setCustomTag:(LY_CustomTag *)customTag {
    _customTag = customTag;
    if (_customTagView) {
        self.customTagView.hidden = !customTag.url.notEmpty;
    }
    if (self.customTag != self.customTag_private) {
        self.customTag_private = self.customTag;
        self.isNeedRefresh = true;
//        [self.customTagView sd_setImageWithURL:[NSURL URLWithString:self.customTag.url]];
//        [self.customTagView sd_setImageWithURL:[NSURL URLWithString:self.customTag.url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//
//            customTagView
//        }];
//        [self.customTagView sd_setimagewithurplac]
    }
}

- (void)setFansTag:(LY_FansTag *)fansTag {
    _fansTag = fansTag;
    if (_fansTagView) {
        self.fansTagView.hidden = !fansTag.name.notEmpty;
    }
    if (self.fansTag != self.fansTag_private) {
        self.fansTag_private = self.fansTag;
        self.isNeedRefresh = true;
        self.fansTagView.fansTag = fansTag;
    }
}

- (void)setWealthLevel:(NSString *)wealthLevel {
    _wealthLevel = wealthLevel;
    if (_wealthLevelView) {
        self.wealthLevelView.hidden = !wealthLevel.notEmpty;
    }
    if (wealthLevel != self.wealthLevel_private) {
        self.wealthLevel_private = wealthLevel;
        self.isNeedRefresh = true;
        self.wealthLevelView.wealthLevel = wealthLevel;
    }
}

- (void)refresh {
    if (self.isNeedRefresh == false) {
        return;
    } else {
        self.isNeedRefresh = false;
    }
    // 判断tag类型
    if (self.type == LY_HPTagViewTypeMine) {
        [self mineTagViewStylt];
//    } else if (self.type == LY_HPTagViewTypeAnchor) {
//        [self anchorTagViewStyle];
    } else {
        [self audienceTagViewStyle];
    }
}

/// 获取最后一个有效的视图
- (UIView *)getSelfLastValidViewForView:(UIView *)view {
    for (NSUInteger i = [self.subviews indexOfObject:view]; i > 0; i --) {
//        NSLog(@"index:i=%d", i);
//        NSLog(@"self.subviews.count=%d", self.subviews.count);
        UIView *view = self.subviews[i-1];
        if (view.hidden == false) {
            if (view == self.trailingView) {
                return nil;
            } else {
                return view;
            }
            break;
        }
    }
    return nil;
}


- (void)mineTagViewStylt {
//    if (self.genderBtn.superview == nil) {
        [self addSubview:self.genderBtn];
//    }
    [self.genderBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(0);
        make.top.bottom.mas_equalTo(self);
        make.width.mas_equalTo(self.genderBtn.mas_height);
    }];
    
    if (self.customTag.url.notEmpty) {
        self.customTagView.hidden = false;
//        if (self.customTagView.superview == nil) {
            [self addSubview:self.customTagView];
//        }
//        [self.customTagView mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.leading.mas_equalTo([self getSelfLastValidViewForView:self.customTagView].mas_trailing).offset(5);
//            make.top.bottom.mas_equalTo(self);
//            make.width.mas_equalTo(self.customTagView.mas_height).multipliedBy(1.0);
//        }]; // 不知道为什么 像观众那么改就不对
        __weak typeof(self) weakSelf = self;
        [self.customTagView sd_setImageWithURL:[NSURL URLWithString:self.customTag.url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (image) {
                [weakSelf.customTagView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.width.mas_equalTo(weakSelf.customTagView.mas_height).multipliedBy(image.size.width/image.size.height);
                    make.leading.mas_equalTo([weakSelf getSelfLastValidViewForView:weakSelf.customTagView].mas_trailing).offset(5);
                    make.top.bottom.mas_equalTo(weakSelf);
                }];
            }
        }];
    } else {
        self.customTagView.hidden = true;
    }
    
//    if (self.levelBtn.superview == nil) {
        [self addSubview:self.levelBtn];
//    }
    [self.levelBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo([self getSelfLastValidViewForView:self.levelBtn].mas_trailing).offset(5);
        make.top.bottom.mas_equalTo(self);
        make.width.mas_equalTo(self.genderBtn.mas_height).multipliedBy(27.5/11);
    }];
    
    // 判断是否为主播
    if (self.isAnchor) {
//        if (self.anchorLabel.superview == nil) {
            [self addSubview:self.anchorLabel];
//        }
        [self.anchorLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo([self getSelfLastValidViewForView:self.anchorLabel].mas_trailing).offset(5);
            make.top.bottom.mas_equalTo(self);
        }];
    } else {
        
    }

    // 判断是否为贵族
    if (self.nobleImgUrlStr.notEmpty) {
//        if (self.nobleImgView.superview == nil) {
            [self addSubview:self.nobleImgView];
//        }
        [self.nobleImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo([self getSelfLastValidViewForView:self.nobleImgView].mas_trailing).offset(5);
            make.top.bottom.mas_equalTo(self);
            make.width.mas_equalTo(self.nobleImgView.mas_height).multipliedBy(16/12);
        }];
    }
    
    if (self.regalRank < 10 && self.regalRank >= 0) {
//        if (self.regalImgView.superview == nil) {
            [self addSubview:self.regalImgView];
//        }
        [self.regalImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo([self getSelfLastValidViewForView:self.regalImgView].mas_trailing).offset(5);
            make.top.bottom.mas_equalTo(self);
            make.width.mas_equalTo(self.regalImgView.mas_height).multipliedBy(30.6/12);
        }];
     }

    if (self.charmRank < 10 && self.charmRank >= 0) {
//        if (self.charmImgView.superview == nil) {
            [self addSubview:self.charmImgView];
//        }
        [self.charmImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo([self getSelfLastValidViewForView:self.charmImgView].mas_trailing).offset(5);
            make.top.bottom.mas_equalTo(self);
            make.width.mas_equalTo(self.charmImgView.mas_height).multipliedBy(30.6/12);
        }];
    }

    // 会员
    if (self.isVip) {
//        if (self.vipImgView.superview == nil) {
            [self addSubview:self.vipImgView];
//        }
        [self.vipImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo([self getSelfLastValidViewForView:self.vipImgView].mas_trailing).offset(5);
            make.top.bottom.mas_equalTo(self);
            make.width.mas_equalTo(self.vipImgView.mas_height).multipliedBy(15/12);
        }];
    }

    // 靓号
    if (self.isSpecialNum) {
//        if (self.specialNumImgView.superview == nil) {
            [self addSubview:self.specialNumImgView];
//        }
        [self.specialNumImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo([self getSelfLastValidViewForView:self.specialNumImgView].mas_trailing).offset(5);
            make.top.bottom.mas_equalTo(self);
            make.width.mas_equalTo(self.specialNumImgView.mas_height).multipliedBy(15/12);
        }];
    }

    // 客服助手
    if (self.isUserService) {
//        if (self.userServiceImgView.superview == nil) {
            [self addSubview:self.userServiceImgView];
//        }
        [self.userServiceImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo([self getSelfLastValidViewForView:self.userServiceImgView].mas_trailing).offset(5);
            make.top.bottom.mas_equalTo(self);
            make.width.mas_equalTo(self.userServiceImgView.mas_height);
            make.trailing.mas_lessThanOrEqualTo(0);
        }];
    }
}
//
//// 主播标签 - 消费等级
//- (void)anchorTagViewStyle {
//    if (self.customTag.url.notEmpty) {
//        if (self.customTagView.superview == nil) {
//            [self addSubview:self.customTagView];
//        }
//        [self.customTagView mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.leading.mas_equalTo(0);
//            make.top.bottom.mas_equalTo(self);
//            make.width.mas_equalTo(self.customTagView.mas_height);
//        }];
//        __weak typeof(self) weakSelf = self;
//        [self.customTagView sd_setImageWithURL:[NSURL URLWithString:self.customTag.url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//            if (image) {
//                [weakSelf.customTagView mas_updateConstraints:^(MASConstraintMaker *make) {
//                    make.width.mas_equalTo(weakSelf.customTagView.mas_height).multipliedBy(image.size.width/image.size.height);
//                }];
//            }
//        }];
//    }
//
//    // 判断是否为贵族
//    if (self.isNoble) {
//        if (self.nobleImgView.superview == nil) {
//            [self addSubview:self.nobleImgView];
//        }
//        UIView *lastView = [self getSelfLastValidViewForView:self.nobleImgView];
//        [self.nobleImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
//            if (lastView) {
//                make.leading.mas_equalTo(lastView.mas_trailing).offset(5);
//            } else {
//                make.leading.mas_equalTo(0);
//            }
//            make.top.bottom.mas_equalTo(self);
//            make.width.mas_equalTo(self.nobleImgView.mas_height).multipliedBy(16/12);
//        }];
//    }
//
//
//
//    if (self.wealthLevel.notEmpty && ![self.wealthLevel isEqualToString:@"0"]) {
//        if (self.wealthLevelView.superview == nil) {
//            [self addSubview:self.wealthLevelView];
//        }
//        [self.wealthLevelView mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.leading.mas_equalTo([self getSelfLastValidViewForView:self.wealthLevelView].mas_trailing).offset(5);
//            make.top.bottom.mas_equalTo(self);
//            make.trailing.mas_lessThanOrEqualTo(0);
//        }];
//    }
//}

// 观众标签
- (void)audienceTagViewStyle {
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    
    if (self.customTag.url.notEmpty) {
        [self addSubview:self.customTagView];
        self.customTagView.hidden = false;
        [self.customTagView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo(0);
            make.centerY.mas_equalTo(0);
            make.height.mas_equalTo([self.customTag.labelType isEqualToString:@"1"] ? 15: 14);
        }];
        __weak typeof(self) weakSelf = self;
        [self.customTagView sd_setImageWithURL:[NSURL URLWithString:self.customTag.url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (image) {
                [weakSelf.customTagView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.width.mas_equalTo(weakSelf.customTagView.mas_height).multipliedBy(image.size.width/image.size.height);
                }];
            }
        }];
    } else {
        self.customTagView.hidden = true;
    }
    
    // 判断是否为贵族
    if (self.nobleImgUrlStr.notEmpty) {
        [self addSubview:self.nobleImgView];
        self.nobleImgView.hidden = false;
        UIView *lastView = [self getSelfLastValidViewForView:self.nobleImgView];
        [self.nobleImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            if (lastView) {
                make.leading.mas_equalTo(lastView.mas_trailing).offset(5);
            } else {
                make.leading.mas_equalTo(0);
            }
//            make.top.bottom.mas_equalTo(self);
            make.height.mas_equalTo(15);
            make.centerY.mas_equalTo(0);
            make.width.mas_equalTo(self.nobleImgView.mas_height).multipliedBy(16/15);
        }];
    } else {
        self.nobleImgView.hidden = true;
    }
    
    // 判断是否为主播
    if (self.isAnchor) {
        [self addSubview:self.anchorLabel];
        UIView *lastView = [self getSelfLastValidViewForView:self.anchorLabel];
        [self.anchorLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            if (lastView) {
                make.leading.mas_equalTo(lastView.mas_trailing).offset(5);
            } else {
                make.leading.mas_equalTo(0);
            }
            make.top.bottom.mas_equalTo(self);
        }];
    } else {
        // 判断是否为粉丝
        if (self.fansTag.level.notEmpty) {
            [self addSubview:self.fansTagView];
            self.fansTagView.hidden = false;
            UIView *lastView = [self getSelfLastValidViewForView:self.fansTagView];
            [self.fansTagView mas_remakeConstraints:^(MASConstraintMaker *make) {
                if (lastView) {
                    make.leading.mas_equalTo(lastView.mas_trailing).offset(5);
                } else {
                    make.leading.mas_equalTo(0);
                }
                make.top.bottom.mas_equalTo(self);
            }];
        } else {
            self.fansTagView.hidden = true;
        }
    }
    
    if (self.wealthLevel.notEmpty && ![self.wealthLevel isEqualToString:@"0"]) {
        [self addSubview:self.wealthLevelView];
        self.wealthLevelView.hidden = false;
        UIView *lastView = [self getSelfLastValidViewForView:self.wealthLevelView];
        [self.wealthLevelView mas_remakeConstraints:^(MASConstraintMaker *make) {
            if (lastView) {
                make.leading.mas_equalTo(lastView.mas_trailing).offset(5);
            } else {
                make.leading.mas_equalTo(0);
            }
            make.top.bottom.mas_equalTo(self);
        }];
    } else {
        self.wealthLevelView.hidden = true;
    }
    
    // 是不是直播间管理员管理员
    if (self.isAdmin) {
        [self addSubview:self.managerLabel];
        self.managerLabel.hidden = NO;
        UIView *lastView = [self getSelfLastValidViewForView:self.managerLabel];
        [self.managerLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            if (lastView) {
                make.leading.mas_equalTo(lastView.mas_trailing).offset(5);
            } else {
                make.leading.mas_equalTo(0);
            }
            make.top.bottom.mas_equalTo(self);
        }];
    }else{
        self.managerLabel.hidden = YES;
    }
    
    
    [self addSubview:self.trailingView];
    UIView *lastView = [self getSelfLastValidViewForView:self.trailingView];
    [self.trailingView mas_remakeConstraints:^(MASConstraintMaker *make) {
        if (lastView) {
            make.leading.mas_equalTo(lastView.mas_trailing).offset(5);
        } else {
            make.leading.mas_equalTo(0);
        }
        make.top.bottom.mas_equalTo(self);
        make.trailing.mas_lessThanOrEqualTo(0);
        make.width.mas_equalTo(1);
    }];
//    self.trailingView.backgroundColor = [UIColor redColor];
}

@end
