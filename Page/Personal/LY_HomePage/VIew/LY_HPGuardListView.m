//
//  LY_HPGuardListView.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_HPGuardListView.h"
#import "LY_HomePageViewController.h"

@interface LY_HPGuardListView () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>;

@property(nonatomic, strong) UILabel *titleLabel;

@property(nonatomic, strong) UIButton *whyBtn;

@property(nonatomic, strong) UICollectionView *collectionView;

@end

@implementation LY_HPGuardListView

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _titleLabel.font = [UIFont mediumFontOfSize:15];
        _titleLabel.text = @"守护榜".localized;
    }
    return _titleLabel;
}

- (UIButton *)whyBtn {
    if (!_whyBtn) {
        _whyBtn = [[UIButton alloc] init];
        [_whyBtn setImage:[UIImage imageNamed:@"ic_wode_qiandao_shuoming"] forState:UIControlStateNormal];
        [_whyBtn addTarget:self action:@selector(whyBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _whyBtn;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [_collectionView registerClass:[LY_HPGuardCollectionViewCell class] forCellWithReuseIdentifier:@"HPGuardIdentifier"];
    }
    return _collectionView;
}

- (instancetype)initWithType:(LY_HomePageType)type {
    self = [super initWithType:type];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
        [self setupDefaultData];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.titleLabel];
    [self addSubview:self.whyBtn];
    [self addSubview:self.collectionView];
}

- (void)setupUIFrame {
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(10);
        make.bottom.mas_equalTo(-90);
    }];
    [self.whyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(35);
        make.leading.mas_equalTo(self.titleLabel.mas_trailing).offset(0);
        make.centerY.mas_equalTo(self.titleLabel);
    }];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(5);
        make.leading.trailing.mas_equalTo(self);
        make.height.mas_equalTo(85);
    }];
}

- (void)setupDefaultData {
    
}

- (void)setUserInfo:(LY_HomePageInfo *)userInfo {
    [super setUserInfo:userInfo];
    NSLog(@"patronsList:%@", userInfo.patronsList);
    [self.collectionView reloadData];
}

- (void)whyBtnAction {
    LYPSTextAlertView *view = [[LYPSTextAlertView alloc] init];
    view.title = @"提示".localized;
    view.message = self.userInfo.patronDesc;
    [view addAction:[LYPSystemAlertAction actionWithTitle:@"确定".localized style:LYPSystemAlertActionStyleCancel handler:nil]];
    [view present];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 4;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LY_HPGuardCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HPGuardIdentifier" forIndexPath:indexPath];
    cell.index = indexPath.row;
    if (indexPath.row < self.userInfo.patronsList.count) {
        cell.patrons = self.userInfo.patronsList[indexPath.row];
    } else {
        cell.patrons = nil;
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.userInfo.patronsList) {
        if (indexPath.row < self.userInfo.patronsList.count) {
            LY_Patrons *patrons = self.userInfo.patronsList[indexPath.row];
            LY_HomePageViewController *homePageVC = [[LY_HomePageViewController alloc] initWithUserId:patrons._id];
            [self.VC.navigationController pushViewController:homePageVC animated:true];
        }
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(81.5, 75);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return (kScreenW - 81.5 * 4) / 5;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, (kScreenW - 81.5 * 4) / 5, 0, (kScreenW - 81.5 * 4) / 5);
}

@end

#import "SVGA.h"

@interface LY_HPGuardCollectionViewCell ()

@property(nonatomic, strong) LY_PortraitView *portraitView;

//@property(nonatomic, strong) UIImageView *headwearImageView;

//@property(nonatomic, strong) SVGAImageView *headwearSVGAView;

@property(nonatomic, strong) UIImageView *rankView;

@end

@implementation LY_HPGuardCollectionViewCell

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
    }
    return _portraitView;
}

//- (UIImageView *)headwearImageView {
//    if (!_headwearImageView) {
//        _headwearImageView = [[UIImageView alloc] init];
//        _headwearImageView.contentMode = UIViewContentModeScaleAspectFill;
//    }
//    return _headwearImageView;
//}

//- (SVGAImageView *)headwearSVGAView {
//    if (!_headwearSVGAView) {
//        _headwearSVGAView = [[SVGAImageView alloc] init];
//        _headwearSVGAView.contentMode = UIViewContentModeScaleAspectFill;
//    }
//    return _headwearSVGAView;
//}

- (UIImageView *)rankView {
    if (!_rankView) {
        _rankView = [[UIImageView alloc] init];
    }
    return _rankView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
        [self setupDefaultData];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.portraitView];
    [self addSubview:self.rankView];
}

- (void)setupUIFrame {
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
    [self.rankView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(44, 30));
        make.bottom.mas_equalTo(0);
        make.centerX.mas_equalTo(0);
    }];
}

- (void)setupDefaultData {
    self.portraitView.imageView.image = [UIImage imageNamed:@"img_gerenzhuye_shouhu_moren"];
}

- (void)setPatrons:(nullable LY_Patrons *)patrons {
    _patrons = patrons;
   
    if (_patrons) {
        self.portraitView.portrait = patrons.portrait;
//        self.portraitView.headwearHidden = false;
    } else {
        self.portraitView.imageView.image = [UIImage imageNamed:@"img_gerenzhuye_shouhu_moren"];
//        self.portraitView.headwearHidden = true;
    }
}

- (void)setIndex:(NSInteger)index {
    _index = index;
    
    NSString *imgName = [NSString stringWithFormat:@"ic_gerenzhuye_shouhu%ld", index + 1];
    self.rankView.image = [UIImage imageNamed:imgName];
    
    if (index == 0) {
        self.portraitView.layer.borderColor = [[UIColor colorWithHexString:@"#FFD563"] CGColor];
    } else if (index == 1) {
        self.portraitView.layer.borderColor = [[UIColor colorWithHexString:@"#D6DFEB"] CGColor];
    } else if (index == 2) {
        self.portraitView.layer.borderColor = [[UIColor colorWithHexString:@"#FFCAAD"] CGColor];
    } else {
        self.portraitView.layer.borderColor = [[UIColor colorWithHexString:@"#CDCDCD"] CGColor];
    }
}

@end
