//
//  LY_HPBottomView.h
//  SimpleDate
//
//  Created by jianyue on 2020/5/11.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_HPBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@class LY_HPBottomView;

@protocol LY_HPBottomViewDelegate <NSObject>

- (void)bottomView:(LY_HPBottomView *)bottomView didClickChatBtn:(UIButton *)chatBtn;

- (void)bottomView:(LY_HPBottomView *)bottomView didClickFriendsBtn:(UIButton *)friendsBtn;

- (void)bottomView:(LY_HPBottomView *)bottomView didClickVideoBtn:(UIButton *)videoBtn;

- (void)bottomView:(LY_HPBottomView *)bottomView didClickAudioBtn:(UIButton *)audioBtn;

- (void)bottomView:(LY_HPBottomView *)bottomView didClickFollowBtn:(UIButton *)followBtn;

@end

@interface LY_HPBottomView : LY_HPBaseView

@property(nonatomic, strong) id<LY_HPBottomViewDelegate> delegate;

- (void)refresh;

@end

NS_ASSUME_NONNULL_END
