//
//  LY_HPSocialView.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_HPSocialView.h"
#import "LY_Button.h"
#import "ChatroomWealthController.h"

@interface LY_HPSocialView ()

@property(nonatomic, strong) UILabel *titleLabel;

@property(nonatomic, strong) LY_Button *moreBtn;

@property(nonatomic, strong) UIImageView *acceptGiftBgView;

@property(nonatomic, strong) UIImageView *acceptGiftIconView;

@property(nonatomic, strong) UILabel *acceptGiftTitleLabel;

@property(nonatomic, strong) UILabel *acceptGiftNumberLabel;

@property(nonatomic, strong) UILabel *acceptGiftDescLabel;


@property(nonatomic, strong) UIImageView *giveGiftBgView;

@property(nonatomic, strong) UIImageView *giveGiftIconView;

@property(nonatomic, strong) UILabel *giveGiftTitleLabel;

@property(nonatomic, strong) UILabel *giveGiftNumberLabel;

@property(nonatomic, strong) UILabel *giveGiftDescLabel;

@end

@implementation LY_HPSocialView

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _titleLabel.font = [UIFont mediumFontOfSize:15];
        _titleLabel.text = @"交际".localized;
    }
    return _titleLabel;
}

- (LY_Button *)moreBtn {
    if (!_moreBtn) {
        _moreBtn = [[LY_Button alloc] initWithImageTitleSpace:5];
        _moreBtn.title = @"更多".localized;
        _moreBtn.image = [UIImage imageNamed:@"ic_zhibo_fensituan_gengduo"].adaptiveRtl;
        _moreBtn.titleColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _moreBtn.titleFont = [UIFont mediumFontOfSize:15];
        [_moreBtn addTarget:self action:@selector(moreBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _moreBtn;
}

- (UIImageView *)acceptGiftBgView {
    if (!_acceptGiftBgView) {
        _acceptGiftBgView = [[UIImageView alloc] init];
        NSArray *colors = @[[UIColor colorWithHexString:@"#FF6B93"], [UIColor whiteColor]];
        UIImage *bgImg = [[UIImage alloc] initWithGradient:colors size:CGSizeMake((kScreenW - 45) / 2, 110) direction:UIImageGradientColorsDirectionHorizontal].adaptiveRtl;
        _acceptGiftBgView.image = bgImg;
        _acceptGiftBgView.layer.cornerRadius = 5;
        _acceptGiftBgView.layer.masksToBounds = true;
    }
    return _acceptGiftBgView;
}

- (UIImageView *)acceptGiftIconView {
    if (!_acceptGiftIconView) {
        _acceptGiftIconView = [[UIImageView alloc] init];
        _acceptGiftIconView.image = [UIImage imageNamed:@"ic_gerenzhuye_shoudaoliwu"];
    }
    return _acceptGiftIconView;
}

- (UILabel *)acceptGiftTitleLabel {
    if (!_acceptGiftTitleLabel) {
        _acceptGiftTitleLabel = [[UILabel alloc] init];
        _acceptGiftTitleLabel.textColor = [UIColor whiteColor];
        _acceptGiftTitleLabel.font = [UIFont mediumFontOfSize:12];
        _acceptGiftTitleLabel.text = @"收到礼物".localized;
    }
    return _acceptGiftTitleLabel;
}

- (UILabel *)acceptGiftNumberLabel {
    if (!_acceptGiftNumberLabel) {
        _acceptGiftNumberLabel = [[UILabel alloc] init];
        _acceptGiftNumberLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _acceptGiftNumberLabel.font = [UIFont mediumFontOfSize:24];
        _acceptGiftNumberLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _acceptGiftNumberLabel;
}

- (UILabel *)acceptGiftDescLabel {
    if (!_acceptGiftDescLabel) {
        _acceptGiftDescLabel = [[UILabel alloc] init];
        _acceptGiftDescLabel.textColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.6];
        _acceptGiftDescLabel.font = [UIFont mediumFontOfSize:12];
        _acceptGiftDescLabel.text = @"本周魅力值".localized;
        _acceptGiftDescLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _acceptGiftDescLabel;
}

- (UIImageView *)giveGiftBgView {
    if (!_giveGiftBgView) {
        _giveGiftBgView = [[UIImageView alloc] init];
        NSArray *colors = @[[UIColor colorWithHexString:@"#FFD767"], [UIColor whiteColor]];
        UIImage *bgImg = [[UIImage alloc] initWithGradient:colors size:CGSizeMake((kScreenW - 45) / 2, 110) direction:UIImageGradientColorsDirectionHorizontal].adaptiveRtl;
        _giveGiftBgView.image = bgImg;
        _giveGiftBgView.layer.cornerRadius = 5;
        _giveGiftBgView.layer.masksToBounds = true;
    }
    return _giveGiftBgView;
}

- (UIImageView *)giveGiftIconView {
    if (!_giveGiftIconView) {
        _giveGiftIconView = [[UIImageView alloc] init];
        _giveGiftIconView.image = [UIImage imageNamed:@"ic_gerenzhuye_songchuliwu"];
    }
    return _giveGiftIconView;
}

- (UILabel *)giveGiftTitleLabel {
    if (!_giveGiftTitleLabel) {
        _giveGiftTitleLabel = [[UILabel alloc] init];
        _giveGiftTitleLabel.textColor = [UIColor whiteColor];
        _giveGiftTitleLabel.font = [UIFont mediumFontOfSize:12];
        _giveGiftTitleLabel.text = @"送出礼物".localized;
    }
    return _giveGiftTitleLabel;
}

- (UILabel *)giveGiftNumberLabel {
    if (!_giveGiftNumberLabel) {
        _giveGiftNumberLabel = [[UILabel alloc] init];
        _giveGiftNumberLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _giveGiftNumberLabel.font = [UIFont mediumFontOfSize:24];
        _giveGiftNumberLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _giveGiftNumberLabel;
}

- (UILabel *)giveGiftDescLabel {
    if (!_giveGiftDescLabel) {
        _giveGiftDescLabel = [[UILabel alloc] init];
        _giveGiftDescLabel.textColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.6];
        _giveGiftDescLabel.font = [UIFont mediumFontOfSize:12];
        _giveGiftDescLabel.text = @"本周土豪值".localized;
        _giveGiftDescLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _giveGiftDescLabel;
}

- (instancetype)initWithType:(LY_HomePageType)type {
    self = [super initWithType:type];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
        [self setupDefaultData];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.titleLabel];
    [self addSubview:self.moreBtn];
    [self addSubview:self.acceptGiftBgView];
    [self.acceptGiftBgView addSubview:self.acceptGiftIconView];
    [self.acceptGiftBgView addSubview:self.acceptGiftTitleLabel];
    [self.acceptGiftBgView addSubview:self.acceptGiftNumberLabel];
    [self.acceptGiftBgView addSubview:self.acceptGiftDescLabel];
    
    [self addSubview:self.giveGiftBgView];
    [self.giveGiftBgView addSubview:self.giveGiftIconView];
    [self.giveGiftBgView addSubview:self.giveGiftTitleLabel];
    [self.giveGiftBgView addSubview:self.giveGiftNumberLabel];
    [self.giveGiftBgView addSubview:self.giveGiftDescLabel];
}

- (void)setupUIFrame {
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(10);
    }];
    [self.moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-15);
        make.height.mas_equalTo(35);
        make.centerY.mas_equalTo(self.titleLabel);
    }];
    [self.acceptGiftBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(10);
        make.height.mas_equalTo(110);
        make.leading.mas_equalTo(15);
        make.width.mas_equalTo((kScreenW - 45) / 2);
        make.bottom.mas_equalTo(-10);
    }];
    [self.acceptGiftIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(11, 10));
        make.top.mas_equalTo(7);
        make.leading.mas_equalTo(7);
    }];
    [self.acceptGiftTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.acceptGiftIconView);
        make.leading.mas_equalTo(self.acceptGiftIconView.mas_trailing).offset(5);
    }];
    [self.acceptGiftNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.acceptGiftTitleLabel.mas_bottom).offset(16);
        make.leading.trailing.mas_equalTo(self.acceptGiftBgView);
    }];
    [self.acceptGiftDescLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.acceptGiftNumberLabel.mas_bottom);
        make.leading.trailing.mas_equalTo(self.acceptGiftBgView);
    }];
    
    [self.giveGiftBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(10);
        make.height.mas_equalTo(110);
        make.trailing.mas_equalTo(-15);
        make.width.mas_equalTo((kScreenW - 45) / 2);
    }];
    [self.giveGiftIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(11, 10));
        make.top.mas_equalTo(7);
        make.leading.mas_equalTo(7);
    }];
    [self.giveGiftTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.giveGiftIconView);
        make.leading.mas_equalTo(self.giveGiftIconView.mas_trailing).offset(5);
    }];
    [self.giveGiftNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.acceptGiftTitleLabel.mas_bottom).offset(16);
        make.leading.trailing.mas_equalTo(self.giveGiftBgView);
    }];
    [self.giveGiftDescLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.acceptGiftNumberLabel.mas_bottom);
        make.leading.trailing.mas_equalTo(self.giveGiftBgView);
    }];
}

- (void)setupDefaultData {
    self.acceptGiftNumberLabel.text = @"0";
    self.giveGiftNumberLabel.text = @"0";
}

- (void)setUserInfo:(LY_HomePageInfo *)userInfo {
    [super setUserInfo:userInfo];
    
    self.acceptGiftNumberLabel.text = [SPDCommonTool transformIntegerToBriefStr:userInfo.charmWeek];
    self.giveGiftNumberLabel.text = [SPDCommonTool transformIntegerToBriefStr:userInfo.richeWeek];
}

- (void)moreBtnAction {
    if (self.type) {
        //[MobClick event:@"FM5_2_9"];
    } else {
        //[MobClick event:@"FM5_3_14"];
    }
    ChatroomWealthController * vc = [ChatroomWealthController new];
    vc.ID = self.userInfo._id;
    [self.VC.navigationController pushViewController:vc animated:YES];
}

@end
