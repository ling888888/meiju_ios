//
//  LY_HPBottomView.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/11.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_HPBottomView.h"
#import "JXPopoverView.h"

@interface LY_HPBottomView ()

@property(nonatomic, strong) UIButton *chatBtn;

@property(nonatomic, strong) UIButton *friendsBtn;

@property(nonatomic, strong) UIButton *videoBtn;

@property(nonatomic, strong) UIButton *audioBtn;

@property(nonatomic, strong) UIButton *followBtn;

@end

@implementation LY_HPBottomView

- (UIButton *)chatBtn {
    if (!_chatBtn) {
        _chatBtn = [[UIButton alloc] init];
        [_chatBtn setImage:[UIImage imageNamed:@"ic_gerenzhuye_faxiaoxi"] forState:UIControlStateNormal];
        [_chatBtn addTarget:self action:@selector(chatBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _chatBtn;
}

- (UIButton *)friendsBtn {
    if (!_friendsBtn) {
        _friendsBtn = [[UIButton alloc] init];
        [_friendsBtn setImage:[UIImage imageNamed:@"ic_gerenzhuye_jiahaoyou"] forState:UIControlStateNormal];
        [_friendsBtn addTarget:self action:@selector(friendsBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _friendsBtn;
}

- (UIButton *)videoBtn {
    if (!_videoBtn) {
        _videoBtn = [[UIButton alloc] init];
        [_videoBtn setImage:[UIImage imageNamed:@"ic_gerenzhuye_shipin"] forState:UIControlStateNormal];
        [_videoBtn addTarget:self action:@selector(videoBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _videoBtn;
}

- (UIButton *)audioBtn {
    if (!_audioBtn) {
        _audioBtn = [[UIButton alloc] init];
        [_audioBtn setImage:[UIImage imageNamed:@"ic_gerenzhuye_yuyin"] forState:UIControlStateNormal];
        [_audioBtn addTarget:self action:@selector(audioBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _audioBtn;
}

- (UIButton *)followBtn {
    if (!_followBtn) {
        _followBtn = [[UIButton alloc] init];
        [_followBtn setBackgroundImage:[UIImage imageNamed:@"ic_gerenzhuye_guanzhu"] forState:UIControlStateNormal];
        [_followBtn setTitle:@"+关注".localized forState:UIControlStateNormal];
        [_followBtn addTarget:self action:@selector(followBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _followBtn;
}


- (instancetype)initWithType:(LY_HomePageType)type {
    self = [super initWithType:type];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
        [self setupDefaultData];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.chatBtn];

}

- (void)setupUIFrame {
    [self.chatBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(72.5);
        make.centerY.mas_equalTo(0);
        make.trailing.mas_equalTo(-10);
    }];
}

- (void)setupDefaultData {
    if (self.type == LY_HomePageTypeMine) {
        self.chatBtn.hidden = true;
    } else {
        self.chatBtn.hidden = false;
    }
}

- (void)refresh {
    if (self.type == LY_HomePageTypeMine) {
        
    } else {
        [self updateSubviews];
    }
}

- (void)setUserInfo:(LY_HomePageInfo *)userInfo {
    [super setUserInfo:userInfo];
    
    if (self.type == LY_HomePageTypeMine) {
        
    } else {
        [self updateSubviews];
    }
}

- (void)updateSubviews {
    if (self.userInfo.isFriend) {
        if (_friendsBtn.superview) {
            [self.friendsBtn removeFromSuperview];
        }
        [self addSubview:self.videoBtn];
        [self addSubview:self.audioBtn];
        [self.videoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(72.5);
            make.centerY.mas_equalTo(0);
            make.trailing.mas_equalTo(self.chatBtn.mas_leading).offset(-5);
        }];
        [self.audioBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(72.5);
            make.centerY.mas_equalTo(0);
            make.trailing.mas_equalTo(self.videoBtn.mas_leading).offset(-5);
        }];
        
        if (self.userInfo.videoEnable) {
            [self.videoBtn setImage:[UIImage imageNamed:@"ic_gerenzhuye_shipin"] forState:UIControlStateNormal];
        } else {
            [self.videoBtn setImage:[UIImage imageNamed:@"ic_gerenzhuye_shipin_zhihui"] forState:UIControlStateNormal];
        }
        if (self.userInfo.audioEnable) {
            [self.audioBtn setImage:[UIImage imageNamed:@"ic_gerenzhuye_yuyin"] forState:UIControlStateNormal];
        } else {
            [self.audioBtn setImage:[UIImage imageNamed:@"ic_gerenzhuye_yuyin_zhihui"] forState:UIControlStateNormal];
        }
        
//        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"videoBtnPopoverView"]) {
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                JXPopoverView *popoverView = [JXPopoverView popoverView];
//                popoverView.style = PopoverViewStyleDark;
//                JXPopoverAction *action1 = [JXPopoverAction actionWithTitle:@"添加好友后可以进行语音或视频聊天".localized handler:^(JXPopoverAction *action) {
//                }];
//                [popoverView showToView:self.videoBtn withActions:@[action1]];
//            });
//            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"videoBtnPopoverView"];
//        }
    } else {
        if (_videoBtn.superview) {
            [self.videoBtn removeFromSuperview];
        }
        if (_audioBtn.superview) {
            [self.audioBtn removeFromSuperview];
        }
        [self addSubview:self.friendsBtn];
        [self.friendsBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(72.5);
            make.centerY.mas_equalTo(0);
            make.trailing.mas_equalTo(self.chatBtn.mas_leading).offset(-5);
        }];
        
        if (self.userInfo.isBlock) {
            [self.friendsBtn setImage:[UIImage imageNamed:@"ic_gerenzhuye_jiahaoyou_zhihui"] forState:UIControlStateNormal];
        } else {
            [self.friendsBtn setImage:[UIImage imageNamed:@"ic_gerenzhuye_jiahaoyou"] forState:UIControlStateNormal];
        }
    }
    
    if (self.userInfo.isAnchor) {
        if (self.userInfo.isFollowLiver) {
            if (_followBtn.superview) {
                [self.followBtn removeFromSuperview];
            }
        } else {
            UIView *lastView = self.subviews.lastObject;
            [self addSubview:self.followBtn];
            [self.followBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(123.5, 72.5));
                make.centerY.mas_equalTo(0);
                make.leading.mas_equalTo(10);
            }];
        }
    }
}

- (void)chatBtnAction {
    if ([self.delegate respondsToSelector:@selector(bottomView:didClickChatBtn:)]) {
        [self.delegate bottomView:self didClickChatBtn:self.chatBtn];
    }
}

- (void)friendsBtnAction {
    if ([self.delegate respondsToSelector:@selector(bottomView:didClickFriendsBtn:)]) {
        [self.delegate bottomView:self didClickFriendsBtn:self.friendsBtn];
    }
}

- (void)videoBtnAction {
    if ([self.delegate respondsToSelector:@selector(bottomView:didClickVideoBtn:)]) {
        [self.delegate bottomView:self didClickVideoBtn:self.videoBtn];
    }
}

- (void)audioBtnAction {
    if ([self.delegate respondsToSelector:@selector(bottomView:didClickAudioBtn:)]) {
        [self.delegate bottomView:self didClickAudioBtn:self.audioBtn];
    }
}

- (void)followBtnAction {
    if ([self.delegate respondsToSelector:@selector(bottomView:didClickFollowBtn:)]) {
        [self.delegate bottomView:self didClickFollowBtn:self.followBtn];
    }
}


@end
