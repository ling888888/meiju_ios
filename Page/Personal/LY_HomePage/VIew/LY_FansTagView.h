//
//  LY_FansTagView.h
//  SimpleDate
//
//  Created by 杨宝龙 on 2020/6/8.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LY_FansTag.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_FansTagView : UIView



@property(nonatomic, strong) LY_FansTag *fansTag;

@end



NS_ASSUME_NONNULL_END
