//
//  LY_HPGuideView.h
//  SimpleDate
//
//  Created by jianyue on 2020/5/13.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_HPGuideView : UIView

@property(nonatomic, assign) CGRect oneFrame;

@property(nonatomic, assign) CGRect twoFrame;

@end

NS_ASSUME_NONNULL_END
