//
//  LY_HPGuardListView.h
//  SimpleDate
//
//  Created by jianyue on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_HPBaseView.h"
#import "LY_Patrons.h"

//NS_ASSUME_NONNULL_BEGIN

@interface LY_HPGuardListView : LY_HPBaseView

@end



@interface LY_HPGuardCollectionViewCell : UICollectionViewCell

@property(nonatomic, strong) LY_Patrons *patrons;

@property(nonatomic, assign) NSInteger index;

@end

//NS_ASSUME_NONNULL_END
