//
//  LY_HPMedalCollectionCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/9/18.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class  MedalModel;
@interface LY_HPMedalCollectionCell : UICollectionViewCell

@property (nonatomic, strong) MedalModel * medalModel;

@end

NS_ASSUME_NONNULL_END
