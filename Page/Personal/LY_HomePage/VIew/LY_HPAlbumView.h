//
//  LY_HPAlbumView.h.h
//  SimpleDate
//
//  Created by jianyue on 2020/5/11.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_HPBaseView.h"
#import "HXPhotoPicker.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_HPAlbumView : LY_HPBaseView

@property(nonatomic, strong) HXPhotoManager *manager;

@property(nonatomic, strong) HXPhotoView *photoView;

@end

NS_ASSUME_NONNULL_END
