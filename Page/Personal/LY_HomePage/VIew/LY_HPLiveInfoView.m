//
//  LY_HPLiveInfoView.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/30.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_HPLiveInfoView.h"
#import "FollowContainerController.h"
#import "DetailsUserFansController.h"
#import "LY_LiveAudienceHalfVC.h"
#import "ZegoKitManager.h"
#import "LiveModel.h"

@interface LY_HPLiveInfoView ()

@property(nonatomic, strong) UIView *backgroundView;

@property(nonatomic, strong) UIControl *followNumBGControl;

@property(nonatomic, strong) UILabel *followNumKeyLabel;

@property(nonatomic, strong) UILabel *followNumValueLabel;

@property(nonatomic, strong) UIImageView *followMoreIconView;

@property(nonatomic, strong) UIControl *fansNumBGControl;

@property(nonatomic, strong) UILabel *fansNumKeyLabel;

@property(nonatomic, strong) UILabel *fansNumValueLabel;

@property(nonatomic, strong) UIImageView *fansMoreIcon;

@property(nonatomic, strong) LY_HPLivingInfoView *livingInfoView;

@property(nonatomic, strong) LY_HPLiveTipRangkInfoView *liveTipRangkInfoView;

@end

@implementation LY_HPLiveInfoView

- (UIView *)backgroundView {
    if (!_backgroundView) {
        _backgroundView = [[UIView alloc] init];
        _backgroundView.backgroundColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.03];
        _backgroundView.layer.cornerRadius = 5;
        _backgroundView.layer.masksToBounds = true;
    }
    return _backgroundView;
}

- (UIControl *)followNumBGControl {
    if (!_followNumBGControl) {
        _followNumBGControl = [[UIControl alloc] init];
        [_followNumBGControl addTarget:self action:@selector(followNumBGControlAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _followNumBGControl;
}

- (UILabel *)followNumKeyLabel {
    if (!_followNumKeyLabel) {
        _followNumKeyLabel = [[UILabel alloc] init];
        _followNumKeyLabel.textColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.6];
        _followNumKeyLabel.font = [UIFont mediumFontOfSize:12];
        _followNumKeyLabel.text = @"关注".localized;
    }
    return _followNumKeyLabel;
}

- (UILabel *)followNumValueLabel {
    if (!_followNumValueLabel) {
        _followNumValueLabel = [[UILabel alloc] init];
        _followNumValueLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _followNumValueLabel.font = [UIFont mediumFontOfSize:18];
    }
    return _followNumValueLabel;
}

- (UIImageView *)followMoreIconView {
    if (!_followMoreIconView) {
        _followMoreIconView = [[UIImageView alloc] init];
        _followMoreIconView.image = [UIImage imageNamed:@"ic_gerenzhuye_guanzhufensi"].adaptiveRtl;
    }
    return _followMoreIconView;
}

- (UIControl *)fansNumBGControl {
    if (!_fansNumBGControl) {
        _fansNumBGControl = [[UIControl alloc] init];
        [_fansNumBGControl addTarget:self action:@selector(fansNumBGControlAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _fansNumBGControl;
}

- (UILabel *)fansNumKeyLabel {
    if (!_fansNumKeyLabel) {
        _fansNumKeyLabel = [[UILabel alloc] init];
        _fansNumKeyLabel.textColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.6];
        _fansNumKeyLabel.font = [UIFont mediumFontOfSize:12];
        _fansNumKeyLabel.text = @"粉丝".localized;
    }
    return _fansNumKeyLabel;
}

- (UILabel *)fansNumValueLabel {
    if (!_fansNumValueLabel) {
        _fansNumValueLabel = [[UILabel alloc] init];
        _fansNumValueLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _fansNumValueLabel.font = [UIFont mediumFontOfSize:18];
    }
    return _fansNumValueLabel;
}

- (UIImageView *)fansMoreIcon {
    if (!_fansMoreIcon) {
        _fansMoreIcon = [[UIImageView alloc] init];
        _fansMoreIcon.image = [UIImage imageNamed:@"ic_gerenzhuye_guanzhufensi"].adaptiveRtl;
    }
    return _fansMoreIcon;
}

- (LY_HPLivingInfoView *)livingInfoView {
    if (!_livingInfoView) {
        _livingInfoView = [[LY_HPLivingInfoView alloc] initWithType:self.type];
//        _liveingInfoView
    }
    return _livingInfoView;
}

- (LY_HPLiveTipRangkInfoView *)liveTipRangkInfoView {
    if (!_liveTipRangkInfoView) {
        _liveTipRangkInfoView = [[LY_HPLiveTipRangkInfoView alloc] initWithType:self.type];
        _liveTipRangkInfoView.layer.cornerRadius = 5;
        _liveTipRangkInfoView.layer.masksToBounds = true;
        _liveTipRangkInfoView.backgroundColor = [[UIColor colorWithHexString:@"#6A2CF7"] colorWithAlphaComponent:0.05];
    }
    return _liveTipRangkInfoView;
}

- (instancetype)initWithType:(LY_HomePageType)type {
    self = [super initWithType:type];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
        [self setupDefaultData];
        
//        [self setFansNumber:0];
        // 有直播、有榜单
//        [self setLivingInfo:@"1" andTipRangkInfo:@"1"];
        // 不直播、没榜单
//        [self setLivingInfo:nil andTipRangkInfo:nil];
        // 不直播、有榜单
//        [self setLivingInfo:nil andTipRangkInfo:@"1"];
        // 有直播、没榜单
//        [self setLivingInfo:@"1" andTipRangkInfo:nil];

    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.backgroundView];
    [self.backgroundView addSubview:self.followNumBGControl];
    [self.followNumBGControl addSubview:self.followNumKeyLabel];
    [self.followNumBGControl addSubview:self.followNumValueLabel];
    [self.followNumBGControl addSubview:self.followMoreIconView];
}

- (void)setupUIFrame {
    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(10, 15, 10, 15));
        make.height.mas_equalTo(44);
//        make.height.mas_equalTo(185);
    }];
    [self.followNumBGControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(9);
        make.leading.mas_equalTo(10);
        make.height.mas_equalTo(20);
    }];
    [self.followNumKeyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(12);
    }];
    [self.followNumValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.followNumKeyLabel.mas_trailing).offset(5);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(17);
    }];
    [self.followMoreIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(6, 10));
        make.leading.mas_equalTo(self.followNumValueLabel.mas_trailing).offset(5);
        make.bottom.mas_equalTo(0);
        make.trailing.mas_equalTo(-5);
    }];
}

- (void)setupDefaultData {
    self.followNumValueLabel.text = @"0";
}

- (void)setUserInfo:(LY_HomePageInfo *)userInfo {
    [super setUserInfo:userInfo];
    
    //+ (NSString *)transformIntegerToBriefStr:(NSInteger)integer;
    
    self.followNumValueLabel.text = [NSString stringWithFormat:@"%d", userInfo.followLiverNum];
    
    if (userInfo.isAnchor) {
        // 设置粉丝数
        [self setFansNumber:userInfo.fanNum];
        // 设置直播中和打赏榜
        if (userInfo.isLiving) {
            self.livingInfoView.userInfo = self.userInfo;
            [self.backgroundView addSubview:self.livingInfoView];
            [self.livingInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.leading.mas_equalTo(10);
                make.trailing.mas_equalTo(-10);
                make.top.mas_equalTo(self.followNumBGControl.mas_bottom).offset(15);
                make.height.mas_equalTo(48);
            }];
        }
        if (userInfo.supportNum > 0) {
            self.liveTipRangkInfoView.userInfo = self.userInfo;
            [self.backgroundView addSubview:self.liveTipRangkInfoView];
            [self.liveTipRangkInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.leading.mas_equalTo(10);
                make.trailing.mas_equalTo(-10);
                if (_livingInfoView) {
                    make.top.mas_equalTo(self.followNumBGControl.mas_bottom).offset(80);
                } else {
                    make.top.mas_equalTo(self.followNumBGControl.mas_bottom).offset(15);
                }
                make.height.mas_equalTo(50);
            }];
        }
        if (userInfo.isLiving && (userInfo.supportNum > 0)) {
            [self.backgroundView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(177);
            }];
        } else if (userInfo.isLiving || (userInfo.supportNum > 0)) {
            [self.backgroundView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(105);
            }];
        } else {
            [self.backgroundView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(44);
            }];
        }
    }
}

/// 设置粉丝数
- (void)setFansNumber:(NSInteger)fansNumber {
    // 如果有粉丝，显示粉丝数
    self.fansNumValueLabel.text = [NSString stringWithFormat:@"%ld", fansNumber];
    [self.backgroundView addSubview:self.fansNumBGControl];
    [self.fansNumBGControl addSubview:self.fansNumKeyLabel];
    [self.fansNumBGControl addSubview:self.fansNumValueLabel];
    [self.fansNumBGControl addSubview:self.fansMoreIcon];
    [self.fansNumBGControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.followNumBGControl.mas_trailing).offset(10);
        make.height.mas_equalTo(20);
        make.bottom.mas_equalTo(self.followNumBGControl);
    }];
    [self.fansNumKeyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(13);
    }];
    [self.fansNumValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.fansNumKeyLabel.mas_trailing).offset(5);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(17);
    }];
    [self.fansMoreIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(6, 10));
        make.leading.mas_equalTo(self.fansNumValueLabel.mas_trailing).offset(5);
        make.trailing.mas_equalTo(-5);
        make.bottom.mas_equalTo(0);
    }];
}


//- (void)setLiving:(BOOL)living liveTitle:(NSString *)liveTitle liveCount:(int)liveCount andTipRangkInfo:(NSString *)tipRangkInfo {
//    if (livingInfo != nil) {
//
//    }
//    if (tipRangkInfo != nil) {
//
//    }
//
//}

// 关注数点击事件
- (void)followNumBGControlAction {
    if (self.type == LY_HomePageTypeMine) {
        //[MobClick event:@"FM5_2_3"];
    } else {
        //[MobClick event:@"FM5_3_2"];
    }
    FollowContainerController * followContainerC = [FollowContainerController new];
    followContainerC.userId = self.userInfo._id;
    [self.VC.navigationController pushViewController:followContainerC animated:true];
}

// 粉丝数点击事件
- (void)fansNumBGControlAction {
    if (self.type == LY_HomePageTypeMine) {
        //[MobClick event:@"FM5_2_4"];
    } else {
        //[MobClick event:@"FM5_3_3"];
    }
    DetailsUserFansController * detailsUserFansC = [DetailsUserFansController new];
    detailsUserFansC.userId = self.userInfo._id;
    [self.VC.navigationController pushViewController:detailsUserFansC animated:true];
}

@end

#import "KKPaddingLabel.h"


@interface LY_HPLivingInfoView ()

@property(nonatomic, strong) UIView *backgroundView;

@property(nonatomic, strong) KKPaddingLabel *livingLabel;

@property(nonatomic, strong) UIImageView *portraitView;

@property(nonatomic, strong) UILabel *textLabel;

@property(nonatomic, strong) UIImageView *iconView;

@property(nonatomic, strong) UILabel *visitorsLabel;

@property(nonatomic, strong) UIButton *toViewBtn;

@end

@implementation LY_HPLivingInfoView

- (UIView *)backgroundView {
    if (!_backgroundView) {
        _backgroundView = [[UIView alloc] init];
        _backgroundView.backgroundColor = [UIColor colorWithHexString:@"#6A2CF7"];
        _backgroundView.layer.cornerRadius = 20;
        _backgroundView.layer.masksToBounds = true;
    }
    return _backgroundView;
}

- (KKPaddingLabel *)livingLabel {
    if (!_livingLabel) {
        _livingLabel = [[KKPaddingLabel alloc] init];
        if (kIsMirroredLayout) {
            _livingLabel.padding = UIEdgeInsetsMake(0, 10, 0, 18);
        } else {
            _livingLabel.padding = UIEdgeInsetsMake(0, 18, 0, 10);
        }
        _livingLabel.layer.cornerRadius = 10;
        _livingLabel.layer.masksToBounds = true;
        _livingLabel.backgroundColor = [UIColor whiteColor];
        _livingLabel.textColor = [UIColor colorWithHexString:@"#6A2CF7"];
        _livingLabel.font = [UIFont mediumFontOfSize:10];
        _livingLabel.text = @"直播中".localized;
    }
    return _livingLabel;
}

- (UIImageView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[UIImageView alloc] init];
        _portraitView.contentMode = UIViewContentModeScaleAspectFill;
        _portraitView.layer.cornerRadius = 24;
        _portraitView.layer.masksToBounds = true;
    }
    return _portraitView;
}

- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] init];
        _textLabel.textColor = [UIColor whiteColor];
        _textLabel.font = [UIFont mediumFontOfSize:12];
    }
    return _textLabel;
}

- (UIImageView *)iconView {
    if (!_iconView) {
        _iconView = [[UIImageView alloc] init];
        NSMutableArray *animationImages = [NSMutableArray array];
        for (NSInteger i = 0; i < 8; i++) {
//            [animationImages addObject:[UIImage imageNamed:[NSString stringWithFormat:@"audio_wave_%zd", i]]];
        }
        _iconView.animationImages = animationImages;
        _iconView.animationDuration = animationImages.count * 0.1;
        [_iconView startAnimating];
    }
    return _iconView;
}

- (UILabel *)visitorsLabel {
    if (!_visitorsLabel) {
        _visitorsLabel = [[UILabel alloc] init];
        _visitorsLabel.textColor = [UIColor whiteColor];
        _visitorsLabel.font = [UIFont mediumFontOfSize:12];
    }
    return _visitorsLabel;
}

- (UIButton *)toViewBtn {
    if (!_toViewBtn) {
        _toViewBtn = [[UIButton alloc] init];
        [_toViewBtn setTitle:@"去捧场".localized forState:UIControlStateNormal];
        [_toViewBtn setTitleColor:[UIColor colorWithHexString:@"#6A2CF7"] forState:UIControlStateNormal];
        _toViewBtn.titleLabel.font = [UIFont mediumFontOfSize:12];
        _toViewBtn.backgroundColor = [UIColor whiteColor];
        _toViewBtn.layer.cornerRadius = 12.5;
        _toViewBtn.layer.masksToBounds = true;
        _toViewBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 13, 0, 13);
        _toViewBtn.userInteractionEnabled = false;
    }
    return _toViewBtn;
}

- (instancetype)initWithType:(LY_HomePageType)type {
    self = [super initWithType:type];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickSelfAction)]];
//        [self setupDefaultData];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.backgroundView];
    [self.backgroundView addSubview:self.livingLabel];
    [self addSubview:self.portraitView];
    [self.backgroundView addSubview:self.textLabel];
    [self.backgroundView addSubview:self.iconView];
    [self.backgroundView addSubview:self.visitorsLabel];
    [self.backgroundView addSubview:self.textLabel];
    [self.backgroundView addSubview:self.toViewBtn];
}

- (void)setupUIFrame {
    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(4, 6, 4, 0));
    }];
    [self.livingLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.leading.mas_equalTo(33);
        make.centerY.mas_equalTo(self);
    }];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(48);
        make.leading.mas_equalTo(self);
        make.centerY.mas_equalTo(self);
    }];
    [self.toViewBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-10);
        make.height.mas_equalTo(25);
        make.centerY.mas_equalTo(0);
    }];
    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.livingLabel.mas_trailing).offset(5);
        make.top.mas_equalTo(3);
        make.trailing.mas_lessThanOrEqualTo(self.toViewBtn.mas_leading).offset(-15);
    }];
    [self.iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.textLabel);
        make.bottom.mas_equalTo(-8);
        make.size.mas_equalTo(CGSizeMake(10, 10));
    }];
    [self.visitorsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.iconView.mas_trailing).offset(5);
        make.bottom.mas_equalTo(-3);
        make.trailing.mas_equalTo(self.textLabel);
    }];
}

- (void)setUserInfo:(LY_HomePageInfo *)userInfo {
    [super setUserInfo:userInfo];
    
    [self.portraitView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:userInfo.avatar]] placeholderImage:[UIImage imageNamed:@"img_portrait_placeholder"]];
    
    self.textLabel.text = userInfo.liveTitle;
    
    self.visitorsLabel.text = [NSString stringWithFormat:@"%d人收听".localized, userInfo.listenNum];
}

- (void)clickSelfAction {
    if (self.type == LY_HomePageTypeMine) {
        //[MobClick event:@"FM5_2_6"];
    } else {
        //[MobClick event:@"FM5_3_12"];
    }
    LiveModel * model = [[LiveModel alloc] init];
    model.userId = self.userInfo._id;
    model.liveId = self.userInfo.liveId;
    [ZegoManager joinLiveRoomWithliveInfo:model isAnchor:NO];
}

@end

#import "LY_Button.h"

@interface LY_HPLiveTipRangkInfoView ()

@property(nonatomic, strong) UIImageView *imgView;

@property(nonatomic, strong) UILabel *titleLabel;

@property(nonatomic, strong) LY_Button *numberBtn;

@property(nonatomic, strong) UIImageView *moreImgView;

@property(nonatomic, strong) UIImageView *portraitView1;

@property(nonatomic, strong) UIImageView *portraitView2;

@property(nonatomic, strong) UIImageView *portraitView3;

@end

@implementation LY_HPLiveTipRangkInfoView

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = [[UIImageView alloc] init];
        _imgView.image = [UIImage imageNamed:@"ic_gerenzhuye_dashangbang"];
    }
    return _imgView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _titleLabel.font = [UIFont mediumFontOfSize:15];
        _titleLabel.text = @"打赏榜".localized;
    }
    return _titleLabel;
}

- (LY_Button *)numberBtn {
    if (!_numberBtn) {
        _numberBtn = [[LY_Button alloc] initWithImageTitleSpace:5];
        _numberBtn.titleColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.6];
        _numberBtn.titleFont = [UIFont mediumFontOfSize:12];
        _numberBtn.image = [UIImage imageNamed:@"ic_gerenzhuye_dashangzhichi"].adaptiveRtl;
    }
    return _numberBtn;
}

- (UIImageView *)moreImgView {
    if (!_moreImgView) {
        _moreImgView = [[UIImageView alloc] init];
        _moreImgView.image = [UIImage imageNamed:@"ic_gerenzhuye_dashangtiaozhuan"].adaptiveRtl;
    }
    return _moreImgView;
}

- (UIImageView *)portraitView1 {
    if (!_portraitView1) {
        _portraitView1 = [[UIImageView alloc] init];
        _portraitView1.layer.cornerRadius = 17;
        _portraitView1.layer.borderWidth = 2;
        _portraitView1.layer.borderColor = [[UIColor whiteColor] CGColor];
        _portraitView1.layer.masksToBounds = true;
    }
    return _portraitView1;
}

- (UIImageView *)portraitView2 {
    if (!_portraitView2) {
        _portraitView2 = [[UIImageView alloc] init];
        _portraitView2.layer.cornerRadius = 17;
        _portraitView2.layer.borderWidth = 2;
        _portraitView2.layer.borderColor = [[UIColor whiteColor] CGColor];
        _portraitView2.layer.masksToBounds = true;
    }
    return _portraitView2;
}

- (UIImageView *)portraitView3 {
    if (!_portraitView3) {
        _portraitView3 = [[UIImageView alloc] init];
        _portraitView3.layer.cornerRadius = 17;
        _portraitView3.layer.borderWidth = 2;
        _portraitView3.layer.borderColor = [[UIColor whiteColor] CGColor];
        _portraitView3.layer.masksToBounds = true;
    }
    return _portraitView3;
}

- (instancetype)initWithType:(LY_HomePageType)type {
    self = [super initWithType:type];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickSelfAction)]];
//        [self setupDefaultData];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.imgView];
    [self addSubview:self.titleLabel];
    [self addSubview:self.numberBtn];
    [self addSubview:self.moreImgView];
    [self addSubview:self.portraitView3];
    [self addSubview:self.portraitView2];
    [self addSubview:self.portraitView1];
}

- (void)setupUIFrame {
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(32);
        make.leading.mas_equalTo(10);
        make.centerY.mas_equalTo(self);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.imgView).offset(-4);
        make.leading.mas_equalTo(self.imgView.mas_trailing).offset(6);
    }];
    [self.numberBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.imgView).offset(4);
        make.leading.mas_equalTo(self.imgView.mas_trailing).offset(6);
    }];
    [self.moreImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(11, 17));
        make.centerY.mas_equalTo(self);
        make.trailing.mas_equalTo(-14);
    }];
    [self.portraitView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(34);
        make.trailing.mas_equalTo(self.moreImgView.mas_leading).offset(-17);
        make.centerY.mas_equalTo(self);
    }];
    [self.portraitView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(34);
        make.trailing.mas_equalTo(self.portraitView3).offset(-18);
        make.centerY.mas_equalTo(self);
    }];
    [self.portraitView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(34);
        make.trailing.mas_equalTo(self.portraitView2).offset(-18);
        make.centerY.mas_equalTo(self);
    }];
    
    self.portraitView1.hidden = true;
    self.portraitView2.hidden = true;
    self.portraitView3.hidden = true;
}

- (void)setUserInfo:(LY_HomePageInfo *)userInfo {
    [super setUserInfo:userInfo];
    
    self.numberBtn.title = [NSString stringWithFormat:@"%d人已支持".localized, userInfo.supportNum];
    
    for (int i = 0; i < userInfo.supportAvatars.count; i ++) {
        NSString *portraitUrlString = userInfo.supportAvatars[i];
        if (i == 0) {
            [self.portraitView1 sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:portraitUrlString]]];
            self.portraitView1.hidden = false;
        }
        if (i == 1) {
            [self.portraitView2 sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:portraitUrlString]]];
            self.portraitView2.hidden = false;
        }
        if (i == 2) {
            [self.portraitView3 sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:portraitUrlString]]];
            self.portraitView3.hidden = false;
        }
    }
}
         
- (void)clickSelfAction {
    if (self.type) {
        //[MobClick event:@"FM5_2_7"];
    } else {
        //[MobClick event:@"FM5_3_13"];
    }
    LY_LiveAudienceHalfVC *audienceHalfVC = [[LY_LiveAudienceHalfVC alloc] initWithControllerHeight:500];
    audienceHalfVC.liveId = self.userInfo.liveId;
    audienceHalfVC.type = @"mine";
    [self.VC presentViewController:audienceHalfVC animated:true completion:nil];
}

@end
