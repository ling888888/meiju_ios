//
//  LY_HPLiveInfoView.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/30.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_HPBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_HPLiveInfoView : LY_HPBaseView


@end

// 正在直播中
@interface LY_HPLivingInfoView : LY_HPBaseView


@end

// 打赏榜
@interface LY_HPLiveTipRangkInfoView : LY_HPBaseView


@end

NS_ASSUME_NONNULL_END
