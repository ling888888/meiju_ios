//
//  LY_HPMedalView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/9/18.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_HPBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_HPMedalView : LY_HPBaseView

@property (nonatomic, strong) NSMutableArray * dataArray;

@property (nonatomic, copy) NSString * userId;

@end

NS_ASSUME_NONNULL_END
