//
//  LY_IDView.h
//  SimpleDate
//
//  Created by jianyue on 2020/2/27.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_IDView : UIControl

@property(nonatomic, strong) UIFont *normalIDFont;

@property(nonatomic, strong) UIFont *specialIDFont;

@property(nonatomic, strong) UIColor *normalIDColor;

@property(nonatomic, strong) UIColor *specialIDColor;

@property(nonatomic, copy, readonly) NSString *value;

- (void)setID:(NSString *)ID isSpecialNum:(BOOL)isSpecialNum;

@end

NS_ASSUME_NONNULL_END
