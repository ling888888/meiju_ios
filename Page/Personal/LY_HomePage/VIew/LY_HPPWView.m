//
//  LY_HPPWView.m
//  SimpleDate
//
//  Created by houling on 2021/11/21.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_HPPWView.h"

@interface PWTypeView: UIView
@property (nonatomic, strong) UIImageView * bg;
@property (nonatomic, strong) UIImageView * icon;
@property (nonatomic, strong) UIImageView * haoping;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * price;
@property (nonatomic, strong) UILabel * priceDesc;
@property (nonatomic, strong) UIButton * order;
@end

@implementation PWTypeView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setui];
        [self setframe];

    }
    return self;
}

- (void)setui {
    [self addSubview:self.bg];
    [self.bg addSubview:self.icon];
    [self.bg addSubview:self.titleLabel];
    [self.bg addSubview:self.haoping];
    [self.bg addSubview:self.price];
    [self.bg addSubview:self.priceDesc];
    [self.bg addSubview:self.order];
}

-(void)setframe {
    [self.bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    [self.icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(14);
        make.centerY.mas_equalTo(self);
        make.width.mas_equalTo(45);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.leading.equalTo(self.icon.mas_trailing).offset(9);
    }];
    [self.haoping mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30);
        make.leading.equalTo(self.icon.mas_trailing).offset(7);
        make.size.mas_equalTo(CGSizeMake(63, 13));
    }];
    [self.price mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.haoping.mas_bottom).offset(5);
        make.leading.equalTo(self.icon.mas_trailing).offset(9);
    }];
    [self.priceDesc mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.price.mas_trailing).offset(4);
        make.centerY.equalTo(self.price.mas_centerY);
    }];
    [self.order mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-15);
        make.centerY.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(72, 24));
    }];
}

- (UIImageView *)bg {
    if (!_bg) {
        _bg = [UIImageView new];
    }
    return _bg;
}

- (UIImageView *)icon {
    if (!_icon) {
        _icon = [UIImageView new];
    }
    return _icon;
}

- (UIImageView *)haoping {
    if (!_haoping) {
        _haoping = [UIImageView new];
    }
    return _haoping;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#FAFAFA"];
        _titleLabel.font = [UIFont mediumFontOfSize:15];
        _titleLabel.text = @"剧本杀".localized;
    }
    return _titleLabel;
}
- (UILabel *)price {
    if (!_price) {
        _price = [[UILabel alloc] init];
        _price.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
        _price.font = [UIFont mediumFontOfSize:18];
        _price.text = @"99".localized;
    }
    return _price;
}

- (UILabel *)priceDesc {
    if (!_priceDesc) {
        _priceDesc = [[UILabel alloc] init];
        _priceDesc.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
        _priceDesc.font = [UIFont mediumFontOfSize:10];
        _priceDesc.text = @"金币/1小时".localized;
    }
    return _priceDesc;
}

- (UIButton *)order {
    if (!_order) {
        _order = [UIButton buttonWithType:UIButtonTypeCustom];
        [_order setBackgroundColor:[[UIColor colorWithHexString:@"#FFFFFF"] colorWithAlphaComponent:0.3]];
        [_order setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_order setTitle:@"立即下单" forState:UIControlStateNormal];
        _order.titleLabel.font = [UIFont systemFontOfSize:12];
        _order.layer.cornerRadius = 12;
        _order.clipsToBounds = YES;
    }
    return _order;
}

@end

@interface LY_HPPWView ()

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) PWTypeView * shaView;
@property (nonatomic, strong) PWTypeView * taotuoView;

@end

@implementation LY_HPPWView

- (instancetype)initWithType:(LY_HomePageType)type {
    self = [super initWithType:type];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.titleLabel];
    [self addSubview:self.shaView];
    [self addSubview:self.taotuoView];
}

-(void)setupUIFrame {
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(10);
    }];
    [self.shaView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(9);
        make.trailing.mas_equalTo(-15);
        make.height.mas_equalTo(75);
    }];
    [self.taotuoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(self.shaView.mas_bottom).offset(9);
        make.trailing.mas_equalTo(-15);
        make.height.mas_equalTo(75);
        make.bottom.mas_equalTo(-10);
    }];
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _titleLabel.font = [UIFont mediumFontOfSize:15];
        _titleLabel.text = @"助教技能".localized;
    }
    return _titleLabel;
}

- (PWTypeView *)shaView {
    if (!_shaView) {
        _shaView = [[PWTypeView alloc]init];
        _shaView.titleLabel.text = @"剧本杀";
        _shaView.icon.image = [UIImage imageNamed:@"ic_jubensha"];
        _shaView.bg.image = [UIImage imageNamed:@"bg_juben"];
    }
    return _shaView;
}

- (PWTypeView *)taotuoView {
    if (!_taotuoView) {
        _taotuoView = [[PWTypeView alloc]init];
        _taotuoView.titleLabel.text = @"密室逃脱";
        _taotuoView.icon.image = [UIImage imageNamed:@"ic_mishitaotuo"];
        _taotuoView.bg.image = [UIImage imageNamed:@"bg_mishi"];
    }
    return _taotuoView;
}

@end
