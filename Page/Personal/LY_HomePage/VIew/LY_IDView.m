//
//  LY_IDView.m
//  SimpleDate
//
//  Created by jianyue on 2020/2/27.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_IDView.h"

@interface LY_IDView ()

// 靓号图标
@property(nonatomic, strong) UIImageView *specialNumIconView;

// ID文本
@property(nonatomic, strong) UILabel *IDLabel;

@property(nonatomic, copy) NSString *_id;

@property(nonatomic, assign) BOOL isSpecialNum;

@end

@implementation LY_IDView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
        
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)initialize {
    self.normalIDColor = [UIColor whiteColor];
    self.normalIDFont = [UIFont mediumFontOfSize:15];
    self.specialIDColor = [UIColor colorWithHexString:@"fed74e"];
    self.specialIDFont = [UIFont boldSystemFontOfSize:16];
}

- (void)setupUI {
    [self addSubview:self.specialNumIconView];
    [self addSubview:self.IDLabel];
}

- (void)setupUIFrame {
    [self.specialNumIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.leading.mas_equalTo(self);
        make.height.mas_equalTo(20);
    }];
    
    [self.IDLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.trailing.mas_equalTo(self);
        make.leading.mas_equalTo(self.specialNumIconView.mas_trailing).offset(5);
    }];
    
//    self.IDLabel.text = @"12edfsergt";
}

- (UIImageView *)specialNumIconView {
    if (!_specialNumIconView) {
        _specialNumIconView = [UIImageView new];
        _specialNumIconView.contentMode = UIViewContentModeScaleAspectFit;
        _specialNumIconView.image = [UIImage imageNamed:@"specialNum_icon_15"];
        
    }
    return _specialNumIconView;
}

- (UILabel *)IDLabel {
    if (!_IDLabel) {
        _IDLabel = [UILabel new];
        _IDLabel.font = [UIFont mediumFontOfSize:15];
        _IDLabel.textColor = [UIColor whiteColor];
    }
    return _IDLabel;
}

- (void)setID:(NSString *)ID isSpecialNum:(BOOL)isSpecialNum {
    if ([self._id isEqualToString:ID]) {
        return;
    }
    self._id = ID;
    self.isSpecialNum = isSpecialNum;
    
    // 判断是否事靓号
    if (isSpecialNum) {
        // 是靓号
        [self addSubview:self.specialNumIconView];
        self.IDLabel.font = self.specialIDFont;
        self.IDLabel.text = [NSString stringWithFormat:@"%@", ID];
        self.IDLabel.textColor = self.specialIDColor;
        [self.specialNumIconView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(3);
            make.bottom.mas_equalTo(-3);
            make.left.mas_equalTo(0);
            make.width.mas_equalTo(self.mas_height);
        }];
        
        [self.IDLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.right.mas_equalTo(self);
            make.left.mas_equalTo(self.specialNumIconView.mas_right).offset(3);
        }];
    } else {
        // 不是靓号
        [self.specialNumIconView removeFromSuperview];
        self.IDLabel.text = [NSString stringWithFormat:@"ID:%@", ID];
        
        self.IDLabel.font = self.normalIDFont;
        self.IDLabel.textColor = self.normalIDColor;
        [self.IDLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.right.mas_equalTo(self);
            make.centerX.mas_equalTo(self);
        }];
    }
}

- (NSString *)value {
    return self._id;
}

@end
