//
//  LY_FansTagView.m
//  SimpleDate
//
//  Created by 杨宝龙 on 2020/6/8.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_FansTagView.h"

@interface LY_FansTagView ()

@property(nonatomic, strong) UIImageView *backgroundImgView;

@property(nonatomic, strong) UIImageView *levelIconView;

@property(nonatomic, strong) UILabel *levelLabel;

@property(nonatomic, strong) UILabel *nameLabel;

@end

@implementation LY_FansTagView

- (UIImageView *)backgroundImgView {
    if (!_backgroundImgView) {
        _backgroundImgView = [[UIImageView alloc] init];
    }
    return _backgroundImgView;
}

- (UIImageView *)levelIconView {
    if (!_levelIconView) {
        _levelIconView = [[UIImageView alloc] init];
    }
    return _levelIconView;
}

- (UILabel *)levelLabel {
    if (!_levelLabel) {
        _levelLabel = [[UILabel alloc] init];
        _levelLabel.textColor = [UIColor colorWithHexString:@"#CC72ED"];
        _levelLabel.font = [UIFont boldSystemFontOfSize:7];
    }
    return _levelLabel;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = [UIColor whiteColor];
        _nameLabel.font = [UIFont mediumFontOfSize:10];
    }
    return _nameLabel;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.layer.cornerRadius = self.frame.size.height / 2;
    self.layer.masksToBounds = true;
}

- (void)setupUI {
    [self addSubview:self.backgroundImgView];
    [self addSubview:self.levelIconView];
    [self addSubview:self.levelLabel];
    [self addSubview:self.nameLabel];
}

- (void)setupUIFrame {
    [self.backgroundImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
    [self.levelIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(0);
        make.width.mas_equalTo(14);
        make.height.mas_equalTo(12);
        make.centerY.mas_equalTo(0);
    }];
    [self.levelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.levelIconView.mas_centerX);
        make.centerY.mas_equalTo(0);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.levelIconView.mas_trailing).offset(2);
        make.centerY.mas_equalTo(0);
        make.trailing.mas_equalTo(-6);
    }];
}

- (void)setFansTag:(LY_FansTag *)fansTag {
    self.levelIconView.image = [UIImage imageNamed:[self getFansIcon:fansTag.level]];
    self.backgroundImgView.image = [UIImage imageNamed:[self getFansbg:fansTag.level]];
    self.nameLabel.text = fansTag.name;
    self.levelLabel.text = fansTag.level;
}

- (NSString *)getFansIcon:(NSString *)level {
    NSInteger fansLevel = [level integerValue];
    if (fansLevel >=1 && fansLevel <= 4) {
        return @"ic_zhibojian_fensixunzhang1-4";
    }else if (fansLevel>= 5 && fansLevel <=9){
        return @"ic_zhibojian_fensixunzhang5-9";
    }else{
        return @"ic_zhibojian_fensixunzhang";
    }
}

- (NSString *)getFansbg:(NSString *)level {
    NSInteger fansLevel = [level integerValue];
    if (fansLevel >=1 && fansLevel <= 4) {
        return @"bg_fensixunzhang1_10";
    }else if (fansLevel>= 5 && fansLevel <=9){
        return @"bg_fensixunzhang_11_20";
    }else{
        return @"bg_fensixunzhang_21_30";
    }
}
@end
