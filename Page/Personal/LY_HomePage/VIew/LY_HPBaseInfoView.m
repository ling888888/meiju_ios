//
//  LY_HPBaseInfoView.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/30.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_HPBaseInfoView.h"
#import "SPButton.h"
#import "LY_PortraitView.h"
#import "LY_IDView.h"
#import "LY_HPTagView.h"
#import "LY_EditInfoViewController.h"
#import "SVGA.h"
#import "ZegoKitManager.h"
#import "LiveModel.h"
#import "PetView.h"
#import "PetModel.h"
#import "PetUpgradeFuncView.h"
#import "LookPetView.h"

@interface LY_HPBaseInfoView ()

// 背景封面视图
@property(nonatomic, strong) UIImageView *backgroundImgView;

// 座驾视图
@property(nonatomic, strong) UIImageView *driverImgView;

// 头像视图
@property(nonatomic, strong) LY_PortraitView *portraitView;

//// 头饰
//@property(nonatomic, strong) UIImageView *headwearImageView;

// 头饰
@property(nonatomic, strong) SVGAImageView *headwearSVGAView;

// 直播中标签
@property(nonatomic, strong) SPButton *livingBtn;

// 编辑资料按钮
@property(nonatomic, strong) UIButton *editInformationBtn;

// 昵称
@property(nonatomic, strong) UILabel *nicknameLabel;

// 标签
@property(nonatomic, strong) LY_HPTagView *tagView;

// ID按钮
@property(nonatomic, strong) LY_IDView *idView;

// 拷贝按钮
@property(nonatomic, strong) UIButton *copyIdBtn;

// 左侧引号视图
@property(nonatomic, strong) UIImageView *leadingSymbolView;

// 简介
@property(nonatomic, strong) UILabel *synopsisLabel;

// 右侧引号视图
@property(nonatomic, strong) UIImageView *trailingSymbolView;

// 手势
@property(nonatomic, strong) UITapGestureRecognizer *tapG;

// 粘贴板对象
@property(nonatomic, strong) UIPasteboard *pasteboard;

// chongwu
@property(nonatomic, strong) PetView *petView;

// xingbie
@property(nonatomic, strong) UIImageView *petGender;

@end

@implementation LY_HPBaseInfoView

- (UIImageView *)backgroundImgView {
    if (!_backgroundImgView) {
        _backgroundImgView = [[UIImageView alloc] init];
        _backgroundImgView.contentMode = UIViewContentModeScaleAspectFill;
        _backgroundImgView.layer.masksToBounds = true;
    }
    return _backgroundImgView;
}

- (UIImageView *)driverImgView {
    if (!_driverImgView) {
        _driverImgView = [[UIImageView alloc] init];
        _driverImgView.contentMode = UIViewContentModeScaleAspectFit;
        _driverImgView.layer.masksToBounds = true;
    }
    return _driverImgView;
}

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
//        _portraitView.imageView.layer.cornerRadius = 38.5;
//        _portraitView.imageView.layer.masksToBounds = true;
//        _portraitView.imageView.layer.borderWidth = 3;
//        _portraitView.imageView.layer.borderColor = [[UIColor whiteColor] CGColor];
    }
    return _portraitView;
}

- (SVGAImageView *)headwearSVGAView {
    if (!_headwearSVGAView) {
        _headwearSVGAView = [[SVGAImageView alloc] init];
        _headwearSVGAView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _headwearSVGAView;
}

- (SPButton *)livingBtn {
    if (!_livingBtn) {
        _livingBtn = [[SPButton alloc] init];
        _livingBtn.imagePosition = SPButtonImagePositionRight;
        _livingBtn.imageTitleSpace = 5;
        _livingBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
        [_livingBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _livingBtn.titleLabel.font = [UIFont mediumFontOfSize:12];
        [_livingBtn setTitle:@"直播中".localized forState:UIControlStateNormal];
        [_livingBtn setImage:[UIImage imageNamed:@"ic_gerenzhuye_zhibozhong"] forState:UIControlStateNormal];
        NSArray *colors = @[[UIColor colorWithHexString:@"#22BBB9"], [UIColor colorWithHexString:@"#45E994"]];
        UIImage *bgImg = [[UIImage alloc] initWithGradient:colors size:CGSizeMake(60, 19) direction:UIImageGradientColorsDirectionHorizontal];
        [_livingBtn setBackgroundImage:bgImg forState:UIControlStateNormal];
        _livingBtn.layer.cornerRadius = 9.5;
        _livingBtn.layer.masksToBounds = true;
        [_livingBtn addTarget:self action:@selector(livingBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _livingBtn;
}

- (UIButton *)editInformationBtn {
    if (!_editInformationBtn) {
        _editInformationBtn = [[UIButton alloc] init];
        [_editInformationBtn setTitle:@"编辑资料".localized forState:UIControlStateNormal];
        [_editInformationBtn setTitleColor:[UIColor colorWithHexString:@"#1A1A1A"] forState:UIControlStateNormal];
        _editInformationBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 12, 0, 12);
        _editInformationBtn.titleLabel.font = [UIFont mediumFontOfSize:12];
        [_editInformationBtn setImage:[UIImage imageNamed:@"ic_gerenzhuye_bianjiziliao"] forState:UIControlStateNormal];;
        _editInformationBtn.layer.cornerRadius = 3;
        _editInformationBtn.layer.masksToBounds = true;
        _editInformationBtn.backgroundColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.05];
        [_editInformationBtn addTarget:self action:@selector(editInformationBtnAction) forControlEvents:UIControlEventTouchUpInside];
        if (kIsMirroredLayout) {
            _editInformationBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 5);
            _editInformationBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 0, -5);
        } else {
            _editInformationBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, -5);
            _editInformationBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 5);
        }
    }
    return _editInformationBtn;
}

- (UILabel *)nicknameLabel {
    if (!_nicknameLabel) {
        _nicknameLabel = [[UILabel alloc] init];
        _nicknameLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _nicknameLabel.font = [UIFont mediumFontOfSize:18];
    }
    return _nicknameLabel;
}

- (LY_HPTagView *)tagView {
    if (!_tagView) {
        _tagView = [[LY_HPTagView alloc] init];
    }
    return _tagView;
}

- (LY_IDView *)idView {
    if (!_idView) {
        _idView = [[LY_IDView alloc] init];
        _idView.normalIDColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _idView.normalIDFont = [UIFont mediumFontOfSize:12];
        _idView.specialIDFont = [UIFont mediumFontOfSize:12];
        [_idView addTarget:self action:@selector(copyIDAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _idView;
}

- (UIButton *)copyIdBtn {
    if (!_copyIdBtn) {
        _copyIdBtn = [[UIButton alloc] init];
        [_copyIdBtn setImage:[UIImage imageNamed:@"ic_gerenzhuye_fuzhi_hei"] forState:UIControlStateNormal];
        [_copyIdBtn addTarget:self action:@selector(copyIDAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _copyIdBtn;
}

- (UIImageView *)leadingSymbolView {
    if (!_leadingSymbolView) {
        _leadingSymbolView = [[UIImageView alloc] init];
        _leadingSymbolView.image = [UIImage imageNamed:@"ic_gerenzhuye_gerenjianjie_zuo"].adaptiveRtl;
    }
    return _leadingSymbolView;
}

- (UILabel *)synopsisLabel {
    if (!_synopsisLabel) {
        _synopsisLabel = [[UILabel alloc] init];
        _synopsisLabel.textColor = [UIColor colorWithHexString:@"#232426"];
        _synopsisLabel.font = [UIFont systemFontOfSize:14];
        _synopsisLabel.numberOfLines = 0;
    }
    return _synopsisLabel;
}

- (UIImageView *)trailingSymbolView {
    if (!_trailingSymbolView) {
        _trailingSymbolView = [[UIImageView alloc] init];
        _trailingSymbolView.image = [UIImage imageNamed:@"ic_gerenzhuye_gerenjianjie_you"].adaptiveRtl;
    }
    return _trailingSymbolView;
}

- (UITapGestureRecognizer *)tapG {
    if (!_tapG) {
        _tapG = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(synopsisLabelTapAction)];
    }
    return _tapG;
}

- (UIPasteboard *)pasteboard {
    if (!_pasteboard) {
      _pasteboard = [UIPasteboard generalPasteboard];
    }
    return _pasteboard;
}

- (instancetype)initWithType:(LY_HomePageType)type {
    self = [super initWithType:type];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
        [self setupDefaultData];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.backgroundImgView];
    [self addSubview:self.driverImgView];
    [self addSubview:self.portraitView];
    [self addSubview:self.headwearSVGAView];
    [self addSubview:self.livingBtn];
    [self addSubview:self.editInformationBtn];
    [self addSubview:self.nicknameLabel];
    [self addSubview:self.tagView];
    [self addSubview:self.idView];
    [self addSubview:self.copyIdBtn];
    [self addSubview:self.leadingSymbolView];
    [self addSubview:self.synopsisLabel];
    [self addSubview:self.trailingSymbolView];
    [self addSubview:self.petView];
    [self addSubview:self.petGender];
}

- (void)setupUIFrame {
    [self.backgroundImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.mas_equalTo(self);
        make.height.mas_equalTo(kScreenW/375*215);
    }];
    
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(124.5);
        make.leading.mas_equalTo(5);
        make.centerY.mas_equalTo(self.backgroundImgView.mas_bottom);
    }];
    [self.driverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(106);
        make.leading.mas_equalTo(5);
        make.bottom.mas_equalTo(self.portraitView.mas_bottom).offset(-100);
    }];
    [self.headwearSVGAView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(109, 101));
        make.centerX.mas_equalTo(self.portraitView);
        make.top.mas_equalTo(self.portraitView).offset(-17.5);
    }];
    [self.livingBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.portraitView.mas_bottom);
        make.centerX.mas_equalTo(self.portraitView);
        make.height.mas_equalTo(19);
    }];
    [self.editInformationBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.backgroundImgView.mas_bottom).offset(10);
        make.trailing.mas_equalTo(-15);
        make.height.mas_equalTo(30);
    }];
    [self.nicknameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.portraitView.mas_bottom).offset(17);
        make.leading.mas_equalTo(15);
        make.trailing.mas_equalTo(-15);
    }];
    [self.tagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nicknameLabel.mas_bottom).offset(15);
        make.leading.mas_equalTo(15);
        make.trailing.mas_equalTo(-15);
        make.height.mas_equalTo(18);
    }];
    [self.idView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.tagView.mas_bottom).offset(15);
        make.leading.mas_equalTo(15);
        make.height.mas_equalTo(15);
    }];
    [self.copyIdBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.idView);
        make.leading.mas_equalTo(self.idView.mas_trailing);
        make.size.mas_equalTo(20);
    }];
    [self.leadingSymbolView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(self.idView.mas_bottom).offset(15);
        make.size.mas_equalTo(CGSizeMake(11, 10));
    }];
    [self.synopsisLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.leadingSymbolView);
        make.leading.mas_equalTo(self.leadingSymbolView.mas_trailing).offset(15);
        make.bottom.mas_equalTo(-10);
    }];
    [self.trailingSymbolView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.synopsisLabel);
        make.trailing.mas_lessThanOrEqualTo(-15);
        make.leading.mas_equalTo(self.synopsisLabel.mas_trailing).offset(15);
        make.size.mas_equalTo(CGSizeMake(11, 10));
    }];
    [self.petGender mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(0);
        make.bottom.equalTo(self.idView.mas_bottom).offset(5);
        make.size.mas_equalTo(CGSizeMake(135, 267));
    }];
    
    [self.petView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.petGender.mas_bottom).offset(-17);
        make.trailing.mas_equalTo(-97);
        make.size.mas_equalTo(CGSizeMake(129, 102/92.0 * 129));
    }];
}

- (void)setupDefaultData {
    self.backgroundImgView.image = [UIImage imageNamed:@"img_wode_bg"];
    self.nicknameLabel.text = @"Loading...";
    self.livingBtn.hidden = true;
    
    [self.idView setID:@"000000" isSpecialNum:false];
    [self setSynopsisText:@""];
//    [self setSynopsisText:@"Loading..."];
    self.backgroundImgView.backgroundColor = [UIColor blackColor];
//    self.portraitView.image = [UIImage imageNamed:@"img_portrait_placeholder"];
    if (self.type == LY_HomePageTypeMine) {
        self.editInformationBtn.hidden = true;
    } else {
        self.editInformationBtn.hidden = true;
    }
}

- (void)setUserInfo:(LY_HomePageInfo *)userInfo {
    [super setUserInfo:userInfo];
    
    if (userInfo.bgImgUrlString.notEmpty) {
        [self.backgroundImgView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:userInfo.bgImgUrlString]] placeholderImage:[UIImage imageNamed:@"img_wode_bg"]];
    }
    
    if (userInfo.drive) {
        [self.driverImgView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:userInfo.drive.imageUrlString]]];
    }
    
    self.portraitView.portrait = userInfo.portrait;
//    [self.portraitView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:userInfo.avatar]] placeholderImage:[UIImage imageNamed:@"img_portrait_placeholder"]];
    if (userInfo.isLiving) {
        self.livingBtn.hidden = false;
    } else {
        self.livingBtn.hidden = true;
    }
    
    
    
//    if (userInfo.headwearsSvga.notEmpty) {
//        self.headwearSVGAView.imageName = [LY_Api getCompleteResourceUrl:userInfo.headwearsSvga];
//        self.headwearSVGAView.hidden = false;
//        self.headwearImageView.hidden = true;
//    } else if (userInfo.headwearsNew.notEmpty) {
//        [self.headwearImageView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:userInfo.headwearsNew]]];
//        self.headwearSVGAView.hidden = true;
//        self.headwearImageView.hidden = false;
//    } else if (userInfo.headwear.notEmpty) {
//        [self.headwearImageView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:userInfo.headwear]]];
//        self.headwearSVGAView.hidden = true;
//        self.headwearImageView.hidden = false;
//    }
    self.tagView.gender = userInfo.gender;
    self.tagView.level = userInfo.level;
    self.tagView.isAnchor = userInfo.isAnchor;
    if (userInfo.medal.notEmpty) {
        self.tagView.isNoble = true;
        self.tagView.nobleImgUrlStr = userInfo.medal;
    } else {
        self.tagView.isNoble = false;
    }
    if (userInfo.richeRank.notEmpty) {
        self.tagView.regalRank = [userInfo.richeRank integerValue];
    } else {
        self.tagView.regalRank = -1;
    }
    if (userInfo.charmRank.notEmpty) {
        self.tagView.charmRank = [userInfo.charmRank integerValue];
    } else {
        self.tagView.charmRank = -1;
    }
    self.tagView.customTag = userInfo.customTag;
    self.tagView.isVip = userInfo.isVip;
    self.tagView.isSpecialNum = false;
    self.tagView.isUserService = userInfo.isServicer;
    [self.tagView refresh];
    
    self.nicknameLabel.text = userInfo.nick_name;
    
    if (userInfo.specialNum.notEmpty) {
        [self.idView setID:userInfo.specialNum isSpecialNum:true];
    } else {
        [self.idView setID:userInfo.inviteCode isSpecialNum:false];
    }
    
    [self setSynopsisText:userInfo.synopsis];
    
    self.petGender.image = [userInfo.gender isEqualToString:@"male"] ? [UIImage imageNamed:@"img_nanshneg"] : [UIImage imageNamed:@"img_nvsheng"];

    [self requestData];
}

- (void)requestData {
    [LY_Network getRequestWithURL:LY_Api.shuxing parameters:@{@"user_id":self.userInfo._id} success:^(id  _Nullable response) {

        if (response[@"decorate"]) {
            NSArray * arr = response[@"decorate"];
            for (NSDictionary * dic in arr) {
                if ([dic[@"position"] isEqualToString:@"pet"]) {
                    PetModel * model = [PetModel initWithDictionary:dic];
                    self.petView.pet = model;
                    self.petGender.hidden = false;
                    self.petView.hidden = false;
                }
            }
        }
    } failture:^(NSError * _Nullable error) {
        [self showTips:error.localizedDescription];
    }];
}

- (void)setSynopsisText:(NSString *)synopsisText {
    if (self.type == LY_HomePageTypeMine) {
        // 判断是否为空
        if (synopsisText == nil || [synopsisText isEqualToString:@""]) {
            NSString *emptyDataText = [NSString stringWithFormat:@"%@%@", @"你还什么都没有说哦！".localized, @"点击编辑".localized];
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:emptyDataText];
            [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, emptyDataText.length)];
            [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#232426"] range:[emptyDataText rangeOfString:@"你还什么都没有说哦！".localized]];
            [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#00D4A0"] range:[emptyDataText rangeOfString:@"点击编辑".localized]];
            [attributedString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:[emptyDataText rangeOfString:@"点击编辑".localized]];
            self.synopsisLabel.attributedText = attributedString;
            
            self.synopsisLabel.userInteractionEnabled = true;
            [self.synopsisLabel addGestureRecognizer:self.tapG];
        } else {
            self.synopsisLabel.text = synopsisText;
            [self.synopsisLabel removeGestureRecognizer:self.tapG];
        }
    } else {
        // 判断是否为空
        if (synopsisText == nil || [synopsisText isEqualToString:@""]) {
            self.synopsisLabel.text = synopsisText;
            self.synopsisLabel.hidden = true;
            self.leadingSymbolView.hidden = true;
            self.trailingSymbolView.hidden = true;
        } else {
            self.synopsisLabel.text = synopsisText;
            self.synopsisLabel.hidden = false;
            self.leadingSymbolView.hidden = false;
            self.trailingSymbolView.hidden = false;
        }
    }
}

/// 编辑资料按钮点击事件
- (void)editInformationBtnAction {
    //[MobClick event:@"FM5_2_2"];
    LY_EditInfoViewController *editInfoVC = [[LY_EditInfoViewController alloc] init];
    [self.VC.navigationController pushViewController:editInfoVC animated:true];
}

/// 复制ID事件
- (void)copyIDAction {
    self.pasteboard.string = self.idView.value;
    [LY_HUD showToastTips:@"复制成功".localized];
}

/// 个人简介触发单击手势
- (void)synopsisLabelTapAction {
    LY_EditInfoViewController *editInfoVC = [[LY_EditInfoViewController alloc] init];
    editInfoVC.isPresentSynopsisAlertView = true;
    [self.VC.navigationController pushViewController:editInfoVC animated:true];
}

/// 直播中按钮点击事件
- (void)livingBtnAction {
    LiveModel * model = [[LiveModel alloc] init];
    model.userId = self.userInfo._id;
    model.liveId = self.userInfo.liveId;
    [ZegoManager joinLiveRoomWithliveInfo:model isAnchor:NO];
}

- (void)handlePetTap:(UITapGestureRecognizer *)tap {
    [LY_Network getRequestWithURL:LY_Api.shuxing parameters:@{@"user_id":self.userInfo._id} success:^(id  _Nullable response) {
        if (response[@"decorate"]) {
            NSArray * arr = response[@"decorate"];
            for (NSDictionary * dic in arr) {
                if ([dic[@"position"] isEqualToString:@"pet"]) {
                    LookPetView * fuc = [LookPetView new];
                    [fuc setDict:[NSMutableDictionary dictionaryWithDictionary:dic]];
                    [fuc present];
                    return;
                }
            }
        }
    } failture:^(NSError * _Nullable error) {
        [self showTips:error.localizedDescription];
    }];
}

- (PetView *)petView {
    if (!_petView) {
        _petView = [PetView new];
        _petView.hidden = true;
        _petView.userInteractionEnabled = YES;
        [_petView setLevelBGColor:[UIColor colorWithHexString:@"#00D8C9"]];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handlePetTap:)];
        [_petView addGestureRecognizer:tap];
    }
    return _petView;
}

- (UIImageView *)petGender {
    if (!_petGender) {
        _petGender = [UIImageView new];
        _petGender.contentMode = UIViewContentModeCenter;
        _petGender.hidden = true;
    }
    return _petGender;
}

@end
