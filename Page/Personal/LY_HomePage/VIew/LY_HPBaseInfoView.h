//
//  LY_HPBaseInfoView.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/30.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_HPBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_HPBaseInfoView : LY_HPBaseView


@end

NS_ASSUME_NONNULL_END
