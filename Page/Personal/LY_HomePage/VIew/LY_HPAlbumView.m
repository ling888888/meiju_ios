//
//  LY_HPAlbumView.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/11.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_HPAlbumView.h"
#import "LY_EditInfoViewController.h"

@interface LY_HPAlbumView () <HXPhotoViewDelegate>

@property(nonatomic, strong) UILabel *titleLabel;

@end

@implementation LY_HPAlbumView

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _titleLabel.font = [UIFont mediumFontOfSize:15];
        _titleLabel.text = @"个人相册".localized;
    }
    return _titleLabel;
}

- (HXPhotoManager *)manager {
    if (!_manager) {
        _manager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhotoAndVideo];
        _manager.configuration.saveSystemAblum = true;
        _manager.configuration.photoMaxNum = 0;
        _manager.configuration.videoMaxNum = 0;
        _manager.configuration.rowCount = 4;
        _manager.configuration.maxNum = 8;
        _manager.configuration.reverseDate = true;
        _manager.configuration.selectTogether = true;
    }
    return _manager;
}

- (void)resetManager {
    _manager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhotoAndVideo];
    _manager.configuration.saveSystemAblum = true;
    _manager.configuration.photoMaxNum = 0;
    _manager.configuration.videoMaxNum = 0;
    _manager.configuration.rowCount = 4;
    _manager.configuration.maxNum = 8;
    _manager.configuration.reverseDate = true;
    _manager.configuration.selectTogether = true;
    self.photoView.manager = _manager;
}

- (HXPhotoView *)photoView {
    if (!_photoView) {
        _photoView = [HXPhotoView photoManager:self.manager];
        _photoView.frame = CGRectMake(15, 10, kScreenW - 30, 100);
        _photoView.lineCount = 4;
        _photoView.spacing = 5;
        _photoView.previewStyle = HXPhotoViewPreViewShowStyleDark;
        _photoView.outerCamera = true;
        _photoView.hideDeleteButton = true;
        _photoView.editEnabled = false;
        _photoView.delegate = self;
        _photoView.interceptAddCellClick = true;
    }
    return _photoView;
}

- (instancetype)initWithType:(LY_HomePageType)type {
    self = [super initWithType:type];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
        [self setupDefaultData];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.titleLabel];
    [self addSubview:self.photoView];
}

- (void)setupUIFrame {
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(25);
    }];
    [self.photoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(10);
        make.leading.mas_equalTo(15);
        make.trailing.mas_equalTo(-15);
        make.bottom.mas_equalTo(-10);
        make.height.mas_equalTo(100);
    }];
}

- (void)setupDefaultData {
    if (self.type == LY_HomePageTypeMine) {
        self.photoView.showAddCell = true;
    } else {
        self.photoView.showAddCell = false;
    }
}

- (void)setUserInfo:(LY_HomePageInfo *)userInfo {
    [super setUserInfo:userInfo];
    
    [self resetManager];
    [self.manager addCustomAssetModel:userInfo.assetArray];
    [self.photoView refreshView];
}

- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal {
    
//    for (int i = 0; i < allList.count; i ++) {
//        HXPhotoSubViewCell * cell = [photoView collectionViewCellWithIndex:i];
//        cell.layer.cornerRadius = 5;
//    }
}

- (BOOL)photoView:(HXPhotoView *)photoView collectionViewShouldSelectItemAtIndexPath:(NSIndexPath *)indexPath model:(HXPhotoModel *)model {
    //[MobClick event:@"FM5_3_4"];
    return true;
}

- (void)photoView:(HXPhotoView *)photoView updateFrame:(CGRect)frame
{
    [self.photoView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(frame.size.height);
    }];
}

- (void)photoViewDidAddCellClick:(HXPhotoView *)photoView {
    LY_EditInfoViewController *editInfoVC = [[LY_EditInfoViewController alloc] init];
    editInfoVC.isGoPhotoViewController = true;
    [self.VC.navigationController pushViewController:editInfoVC animated:true];
    //[MobClick event:@"FM5_2_8"];
}

@end
