//
//  LY_HPMedalCollectionCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/18.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_HPMedalCollectionCell.h"
#import "YYWebImage.h"
#import "MedalModel.h"

@interface LY_HPMedalCollectionCell ()

@property (nonatomic, strong) YYAnimatedImageView * medalImage;
@property (nonatomic, strong) UIImageView * commonImageView;


@end

@implementation LY_HPMedalCollectionCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.medalImage = [YYAnimatedImageView new];
        [self.contentView addSubview:self.medalImage];
        [self.medalImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(0);
        }];
        self.commonImageView = [UIImageView new];
        [self.contentView addSubview:self.commonImageView];
        [self.commonImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(0);
        }];
    }
    return self;
}

- (void)setMedalModel:(MedalModel *)medalModel {
    _medalModel = medalModel;
   if ([_medalModel.cover containsString:@"webp"]) {
        if ([_medalModel.cover containsString:@"http:"]) {
            self.medalImage.yy_imageURL = [NSURL URLWithString:_medalModel.cover];
        }else{
            self.medalImage.yy_imageURL = [NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,_medalModel.cover]];
        }
       self.medalImage.hidden = NO;
       self.commonImageView.hidden = YES;
    }else{
        [self.commonImageView fp_setImageWithURLString:_medalModel.cover];
        self.medalImage.hidden = YES;
        self.commonImageView.hidden = NO;
    }
}

@end
