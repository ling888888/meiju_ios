//
//  LY_HPGuideView.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/13.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_HPGuideView.h"

@interface LY_HPGuideView ()

@property (weak, nonatomic) IBOutlet UIImageView *imgView1;

@property (weak, nonatomic) IBOutlet UIImageView *arrowImgView1;

@property (weak, nonatomic) IBOutlet UILabel *textLabel1;

@property (weak, nonatomic) IBOutlet UILabel *nextLabel1;

@property (weak, nonatomic) IBOutlet UIButton *editBtn1;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top1;


@property (weak, nonatomic) IBOutlet UIImageView *imgView2;

@property (weak, nonatomic) IBOutlet UILabel *textLabel2;

@property (weak, nonatomic) IBOutlet UILabel *nextLabel2;


@property (nonatomic, assign) NSInteger step;

@end

@implementation LY_HPGuideView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UIImage *img1 = [UIImage imageNamed:@"img_yindao_1"].adaptiveRtl;
    self.imgView1.image = img1;
    
    UIImage *img2 = [UIImage imageNamed:@"img_yindao_2"].adaptiveRtl;
    self.imgView2.image = img2;
    
    if (kIsMirroredLayout) {
        self.editBtn1.titleEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 5);
        self.editBtn1.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 0, -5);
    } else {
        self.editBtn1.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, -5);
        self.editBtn1.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 5);
    }
    
    self.nextLabel2.layer.cornerRadius = 21;
    self.nextLabel2.layer.masksToBounds = true;
    
    self.textLabel1.text = @"点击这里编辑自己的资料".localized;
    self.nextLabel1.text = @"下一步".localized;
    self.textLabel2.text = @"你上传的资料越完整，别人看到的你的主页就会越丰富".localized;
    self.nextLabel2.text = @"太棒了！".localized;
    
    
    self.textLabel1.lineBreakMode = NSLineBreakByTruncatingTail;
//    self.textLabel2.lineBreakMode = NSLineBreakByTruncatingTail;
    
    NSString *text = @"你上传的资料越完整，别人看到的你的主页就会越丰富".localized;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, text.length)];
    self.textLabel2.attributedText = attributedString;
    
    
//    if ([UIDevice currentDevice].is_iPhoneX) {
//        self.top1.constant = kScreenW/375*215-naviBarHeight;
//    } else {
        self.top1.constant = kScreenW/375*215-naviBarHeight+20;
//    }
    
    [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(action)]];

    self.imgView1.hidden = false;
    self.arrowImgView1.hidden = false;
    self.textLabel1.hidden = false;
    self.nextLabel1.hidden = false;
    self.editBtn1.hidden = false;
    
    
    self.imgView2.hidden = true;
    self.textLabel2.hidden = true;
    self.nextLabel2.hidden = true;
    
    self.step = 0;
}

- (void)action {
    if (self.step == 0) {
        self.imgView1.hidden = true;
        self.arrowImgView1.hidden = true;
        self.textLabel1.hidden = true;
        self.nextLabel1.hidden = true;
        self.editBtn1.hidden = true;
        
        self.imgView2.hidden = false;
        self.textLabel2.hidden = false;
        self.nextLabel2.hidden = false;
        self.step = 1;
    } else {
        [self removeFromSuperview];
    }
}

@end
