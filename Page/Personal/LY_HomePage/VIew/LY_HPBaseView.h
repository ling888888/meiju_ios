//
//  LY_HPBaseView.h
//  SimpleDate
//
//  Created by jianyue on 2020/5/9.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LY_HomePageInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_HPBaseView : UIView

/// 初始化方法
- (instancetype)initWithType:(LY_HomePageType)type;

/// 查看资料是：自己信息、其他人信息
@property(nonatomic, assign, readonly) LY_HomePageType type;

@property(nonatomic, strong) LY_HomePageInfo *userInfo;

@end

NS_ASSUME_NONNULL_END
