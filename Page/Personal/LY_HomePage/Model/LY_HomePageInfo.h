//
//  LY_HomePageInfo.h
//  SimpleDate
//
//  Created by jianyue on 2020/5/8.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LY_Portrait.h"
#import "LY_Album.h"
#import "LY_Drive.h"
#import "LY_Patrons.h"
#import "HXPhotoPicker.h"
#import "LY_HPTagView.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, LY_HomePageType) {
    LY_HomePageTypeMine,
    LY_HomePageTypeOthers,
};

@interface LY_HomePageInfo : NSObject

/// 用户id
@property(nonatomic, copy) NSString *_id;

/// 靓号
@property(nonatomic, copy) NSString *specialNum;

/// 头像
@property(nonatomic, copy) NSString *avatar;

/// 头像对象
@property(nonatomic, strong) LY_Portrait *portrait;

/// 自定义标签
@property(nonatomic, strong) LY_CustomTag *customTag;

/// 头饰
@property(nonatomic, copy) NSString *headwearsSvga;
@property(nonatomic, copy) NSString *headwearsNew;
@property(nonatomic, copy) NSString *headwear;

/// 背景图
@property(nonatomic, copy) NSString *bgImgUrlString;

/// 座驾
@property(nonatomic, strong) LY_Drive *drive;

/// 昵称
@property(nonatomic, copy) NSString *nick_name;

/// 生日
@property(nonatomic, copy) NSString *birthday;

/// 年龄
@property(nonatomic, copy) NSString *age;

/// 性别
@property(nonatomic, copy) NSString *gender;

/// 简介
@property(nonatomic, copy) NSString *synopsis;

/// 邀请码
@property(nonatomic, copy) NSString *inviteCode;

/// 用户等级
@property(nonatomic, assign) int level;

/// 贵族
@property(nonatomic, copy) NSString *medal;

/// 贵族
@property(nonatomic, strong) NSString *noble;

/// VIP
@property(nonatomic, assign) BOOL isVip;

/// 是否是客服助手
@property(nonatomic, assign) BOOL isServicer;

/// 土豪榜排名
@property(nonatomic, copy) NSString *richeRank;

/// 魅力榜排名
@property(nonatomic, copy) NSString *charmRank;

/// 是否是主播
@property(nonatomic, assign) BOOL isAnchor;

/// 是否正在直播
@property(nonatomic, assign) BOOL isLiving;

/// 直播间ID
@property(nonatomic, copy) NSString *liveId;

/// 直播间标题
@property(nonatomic, copy) NSString *liveTitle;

/// 你是否关注了这个主播
@property(nonatomic, assign) BOOL isFollowLiver;

/// 关注主播的数量
@property(nonatomic, assign) int followLiverNum;

/// 粉丝的数量
@property(nonatomic, assign) int fanNum;

/// 收听的人数
@property(nonatomic, assign) int listenNum;

/// 打赏榜支持的人数
@property(nonatomic, assign) int supportNum;

/// 打赏榜前三名的头像（最多三个，没有则无需返回）
@property(nonatomic, strong) NSArray<NSString *> *supportAvatars;

/// 家族id
@property(nonatomic, copy) NSString *clanId;

/// 家族名称
@property(nonatomic, copy) NSString *clanName;

/// 是否是家族国家
@property(nonatomic, assign) BOOL isCountryClan;

/// 家族身份 member：成员、deputychief：副族长、chief：族长
@property(nonatomic, copy) NSString *clanIdentity;

/// 守护榜
@property(nonatomic, strong) NSArray<LY_Patrons *> *patronsList;

/// 守护榜描述
@property(nonatomic, copy) NSString *patronDesc;

/// 本周魅力榜
@property(nonatomic, assign) NSInteger charmWeek;

/// 本周土豪值
@property(nonatomic, assign) NSInteger richeWeek;

/// 是否是好友
@property(nonatomic, assign) BOOL isFriend;

/// 是否拉入黑名单
@property(nonatomic, assign) BOOL isBlock;

/// 主页飘图片Url
@property(nonatomic, copy) NSString *homepageFalling;

/// 多媒体资源（视频、图片）
@property(nonatomic, strong) NSArray<LY_Album *> *albumList;

/// 多媒体资源（视频、图片）
@property(nonatomic, strong) NSArray<HXCustomAssetModel *> *assetArray;

/// 语音开关
@property(nonatomic, assign) BOOL audioEnable;

/// 视频开关
@property(nonatomic, assign) BOOL videoEnable;

/// 用户所在语言区
@property(nonatomic, copy) NSString *lang;

/// 财富等级
@property(nonatomic, copy) NSString *wealthLevel;

@end

NS_ASSUME_NONNULL_END
