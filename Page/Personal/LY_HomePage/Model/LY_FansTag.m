//
//  LY_FansTag.m
//  SimpleDate
//
//  Created by 杨宝龙 on 2020/6/8.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_FansTag.h"

@implementation LY_FansTag

- (instancetype)initWithName:(NSString *)name level:(NSString *)level;
{
    self = [super init];
    if (self) {
        self.name = name;
        self.level = level;
    }
    return self;
}

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"name":@"fansName",
             @"level":@"fansLevel",
             };
}
@end
