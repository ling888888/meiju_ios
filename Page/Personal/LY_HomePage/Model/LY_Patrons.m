//
//  LY_Patrons.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/11.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_Patrons.h"

@implementation LY_Patrons

- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    
    LY_Portrait *portrait = [[LY_Portrait alloc] init];
    portrait.url = dic[@"avatar"];
    portrait.headwearUrl = dic[@"headwearWebp"];
    self.portrait = portrait;
    
    return YES;
}

@end
