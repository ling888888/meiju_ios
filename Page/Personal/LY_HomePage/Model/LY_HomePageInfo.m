//
//  LY_HomePageInfo.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/8.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_HomePageInfo.h"

@implementation LY_HomePageInfo

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"isSpecialNum":@"if_special",
             @"inviteCode":@"invite_code",
             @"level":@"level.user_level",
             @"isServicer":@"is_agent_gm",
             @"synopsis":@"personal_desc",
             @"albumList":@"album",
             @"bgImgUrlString":@"cover_img",
             @"isAnchor":@"isLiver",
             @"isLiving":@"liveId",
             @"liveId":@"newLiveId",
             @"drive":@"vehicleList",
             @"clanId":@"clan_id",
             @"clanName":@"clan_name",
             @"clanIdentity":@"clan_standing",
             @"patronsList":@"patrons",
             @"isFriend":@"is_friend",
             @"isBlock":@"is_block",
             @"homepageFalling":@"packages.homepage_falling",
             @"medal":@"packages.medal",
             @"noble":@"noble_name",
             @"isVip":@"is_vip",
             @"richeRank":@"riche.rank",
             @"charmRank":@"charm.rank",
             @"audioEnable":@"audio_enable",
             @"videoEnable":@"video_enable",
             @"headwearsSvga":@"packages.headwearsSvga",
             @"headwearsNew":@"packages.headwearsNew",
             @"headwear":@"packages.headwear",
             @"customTag":@"diyLabel",
             };
}

// 声明自定义类参数类型
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
        @"albumList" : [LY_Album class],
        @"drive":[LY_Drive class],
        @"patronsList":[LY_Patrons class],
        @"customTag":[LY_CustomTag class],
    };
}

- (NSDictionary *)modelCustomWillTransformFromDictionary:(NSDictionary *)dic {
    NSMutableDictionary *newDict = [NSMutableDictionary dictionaryWithDictionary:dic];
    for (NSDictionary *dict in dic[@"vehicleList"]) {
        //isLiving
        if ([dict[@"is_drive"] boolValue] == true) {
            newDict[@"vehicleList"] = dict;
//            return newDict;
        }
    }
    NSString *liveId = (NSString *)dic[@"liveId"];
    newDict[@"liveId"] = @(liveId.notEmpty);
    return newDict;
}

- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    
    LY_Portrait *portrait = [[LY_Portrait alloc] init];
    portrait.url = dic[@"avatar"];
    portrait.headwearUrl = [dic[@"packages"] valueForKey:@"headwearWebp"];
    self.portrait = portrait;
    
    return YES;
}

- (NSArray<HXCustomAssetModel *> *)assetArray {
    if (self.albumList.count == 0) {
        return nil;
    }
    NSMutableArray<HXCustomAssetModel *> *models = [NSMutableArray array];
    for (LY_Album *album in self.albumList) {
        // 判断是图片还是视频
        if ([album.type isEqualToString:@"image"]) {
            HXCustomAssetModel *asset = [HXCustomAssetModel assetWithNetworkImageURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:album.resourceId]] selected:YES];
            [models addObject:asset];
        } else {
            NSString *filePath = [[NSBundle mainBundle] pathForResource:@"img_wodezhuye_xiangce_shiping.png" ofType:nil];
            NSURL *url = [NSURL fileURLWithPath:filePath];
            HXCustomAssetModel *asset = [HXCustomAssetModel assetWithNetworkVideoURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:album.resourceId]] videoCoverURL:url videoDuration:0 selected:YES];
            [models addObject:asset];
        }
    }
    return models;
}

@end
