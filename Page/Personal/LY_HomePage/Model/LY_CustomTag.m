//
//  LY_HPCustomTag.m
//  SimpleDate
//
//  Created by 杨宝龙 on 2020/6/8.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_CustomTag.h"

@implementation LY_CustomTag

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"url":[NSString stringWithFormat:@"%@Url", [SPDCommonTool getLanguageStrPostToServer:[SPDCommonTool getFamyLanguage]]],
             };
}

@end
