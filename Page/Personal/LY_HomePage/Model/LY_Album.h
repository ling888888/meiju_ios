//
//  LY_Album.h
//  SimpleDate
//
//  Created by jianyue on 2020/5/9.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_Album : NSObject

/// ID
@property(nonatomic, copy) NSString *_id;

/// image, video
@property(nonatomic, copy) NSString *type;

/// 资源ID
@property(nonatomic, copy) NSString *resourceId;

///
@property(nonatomic, copy) NSString *content;

@end

NS_ASSUME_NONNULL_END
