//
//  LY_Album.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/9.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_Album.h"

@implementation LY_Album

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"type":@"res_type",
             @"resourceId":@"res_id",
             };
}

@end
