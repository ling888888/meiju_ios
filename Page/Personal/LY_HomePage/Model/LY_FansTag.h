//
//  LY_FansTag.h
//  SimpleDate
//
//  Created by 杨宝龙 on 2020/6/8.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_FansTag : NSObject

@property(nonatomic, copy) NSString *name;

@property(nonatomic, copy) NSString *level;


- (instancetype)initWithName:(NSString *)name level:(NSString *)level;

@end

NS_ASSUME_NONNULL_END
