//
//  LY_Patrons.h
//  SimpleDate
//
//  Created by jianyue on 2020/5/11.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_Patrons : NSObject

/// 用户ID
@property(nonatomic, copy) NSString *_id;

/// 用户头像
@property(nonatomic, copy) NSString *avatar;

@property(nonatomic, strong) LY_Portrait *portrait;

/// 排序
@property(nonatomic, assign) NSString* rank;

@property(nonatomic, copy) NSString *headwear;

@property(nonatomic, copy) NSString *headwearsNew;

@property(nonatomic, copy) NSString *headwearsSvga;

@end

NS_ASSUME_NONNULL_END
