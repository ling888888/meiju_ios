//
//  LY_Drive.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/9.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_Drive.h"

@implementation LY_Drive

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"imageUrlString":@"image",
             @"isDrive":@"is_drive",
             @"vehicleId":@"vehicle_id",
             };
}

@end
