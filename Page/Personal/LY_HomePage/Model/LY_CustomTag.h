//
//  LY_CustomTag.h
//  SimpleDate
//
//  Created by 杨宝龙 on 2020/6/8.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_CustomTag : NSObject

@property(nonatomic, copy) NSString *url;

// 0横向，1圆形图标
@property(nonatomic, copy) NSString *labelType;

@property(nonatomic, copy) NSString *enUrl;

@property(nonatomic, copy) NSString *zhUrl;

@property(nonatomic, copy) NSString *arUrl;

@end

NS_ASSUME_NONNULL_END
