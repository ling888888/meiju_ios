//
//  LY_Drive.h
//  SimpleDate
//
//  Created by jianyue on 2020/5/9.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_Drive : NSObject

// 过期时间 - expire_time

/// 座驾图片
@property(nonatomic, copy) NSString *imageUrlString;

/// 是否穿戴
@property(nonatomic, assign) BOOL isDrive;

/// 座驾名称
@property(nonatomic, copy) NSString *name;

/// ID
@property(nonatomic, copy) NSString *vehicleId;

@end

NS_ASSUME_NONNULL_END
