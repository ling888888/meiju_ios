//
//  MedalModel.h
//  SimpleDate
//
//  Created by ling Hou on 2020/9/17.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "CommonModel.h"


NS_ASSUME_NONNULL_BEGIN

@class MedalItemModel;

@interface MedalModel : CommonModel

@property (nonatomic, copy) NSString * name;
@property (nonatomic, assign) BOOL flag;

@property (nonatomic, copy) NSString * cover;
@property (nonatomic, assign) NSInteger level; // [-1 ,1, 2 ,3]
@property (nonatomic, strong) NSMutableArray<MedalItemModel *> * items;
@property (nonatomic, copy) NSString * expire_time_str;
@property (nonatomic, assign) double expire_time;


@end


@interface MedalItemModel : CommonModel

@property (nonatomic, copy) NSString * name;
@property (nonatomic, copy) NSString * cover;
@property (nonatomic, copy) NSString * explain;
@property (nonatomic, assign) BOOL flag;
@property (nonatomic, assign) NSInteger score;
@property (nonatomic, assign) NSInteger mScore;
@property (nonatomic, assign) double create_time;
@property (nonatomic, assign) double expire_time;

@property (nonatomic, copy) NSString * create_time_str;

@property (nonatomic, copy) NSString * expire_time_str;

@end

NS_ASSUME_NONNULL_END
