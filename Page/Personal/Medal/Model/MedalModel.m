//
//  MedalModel.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/17.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "MedalModel.h"

@implementation MedalModel

- (void)setValue:(id)value forKey:(NSString *)key {
    if ([key isEqualToString:@"cover_url"]) {
        NSArray * array = value;
        for (NSDictionary * dict in array) {
            MedalItemModel * model = [MedalItemModel initWithDictionary:dict];
            model.expire_time = [dict[@"expire_time"] doubleValue];
            [self.items addObject:model];
        }
    }else{
        [super setValue:value forKey:key];
    }
}

- (NSMutableArray<MedalItemModel *> *)items {
    if (!_items) {
        _items = [NSMutableArray new];
    }
    return _items;
}

- (void)setExpire_time:(double)expire_time {
    _expire_time = expire_time;
    if (expire_time == -1) {
        self.expire_time_str = @"永久".localized;
    }else{
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setTimeZone:[NSTimeZone localTimeZone]];
        [format setDateFormat:@"yyyy/MM/dd"];
        NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:_expire_time/ 1000];
        self.expire_time_str = [NSString stringWithFormat:SPDStringWithKey(@"%@到期", nil), [format stringFromDate:date]];
    }
}


@end

@implementation MedalItemModel

- (void)setCreate_time:(double)create_time {
    _create_time = create_time;
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setTimeZone:[NSTimeZone localTimeZone]];
    [format setDateFormat:@"yyyy/MM/dd"];
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:_create_time/ 1000];
    self.create_time_str = [NSString stringWithFormat:SPDStringWithKey(@"%@获得", nil), [format stringFromDate:date]];
}

- (void)setExpire_time:(double)expire_time {
    _expire_time  = expire_time;
    if (expire_time == -1) {
        self.expire_time_str = @"永久".localized;
    }else{
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setTimeZone:[NSTimeZone localTimeZone]];
        [format setDateFormat:@"yyyy/MM/dd"];
        NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:_expire_time/ 1000];
        self.expire_time_str = [NSString stringWithFormat:SPDStringWithKey(@"%@到期", nil), [format stringFromDate:date]];
    }
}

@end
