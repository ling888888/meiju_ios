//
//  MedalListController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/17.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "MedalListController.h"
#import "MedalListItemController.h"
#import "MedalModel.h"

@interface MedalListController ()

@property (nonatomic, strong) NSArray * titleArray;
@property (nonatomic, strong) MedalListItemController * achievementVC; // 成就
@property (nonatomic, strong) MedalListItemController * activityVC; // 成就
@property (nonatomic, strong) UIImageView * background;

@end

@implementation MedalListController

- (void)viewDidLoad {

    self.navigationItem.title = self.name.notEmpty && ![self.userId isEqualToString:[SPDApiUser currentUser].userId] ?  [NSString stringWithFormat:@"%@的勋章墙".localized,_name]: @"我的勋章墙".localized;
    self.titleColorNormal = [[UIColor colorWithHexString:@"FFFFFF"] colorWithAlphaComponent:0.5];
    self.titleColorSelected = [UIColor colorWithHexString:@"FFFFFF"];
    self.titleSizeNormal = 18;
    self.titleSizeSelected = 18;
    self.menuView.fontName = @"PingFangSC-Medium";;
    self.menuView.style = WMMenuViewStyleLine;
   
    self.menuItemWidth = kScreenW / 2;
    
    [super viewDidLoad];
    
    self.menuView.progressViewBottomSpace = 10;
    self.progressWidth = 85;
    self.menuView.progressHeight = 2;
    self.menuView.lineColor = [[UIColor colorWithHexString:@"#00D4A0"] colorWithAlphaComponent:0.2];

    [self reloadData];
    
    [self.view insertSubview:self.background atIndex:0];
    [self.background mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    
    // 分割线
    UIView * lineView = [UIView new];
    lineView.backgroundColor = [UIColor whiteColor];
    [self.menuView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(1, 20));
        make.centerX.mas_equalTo(0);
        make.centerY.mas_equalTo(0);
    }];
    
    // 背景光线
    UIImageView * bgImageView = [UIImageView new];
    bgImageView.image = [UIImage imageNamed:@"背景光线"].adaptiveRtl;
    [self.view addSubview:bgImageView];
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(253, 259));
    }];
    
    self.menuView.scrollView.backgroundColor = [UIColor clearColor];
    self.scrollView.backgroundColor = [UIColor clearColor];
    
    [self requestList];
    [self requestActiveList];
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithHexString:@"ffffff"];
    [self.navigationController.navigationBar vhl_setBackgroundColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar vhl_setShadowImageHidden:true];
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:0];
    self.navigationController.navigationBar.titleTextAttributes = @{
        NSFontAttributeName:[UIFont mediumFontOfSize:18],
        NSForegroundColorAttributeName:[UIColor colorWithHexString:@"ffffff"]
    };
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithHexString:@"#1A1A1A"];
    [self.navigationController.navigationBar vhl_setBackgroundColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar vhl_setShadowImageHidden:true];
    self.navigationController.navigationBar.titleTextAttributes = @{
        NSFontAttributeName:[UIFont mediumFontOfSize:18],
        NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#1A1A1A"]
    };
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;

}

#pragma mark - request

- (void)requestList {
    [RequestUtils GET:URL_Server(@"user.detail.medal") parameters:@{@"user_id":self.userId,@"type":@"achievement"} success:^(id  _Nullable suceess) {
        NSMutableArray * dataArray = [NSMutableArray new];
        for (NSDictionary * dic in suceess[@"list"]) {
            MedalModel * model = [MedalModel initWithDictionary:dic];
            [dataArray  addObject:model];
        }
        if (dataArray.count == 0) {
            MedalModel * model = [MedalModel new];
            model.cover = @"https://famy.17jianyue.cn/ic_xunzhang_moren%402x.png";
            model.name = @"敬请期待".localized;
            [dataArray addObject:model];
        }
        self.achievementVC.dataArray = [dataArray mutableCopy];
        
    } failure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestActiveList {
    [RequestUtils GET:URL_Server(@"user.detail.medal") parameters:@{@"user_id":self.userId,@"type":@"active"} success:^(id  _Nullable suceess) {
        
        NSMutableArray * activityArray = [NSMutableArray new];
        for (NSDictionary * dic in suceess[@"list"]) {
            MedalModel * model = [MedalModel initWithDictionary:dic];
            [activityArray  addObject:model];
        }
        if ( activityArray.count == 0) {
            MedalModel * model = [MedalModel new];
            model.cover = @"https://famy.17jianyue.cn/ic_xunzhang_moren%402x.png";
            model.name = @"敬请期待".localized;
            [activityArray addObject:model];
        }
        self.activityVC.dataArray = [activityArray mutableCopy];
        
    } failure:^(id  _Nullable failure) {
        
    }];
}

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.titleArray.count;
}

- (__kindof UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    return index == 0 ? self.achievementVC : self.activityVC;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    NSString *title = self.titleArray[index];
    return title.localized;
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    return CGRectMake(0, naviBarHeight,kScreenW, 60);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    return CGRectMake(0, 60 + naviBarHeight , kScreenW, kScreenH - 60 - naviBarHeight - IphoneX_Bottom);
}


#pragma mark - getters

- (NSArray *)titleArray {
    if (!_titleArray) {
        _titleArray = @[@"成就勋章",@"活动勋章"];
    }
    return _titleArray;
}

- (MedalListItemController *)achievementVC {
    if (!_achievementVC) {
        _achievementVC = [MedalListItemController new];
        _achievementVC.userId = self.userId;
    }
    return _achievementVC;
}

- (MedalListItemController *)activityVC {
    if (!_activityVC) {
        _activityVC = [MedalListItemController new];
        _activityVC.userId = self.userId;
    }
    return _activityVC;
}

- (UIImageView *)background {
    if (!_background) {
        _background = [UIImageView new];
        _background.image = [UIImage imageNamed:@"img_wodexunzhang_bg_xiewen"];
    }
    return _background;
}

@end
