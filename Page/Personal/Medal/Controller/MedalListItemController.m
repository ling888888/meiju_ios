//
//  MedalListItemController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/17.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "MedalListItemController.h"
#import "MedalModel.h"
#import "MedalListCell.h"
#import "MedalLevelViewController.h"

@interface MedalListItemController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView * collectionView;
@property (nonatomic, strong) UIImageView * bgImageView;

@end

@implementation MedalListItemController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.backgroundColor = SPD_HEXCOlOR(@"121424");
    self.view.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.bgImageView];
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 14, 31+IphoneX_Bottom, 14));
    }];
    
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(14, 0, 31 + IphoneX_Bottom + 1, 0));
    }];
}

- (void)setDataArray:(NSMutableArray *)dataArray {
    _dataArray = dataArray;
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MedalListCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MedalListCell" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    [cell bindCurrentIndex:indexPath.row allCount:self.dataArray.count];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    MedalModel * model = self.dataArray[indexPath.row];
    if (model.items.count == 0) {
        return;
    }
    MedalLevelViewController * vc = [MedalLevelViewController new];
    vc.currentIndex = model.level;
    vc.userId = self.userId;
    vc.medalArray = [model.items mutableCopy];
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma  mark - getters

- (UIImageView *)bgImageView {
    if (!_bgImageView) {
        _bgImageView = [UIImageView new];
        _bgImageView.image = [UIImage imageNamed:@"img_wodexunzhang_kuang_bg"];
    }
    return _bgImageView;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout new];
        layout.sectionInset = UIEdgeInsetsMake(0, 14, 0, 14);
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        CGFloat width = (kScreenW - 2* 14)/2.0f;
        layout.itemSize = CGSizeMake(width, 185.0f/174*width);
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor clearColor];
        [_collectionView registerClass:[MedalListCell class] forCellWithReuseIdentifier:@"MedalListCell"];
    }
    return _collectionView;
}

@end
