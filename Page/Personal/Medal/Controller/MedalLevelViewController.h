//
//  MedalLevelViewController.h
//  SimpleDate
//
//  Created by ling Hou on 2020/9/17.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MedalLevelViewController : UIViewController

@property (nonatomic, assign) NSInteger currentIndex;

@property (nonatomic, strong) NSMutableArray * medalArray;

@property (nonatomic, copy) NSString * userId;

@end

NS_ASSUME_NONNULL_END
