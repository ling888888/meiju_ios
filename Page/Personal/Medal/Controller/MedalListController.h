//
//  MedalListController.h
//  SimpleDate
//
//  Created by ling Hou on 2020/9/17.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WMPageController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MedalListController : WMPageController

@property (nonatomic, copy) NSString * userId;
@property (nonatomic, copy) NSString * name;// 查看某个用户的名字

@end

NS_ASSUME_NONNULL_END
