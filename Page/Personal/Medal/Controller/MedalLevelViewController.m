//
//  MedalLevelViewController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/17.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "MedalLevelViewController.h"
#import "MedalLevelCell.h"
#import "MedalLevelProcessCell.h"

@interface MedalLevelViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate>

@property (nonatomic, strong) UICollectionView * collectionView;
@property (nonatomic, strong) UICollectionView * progressCollectionView;
@property (nonatomic, strong) UIImageView * bgImageView;
@property (nonatomic, strong) UIImageView * background;

@end

@implementation MedalLevelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = SPD_HEXCOlOR(@"111423");
    
    // 背景
    [self.view insertSubview:self.background atIndex:0];
    [self.background mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    
    // 背景光线
    UIImageView * bgImageView = [UIImageView new];
    bgImageView.image = [UIImage imageNamed:@"背景光线"].adaptiveRtl;
    [self.view addSubview:bgImageView];
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(253, 259));
    }];
    
    // collection
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    
    // 退出按钮
    UIButton * exitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [exitButton setImage:[UIImage imageNamed:@"ic_tongyong_guanbi"] forState:UIControlStateNormal];
    [self.view addSubview:exitButton];
    [exitButton addTarget:self action:@selector(handleExitButton:) forControlEvents:UIControlEventTouchUpInside];
    [exitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(34);
        make.leading.mas_equalTo(15);
        make.size.mas_equalTo(50);
    }];
    
    // progress
    [self.view addSubview:self.progressCollectionView];
    [self.progressCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(57);
        make.width.mas_equalTo(3*80);
        make.centerX.mas_equalTo(kIsMirroredLayout ? (self.medalArray.count == 2 ? -60: -20):(self.medalArray.count == 2 ? 60: 20));
        make.bottom.mas_equalTo(-100);
    }];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
}

- (void)setMedalArray:(NSMutableArray *)medalArray {
    _medalArray = medalArray;
    [self.collectionView reloadData];
    if (self.currentIndex != -1) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self collectionViewScrollTo:_currentIndex -1];
        });
    }else{
        [self.progressCollectionView reloadData];
    }
}

- (void)collectionViewScrollTo:(NSInteger)index {
    if (index < self.medalArray.count) {
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
           [self.progressCollectionView reloadData];
    }
}

- (void)handleExitButton:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == self.collectionView) {
        return self.medalArray.count;
    }else{
        return self.medalArray.count == 1 ? 0 : self.medalArray.count;
    }
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.collectionView) {
        MedalLevelCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MedalLevelCell" forIndexPath:indexPath];
        cell.userId = self.userId;
        cell.model = self.medalArray[indexPath.row];
        return cell;
    }else{
        MedalLevelProcessCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MedalLevelProcessCell" forIndexPath:indexPath];
        cell.index = (self.medalArray.count == 2)?  indexPath.row + 1 : indexPath.row;
        cell.lineImageView.hidden = (self.medalArray.count - 1 == indexPath.row);
        if (kIsMirroredLayout) {
            cell.show = ((self.medalArray.count - 1 - (self.collectionView.contentOffset.x < 0 ? self.medalArray.count -1 : (self.collectionView.contentOffset.x / kScreenW))) == indexPath.row);
        }else{
            cell.show = ((self.collectionView.contentOffset.x / kScreenW) == indexPath.row);
        }
        return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.progressCollectionView) {
        [self collectionViewScrollTo:indexPath.row];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {    // called when scroll view grinds to a halt
    if (scrollView == self.collectionView) {
        [self.progressCollectionView reloadData];
    }
}

#pragma mark - getters

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout new];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        layout.itemSize = CGSizeMake(kScreenW, kScreenH);
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.pagingEnabled = YES;
        _collectionView.backgroundColor = [UIColor clearColor];
        [_collectionView registerClass:[MedalLevelCell class] forCellWithReuseIdentifier:@"MedalLevelCell"];
    }
    return _collectionView;
}

- (UICollectionView *)progressCollectionView {
    if (!_progressCollectionView) {
        UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout new];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        layout.itemSize = CGSizeMake(80, 57);
        _progressCollectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _progressCollectionView.delegate = self;
        _progressCollectionView.dataSource = self;
        _progressCollectionView.pagingEnabled = YES;
        _progressCollectionView.backgroundColor = [UIColor clearColor];
        [_progressCollectionView registerNib:[UINib nibWithNibName:@"MedalLevelProcessCell" bundle:nil] forCellWithReuseIdentifier:@"MedalLevelProcessCell"];
    }
    return _progressCollectionView;
}

- (UIImageView *)background {
    if (!_background) {
        _background = [UIImageView new];
        _background.image = [UIImage imageNamed:@"img_wodexunzhang_bg_xiewen"];
    }
    return _background;
}

@end
