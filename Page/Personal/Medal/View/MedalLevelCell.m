//
//  MedalLevelCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/17.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "MedalLevelCell.h"
#import "YYWebImage.h"

@interface MedalLevelCell ()

@property (nonatomic, strong) YYAnimatedImageView * iconImageView; // 图标
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * descLabel; // ** 获得
@property (nonatomic, strong) UILabel * timeLabel; // 到期
@property (nonatomic, strong) UILabel * contentLabel;//获得榜单第一名1次获得

@end

@implementation MedalLevelCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubview];
    }
    return self;
}

- (void)initSubview {
    
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.centerY.mas_equalTo(-20);
    }];
    
    [self.contentView addSubview:self.iconImageView];
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.bottom.equalTo(self.titleLabel.mas_top).offset(-22);
        make.size.mas_equalTo(200.0f/ 375 * kScreenW);
    }];
    
    [self.contentView addSubview:self.descLabel];
    [self.descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(14);
    }];
    
    [self.contentView addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(self.descLabel.mas_bottom).offset(5);
    }];

    [self.contentView addSubview:self.contentLabel];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(self.timeLabel.mas_bottom).offset(23);
        make.leading.mas_equalTo(30);
        make.trailing.mas_equalTo(-30);
    }];
    
}

- (void)setModel:(MedalItemModel *)model {
    _model = model;
    if ([_model.cover containsString:@"webp"]) {
        if ([_model.cover containsString:@"http:"]) {
            [self.iconImageView yy_setImageWithURL:[NSURL URLWithString:_model.cover] placeholder:[UIImage imageNamed:@"icon_noble_vip_4_0"]];
        }else{
            [self.iconImageView yy_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,_model.cover]] placeholder:[UIImage imageNamed:@"icon_noble_vip_4_0"]];
        }
    }else{
        [self.iconImageView fp_setImageWithURLString:_model.cover];
    }
    self.titleLabel.text = _model.name;
    if (_model.flag) {
        self.timeLabel.hidden = NO;
        self.timeLabel.text = _model.expire_time_str;
        self.descLabel.text = _model.create_time_str;
    }else{
        self.timeLabel.hidden = YES;
        if ([self.userId isEqualToString:[SPDApiUser currentUser].userId]) {
            self.descLabel.text = [NSString stringWithFormat:@"未获得 当前%d/%d".localized, _model.mScore,_model.score];
        }else{
            self.descLabel.text = @"未获得".localized;
        }
    }
    self.contentLabel.text = _model.explain;
    
}

- (YYAnimatedImageView *)iconImageView {
    if (!_iconImageView) {
        _iconImageView = [YYAnimatedImageView new];
    }
    return _iconImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.textColor = SPD_HEXCOlOR(@"ffffff");
        _titleLabel.font = [UIFont boldSystemFontOfSize:18];
    }
    return _titleLabel;
}

- (UILabel *)descLabel {
    if (!_descLabel) {
        _descLabel = [UILabel new];
        _descLabel.textColor = [SPD_HEXCOlOR(@"ffffff") colorWithAlphaComponent:0.6];
        _descLabel.font = [UIFont boldSystemFontOfSize:12];
    }
    return _descLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [UILabel new];
        _timeLabel.textColor = [SPD_HEXCOlOR(@"ffffff") colorWithAlphaComponent:0.6];
        _timeLabel.font = [UIFont boldSystemFontOfSize:12];
    }
    return _timeLabel;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [UILabel new];
        _contentLabel.textColor = [SPD_HEXCOlOR(@"ffffff") colorWithAlphaComponent:0.6];
        _contentLabel.font = [UIFont boldSystemFontOfSize:16];
        _contentLabel.numberOfLines = 0;
        _contentLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _contentLabel;
}

@end
