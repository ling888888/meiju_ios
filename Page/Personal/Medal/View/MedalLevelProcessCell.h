//
//  MedalLevelProcessCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/9/17.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MedalLevelProcessCell : UICollectionViewCell

@property (nonatomic, assign) NSInteger index;

@property (nonatomic, assign) BOOL show;

@property (weak, nonatomic) IBOutlet UIImageView *lineImageView;


@end

NS_ASSUME_NONNULL_END
