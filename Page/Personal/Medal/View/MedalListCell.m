//
//  MedalListCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/17.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "MedalListCell.h"
#import "MedalModel.h"
#import "YYWebImage.h"

@interface MedalListCell ()

@property (nonatomic, strong) YYAnimatedImageView * iconImageView;
@property (nonatomic, strong) UIImageView * commonImageView;

@property (nonatomic, strong) UILabel * titleLabel; // title
@property (nonatomic, strong) UILabel * timeLabel; // &*** 到期
@property (nonatomic, strong) UIImageView * topLine;
@property (nonatomic, strong) UIImageView * rightLine;
@property (nonatomic, strong) UIImageView * bottomLine;

@end

@implementation MedalListCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews {
    CGFloat width = (kScreenW - 14)/2.0f;
    CGFloat ratio = width*1.0f/174;
    
    [self.contentView addSubview:self.iconImageView];
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(kScreenW < 375 ? 110.0f/375*kScreenW :110.0f);
        make.top.mas_equalTo(ratio * 31);
    }];
    [self.contentView addSubview:self.commonImageView];
    [self.commonImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(kScreenW < 375 ? 110.0f/375*kScreenW :110.0f);
        make.top.mas_equalTo(ratio * 31);
    }];
    
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(self.iconImageView.mas_bottom).offset(5 * ratio);
    }];
    
    [self.contentView addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(5 * ratio);
    }];
    
//    [self.contentView addSubview:self.topLine];
//    [self.topLine mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(-0.5);
//        make.width.mas_equalTo(self);
//        make.height.mas_equalTo(1);
//    }];
    [self.contentView addSubview:self.bottomLine];
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(0.5);
        make.width.mas_equalTo(self);
        make.height.mas_equalTo(1);
        make.centerX.mas_equalTo(0);
    }];
    [self.contentView addSubview:self.rightLine];
    [self.rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(self);
        make.trailing.mas_equalTo(0.5);
        make.bottom.mas_equalTo(0);
    }];
}

- (void)setModel:(MedalModel *)model {
    _model = model;
    
    if ([_model.cover containsString:@"webp"]) {
        if ([_model.cover containsString:@"http:"]) {
            self.iconImageView.yy_imageURL = [NSURL URLWithString:_model.cover];
        }else{
            self.iconImageView.yy_imageURL = [NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,_model.cover]];
        }
        self.iconImageView.hidden = NO;
        self.commonImageView.hidden = YES;
    }else{
        [self.commonImageView fp_setImageWithURLString:_model.cover];
        self.commonImageView.hidden = NO;
        self.iconImageView.hidden = YES;


    }
    self.titleLabel.text = _model.name;
    if (_model.flag) {
        self.timeLabel.hidden = NO;
        self.timeLabel.text = _model.expire_time_str;
    }else{
        self.timeLabel.hidden = YES;
    }
    
}

- (void)bindCurrentIndex:(NSInteger)index allCount:(NSInteger)allCount {
    if (allCount% 2== 0) {
        self.bottomLine.hidden = (index == allCount -1 || index == allCount - 2);
    }else{
        self.bottomLine.hidden = (index == allCount -1);
    }
    self.rightLine.hidden = (index %2 != 0);
    self.bottomLine.image = (index %2 == 0) ? [UIImage imageNamed:@"img_xunzhang_hengtiao_zuo"].adaptiveRtl:[UIImage imageNamed:@"img_xunzhang_hengtiao_you"].adaptiveRtl;
    if (index == 0) {
        self.rightLine.image = [UIImage imageNamed:@"img_xunzhang_shutiao_shang"];
    }else if (index == allCount - 1){
        self.rightLine.image = [UIImage imageNamed:@"img_xunzhang_shutiao_xia"];
    }else{
        self.rightLine.image = [UIImage imageNamed:@"img_xunzhang_shutiao_xia_2"];
    }
    
}

#pragma mark - getters

- (YYAnimatedImageView *)iconImageView {
    if (!_iconImageView) {
        _iconImageView = [YYAnimatedImageView new];
    }
    return _iconImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.textColor = SPD_HEXCOlOR(@"#FFFFFF");
        _titleLabel.font = [UIFont boldSystemFontOfSize:15];
    }
    return _titleLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [UILabel new];
        _timeLabel.textColor = [SPD_HEXCOlOR(@"#FFFFFF") colorWithAlphaComponent:0.6];
        _timeLabel.font = [UIFont boldSystemFontOfSize:10];
    }
    return _timeLabel;
}

- (UIImageView *)topLine {
    if (!_topLine) {
        _topLine = [UIImageView new];
    }
    return _topLine;
}

- (UIImageView *)rightLine {
    if (!_rightLine) {
        _rightLine = [UIImageView new];
//        _rightLine.backgroundColor = [UIColor cyanColor];
    }
    return _rightLine;
}

- (UIImageView *)bottomLine {
    if (!_bottomLine) {
        _bottomLine = [UIImageView new];
//        _bottomLine.backgroundColor = [UIColor redColor];
    }
    return _bottomLine;
}

- (UIImageView *)commonImageView {
    if (!_commonImageView) {
        _commonImageView = [UIImageView new];
    }
    return _commonImageView;
}

@end
