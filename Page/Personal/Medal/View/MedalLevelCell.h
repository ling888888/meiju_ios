//
//  MedalLevelCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/9/17.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedalModel.h"

NS_ASSUME_NONNULL_BEGIN


@interface MedalLevelCell : UICollectionViewCell

@property (nonatomic, strong) MedalItemModel * model;

@property (nonatomic, strong) NSString * userId;

@end

NS_ASSUME_NONNULL_END
