//
//  MedalLevelProcessCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/17.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "MedalLevelProcessCell.h"

@interface MedalLevelProcessCell ()
@property (weak, nonatomic) IBOutlet UIImageView *lightImageView;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;

@end


@implementation MedalLevelProcessCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (void)setIndex:(NSInteger)index {
    _index = index;
    self.iconImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_xunzhang_xiangqing_%@",@(index)]];
}

- (void)setShow:(BOOL)show {
    _show = show;
    self.lightImageView.hidden = !_show;
    self.arrowImageView.hidden = !_show;
}

@end
