//
//  GoodBubbleCell.h
//  SimpleDate
//
//  Created by Monkey on 2019/4/24.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GoodModel;

NS_ASSUME_NONNULL_BEGIN

@interface GoodBubbleCell : UICollectionViewCell

@property (nonatomic, assign) BOOL is_mine;

@property (nonatomic , strong) GoodModel * model;

@property (nonatomic, assign) BOOL isSelected;


@end

NS_ASSUME_NONNULL_END
