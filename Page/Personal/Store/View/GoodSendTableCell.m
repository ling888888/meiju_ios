//
//  GoodSendTableCell.m
//  SimpleDate
//
//  Created by Monkey on 2019/6/3.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "GoodSendTableCell.h"
//#import "SPDCustomView.h"
#import "HomeModel.h"

@interface GoodSendTableCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
@property (weak, nonatomic) IBOutlet UIButton *genderAgeBtn;
@property (weak, nonatomic) IBOutlet UIButton *levelBtn;

@end

@implementation GoodSendTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.sendBtn setTitle:SPDStringWithKey(@"赠送", nil) forState:UIControlStateNormal];
}

- (void)setModel:(HomeModel *)model {
    _model = model;
    self.nameLabel.text = _model.nick_name;
    [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _model.avatar]]];
    
    if ([_model.gender isEqualToString:@"female"]) {
        [self.genderAgeBtn setBackgroundColor:[UIColor colorWithHexString:@"ff7eca"]];
        [self.genderAgeBtn setImage:[UIImage imageNamed:@"chatroomuser_female"] forState:UIControlStateNormal];
    } else if ([_model.gender isEqualToString:@"male"]) {
        [self.genderAgeBtn setBackgroundColor:[UIColor colorWithHexString:@"5db4fd"]];
        [self.genderAgeBtn setImage:[UIImage imageNamed:@"chatroomuser_male"] forState:UIControlStateNormal];
    }
    [self.genderAgeBtn setTitle:[NSString stringWithFormat:@"%@", _model.age] forState:UIControlStateNormal];
    [SPDCommonTool configDataForLevelBtnWithLevel:[_model.level intValue] andUIButton:self.levelBtn];

}

- (IBAction)sendBtnClicked:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedGoodSendTableCellWith:)]) {
        [self.delegate clickedGoodSendTableCellWith:self.model];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
