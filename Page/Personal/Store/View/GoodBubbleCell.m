//
//  GoodBubbleCell.m
//  SimpleDate
//
//  Created by Monkey on 2019/4/24.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "GoodBubbleCell.h"
#import "GoodModel.h"
@interface GoodBubbleCell ()

@property (weak, nonatomic) IBOutlet UIImageView *goodImageView;
@property (weak, nonatomic) IBOutlet UILabel *goodNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *goodTypeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *goldImageView;
@property (weak, nonatomic) IBOutlet UILabel *goldNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *tagLabel;
@property (weak, nonatomic) IBOutlet UIImageView *equippedImageView;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIView *makeBorderView;
@property (weak, nonatomic) IBOutlet UILabel *expireTimeLabel;
@end

@implementation GoodBubbleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.tagLabel.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5,5)];//圆角大小
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.tagLabel.bounds;
    maskLayer.path = maskPath.CGPath;
    self.tagLabel.layer.mask = maskLayer;
}

- (void)setModel:(GoodModel *)model {
    _model = model;
    [self.goodImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:_model.cover]];
    self.goodNameLabel.text = _model.good_name;
    self.goodNameLabel.font = FONT_SIZE(14);
    self.goodTypeLabel.hidden = self.is_mine;
    self.goodTypeLabel.text = SPDStringWithKey(@"气泡", nil);
    
    self.goldImageView.hidden = ([model.price_origin integerValue] == -1 && [model.price_renew integerValue] == -1);
    self.goldNumLabel.hidden = ([model.price_origin integerValue] == -1 && [model.price_renew integerValue] == -1);
    self.goldNumLabel.font = FONT_SIZE(12);
    self.descLabel.hidden = !([model.price_origin integerValue] == -1 && [model.price_renew integerValue] == -1);
    BOOL useGoldBuy = [model.pay_method isEqualToString:@"gold"];
    self.goldImageView.image = [UIImage imageNamed:useGoldBuy ? @"ic_store_gold":@"ic_store_diamonds"];
    NSNumber * cost = _model.owned ? _model.price_renew : _model.price_origin;
    self.goldNumLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"%@／%@天", nil),cost,_model.duration];
    self.descLabel.text = _model.desc;
    self.descLabel.font = FONT_SIZE(10);
    self.equippedImageView.hidden = !([_model.equip integerValue] == 1);
    self.tagLabel.hidden = [SPDCommonTool isEmpty:_model.tag];
    self.tagLabel.text = _model.tag;
    self.expireTimeLabel.hidden = !self.is_mine || ([_model.price_origin integerValue] == -1 && [_model.price_renew integerValue] == -1 && [_model.limit_type isEqualToString:@"noble"]);
    self.expireTimeLabel.text = model.expire_time;
}

- (void)setIsSelected:(BOOL)isSelected {
    _isSelected = isSelected;
    if (_isSelected) {
        self.makeBorderView.layer.borderWidth = 1;
        self.makeBorderView.layer.borderColor = [UIColor colorWithHexString:COMMON_PINK].CGColor;
    }else{
        self.makeBorderView.layer.borderWidth = 0;
    }
}

@end
