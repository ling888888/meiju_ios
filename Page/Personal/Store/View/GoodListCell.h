//
//  GoodListCell.h
//  SimpleDate
//
//  Created by Monkey on 2019/4/23.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GoodModel;

NS_ASSUME_NONNULL_BEGIN

@interface GoodListCell : UICollectionViewCell

@property (nonatomic, strong) GoodModel * model;

@property (nonatomic, assign) BOOL isSelected;

@property (nonatomic, assign) BOOL is_mine;

@property (weak, nonatomic) IBOutlet UILabel *goodTypeLabel;

@end

NS_ASSUME_NONNULL_END
