//
//  GoodListCell.m
//  SimpleDate
//
//  Created by Monkey on 2019/4/23.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "GoodListCell.h"
#import "GoodModel.h"
#import "NSString+XXWAddition.h"
#import "YYWebImage.h"
#import "SPDCommonDefine.h"

@interface GoodListCell ()
@property (weak, nonatomic) IBOutlet YYAnimatedImageView *goodImageView;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *goodNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *goldImageView;
@property (weak, nonatomic) IBOutlet UILabel *goldNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIView *coverBgView;
@property (weak, nonatomic) IBOutlet UIImageView *equippedImageView;
@property (weak, nonatomic) IBOutlet UILabel *tagLabel;
@property (weak, nonatomic) IBOutlet UILabel *expireTimeLabel;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *typeLabelWidthCons;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tagLabelWidthCons;

@property (nonatomic, strong) YYAnimatedImageView *headwearImageView;

@end

@implementation GoodListCell

//- (YYAnimatedImageView *)headwearImageView {
//    if (!_headwearImageView) {
//        _headwearImageView = [[YYAnimatedImageView alloc] init];
//        _headwearImageView.contentMode = UIViewContentModeScaleAspectFill;
//        _headwearImageView.userInteractionEnabled = true;
//    }
//    return _headwearImageView;
//}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.tagLabel.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5,5)];//圆角大小
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.tagLabel.bounds;
    maskLayer.path = maskPath.CGPath;
    self.tagLabel.layer.mask = maskLayer;
}

- (void)setModel:(GoodModel *)model {
    _model = model;
    
    if ([model.sub_type isEqualToString:Headwear] && model.headwearWebp.notEmpty) {
        self.goodImageView.yy_imageURL = [NSURL URLWithString:model.headwearWebp];
        self.goldImageView.hidden = NO;
        self.iconImageView.hidden = YES;

    } else {
//        [self.iconImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:_model.cover]];
        self.goldImageView.hidden = YES;
        self.iconImageView.hidden = NO;
        [self.iconImageView yy_setImageWithURL:[NSURL URLWithString:_model.cover] placeholder:[UIImage imageNamed:@""]];
    }
    self.goodNameLabel.text = _model.good_name;
    self.goodNameLabel.font = FONT_SIZE(14);
    [self configGoodTypeLabel:_model.sub_type];
    self.goodTypeLabel.hidden = self.is_mine;
    
    self.goldImageView.hidden = ([model.price_origin integerValue] == -1 && [model.price_renew integerValue] == -1);
    self.goldNumLabel.hidden = ([model.price_origin integerValue] == -1 && [model.price_renew integerValue] == -1);
    self.descLabel.hidden = !([model.price_origin integerValue] == -1 && [model.price_renew integerValue] == -1);
    BOOL useGoldBuy = [model.pay_method isEqualToString:@"gold"];
    self.goldImageView.image = [UIImage imageNamed:useGoldBuy ? @"ic_store_gold":@"ic_store_diamonds"];
    NSNumber * cost = _model.owned ? _model.price_renew : _model.price_origin;
    self.goldNumLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"%@／%@天", nil),cost,_model.duration];
    self.descLabel.text = _model.desc;
    self.equippedImageView.hidden = !([_model.equip integerValue] == 1);
    self.tagLabel.hidden = [SPDCommonTool isEmpty:_model.tag];
    self.tagLabel.text = _model.tag;
//    CGSize tagSize = [NSString sizeWithString:_model.tag andFont:FONT_SIZE(12) andMaxSize:CGSizeMake(CGFLOAT_MAX, 20)];
//    self.tagLabelWidthCons.constant = tagSize.width + 20;
//    [self layoutIfNeeded];
    
    self.descLabel.font = FONT_SIZE(10);
    self.expireTimeLabel.hidden = !self.is_mine || ([_model.price_origin integerValue] == -1 && [_model.price_renew integerValue] == -1 && [_model.limit_type isEqualToString:@"noble"]);
    self.expireTimeLabel.text = model.expire_time;
    
}

- (void)configGoodTypeLabel:(NSString *)sub_type {

    if ([sub_type isEqualToString:@"headwear"]) {
        
        self.goodTypeLabel.backgroundColor = [UIColor colorWithHexString:@"#FF7796"];
        self.goodTypeLabel.text = SPDStringWithKey(@"头饰", nil);
        self.goodTypeLabel.textColor = [UIColor whiteColor];
        
    }else if ([sub_type isEqualToString:@"medal"]){
        
        self.goodTypeLabel.backgroundColor = [UIColor colorWithHexString:@"#FFD400"];
        self.goodTypeLabel.text = SPDStringWithKey(@"勋章", nil);
        self.goodTypeLabel.textColor = [UIColor colorWithHexString:@"#AB5500"];
        
    }else if ([sub_type isEqualToString:@"homepage_falling"]){
        
        self.goodTypeLabel.backgroundColor = [UIColor colorWithHexString:@"#FF991B"];
        self.goodTypeLabel.text = SPDStringWithKey(@"主页飘", nil);
        self.goodTypeLabel.textColor = [UIColor whiteColor];
        
    }else if ([sub_type isEqualToString:@"bubble"]){
        
        self.goodTypeLabel.backgroundColor = [UIColor colorWithHexString:@"#8688FB"];
        self.goodTypeLabel.text = SPDStringWithKey(@"气泡", nil);
        self.goodTypeLabel.textColor = [UIColor whiteColor];
        
    }else{
        
        self.goodTypeLabel.backgroundColor = [UIColor colorWithHexString:@"#FF7796"];
        self.goodTypeLabel.text = SPDStringWithKey(@"头饰", nil);
        self.goodTypeLabel.textColor = [UIColor whiteColor];
        
    }
//    CGSize size = [NSString sizeWithString:self.goodTypeLabel.text andFont:FONT_SIZE(14) andMaxSize:CGSizeMake(CGFLOAT_MAX, 16)];
//    self.typeLabelWidthCons.constant = size.width + 4;
}

- (void)setIsSelected:(BOOL)isSelected {
    _isSelected = isSelected;
    if (_isSelected) {
        self.coverBgView.layer.borderWidth = 1;
        self.coverBgView.layer.borderColor = [UIColor colorWithHexString:COMMON_PINK].CGColor;
    }else{
        self.coverBgView.layer.borderWidth = 0;
    }
}

@end
