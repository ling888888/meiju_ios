//
//  GoodSendTableCell.h
//  SimpleDate
//
//  Created by Monkey on 2019/6/3.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


@class HomeModel;

@protocol GoodSendTableCellDelegate <NSObject>

- (void)clickedGoodSendTableCellWith:(HomeModel *)model;

@end

@interface GoodSendTableCell : BaseTableViewCell

@property (nonatomic, strong)HomeModel * model;

@property (nonatomic, weak)id<GoodSendTableCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
