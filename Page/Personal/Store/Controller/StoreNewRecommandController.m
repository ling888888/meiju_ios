//
//  StoreNewRecommandController.m
//  SimpleDate
//
//  Created by Monkey on 2019/4/25.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "StoreNewRecommandController.h"
#import "GoodModel.h"
#import "GoodTypeCell.h"
#import "StoreTitleCollectionCell.h"
#import "GoodListCell.h"
#import "GoodBubbleCell.h"
#import "PackageViewController.h"
#import "StoreCommonViewController.h"

@interface StoreNewRecommandController ()

@property (nonatomic, strong) NSMutableArray * dataListArray; //不包括bubble
@property (nonatomic, strong) NSMutableArray * bubbleListArray; //bubble
@property (nonatomic ,strong) NSMutableArray * packageListArray; // 我的背包列表
@property (nonatomic ,strong) NSMutableArray * packageGoodIdArray; // 我的背包物品ID列表列表
@property (nonatomic ,copy) NSArray * typeListArray;
@end

@implementation StoreNewRecommandController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem * barItem = [[UIBarButtonItem alloc]initWithTitle:SPDStringWithKey(@"我的", nil) style:UIBarButtonItemStyleDone target:self action:@selector(handleMyPackage)];
    self.navigationItem.rightBarButtonItem = barItem;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:@"StoreViewController" bundle:nibBundleOrNil]) {
        
    }
    return self;
}

- (void)handleMyPackage {
    PackageViewController * packageVC = [[PackageViewController alloc]init];
    [self.navigationController pushViewController:packageVC animated:YES];
}

- (void)setDataArray:(NSMutableArray *)dataArray {
    [self.bubbleListArray removeAllObjects];
    [self.dataListArray removeAllObjects];
    for (GoodModel * model in dataArray) {
        if (!model.is_new) { continue;}
        if ([model.sub_type isEqualToString:@"bubble"]) {
            [self.bubbleListArray addObject:model];
        }else{
            [self.dataListArray addObject:model];
        }
    }
}

#pragma mark collection view

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 4;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == 0) {
        return self.typeListArray.count;
    }else if (section == 1){
        return 1;
    }else if (section == 2){
        return self.dataListArray.count;
    }else{
        return self.bubbleListArray.count;
    }
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        GoodTypeCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GoodTypeCell" forIndexPath:indexPath];
        cell.coverImageView.image = [UIImage imageNamed:self.typeListArray[indexPath.row][@"img"]?:@""];
        cell.nameLabel.text = SPDStringWithKey(self.typeListArray[indexPath.row][@"name"], nil)?:@"";
        return cell;
    }else if (indexPath.section == 1) {
        StoreTitleCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"StoreTitleCollectionCell" forIndexPath:indexPath];
        cell.titleLabel.text = SPDStringWithKey(@"新品", nil);
        return cell;
    }else if (indexPath.section == 2){
        GoodListCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GoodListCell" forIndexPath:indexPath];
        GoodModel * model = self.dataListArray[indexPath.row];
        cell.model = model;
        cell.isSelected = self.selectedModel && [self.selectedModel.good_id isEqualToString:model.good_id];
        return cell;
    }
    else{
        GoodBubbleCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GoodBubbleCell" forIndexPath:indexPath];
        GoodModel * model = self.bubbleListArray[indexPath.row];
        cell.model = model;
        cell.isSelected = self.selectedModel && [self.selectedModel.good_id isEqualToString:model.good_id];
        return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        [self cancelChooseStatus];
        StoreCommonViewController * store = [[StoreCommonViewController alloc]init];
        store.sub_type = self.typeListArray[indexPath.row][@"sub_type"]?:@"headwear";
        [self.navigationController pushViewController:store animated:YES];
        return;
    }
    if (indexPath.section == 1) {
        return;
    }
    GoodModel * model = indexPath.section == 2 ? self.dataListArray[indexPath.row] : self.bubbleListArray[indexPath.row];
    self.selectedModel = model;
    [self.collectionView reloadData];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        CGFloat width = (kScreenW  - 11 * 2 - (self.typeListArray.count -1) * 4)/ self.typeListArray.count ;
        return CGSizeMake(width, width);
    }else if (indexPath.section == 1){
        return CGSizeMake((kScreenW - 11*2), 40);
    }else if (indexPath.section == 2){
        CGFloat width = floor((kScreenW - 11*2 - 4*2)/3);
        return CGSizeMake(width, 170/115.0f * width);
    }else{
        CGFloat width = floor((kScreenW - 11*2 - 4)/2);
        return CGSizeMake(width, 135/175.0f * width);
    }
}

#pragma mark - getter

- (NSMutableArray *)dataListArray {
    if (!_dataListArray) {
        _dataListArray = [NSMutableArray new];
    }
    return _dataListArray;
}

- (NSMutableArray *)bubbleListArray {
    if (!_bubbleListArray) {
        _bubbleListArray = [NSMutableArray new];
    }
    return _bubbleListArray;
}

- (NSMutableArray *)packageListArray {
    if (!_packageListArray) {
        _packageListArray = [NSMutableArray new];
    }
    return _packageListArray;
}

- (NSMutableArray *)packageGoodIdArray {
    if (!_packageGoodIdArray) {
        _packageGoodIdArray = [NSMutableArray new];
    }
    return _packageGoodIdArray;
}


- (NSArray *)typeListArray {
    if (!_typeListArray) {
        _typeListArray =@[@{@"name":@"头饰",@"img":@"ic_headdress",@"sub_type":@"headwear"},
                          @{@"name":@"勋章",@"img":@"ic_medal",@"sub_type":@"medal"},
                          @{@"name":@"主页飘",@"img":@"ic_zhuyepiao",@"sub_type":@"homepage_falling"},
                          @{@"name":@"气泡",@"img":@"ic_bubble",@"sub_type":@"bubble"}];
    }
    return _typeListArray;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
