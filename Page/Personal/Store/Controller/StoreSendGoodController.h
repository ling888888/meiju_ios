//
//  StoreSendGoodController.h
//  SimpleDate
//
//  Created by Monkey on 2019/6/3.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "BaseViewController.h"
#import "GoodModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface StoreSendGoodController : BaseViewController

@property (nonatomic,strong)GoodModel * goodModel; // 商品model

@end

NS_ASSUME_NONNULL_END
