//
//  StoreCommonViewController.h
//  SimpleDate
//
//  Created by Monkey on 2019/4/25.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "StoreViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface StoreCommonViewController : StoreViewController


@property (nonatomic, copy) NSString * sub_type; // 这两个参数必须a要传递

@end

NS_ASSUME_NONNULL_END
