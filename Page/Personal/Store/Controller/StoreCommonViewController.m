//
//  StoreCommonViewController.m
//  SimpleDate
//
//  Created by Monkey on 2019/4/25.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "StoreCommonViewController.h"
#import "GoodModel.h"
#import "GoodListCell.h"
#import "GoodBubbleCell.h"
#import "SPDCommonDefine.h"

@interface StoreCommonViewController ()

@property (nonatomic, strong) NSMutableArray * goodsArray;

@end

@implementation StoreCommonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.collectionView setContentInset:UIEdgeInsetsMake(15, 0, IphoneX_Bottom + 51, 0)];
    
    if ([self.sub_type isEqualToString:Headwear]) {
        self.navigationItem.title = SPDStringWithKey(@"头饰", nil);
    }else if ([self.sub_type isEqualToString:Bubble]) {
        self.navigationItem.title = SPDStringWithKey(@"气泡", nil);
    }else if ([self.sub_type isEqualToString:HomePage_Falling]) {
        self.navigationItem.title = SPDStringWithKey(@"主页飘", nil);
    }else if ([self.sub_type isEqualToString:Medal]) {
        self.navigationItem.title = SPDStringWithKey(@"勋章", nil);
    }
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:@"StoreViewController" bundle:nibBundleOrNil]) {
        
    }
    return self;
}

- (void)setDataArray:(NSMutableArray *)dataArray {
    if (self.goodsArray.count != 0) {
        [self.goodsArray removeAllObjects];
    }
    for (GoodModel * model in dataArray) {
        if ([model.sub_type isEqualToString:self.sub_type]) {
            [self.goodsArray addObject:model];
        }
    }
    [self.collectionView reloadData];
}

#pragma mark - collection view

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (![self.sub_type isEqualToString:Bubble]) {
        GoodListCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GoodListCell" forIndexPath:indexPath];
        GoodModel * model = self.goodsArray[indexPath.row];
        cell.model = model;
        cell.isSelected = self.selectedModel && [self.selectedModel.good_id isEqualToString:model.good_id];
        return cell;
    }else{
        GoodBubbleCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GoodBubbleCell" forIndexPath:indexPath];
        GoodModel * model = self.goodsArray[indexPath.row];
        cell.model = model;
        cell.isSelected = self.selectedModel && [self.selectedModel.good_id isEqualToString:model.good_id];
        return cell;
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.goodsArray.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    GoodModel * model = self.goodsArray[indexPath.row];
    self.selectedModel = model;
    [self.collectionView reloadData];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (![self.sub_type isEqualToString:Bubble]) {
        CGFloat width = floor((kScreenW - 11*2 - 4*2)/3);
        return CGSizeMake(width, 170/115.0f * width);
    }else{
        CGFloat width = floor((kScreenW - 11*2 - 4)/2);
        return CGSizeMake(width, 135/175.0f * width);
    }
}


- (NSMutableArray *)goodsArray {
    if (!_goodsArray) {
        _goodsArray = [NSMutableArray new];
    }
    return _goodsArray;
}

@end
