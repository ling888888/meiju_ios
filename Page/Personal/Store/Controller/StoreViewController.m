//
//  StoreViewController.m
//  SimpleDate
//
//  Created by Monkey on 2019/4/22.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "StoreViewController.h"
#import "GoodModel.h"
#import "GoodTypeCell.h"
#import "StoreTitleCollectionCell.h"
#import "GoodListCell.h"
#import "GoodBubbleCell.h"
#import "PackageViewController.h"
#import "StoreCommonViewController.h"
#import "SPDCommonDefine.h"
#import "StoreSendGoodController.h"

@interface StoreViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UIImageView *storeHeaderBg;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIImageView *costImageView;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;
@property (weak, nonatomic) IBOutlet UIButton *buyBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewBottomConstant;
@property (weak, nonatomic) IBOutlet UILabel *nonOwnedLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topLeadingConstant;
@property (nonatomic, strong)CAEmitterLayer * emitterLayer;
@property (nonatomic, copy)NSString * headwear; // 当前装备的头像
@property (nonatomic, strong)UIView * emptyView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionBottomConstant;
@property (weak, nonatomic) IBOutlet UILabel *nobleSaleLabel;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn; // 赠送按钮
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *costImageViewCenterCons;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *costImageViewWidthCons;

@property (nonatomic, strong) LY_PortraitView *portraitView;

@end

@implementation StoreViewController

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
    }
    return _portraitView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.topLeadingConstant.constant = NavHeight;
    self.bottomViewBottomConstant.constant = IphoneX_Bottom;
    self.collectionBottomConstant.constant = IphoneX_Bottom;
    
    [self.sendBtn setImage:[UIImage imageNamed:@"ic_send_good_btn"] forState:UIControlStateNormal];
    [self.sendBtn setTitle:SPDStringWithKey(@"赠", nil) forState:UIControlStateNormal];
    [self.sendBtn setBackgroundColor:SPD_HEXCOlOR(@"#E3FFF1")];
    [self.sendBtn setTitleColor:SPD_HEXCOlOR(@"#232426") forState:UIControlStateNormal];
    self.sendBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.sendBtn setImagePosition:HHImagePositionTop spacing:6.0f];

//    storeHeaderBg
//    self.avatarImageView.layer.cornerRadius = (84/375.f * kScreenW)/2;
//    self.avatarImageView.clipsToBounds = YES;
//
//    self.borderImageView.layer.cornerRadius = (92/375.f * kScreenW)/2;
//    self.borderImageView.clipsToBounds = YES;
//    self.borderImageView.backgroundColor = SPD_HEXCOlOR(@"#5fdbf1");
//
//    [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,[SPDApiUser currentUser].avatar]]];
    self.navigationItem.title = SPDStringWithKey(@"个性装扮", nil);
    self.bottomView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.bottomView.layer.shadowOffset = CGSizeMake(0,-0.3);
    self.bottomView.layer.shadowOpacity = 0.5;
    self.bottomView.layer.shadowRadius = 0.5;
    self.bottomView.clipsToBounds = NO;
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.showsVerticalScrollIndicator = NO;
    [self.collectionView setContentInset:UIEdgeInsetsMake(0, 0, IphoneX_Bottom + 51, 0)];
    [self.collectionView registerNib:[UINib nibWithNibName:@"GoodTypeCell" bundle:nil] forCellWithReuseIdentifier:@"GoodTypeCell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"StoreTitleCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"StoreTitleCollectionCell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"GoodListCell" bundle:nil] forCellWithReuseIdentifier:@"GoodListCell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"GoodBubbleCell" bundle:nil] forCellWithReuseIdentifier:@"GoodBubbleCell"];
    
    [self.storeHeaderBg addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(110);
        make.center.mas_equalTo(0);
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self requestMyPackage];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self cancelChooseStatus];
}

#pragma mark request

- (void)requestMyPackage {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"store/package.list" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        [self.packageArray removeAllObjects];
        self.packageArray = [suceess[@"good_list"] mutableCopy];
       
        //只有我的背包页面有一个空页面
        if (self.packageArray.count == 0 && [self isKindOfClass:[PackageViewController class]]) {
            [self.view addSubview:self.emptyView];
        }
        
        NSString * currentHeadwear = nil;
        for (NSDictionary * dict in self.packageArray) {
            if ([dict[@"title"] isEqualToString:Headwear]) {
                for (NSDictionary * goodInfo in dict[@"list"]) {
                    if ([goodInfo[@"equip"] integerValue] == 1) {
                        currentHeadwear = goodInfo[@"headwearWebp"];
                    }
                }
            }
        }
        self.headwear = currentHeadwear;
        [self resetDecorate];
        [self.collectionView reloadData];
    } bFailure:^(id  _Nullable failure) {
        [self showMessageToast:failure[@"msg"]];
    }];
    
    [self requestGoodList];
}

- (void)requestGoodList {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"store/good.list" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        NSArray * good_list = suceess[@"good_list"];
        [self.dataArray removeAllObjects];
        NSMutableArray * goodsArray = [NSMutableArray new];
        for (NSDictionary * dict in good_list) {
            GoodModel * model = [GoodModel initWithDictionary:dict];
            if (self.selectedModel && [self.selectedModel.good_id isEqualToString:model.good_id]) {
                [self setSelectedModel:model];
            }// 这里是为了购买之后重置购买之后的 贵族打折价格
            [goodsArray addObject: model];
        }
        self.dataArray = goodsArray;
        [self.collectionView reloadData];
        
    } bFailure:^(id  _Nullable failure) {
        [self showMessageToast:failure[@"msg"]];
    }];
}

//购买 续费
- (void)requestGoodBuy:(GoodModel *)model isRenew:(BOOL)isRenew {
    NSMutableDictionary * dict = [NSMutableDictionary new];
    [dict setValue:model.good_id forKey:@"good_id"];

    [RequestUtils commonPostRequestUtils:dict bURL:@"store/good.buy" bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if (!isRenew) {
            model.owned = true;
            [self setSelectedModel:model];
            
            [self presentCommonController:SPDStringWithKey(@"购买成功确定装扮吗？", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
                [self showMessageToast:SPDStringWithKey(@"购买成功", nil)];
            } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                [self requestPackageEquiped:model Equip:1];
            }];
        }else{
            [self showMessageToast:SPDStringWithKey(@"续费成功，可自行装扮", nil)];
        }
        [self requestMyPackage];
    } bFailure:^(id  _Nullable failure) {
        if ([failure[@"code"] integerValue] == 2006) {
            [self presentCommonController:SPDStringWithKey(@"余额不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
                
            } confirmTitle:SPDStringWithKey(@"充值", nil) confirmAction:^{
                [self pushToRechageController];
            }];
        }else{
            [self showMessageToast:failure[@"msg"]];
        }
    }];
}

//装备1 解除装备-1
- (void)requestPackageEquiped: (GoodModel *)model Equip: (NSInteger )status{
    NSMutableDictionary * dict = [NSMutableDictionary new];
    [dict setValue:model.good_id forKey:@"good_id"];
    [dict setValue:@(status) forKey:@"equip"];
    [RequestUtils commonPostRequestUtils:dict bURL:@"store/package.equip" bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        [self showMessageToast:status == 1 ? ( [model.sub_type isEqualToString:Bubble] ?SPDStringWithKey(@"使用成功", nil):SPDStringWithKey(@"装扮成功", nil)):([model.sub_type isEqualToString:Bubble]?SPDStringWithKey(@"已取消使用",nil): SPDStringWithKey(@"已取消装扮", nil)) ];
        model.equip = status == 1 ? @(1):@(0);
        [self setSelectedModel:model];
        [self requestMyPackage];
    } bFailure:^(id  _Nullable failure) {
        [self showMessageToast:failure[@"msg"]];
    }];
}

- (void)setSelectedModel:(GoodModel *)selectedModel {
    _selectedModel = selectedModel;
    [self chooseGoodWithModel:_selectedModel];
}

- (void)chooseGoodWithModel:(GoodModel *) model {
    
    [self resetDecorate];
    
    if ([model.sub_type isEqualToString:Headwear]) {
        LY_Portrait *portrait = [[LY_Portrait alloc] init];
        portrait.url = [LY_Api getCompleteResourceUrl:[SPDApiUser currentUser].avatar];
        portrait.headwearUrl = model.headwearWebp;
        
        self.portraitView.portrait = portrait;
//        [self.coverImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:model.cover]];
//        self.borderImageView.hidden = YES;
    }else if([model.sub_type isEqualToString:HomePage_Falling]){
        [self showNobleAnimation:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:model.cover]]]];
    }
    self.bottomView.hidden = NO;
    self.costImageViewCenterCons.constant = 23;
    self.nobleSaleLabel.hidden = YES;
    self.costImageViewCenterCons.constant = 0;

    // 底部
    if ([model.price_origin integerValue] == -1 && [model.price_renew integerValue] == -1) {
        self.costImageView.hidden = YES;
        self.sendBtn.hidden = YES;
        if (model.owned) {
            self.costLabel.hidden = NO;
            self.costLabel.text = model.desc;
            self.buyBtn.hidden = NO;
            self.cancelBtn.hidden = YES;
            self.nonOwnedLabel.hidden = YES;
            [self.buyBtn setTitle:([model.equip integerValue] == 1) ?SPDStringWithKey(@"取消装扮", nil):SPDStringWithKey(@"装扮", nil) forState:UIControlStateNormal];
            self.buyBtn.tag = ([model.equip integerValue] == 1) ? 3 :4; // 3 代表取消装扮 4 代表装扮
        }else{
            self.costLabel.hidden = YES;
            self.buyBtn.hidden = YES;
            self.cancelBtn.hidden = YES;
            self.nonOwnedLabel.hidden = NO;
            self.nonOwnedLabel.text = model.desc;
        }
    }else{
        self.costImageView.hidden = NO;
        self.costLabel.hidden = NO;
        self.nonOwnedLabel.hidden = YES;
        BOOL useGoldBuy = [model.pay_method isEqualToString:@"gold"];
        self.costImageView.image = [UIImage imageNamed:useGoldBuy ? @"ic_store_gold":@"ic_store_diamonds"];
        self.buyBtn.hidden = NO;
        self.cancelBtn.hidden = !model.owned;
        self.sendBtn.hidden = NO;
        if (model.owned) {
            //我拥有 应该使用续费
            self.costLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"%@／%@天", nil),model.price_renew,model.duration];
            [self.buyBtn setTitle:SPDStringWithKey(@"续费", nil ) forState:UIControlStateNormal];
            self.buyBtn.tag = 2;
            [self configNobleSaleLabel:model];
            if ([model.equip integerValue] == 1) { // 我是否装备
                [self.cancelBtn setTitle:[model.sub_type isEqualToString:Bubble] ? SPDStringWithKey(@"取消使用", nil ):SPDStringWithKey(@"取消装扮", nil ) forState:UIControlStateNormal];
                self.cancelBtn.tag = -1;
            }else  {
                [self.cancelBtn setTitle:[model.sub_type isEqualToString:Bubble] ? SPDStringWithKey(@"使用", nil ) : SPDStringWithKey(@"装扮", nil ) forState:UIControlStateNormal];
                self.cancelBtn.tag = 1;
            }
        }else{
            if (![SPDCommonTool isEmpty:model.limit_type]) {
                NSInteger myNobleValue = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"noble:%@",[SPDApiUser currentUser].userId]];
                if (([model.limit_type isEqualToString:@"noble"] && myNobleValue >= [model.limit_value integerValue] )|| ([model.limit_type isEqualToString:@"vip"] && [[NSUserDefaults standardUserDefaults] boolForKey:ISVIP])) {
                    [self.buyBtn setTitle:SPDStringWithKey(@"购买", nil ) forState:UIControlStateNormal];
                    self.buyBtn.tag = 1;
                    self.costLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"%@／%@天", nil),model.price_origin,model.duration];
                    [self configNobleSaleLabel:model];
                }
                else{
                    // limitvalue 的要求
                    self.costImageView.hidden = YES;
                    self.costLabel.hidden = YES;
                    self.buyBtn.hidden = YES;
                    self.cancelBtn.hidden = YES;
                    self.nonOwnedLabel.hidden = NO;
                    self.nonOwnedLabel.text = model.desc?:SPDStringWithKey(@"贵族可购买", nil);
                    self.sendBtn.hidden = YES;
                }
            }else{
                [self.buyBtn setTitle:SPDStringWithKey(@"购买", nil ) forState:UIControlStateNormal];
                self.buyBtn.tag = 1;
                self.costLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"%@／%@天", nil),model.price_origin,model.duration];
                [self configNobleSaleLabel:model];
            }// limit_type
        }// 非拥有
    }
}

- (void)configNobleSaleLabel :(GoodModel *)model {
    if (![SPDCommonTool isEmpty:model.noble_sale]) {
        self.costImageViewCenterCons.constant = -10;
        self.nobleSaleLabel.hidden = NO;
        self.nobleSaleLabel.text = model.noble_sale;
        self.costLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"%@／%@天", nil),model.noble_price,model.duration];
        self.costImageViewWidthCons.constant = 16;
    }
}

// 取消底部选择状态
- (void)cancelChooseStatus {
    self.selectedModel = nil;
    self.bottomView.hidden = YES;
}

// reset 头部装饰
- (void)resetDecorate {
//    self.borderImageView.hidden = NO;
//    if (self.headwear) {
//        [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:self.headwear]];
//        self.borderImageView.hidden = YES;
//    }else{
//        self.coverImageView.image = [UIImage new];
//    }
//    borderImageView
    
    LY_Portrait *portrait = [[LY_Portrait alloc] init];
    portrait.url = [LY_Api getCompleteResourceUrl:[SPDApiUser currentUser].avatar];
    portrait.headwearUrl = self.headwear;
    
    self.portraitView.portrait = portrait;
    [self.emitterLayer removeFromSuperlayer];
}


- (void)showNobleAnimation:(UIImage *)image {
    [self.view.layer addSublayer:self.emitterLayer];

    CAEmitterCell *emitterCell = [CAEmitterCell emitterCell];
    emitterCell.birthRate = 2.5;
    emitterCell.contents = (id)image.CGImage;
    emitterCell.velocity = 100.0;
    emitterCell.lifetime = 50;//粒子存活时间
    emitterCell.emissionLongitude = M_PI;
    emitterCell.scale = 0.3;//缩放比例
    emitterCell.scaleRange = 0.2;//缩放比例
    emitterCell.yAcceleration = 2;//y轴方向的加速度，落叶下飘只需要y轴正向加速度。
    emitterCell.spin = 1.0;//粒子旋转速度
    emitterCell.spinRange = 2.0;//粒子旋转速度范围
    self.emitterLayer.emitterCells = [NSArray arrayWithObject:emitterCell];
}

#pragma mark - clicked

// 1 购买 2 续费 3 装扮 4 取消装扮
- (IBAction)buyBtnClicked:(UIButton *)sender {
    NSString * str = @"";
    if (sender.tag == 1) {
        BOOL useGoldBuy = [self.selectedModel.pay_method isEqualToString:@"gold"];
        NSNumber * cost = self.selectedModel.price_origin;
        if (![SPDCommonTool isEmpty:self.selectedModel.noble_sale]) {
            cost = self.selectedModel.noble_price;
        }
        str = [NSString stringWithFormat:SPDStringWithKey(@"确定花费%@%@购买装饰？",nil),cost,(useGoldBuy? SPDStringWithKey(@"金币", nil):SPDStringWithKey(@"钻石", nil))];
        [self presentCommonController:SPDStringWithKey(str, nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
        } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
            [self requestGoodBuy:self.selectedModel isRenew:NO];
        }];
    }else if (sender.tag == 2){
        BOOL useGoldBuy = [self.selectedModel.pay_method isEqualToString:@"gold"];
        NSNumber * cost = self.selectedModel.price_renew;
        if (![SPDCommonTool isEmpty:self.selectedModel.noble_sale]) {
            cost = self.selectedModel.noble_price;
        }
        str = [NSString stringWithFormat:SPDStringWithKey(@"确定花费%@%@续费装饰？", nil),cost,(useGoldBuy?SPDStringWithKey(@"金币", nil):SPDStringWithKey(@"钻石", nil))];
        [self presentCommonController:SPDStringWithKey(str, nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
            
        } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
            [self requestGoodBuy:self.selectedModel isRenew:YES];
        }];
    }else{
        [self requestPackageEquiped:self.selectedModel Equip:sender.tag == 3 ? -1: 1];
    }
}

// tag = 1 装扮 tage =  -1
- (IBAction)cancelBtnClicked:(UIButton *)sender {
    [self requestPackageEquiped:self.selectedModel Equip:sender.tag];
}

#pragma mark - collection

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 4;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 4;
}

#pragma mark - emmiter

- (CAEmitterLayer *)emitterLayer {
    if (!_emitterLayer) {
        _emitterLayer = [[CAEmitterLayer alloc] init];
        _emitterLayer.emitterPosition = CGPointMake(kScreenW / 2, -17);
        _emitterLayer.emitterShape = kCAEmitterLayerLine;
        _emitterLayer.emitterMode = kCAEmitterLayerOutline;//从发射器边缘发出
        _emitterLayer.emitterSize = CGSizeMake(kScreenW, 0);
        _emitterLayer.renderMode = kCAEmitterLayerSurface;
    }
    return _emitterLayer;
}

- (NSMutableArray *)packageArray{
    if (!_packageArray) {
        _packageArray = [NSMutableArray new];
    }
    return _packageArray;
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

- (UIView *)emptyView {
    if (!_emptyView) {
        _emptyView = [[UIView alloc]initWithFrame:self.view.bounds];
        _emptyView.backgroundColor = [UIColor whiteColor];
        _emptyView.userInteractionEnabled = YES;
        UIImageView * image1 = [[UIImageView alloc]initWithFrame:CGRectMake((kScreenW - 125)/2.0f, (kScreenH - 206)/2, 121, 117)];
        [_emptyView addSubview:image1];
        image1.image = [UIImage imageNamed:@"ic_package_empty"];
        UILabel * label = [UILabel new];
        label.center = CGPointMake(image1.center.x, image1.center.y + 23+ 117/2.0f);
        label.bounds = CGRectMake(0, 0, kScreenW, 16);
        label.text = SPDStringWithKey(@"快去购买装扮装饰自己吧~", nil);
        label.font = [UIFont systemFontOfSize:15];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = SPD_HEXCOlOR(@"#666666");
        [_emptyView addSubview:label];
        UIButton * buyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [buyBtn setTitle:SPDStringWithKey(@"购买", nil) forState:UIControlStateNormal];
        buyBtn.bounds = CGRectMake(0, 0, 171, 41);
        buyBtn.layer.cornerRadius = 20;
        buyBtn.clipsToBounds = YES;
        [buyBtn setBackgroundColor:[UIColor colorWithHexString:COMMON_PINK]];
        buyBtn.center = CGPointMake(_emptyView.center.x,CGRectGetMaxY(label.frame)+60);
        [buyBtn addTarget:self action:@selector(handleBuy) forControlEvents:UIControlEventTouchUpInside];
        [_emptyView addSubview:buyBtn];
    }
    return _emptyView;
}

- (void)handleBuy {
    [self.navigationController popViewControllerAnimated:YES];
}

// 赠送按钮点击
- (IBAction)sendBtnClicked:(id)sender {
    StoreSendGoodController * vc = [[StoreSendGoodController alloc]init];
    vc.goodModel = self.selectedModel;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
