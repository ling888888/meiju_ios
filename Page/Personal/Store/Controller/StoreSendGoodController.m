//
//  StoreSendGoodController.m
//  SimpleDate
//
//  Created by Monkey on 2019/6/3.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "StoreSendGoodController.h"
#import "HomeModel.h"
#import "GoodSendTableCell.h"
#import "PresentMessage.h"

@interface StoreSendGoodController ()<UITableViewDelegate,UITableViewDataSource,GoodSendTableCellDelegate>
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * dataArray;
@property (assign, nonatomic) int  page;
@property (nonatomic,strong) UILabel * emptyLabel;

@end

@implementation StoreSendGoodController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.page = 0;
    self.navigationItem.title = SPDStringWithKey(@"赠送好友", nil);
     [self requestFriendList];
    
    self.tableView.tableFooterView = [[UIView alloc]init];
    self.tableView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
        self.page = 0;
        [self requestFriendList];
    }];
    self.tableView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
        self.page ++;
        [self requestFriendList];
    }];
}

- (void)requestFriendList {
    NSMutableDictionary * dict = [NSMutableDictionary new];
    [dict setValue:@"list" forKey:@"type"];
    [dict setValue:@(self.page) forKey:@"page"];
    [RequestUtils commonGetRequestUtils:dict bURL:@"friend" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable list) {
        
        if (self.page == 0) {
            [self.dataArray removeAllObjects];
            [self.tableView.mj_header endRefreshing];
        }else{
            [self.tableView.mj_footer endRefreshing];
        }
        
        NSArray * friendsArray = list[@"list"];
        for (NSDictionary * dict in friendsArray) {
            HomeModel * model = [HomeModel initWithDictionary:dict];
            [self.dataArray addObject:model];
        }
        self.emptyLabel.hidden = !(self.dataArray.count == 0);
        [self.tableView reloadData];
        self.tableView.mj_footer.hidden = (friendsArray.count < 10);
    } bFailure:^(id  _Nullable failure) {
        [self showToast:failure[@"msg"]];
    }];
}


#pragma mark - delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GoodSendTableCell * cell = [GoodSendTableCell cellWithTableView:tableView];
    cell.model = self.dataArray[indexPath.row];
    cell.delegate = self;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    HomeModel * model = self.dataArray[indexPath.row];
    [self pushToDetailTableViewController:self userId:model.user_id];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 77;
}

- (void)clickedGoodSendTableCellWith:(HomeModel *)model {
    [self presentCommonController:SPDStringWithKey(@"确定赠送对方个性装扮？", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
    } confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
        [self requestAPI:model];
    }];
}

- (void)requestAPI:(HomeModel *)model {
    if ([SPDCommonTool isEmpty:self.goodModel.good_id]) {
        return;
    }
    NSMutableDictionary * dict = [NSMutableDictionary new];
    [dict setObject:self.goodModel.good_id forKey:@"good_id"];
    [dict setObject:model.user_id forKey:@"user_id"];
    [RequestUtils commonPostRequestUtils:dict bURL:@"store/good.send" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setTimeZone:[NSTimeZone localTimeZone]];
        [format setDateFormat:@"MM/dd"];
        NSNumber *expire = suceess[@"expire_time"];
        NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:expire.longLongValue / 1000];
        self.goodModel.expire_time = [NSString stringWithFormat:SPDStringWithKey(@"%@到期", nil), [format stringFromDate:date]];
        [self sendGoodMessage:model.user_id];

    } bFailure:^(id  _Nullable failure) {
        if ([failure[@"code"] integerValue] == 2006) {
            [self presentCommonController:SPDStringWithKey(@"账户余额不足", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:^{
                
            } confirmTitle:SPDStringWithKey(@"充值", nil) confirmAction:^{
                [self pushToRechageController];
            }];
        }else{
            [self showMessageToast:failure[@"msg"]];
        }
        
    }];
}

- (void)sendGoodMessage:(NSString *)user_id {
    NSString * str = SPDStringWithKey(@"送你一个装饰，希望你能喜欢", nil);
    NSRange range = [self.goodModel.cover rangeOfString:@"image/"];
    NSString * imageName = [self.goodModel.cover substringFromIndex:range.location+6];
    PresentMessage * msg = [PresentMessage messageWithContent:str andVehicle_id:self.goodModel.good_id andGiftName:self.goodModel.good_name andImageName:imageName andGoldNum:@"0" andCharamScore:@"0" andType:@"good" andExpire_time:self.goodModel.expire_time];
   
    [[RCIM sharedRCIM] sendMessage:ConversationType_PRIVATE targetId:user_id content:msg pushContent:str pushData:str success:^(long messageId) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SPDCommonTool showWindowToast:SPDStringWithKey(@"赠送个性装扮成功", nil)];
        });
    } error:^(RCErrorCode nErrorCode, long messageId) {
    }];
}

#pragma mark - getter
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.showsVerticalScrollIndicator = NO;
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

- (UILabel *)emptyLabel {
    if (!_emptyLabel) {
        _emptyLabel = [[UILabel alloc]init];
        _emptyLabel.text = SPDStringWithKey(@"您还没有好友,快去添加好友吧~", nil);
        _emptyLabel.frame = CGRectMake(0, self.view.center.y - 21 - NavHeight, kScreenW, 21);
        _emptyLabel.textAlignment = NSTextAlignmentCenter;
        _emptyLabel.textColor = [UIColor colorWithHexString:@"232426"];
        _emptyLabel.font = [UIFont systemFontOfSize:15];
        [self.tableView addSubview:_emptyLabel];
    }
    return _emptyLabel;
}

@end
