//
//  StoreViewController.h
//  SimpleDate
//
//  Created by Monkey on 2019/4/22.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "BaseViewController.h"
@class GoodModel;

NS_ASSUME_NONNULL_BEGIN

@interface StoreViewController : BaseViewController

//@property (nonatomic ,copy) NSString * type; // [@"new",@"headwear",@"medal",@"homepage_falling", @"bubble",@"mine"]; new 代表新品  mine 代表我的

- (void)chooseGoodWithModel:(GoodModel *) model ;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong)NSMutableArray * dataArray;
@property (nonatomic, strong)NSMutableArray * packageArray;
@property (nonatomic ,strong)GoodModel * selectedModel; // 当前选中


- (void)cancelChooseStatus; // 取消选择状态

@end

NS_ASSUME_NONNULL_END
