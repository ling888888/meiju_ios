//
//  PackageViewController.m
//  SimpleDate
//
//  Created by Monkey on 2019/4/25.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "PackageViewController.h"
#import "GoodModel.h"
#import "StoreTitleCollectionCell.h"
#import "GoodListCell.h"
#import "GoodBubbleCell.h"
#import "SPDCommonDefine.h"

@interface PackageViewController ()

@end

@implementation PackageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = SPDStringWithKey(@"我的", nil);
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:@"StoreViewController" bundle:nibBundleOrNil]) {
        
    }
    return self;
}

#pragma mark collection view

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.packageArray.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSArray * list = self.packageArray[section][@"list"];
    return list.count + 1;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        StoreTitleCollectionCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"StoreTitleCollectionCell" forIndexPath:indexPath];
        NSString * str = [self getTitleWithSubtype:self.packageArray[indexPath.section][@"title"]?:Medal];
        cell.titleLabel.text = SPDStringWithKey(str, nil);
        return cell;
    }else{
        NSString * sub_type =  self.packageArray[indexPath.section][@"title"]?:@"";
        NSArray * list = self.packageArray[indexPath.section][@"list"];
        GoodModel * model = [GoodModel initWithDictionary:list[indexPath.row -1]];
        if ([sub_type isEqualToString:Bubble]) {
            GoodBubbleCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GoodBubbleCell" forIndexPath:indexPath];
            cell.is_mine = true;
            cell.model = model;
            cell.isSelected = self.selectedModel && [self.selectedModel.good_id isEqualToString:cell.model.good_id];
            return cell;
        }else{
            GoodListCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GoodListCell" forIndexPath:indexPath];
            cell.is_mine = true;
            cell.model = model;
            cell.isSelected = self.selectedModel && [self.selectedModel.good_id isEqualToString:cell.model.good_id];
            return cell;
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row != 0) {
        NSArray * list = self.packageArray[indexPath.section][@"list"];
        GoodModel * model = [GoodModel initWithDictionary:list[indexPath.row -1]];
        self.selectedModel = model;
        [self.collectionView reloadData];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return CGSizeMake(kScreenW, 40);
    }else{
        NSString * sub_type =  self.packageArray[indexPath.section][@"title"]?:@"";
        if (![sub_type isEqualToString:Bubble]) {
            CGFloat width = floor((self.collectionView.bounds.size.width - 4*2)/3);
            return CGSizeMake(width, 170/115.0f * width);
        }else{
            CGFloat width = floor((self.collectionView.bounds.size.width - 4)/2);
            return CGSizeMake(width, 135/175.0f * width);
        }
    }
}


- (NSString *)getTitleWithSubtype: (NSString *)sub_type {
    if ([sub_type isEqualToString:Bubble]) {
        return @"气泡";
    }else if ([sub_type isEqualToString:HomePage_Falling]) {
        return @"主页飘";
    }else if ([sub_type isEqualToString:Medal]) {
        return @"勋章";
    }else  {
        return @"头饰";
    }
}




@end
