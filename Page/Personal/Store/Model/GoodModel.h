//
//  GoodModel.h
//  SimpleDate
//
//  Created by Monkey on 2019/4/23.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "CommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodModel : CommonModel

@property(nonatomic ,copy) NSString * good_id;
@property(nonatomic ,copy) NSString * good_name;
@property(nonatomic ,copy) NSString * type;
@property(nonatomic ,copy) NSString * sub_type; // [@"headewear",@"medal",@"homepage_float",@"bubble"]
@property(nonatomic ,copy) NSString * cover;
@property(nonatomic ,copy) NSString * pay_method; //"gold"/"score"
@property(nonatomic ,copy) NSNumber * price_origin;
@property(nonatomic ,copy) NSNumber * price_renew;
@property(nonatomic ,copy) NSString * limit_type;
@property(nonatomic ,copy) NSString * limit_value;
@property(nonatomic ,assign) BOOL equipable; // 是否可以装备
@property(nonatomic ,copy)  NSNumber *equip; //  1: 已装备, -1: 未装备, 0: 不可装备
@property(nonatomic, assign) BOOL owned; // true 代表拥有
@property(nonatomic ,copy) NSNumber * duration;
@property(nonatomic ,copy) NSString * tag;
@property(nonatomic ,assign) BOOL is_new;
@property(nonatomic ,copy) NSString * desc;
@property(nonatomic ,copy) NSString *expire_time;
@property(nonatomic ,copy) NSString *noble_sale; // 贵族打7折
@property(nonatomic ,copy) NSNumber *noble_price; // 贵族打折之后的价格 这里都是server 处理 本地并没有进行判断个人爵位

@property(nonatomic ,copy) NSString *headwearWebp;

@end

NS_ASSUME_NONNULL_END
