//
//  GoodModel.m
//  SimpleDate
//
//  Created by Monkey on 2019/4/23.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "GoodModel.h"

@implementation GoodModel


- (void)setValue:(id)value forKey:(NSString *)key {
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"is_new"]) {
        self.is_new = [value boolValue];
    }else if ([key isEqualToString:@"expire_time"]){
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setTimeZone:[NSTimeZone localTimeZone]];
        [format setDateFormat:@"MM/dd"];
        NSNumber *expire = value;
        NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:expire.longLongValue / 1000];
        self.expire_time = [NSString stringWithFormat:SPDStringWithKey(@"%@到期", nil), [format stringFromDate:date]];
    }else if ([key isEqualToString:@"owned"]) {
        self.owned = [value boolValue];
    }
}


@end
