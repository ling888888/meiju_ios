//
//  SpecialNumListViewController.h
//  SimpleDate
//
//  Created by 李楠 on 2019/7/15.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SpecialNumListViewController : BaseViewController

@property (nonatomic, copy) NSString *type;

@end

NS_ASSUME_NONNULL_END
