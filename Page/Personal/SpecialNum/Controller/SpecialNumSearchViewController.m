//
//  SpecialNumSearchViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2019/7/16.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "SpecialNumSearchViewController.h"
#import "SpecialNumListCell.h"
#import "SpecialNumModel.h"
#import "SpecialNumPriceCell.h"

@interface SpecialNumSearchViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, SpecialNumListCellDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchViewTop;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (weak, nonatomic) IBOutlet UICollectionView *priceCollectionView;
@property (weak, nonatomic) IBOutlet UIView *emptyView;
@property (weak, nonatomic) IBOutlet UILabel *emptyLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *listCollectionView;

@property (nonatomic, strong) NSMutableArray *priceArray;
@property (nonatomic, strong) NSURLSessionDataTask *listTask;
@property (nonatomic, copy) NSString *searchText;
@property (nonatomic, assign) NSInteger selectedPriceIndex;
@property (nonatomic, copy) NSString *priceRange;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) SpecialNumModel *mySpecialNum;
@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation SpecialNumSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureSubviews];
    [self requestSpecialNumConfig];
}

#pragma mark - Private methods

- (void)configureSubviews {
    self.navigationItem.title = SPDStringWithKey(@"靓号ID", nil);
    self.searchViewTop.constant = NavHeight;
    self.searchTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:SPDStringWithKey(@"输入2—5位数字搜索", nil) attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"999999"]}];
    [self.actionButton setTitle:SPDStringWithKey(@"取消", nil) forState:UIControlStateNormal];
    [self.actionButton setTitleColor:[UIColor colorWithHexString:@"232426"] forState:UIControlStateNormal];
    [self.actionButton setTitle:SPDStringWithKey(@"搜索", nil) forState:UIControlStateSelected];
    [self.actionButton setTitleColor:[UIColor colorWithHexString:COMMON_PINK] forState:UIControlStateSelected];
    
    [self.priceCollectionView registerNib:[UINib nibWithNibName:@"SpecialNumPriceCell" bundle:nil] forCellWithReuseIdentifier:@"SpecialNumPriceCell"];
    self.emptyLabel.text = SPDStringWithKey(@"该价格区间暂时没有符合搜索条件的靓号", nil);
    [self.listCollectionView registerNib:[UINib nibWithNibName:@"SpecialNumListCell" bundle:nil] forCellWithReuseIdentifier:@"SpecialNumListCell"];
    __weak typeof(self) weakSelf = self;
    self.listCollectionView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
        weakSelf.page = 0;
        [weakSelf requestSpecialNumListWithnimated:NO];
    }];
    self.listCollectionView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
        [weakSelf requestSpecialNumListWithnimated:NO];
    }];
    self.listCollectionView.mj_footer.hidden = YES;
}

- (void)requestSpecialNumConfig {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"specialNum.config" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        NSArray *sectionArray = suceess[@"section"];
        for (NSInteger i = 0; i < sectionArray.count; i++) {
            if (i < sectionArray.count - 1) {
                [self.priceArray addObject:[NSString stringWithFormat:@"%@-%@", sectionArray[i], sectionArray[i + 1]]];
            } else {
                [self.priceArray addObject:[NSString stringWithFormat:@"%@+", sectionArray[i]]];
            }
        }
        [self.priceCollectionView reloadData];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestSpecialNumListWithnimated:(BOOL)animated {
    NSMutableDictionary *mutableDic = [@{@"text": self.searchText, @"page": @(self.page), @"onsale": @(1)} mutableCopy];
    if (self.priceRange) {
        [mutableDic setObject:self.priceRange forKey:@"price_range"];
    }
    [self.listTask cancel];
    self.listTask = [RequestUtils commonGetRequestUtils:mutableDic bURL:@"specialNum.list" bAnimated:animated bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.priceCollectionView.hidden = NO;
        NSDictionary *mine = suceess[@"mine"];
        if (mine.count) {
            self.mySpecialNum = [SpecialNumModel initWithDictionary:mine];
        }
        
        if (self.page == 0) {
            [self.dataArray removeAllObjects];
            [self.listCollectionView setContentOffset:CGPointZero];
        }
        NSArray *list = suceess[@"list"];
        for (NSDictionary *dic in suceess[@"list"]) {
            SpecialNumModel *model = [SpecialNumModel initWithDictionary:dic];
            if (![SPDCommonTool isEmpty:model.highlight]) {
                model.attributedSpecialNum = [[NSMutableAttributedString alloc] initWithString:model.specialNum];
                NSMutableArray *rangArray = [SPDCommonTool allRangesOfString:model.highlight withinString:model.specialNum];
                for (NSValue *value in rangArray) {
                    [model.attributedSpecialNum addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"FDCB03"] range:value.rangeValue];
                }
            }
            [self.dataArray addObject:model];
        }
        self.emptyView.hidden = self.dataArray.count;
        self.listCollectionView.hidden = !self.dataArray.count;
        [self.listCollectionView reloadData];
        self.page++;
        
        [self.listCollectionView.mj_header endRefreshing];
        [self.listCollectionView.mj_footer endRefreshing];
        self.listCollectionView.mj_footer.hidden = !list.count;
    } bFailure:^(id  _Nullable failure) {
        [self.listCollectionView.mj_header endRefreshing];
        [self.listCollectionView.mj_footer endRefreshing];
    }];
}

- (void)requestSpecialNumBuyWithModel:(SpecialNumModel *)model {
    NSDictionary *dic = @{@"specialNum": model.specialNum};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"specialNum.buy" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.mySpecialNum = [SpecialNumModel initWithDictionary:suceess];
        [self.dataArray removeObject:model];
        self.emptyView.hidden = self.dataArray.count;
        self.listCollectionView.hidden = !self.dataArray.count;
        [self.listCollectionView reloadData];
    } bFailure:^(id  _Nullable failure) {
        if ([failure isKindOfClass:[NSDictionary class]]) {
            if ([failure[@"code"] integerValue] == 2003) {
                [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                    [self pushToRechageController];
                }];
            } else if ([failure[@"code"] integerValue] == 2004) {
                [self showToast:SPDStringWithKey(@"你已拥有靓号ID，不可再重复购买", nil)];
            } else {
                [self showToast:failure[@"msg"]];
            }
        }
    }];
}

#pragma mark - Event response

- (IBAction)clickActionButton:(UIButton *)sender {
    if (sender.selected) {
        if (![self.searchTextField.text isEqualToString:self.searchText]) {
            self.searchText = self.searchTextField.text;
            self.page = 0;
            [self requestSpecialNumListWithnimated:YES];
        }
    } else {
        self.searchTextField.text = @"";
    }
    [self.searchTextField resignFirstResponder];
}

- (IBAction)searchTextFieldEditingChanged:(UITextField *)sender {
    if (sender.text.length < 2) {
        self.actionButton.selected = NO;
    } else {
        self.actionButton.selected = YES;
        if (sender.text.length > 5) {
            sender.text = [sender.text substringToIndex:5];
        }
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if ([collectionView isEqual:self.priceCollectionView]) {
        return self.priceArray.count > 1 ? self.priceArray.count : 0;
    } else {
        return 1;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == self.priceCollectionView) {
        return 1;
    } else {
        return self.dataArray.count;
    }
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.priceCollectionView) {
        SpecialNumPriceCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SpecialNumPriceCell" forIndexPath:indexPath];
        cell.priceLabel.text = self.priceArray[indexPath.section];
        if (indexPath.section == self.selectedPriceIndex) {
            cell.priceLabel.textColor = [UIColor colorWithHexString:@"232426"];
            cell.selectedView.hidden = NO;
        } else {
            cell.priceLabel.textColor = [UIColor colorWithHexString:@"999999"];
            cell.selectedView.hidden = YES;
        }
        return cell;
    } else {
        SpecialNumListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SpecialNumListCell" forIndexPath:indexPath];
        cell.model = self.dataArray[indexPath.row];
        cell.delegate = self;
        return cell;
    }
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.priceCollectionView && indexPath.section != self.selectedPriceIndex) {
        self.selectedPriceIndex = indexPath.section;
        [self.priceCollectionView reloadData];
        if (![SPDCommonTool currentVersionIsArbic] || [[UIDevice currentDevice].systemVersion floatValue] >= 11.0) {
            [self.priceCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
        
        NSString *price = self.priceArray[self.selectedPriceIndex];
        if (self.selectedPriceIndex == 0) {
            self.priceRange = nil;
        } else if (self.selectedPriceIndex == self.priceArray.count - 1) {
            self.priceRange = [price stringByReplacingOccurrencesOfString:@"+" withString:@","];
        } else {
            self.priceRange = [price stringByReplacingOccurrencesOfString:@"-" withString:@","];
        }
        self.page = 0;
        [self requestSpecialNumListWithnimated:YES];
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.priceCollectionView) {
        NSString *price = self.priceArray[indexPath.section];
        CGFloat width = [price boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 50) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16]} context:nil].size.width;
        return CGSizeMake(ceilf(width) + 15 * 2, 50);
    } else {
        return CGSizeMake((kScreenW - 5 * 2 - 2) / 2, 159);
    }
}

#pragma mark - SpecialNumListCellDelegate

- (void)didClickBuySpecialNumWithModel:(SpecialNumModel *)model {
    if (self.mySpecialNum) {
        [self showToast:SPDStringWithKey(@"你已拥有靓号ID，不可再重复购买", nil)];
    } else {
        NSString *title = [NSString stringWithFormat:SPDStringWithKey(@"确定花费%@金币购买%@天靓号ID？", nil), model.price, model.day];
        [self presentCommonController:title messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
            [self requestSpecialNumBuyWithModel:model];
        }];
    }
}

#pragma mark - Setters and Getters

- (NSMutableArray *)priceArray {
    if (!_priceArray) {
        _priceArray = [NSMutableArray array];
        [_priceArray addObject:@"All"];
    }
    return _priceArray;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

@end
