//
//  SpecialNumViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2018/7/27.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SpecialNumViewController.h"
#import "SpecialNumHeaderCell.h"
#import "SpecialNumListCell.h"
#import "SpecialNumModel.h"
#import "SpecialNumCategoryViewController.h"

@interface SpecialNumViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, SpecialNumHeaderCellDelegate, SpecialNumListCellDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) SpecialNumModel *mySpecialNum;
@property (nonatomic, copy) NSString *balance;

@end

@implementation SpecialNumViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = SPDStringWithKey(@"靓号ID", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"family_help"] style:UIBarButtonItemStylePlain target:self action:@selector(clickRightBarButtonItem:)];
    [self.view addSubview:self.collectionView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self requestSpecialNumList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Event response

- (void)clickRightBarButtonItem:(UIBarButtonItem *)sender {
    [self loadWebViewController:self title:@"" url:SPECIALNUM_HELP_URL];
}

#pragma mark - Private methods

- (void)requestSpecialNumList {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"specialNum.hot" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        NSDictionary *mine = suceess[@"mine"];
        if (mine.count) {
            self.mySpecialNum = [SpecialNumModel initWithDictionary:mine];
        }
        
        [self.dataArray removeAllObjects];
        for (NSDictionary *dic in suceess[@"list"]) {
            if (!self.mySpecialNum || ![dic[@"specialNum"] isEqualToString:self.mySpecialNum.specialNum]) {
                SpecialNumModel *model = [SpecialNumModel initWithDictionary:dic];
                [self.dataArray addObject:model];
            }
        }
        [self requestMyBalance];
        [self.collectionView reloadData];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestMyBalance {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.balance" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.balance = [NSString stringWithFormat:SPDStringWithKey(@"金币余额: %@", nil), suceess[@"balance"]];
        if (self.dataArray.count) {
            [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]]];
        }
    } bFailure:^(id  _Nullable failure) {

    }];
}

- (void)requestSpecialNumBuyWithModel:(SpecialNumModel *)model {
    NSDictionary *dic = @{@"specialNum": model.specialNum};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"specialNum.buy" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.mySpecialNum = [SpecialNumModel initWithDictionary:suceess];
        [self.dataArray removeObject:model];
        [self.collectionView reloadData];
        [self requestMyBalance];
    } bFailure:^(id  _Nullable failure) {
        if ([failure isKindOfClass:[NSDictionary class]]) {
            if ([failure[@"code"] integerValue] == 2003) {
                [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                    [self pushToRechageController];
                }];
            } else if ([failure[@"code"] integerValue] == 2004) {
                [self showToast:SPDStringWithKey(@"你已拥有靓号ID，不可再重复购买", nil)];
            } else {
                [self showToast:failure[@"msg"]];
            }
        }
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.dataArray.count ? 2 : 0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else {
        return self.dataArray.count;
    }
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        SpecialNumHeaderCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SpecialNumHeaderCell" forIndexPath:indexPath];
        cell.delegate = self;
        cell.balanceStr = self.balance;
        if (self.mySpecialNum) {
            cell.model = self.mySpecialNum;
        }
        return cell;
    } else {
        SpecialNumListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SpecialNumListCell" forIndexPath:indexPath];
        cell.delegate = self;
        cell.model = self.dataArray[indexPath.row];
        return cell;
    }
}

#pragma mark - UICollectionViewDelegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return CGSizeMake(kScreenW, self.mySpecialNum ? 235 : 210);
    } else {
        return CGSizeMake((kScreenW - 5 * 2 - 2) / 2, 159);
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (section == 0) {
        return UIEdgeInsetsZero;
    } else {
        return UIEdgeInsetsMake(0.0f, 5.0f, 0.0f, 5.0f);
    }
}

#pragma mark - SpecialNumHeaderCellDelegate

- (void)didClickRenewSpecialNumWithModel:(SpecialNumModel *)model {
    NSString *title = [NSString stringWithFormat:SPDStringWithKey(@"确定花费%@金币续费%@天靓号ID？", nil), model.price, model.day];
    [self presentCommonController:title messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
        [self requestSpecialNumBuyWithModel:model];
    }];
}

- (void)didClickSearchButton {
    [self.navigationController pushViewController:[SpecialNumCategoryViewController new] animated:YES];
}

#pragma mark - SpecialNumListCellDelegate

- (void)didClickBuySpecialNumWithModel:(SpecialNumModel *)model {
    if (self.mySpecialNum) {
        [self showToast:SPDStringWithKey(@"你已拥有靓号ID，不可再重复购买", nil)];
    } else {
        NSString *title = [NSString stringWithFormat:SPDStringWithKey(@"确定花费%@金币购买%@天靓号ID？", nil), model.price, model.day];
        [self presentCommonController:title messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
            [self requestSpecialNumBuyWithModel:model];
            
            //[MobClick event:@"clickSpecialIDOk"];
        }];
        
        //[MobClick event:@"clickSpecialIDBuy"];
    }
}

#pragma mark - Setters and Getters

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.minimumLineSpacing = 0;
        flowLayout.minimumInteritemSpacing = 0;
        _collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.showsVerticalScrollIndicator = NO;
        [_collectionView registerNib:[UINib nibWithNibName:@"SpecialNumHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"SpecialNumHeaderCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"SpecialNumListCell" bundle:nil] forCellWithReuseIdentifier:@"SpecialNumListCell"];
    }
    return _collectionView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

@end
