//
//  SpecialNumCategoryViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2019/7/15.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "SpecialNumCategoryViewController.h"
#import "SpecialNumCategoryCell.h"
#import "SpecialNumListViewController.h"
#import "SpecialNumSearchViewController.h"

@interface SpecialNumCategoryViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, SpecialNumCategoryCellDelegate>

@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchButtonTop;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;

@property (nonatomic, strong) NSArray *categoryArray;
@property (nonatomic, strong) NSMutableArray *egArray;
@property (nonatomic, strong) NSArray *typeArray;

@end

@implementation SpecialNumCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = SPDStringWithKey(@"靓号ID", nil);
    self.searchButtonTop.constant = NavHeight + 10;
    [self.searchButton setTitle:SPDStringWithKey(@"输入2—5位数字搜索", nil) forState:UIControlStateNormal];
    if ([SPDCommonTool currentVersionIsArbic]) {
        self.searchButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
        self.searchButton.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    }
    self.categoryLabel.text = SPDStringWithKey(@"靓号分类", nil);
    [self.collectionView registerNib:[UINib nibWithNibName:@"SpecialNumCategoryCell" bundle:nil] forCellWithReuseIdentifier:@"SpecialNumCategoryCell"];
    self.flowLayout.itemSize = CGSizeMake(kScreenW, 92);
}

#pragma mark - Event response

- (IBAction)clickSearchButton:(UIButton *)sender {
    [self.navigationController pushViewController:[SpecialNumSearchViewController new] animated:YES];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 3;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SpecialNumCategoryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SpecialNumCategoryCell" forIndexPath:indexPath];
    cell.categoryLabel.text = self.categoryArray[indexPath.row];
    cell.egLabel.attributedText = self.egArray[indexPath.row];
    NSArray *typeArray = self.typeArray[indexPath.row];
    [cell.typeButton1 setTitle:typeArray[0] forState:UIControlStateNormal];
    [cell.typeButton2 setTitle:typeArray[1] forState:UIControlStateNormal];
    if (typeArray.count > 2) {
        cell.typeButton3.hidden = NO;
        [cell.typeButton3 setTitle:typeArray[2] forState:UIControlStateNormal];
    } else {
        cell.typeButton3.hidden = YES;
    }
    cell.delegate = self;
    return cell;
}

#pragma mark - SpecialNumCategoryCellDelegate

- (void)didClickTypeButtonWithType:(NSString *)type {
    SpecialNumListViewController *vc = [SpecialNumListViewController new];
    vc.type = type;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Setters and Getters

- (NSArray *)categoryArray {
    if (!_categoryArray) {
        _categoryArray = @[SPDStringWithKey(@"成对", nil), SPDStringWithKey(@"重复", nil), SPDStringWithKey(@"序列", nil)];
    }
    return _categoryArray;
}

- (NSMutableArray *)egArray {
    if (!_egArray) {
        _egArray = [NSMutableArray array];

        NSArray *textArray = @[@"(AABB e.g.:1100)", @"(AAAA e.g.:1111)", @"(ABCDE e.g.:12345)"];
        NSArray *numArray = @[@"1100", @"1111", @"12345"];
        for (NSInteger i = 0; i < textArray.count; i++) {
             NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:textArray[i]];
             [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"FDCB03"] range:[textArray[i] rangeOfString:numArray[i]]];
            [_egArray addObject:attributedString];
        }
    }
    return _egArray;
}

- (NSArray *)typeArray {
    if (!_typeArray) {
        _typeArray = @[@[@"AABB", @"ABAB"],
                       @[@"AAA", @"AAAA", @"AAAAA"],
                       @[@"ABC", @"ABCD", @"ABCDE"]];
    }
    return _typeArray;
}

@end
