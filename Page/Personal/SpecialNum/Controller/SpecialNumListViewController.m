//
//  SpecialNumListViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2019/7/15.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "SpecialNumListViewController.h"
#import "SpecialNumListCell.h"
#import "SpecialNumModel.h"

@interface SpecialNumListViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, SpecialNumListCellDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) SpecialNumModel *mySpecialNum;
@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation SpecialNumListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = SPDStringWithKey(@"靓号ID", nil);
    [self.view addSubview:self.collectionView];
    [self requestSpecialNumList];
}

#pragma mark - Private methods

- (void)requestSpecialNumList {
    NSDictionary *dic = @{@"type": self.type, @"onsale": @(1), @"page": @(self.page)};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"specialNum.list" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        NSDictionary *mine = suceess[@"mine"];
        if (mine.count) {
            self.mySpecialNum = [SpecialNumModel initWithDictionary:mine];
        }
        
        if (self.page == 0) {
            [self.dataArray removeAllObjects];
        }
        NSArray *list = suceess[@"list"];
        for (NSDictionary *dic in suceess[@"list"]) {
            SpecialNumModel *model = [SpecialNumModel initWithDictionary:dic];
            if (![SPDCommonTool isEmpty:model.highlight]) {
                model.attributedSpecialNum = [[NSMutableAttributedString alloc] initWithString:model.specialNum];
                [model.attributedSpecialNum addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"FDCB03"] range:[model.specialNum rangeOfString:model.highlight]];
            }
            [self.dataArray addObject:model];
        }
        [self.collectionView reloadData];
        self.page++;
        
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
        self.collectionView.mj_footer.hidden = !list.count;
    } bFailure:^(id  _Nullable failure) {
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
    }];
}

- (void)requestSpecialNumBuyWithModel:(SpecialNumModel *)model {
    NSDictionary *dic = @{@"specialNum": model.specialNum};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"specialNum.buy" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.mySpecialNum = [SpecialNumModel initWithDictionary:suceess];
        [self.dataArray removeObject:model];
        [self.collectionView reloadData];
    } bFailure:^(id  _Nullable failure) {
        if ([failure isKindOfClass:[NSDictionary class]]) {
            if ([failure[@"code"] integerValue] == 2003) {
                [self presentCommonController:SPDStringWithKey(@"金币不足，请充值", nil) messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
                    [self pushToRechageController];
                }];
            } else if ([failure[@"code"] integerValue] == 2004) {
                [self showToast:SPDStringWithKey(@"你已拥有靓号ID，不可再重复购买", nil)];
            } else {
                [self showToast:failure[@"msg"]];
            }
        }
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SpecialNumListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SpecialNumListCell" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    cell.delegate = self;
    return cell;
}

#pragma mark - SpecialNumListCellDelegate

- (void)didClickBuySpecialNumWithModel:(SpecialNumModel *)model {
    if (self.mySpecialNum) {
        [self showToast:SPDStringWithKey(@"你已拥有靓号ID，不可再重复购买", nil)];
    } else {
        NSString *title = [NSString stringWithFormat:SPDStringWithKey(@"确定花费%@金币购买%@天靓号ID？", nil), model.price, model.day];
        [self presentCommonController:title messageString:@"" cancelTitle:SPDStringWithKey(@"取消", nil) cancelAction:nil confirmTitle:SPDStringWithKey(@"确定", nil) confirmAction:^{
            [self requestSpecialNumBuyWithModel:model];
        }];
    }
}

#pragma mark - Setters and Getters

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake((kScreenW - 5 * 2 - 2) / 2, 159);
        flowLayout.sectionInset = UIEdgeInsetsMake(2, 5, 0, 5);
        flowLayout.minimumLineSpacing = 0;
        flowLayout.minimumInteritemSpacing = 0;
        _collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [_collectionView registerNib:[UINib nibWithNibName:@"SpecialNumListCell" bundle:nil] forCellWithReuseIdentifier:@"SpecialNumListCell"];
        
        __weak typeof(self) weakSelf = self;
        _collectionView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            weakSelf.page = 0;
            [weakSelf requestSpecialNumList];
        }];
        _collectionView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            [weakSelf requestSpecialNumList];
        }];
        _collectionView.mj_footer.hidden = YES;
    }
    return _collectionView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

@end
