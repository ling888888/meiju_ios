//
//  SpecialNumPriceCell.h
//  SimpleDate
//
//  Created by 李楠 on 2019/7/16.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SpecialNumPriceCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIView *selectedView;

@end

NS_ASSUME_NONNULL_END
