//
//  SpecialNumHeaderCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/7/27.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SpecialNumModel;

@protocol SpecialNumHeaderCellDelegate <NSObject>

@optional

- (void)didClickRenewSpecialNumWithModel:(SpecialNumModel *)model;
- (void)didClickSearchButton;

@end

@interface SpecialNumHeaderCell : UICollectionViewCell

@property (nonatomic, strong) SpecialNumModel *model;
@property (nonatomic, copy) NSString *balanceStr;
@property (nonatomic, weak) id<SpecialNumHeaderCellDelegate> delegate;

@end
