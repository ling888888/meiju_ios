//
//  SpecialNumCategoryCell.m
//  SimpleDate
//
//  Created by 李楠 on 2019/7/15.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "SpecialNumCategoryCell.h"

@implementation SpecialNumCategoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (IBAction)clickTypeButton:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickTypeButtonWithType:)]) {
        [self.delegate didClickTypeButtonWithType:sender.titleLabel.text];
    }
}

@end
