//
//  SpecialNumListCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/7/27.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SpecialNumListCell.h"
#import "SpecialNumModel.h"

@interface SpecialNumListCell ()

@property (weak, nonatomic) IBOutlet UIImageView *activityImageView;
@property (weak, nonatomic) IBOutlet UILabel *activityLabel;
@property (weak, nonatomic) IBOutlet UILabel *specialNumLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *specialNumLabelCenterX;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *priceLabelCenterX;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;

@end

@implementation SpecialNumListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.activityImageView.image = [UIImage imageNamed:@"specialNum_activity_bg"].adaptiveRtl;
    if ([SPDCommonTool currentVersionIsArbic]) {
        self.specialNumLabelCenterX.constant = -21;
        self.priceLabelCenterX.constant = -15;
    }
}

- (void)setModel:(SpecialNumModel *)model {
    _model = model;
    
    if (model.attributedSpecialNum) {
        self.specialNumLabel.textColor = [UIColor colorWithHexString:@"999999"];
        self.specialNumLabel.attributedText = model.attributedSpecialNum;
    } else {
        self.specialNumLabel.textColor = [UIColor colorWithHexString:@"FDCB03"];
        self.specialNumLabel.text = _model.specialNum;
    }
    self.priceLabel.text = [NSString stringWithFormat:@"%@", _model.price];
    self.activityImageView.hidden = [SPDCommonTool isEmpty:_model.activity];
    self.activityLabel.hidden = [SPDCommonTool isEmpty:_model.activity];
    self.activityLabel.text = _model.activity;
    self.buyButton.userInteractionEnabled = !_model.isSold;
    self.buyButton.backgroundColor = [UIColor colorWithHexString:_model.isSold ? @"c2c2c2" : @"ff7781"];
    [self.buyButton setTitle:SPDStringWithKey(_model.isSold ? @"已售" :@"购买", nil) forState:UIControlStateNormal];
}

- (IBAction)clickBuyButton:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickBuySpecialNumWithModel:)]) {
        [self.delegate didClickBuySpecialNumWithModel:self.model];
    }
}

@end
