//
//  SpecialNumListCell.h
//  SimpleDate
//
//  Created by 李楠 on 2018/7/27.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SpecialNumModel;

@protocol SpecialNumListCellDelegate <NSObject>

@optional

- (void)didClickBuySpecialNumWithModel:(SpecialNumModel *)model;

@end

@interface SpecialNumListCell : UICollectionViewCell

@property (nonatomic, strong) SpecialNumModel *model;
@property (nonatomic, weak) id<SpecialNumListCellDelegate> delegate;

@end
