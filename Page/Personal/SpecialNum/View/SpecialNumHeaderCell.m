//
//  SpecialNumHeaderCell.m
//  SimpleDate
//
//  Created by 李楠 on 2018/7/27.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SpecialNumHeaderCell.h"
#import "SpecialNumModel.h"

@interface SpecialNumHeaderCell ()

@property (weak, nonatomic) IBOutlet UILabel *userIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *doNotHaveLabel;
@property (weak, nonatomic) IBOutlet UIView *specialNumView;
@property (weak, nonatomic) IBOutlet UILabel *specialNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *originalIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *expireTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;
@property (weak, nonatomic) IBOutlet UILabel *hotLabel;
@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;

@end

@implementation SpecialNumHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.userIDLabel.text = [NSString stringWithFormat:@"ID: %@", [SPDCommonTool getInvitationCodeWithUserId:[SPDApiUser currentUser].userId]];
    self.doNotHaveLabel.text = SPDStringWithKey(@"您还没有靓号ID", nil);
    self.originalIDLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"(原ID: %@)", nil), [SPDCommonTool getInvitationCodeWithUserId:[SPDApiUser currentUser].userId]];
    [self.buyButton setTitle:SPDStringWithKey(@"续费", nil) forState:UIControlStateNormal];
    self.hotLabel.text = SPDStringWithKey(@"热门靓号", nil);
    [self.searchButton setTitle:SPDStringWithKey(@"输入2—5位数字搜索", nil) forState:UIControlStateNormal];
    if ([SPDCommonTool currentVersionIsArbic]) {
        self.searchButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
        self.searchButton.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    }
}

- (void)setModel:(SpecialNumModel *)model {
    _model = model;
    
    self.specialNumView.hidden = NO;
    self.specialNumLabel.text = _model.specialNum;
    self.priceLabel.text = [NSString stringWithFormat:@"%@", _model.price];
    self.expireTimeLabel.text = _model.expireTimeStr;
}

- (void)setBalanceStr:(NSString *)balanceStr {
    _balanceStr = balanceStr;
    
    self.balanceLabel.text = _balanceStr;
}

- (IBAction)clickBuyButton:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickRenewSpecialNumWithModel:)]) {
        [self.delegate didClickRenewSpecialNumWithModel:self.model];
    }
}

- (IBAction)clickSearchButton:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickSearchButton)]) {
        [self.delegate didClickSearchButton];
    }
}

@end
