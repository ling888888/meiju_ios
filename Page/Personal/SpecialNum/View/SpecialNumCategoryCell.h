//
//  SpecialNumCategoryCell.h
//  SimpleDate
//
//  Created by 李楠 on 2019/7/15.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SpecialNumCategoryCellDelegate <NSObject>

@optional

- (void)didClickTypeButtonWithType:(NSString *)type;

@end

@interface SpecialNumCategoryCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *egLabel;
@property (weak, nonatomic) IBOutlet UIButton *typeButton1;
@property (weak, nonatomic) IBOutlet UIButton *typeButton2;
@property (weak, nonatomic) IBOutlet UIButton *typeButton3;

@property (nonatomic, weak) id<SpecialNumCategoryCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
