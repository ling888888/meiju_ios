//
//  SpecialNumModel.m
//  SimpleDate
//
//  Created by 李楠 on 2018/8/2.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "SpecialNumModel.h"

@implementation SpecialNumModel

- (void)setExpireTime:(NSNumber *)expireTime {
    _expireTime = expireTime;
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setTimeZone:[NSTimeZone localTimeZone]];
    [format setDateFormat:@"yyyy/MM/dd"];
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:[_expireTime longLongValue] / 1000];
    self.expireTimeStr = [NSString stringWithFormat:SPDStringWithKey(@"%@到期", nil), [format stringFromDate:date]];
}

@end
