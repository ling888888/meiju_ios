//
//  SpecialNumModel.h
//  SimpleDate
//
//  Created by 李楠 on 2018/8/2.
//  Copyright © 2018年 WYB. All rights reserved.
//

#import "CommonModel.h"

@interface SpecialNumModel : CommonModel

@property (nonatomic, copy) NSString *specialNum;
@property (nonatomic, copy) NSNumber *price;
@property (nonatomic, copy) NSString *activity;
@property (nonatomic, assign) BOOL isSold;
@property (nonatomic, strong) NSNumber *day;
@property (nonatomic, strong) NSNumber *expireTime;
@property (nonatomic, copy) NSString *expireTimeStr;
@property (nonatomic, copy) NSString *highlight;
@property (nonatomic, strong) NSMutableAttributedString *attributedSpecialNum;

@end
