//
//  CallSettingsViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2019/3/26.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "CallSettingsViewController.h"
#import "SettingCell.h"

@interface CallSettingsViewController ()<UITableViewDataSource, UITableViewDelegate, SettingCellDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) BOOL videoSwitch;
@property (nonatomic, assign) BOOL audioSwitch;

@end

@implementation CallSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = SPDStringWithKey(@"通话设置", nil);
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:self.tableView];
    [self requestMyInfoDetail];
}

#pragma mark - Private methods

- (void)requestMyInfoDetail {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.info.detail" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.audioSwitch = [suceess[@"audio_enable"] boolValue];
        self.videoSwitch = [suceess[@"video_enable"] boolValue];
        [self.tableView reloadData];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestMyPhoneRing {
    NSDictionary *dic = @{@"audio_enable": self.audioSwitch ? @"true" : @"false",
                          @"video_enable": self.videoSwitch ? @"true" : @"false"};
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:dic] bURL:@"my.phone.ring" bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        
    } bFailure:^(id  _Nullable failure) {
        [self showToast:failure[@"msg"]];
        [self requestMyInfoDetail];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SettingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.actionSwitch.hidden = NO;
    cell.arrowImageView.hidden = YES;
    cell.descLabel.hidden = YES;
    cell.delegate = self;
    switch (indexPath.row) {
        case 0:
            cell.nameLabel.text = SPDStringWithKey(@"语音开关", nil);
            cell.actionSwitch.on = self.audioSwitch;
            break;
        case 1:
            cell.nameLabel.text = SPDStringWithKey(@"视频开关", nil);
            cell.actionSwitch.on = self.videoSwitch;
            break;
        default:
            break;
    }
    return cell;
}

#pragma mark - SettingCellDelegate

- (void)actionSwitch:(UISwitch *)sender valueChangedWithName:(NSString *)name {
    if ([name isEqualToString:SPDStringWithKey(@"语音开关", nil)]) {
        self.audioSwitch = sender.on;
        [self requestMyPhoneRing];
    } else if ([name isEqualToString:SPDStringWithKey(@"视频开关", nil)]) {
        self.videoSwitch = sender.on;
        [self requestMyPhoneRing];
    }
}

#pragma mark - Setters & Getters

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, NavHeight, kScreenW, kScreenH - NavHeight) style:UITableViewStyleGrouped];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.rowHeight = 50;
        _tableView.tableFooterView = [UIView new];
        [_tableView registerNib:[UINib nibWithNibName:@"SettingCell" bundle:nil] forCellReuseIdentifier:@"SettingCell"];
        
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
    }
    return _tableView;
}

@end
