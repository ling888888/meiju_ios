//
//  LY_EIPhotoView.h
//  SimpleDate
//
//  Created by jianyue on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HXPhotoPicker.h"

NS_ASSUME_NONNULL_BEGIN

@class LY_EIPhotoView;

@protocol LY_EIPhotoViewDelegate <NSObject>

- (void)photoView:(LY_EIPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal;

@end

@interface LY_EIPhotoView : UIView

@property(nonatomic, weak) id<LY_EIPhotoViewDelegate> delegate;

@property(nonatomic, copy) NSString *title;

@property(nonatomic, strong) HXPhotoManager *manager;

@property(nonatomic, strong) HXPhotoView *photoView;

@end

NS_ASSUME_NONNULL_END
