//
//  LY_EINicknameAlertView.h
//  SimpleDate
//
//  Created by jianyue on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LYPCustomAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@class LY_EINicknameAlertView;

@protocol LY_EINicknameAlertViewDelegate <NSObject>

- (void)nicknameAlertViewDidClickFinishBtnWith:(LY_EINicknameAlertView *)nicknameAlerView;

@end

@interface LY_EINicknameAlertView : LYPCustomAlertView

@property(nonatomic, weak) id<LY_EINicknameAlertViewDelegate> delegate;

@property(nonatomic, copy) NSString *value;

@property(nonatomic, assign) NSInteger maxChartCount;

@end

NS_ASSUME_NONNULL_END
