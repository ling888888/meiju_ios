//
//  LY_EIPortraitView.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_EIPortraitView.h"

@interface LY_EIPortraitView ()

@property(nonatomic, strong) UILabel *titleLabel;

@property(nonatomic, strong) UIImageView *moreIconView;

@end

@implementation LY_EIPortraitView

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#333333"];
        _titleLabel.font = [UIFont mediumFontOfSize:16];
    }
    return _titleLabel;
}

- (UIImageView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[UIImageView alloc] init];
        _portraitView.contentMode = UIViewContentModeScaleAspectFill;
        _portraitView.layer.cornerRadius = 23.5;
        _portraitView.layer.masksToBounds = true;
    }
    return _portraitView;
}

- (UIImageView *)moreIconView {
    if (!_moreIconView) {
        _moreIconView = [[UIImageView alloc] init];
        _moreIconView.image = [UIImage imageNamed:@"ic_zhibo_fensituan_gengduo"];
    }
    return _moreIconView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
        [self setupDefaultData];
        
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickSelfAction)]];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.titleLabel];
    [self addSubview:self.moreIconView];
    [self addSubview:self.portraitView];
}

- (void)setupUIFrame {
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(25);
        make.bottom.mas_equalTo(-28);
    }];
    [self.moreIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-15);
        make.size.mas_equalTo(CGSizeMake(6, 10));
        make.centerY.mas_equalTo(0);
    }];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(47);
        make.trailing.mas_equalTo(self.moreIconView.mas_leading).offset(-15);
        make.top.mas_equalTo(11.5);
        make.bottom.mas_equalTo(-11.5);
    }];
}

- (void)setupDefaultData {
    
}

- (void)setTitle:(NSString *)title {
    _title = title;
    
    self.titleLabel.text = title;
}

- (void)setUrl:(NSString *)url {
    _url = url;
    
    [self.portraitView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:url]] placeholderImage:[UIImage imageNamed:@""]];
    
}

- (void)clickSelfAction {
    if ([self.delegate respondsToSelector:@selector(portraitViewDidClickWith:)]) {
        [self.delegate portraitViewDidClickWith:self];
    }
}

@end
