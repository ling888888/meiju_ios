//
//  LY_EINicknameAlertView.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_EINicknameAlertView.h"

@interface LY_EINicknameAlertView () <UITextFieldDelegate>

@property(nonatomic, strong) UILabel *titleLabel;

@property(nonatomic, strong) UIButton *closeBtn;

@property(nonatomic, strong) UITextField *textView;

@property(nonatomic, strong) UIButton *finishBtn;

@end

@implementation LY_EINicknameAlertView

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _titleLabel.font = [UIFont mediumFontOfSize:18];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.text = @"修改昵称".localized;
    }
    return _titleLabel;
}

- (UIButton *)closeBtn {
    if (!_closeBtn) {
        _closeBtn = [[UIButton alloc] init];
        [_closeBtn setImage:[UIImage imageNamed:@"ic_tongyong_shanchu"] forState:UIControlStateNormal];
        [_closeBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeBtn;
}

- (UITextField *)textView {
    if (!_textView) {
        _textView = [[UITextField alloc] init];
        _textView.placeholder = @"请输入你的昵称".localized;
//        _textView.placeholderTextColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.6];
        _textView.font = [UIFont systemFontOfSize:14];
        _textView.backgroundColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.06];
        _textView.layer.cornerRadius = 5;
        _textView.layer.masksToBounds = true;
        _textView.delegate = self;
    }
    return _textView;
}

- (UIButton *)finishBtn {
    if (!_finishBtn) {
        _finishBtn = [[UIButton alloc] init];
        [_finishBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _finishBtn.titleLabel.font = [UIFont mediumFontOfSize:15];
        [_finishBtn setTitle:@"确定".localized forState:UIControlStateNormal];
        [_finishBtn setBackgroundImage:[UIImage imageWithUIColor:[UIColor colorWithHexString:@"#D0D0D0"]] forState:UIControlStateNormal];
        [_finishBtn setBackgroundImage:[UIImage imageWithUIColor:[UIColor colorWithHexString:@"#00D4A0"]] forState:UIControlStateSelected];
        _finishBtn.layer.cornerRadius = 16.5;
        _finishBtn.layer.masksToBounds = true;
        [_finishBtn addTarget:self action:@selector(finishBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _finishBtn;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self.containerView addSubview:self.titleLabel];
    [self.containerView addSubview:self.closeBtn];
    [self.containerView addSubview:self.textView];
    [self.containerView addSubview:self.finishBtn];
}

- (void)setupUIFrame {
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.centerY.mas_equalTo(-50);
        make.width.mas_equalTo(CONTAINERDEFAULTWIDTH);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.centerX.mas_equalTo(self.containerView);
    }];
    [self.closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(35);
        make.top.trailing.mas_equalTo(self.containerView);
    }];
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(63, 22, 69, 22));
        make.height.mas_equalTo(50);
    }];
    [self.finishBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(115, 33));
        make.bottom.mas_equalTo(-15);
        make.centerX.mas_equalTo(0);
    }];
}

- (void)setValue:(NSString *)value {
    _value = value;
    
    self.textView.text = value;
    if (self.textView.text == nil || [self.textView.text isEqualToString:@""]) {
        [self.finishBtn setSelected:false];
        self.finishBtn.userInteractionEnabled = false;
    } else {
        [self.finishBtn setSelected:true];
        self.finishBtn.userInteractionEnabled = true;
    }
}

- (void)finishBtnAction {
    [self dismiss];
    if ([self.delegate respondsToSelector:@selector(nicknameAlertViewDidClickFinishBtnWith:)]) {
        [self.delegate nicknameAlertViewDidClickFinishBtnWith:self];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *text = [textField changeCharactersInRange:range replacementString:string];
    
    if (text.length > self.maxChartCount) {
        return false;
    }
    _value = text;
    if (text == nil || [text isEqualToString:@""]) {
        [self.finishBtn setSelected:false];
        self.finishBtn.userInteractionEnabled = false;
    } else {
        [self.finishBtn setSelected:true];
        self.finishBtn.userInteractionEnabled = true;
    }
    
    return true;
}

@end
