//
//  LY_EISynopsisAlertView.h
//  SimpleDate
//
//  Created by jianyue on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LYPCustomAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@class LY_EISynopsisAlertView;

@protocol LY_EISynopsisAlertViewDelegate <NSObject>

- (void)synopsisAlertViewDidClickFinishBtnWith:(LY_EISynopsisAlertView *)synopsisAlertView;

@end


@interface LY_EISynopsisAlertView : LYPCustomAlertView

@property(nonatomic, weak) id<LY_EISynopsisAlertViewDelegate> delegate;

@property(nonatomic, copy) NSString *value;

@end

NS_ASSUME_NONNULL_END
