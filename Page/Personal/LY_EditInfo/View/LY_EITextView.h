//
//  LY_EITextView.h
//  SimpleDate
//
//  Created by jianyue on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LY_EITextView;

@protocol LY_EITextViewDelegate <NSObject>

- (void)textViewDidClickWith:(LY_EITextView *)textView;

@end

@interface LY_EITextView : UIView

@property(nonatomic, weak) id<LY_EITextViewDelegate> delegate;

@property(nonatomic, copy) NSString *title;

@property(nonatomic, copy) NSString *value;

@property(nonatomic, assign) BOOL isGender;

@property(nonatomic, copy) NSString *valuePlaceholder;

@end

NS_ASSUME_NONNULL_END
