//
//  LY_EITextView.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_EITextView.h"

@interface LY_EITextView ()

@property(nonatomic, strong) UILabel *titleLabel;

@property(nonatomic, strong) UIImageView *moreIconView;

@property(nonatomic, strong) UILabel *valueLabel;

@end

@implementation LY_EITextView

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#333333"];
        _titleLabel.font = [UIFont mediumFontOfSize:16];
    }
    return _titleLabel;
}

- (UIImageView *)moreIconView {
    if (!_moreIconView) {
        _moreIconView = [[UIImageView alloc] init];
        _moreIconView.image = [UIImage imageNamed:@"ic_zhibo_fensituan_gengduo"];
    }
    return _moreIconView;
}

- (UILabel *)valueLabel {
    if (!_valueLabel) {
        _valueLabel = [[UILabel alloc] init];
        _valueLabel.textColor = [UIColor colorWithHexString:@"#333333"];
        _valueLabel.font = [UIFont mediumFontOfSize:15];
        _valueLabel.numberOfLines = 0;
    }
    return _valueLabel;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
        [self setupDefaultData];
        
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickSelfAction)]];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.titleLabel];
    [self addSubview:self.moreIconView];
    [self addSubview:self.valueLabel];
}

- (void)setupUIFrame {
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(25);
    }];
    [self.moreIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-15);
        make.size.mas_equalTo(CGSizeMake(6, 10));
        make.centerY.mas_equalTo(0);
    }];
    [self.valueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(self.moreIconView.mas_leading).offset(-15);
        make.top.mas_equalTo(26);
        make.bottom.mas_equalTo(-26);
        make.width.mas_lessThanOrEqualTo(170);
    }];
}

- (void)setupDefaultData {
    
}

- (void)setTitle:(NSString *)title {
    _title = title;
    
    self.titleLabel.text = title;
}

- (void)setValue:(NSString *)value {
    _value = value;
    
    [self checkValuePlaceholder];
}

- (void)setValuePlaceholder:(NSString *)valuePlaceholder {
    _valuePlaceholder = valuePlaceholder;
    
    [self checkValuePlaceholder];
}

- (void)checkValuePlaceholder {
    if (self.value == nil || [self.value isEqualToString:@""]) {
        self.valueLabel.text = self.valuePlaceholder;
        self.valueLabel.textColor = [[UIColor colorWithHexString:@"#333333"] colorWithAlphaComponent:0.6];
    } else {
        if (self.isGender) {
            self.valueLabel.text = [self.value isEqualToString:@"male"] ? @"男".localized : @"女".localized;
        } else {
            self.valueLabel.text = self.value;
        }
        self.valueLabel.textColor = [UIColor colorWithHexString:@"#333333"];
    }
}

- (void)clickSelfAction {
    if ([self.delegate respondsToSelector:@selector(textViewDidClickWith:)]) {
        [self.delegate textViewDidClickWith:self];
    }
}

@end
