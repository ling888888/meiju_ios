//
//  LY_EIPortraitView.h
//  SimpleDate
//
//  Created by jianyue on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LY_EIPortraitView;

@protocol LY_EIPortraitViewDelegate <NSObject>

- (void)portraitViewDidClickWith:(LY_EIPortraitView *)portraitView;

@end

@interface LY_EIPortraitView : UIView

@property(nonatomic, weak) id<LY_EIPortraitViewDelegate> delegate;

@property(nonatomic, strong) UIImageView *portraitView;

@property(nonatomic, copy) NSString *title;

@property(nonatomic, copy) NSString *url;

@end

NS_ASSUME_NONNULL_END
