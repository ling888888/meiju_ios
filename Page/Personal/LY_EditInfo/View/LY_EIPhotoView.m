//
//  LY_EIPhotoView.m
//  SimpleDate
//
//  Created by jianyue on 2020/5/6.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_EIPhotoView.h"
//#import "HXPhotoPicker.h"


@interface LY_EIPhotoView ()  <HXPhotoViewDelegate>

@property(nonatomic, strong) UILabel *titleLabel;

//@property(nonatomic, strong) HXDatePhotoToolManager *datePhotoToolManager;

@end

@implementation LY_EIPhotoView

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#333333"];
        _titleLabel.font = [UIFont mediumFontOfSize:16];
        
    }
    return _titleLabel;
}

- (HXPhotoManager *)manager {
    if (!_manager) {
        _manager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhotoAndVideo];
        _manager.configuration.saveSystemAblum = YES;
        _manager.configuration.photoMaxNum = 0;
        _manager.configuration.videoMaxNum = 0;
        _manager.configuration.rowCount = 4;
        _manager.configuration.maxNum = 8;
        _manager.configuration.reverseDate = YES;
        _manager.configuration.selectTogether = YES;
    }
    return _manager;
}

- (HXPhotoView *)photoView {
    if (!_photoView) {
        _photoView = [HXPhotoView photoManager:self.manager];
        _photoView.frame = CGRectMake(15, 10, kScreenW - 30, 100);
        _photoView.lineCount = 4;
        _photoView.spacing = 5;
        _photoView.previewStyle = HXPhotoViewPreViewShowStyleDark;
        _photoView.outerCamera = YES;
        _photoView.delegate = self;
        
//        img_wodezhuye_xiangce_shiping
    }
    return _photoView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
        [self setupDefaultData];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.titleLabel];
    [self addSubview:self.photoView];
}

- (void)setupUIFrame {
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(25);
    }];
    [self.photoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(10);
        make.leading.mas_equalTo(15);
        make.trailing.mas_equalTo(-15);
        make.bottom.mas_equalTo(-10);
        make.height.mas_equalTo(100);
    }];
}

- (void)setupDefaultData {
    
}

- (void)setTitle:(NSString *)title {
    _title = title;
    
    self.titleLabel.text = title;
}

- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal {
    
    if ([self.delegate respondsToSelector:@selector(photoView:changeComplete:photos:videos:original:)]) {
        [self.delegate photoView:self changeComplete:allList photos:photos videos:videos original:isOriginal];
    }
}

- (void)photoView:(HXPhotoView *)photoView updateFrame:(CGRect)frame
{
    [self.photoView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(frame.size.height);
    }];
}

@end
