//
//  LY_EditInfoViewController.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/3.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_EditInfoViewController.h"
#import "LY_EIPortraitView.h"
#import "LY_EITextView.h"
#import "LY_EIPhotoView.h"

#import "LY_EISynopsisAlertView.h"
#import "LY_EINicknameAlertView.h"

#import "LY_PickerView.h"
#import "LY_DatePickerView.h"
#import "LY_HomePageInfo.h"


@interface LY_EditInfoViewController () <LY_EIPortraitViewDelegate, LY_EITextViewDelegate, LY_EISynopsisAlertViewDelegate, LY_EINicknameAlertViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, LY_EIPhotoViewDelegate>

/// 保存按钮
@property(nonatomic, strong) UIButton *saveBtn;

/// 滑动视图
@property(nonatomic, strong) UIScrollView *scrollView;

/// 滑动视图的内容视图（目的为了自动填充高度）
@property(nonatomic, strong) UIView *containerView;

@property(nonatomic, strong) LY_EIPortraitView *portraitView;

@property(nonatomic, strong) LY_EITextView *nicknameView;

@property(nonatomic, strong) LY_EITextView *genderView;

@property(nonatomic, strong) LY_EITextView *synopsisView;

@property(nonatomic, strong) LY_EITextView *birthdayView;

@property(nonatomic, strong) LY_EIPhotoView *photoView;

@property(nonatomic, strong) UIImagePickerController *imagePickerController;

@property(nonatomic, strong) UIImage *selectedPortraitImg;

@property(nonatomic, strong) LY_HomePageInfo *mine;

@property(nonatomic, strong) NSArray<HXPhotoModel *> *photos;

@end

@implementation LY_EditInfoViewController

- (UIButton *)saveBtn {
    if (!_saveBtn) {
        _saveBtn = [[UIButton alloc] init];
        [_saveBtn setTitleColor:[[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.6] forState:UIControlStateNormal];
        [_saveBtn setTitleColor:[UIColor colorWithHexString:@"#00D4A0"] forState:UIControlStateSelected];
        _saveBtn.titleLabel.font = [UIFont mediumFontOfSize:16];
        [_saveBtn setTitle:@"保存".localized forState:UIControlStateNormal];
        [_saveBtn addTarget:self action:@selector(saveBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _saveBtn;
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
    }
    return _scrollView;
}

- (UIView *)containerView {
    if (!_containerView) {
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor whiteColor];
    }
    return _containerView;
}

- (LY_EIPortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_EIPortraitView alloc] init];
        _portraitView.title = @"头像".localized;
        _portraitView.delegate = self;
    }
    return _portraitView;
}

- (LY_EITextView *)nicknameView {
    if (!_nicknameView) {
        _nicknameView = [[LY_EITextView alloc] init];
        _nicknameView.title = @"昵称".localized;
        _nicknameView.delegate = self;
    }
    return _nicknameView;
}

- (LY_EITextView *)genderView {
    if (!_genderView) {
        _genderView = [[LY_EITextView alloc] init];
        _genderView.title = @"性别".localized;
        _genderView.isGender = true;
        _genderView.delegate = self;
    }
    return _genderView;
}

- (LY_EITextView *)synopsisView {
    if (!_synopsisView) {
        _synopsisView = [[LY_EITextView alloc] init];
        _synopsisView.title = @"个人简介".localized;
        _synopsisView.valuePlaceholder = @"点击编辑你的个人简介".localized;
        _synopsisView.delegate = self;
    }
    return _synopsisView;
}

- (LY_EITextView *)birthdayView {
    if (!_birthdayView) {
        _birthdayView = [[LY_EITextView alloc] init];
        _birthdayView.title = @"生日".localized;
        _birthdayView.delegate = self;
    }
    return _birthdayView;
}

- (LY_EIPhotoView *)photoView {
    if (!_photoView) {
        _photoView = [[LY_EIPhotoView alloc] init];
        _photoView.title = @"个人相册".localized;
        _photoView.delegate = self;
    }
    return _photoView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavigation];
    
    [self setupUI];
    
    [self setupUIFrame];
    
    [self setupData];
    
    if (self.isGoPhotoViewController) {
        [self.photoView.photoView goPhotoViewController];
    }
    
    if (self.isPresentSynopsisAlertView) {
        LY_EISynopsisAlertView *alertView = [[LY_EISynopsisAlertView alloc] init];
        alertView.value = self.synopsisView.value;
        alertView.delegate = self;
        [alertView present];
    }
}

- (void)setupNavigation {
    self.title = @"编辑资料".localized;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.saveBtn];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
}

- (void)setupUI {
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.containerView];
    [self.containerView addSubview:self.portraitView];
    [self.containerView addSubview:self.nicknameView];
    [self.containerView addSubview:self.genderView];
    [self.containerView addSubview:self.synopsisView];
    [self.containerView addSubview:self.birthdayView];
    [self.containerView addSubview:self.photoView];
}

- (void)setupUIFrame {
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.mas_equalTo(self.view);
        make.top.bottom.mas_equalTo(self.scrollView);
    }];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.leading.trailing.mas_equalTo(0);
    }];
    [self.nicknameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.portraitView.mas_bottom);
        make.leading.trailing.mas_equalTo(0);
    }];
    [self.genderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nicknameView.mas_bottom);
        make.leading.trailing.mas_equalTo(0);
    }];
    [self.synopsisView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.genderView.mas_bottom);
        make.leading.trailing.mas_equalTo(0);
    }];
    [self.birthdayView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.synopsisView.mas_bottom);
        make.leading.trailing.mas_equalTo(0);
    }];
    [self.photoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.birthdayView.mas_bottom);
        make.leading.trailing.mas_equalTo(0);
        make.bottom.mas_equalTo(-20);
    }];
}

- (void)setupData {
    self.portraitView.url = [SPDApiUser currentUser].avatar;
    
    self.nicknameView.value = [SPDApiUser currentUser].nickName;
    
    self.genderView.value = [SPDApiUser currentUser].gender;
    
    self.synopsisView.value = @"";
    
    __weak typeof(self) weakSelf = self;
    NSDictionary *params = @{@"user_id": [SPDApiUser currentUser].userId};
    [LY_Network getRequestWithURL:LY_Api.user_detail parameters:params success:^(id  _Nullable response) {
        LY_HomePageInfo *mine = [LY_HomePageInfo yy_modelWithDictionary:response];
        weakSelf.mine = mine;
        weakSelf.portraitView.url = mine.avatar;
        weakSelf.nicknameView.value = mine.nick_name;
        weakSelf.genderView.value = mine.gender;
        weakSelf.synopsisView.value = mine.synopsis;
        weakSelf.birthdayView.value = mine.birthday;
        
        [weakSelf.photoView.manager addCustomAssetModel:mine.assetArray];
        [weakSelf.photoView.photoView refreshView];
    } failture:^(NSError * _Nullable error) {
        
    }];
}

- (UIImagePickerController *)imagePickerController {
    if (!_imagePickerController) {
        _imagePickerController = [[UIImagePickerController alloc] init];
        _imagePickerController.delegate = self;
        _imagePickerController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        // 设置是否可以管理已经存在的图片或者视频
        _imagePickerController.allowsEditing = true;
    }
    return _imagePickerController;
}

/// 开启相机相册
- (void)getImageFromPhotoLib:(UIImagePickerControllerSourceType)type {
    self.imagePickerController.sourceType = type;
    // 判断是否支持相机
    if ([UIImagePickerController isSourceTypeAvailable:type]) {
        [self presentViewController:self.imagePickerController animated:true completion:^{
            self.imagePickerController.delegate = self;
        }];
    }
}

- (BOOL)isDidChangeUserInfo {
    if (![self.mine.nick_name isEqualToString:self.nicknameView.value]) {
        return true;
    }
    if (![self.mine.gender isEqualToString:self.genderView.value]) {
        return true;
    }
    if (![self.mine.synopsis isEqualToString:self.synopsisView.value]) {
        return true;
    }
    if (![self.mine.birthday isEqualToString:self.birthdayView.value]) {
        return true;
    }
    if (self.selectedPortraitImg) {
        return true;
    }
    if (self.mine.albumList.count != self.photos.count) {
        return true;
    }
    return false;
}

- (void)backBtnAction {
    if ([self isDidChangeUserInfo]) {
        LYPSTextAlertView *view = [[LYPSTextAlertView alloc] init];
        view.title = @"是否保存新的资料？".localized;
        [view addAction:[LYPSystemAlertAction actionWithTitle:@"返回".localized style:LYPSystemAlertActionStyleCancel handler:^(LYPSystemAlertAction * _Nonnull action) {
            [super backBtnAction];
        }]];
        [view addAction:[LYPSystemAlertAction actionWithTitle:@"保存".localized style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
            [self saveBtnAction];
        }]];
        [view present];
    } else {
        [super backBtnAction];
    }
}

/// 保存按钮点击事件
- (void)saveBtnAction {
    [LY_HUD show];
    
    dispatch_group_t group = dispatch_group_create();
    NSMutableDictionary *params = [NSMutableDictionary dictionary];

    if (![self.mine.nick_name isEqualToString:self.nicknameView.value]) {
        [params setValue:self.nicknameView.value forKey:@"nick_name"];
    }
    if (![self.mine.gender isEqualToString:self.genderView.value]) {
        [params setValue:self.genderView.value forKey:@"gender"];
    }
    if (![self.mine.synopsis isEqualToString:self.synopsisView.value]) {
        [params setValue:self.synopsisView.value forKey:@"personal_desc"];
    }
    if (![self.mine.birthday isEqualToString:self.birthdayView.value]) {
        [params setValue:self.birthdayView.value forKey:@"birthday"];
    }
    dispatch_group_enter(group);
    NSMutableArray *images = [NSMutableArray array];
    if (self.selectedPortraitImg) {
        [images addObject:self.selectedPortraitImg];
    }
    [LY_Network postImageRequestWithURL:LY_Api.edit_mineInfo parameters:params imageParametersName:@"avatar" images:images success:^(id  _Nullable response) {

        dispatch_group_leave(group);

    } failture:^(NSError * _Nullable error) {
        dispatch_group_leave(group);
    }];
    
    dispatch_group_enter(group);
    [self requestDataForPhotoModels:self.photos datas:[NSMutableArray array] currentIndex:0 dataReceivedHandler:^(NSArray<NSDictionary *> *dictArray) {
        [LY_Network postImageRequestWithURL:LY_Api.edit_mineInfoPhoto parameters:[NSMutableDictionary dictionary] constructingBody:^(id<AFMultipartFormData>  _Nonnull formData) {
            NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
            NSString *timeStampString = [NSString stringWithFormat:@"%f", timeStamp];
            for (int i = 0; i < dictArray.count; i ++) {
                NSString *fileType = dictArray[i][@"fileType"];
                if ([dictArray[i].allKeys containsObject:@"data"]) {
                    NSData *data = dictArray[i][@"data"];
                    NSString *fileName = [NSString stringWithFormat:@"%@%@", timeStampString, fileType];
                    if ([fileType isEqualToString:@".png"]) {
                        [formData appendPartWithFileData:data name:@"photo" fileName:fileName mimeType:@"image/png"];
                    } else {
                        [formData appendPartWithFileData:data name:@"video" fileName:fileName mimeType:@"video/mp4"];
                    }
                }
            }
        } success:^(id  _Nullable response) {
            dispatch_group_leave(group);
        } failture:^(NSError * _Nullable error) {
            [LY_HUD showToastTips:error.localizedDescription];
            dispatch_group_leave(group);
        }];
    }];
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        NSLog(@"全部执行结束");
        [LY_HUD dismiss];
        [self.navigationController popViewControllerAnimated:true];
    });
}

- (void)requestDataForPhotoModels:(NSArray<HXPhotoModel *> *)models datas:(NSMutableArray *)datas currentIndex:(NSUInteger)index dataReceivedHandler:(void (^)(NSArray<NSDictionary *> *dictArray))handler {
    if (models.count == 0) {
        handler([datas copy]);
        return;
    }
    __block NSUInteger currentIndex = index;
    __weak typeof(self) weakSelf = self;
    [self requestDataForPhotoModel:models[index] dataReceivedHandler:^(NSData *data, NSString *fileType) {
     
        currentIndex++;
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObject:fileType forKey:@"fileType"];
  //@{
//            @"":fileType
//        };
        if (data) {
            [dict setObject:data forKey:@"data"];
        }
        [datas addObject:dict];
        // 如果currentIndex == imageUrls.count 则停止递归并返回
        if (currentIndex == models.count) {
            handler([datas copy]);
        } else {
            //继续递归...并将参数传递下去,不然Boloc无法正常回调
            [weakSelf requestDataForPhotoModels:models datas:datas currentIndex:currentIndex dataReceivedHandler:handler];
        }
    }];
}

- (void)requestDataForPhotoModel:(HXPhotoModel *)model
             dataReceivedHandler:(void (^)(NSData *data, NSString *fileType))handler {
    
    if (model.asset) {
        if (model.subType == HXPhotoModelMediaSubTypePhoto) {
            [[PHImageManager defaultManager] requestImageDataForAsset:model.asset options:nil resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
                handler(imageData, @".png");
            }];
        } else {
            [[PHImageManager defaultManager] requestAVAssetForVideo:model.asset options:nil resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
                AVURLAsset *urlAsset = (AVURLAsset *)asset;
                NSURL *url = urlAsset.URL;
                NSData *data = [NSData dataWithContentsOfURL:url];
                handler(data, @".mp4");
            }];
        }
    } else {
        if (model.subType == HXPhotoModelMediaSubTypePhoto) {
            NSData *imgData = UIImageJPEGRepresentation(model.previewPhoto, 1.0);
            handler(imgData, @".png");
        } else if (model.subType == HXPhotoModelMediaSubTypeVideo) {
            [model exportVideoWithPresetName:AVAssetExportPresetMediumQuality startRequestICloud:nil iCloudProgressHandler:nil exportProgressHandler:^(float progress, HXPhotoModel * _Nullable model) {
                // 导出视频时的进度，在iCloud下载完成之后
            } success:^(NSURL * _Nullable videoURL, HXPhotoModel * _Nullable model) {
                NSData *videoData = [NSData dataWithContentsOfURL:videoURL];
                handler(videoData, @".mp4");
            } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
                handler(nil, @".mp4");
            }];
        }
    }
}

// MARK: - LY_EIPortraitViewDelegate

- (void)portraitViewDidClickWith:(LY_EIPortraitView *)portraitView {
    LYPSActionSheetView *actionSheetView = [[LYPSActionSheetView alloc] init];
    [actionSheetView addAction:[LYPSystemAlertAction actionWithTitle:@"从相册中选择".localized style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
        [self getImageFromPhotoLib:UIImagePickerControllerSourceTypePhotoLibrary];
    }]];
    [actionSheetView addAction:[LYPSystemAlertAction actionWithTitle:@"拍照".localized style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
        [self getImageFromPhotoLib:UIImagePickerControllerSourceTypeCamera];
    }]];
    [actionSheetView present];
}

// MARK: - LY_EITextViewDelegate

- (void)textViewDidClickWith:(LY_EITextView *)textView {
    
    if (textView == self.nicknameView) {
        LY_EINicknameAlertView *alertView = [[LY_EINicknameAlertView alloc] init];
        alertView.value = self.nicknameView.value;
        alertView.maxChartCount = 20;
        alertView.delegate = self;
        [alertView present];
        
    } else if (textView == self.synopsisView) {
        LY_EISynopsisAlertView *alertView = [[LY_EISynopsisAlertView alloc] init];
        alertView.value = self.synopsisView.value;
        alertView.delegate = self;
        [alertView present];
    } else {
        // 判断是性别 还是 生日
        if (textView == self.genderView) {
            // 性别
            LY_PickerView *pickerView = [[LY_PickerView alloc] initWithStyle:LY_AlertViewStyleActionSheet];
            pickerView.itemArray = @[@"女".localized, @"男".localized];
            [pickerView showTopToolBarWithTitle:@""];
            [pickerView present];
            __weak typeof(self) weakSelf = self;
            [pickerView done:^(id  _Nullable result) {
                int index = [result intValue];
                NSLog(@"index:%d", index);
                weakSelf.genderView.value = [pickerView.itemArray[index] isEqualToString:@"男".localized] ? @"male" : @"female";
                self.saveBtn.selected = [self isDidChangeUserInfo];
            }];
        } else {
            // 生日
            LY_DatePickerView *datePickerView = [[LY_DatePickerView alloc] initWithStyle:LY_AlertViewStyleActionSheet];
            [datePickerView showTopToolBarWithTitle:@""];
            [datePickerView present];
            __weak typeof(self) weakSelf = self;
            [datePickerView done:^(id  _Nullable result) {
                NSLog(@"result:%@", result);
                weakSelf.birthdayView.value = result;
                self.saveBtn.selected = [self isDidChangeUserInfo];
            }];
        }
    }
}

// MARK: - LY_EISynopsisAlertViewDelegate
- (void)synopsisAlertViewDidClickFinishBtnWith:(LY_EISynopsisAlertView *)synopsisAlertView {
    self.synopsisView.value = synopsisAlertView.value;
    self.saveBtn.selected = [self isDidChangeUserInfo];
}

// MARK: - LY_EINicknameAlertViewDelegate
- (void)nicknameAlertViewDidClickFinishBtnWith:(LY_EINicknameAlertView *)nicknameAlerView {
    self.nicknameView.value = nicknameAlerView.value;
    self.saveBtn.selected = [self isDidChangeUserInfo];
}

// MARK: - LY_EIPhotoViewDelegate
- (void)photoView:(LY_EIPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal {
    self.photos = allList;
    self.saveBtn.selected = [self isDidChangeUserInfo];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info {
    [picker dismissViewControllerAnimated:true completion:nil];
    
    UIImage *img = info[@"UIImagePickerControllerEditedImage"];
    self.selectedPortraitImg = img;
    self.portraitView.portraitView.image = img;
    self.saveBtn.selected = [self isDidChangeUserInfo];
}


@end
