//
//  LY_EditInfoViewController.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/3.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LY_BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_EditInfoViewController : LY_BaseViewController

@property(nonatomic, assign) BOOL isGoPhotoViewController;

@property(nonatomic, assign) BOOL isPresentSynopsisAlertView;

@end

NS_ASSUME_NONNULL_END
