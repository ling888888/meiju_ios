//
//  OListNavView.m
//  SimpleDate
//
//  Created by houling on 2021/11/21.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "OListNavView.h"

@interface OListNavView ()

@property(nonatomic, strong) UILabel * titleLabel;
@property(nonatomic, strong) UIButton * cityBtn;
@property(nonatomic, strong) UIButton * searchBtn;

@end

@implementation OListNavView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self p_setui];
        [self p_setframe];
    }
    return self;
}

- (void)p_setui {
    [self addSubview:self.titleLabel];
    [self addSubview:self.cityBtn];
    [self addSubview:self.searchBtn];
}

- (void)p_setframe {
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(58);
    }];
    [self.cityBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.titleLabel.mas_trailing).offset(15);
        make.centerY.equalTo(self.titleLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(52, 24));
    }];
    [self.searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(18, 18));
        make.bottom.mas_equalTo(-19);
        make.trailing.mas_equalTo(-20);
    }];
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.text = @"找助教";
        _titleLabel.font = [UIFont boldSystemFontOfSize:18];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#131313"];
    }
    return _titleLabel;
}

- (UIButton *)cityBtn {
    if (!_cityBtn) {
        _cityBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cityBtn setTitle:@"全国" forState:UIControlStateNormal];
        _cityBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        [_cityBtn setBackgroundColor:[[UIColor colorWithHexString:@"#1DD2A2"] colorWithAlphaComponent:0.1]];
        _cityBtn.layer.cornerRadius = 12;
        _cityBtn.clipsToBounds = YES;
        [_cityBtn setTitleColor:[UIColor colorWithHexString:@"#1A1A1A"] forState:UIControlStateNormal];
    }
    return _cityBtn;
}

- (UIButton *)searchBtn {
    if (!_searchBtn) {
        _searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_searchBtn setImage:[UIImage imageNamed:@"ci_souusuo"] forState:UIControlStateNormal];
    }
    return _searchBtn;
}

@end
