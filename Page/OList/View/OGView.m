//
//  OGView.m
//  SimpleDate
//
//  Created by houling on 2021/11/21.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "OGView.h"

@interface OGCollectionCell: UICollectionViewCell

@property(nonatomic, strong)UIImageView * icon;
@property(nonatomic, strong)UILabel * title;
@property(nonatomic, copy)NSDictionary * dict;

@end

@implementation OGCollectionCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self p_setui];
        [self p_setframe];
    }
    return self;
}

- (void)p_setui {
    [self addSubview:self.icon];
    [self addSubview:self.title];
}
- (void)p_setframe {
    [self.icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(4);
        make.centerX.equalTo(self);
        make.size.mas_equalTo(43);
    }];
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.icon.mas_bottom).offset(4);
        make.centerX.equalTo(self);
    }];
}

- (void)setDict:(NSDictionary *)dict {
    _dict = [dict copy];
    self.icon.image = [UIImage imageNamed:dict[@"im"]];
    self.title.text = dict[@"title"];
}

- (UIImageView *)icon {
    if (!_icon) {
        _icon = [UIImageView new];
    }
    return _icon;
}

- (UILabel *)title {
    if (!_title) {
        _title = [UILabel new];
        _title.textColor = [UIColor colorWithHexString:@"#131313"];
        _title.font = [UIFont systemFontOfSize:13];
    }
    return _title;
}

@end


@interface OGView ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property(nonatomic, strong) UICollectionView * gcollection;
@property(nonatomic, strong) NSMutableArray * data;

@end

@implementation OGView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.gcollection];
        [self.gcollection mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo(10);
            make.top.bottom.mas_equalTo(0);
            make.trailing.mas_equalTo(-10);
        }];
        [self.gcollection reloadData];
    }
    return self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return  self.data.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    OGCollectionCell * cell = [_gcollection dequeueReusableCellWithReuseIdentifier:@"OGCollectionCell" forIndexPath:indexPath];
    if (cell && indexPath.row < self.data.count) {
        cell.dict = self.data[indexPath.row];
        return cell;
    }
    return  [UICollectionViewCell new];;
}

- (UICollectionView *)gcollection {
    if (!_gcollection) {
        UICollectionViewFlowLayout * ly = [UICollectionViewFlowLayout new];
        ly.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        ly.itemSize = CGSizeMake(70, 70);
        ly.minimumLineSpacing = 4;
        ly.minimumInteritemSpacing = 4;
        _gcollection = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:ly];
        _gcollection.delegate = self;
        _gcollection.dataSource = self;
        [_gcollection registerClass:[OGCollectionCell class] forCellWithReuseIdentifier:@"OGCollectionCell"];
    }
    return _gcollection;
}

- (NSMutableArray *)data {
    if (!_data) {
        _data = [@[@{@"im":@"ic_jubensha",@"title":@"剧本杀"},
                   @{@"im":@"ic_mishitaotuo",@"title":@"密室逃脱"}] mutableCopy];
    }
    return _data;
}

@end
