//
//  OListCollectionViewCell.m
//  SimpleDate
//
//  Created by houling on 2021/11/21.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "OListCollectionViewCell.h"
#import "LY_HPTagView.h"

@interface OListCollectionViewCell ()

@property(nonatomic, strong) UIImageView * coverImageView;
@property(nonatomic, strong) UILabel * nameLabel;
@property(nonatomic, strong) UILabel * distanceLabel;
@property(nonatomic, strong) UIView * onlineIcon;
@property(nonatomic, strong) UILabel * cityLabel;
@property(nonatomic, strong) UIImageView * cityIcon;
@property(nonatomic, strong) UIImageView * haopingIcon;
@property(nonatomic, strong) UILabel * priceLabel;
@property(nonatomic, strong) UILabel * priceDesc;
@property(nonatomic, strong) UIView * backview;

// 标签
@property(nonatomic, strong) LY_HPTagView *tagView;

@end

@implementation OListCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self p_setui];
        [self p_setframe];
    }
    return self;
}

- (void)p_setui {
    self.contentView.backgroundColor = [UIColor colorWithHexString:@"#FAFAFC"];
    [self.contentView addSubview:self.backview];
    [self.backview addSubview:self.coverImageView];
    [self.backview addSubview:self.nameLabel];
    [self.backview addSubview:self.tagView];
    [self.backview addSubview:self.cityIcon];
    [self.backview addSubview:self.cityLabel];
    [self.backview addSubview:self.distanceLabel];
    [self.backview addSubview:self.onlineIcon];
    [self.backview addSubview:self.priceLabel];
    [self.backview addSubview:self.priceDesc];
    [self.backview addSubview:self.haopingIcon];
}

- (void)p_setframe {
    [self.backview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(0);
        make.trailing.mas_equalTo(-15);
        make.height.mas_equalTo(117);
    }];
    [self.coverImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.top.bottom.mas_equalTo(0);
        make.width.mas_equalTo(117);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.coverImageView.mas_trailing).offset(12);
        make.top.mas_equalTo(8);
    }];
    [self.tagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.coverImageView.mas_trailing).offset(12);
        make.height.mas_equalTo(12);
        make.top.equalTo(self.nameLabel.mas_bottom).offset(7);
    }];
    [self.haopingIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.coverImageView.mas_trailing).offset(12);
        make.width.mas_equalTo(63);
        make.height.mas_equalTo(12);
        make.top.equalTo(self.tagView.mas_bottom).offset(7);
    }];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.coverImageView.mas_trailing).offset(12);
        make.top.equalTo(self.nameLabel.mas_bottom).offset(70);
    }];
    [self.priceDesc mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.priceLabel.mas_trailing).offset(4);
        make.top.equalTo(self.priceLabel.mas_top).offset(3);
    }];
    [self.cityLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-11);
        make.top.mas_equalTo(10);
    }];
    [self.cityIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.cityLabel.mas_centerY);
        make.trailing.equalTo(self.cityLabel.mas_leading).offset(-3);
        make.size.mas_equalTo(CGSizeMake(12, 13));
    }];
    [self.distanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-11);
        make.top.equalTo(self.cityLabel.mas_bottom).offset(10);
    }];
    [self.onlineIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(self.distanceLabel.mas_leading).offset(-8);
        make.centerY.equalTo(self.distanceLabel.mas_centerY);
    }];
}

- (LY_HPTagView *)tagView {
    if (!_tagView) {
        _tagView = [[LY_HPTagView alloc] init];
        _tagView.level = 12;
        _tagView.gender = @"female";
    }
    return _tagView;
}

- (UIImageView *)coverImageView {
    if (!_coverImageView) {
        _coverImageView = [UIImageView new];
        _coverImageView.contentMode = UIViewContentModeScaleAspectFill;
        _coverImageView.layer.cornerRadius = 11;
        _coverImageView.clipsToBounds = YES;
    }
    return _coverImageView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.font = [UIFont boldSystemFontOfSize:15];
        _nameLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _nameLabel.text = @"text";
    }
    return _nameLabel;
}

- (UILabel *)cityLabel {
    if (!_cityLabel) {
        _cityLabel = [UILabel new];
        _cityLabel.text = @"北京";
        _cityLabel.font = [UIFont boldSystemFontOfSize:10];
        _cityLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
    }
    return _cityLabel;
}

- (UIImageView *)cityIcon {
    if (!_cityIcon) {
        _cityIcon = [UIImageView new];
        _cityIcon.image = [UIImage imageNamed:@"ic_weishi"];
    }
    return _cityIcon;
}

- (UIView *)onlineIcon {
    if (!_onlineIcon) {
        _onlineIcon = [UIView new];
        _onlineIcon.backgroundColor = [UIColor colorWithHexString:@"#35F547"];
        _onlineIcon.layer.cornerRadius = 3;
    }
    return _onlineIcon;
}

- (UIImageView *)haopingIcon {
    if (!_haopingIcon) {
        _haopingIcon = [UIImageView new];
    }
    return _haopingIcon;
}

- (UILabel *)distanceLabel {
    if (!_distanceLabel) {
        _distanceLabel = [UILabel new];
        _distanceLabel.text = @"2.3km";
        _distanceLabel.font = [UIFont boldSystemFontOfSize:10];
        _distanceLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
    }
    return _distanceLabel;
}

- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [UILabel new];
        _priceLabel.textColor = [UIColor colorWithHexString:@"#FF3369"];
        _priceLabel.font = [UIFont boldSystemFontOfSize:18];
        _priceLabel.text = @"999";
    }
    return _priceLabel;
}

- (UILabel *)priceDesc {
    if (!_priceDesc) {
        _priceDesc = [UILabel new];
        _priceDesc.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _priceDesc.font = [UIFont boldSystemFontOfSize:12];
        _priceDesc.text = @"金币/1小时";
    }
    return _priceDesc;
}

- (UIView *)backview {
    if (!_backview) {
        _backview = [UIView new];
        _backview.backgroundColor = [UIColor whiteColor];
        _backview.layer.cornerRadius = 11;
        _backview.clipsToBounds = YES;
    }
    return _backview;
}

@end
