//
//  OHCollectionReusableView.m
//  SimpleDate
//
//  Created by houling on 2021/11/21.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "OHCollectionReusableView.h"
#import "OListNavView.h"
#import "OGView.h"
#import "LY_BannerView.h"

@interface OHCollectionReusableView ()

@property (nonatomic, strong) OGView * listView;
@property (nonatomic, strong) LY_BannerView *bannerView;

@end

@implementation OHCollectionReusableView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self p_setui];
        [self p_setframe];
    }
    return self;
}

- (void)p_setui {
    [self addSubview:self.listView];
    [self addSubview:self.bannerView];
}

- (void)p_setframe {
    [self.listView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(70);
        make.width.equalTo(self);
        make.centerX.equalTo(self);
        make.top.mas_equalTo(0);
    }];
    [self.bannerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(97);
        make.width.equalTo(self);
        make.centerX.equalTo(self);
        make.top.equalTo(self.listView.mas_bottom).offset(9);
    }];
}

- (void)setBannerList:(NSArray<BannerModel *> *)bannerList {
    _bannerList = bannerList;
    self.bannerView.bannerList = bannerList;
}
- (OGView *)listView {
    if (!_listView) {
        _listView = [OGView new];
    }
    return _listView;
}

- (LY_BannerView *)bannerView {
    if (!_bannerView) {
        _bannerView = [[LY_BannerView alloc] init];
    }
    return _bannerView;
}

@end
