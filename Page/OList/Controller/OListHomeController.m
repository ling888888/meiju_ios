//
//  OListHomeController.m
//  SimpleDate
//
//  Created by houling on 2021/11/21.
//  Copyright © 2021 简约互动科技（北京）有限公司. All rights reserved.
//

#import "OListHomeController.h"
#import "OListCollectionViewCell.h"
#import "OHCollectionReusableView.h"
#import "BannerModel.h"
#import "OListNavView.h"

@interface OListHomeController ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (nonatomic, strong) OListNavView * nav;
@property(nonatomic, strong) UICollectionView * listCollectionView;
@property(nonatomic, strong) NSMutableArray * list;
@property(nonatomic, strong) OHCollectionReusableView *headerView;

@end

@implementation OListHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (@available(iOS 11.0, *)) {
        self.listCollectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    [self p_setui];
    [self p_setframes];
    [self.listCollectionView reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setupData];
}

- (void)p_setui {
    [self.view addSubview:self.nav];
    [self.view addSubview:self.listCollectionView];
}

- (void)p_setframes {
    [self.nav mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.height.mas_equalTo(93);
        make.right.mas_equalTo(0);
    }];
    [self.listCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(93, 0, 0, 0));
    }];
}
#pragma mark - network

- (void)setupData {
    // 获取Banner
    NSDictionary *param = @{@"name": @"iOS"};
    [LY_Network getRequestWithURL:LY_Api.clan_banner parameters:param success:^(id  _Nullable response) {
        NSArray *bannerArray = response[@"banner"];
        NSMutableArray *models = [NSMutableArray array];
        for (NSDictionary *dict in bannerArray) {
            BannerModel *model = [BannerModel initWithDictionary:dict];
            double beginTime = [model.begin_time doubleValue] / 1000;
            double endTime = [model.end_time doubleValue] / 1000;
            NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
            if (currentTime >= beginTime && currentTime <= endTime) {
                [models addObject:model];
            }
        }
        self.headerView.bannerList = models;
    } failture:^(NSError * _Nullable error) {
        
    }];
}

#pragma mark - delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//    return self.list.count;
    return 10;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    OListCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"OListCollectionViewCell" forIndexPath:indexPath];
    if (cell) {
        return cell;
    }
    return [UICollectionViewCell new];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    _headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"OHCollectionReusableView" forIndexPath:indexPath];
    return _headerView;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(kScreenW, 117);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 13;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 13;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(kScreenW, 197);
}

#pragma mark - getter

- (UICollectionView *)listCollectionView {
    if (!_listCollectionView) {
        UICollectionViewFlowLayout *ly = [UICollectionViewFlowLayout new];
        _listCollectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:ly];
        _listCollectionView.dataSource = self;
        _listCollectionView.delegate = self;
        [_listCollectionView registerClass:[OListCollectionViewCell class] forCellWithReuseIdentifier:@"OListCollectionViewCell"];
        _listCollectionView.backgroundColor = [UIColor colorWithHexString:@"#FAFAFC"];
        [_listCollectionView registerClass:[OHCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"OHCollectionReusableView"];
    }
    return _listCollectionView;
}

- (NSMutableArray *)list {
    if (!_list) {
        _list = [NSMutableArray new];
    }
    return _list;
}

- (OHCollectionReusableView *)headerView {
    if (!_headerView) {
        _headerView = [OHCollectionReusableView new];
    }
    return _headerView;
}

- (OListNavView *)nav {
    if (!_nav) {
        _nav = [OListNavView new];
    }
    return _nav;
}

@end
