//
//  APBecomeAnchorViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2019/9/26.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APBecomeAnchorViewController.h"

@interface APBecomeAnchorViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UIButton *applyButton;
@property (weak, nonatomic) IBOutlet UILabel *limitLabel;

@end

@implementation APBecomeAnchorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = SPDStringWithKey(@"成为主播", nil);
    [self.applyButton setTitle:SPDStringWithKey(@"成为主播，接单赚钱", nil) forState:UIControlStateNormal];
    self.limitLabel.text = SPDStringWithKey(@"个人等级≥10级的女用户可申请成为主播", nil);
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"BecomeAnchor"];
}

- (IBAction)clickApplyButton:(UIButton *)sender {
    if ([[SPDApiUser currentUser].gender isEqualToString:@"female"] && MyUserLevel >= 10) {
        NSDictionary *params = @{@"userId": [SPDApiUser currentUser].userId};
        [RequestUtils commonPostRequestUtils:[params mutableCopy] bURL:@"ap.become.anchor" bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
            [self showToast:SPDStringWithKey(@"申请已提交，请等待官方审核", nil)];
        } bFailure:^(id  _Nullable failure) {
            [self showToast:failure[@"msg"]];
        }];
    } else {
        [self showToast:SPDStringWithKey(@"未达到主播申请条件", nil)];
    }
}

@end
