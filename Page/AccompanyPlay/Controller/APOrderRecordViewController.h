//
//  APOrderRecordViewController.h
//  SimpleDate
//
//  Created by 李楠 on 2019/9/3.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface APOrderRecordViewController : BaseViewController

@property (nonatomic, assign) BOOL isReceive;

@end

NS_ASSUME_NONNULL_END
