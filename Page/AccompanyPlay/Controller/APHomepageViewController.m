//
//  APHomepageViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2019/8/27.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APHomepageViewController.h"
#import "APHomepageHeaderView.h"
#import "APListCell.h"
#import "APItemModel.h"
#import "APListModel.h"
#import "APItemDetailViewController.h"
#import "APAllItemsViewController.h"
#import "APAnchorListViewController.h"
#import "APAnchorDetailViewController.h"

@interface APHomepageViewController ()<UITableViewDataSource, UITableViewDelegate, APHomepageHeaderViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) APHomepageHeaderView *headerView;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray<APListModel *> *dataArray;

@end

@implementation APHomepageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = SPDStringWithKey(@"陪玩", nil);
    
    [self requestMyInfoDetail];
    [self requestApRecommend];
}

#pragma mark - Private methods

- (void)requestMyInfoDetail {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.info.detail" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if ([suceess[@"isDeacon"] boolValue]) {
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:SPDStringWithKey(@"主播", nil) style:UIBarButtonItemStylePlain target:self action:@selector(clickLeftBarButtonItem:)];
        }
        if ([suceess[@"is_anchor"] boolValue]) {
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:SPDStringWithKey(@"接单", nil) style:UIBarButtonItemStylePlain target:self action:@selector(clickRightBarButtonItem:)];
        }
    } bFailure:^(id  _Nullable failure) {

    }];
}

- (void)requestApRecommend {
    NSDictionary *params = @{@"page": @(self.page)};
    [RequestUtils commonGetRequestUtils:[params mutableCopy] bURL:@"ap.recommend" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if (self.page == 0) {
            [self.dataArray removeAllObjects];
            
            NSMutableArray *items = [NSMutableArray new];
            for (NSDictionary *dic in suceess[@"games"]) {
                APItemModel *model = [APItemModel initWithDictionary:dic];
                [items addObject:model];
            }
            self.headerView.dataArray = items;
            self.headerView.bounds = CGRectMake(0, 0, kScreenW, items.count > 4 ? 208 : 108);
        }
        
        NSArray *list = suceess[@"list"];
        for (NSDictionary *dic in list) {
            APListModel *model = [APListModel initWithDictionary:dic];
            [self.dataArray addObject:model];
        }
        [self.tableView reloadData];
        self.page++;
        
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        self.tableView.mj_footer.hidden = !list.count;
    } bFailure:^(id  _Nullable failure) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }];
}

#pragma mark - Event responses

- (void)clickLeftBarButtonItem:(UIBarButtonItem *)sender {
    [self.navigationController pushViewController:[APAnchorListViewController new] animated:YES];
}

- (void)clickRightBarButtonItem:(UIBarButtonItem *)sender {
    [self.navigationController pushViewController:[APAnchorDetailViewController new] animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    APListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"APListCell" forIndexPath:indexPath];
    cell.levelButton.hidden = YES;
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[NSBundle mainBundle] loadNibNamed:@"APRecommendView" owner:self options:nil].firstObject;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath; {
    [self pushToDetailTableViewController:self userId:self.dataArray[indexPath.row].userId];
}

#pragma mark - APHomepageHeaderViewDelegate

- (void)didClickItemWithModel:(APItemModel *)model {
    APItemDetailViewController *vc = [APItemDetailViewController new];
    vc.model = model;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didClickMoreItems {
    [self.navigationController pushViewController:[APAllItemsViewController new] animated:YES];
}

#pragma mark - Setters & getters

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, NavHeight, kScreenW, kScreenH - NavHeight - TabBarHeight) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.rowHeight = 108.5;
        _tableView.sectionHeaderHeight = 28;
        _tableView.separatorColor = [UIColor colorWithHexString:@"E1E1E1"];
        _tableView.tableFooterView = [UIView new];
        [_tableView registerNib:[UINib nibWithNibName:@"APListCell" bundle:nil] forCellReuseIdentifier:@"APListCell"];
        
        __weak typeof(self) weakself = self;
        _tableView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            weakself.page = 0;
            [weakself requestApRecommend];
        }];
        _tableView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            [weakself requestApRecommend];
        }];
        _tableView.mj_footer.hidden = YES;
        
        if (@available(iOS 11.0, *)) {
            _tableView.estimatedRowHeight = 0;
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
        
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

- (APHomepageHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[NSBundle mainBundle] loadNibNamed:@"APHomepageHeaderView" owner:self options:nil].firstObject;
        _headerView.delegate = self;
        self.tableView.tableHeaderView = _headerView;
    }
    return _headerView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

@end
