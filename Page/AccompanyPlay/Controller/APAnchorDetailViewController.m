//
//  APAnchorDetailViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2019/8/31.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APAnchorDetailViewController.h"
#import "APAnchorDetailHeaderCell.h"
#import "APAnchorDetailTitleView.h"
#import "APAnchorDetailTimeCell.h"
#import "APAnchorDetailItemCell.h"
#import "APItemModel.h"
#import "APPickerView.h"
#import "APOrderRecordViewController.h"
#import "APIncomeDetailViewController.h"

@interface APAnchorDetailViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, APAnchorDetailHeaderCellDelegate, APAnchorDetailItemCellDelegate, APPickerViewDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSNumber *lastWeekIncome;
@property (nonatomic, strong) NSNumber *thisWeekIncome;
@property (nonatomic, strong) NSString *orderTime;
@property (nonatomic, strong) NSMutableArray<APItemModel *> *dataArray;
@property (nonatomic, strong) APPickerView *pickerView;
@property (nonatomic, strong) NSArray *orderTimes;
@property (nonatomic, strong) APAnchorDetailItemCell *editingItemCell;

@end

@implementation APAnchorDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = SPDStringWithKey(@"接单", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:SPDStringWithKey(@"接单记录", nil) style:UIBarButtonItemStylePlain target:self action:@selector(clickRightBarButtonItem:)];
    self.view.backgroundColor = [UIColor colorWithHexString:COMMON_PINK];
    
    [self requestApAnchorDetail];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
}

#pragma mark - Private methods

- (void)requestApAnchorDetail {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"ap.anchor.detail" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.lastWeekIncome = suceess[@"lastWeekEarning"];
        self.thisWeekIncome = suceess[@"thisWeekEarning"];
        self.orderTime = suceess[@"orderReceiveTime"];
        for (NSDictionary *dic in suceess[@"list"]) {
            APItemModel *model = [APItemModel initWithDictionary:dic];
            [self.dataArray addObject:model];
        }
        [self.collectionView reloadData];
    } bFailure:^(id  _Nullable failure) {

    }];
}

- (void)requestApTimwSetWithTime:(NSString *)time {
    NSDictionary *params = @{@"time": time};
    [RequestUtils commonPostRequestUtils:[params mutableCopy] bURL:@"ap.time.set" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.orderTime = time;
    } bFailure:^(id  _Nullable failure) {
        [self.collectionView reloadData];
        [self showToast:failure[@"msg"]];
    }];
}

- (void)requestApGameSetWithParams:(NSMutableDictionary *)params model:(APItemModel *)model {
    [params setObject:model.itemId forKey:@"gameId"];
    [RequestUtils commonPostRequestUtils:params bURL:@"ap.game.set" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        [model setValuesForKeysWithDictionary:params];
    } bFailure:^(id  _Nullable failure) {
        [self.collectionView reloadData];
        [self showToast:failure[@"msg"]];
    }];
}

#pragma mark - Event responses

- (void)clickRightBarButtonItem:(UIBarButtonItem *)sender {
    APOrderRecordViewController *vc = [APOrderRecordViewController new];
    vc.isReceive = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 3;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    switch (section) {
        case 2:
            return self.dataArray.count + 1;
        default:
            return 1;
    }
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0: {
            APAnchorDetailHeaderCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"APAnchorDetailHeaderCell" forIndexPath:indexPath];
            cell.lastWeekIncomeLabel.text = [NSString stringWithFormat:@"$%@", self.lastWeekIncome.notEmpty ? self.lastWeekIncome : @"0"];
            cell.thisWeekIncomeLabel.text = [NSString stringWithFormat:@"$%@", self.thisWeekIncome.notEmpty ? self.thisWeekIncome : @"0"];
            cell.delegate = self;
            return cell;
        }
        case 1: {
            APAnchorDetailTimeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"APAnchorDetailTimeCell" forIndexPath:indexPath];
            cell.timeLabel.text = self.orderTime;
            return cell;
        }
        default: {
            APAnchorDetailItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"APAnchorDetailItemCell" forIndexPath:indexPath];
            if (indexPath.row < self.dataArray.count) {
                cell.model = self.dataArray[indexPath.row];
                cell.delegate = self;
                cell.addView.hidden = YES;
            } else {
                cell.addView.hidden = NO;
            }
            return cell;
        }
    }
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    APAnchorDetailTitleView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"APAnchorDetailTitleView" forIndexPath:indexPath];
    view.titleLabel.text = SPDStringWithKey(indexPath.section == 1 ? @"接单设置" : @"陪玩", nil);
    return view;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 1:
            self.pickerView.type = APPickerTypeOrderTime;
            self.pickerView.dataArray = self.orderTimes;
            [self.pickerView show];
            break;
        case 2:
            if (indexPath.row == self.dataArray.count) {
                [self presentAlertWithTitle:@"添加更多技能，请联系Famy客服" message:nil cancelTitle:@"取消" cancelHandler:nil actionTitle:@"确定" actionHandler:^(UIAlertAction * _Nonnull action) {
                    [self pushToConversationViewControllerWithTargetId:@"13"];
                }];
            }
        default:
            break;
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            return CGSizeMake(kScreenW, 170);
        case 1:
            return CGSizeMake(kScreenW - 10 * 2, 50);
        default:
            return CGSizeMake((kScreenW - 10 * 2 - 7) / 2, 105);
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    switch (section) {
        case 0:
            return UIEdgeInsetsZero;
        default:
            return UIEdgeInsetsMake(8, 10, 12, 10);
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return CGSizeZero;
        default:
            return CGSizeMake(kScreenW, 28);
    }
}

#pragma mark - APAnchorDetailHeaderCellDelegate

- (void)headerCell:(APAnchorDetailHeaderCell *)headerCell didClickIncomeButtonWithWeek:(NSString *)week {
    APIncomeDetailViewController *vc = [APIncomeDetailViewController new];
    vc.week = week;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - APAnchorDetailItemCellDelegate

- (void)itemCell:(APAnchorDetailItemCell *)itemCell didClickPriceButton:(UIButton *)sender {
    self.editingItemCell = itemCell;
    self.pickerView.type = APPickerTypePrice;
    self.pickerView.dataArray = @[itemCell.model.prices, itemCell.model.units];
    [self.pickerView show];
}

- (void)itemCell:(APAnchorDetailItemCell *)itemCell switchValueDidChange:(UISwitch *)sender {
    [self requestApGameSetWithParams:[@{@"isReceive": sender.on ? @"true" : @"false"} mutableCopy] model:itemCell.model];
}

#pragma mark - APPickerViewDelegate

- (void)pickerView:(APPickerView *)pickerView didClickConfirmButtonWithSelectedValues:(NSArray *)selectedValues {
    if (pickerView.type == APPickerTypeOrderTime) {
        APAnchorDetailTimeCell *cell = (APAnchorDetailTimeCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
        cell.timeLabel.text = [selectedValues componentsJoinedByString:@"-"];
        [self requestApTimwSetWithTime:cell.timeLabel.text];
    } else {
        [self.editingItemCell.priceButton setTitle:[NSString stringWithFormat:@"%@%@/%@min", selectedValues.firstObject, SPDStringWithKey(@"钻", nil), selectedValues.lastObject] forState:UIControlStateNormal];
        [self requestApGameSetWithParams:[@{@"price": selectedValues.firstObject, @"unit": selectedValues.lastObject} mutableCopy] model:self.editingItemCell.model];
    }
}

#pragma mark - Setters & Getters

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.minimumLineSpacing = 12;
        layout.minimumInteritemSpacing = 0;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, NavHeight, kScreenW, kScreenH - NavHeight) collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor colorWithHexString:@"F3F3F3"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerNib:[UINib nibWithNibName:@"APAnchorDetailHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"APAnchorDetailHeaderCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"APAnchorDetailTitleView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"APAnchorDetailTitleView"];
        [_collectionView registerNib:[UINib nibWithNibName:@"APAnchorDetailTimeCell" bundle:nil] forCellWithReuseIdentifier:@"APAnchorDetailTimeCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"APAnchorDetailItemCell" bundle:nil] forCellWithReuseIdentifier:@"APAnchorDetailItemCell"];
        
        if (@available(iOS 11.0, *)) {
            _collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
        
        [self.view addSubview:_collectionView];
    }
    return _collectionView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

- (APPickerView *)pickerView {
    if (!_pickerView) {
        _pickerView = [[NSBundle mainBundle] loadNibNamed:@"APPickerView" owner:self options:nil].firstObject;
        _pickerView.frame = self.view.bounds;
        _pickerView.delegate = self;
        [self.view addSubview:_pickerView];
    }
    return _pickerView;
}

- (NSArray *)orderTimes {
    if (!_orderTimes) {
        _orderTimes = [NSMutableArray new];
        NSMutableArray *startTimes = [NSMutableArray new];
        NSMutableArray *endTimes = [NSMutableArray new];
        for (NSInteger i = 0; i < 23; i++) {
            [startTimes addObject:[NSString stringWithFormat:@"%02ld:00", i]];
            [endTimes addObject:[NSString stringWithFormat:@"%02ld:00", i + 1]];
        }
        _orderTimes = @[startTimes, endTimes];
    }
    return _orderTimes;
}

@end
