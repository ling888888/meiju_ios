//
//  APIncomeDetailViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2019/9/2.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APIncomeDetailViewController.h"
#import "APOrderRecordEmptyView.h"
#import "APIncomeDetailCell.h"
#import "APIncomeModel.h"

@interface APIncomeDetailViewController ()<UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray<APIncomeModel *> *dataArray;

@end

@implementation APIncomeDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = SPDStringWithKey(@"收益明细", nil);
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self requestApEarningList];
}

#pragma mark - Private methods

- (void)requestApEarningList {
    NSDictionary *params = @{@"week": self.week, @"page": @(self.page)};
    [RequestUtils commonGetRequestUtils:[params mutableCopy] bURL:@"ap.earning.list" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if (self.page == 0) {
            [self.dataArray removeAllObjects];
        }
        
        NSArray *list = suceess[@"list"];
        for (NSDictionary *dic in list) {
            APIncomeModel *model = [APIncomeModel initWithDictionary:dic];
            [self.dataArray addObject:model];
        }
        [self.tableView reloadData];
        self.page++;
        
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        self.tableView.mj_footer.hidden = !list.count;
        self.tableView.backgroundColor = self.dataArray.count ? [UIColor whiteColor] : [UIColor clearColor];
    } bFailure:^(id  _Nullable failure) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    APIncomeDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"APIncomeDetailCell" forIndexPath:indexPath];
    APIncomeModel *model = self.dataArray[indexPath.row];
    cell.incomeLabel.text = model.income;
    cell.timeLabel.text = model.time;
    return cell;
}

#pragma mark - Setters & getters

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.dataSource = self;
        _tableView.rowHeight = 66;
        _tableView.separatorColor = [UIColor colorWithHexString:@"DDDDDD"];
        _tableView.tableFooterView = [UIView new];
        [_tableView registerNib:[UINib nibWithNibName:@"APIncomeDetailCell" bundle:nil] forCellReuseIdentifier:@"APIncomeDetailCell"];
        
        __weak typeof(self) weakself = self;
        _tableView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            weakself.page = 0;
            [weakself requestApEarningList];
        }];
        _tableView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            [weakself requestApEarningList];
        }];
        _tableView.mj_footer.hidden = YES;
        
        APOrderRecordEmptyView *view = [[NSBundle mainBundle] loadNibNamed:@"APOrderRecordEmptyView" owner:self options:nil].firstObject;
        view.frame = self.view.bounds;
        view.textLabel.text = SPDStringWithKey(@"尚未有收益，快去接单赚钱吧～", nil);
        [view addSubview:_tableView];
        [self.view addSubview:view];
    }
    return _tableView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

@end
