//
//  APItemDetailViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2019/8/28.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APItemDetailViewController.h"
#import "APItemDetailHeaderView.h"
#import "APItemDetailFilterView.h"
#import "APListCell.h"
#import "APItemModel.h"
#import "APListModel.h"

@interface APItemDetailViewController ()<UITableViewDataSource, UITableViewDelegate, APItemDetailFilterViewDelegate>

@property (nonatomic, strong) UIImageView *coverImageView;
@property (nonatomic, strong) APItemDetailHeaderView *headerView;
@property (nonatomic, strong) APItemDetailFilterView *filterView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray<APListModel *> *dataArray;

@end

@implementation APItemDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];

    [self requestApGameDetail];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
}

#pragma mark - Private methods

- (void)requestApGameDetail {
    self.filterView.userInteractionEnabled = NO;
    
    NSMutableDictionary *params = [@{@"gameId": self.model.itemId, @"page": @(self.page)} mutableCopy];
    [params setValuesForKeysWithDictionary:self.filterView.optionParams];
    if ([params[@"price"] isEqualToString:@"all"]) {
        [params removeObjectForKey:@"price"];
    }
    [RequestUtils commonGetRequestUtils:params bURL:@"ap.game.detail" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if (self.page == 0) {
            [self.dataArray removeAllObjects];
            
            [self.coverImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, suceess[@"cover"]]]];
            self.headerView.nameLabel.text = suceess[@"name"];
            self.headerView.descLabel.text = suceess[@"desc"];
        }
        
        NSArray *list = suceess[@"list"];
        for (NSDictionary *dic in list) {
            APListModel *model = [APListModel initWithDictionary:dic];
            [self.dataArray addObject:model];
        }
        [self.tableView reloadData];
        self.page++;
        
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        self.filterView.userInteractionEnabled = YES;
        self.tableView.mj_footer.hidden = !list.count;
    } bFailure:^(id  _Nullable failure) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        self.filterView.userInteractionEnabled = YES;
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    APListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"APListCell" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath; {
    [self pushToDetailTableViewController:self userId:self.dataArray[indexPath.row].userId];
}

#pragma mark - APItemDetailFilterViewDelegate

- (void)optionParamsDidChange {
    self.page = 0;
    [self requestApGameDetail];
}

#pragma mark - Setters & getters

- (UIImageView *)coverImageView {
    if (!_coverImageView) {
        _coverImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, NavHeight + 28 + 68 + 15)];
        _coverImageView.contentMode = UIViewContentModeScaleAspectFill;
        _coverImageView.clipsToBounds = YES;
        [self.view addSubview:_coverImageView];
    }
    return _coverImageView;
}


- (APItemDetailHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[NSBundle mainBundle] loadNibNamed:@"APItemDetailHeaderView" owner:self options:nil].firstObject;
        _headerView.frame = CGRectMake(0, NavHeight + 28, kScreenW, 68);
        [self.view addSubview:_headerView];
    }
    return _headerView;
}

- (APItemDetailFilterView *)filterView {
    if (!_filterView) {
        _filterView = [[NSBundle mainBundle] loadNibNamed:@"APItemDetailFilterView" owner:self options:nil].firstObject;
        _filterView.frame = CGRectMake(0, NavHeight + 28 + 68, kScreenW, 40);
        _filterView.delegate = self;
    }
    return _filterView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, NavHeight + 28 + 68 + 40, kScreenW, kScreenH - NavHeight - 28 - 68 - 40) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.rowHeight = 108.5;
        _tableView.sectionHeaderHeight = 40;
        _tableView.separatorColor = [UIColor colorWithHexString:@"E1E1E1"];
        _tableView.tableFooterView = [UIView new];
        [_tableView registerNib:[UINib nibWithNibName:@"APListCell" bundle:nil] forCellReuseIdentifier:@"APListCell"];
        
        __weak typeof(self) weakself = self;
        _tableView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            weakself.page = 0;
            [weakself requestApGameDetail];
        }];
        _tableView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            [weakself requestApGameDetail];
        }];
        _tableView.mj_footer.hidden = YES;
        
        [self.view addSubview:_tableView];
        [self.view addSubview:self.filterView];
    }
    return _tableView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

@end
