//
//  APOrderDetailViewController.h
//  SimpleDate
//
//  Created by 李楠 on 2019/9/17.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@class APListModel, APItemModel;

@interface APOrderDetailViewController : BaseViewController

@property (nonatomic, assign) BOOL submitted;

@property (nonatomic, strong) APListModel *model;
@property (nonatomic, strong) NSMutableArray<APItemModel *> *items;
@property (nonatomic, strong) NSNumber *currentTime;
@property (nonatomic, strong) NSString *serviceTime;

@end

NS_ASSUME_NONNULL_END
