//
//  APOrderDetailViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2019/9/17.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APOrderDetailViewController.h"
#import "APOrderDetailHeaderCell.h"
#import "APOrderDetailOptionCell.h"
#import "APOrderDetailQuantityCell.h"
#import "APOrderDetailSumCell.h"
#import "APOrderDetailRemarkCell.h"
#import "APPickerView.h"
#import "APListModel.h"
#import "APItemModel.h"

@interface APOrderDetailViewController ()<UITableViewDataSource, UITableViewDelegate, APPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *submitButtonBottom;
@property (weak, nonatomic) IBOutlet UILabel *sumLabel;

@property (nonatomic, strong) APPickerView *pickerView;
@property (nonatomic, assign) NSTimeInterval zeroTimeStamp;
@property (nonatomic, assign) NSTimeInterval nowTimeStamp;
@property (nonatomic, strong) NSString *nowTime;
@property (nonatomic, assign) BOOL nowBeyondTimeLimit;
@property (nonatomic, assign) NSTimeInterval serviceTimeStamp;
@property (nonatomic, strong) NSArray *serviceTimes;
@property (nonatomic, assign) BOOL beyondTimeLimit;

@end

@implementation APOrderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.submitted) {
        self.navigationItem.title = SPDStringWithKey(@"订单详情", nil);
        self.bottomView.hidden = YES;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kScreenW, 45)];
        label.text = SPDStringWithKey(@"待确定", nil);
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor colorWithHexString:@"191B1A"];
        label.font = [UIFont systemFontOfSize:15];
        self.tableView.tableFooterView = label;
    } else {
        self.navigationItem.title = SPDStringWithKey(@"确认订单", nil);
        self.bottomView.hidden = NO;
        [self.submitButton setTitle:SPDStringWithKey(@"提交订单", nil) forState:UIControlStateNormal];
        [self refreshSum];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
    
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
    [self.tableView registerNib:[UINib nibWithNibName:@"APOrderDetailHeaderCell" bundle:nil] forCellReuseIdentifier:@"APOrderDetailHeaderCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"APOrderDetailOptionCell" bundle:nil] forCellReuseIdentifier:@"APOrderDetailOptionCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"APOrderDetailQuantityCell" bundle:nil] forCellReuseIdentifier:@"APOrderDetailQuantityCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"APOrderDetailSumCell" bundle:nil] forCellReuseIdentifier:@"APOrderDetailSumCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"APOrderDetailRemarkCell" bundle:nil] forCellReuseIdentifier:@"APOrderDetailRemarkCell"];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Private methods

- (void)refreshSum {
    NSString *string = [NSString stringWithFormat:@"%@:%@%@", SPDStringWithKey(@"总额", nil), self.model.price, SPDStringWithKey(@"钻", nil)];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    [attributedString addAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"FF5151"], NSFontAttributeName: [UIFont systemFontOfSize:23]} range:[string rangeOfString:self.model.price.stringValue]];
    self.sumLabel.attributedText = attributedString;
}

#pragma mark - Event responses

- (IBAction)clickSubmitButton:(UIButton *)sender {
    NSString *title = [NSString stringWithFormat:SPDStringWithKey(@"确定花费%@钻石下单吗？", nil), self.model.price];
    [self presentAlertWithTitle:title message:nil cancelTitle:SPDStringWithKey(@"取消", nil) cancelHandler:nil actionTitle:SPDStringWithKey(@"确定", nil) actionHandler:^(UIAlertAction * _Nonnull action) {
        
        NSMutableDictionary *params = [@{@"anchorId": self.model.userId, @"gameId": self.model.itemId, @"time": @(self.serviceTimeStamp * 1000)} mutableCopy];
        APOrderDetailRemarkCell *cell = (APOrderDetailRemarkCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
        if (cell.remark.notEmpty) {
            [params setObject:cell.remark forKey:@"remark"];
        }
        [RequestUtils commonPostRequestUtils:params bURL:@"ap.order.submit" bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
            APOrderDetailViewController *vc = [APOrderDetailViewController new];
            vc.submitted = YES;
            vc.model = self.model;
            vc.serviceTime = self.serviceTime;
            [self.navigationController pushViewController:vc animated:YES];
        } bFailure:^(id  _Nullable failure) {
            if ([failure[@"code"] integerValue] == 2006) {
                [self presentAlertWithTitle:@"对方暂时不方便接单，请沟通后再次下单" message:nil cancelTitle:nil cancelHandler:nil actionTitle:@"知道了" actionHandler:nil];
            } else {
                [self showToast:failure[@"msg"]];
            }
        }];
    }];
}

- (void)KeyboardWillShow:(NSNotification *)notification {
    CGRect keyboardFrameEnd = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat animationDuration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^{
        CGRect tableViewFrame = self.tableView.frame;
        tableViewFrame.size.height = self.view.frame.size.height - keyboardFrameEnd.size.height;
        self.tableView.frame = tableViewFrame;
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }];
}

- (void)KeyboardWillHide:(NSNotification *)notification {
    CGFloat animationDuration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^{
        self.tableView.frame = self.view.bounds;
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.submitted ? 2 : 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 1:
            return 4;
        default:
            return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0: {
            APOrderDetailHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"APOrderDetailHeaderCell" forIndexPath:indexPath];
            cell.model = self.model;
            if (self.submitted) {
                cell.nameLbaelTop.constant = 18;
                cell.priceLabel.hidden = YES;
            }
            return cell;
        }
        case 1:
            switch (indexPath.row) {
                case 0: {
                    APOrderDetailOptionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"APOrderDetailOptionCell" forIndexPath:indexPath];
                    cell.titleLabel.text = SPDStringWithKey(self.submitted ? @"类型" : @"购买类型", nil);
                    cell.optionLabel.text = self.model.itemName;
                    cell.beyondTimeLimitLabel.hidden = YES;
                    return cell;
                }
                case 1: {
                    APOrderDetailOptionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"APOrderDetailOptionCell" forIndexPath:indexPath];
                    cell.titleLabel.text = SPDStringWithKey(self.submitted ? @"时间" : @"服务时间", nil);
                    cell.optionLabel.text = self.serviceTime;
                    cell.beyondTimeLimitLabel.hidden = !self.beyondTimeLimit;
                    return cell;
                }
                case 2: {
                    if (self.submitted) {
                        APOrderDetailOptionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"APOrderDetailOptionCell" forIndexPath:indexPath];
                        cell.titleLabel.text = SPDStringWithKey(@"费用", nil);
                        cell.optionLabel.text = [NSString stringWithFormat:@"%@%@/%@min x1", self.model.price, SPDStringWithKey(@"钻", nil), self.model.unit];
                        cell.beyondTimeLimitLabel.hidden = YES;
                        return cell;
                    } else {
                        APOrderDetailQuantityCell *cell = [tableView dequeueReusableCellWithIdentifier:@"APOrderDetailQuantityCell" forIndexPath:indexPath];
                        return cell;
                    }
                }
                default: {
                    APOrderDetailSumCell *cell = [tableView dequeueReusableCellWithIdentifier:@"APOrderDetailSumCell" forIndexPath:indexPath];
                    cell.quantityLabel.hidden = self.submitted;
                    cell.quantityLabel.text = [NSString stringWithFormat:@"%@: %@", SPDStringWithKey(@"数量", nil), @"1"];
                    NSString *string = [NSString stringWithFormat:@"%@: %@%@", SPDStringWithKey(@"小计", nil), self.model.price, SPDStringWithKey(@"钻", nil)];
                    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
                    [attributedString addAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"FF5151"]} range:[string rangeOfString:self.model.price.stringValue]];
                    cell.sumLabel.attributedText = attributedString;
                    return cell;
                }
            };
        default: {
            APOrderDetailRemarkCell *cell = [tableView dequeueReusableCellWithIdentifier:@"APOrderDetailRemarkCell" forIndexPath:indexPath];
            return cell;
        }
    }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            return 94;
        case 1:
            switch (indexPath.row) {
                case 1:
                    return self.beyondTimeLimit ? 60 : 50;
                default:
                    return 50;
            }
        default:
            return 100;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.submitted) {
        [self.view endEditing:YES];
        switch (indexPath.section) {
            case 1:
                switch (indexPath.row) {
                    case 0:
                        self.pickerView.type = APPickerTypeItem;
                        self.pickerView.dataArray = @[self.items];
                        [self.pickerView show];
                        break;
                    case 1:
                        self.pickerView.orderTime = self.model.receiveOrderTime;
                        self.pickerView.type = APPickerTypeSeviceTime;
                        self.pickerView.dataArray = self.serviceTimes;
                        [self.pickerView show];
                        break;
                    default:
                        break;
                }
            default:
                break;
        }
    }
}

#pragma mark - APPickerViewDelegate

- (void)pickerView:(APPickerView *)pickerView didClickConfirmButtonWithSelectedValues:(NSArray *)selectedValues {
    if (pickerView.type == APPickerTypeItem) {
        APItemModel *model = selectedValues.firstObject;
        self.model.itemId = model.itemId;
        self.model.image = model.image;
        self.model.itemName = model.name;
        self.model.price = model.price;
        self.model.unit = model.unit;
        [self.tableView reloadData];
        [self refreshSum];
    } else {
        NSString *day = selectedValues[0];
        NSString *hour = selectedValues[1];
        NSString *min = selectedValues[2];
        if ([hour isEqualToString:SPDStringWithKey(@"现在", nil)]) {
            self.serviceTimeStamp = self.nowTimeStamp;
            self.serviceTime = self.nowTime;
            self.beyondTimeLimit = self.nowBeyondTimeLimit;
        } else {
            if ([day isEqualToString:SPDStringWithKey(@"今天", nil)]) {
                self.serviceTimeStamp = self.zeroTimeStamp + (hour.integerValue * 60 + min.integerValue) * 60;
            } else if ([day isEqualToString:SPDStringWithKey(@"明天", nil)]) {
                self.serviceTimeStamp = self.zeroTimeStamp + ((24 + hour.integerValue) * 60 + min.integerValue) * 60;
            } else {
                self.serviceTimeStamp = self.zeroTimeStamp + ((48 + hour.integerValue) * 60 + min.integerValue) * 60;
            }
            self.serviceTime = [NSString stringWithFormat:@"%@ %@:%@", day, hour, min];
            
            NSInteger startHour = [self.model.receiveOrderTime substringToIndex:2].integerValue;
            NSInteger endHour = [self.model.receiveOrderTime substringWithRange:NSMakeRange(6, 2)].integerValue;
            self.beyondTimeLimit = (hour.integerValue < startHour || hour.integerValue > endHour) || (hour.integerValue == endHour && min.integerValue > 0);
        }
        [self.tableView reloadData];
    }
}

#pragma mark - Setters & Getters

- (void)setCurrentTime:(NSNumber *)currentTime {
    _currentTime = currentTime;
    
    NSTimeInterval currentTimeStamp = _currentTime.doubleValue / 1000;
    NSDate *currentDate = [[NSDate alloc] initWithTimeIntervalSince1970:currentTimeStamp];
    
    NSDateFormatter *dateFomater1 = [[NSDateFormatter alloc] init];
    dateFomater1.dateFormat = @"yyyy-MM-dd";
    dateFomater1.timeZone = [NSTimeZone localTimeZone];
    NSString *zeroDateString = [dateFomater1 stringFromDate:currentDate];
    NSDate *zeroDate = [dateFomater1 dateFromString:zeroDateString];
    self.zeroTimeStamp = [zeroDate timeIntervalSince1970];
    
    self.nowTimeStamp = currentTimeStamp + 15 * 60;
    NSDate *nowDate = [[NSDate alloc] initWithTimeIntervalSince1970:self.nowTimeStamp];
    NSDateFormatter *dateFomater2 = [[NSDateFormatter alloc] init];
    dateFomater2.dateFormat = @"HH:mm";
    dateFomater2.timeZone = [NSTimeZone localTimeZone];
    NSString *nowDateString = [dateFomater2 stringFromDate:nowDate];
    self.nowTime = [NSString stringWithFormat:@"%@ %@", SPDStringWithKey(@"今天", nil), nowDateString];
    NSInteger startHour = [self.model.receiveOrderTime substringToIndex:2].integerValue;
    NSInteger endHour = [self.model.receiveOrderTime substringWithRange:NSMakeRange(6, 2)].integerValue;
    NSInteger hour = [nowDateString substringToIndex:2].integerValue;
    NSInteger min = [nowDateString substringFromIndex:3].integerValue;
    self.nowBeyondTimeLimit = (hour < startHour || hour > endHour) || (hour == endHour && min > 0);
    self.serviceTimeStamp = self.nowTimeStamp;
    self.serviceTime = self.nowTime;
    self.beyondTimeLimit = self.nowBeyondTimeLimit;
    
    NSDateFormatter *dateFomater3 = [[NSDateFormatter alloc] init];
    dateFomater3.dateFormat = @"HH";
    dateFomater3.timeZone = [NSTimeZone localTimeZone];
    NSInteger currentHour = [dateFomater3 stringFromDate:currentDate].integerValue;
    NSMutableArray *alllHours = [NSMutableArray new];
    NSMutableArray *todayHours = [NSMutableArray arrayWithObject:SPDStringWithKey(@"现在", nil)];
    for (NSInteger i = 0; i < 24; i++) {
        NSString *hour = [NSString stringWithFormat:@"%02ld", i];
        [alllHours addObject:hour];
        if (i > currentHour) {
            [todayHours addObject:hour];
        }
    }
    NSArray *days = @[SPDStringWithKey(@"今天", nil), SPDStringWithKey(@"明天", nil), SPDStringWithKey(@"后天", nil)];
    NSArray *mins = @[@"00", @"15", @"30", @"45"];
    self.serviceTimes = @[days, @[todayHours, alllHours], @[@[@""], mins]];
}

- (APPickerView *)pickerView {
    if (!_pickerView) {
        _pickerView = [[NSBundle mainBundle] loadNibNamed:@"APPickerView" owner:self options:nil].firstObject;
        _pickerView.frame = self.view.bounds;
        _pickerView.delegate = self;
        [self.view addSubview:_pickerView];
    }
    return _pickerView;
}

@end
