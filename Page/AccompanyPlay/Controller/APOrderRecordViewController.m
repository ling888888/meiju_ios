//
//  APOrderRecordViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2019/9/3.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APOrderRecordViewController.h"
#import "LazyPageScrollView.h"
#import "APOrderRecordEmptyView.h"
#import "APOrderRecordListCell.h"
#import "APOrderModel.h"
#import "APListModel.h"
#import "APItemModel.h"
#import "APOrderDetailViewController.h"

@interface APOrderRecordViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, APOrderRecordListCellDelegate>

@property (nonatomic, strong) LazyPageScrollView *lazyPageScrollView;
@property (nonatomic, strong) UICollectionView *ongoingCollectionView;
@property (nonatomic, strong) UICollectionView *historyCollectionView;
@property (nonatomic, strong) NSString *requestURL;
@property (nonatomic, assign) NSInteger ongoingPage;
@property (nonatomic, assign) NSInteger historyPage;
@property (nonatomic, strong) NSMutableArray<APOrderModel *> *ongoingArray;
@property (nonatomic, strong) NSMutableArray<APOrderModel *> *historyArray;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) BOOL stopTimer;

@end

@implementation APOrderRecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.isReceive) {
        self.navigationItem.title = SPDStringWithKey(@"接单记录", nil);
        self.requestURL = @"ap.order.receive";
    } else {
        self.navigationItem.title = SPDStringWithKey(@"下单记录", nil);
        self.requestURL = @"ap.order.place";
    }
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.lazyPageScrollView];
    [self requestOrderRecordWithType:@"proceed"];
    [self requestOrderRecordWithType:@"history"];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.stopTimer = YES;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    if (self.stopTimer) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

#pragma mark - Private methods

- (void)requestOrderRecordWithType:(NSString *)type {
    NSInteger page;
    NSMutableArray *dataArray;
    UICollectionView *collectionView;
    if ([type isEqualToString:@"proceed"]) {
        page = self.ongoingPage;
        dataArray = self.ongoingArray;
        collectionView = self.ongoingCollectionView;
    } else {
        page = self.historyPage;
        dataArray = self.historyArray;
        collectionView = self.historyCollectionView;
    }
    NSDictionary *params = @{@"type": type, @"page": @(page)};
    [RequestUtils commonGetRequestUtils:[params mutableCopy] bURL:self.requestURL bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if (page == 0) {
            [dataArray removeAllObjects];
        }
        NSArray *list = suceess[@"list"];
        for (NSDictionary *dic in list) {
            APOrderModel *model = [APOrderModel initWithDictionary:dic];
            [dataArray addObject:model];
            if ([model.status isEqualToString:@"await"]) {
                [self initTimer];
            }
        }
        if ([type isEqualToString:@"proceed"]) {
            self.ongoingPage++;
        } else {
            self.historyPage++;
        }
        [collectionView reloadData];
        
        [collectionView.mj_header endRefreshing];
        [collectionView.mj_footer endRefreshing];
        collectionView.mj_footer.hidden = !list.count;
        collectionView.backgroundColor = dataArray.count ? [UIColor colorWithHexString:@"F3F3F3"] : [UIColor clearColor];
    } bFailure:^(id  _Nullable failure) {
        [collectionView.mj_header endRefreshing];
        [collectionView.mj_footer endRefreshing];
    }];
}

- (void)initTimer {
    if (!self.timer) {
        self.timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(countdown) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
        [self.timer fire];
    }
}

- (void)countdown {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderRecordCountdownNotification" object:nil];
}

- (void)requestOrderActionWithURL:(NSString *)url model:(APOrderModel *)model sender:(UIButton *)sender {
    NSDictionary *params = @{@"orderId": model.orderId, @"type": @"now"};
    [RequestUtils commonPostRequestUtils:[params mutableCopy] bURL:url bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if ([url isEqualToString:@"ap.order.server"]) {
            [DBUtil recordAPServedOrderWithOrderId:model.orderId];
            sender.hidden = YES;
        } else {
            self.ongoingPage = 0;
            [self requestOrderRecordWithType:@"proceed"];
        }
    } bFailure:^(id  _Nullable failure) {
        [self showToast:failure[@"msg"]];
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == self.ongoingCollectionView) {
        return self.ongoingArray.count;
    } else {
        return self.historyArray.count;
    }
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    APOrderRecordListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"APOrderRecordListCell" forIndexPath:indexPath];
    cell.isReceive = self.isReceive;
    if (collectionView == self.ongoingCollectionView) {
        cell.model = self.ongoingArray[indexPath.row];
    } else {
        cell.model = self.historyArray[indexPath.row];
    }
    cell.delegate = self;
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    [[NSNotificationCenter defaultCenter] addObserver:cell selector:@selector(orderRecordCountdown:) name:@"OrderRecordCountdownNotification" object:nil];
}


- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isReceive && collectionView == self.historyCollectionView && ([self.historyArray[indexPath.row].status isEqualToString:@"cancel"] || [self.historyArray[indexPath.row].status isEqualToString:@"refund"])) {
        return CGSizeMake(kScreenW, 105);
    } else {
        return CGSizeMake(kScreenW, 143);
    }
}

#pragma mark - APOrderRecordListCellDelegate

- (void)listCell:(APOrderRecordListCell *)listCell didClickActionButton:(UIButton *)sender action:(APOrderAction)action {
    switch (action) {
        case APOrderActionAccept: {
            [self presentAlertWithTitle:@"确定接单吗？" message:nil cancelTitle:@"取消" cancelHandler:nil actionTitle:@"确定" actionHandler:^(UIAlertAction * _Nonnull action) {
                [self requestOrderActionWithURL:@"ap.order.receive" model:listCell.model sender:sender];
            }];
            break;
        }
        case APOrderActionRefuse: {
            [self presentAlertWithTitle:@"确定拒绝接单吗？" message:nil cancelTitle:@"取消" cancelHandler:nil actionTitle:@"确定" actionHandler:^(UIAlertAction * _Nonnull action) {
                [self requestOrderActionWithURL:@"ap.order.refuse" model:listCell.model sender:sender];
            }];
            break;
        }
        case APOrderActionServe:
            [self requestOrderActionWithURL:@"ap.order.server" model:listCell.model sender:sender];
            break;
        case APOrderActionComplete: {
            [self presentAlertWithTitle:@"请确保服务结束后完成订单，完成后钱款将进入对方账户" message:nil cancelTitle:@"取消" cancelHandler:nil actionTitle:@"确定" actionHandler:^(UIAlertAction * _Nonnull action) {
                [self requestOrderActionWithURL:@"ap.order.complete" model:listCell.model sender:sender];
            }];
            break;
        }
        case APOrderActionContactUs:
            self.stopTimer = NO;
            [self pushToConversationViewControllerWithTargetId:@"13"];
            break;
        case APOrderActionAgain: {
            self.stopTimer = NO;
            NSDictionary *params = @{@"anchorId": listCell.model.receiveUserId};
            [RequestUtils commonGetRequestUtils:[params mutableCopy] bURL:@"ap.order.pre" bAnimated:YES bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
                APListModel *model = [APListModel initWithDictionary:suceess];
                model.userId = listCell.model.receiveUserId.stringValue;
                NSMutableArray *items = [NSMutableArray new];
                for (NSDictionary *dic in suceess[@"games"]) {
                    APItemModel *item = [APItemModel initWithDictionary:dic];
                    [items addObject:item];
                    if ([item.itemId isEqualToString:listCell.model.itemId]) {
                        model.itemId = item.itemId;
                        model.image = item.image;
                        model.itemName = item.name;
                        model.price = item.price;
                        model.unit = item.unit;
                    }
                }
                if (model.itemId.notEmpty) {
                    APOrderDetailViewController *vc = [APOrderDetailViewController new];
                    vc.submitted = NO;
                    vc.model = model;
                    vc.items = items;
                    vc.currentTime = suceess[@"time"];
                    [self.navigationController pushViewController:vc animated:YES];
                } else {
                    [self presentAlertWithTitle:@"对方暂时不方便接单，请沟通后再次下单" message:nil cancelTitle:nil cancelHandler:nil actionTitle:@"知道了" actionHandler:nil];
                }
            } bFailure:^(id  _Nullable failure) {
                
            }];
            break;
        }
        case APOrderActionContactUser:
            self.stopTimer = NO;
            [self pushToConversationViewControllerWithTargetId:listCell.model.receiveUserId.stringValue];
            break;
        default:
            break;
    }
}

#pragma mark - Setters & getters

- (LazyPageScrollView *)lazyPageScrollView {
    if (!_lazyPageScrollView) {
        _lazyPageScrollView = [[LazyPageScrollView alloc] initWithFrame:CGRectMake(0, NavHeight, kScreenW, kScreenH - NavHeight)];
        _lazyPageScrollView.backgroundColor = [UIColor whiteColor];
        [_lazyPageScrollView initTab:YES Gap:0 TabHeight:44 VerticalDistance:5 BkColor:[UIColor whiteColor]];
        [_lazyPageScrollView setTitleStyle:[UIFont systemFontOfSize:15] SelFont:[UIFont systemFontOfSize:15] Color:[UIColor colorWithHexString:@"999999"] SelColor:[UIColor colorWithHexString:@"333333"]];
        [_lazyPageScrollView enableTabBottomLine:YES LineHeight:3 LineColor:[UIColor colorWithHexString:COMMON_PINK] LineBottomGap:5 ExtraWidth:-34];
        [_lazyPageScrollView addTab:SPDStringWithKey(@"进行订单", nil) View:self.ongoingCollectionView.superview Info:nil];
        [_lazyPageScrollView addTab:SPDStringWithKey(@"历史订单", nil) View:self.historyCollectionView.superview Info:nil];
        [_lazyPageScrollView generate:nil];
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 48, kScreenW, 1)];
        view.backgroundColor = [UIColor colorWithHexString:@"E1E1E1"];
        [_lazyPageScrollView addSubview:view];
    }
    return _lazyPageScrollView;
}

- (UICollectionView *)ongoingCollectionView {
    if (!_ongoingCollectionView) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.minimumLineSpacing = 5;
        layout.minimumInteritemSpacing = 0;
        layout.sectionInset = UIEdgeInsetsMake(5, 0, 5, 0);
        _ongoingCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH - NavHeight - 49) collectionViewLayout:layout];
        _ongoingCollectionView.backgroundColor = [UIColor colorWithHexString:@"F3F3F3"];
        _ongoingCollectionView.delegate = self;
        _ongoingCollectionView.dataSource = self;
        [_ongoingCollectionView registerNib:[UINib nibWithNibName:@"APOrderRecordListCell" bundle:nil] forCellWithReuseIdentifier:@"APOrderRecordListCell"];
        
        __weak typeof(self) weakself = self;
        _ongoingCollectionView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            weakself.ongoingPage = 0;
            [weakself requestOrderRecordWithType:@"proceed"];
        }];
        _ongoingCollectionView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            [weakself requestOrderRecordWithType:@"proceed"];
        }];
        _ongoingCollectionView.mj_footer.hidden = YES;
        
        APOrderRecordEmptyView *view = [[NSBundle mainBundle] loadNibNamed:@"APOrderRecordEmptyView" owner:self options:nil].firstObject;
        view.frame = CGRectMake(0, 0, kScreenW, kScreenH - NavHeight - 49);
        view.textLabel.text = SPDStringWithKey(self.isReceive ? @"加速曝光有利于接单～" : @"还未有过订单，快去下单吧～", nil);
        [view addSubview:_ongoingCollectionView];
    }
    return _ongoingCollectionView;
}

- (UICollectionView *)historyCollectionView {
    if (!_historyCollectionView) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.minimumLineSpacing = 5;
        layout.minimumInteritemSpacing = 0;
        layout.sectionInset = UIEdgeInsetsMake(5, 0, 5, 0);
        _historyCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH - NavHeight - 49) collectionViewLayout:layout];
        _historyCollectionView.backgroundColor = [UIColor colorWithHexString:@"F3F3F3"];
        _historyCollectionView.delegate = self;
        _historyCollectionView.dataSource = self;
        [_historyCollectionView registerNib:[UINib nibWithNibName:@"APOrderRecordListCell" bundle:nil] forCellWithReuseIdentifier:@"APOrderRecordListCell"];
        
        __weak typeof(self) weakself = self;
        _historyCollectionView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            weakself.historyPage = 0;
            [weakself requestOrderRecordWithType:@"history"];
        }];
        _historyCollectionView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            [weakself requestOrderRecordWithType:@"history"];
        }];
        _historyCollectionView.mj_footer.hidden = YES;
        
        APOrderRecordEmptyView *view = [[NSBundle mainBundle] loadNibNamed:@"APOrderRecordEmptyView" owner:self options:nil].firstObject;
        view.frame = CGRectMake(0, 0, kScreenW, kScreenH - NavHeight - 49);
        view.textLabel.text = SPDStringWithKey(self.isReceive ? @"加速曝光有利于接单～" : @"还未有过订单，快去下单吧～", nil);
        [view addSubview:_historyCollectionView];
    }
    return _historyCollectionView;
}

- (NSMutableArray *)ongoingArray {
    if (!_ongoingArray) {
        _ongoingArray = [NSMutableArray array];
    }
    return _ongoingArray;
}

- (NSMutableArray *)historyArray {
    if (!_historyArray) {
        _historyArray = [NSMutableArray array];
    }
    return _historyArray;
}

@end
