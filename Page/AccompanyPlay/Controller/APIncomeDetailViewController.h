//
//  APIncomeDetailViewController.h
//  SimpleDate
//
//  Created by 李楠 on 2019/9/2.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface APIncomeDetailViewController : BaseViewController

@property (nonatomic, copy) NSString *week;

@end

NS_ASSUME_NONNULL_END
