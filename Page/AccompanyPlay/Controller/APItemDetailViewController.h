//
//  APItemDetailViewController.h
//  SimpleDate
//
//  Created by 李楠 on 2019/8/28.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@class APItemModel;

@interface APItemDetailViewController : BaseViewController

@property (nonatomic, strong) APItemModel *model;

@end

NS_ASSUME_NONNULL_END
