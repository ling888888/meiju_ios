//
//  APAllItemsViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2019/8/28.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APAllItemsViewController.h"
#import "APItemCategoryView.h"
#import "APItemCell.h"
#import "APItemModel.h"
#import "APItemDetailViewController.h"

@interface APAllItemsViewController ()<UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray<NSString *> *categorys;
@property (nonatomic, strong) NSMutableArray<NSArray *> *items;

@end

@implementation APAllItemsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = SPDStringWithKey(@"全部", nil);
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self requesApGames];
}

#pragma mark - Private methods

- (void)requesApGames {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"ap.games" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        for (NSDictionary *dic in suceess[@"list"]) {
            [self.categorys addObject:dic[@"category"]];
            NSMutableArray *items = [NSMutableArray new];
            for (NSDictionary *item in dic[@"games"]) {
                APItemModel *model = [APItemModel initWithDictionary:item];
                [items addObject:model];
            }
            [self.items addObject:items];
        }
        [self.collectionView reloadData];
    } bFailure:^(id  _Nullable failure) {

    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.categorys.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.items[section].count;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    APItemCategoryView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"APItemCategoryView" forIndexPath:indexPath];
    view.categoryLabel.text = self.categorys[indexPath.section];
    return view;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    APItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"APItemCell" forIndexPath:indexPath];
    APItemModel *model = self.items[indexPath.section][indexPath.row];
    [cell.imageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, model.image]]];
    cell.nameLabel.text = model.name;
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    APItemDetailViewController *vc = [APItemDetailViewController new];
    vc.model = self.items[indexPath.section][indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Setters & Getters

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.itemSize = CGSizeMake(74, 100);
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 15;
        layout.sectionInset = UIEdgeInsetsMake(0, 25, 0, 25);
        layout.headerReferenceSize = CGSizeMake(kScreenW, 46);
        _collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerNib:[UINib nibWithNibName:@"APItemCategoryView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"APItemCategoryView"];
        [_collectionView registerNib:[UINib nibWithNibName:@"APItemCell" bundle:nil] forCellWithReuseIdentifier:@"APItemCell"];
        [self.view addSubview:_collectionView];
    }
    return _collectionView;
}

- (NSMutableArray *)categorys {
    if (!_categorys) {
        _categorys = [NSMutableArray new];
    }
    return _categorys;
}

- (NSMutableArray *)items {
    if (!_items) {
        _items = [NSMutableArray new];
    }
    return _items;
}

@end
