//
//  APAnchorListViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2019/9/25.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APAnchorListViewController.h"
#import "APAnchorListCell.h"
#import "APAnchorModel.h"

@interface APAnchorListViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSString *weak;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray<APAnchorModel *> *dataArray;
@property (nonatomic, strong) UIButton *weekButton;
@property (nonatomic, strong) UIButton *changeWeekButton;

@end

@implementation APAnchorListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = SPDStringWithKey(@"我的主播", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.weekButton];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.weak = @"this";
    [self requestApAnchorList];
}

#pragma mark - Private methods

- (void)requestApAnchorList {
    NSDictionary *params = @{@"week": self.weak, @"page": @(self.page)};
    [RequestUtils commonGetRequestUtils:[params mutableCopy] bURL:@"ap.anchor.list" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if (self.page == 0) {
            [self.dataArray removeAllObjects];
        }
        
        NSArray *list = suceess[@"list"];
        for (NSDictionary *dic in list) {
            APAnchorModel *model = [APAnchorModel initWithDictionary:dic];
            [self.dataArray addObject:model];
        }
        [self.tableView reloadData];
        self.page++;
        
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        self.tableView.mj_footer.hidden = !list.count;
    } bFailure:^(id  _Nullable failure) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }];
}

#pragma mark - Event responses

- (void)clickWeekButton:(UIButton *)sender {
    if (self.changeWeekButton.superview.alpha) {
        [UIView animateWithDuration:0.25 animations:^{
            self.changeWeekButton.superview.alpha = 0;
        }];
    } else {
        NSString *title = SPDStringWithKey([self.weak isEqualToString:@"this"] ? @"上周" : @"本周", nil);
        [self.changeWeekButton setTitle:title forState:UIControlStateNormal];
        [UIView animateWithDuration:0.25 animations:^{
            self.changeWeekButton.superview.alpha = 1;
        }];
    }
}

- (void)clickChangeWeekButton:(UIButton *)sender {
    self.weak = [self.weak isEqualToString:@"this"] ? @"last" : @"this";
    self.page = 0;
    [self requestApAnchorList];
    
    NSString *title = SPDStringWithKey([self.weak isEqualToString:@"this"] ? @"本周" : @"上周" , nil);
    [self.weekButton setTitle:title forState:UIControlStateNormal];
    [UIView animateWithDuration:0.25 animations:^{
        self.changeWeekButton.superview.alpha = 0;
    }];
}

- (void)tapBackgroundView:(UITapGestureRecognizer *)sender {
    [UIView animateWithDuration:0.25 animations:^{
        self.changeWeekButton.superview.alpha = 0;
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    APAnchorListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"APAnchorListCell" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self pushToDetailTableViewController:self userId:self.dataArray[indexPath.row].userId];
}

#pragma mark - Setters & getters

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.rowHeight = 94;
        _tableView.separatorColor = [UIColor colorWithHexString:@"E1E1E1"];
        _tableView.tableFooterView = [UIView new];
        [_tableView registerNib:[UINib nibWithNibName:@"APAnchorListCell" bundle:nil] forCellReuseIdentifier:@"APAnchorListCell"];
        
        __weak typeof(self) weakself = self;
        _tableView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            weakself.page = 0;
            [weakself requestApAnchorList];
        }];
        _tableView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            [weakself requestApAnchorList];
        }];
        _tableView.mj_footer.hidden = YES;
        
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

- (UIButton *)weekButton {
    if (!_weekButton) {
        _weekButton = [UIButton buttonWithType:UIButtonTypeSystem];
        _weekButton.frame = CGRectMake(0, 0, 85, 30);
        _weekButton.backgroundColor = [UIColor whiteColor];
        _weekButton.layer.cornerRadius = 15;
        _weekButton.tintColor = [UIColor colorWithHexString:COMMON_PINK];
        _weekButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_weekButton setTitle:SPDStringWithKey(@"本周", nil) forState:UIControlStateNormal];
        [_weekButton setImage:[UIImage imageNamed:@"ap_filter_arrow_normal"] forState:UIControlStateNormal];
        if ([SPDCommonTool currentVersionIsArbic]) {
            _weekButton.imageEdgeInsets = UIEdgeInsetsMake(0, 7, 0, 0);
            _weekButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 7);
        } else {
            _weekButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 7);
            _weekButton.titleEdgeInsets = UIEdgeInsetsMake(0, 7, 0, 0);
        }
        [_weekButton addTarget:self action:@selector(clickWeekButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _weekButton;
}

- (UIButton *)changeWeekButton {
    if (!_changeWeekButton) {
        UIView *backgroundView = [[UIView alloc] initWithFrame:self.view.bounds];
        backgroundView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        backgroundView.alpha = 0;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapBackgroundView:)];
        [backgroundView addGestureRecognizer:tap];
        [self.view addSubview:backgroundView];
        
        _changeWeekButton = [UIButton buttonWithType:UIButtonTypeSystem];
        _changeWeekButton.backgroundColor = [UIColor whiteColor];
        _changeWeekButton.layer.cornerRadius = 15;
        _changeWeekButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_changeWeekButton setTitleColor:[UIColor colorWithHexString:COMMON_PINK] forState:UIControlStateNormal];
        [_changeWeekButton addTarget:self action:@selector(clickChangeWeekButton:) forControlEvents:UIControlEventTouchUpInside];
        [backgroundView addSubview:_changeWeekButton];
        [_changeWeekButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(NavHeight + 3);
            make.trailing.mas_equalTo(-16);
            make.width.mas_equalTo(85);
            make.height.mas_equalTo(30);
        }];
    }
    return _changeWeekButton;
}

@end

