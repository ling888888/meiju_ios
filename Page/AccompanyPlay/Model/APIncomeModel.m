//
//  APIncomeModel.m
//  SimpleDate
//
//  Created by 李楠 on 2019/9/3.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APIncomeModel.h"

@implementation APIncomeModel

- (void)setValue:(id)value forKey:(nonnull NSString *)key {
    if ([key isEqualToString:@"time"]) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        dateFormat.dateFormat = @"yyyy-MM-dd HH:mm";
        dateFormat.timeZone = [NSTimeZone localTimeZone];
        NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:[value longLongValue] / 1000];
        self.time = [dateFormat stringFromDate:date];
    } else {
        [super setValue:value forKey:key];
    }
}

- (void)setValue:(id)value forUndefinedKey:(nonnull NSString *)key {
    if ([key isEqualToString:@"earning"]) {
        self.income = [NSString stringWithFormat:@"$%@", value];
    }
}

@end
