//
//  APListModel.h
//  SimpleDate
//
//  Created by 李楠 on 2019/8/28.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "CommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface APListModel : CommonModel

@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSNumber *receiveOrderNum;
@property (nonatomic, copy) NSString *receiveOrderTime;
@property (nonatomic, copy) NSNumber *price;
@property (nonatomic, copy) NSNumber *unit;

// recommend list
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSNumber *age;
@property (nonatomic, copy) NSNumber *level;
@property (nonatomic, assign) BOOL isOnline;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *itemName;

// user detail
@property (nonatomic, copy) NSString *itemId;

@end

NS_ASSUME_NONNULL_END
