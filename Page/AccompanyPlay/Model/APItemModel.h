//
//  APItemModel.h
//  SimpleDate
//
//  Created by 李楠 on 2019/8/28.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "CommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface APItemModel : CommonModel

@property (nonatomic, copy) NSString *itemId;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *name;

// anchor detail
@property (nonatomic, copy) NSNumber *price;
@property (nonatomic, copy) NSNumber *unit;
@property (nonatomic, assign) BOOL isReceive;
@property (nonatomic, copy) NSArray *prices;
@property (nonatomic, copy) NSArray *units;

@end

NS_ASSUME_NONNULL_END
