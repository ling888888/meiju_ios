//
//  APPlaceOrderMessage.h
//  SimpleDate
//
//  Created by 李楠 on 2019/9/24.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

NS_ASSUME_NONNULL_BEGIN

@interface APPlaceOrderMessage : RCMessageContent

@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, copy) NSString *gameName;
@property (nonatomic, copy) NSString *earning;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *orderTime;
@property (nonatomic, copy) NSString *unit;
@property (nonatomic, assign) NSInteger countDown;
@property (nonatomic, copy) NSString *remark;

@end

NS_ASSUME_NONNULL_END
