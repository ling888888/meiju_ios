//
//  APAnchorModel.h
//  SimpleDate
//
//  Created by 李楠 on 2019/9/25.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "CommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface APAnchorModel : CommonModel

@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSNumber *age;
@property (nonatomic, copy) NSNumber *level;
@property (nonatomic, copy) NSNumber *earning;
@property (nonatomic, copy) NSNumber *receiveOrderNum;

@end

NS_ASSUME_NONNULL_END
