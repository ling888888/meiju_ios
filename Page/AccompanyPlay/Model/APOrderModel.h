//
//  APOrderModel.h
//  SimpleDate
//
//  Created by 李楠 on 2019/9/16.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "CommonModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, APOrderAction) {
    APOrderActionSubmit,
    APOrderActionAccept,
    APOrderActionRefuse,
    APOrderActionServe,
    APOrderActionComplete,
    APOrderActionContactUs,
    APOrderActionAgain,
    APOrderActionContactUser
};

@interface APOrderModel : CommonModel

@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, copy) NSNumber *placeUserId;
@property (nonatomic, copy) NSNumber *receiveUserId;
@property (nonatomic, copy) NSString *itemId;
@property (nonatomic, copy) NSString *status; // await, agree, process, done, cancel, refund
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *orderTime;
@property (nonatomic, copy) NSString *unit;
@property (nonatomic, assign) NSInteger countDown;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *earning;
@property (nonatomic, copy) NSNumber *cost;

@end

NS_ASSUME_NONNULL_END
