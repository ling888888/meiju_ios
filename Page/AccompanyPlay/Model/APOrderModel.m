//
//  APOrderModel.m
//  SimpleDate
//
//  Created by 李楠 on 2019/9/16.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APOrderModel.h"

@implementation APOrderModel

- (void)setValue:(id)value forKey:(nonnull NSString *)key {
    if ([key isEqualToString:@"orderTime"]) {
        self.orderTime = [SPDCommonTool orderTimeStingWithTimeStamp:[value longLongValue] / 1000];
    } else if ([key isEqualToString:@"unit"]) {
        self.unit = [NSString stringWithFormat:@"%@min", value];
    } else if ([key isEqualToString:@"countDown"]) {
        self.countDown = [value integerValue] / 1000;
    } else if ([key isEqualToString:@"earning"]) {
        self.earning = [NSString stringWithFormat:@"$%@", value];
    } else {
        [super setValue:value forKey:key];
    }
}

- (void)setValue:(id)value forUndefinedKey:(nonnull NSString *)key {
    if ([key isEqualToString:@"gameId"]) {
        self.itemId = value;
    }
}

@end
