//
//  APItemModel.m
//  SimpleDate
//
//  Created by 李楠 on 2019/8/28.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APItemModel.h"

@implementation APItemModel

- (void)setValue:(id)value forUndefinedKey:(nonnull NSString *)key {
    if ([key isEqualToString:@"gameId"]) {
        self.itemId = value;
    }
}

@end
