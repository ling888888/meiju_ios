//
//  APIncomeModel.h
//  SimpleDate
//
//  Created by 李楠 on 2019/9/3.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "CommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface APIncomeModel : CommonModel

@property (nonatomic, copy) NSString *income;
@property (nonatomic, copy) NSString *time;

@end

NS_ASSUME_NONNULL_END
