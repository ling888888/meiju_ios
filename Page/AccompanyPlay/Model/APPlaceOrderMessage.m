//
//  APPlaceOrderMessage.m
//  SimpleDate
//
//  Created by 李楠 on 2019/9/24.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APPlaceOrderMessage.h"
#import "NSObject+Coding.h"

@implementation APPlaceOrderMessage

// 消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return MessagePersistent_STATUS;
}

// NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        [self hl_decodeWithDeCoder:aDecoder];
    }
    return self;
}

// NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [self hl_encodeWithCoder:aCoder];
}

// 将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [NSMutableDictionary dictionary];
    if (self.orderId) {
        [dataDict setObject:self.orderId forKey:@"orderId"];
    }
    if (self.gameName) {
        [dataDict setObject:self.gameName forKey:@"gameName"];
    }
    if (self.earning) {
        [dataDict setObject:self.earning forKey:@"earning"];
    }
    if (self.userName) {
        [dataDict setObject:self.userName forKey:@"userName"];
    }
    if (self.orderTime) {
        [dataDict setObject:self.orderTime forKey:@"orderTime"];
    }
    if (self.unit) {
        [dataDict setObject:self.unit forKey:@"unit"];
    }
    if (self.countDown) {
        [dataDict setObject:@(self.countDown) forKey:@"countDown"];
    }
    if (self.remark) {
        [dataDict setObject:self.remark forKey:@"remark"];
    }
    if (self.senderUserInfo) {
        NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc] init];
        if (self.senderUserInfo.name) {
            [userInfoDic setObject:self.senderUserInfo.name
                 forKeyedSubscript:@"name"];
        }
        if (self.senderUserInfo.portraitUri) {
            [userInfoDic setObject:self.senderUserInfo.portraitUri
                 forKeyedSubscript:@"icon"];
        }
        if (self.senderUserInfo.userId) {
            [userInfoDic setObject:self.senderUserInfo.userId
                 forKeyedSubscript:@"id"];
        }
        [dataDict setObject:userInfoDic forKey:@"user"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

// 将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        
        if (dictionary) {
            self.orderId = dictionary[@"orderId"];
            self.gameName = dictionary[@"gameName"];
            self.earning = [NSString stringWithFormat:@"$%@", dictionary[@"earning"]];
            self.userName = dictionary[@"userName"];
            self.orderTime = [SPDCommonTool orderTimeStingWithTimeStamp:[dictionary[@"orderTime"] longLongValue] / 1000];
            self.unit = dictionary[@"unit"];
            self.countDown = [dictionary[@"countDown"] integerValue] / 1000;
            self.remark = dictionary[@"remark"];
            NSDictionary *userinfoDic = dictionary[@"user"];
            [self decodeUserInfo:userinfoDic];
        }
    }
}

// 会话列表中显示的摘要
- (NSString *)conversationDigest {
    return @"";
}

// 消息的类型名
+ (NSString *)getObjectName {
    return @"SPD:apPlaceOrderMsg";
}

@end
