//
//  APListCell.h
//  SimpleDate
//
//  Created by 李楠 on 2019/8/27.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class APListCell, APListModel;

@protocol APListCellDelegate <NSObject>

@optional

- (void)listCell:(APListCell *)listCell didClickOrderButton:(UIButton *)sender;

@end

@interface APListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *levelButton;
@property (weak, nonatomic) IBOutlet UIButton *orderButton;

@property (nonatomic, strong) APListModel *model;
@property (nonatomic, weak) id<APListCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
