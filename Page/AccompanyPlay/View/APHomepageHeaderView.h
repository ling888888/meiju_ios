//
//  APHomepageHeaderView.h
//  SimpleDate
//
//  Created by 李楠 on 2019/8/27.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class APItemModel;

@protocol APHomepageHeaderViewDelegate <NSObject>

@optional

- (void)didClickItemWithModel:(APItemModel *)model;
- (void)didClickMoreItems;

@end

@interface APHomepageHeaderView : UIView

@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, weak) id<APHomepageHeaderViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
