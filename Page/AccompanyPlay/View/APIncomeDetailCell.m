//
//  APIncomeDetailCell.m
//  SimpleDate
//
//  Created by 李楠 on 2019/9/3.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APIncomeDetailCell.h"

@interface APIncomeDetailCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;

@end

@implementation APIncomeDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.titleLabel.text = SPDStringWithKey(@"接单收入", nil);
    self.stateLabel.text = SPDStringWithKey(@"已完成", nil);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
