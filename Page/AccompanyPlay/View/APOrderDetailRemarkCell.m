//
//  APOrderDetailRemarkCell.m
//  SimpleDate
//
//  Created by 李楠 on 2019/9/17.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APOrderDetailRemarkCell.h"
#import "LDTextView.h"

@interface APOrderDetailRemarkCell ()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet LDTextView *textView;

@end

@implementation APOrderDetailRemarkCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.titleLabel.text = [NSString stringWithFormat:@"%@:", SPDStringWithKey(@"备注", nil)];
    self.textView.textContainerInset = UIEdgeInsetsZero;
    self.textView.placeholderColor = [UIColor colorWithHexString:@"CCCCCC"];
    self.textView.placeholderFont = [UIFont systemFontOfSize:15];
    self.textView.placeholder = SPDStringWithKey(@"可输入50字陪玩要求", nil);
    if ([SPDCommonTool currentVersionIsArbic]) {
        self.textView.textAlignment = NSTextAlignmentLeft;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    } else {
        return YES;
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    if (textView.text.length > 50 && ![textView markedTextRange]) {
        NSRange range = [textView.text rangeOfComposedCharacterSequenceAtIndex:50];
        textView.text = [textView.text substringToIndex:range.location];
    }
    self.remark = textView.text;
}

@end
