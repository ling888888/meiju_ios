//
//  APAnchorListCell.h
//  SimpleDate
//
//  Created by 李楠 on 2019/9/25.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class APAnchorModel;

@interface APAnchorListCell : UITableViewCell

@property (nonatomic, strong) APAnchorModel *model;

@end

NS_ASSUME_NONNULL_END
