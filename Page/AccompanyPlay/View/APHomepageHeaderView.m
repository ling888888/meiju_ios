//
//  APHomepageHeaderView.m
//  SimpleDate
//
//  Created by 李楠 on 2019/8/27.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APHomepageHeaderView.h"
#import "APItemCell.h"
#import "APItemModel.h"

@interface APHomepageHeaderView ()<UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;

@end

@implementation APHomepageHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"APItemCell" bundle:nil] forCellWithReuseIdentifier:@"APItemCell"];
    self.flowLayout.itemSize = CGSizeMake((kScreenW - 4 * 2) / 5, 100);
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count + 1;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    APItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"APItemCell" forIndexPath:indexPath];
    if (indexPath.row < self.dataArray.count) {
        APItemModel *model = self.dataArray[indexPath.row];
        [cell.imageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, model.image]]];
        cell.nameLabel.text = model.name;
    } else {
        cell.imageView.image = [UIImage imageNamed:@"ap_item_more"];
        cell.nameLabel.text = SPDStringWithKey(@"更多", nil);
    }
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.dataArray.count) {
        if ([self.delegate respondsToSelector:@selector(didClickMoreItems)]) {
            [self.delegate didClickMoreItems];
        }
    } else {
        if ([self.delegate respondsToSelector:@selector(didClickItemWithModel:)]) {
            [self.delegate didClickItemWithModel:self.dataArray[indexPath.row]];
        }
    }
}

#pragma mark - Setters & Getters

- (void)setDataArray:(NSMutableArray *)dataArray {
    _dataArray = dataArray;
    
    [self.collectionView reloadData];
}

@end
