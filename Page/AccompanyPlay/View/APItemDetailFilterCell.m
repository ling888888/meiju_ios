//
//  APItemDetailFilterCell.m
//  SimpleDate
//
//  Created by 李楠 on 2019/8/30.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APItemDetailFilterCell.h"

@implementation APItemDetailFilterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    self.optionLabel.textColor = [UIColor colorWithHexString:selected ? COMMON_PINK : @"333333"];
    self.selectedImageView.hidden = !selected;
}

@end
