//
//  APPickerView.m
//  SimpleDate
//
//  Created by 李楠 on 2019/9/2.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APPickerView.h"
#import "APPickerItemView.h"
#import "APItemModel.h"

@interface APPickerView ()<UIPickerViewDataSource, UIPickerViewDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pickerViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pickerViewBottom;
@property (weak, nonatomic) IBOutlet UIView *lineView;

@property (nonatomic, strong) NSMutableArray *selectedValues;

@end

@implementation APPickerView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.cancelButton setTitle:SPDStringWithKey(@"取消", nil) forState:UIControlStateNormal];
    [self.confirmButton setTitle:SPDStringWithKey(@"确定", nil) forState:UIControlStateNormal];
    
    self.contentView.frame = CGRectMake(0, kScreenH, kScreenW, 250);
}

#pragma mark - Public methods

- (void)show {
    self.hidden = NO;
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        self.pickerViewBottom.constant = IphoneX_Bottom;
        [self layoutIfNeeded];
    }];
}

#pragma mark - Private methods

- (void)hide {
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [UIColor clearColor];
        self.pickerViewBottom.constant = -205;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

#pragma mark - Event responses

- (IBAction)clickCancelButton:(UIButton *)sender {
    [self hide];
}

- (IBAction)clickConfirmButton:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(pickerView:didClickConfirmButtonWithSelectedValues:)]) {
        [self.selectedValues removeAllObjects];

        NSInteger component = 0;
        for (NSArray *array in self.dataArray) {
            NSInteger selectedRow = [self.pickerView selectedRowInComponent:component];
            switch (self.type) {
                case APPickerTypeOrderTime:
                    [self.selectedValues addObject:array[selectedRow]];
                    if (component == 1 && [self.selectedValues.firstObject integerValue] > [self.selectedValues.lastObject integerValue]) {
                        [SPDCommonTool showWindowToast:SPDStringWithKey(@"接单结束时段不可小于开始时段", nil)];
                        return;
                    }
                    break;
                case APPickerTypeSeviceTime:
                    switch (component) {
                        case 0:
                            [self.selectedValues addObject:array[selectedRow]];
                            break;
                        case 1: {
                            NSInteger index = [self.pickerView selectedRowInComponent:0] == 0 ? 0 : 1;
                            [self.selectedValues addObject:array[index][selectedRow]];
                            break;
                        }
                        default: {
                            NSInteger index = [self.pickerView selectedRowInComponent:0] == 0 && [self.pickerView selectedRowInComponent:1] == 0 ? 0 : 1;
                            [self.selectedValues addObject:array[index][selectedRow]];
                            break;
                        }
                    }
                    break;
                default:
                    [self.selectedValues addObject:array[selectedRow]];
                    break;
            }
            component++;
        }
        [self.delegate pickerView:self didClickConfirmButtonWithSelectedValues:self.selectedValues];
    }
    [self hide];
}

- (IBAction)tapBackground:(UITapGestureRecognizer *)sender {
    [self hide];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return self.dataArray.count;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    switch (self.type) {
        case APPickerTypeSeviceTime:
            switch (component) {
                case 0:
                    return self.dataArray[component].count;
                case 1: {
                    NSInteger index = [pickerView selectedRowInComponent:0] == 0 ? 0 : 1;
                    NSArray *array = self.dataArray[1][index];
                    return array.count;
                }
                default: {
                    NSInteger index = [pickerView selectedRowInComponent:0] == 0 && [pickerView selectedRowInComponent:1] == 0 ? 0 : 1;
                    NSArray *array = self.dataArray[2][index];
                    return array.count;
                }
            }
        default:
            return self.dataArray[component].count;
    }
}

#pragma mark - UIPickerViewDelegate

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    switch (self.type) {
        case APPickerTypeItem:
            return 60;
        default:
            return 35;
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    switch (self.type) {
        case APPickerTypeItem: {
            APPickerItemView *itemView;
            if ([view isKindOfClass:[APPickerItemView class]]) {
                itemView = (APPickerItemView *)view;
            } else {
                itemView = [[NSBundle mainBundle] loadNibNamed:@"APPickerItemView" owner:self options:nil].firstObject;
            }
            
            APItemModel *model = self.dataArray[component][row];
            [itemView.imageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, model.image]]];
            itemView.nameLabel.text = model.name;
            return itemView;
        }
        default: {
            UILabel *label;
            if ([view isKindOfClass:[UILabel class]]) {
                label = (UILabel *)view;
            } else {
                label = [UILabel new];
                label.font = [UIFont systemFontOfSize:15];
                label.textColor = [UIColor colorWithHexString:@"333333"];
                label.textAlignment = NSTextAlignmentCenter;
            }
            
            switch (self.type) {
                case APPickerTypePrice:
                    label.text = [NSString stringWithFormat:@"%@%@", self.dataArray[component][row], component == 0 ? SPDStringWithKey(@"钻石", nil) : @"min"];
                    break;
                case APPickerTypeSeviceTime:
                    switch (component) {
                        case 0:
                            label.text = self.dataArray[component][row];
                            break;
                        case 1: {
                            NSInteger index = [pickerView selectedRowInComponent:0] == 0 ? 0 : 1;
                            NSArray *array = self.dataArray[1][index];
                            label.text = array[row];
                            break;
                        }
                        default: {
                            NSInteger index = [pickerView selectedRowInComponent:0] == 0 && [pickerView selectedRowInComponent:1] == 0 ? 0 : 1;
                            NSArray *array = self.dataArray[2][index];
                            label.text = array[row];
                            break;
                        }
                    }
                    break;
                default:
                    label.text = self.dataArray[component][row];
                    break;
            }
            return label;
        }
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    switch (self.type) {
        case APPickerTypeSeviceTime:
            switch (component) {
                case 0:
                    [pickerView reloadComponent:1];
                    [pickerView selectRow:0 inComponent:1 animated:NO];
                    [pickerView reloadComponent:2];
                    [pickerView selectRow:0 inComponent:2 animated:NO];
                    break;
                case 1: {
                    [pickerView reloadComponent:2];
                    [pickerView selectRow:0 inComponent:2 animated:NO];
                    break;
                }
                default:
                    break;
            }
            break;
        default:
            break;
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ![touch.view isDescendantOfView:self.contentView];
}

#pragma mark - Setters & getters

- (void)setType:(APPickerType)type {
    _type = type;
    
    switch (_type) {
        case APPickerTypeOrderTime:
            self.titleLabel.text = SPDStringWithKey(@"选择接单时段", nil);
            self.descLabel.text = SPDStringWithKey(@"所设置时间内不接单会产生负面影响", nil);
            self.lineView.hidden = NO;
            self.pickerViewTop.constant = 30;
            break;
        case APPickerTypePrice:
            self.titleLabel.text = SPDStringWithKey(@"选择接单价格", nil);
            self.descLabel.text = SPDStringWithKey(@"提高接单数量和服务有机会调至更高价格", nil);
            self.lineView.hidden = NO;
            self.pickerViewTop.constant = 30;
            break;
        case APPickerTypeItem:
            self.titleLabel.text = SPDStringWithKey(@"选择陪玩", nil);
            self.lineView.hidden = YES;
            self.pickerViewTop.constant = 0;
            break;
        case APPickerTypeSeviceTime:
            self.titleLabel.text = SPDStringWithKey(@"选择服务时间", nil);
            self.descLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"每天%@可接单", nil), self.orderTime];
            self.lineView.hidden = YES;
            self.pickerViewTop.constant = 30;
            break;
        default:
            break;
    }
}

- (void)setDataArray:(NSArray<NSArray *> *)dataArray {
    _dataArray = dataArray;
    
    for (NSInteger component = 0; component < self.pickerView.numberOfComponents; component++) {
        [self.pickerView selectRow:0 inComponent:component animated:NO];
    }
    [self.pickerView reloadAllComponents];
}

- (NSMutableArray *)selectedValues {
    if (!_selectedValues) {
        _selectedValues = [NSMutableArray new];
    }
    return _selectedValues;
}

@end
