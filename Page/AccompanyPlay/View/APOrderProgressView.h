//
//  APOrderProgressView.h
//  SimpleDate
//
//  Created by 李楠 on 2019/9/23.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@class APOrderProgressView, APItemModel;

@protocol APOrderProgressViewDelegate <NSObject>

@optional

- (void)orderProgressView:(APOrderProgressView *)orderProgressView didClickActionButton:(UIButton *)sender action:(APOrderAction)action;

@end

@interface APOrderProgressView : UIView

@property (nonatomic, strong) APItemModel *itemMode;
@property (nonatomic, strong) APOrderModel *orderModel;
@property (nonatomic, weak) id<APOrderProgressViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
