//
//  APOrderProgressView.m
//  SimpleDate
//
//  Created by 李楠 on 2019/9/23.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APOrderProgressView.h"
#import "APItemModel.h"

@interface APOrderProgressView ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLabelTop;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton *action1Button;
@property (weak, nonatomic) IBOutlet UIButton *action2Button;
@property (weak, nonatomic) IBOutlet UIView *progressTrackView1;
@property (weak, nonatomic) IBOutlet UIView *progressDotView1;
@property (weak, nonatomic) IBOutlet UIView *progressTrackView2;
@property (weak, nonatomic) IBOutlet UIView *progressDotView2;
@property (weak, nonatomic) IBOutlet UIView *progressTrackView3;
@property (weak, nonatomic) IBOutlet UIView *progressDotView3;
@property (weak, nonatomic) IBOutlet UIView *progressTrackView4;
@property (weak, nonatomic) IBOutlet UIView *progressDotView4;
@property (weak, nonatomic) IBOutlet UIView *progressTrackView5;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel1;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel2;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel3;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel4;

@property (nonatomic, assign) APOrderAction action1;
@property (nonatomic, assign) APOrderAction action2;

@end

@implementation APOrderProgressView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.layer.shadowColor = [UIColor colorWithRed:193 / 255.0 green:193 / 255.0 blue:193 / 255.0 alpha:0.1].CGColor;
    self.layer.shadowOffset = CGSizeMake(0, 2);
    self.layer.shadowOpacity = 1;
    self.layer.shadowRadius = 6;
    
    self.progressLabel1.text = SPDStringWithKey(@"待确定", nil);
    self.progressLabel2.text = SPDStringWithKey(@"待服务", nil);
    self.progressLabel3.text = SPDStringWithKey(@"进行中", nil);
    self.progressLabel4.text = SPDStringWithKey(@"已完成", nil);
}

- (IBAction)clickAction1Button:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(orderProgressView:didClickActionButton:action:)]) {
        [self.delegate orderProgressView:self didClickActionButton:sender action:self.action1];
    }
}

- (IBAction)clickAction2Button:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(orderProgressView:didClickActionButton:action:)]) {
        [self.delegate orderProgressView:self didClickActionButton:sender action:self.action2];
    }
}

- (void)setItemMode:(APItemModel *)itemMode {
    _itemMode = itemMode;
    
    [self.imageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _itemMode.image]]];
    self.nameLabel.text = _itemMode.name;
    self.nameLabelTop.constant = 3;
    self.timeLabel.hidden = YES;
    self.priceLabel.hidden = NO;
    self.priceLabel.text = [NSString stringWithFormat:@"%@%@/%@min", _itemMode.price, SPDStringWithKey(@"钻", nil), _itemMode.unit];
    self.action1Button.hidden = NO;
    self.action1 = APOrderActionSubmit;
    [self.action1Button setTitle:SPDStringWithKey(@"立即下单", nil) forState:UIControlStateNormal];
    self.action2Button.hidden = YES;
}

- (void)setOrderModel:(APOrderModel *)orderModel {
    _orderModel = orderModel;
    
    [self.imageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _orderModel.image]]];
    self.nameLabel.text = _orderModel.name;
    self.nameLabelTop.constant = 10;
    self.timeLabel.hidden = NO;
    self.timeLabel.text = _orderModel.orderTime;
    self.priceLabel.hidden = YES;
    
    NSInteger progress;
    if ([_orderModel.status isEqualToString:@"await"]) {
        progress = 1;
        if (self.isReceive) {
            self.action1Button.hidden = NO;
            self.action2Button.hidden = NO;
            self.action1 = APOrderActionAccept;
            [self.action1Button setTitle:SPDStringWithKey(@"确定接单", nil) forState:UIControlStateNormal];
            self.action2 = APOrderActionRefuse;
            [self.action2Button setTitle:SPDStringWithKey(@"拒绝", nil) forState:UIControlStateNormal];
        } else {
            self.action1Button.hidden = YES;
            self.action2Button.hidden = YES;
        }
    } else if ([_orderModel.status isEqualToString:@"accept"]) {
        progress = 2;
        if (self.isReceive) {
            self.action1Button.hidden = [DBUtil queryAPServedOrderWithOrderId:_orderModel.orderId];
            self.action2Button.hidden = YES;
            self.action1 = APOrderActionServe;
            [self.action1Button setTitle:SPDStringWithKey(@"立即服务", nil) forState:UIControlStateNormal];
        } else {
            self.action1Button.hidden = YES;
            self.action2Button.hidden = YES;
        }
    } else if ([_orderModel.status isEqualToString:@"process"]) {
        progress = 3;
        if (self.isReceive) {
            self.action1Button.hidden = YES;
            self.action2Button.hidden = YES;
        } else {
            self.action1Button.hidden = NO;
            self.action2Button.hidden = NO;
            self.action1 = APOrderActionComplete;
            [self.action1Button setTitle:SPDStringWithKey(@"完成", nil) forState:UIControlStateNormal];
            self.action2 = APOrderActionContactUs;
            [self.action2Button setTitle:SPDStringWithKey(@"联系客服", nil) forState:UIControlStateNormal];
        }
    } else {
        progress = 4;
        self.action1Button.hidden = YES;
        self.action2Button.hidden = YES;
        [self viewWithTag:104].backgroundColor = [UIColor colorWithHexString:COMMON_PINK];
    }
    
    for (NSInteger i = 0; i < progress; i++) {
        [self viewWithTag:100 + i].backgroundColor = [UIColor colorWithHexString:COMMON_PINK];
        [self viewWithTag:200 + i].layer.borderColor = [UIColor colorWithHexString:COMMON_PINK].CGColor;
    }
}

- (BOOL)isReceive {
    return [[SPDApiUser currentUser].userId isEqualToString:self.orderModel.receiveUserId.stringValue];
}

@end
