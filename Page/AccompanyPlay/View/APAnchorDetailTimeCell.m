//
//  APAnchorDetailTimeCell.m
//  SimpleDate
//
//  Created by 李楠 on 2019/8/31.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APAnchorDetailTimeCell.h"

@implementation APAnchorDetailTimeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.titleLabel.text = SPDStringWithKey(@"接单时段", nil);
    self.arrowImageView.image = [UIImage imageNamed:@"ap_detail_arrow"].adaptiveRtl;
}

@end
