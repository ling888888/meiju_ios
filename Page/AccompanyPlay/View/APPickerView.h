//
//  APPickerView.h
//  SimpleDate
//
//  Created by 李楠 on 2019/9/2.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class APPickerView;

typedef NS_ENUM(NSUInteger, APPickerType) {
    APPickerTypeOrderTime,
    APPickerTypePrice,
    APPickerTypeItem,
    APPickerTypeSeviceTime
};

@protocol APPickerViewDelegate <NSObject>

@optional

- (void)pickerView:(APPickerView *)pickerView didClickConfirmButtonWithSelectedValues:(NSArray *)selectedValues;

@end

@interface APPickerView : UIView

@property (nonatomic, strong) NSString *orderTime;
@property (nonatomic, assign) APPickerType type;
@property (nonatomic, strong) NSArray<NSArray *> *dataArray;
@property (nonatomic, weak) id<APPickerViewDelegate> delegate;

- (void)show;

@end

NS_ASSUME_NONNULL_END
