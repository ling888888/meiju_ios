//
//  APReceivedOrderView.h
//  SimpleDate
//
//  Created by 李楠 on 2019/9/24.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class APPlaceOrderMessage;

@interface APReceivedOrderView : UIView

- (void)showWithMessage:(APPlaceOrderMessage *)message;

@end

NS_ASSUME_NONNULL_END
