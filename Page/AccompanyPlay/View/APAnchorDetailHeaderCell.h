//
//  APAnchorDetailHeaderCell.h
//  SimpleDate
//
//  Created by 李楠 on 2019/8/31.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class APAnchorDetailHeaderCell;

@protocol APAnchorDetailHeaderCellDelegate <NSObject>

@optional

- (void)headerCell:(APAnchorDetailHeaderCell *)headerCell didClickIncomeButtonWithWeek:(NSString *)week;

@end

@interface APAnchorDetailHeaderCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *lastWeekIncomeLabel;
@property (weak, nonatomic) IBOutlet UILabel *thisWeekIncomeLabel;

@property (nonatomic, weak) id<APAnchorDetailHeaderCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
