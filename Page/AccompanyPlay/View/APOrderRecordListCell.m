//
//  APOrderRecordListCell.m
//  SimpleDate
//
//  Created by 李楠 on 2019/9/12.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APOrderRecordListCell.h"

@interface APOrderRecordListCell ()

@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UILabel *countdownLabel;
@property (weak, nonatomic) IBOutlet UIImageView *itemImageView;
@property (weak, nonatomic) IBOutlet UIButton *action1Button;
@property (weak, nonatomic) IBOutlet UIButton *action2Button;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *action2ButtonTrailing;
@property (weak, nonatomic) IBOutlet UILabel *transactionLabel;

@property (nonatomic, assign) APOrderAction action1;
@property (nonatomic, assign) APOrderAction action2;

@end

@implementation APOrderRecordListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)orderRecordCountdown:(NSNotification *)notification {
    if ([self.model.status isEqualToString:@"await"]) {
        self.countdownLabel.text = self.model.countDown > 0 ? [NSString stringWithFormat:@"%02ld:%02ld", self.model.countDown / 60, self.model.countDown % 60] : @"00:00";
        self.model.countDown--;
    }
}

- (IBAction)clickAction1Button:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(listCell:didClickActionButton:action:)]) {
        [self.delegate listCell:self didClickActionButton:sender action:self.action1];
    }
}

- (IBAction)clickAction2Button:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(listCell:didClickActionButton:action:)]) {
        [self.delegate listCell:self didClickActionButton:sender action:self.action2];
    }
}

- (void)setModel:(APOrderModel *)model {
    _model = model;
    
    [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _model.avatar]]];
    self.timeLabel.text = _model.orderTime;
    self.durationLabel.text = _model.unit;
    [self.itemImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _model.image]]];

    if ([_model.status isEqualToString:@"await"]) {
        self.stateLabel.text = SPDStringWithKey(@"待确定", nil);
        self.countdownLabel.hidden = NO;
        self.countdownLabel.text = _model.countDown > 0 ? [NSString stringWithFormat:@"%02ld:%02ld", self.model.countDown / 60, _model.countDown % 60] : @"00:00";
        if (self.isReceive) {
            self.action1Button.hidden = NO;
            self.action2Button.hidden = NO;
            self.action1 = APOrderActionAccept;
            [self.action1Button setTitle:SPDStringWithKey(@"确定接单", nil) forState:UIControlStateNormal];
            self.action2 = APOrderActionRefuse;
            [self.action2Button setTitle:SPDStringWithKey(@"拒绝", nil) forState:UIControlStateNormal];
            self.action2ButtonTrailing.constant = 103;
            self.transactionLabel.text = [NSString stringWithFormat:@"%@: %@", SPDStringWithKey(@"预计收入", nil), _model.earning];
        } else {
            self.action1Button.hidden = YES;
            self.action2Button.hidden = YES;
            self.transactionLabel.text = [NSString stringWithFormat:@"%@: %@%@", SPDStringWithKey(@"已支付", nil), _model.cost, SPDStringWithKey(@"钻", nil)];
        }
    } else if ([_model.status isEqualToString:@"accept"]) {
        self.stateLabel.text = SPDStringWithKey(@"待服务", nil);
        self.countdownLabel.hidden = YES;
        if (self.isReceive) {
            self.action1Button.hidden = [DBUtil queryAPServedOrderWithOrderId:_model.orderId];
            self.action2Button.hidden = YES;
            self.action1 = APOrderActionServe;
            [self.action1Button setTitle:SPDStringWithKey(@"立即服务", nil) forState:UIControlStateNormal];
            self.transactionLabel.text = [NSString stringWithFormat:@"%@: %@", SPDStringWithKey(@"预计收入", nil), _model.earning];
        } else {
            self.action1Button.hidden = YES;
            self.action2Button.hidden = YES;
            self.transactionLabel.text = [NSString stringWithFormat:@"%@: %@%@", SPDStringWithKey(@"已支付", nil), _model.cost, SPDStringWithKey(@"钻", nil)];
        }
    } else if ([_model.status isEqualToString:@"process"]) {
        self.stateLabel.text = SPDStringWithKey(@"进行中", nil);
        self.countdownLabel.hidden = YES;
        if (self.isReceive) {
            self.action1Button.hidden = YES;
            self.action2Button.hidden = YES;
            self.transactionLabel.text = [NSString stringWithFormat:@"%@: %@", SPDStringWithKey(@"预计收入", nil), _model.earning];
        } else {
            self.action1Button.hidden = NO;
            self.action2Button.hidden = NO;
            self.action1 = APOrderActionComplete;
            [self.action1Button setTitle:SPDStringWithKey(@"完成", nil) forState:UIControlStateNormal];
            self.action2 = APOrderActionContactUs;
            [self.action2Button setTitle:SPDStringWithKey(@"联系客服", nil) forState:UIControlStateNormal];
            self.action2ButtonTrailing.constant = 103;
            self.transactionLabel.text = [NSString stringWithFormat:@"%@: %@%@", SPDStringWithKey(@"已支付", nil), _model.cost, SPDStringWithKey(@"钻", nil)];
        }
    } else if ([_model.status isEqualToString:@"done"]) {
        self.stateLabel.text = SPDStringWithKey(@"已完成", nil);
        self.countdownLabel.hidden = YES;
        if (self.isReceive) {
            self.action1Button.hidden = YES;
            self.action2Button.hidden = YES;
            self.transactionLabel.text = [NSString stringWithFormat:@"%@: %@", SPDStringWithKey(@"已到账", nil), _model.earning];
        } else {
            self.action1Button.hidden = NO;
            self.action2Button.hidden = NO;
            self.action1 = APOrderActionAgain;
            [self.action1Button setTitle:SPDStringWithKey(@"再来一单", nil) forState:UIControlStateNormal];
            self.action2 = APOrderActionContactUser;
            [self.action2Button setTitle:SPDStringWithKey(@"联系TA", nil) forState:UIControlStateNormal];
            self.action2ButtonTrailing.constant = 103;
            self.transactionLabel.text = [NSString stringWithFormat:@"%@: %@%@", SPDStringWithKey(@"已支付", nil), _model.cost, SPDStringWithKey(@"钻", nil)];
        }
    } else {
        self.stateLabel.text = SPDStringWithKey([_model.status isEqualToString:@"cancel"] ? @"已取消" : @"已退款", nil);
        self.countdownLabel.hidden = YES;
        self.action1Button.hidden = YES;
        self.action2Button.hidden = NO;
        self.action2 = APOrderActionContactUser;
        [self.action2Button setTitle:SPDStringWithKey(@"联系TA", nil) forState:UIControlStateNormal];
        self.action2ButtonTrailing.constant = 18;
        self.transactionLabel.text = [NSString stringWithFormat:@"%@: %@%@", SPDStringWithKey(@"已退款", nil), _model.cost, SPDStringWithKey(@"钻", nil)];
    }
}

@end
