//
//  APItemDetailFilterCell.h
//  SimpleDate
//
//  Created by 李楠 on 2019/8/30.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface APItemDetailFilterCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *optionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *selectedImageView;

@end

NS_ASSUME_NONNULL_END
