//
//  APOrderDetailRemarkCell.h
//  SimpleDate
//
//  Created by 李楠 on 2019/9/17.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface APOrderDetailRemarkCell : UITableViewCell

@property (nonatomic, strong) NSString *remark;

@end

NS_ASSUME_NONNULL_END
