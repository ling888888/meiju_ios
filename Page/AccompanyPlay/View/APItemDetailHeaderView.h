//
//  APItemDetailHeaderView.h
//  SimpleDate
//
//  Created by 李楠 on 2019/8/29.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface APItemDetailHeaderView : UIView

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;

@end

NS_ASSUME_NONNULL_END
