//
//  APOrderRecordListCell.h
//  SimpleDate
//
//  Created by 李楠 on 2019/9/12.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@class APOrderRecordListCell;

@protocol APOrderRecordListCellDelegate <NSObject>

@optional

- (void)listCell:(APOrderRecordListCell *)listCell didClickActionButton:(UIButton *)sender action:(APOrderAction)action;

@end

@interface APOrderRecordListCell : UICollectionViewCell

@property (nonatomic, assign) BOOL isReceive;
@property (nonatomic, strong) APOrderModel *model;
@property (nonatomic, weak) id<APOrderRecordListCellDelegate> delegate;

- (void)orderRecordCountdown:(NSNotification *)notification;

@end

NS_ASSUME_NONNULL_END
