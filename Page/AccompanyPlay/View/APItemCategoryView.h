//
//  APItemCategoryView.h
//  SimpleDate
//
//  Created by 李楠 on 2019/8/28.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface APItemCategoryView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;

@end

NS_ASSUME_NONNULL_END
