//
//  APPickerItemView.m
//  SimpleDate
//
//  Created by 李楠 on 2019/9/18.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APPickerItemView.h"

@implementation APPickerItemView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    if ([SPDCommonTool currentVersionIsArbic]) {
        self.nameLabelCenterX.constant = -23.5;
    }
}

@end
