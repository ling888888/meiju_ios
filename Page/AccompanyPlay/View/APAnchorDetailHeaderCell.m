//
//  APAnchorDetailHeaderCell.m
//  SimpleDate
//
//  Created by 李楠 on 2019/8/31.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APAnchorDetailHeaderCell.h"

@interface APAnchorDetailHeaderCell ()

@property (weak, nonatomic) IBOutlet UILabel *incomeTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *incomeBgView;
@property (weak, nonatomic) IBOutlet UILabel *lastWeekLabel;
@property (weak, nonatomic) IBOutlet UILabel *thisWeekLabel;

@end

@implementation APAnchorDetailHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.incomeTitleLabel.text = SPDStringWithKey(@"接单收入", nil);
    self.lastWeekLabel.text = SPDStringWithKey(@"上周", nil);
    self.thisWeekLabel.text = SPDStringWithKey(@"本周", nil);
    
    self.incomeBgView.layer.shadowColor = [UIColor colorWithRed:4 / 255.0 green:121 / 255.0 blue:92 / 255.0 alpha:0.1].CGColor;
    self.incomeBgView.layer.shadowOffset = CGSizeMake(0, 2);
    self.incomeBgView.layer.shadowOpacity = 1;
    self.incomeBgView.layer.shadowRadius = 6;
}


- (IBAction)clickIncomeButton:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(headerCell:didClickIncomeButtonWithWeek:)]) {
        [self.delegate headerCell:self didClickIncomeButtonWithWeek:sender.tag ? @"this" : @"last"];
    }
}

@end
