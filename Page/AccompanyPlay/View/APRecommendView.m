//
//  APRecommendView.m
//  SimpleDate
//
//  Created by 李楠 on 2019/8/29.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APRecommendView.h"

@interface APRecommendView ()

@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end

@implementation APRecommendView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.textLabel.text = SPDStringWithKey(@"推荐", nil);
}

@end
