//
//  APOrderRecordEmptyView.h
//  SimpleDate
//
//  Created by 李楠 on 2019/9/9.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface APOrderRecordEmptyView : UIView

@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end

NS_ASSUME_NONNULL_END
