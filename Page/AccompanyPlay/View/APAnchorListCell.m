//
//  APAnchorListCell.m
//  SimpleDate
//
//  Created by 李楠 on 2019/9/25.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APAnchorListCell.h"
#import "APAnchorModel.h"

@interface APAnchorListCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *genderAgeButton;
@property (weak, nonatomic) IBOutlet UIButton *levelButton;
@property (weak, nonatomic) IBOutlet UILabel *incomeTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *incomeLabel;

@end

@implementation APAnchorListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.incomeTitleLabel.text = SPDStringWithKey(@"收入", nil);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(APAnchorModel *)model {
    _model = model;
    
    [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _model.avatar]]];
    self.nameLabel.text = _model.name;
    if ([_model.gender isEqualToString:@"female"]) {
        [self.genderAgeButton setBackgroundColor:[UIColor colorWithHexString:@"ff7eca"]];
        [self.genderAgeButton setImage:[UIImage imageNamed:@"chatroomuser_female"] forState:UIControlStateNormal];
    } else if ([_model.gender isEqualToString:@"male"]) {
        [self.genderAgeButton setBackgroundColor:[UIColor colorWithHexString:@"5db4fd"]];
        [self.genderAgeButton setImage:[UIImage imageNamed:@"chatroomuser_male"] forState:UIControlStateNormal];
    }
    [self.genderAgeButton setTitle:[NSString stringWithFormat:@"%@", _model.age] forState:UIControlStateNormal];
    [SPDCommonTool configDataForLevelBtnWithLevel:[_model.level intValue] andUIButton:self.levelButton];
    self.incomeLabel.text = [NSString stringWithFormat:@"$%@", _model.earning];
}

@end
