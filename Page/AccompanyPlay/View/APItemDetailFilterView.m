//
//  APItemDetailFilterView.m
//  SimpleDate
//
//  Created by 李楠 on 2019/8/29.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APItemDetailFilterView.h"
#import "APItemDetailFilterCell.h"

@interface APItemDetailFilterView ()<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *onlineButton;
@property (weak, nonatomic) IBOutlet UIButton *genderButton;
@property (weak, nonatomic) IBOutlet UIButton *priceButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;

@property (nonatomic, strong) NSArray *onlineStrings;
@property (nonatomic, strong) NSArray *onlineValues;

@property (nonatomic, strong) NSArray *genderStrings;
@property (nonatomic, strong) NSArray *genderValues;

@property (nonatomic, strong) NSArray *priceStrings;
@property (nonatomic, strong) NSArray *priceValues;

@property (nonatomic, strong) NSArray *optionStrings;
@property (nonatomic, strong) NSString *optionKey;
@property (nonatomic, strong) NSArray *optionValues;

@property (nonatomic, strong) UIButton *selectedButton;
@property (nonatomic, strong) UIView *backgroundView;


@end

@implementation APItemDetailFilterView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.onlineStrings = @[SPDStringWithKey(@"全部", nil),
                           SPDStringWithKey(@"在线", nil)];
    self.onlineValues = @[@"0", @"1"];
    
    self.genderStrings = @[SPDStringWithKey(@"不限性别", nil),
                           SPDStringWithKey(@"帅哥", nil),
                           SPDStringWithKey(@"美女", nil)];
    self.genderValues = @[@"all", @"male", @"female"];
    
    self.priceStrings = @[SPDStringWithKey(@"全部价格", nil), @"1000-5000", @"5001-10000", @"10000+"];
    self.priceValues = @[@"all", @"1000,5000", @"5001,10000", @"10000"];
    
    
    self.optionParams = [@{@"online": self.onlineValues.firstObject,
                           @"gender": self.genderValues.firstObject,
                           @"price": self.priceValues.firstObject
                           } mutableCopy];
    
    [self.onlineButton setTitle:self.onlineStrings.firstObject forState:UIControlStateNormal];
    [self.genderButton setTitle:self.genderStrings.firstObject forState:UIControlStateNormal];
    [self.priceButton setTitle:self.priceStrings.firstObject forState:UIControlStateNormal];
    if ([SPDCommonTool currentVersionIsArbic]) {
        [self.onlineButton setImageEdgeInsets:UIEdgeInsetsMake(0, 6, 0, 0)];
        [self.onlineButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 6)];
        [self.genderButton setImageEdgeInsets:UIEdgeInsetsMake(0, 6, 0, 0)];
        [self.genderButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 6)];
        [self.priceButton setImageEdgeInsets:UIEdgeInsetsMake(0, 6, 0, 0)];
        [self.priceButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 6)];
    }
    [self.tableView registerNib:[UINib nibWithNibName:@"APItemDetailFilterCell" bundle:nil] forCellReuseIdentifier:@"APItemDetailFilterCell"];
}

#pragma mark - Event responses

- (IBAction)clickOnlineButton:(UIButton *)sender {
    self.optionStrings = self.onlineStrings;
    self.optionKey = @"online";
    self.optionValues = self.onlineValues;
    [self openOptionsWithButton:sender];
}

- (IBAction)clickGenderButton:(UIButton *)sender {
    self.optionStrings = self.genderStrings;
    self.optionKey = @"gender";
    self.optionValues = self.genderValues;
    [self openOptionsWithButton:sender];
}

- (IBAction)clickPriceButton:(UIButton *)sender {
    self.optionStrings = self.priceStrings;
    self.optionKey = @"price";
    self.optionValues = self.priceValues;
    [self openOptionsWithButton:sender];
}

#pragma mark - Private methods

- (void)openOptionsWithButton:(UIButton *)sender {
    [self.superview insertSubview:self.backgroundView belowSubview:self];
    
    if (self.selectedButton) {
        self.selectedButton.selected = NO;
    }
    sender.selected = YES;
    self.selectedButton = sender;
    [self.tableView reloadData];
    
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
        
        self.tableViewHeight.constant = self.optionStrings.count * 48;
        [self layoutIfNeeded];
        
        CGRect selfFrame = self.frame;
        selfFrame.size.height = (self.optionStrings.count + 1) * 48;
        self.frame = selfFrame;
    }];
}

- (void)closeOptions {
    self.selectedButton.selected = NO;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundView.backgroundColor = [UIColor clearColor];
        
        CGRect selfFrame = self.frame;
        selfFrame.size.height = 40;
        self.frame = selfFrame;
        
        self.tableViewHeight.constant = 15;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self.backgroundView removeFromSuperview];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.optionStrings.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    APItemDetailFilterCell *cell = [tableView dequeueReusableCellWithIdentifier:@"APItemDetailFilterCell" forIndexPath:indexPath];
    cell.optionLabel.text = self.optionStrings[indexPath.row];
    if ([self.optionStrings[indexPath.row] isEqualToString:self.selectedButton.titleLabel.text]) {
        [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath; {
    [self.selectedButton setTitle:self.optionStrings[indexPath.row] forState:UIControlStateNormal];
    [self closeOptions];

    NSString *newValue = self.optionValues[indexPath.row];
    if (![newValue isEqualToString:self.optionParams[self.optionKey]]) {
        [self.optionParams setObject:newValue forKey:self.optionKey];
        if ([self.delegate respondsToSelector:@selector(optionParamsDidChange)]) {
            [self.delegate optionParamsDidChange];
        }
    }
}

#pragma mark - Setters & getters

- (UIView *)backgroundView {
    if (!_backgroundView) {
        _backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH)];
        _backgroundView.backgroundColor = [UIColor clearColor];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeOptions)];
        [_backgroundView addGestureRecognizer:tap];
    }
    return _backgroundView;
}

@end
