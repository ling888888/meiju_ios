//
//  APOrderDetailOptionCell.m
//  SimpleDate
//
//  Created by 李楠 on 2019/9/17.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APOrderDetailOptionCell.h"

@implementation APOrderDetailOptionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.arrowImageView.image = [UIImage imageNamed:@"ap_detail_arrow"].adaptiveRtl;
    self.beyondTimeLimitLabel.text = SPDStringWithKey(@"该时间不在接单时间范围内，建议先沟通", nil);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
