//
//  APReceivedOrderView.m
//  SimpleDate
//
//  Created by 李楠 on 2019/9/24.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APReceivedOrderView.h"
#import "APPlaceOrderMessage.h"

@interface APReceivedOrderView ()

@property (weak, nonatomic) IBOutlet UILabel *itemNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *incomeLabel;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UILabel *remarkLabel;
@property (weak, nonatomic) IBOutlet UIButton *refuseButton;
@property (weak, nonatomic) IBOutlet UIButton *acceptButton;

@property (nonatomic, strong) APPlaceOrderMessage *message;
@property (nonatomic, strong) NSTimer *timer;

@end

@implementation APReceivedOrderView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.hidden = YES;
    self.layer.shadowColor = [UIColor colorWithRed:0 / 255.0 green:0 / 255.0 blue:0 / 255.0 alpha:0.1].CGColor;
    self.layer.shadowOffset = CGSizeMake(0, 3);
    self.layer.shadowOpacity = 1;
    self.layer.shadowRadius = 6;
    
    [self.refuseButton setTitle:SPDStringWithKey(@"拒绝", nil) forState:UIControlStateNormal];
}

#pragma mark - Public methods

- (void)showWithMessage:(APPlaceOrderMessage *)message {
    if (self.hidden) {
        self.message = message;
        
        self.hidden = NO;
        [self.superview bringSubviewToFront:self];
        [UIView animateWithDuration:0.25 animations:^{
            CGRect frame = self.frame;
            frame.origin.y = StatusBarHeight + 5;
            self.frame = frame;
        } completion:^(BOOL finished) {
            [self performSelector:@selector(hideWithCompletion:) withObject:nil afterDelay:self.message.countDown inModes:@[NSRunLoopCommonModes]];
        }];
    } else {
        [self hideWithCompletion:^(BOOL finished) {
            [self showWithMessage:message];
        }];
    }
}

#pragma mark - Private methods

- (void)hideWithCompletion:(void (^ __nullable)(BOOL finished))completion {
    if (!self.hidden) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self];
        [self releaseTimer];
        
        [UIView animateWithDuration:0.25 animations:^{
            CGRect frame = self.frame;
            frame.origin.y = -frame.size.height;
            self.frame = frame;
        } completion:^(BOOL finished) {
            self.hidden = YES;
            if (completion) {
                completion(finished);
            }
        }];
    }
}

- (void)initTimer {
    if (!self.timer) {
        self.timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(countdown) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
        [self.timer fire];
    }
}

- (void)countdown {
    NSString *countDown = self.message.countDown > 0 ? [NSString stringWithFormat:@"%02ld:%02ld", self.message.countDown / 60, self.message.countDown % 60] : @"00:00";
    [self.acceptButton setTitle:[NSString stringWithFormat:@"%@(%@)", SPDStringWithKey(@"接单", nil), countDown] forState:UIControlStateNormal];
    self.message.countDown--;
}

- (void)releaseTimer {
    [self.timer invalidate];
    self.timer = nil;
}

- (void)requestOrderActionWithURL:(NSString *)url {
    [self hideWithCompletion:nil];
    
    NSDictionary *params = @{@"orderId": self.message.orderId};
    [RequestUtils commonPostRequestUtils:[params mutableCopy] bURL:url bAnimated:YES bAnimatedViewController:[UIViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        
    } bFailure:^(id  _Nullable failure) {
        [SPDCommonTool showWindowToast:failure[@"msg"]];
    }];
}


#pragma mark - Event response

- (IBAction)clickRefuseButton:(UIButton *)sender {
    [SPDCommonTool presentAlertWithTitle:@"确定拒绝接单吗？" message:nil cancelTitle:@"取消" cancelHandler:nil actionTitle:@"确定" actionHandler:^(UIAlertAction * _Nonnull action) {
        [self requestOrderActionWithURL:@"ap.order.refuse"];
    }];
}

- (IBAction)clickAcceptButton:(UIButton *)sender {
    [SPDCommonTool presentAlertWithTitle:@"确定接单吗？" message:nil cancelTitle:@"取消" cancelHandler:nil actionTitle:@"确定" actionHandler:^(UIAlertAction * _Nonnull action) {
        [self requestOrderActionWithURL:@"ap.order.receive"];
    }];
}

#pragma mark - Setters & Getters

- (void)setMessage:(APPlaceOrderMessage *)message {
    _message = message;
    
    self.itemNameLabel.text = _message.gameName;
    NSString *string = [NSString stringWithFormat:@"%@ %@", SPDStringWithKey(@"预计收入", nil), _message.earning];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    [attributedString addAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"FF5151"]} range:[string rangeOfString:_message.earning]];
    self.incomeLabel.attributedText = attributedString;
    self.userNameLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"%@给你下了订单", nil), _message.userName];
    self.timeLabel.text = [NSString stringWithFormat:@"%@: %@", SPDStringWithKey(@"时间", nil), _message.orderTime];
    self.durationLabel.text = [NSString stringWithFormat:@"%@: %@min", SPDStringWithKey(@"时长", nil), _message.unit];
    self.remarkLabel.text = [NSString stringWithFormat:@"%@: %@", SPDStringWithKey(@"备注", nil), _message.remark ? : @""];
    [self initTimer];

    CGFloat height = [self.remarkLabel.text boundingRectWithSize:CGSizeMake(kScreenW - 9 * 2 - 14 * 2, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:13]} context:nil].size.height;
    height = ceil(height) > 80 ? 80 : ceil(height);
    self.frame = CGRectMake(9, -(147 + height), kScreenW - 9 * 2, 147 + height);
}

@end
