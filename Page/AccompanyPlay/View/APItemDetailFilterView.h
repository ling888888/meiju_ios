//
//  APItemDetailFilterView.h
//  SimpleDate
//
//  Created by 李楠 on 2019/8/29.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol APItemDetailFilterViewDelegate <NSObject>

@optional

- (void)optionParamsDidChange;

@end

@interface APItemDetailFilterView : UIView

@property (nonatomic, strong) NSMutableDictionary *optionParams;
@property (nonatomic, weak) id<APItemDetailFilterViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
