//
//  APAnchorDetailItemCell.m
//  SimpleDate
//
//  Created by 李楠 on 2019/8/31.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APAnchorDetailItemCell.h"
#import "APItemModel.h"

@interface APAnchorDetailItemCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet UISwitch *itemSwitch;

@end

@implementation APAnchorDetailItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (IBAction)clickPriceButton:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(itemCell:didClickPriceButton:)]) {
        [self.delegate itemCell:self didClickPriceButton:sender];
    }
}

- (IBAction)switchValueChanged:(UISwitch *)sender {
    if ([self.delegate respondsToSelector:@selector(itemCell:switchValueDidChange:)]) {
        self.stateLabel.text = SPDStringWithKey(sender.on ? @"接单中" : @"未开启", nil);
        [self.delegate itemCell:self switchValueDidChange:sender];
    }
}

- (void)setModel:(APItemModel *)model {
    _model = model;
    
    [self.imageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _model.image]]];
    self.nameLabel.text = _model.name;
    self.stateLabel.text = SPDStringWithKey(_model.isReceive ? @"接单中" : @"未开启", nil);
    [self.priceButton setTitle:[NSString stringWithFormat:@"%@%@/%@min", _model.price, SPDStringWithKey(@"钻", nil), _model.unit] forState:UIControlStateNormal];
    self.itemSwitch.on = _model.isReceive;
}

@end
