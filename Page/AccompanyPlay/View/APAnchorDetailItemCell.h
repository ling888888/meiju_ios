//
//  APAnchorDetailItemCell.h
//  SimpleDate
//
//  Created by 李楠 on 2019/8/31.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class APAnchorDetailItemCell, APItemModel;

@protocol APAnchorDetailItemCellDelegate <NSObject>

@optional

- (void)itemCell:(APAnchorDetailItemCell *)itemCell didClickPriceButton:(UIButton *)sender;
- (void)itemCell:(APAnchorDetailItemCell *)itemCell switchValueDidChange:(UISwitch *)sender;

@end

@interface APAnchorDetailItemCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton *priceButton;
@property (weak, nonatomic) IBOutlet UIView *addView;

@property (nonatomic, strong) APItemModel *model;
@property (nonatomic, weak) id<APAnchorDetailItemCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
