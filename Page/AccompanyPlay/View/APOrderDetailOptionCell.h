//
//  APOrderDetailOptionCell.h
//  SimpleDate
//
//  Created by 李楠 on 2019/9/17.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface APOrderDetailOptionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (weak, nonatomic) IBOutlet UILabel *optionLabel;
@property (weak, nonatomic) IBOutlet UILabel *beyondTimeLimitLabel;

@end

NS_ASSUME_NONNULL_END
