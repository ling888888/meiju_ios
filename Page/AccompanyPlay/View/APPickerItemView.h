//
//  APPickerItemView.h
//  SimpleDate
//
//  Created by 李楠 on 2019/9/18.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface APPickerItemView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLabelCenterX;

@end

NS_ASSUME_NONNULL_END
