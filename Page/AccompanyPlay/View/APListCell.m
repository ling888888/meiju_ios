//
//  APListCell.m
//  SimpleDate
//
//  Created by 李楠 on 2019/8/27.
//  Copyright © 2019 WYB. All rights reserved.
//

#import "APListCell.h"
#import "APListModel.h"

@interface APListCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *genderAgeButton;
@property (weak, nonatomic) IBOutlet UIView *statusView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UIImageView *itemImageView;
@property (weak, nonatomic) IBOutlet UILabel *itemNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@end

@implementation APListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.orderButton setTitle:SPDStringWithKey(@"下单", nil) forState:UIControlStateNormal];
    if ([SPDCommonTool currentVersionIsArbic]) {
        [self.genderAgeButton setImageEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        [self.genderAgeButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 5)];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickOrderButton:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(listCell:didClickOrderButton:)]) {
        [self.delegate listCell:self didClickOrderButton:sender];
    }
}

- (void)setModel:(APListModel *)model {
    _model = model;
    
    [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _model.avatar]]];
    self.nameLabel.text = _model.name;
    self.timeLabel.text = [NSString stringWithFormat:@"%@: %@", SPDStringWithKey(@"接单", nil), _model.receiveOrderTime];
    self.priceLabel.text = [NSString stringWithFormat:@"%@%@/%@min", _model.price, SPDStringWithKey(@"钻", nil), _model.unit];
    
    if (_model.image.notEmpty) {
        self.countLabel.hidden = YES;
        [self.itemImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _model.image]]];
        self.itemNameLabel.text = _model.itemName;
    } else {
        self.itemImageView.hidden = YES;
        self.itemNameLabel.hidden = YES;
        self.countLabel.text = [NSString stringWithFormat:@"%@: %@", SPDStringWithKey(@"接单量", nil), _model.receiveOrderNum];
    }
    
    if (_model.userId.notEmpty) { 
        self.orderButton.hidden = YES;
        
        if ([_model.gender isEqualToString:@"female"]) {
            [self.genderAgeButton setBackgroundColor:[UIColor colorWithHexString:@"ff7eca"]];
            [self.genderAgeButton setImage:[UIImage imageNamed:@"chatroomuser_female"] forState:UIControlStateNormal];
        } else if ([_model.gender isEqualToString:@"male"]) {
            [self.genderAgeButton setBackgroundColor:[UIColor colorWithHexString:@"5db4fd"]];
            [self.genderAgeButton setImage:[UIImage imageNamed:@"chatroomuser_male"] forState:UIControlStateNormal];
        }
        [self.genderAgeButton setTitle:[NSString stringWithFormat:@"%@", _model.age] forState:UIControlStateNormal];
        [SPDCommonTool configDataForLevelBtnWithLevel:[_model.level intValue] andUIButton:self.levelButton];
        self.statusView.backgroundColor = [UIColor colorWithHexString:_model.isOnline ? COMMON_PINK : @"999999"];
        self.statusLabel.text = SPDStringWithKey(_model.isOnline ? @"在线" : @"离线", nil);
    } else {
        self.genderAgeButton.hidden = YES;
        self.levelButton.hidden = YES;
        self.statusView.hidden = YES;
        self.statusLabel.hidden = YES;
    }
}

@end
