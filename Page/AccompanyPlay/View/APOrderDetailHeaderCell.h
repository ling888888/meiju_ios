//
//  APOrderDetailHeaderCell.h
//  SimpleDate
//
//  Created by 李楠 on 2019/9/17.
//  Copyright © 2019 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class APListModel;

@interface APOrderDetailHeaderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLbaelTop;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (nonatomic, strong) APListModel *model;

@end

NS_ASSUME_NONNULL_END
