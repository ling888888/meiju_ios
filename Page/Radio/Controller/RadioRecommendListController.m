//
//  RadioRecommendListController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/2/20.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioRecommendListController.h"
#import "RadioListTableCell.h"
#import "RadioModel.h"
#import "RadioFollowListEmptyView.h"

@interface RadioRecommendListController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray * listArray;
@property (nonatomic, copy) NSString * url;
@end

@implementation RadioRecommendListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.page = 0;
    [self.view addSubview:self.tableView];
    
    if ([_type isEqualToString:@"recommend"]) {
        self.url = @"radio.local.recommend.list";
        self.navigationItem.title = SPDLocalizedString(@"精选推荐");
    }else if ([_type isEqualToString:@"history"]){
        self.url = @"radio.history.list";
        self.navigationItem.title = SPDLocalizedString(@"最近收听");
        UIBarButtonItem * rightBarItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"clear_message"] style:UIBarButtonItemStylePlain target:self action:@selector(clickClearBarButtonItem:)];
        self.navigationItem.rightBarButtonItem = rightBarItem;
    }else if ([_type isEqualToString:@"rank"]){
        self.navigationItem.title = SPDLocalizedString(@"排行榜");
        self.url = @"radio.rank.list";
    }else if ([_type isEqualToString:@"follow"]){
        self.url = @"radio.follow.list";
        self.navigationItem.title = SPDLocalizedString(@"关注台");
    }
    if (![_type isEqualToString:@"history"]) {
        [self requestData];
    }else{
        [self queryRadioHistory];
    }
}



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)clickClearBarButtonItem:(UIBarButtonItem *)sender {
    [self presentAutoController:SPDLocalizedString(@"提示") titleColor:@"1A1A1A" titleFontSize:18 messageString:SPDLocalizedString(@"您确定清空所有播放历史吗？") messageColor:@"1A1A1A" messageFontSize:15 cancelTitle:SPDLocalizedString(@"取消") cancelAction:^{
        //[MobClick event:@"FM1_9_37"];
    } confirmTitle:SPDLocalizedString(@"清空") confirmAction:^{
        [DBUtil deleteRadioHistory];
        [self.listArray removeAllObjects];
        [self.tableView reloadData];
        //[MobClick event:@"FM1_9_38"];
    }];
}

#pragma mark - Request

- (void)requestData {
    [RequestUtils commonGetRequestUtils:[@{@"page":@(self.page)}mutableCopy] bURL:self.url bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        NSArray *list = suceess[@"list"];
        if (self.page == 0) {
            [self.listArray removeAllObjects];
        }
        for (NSDictionary * dict in list) {
            RadioModel * model = [RadioModel initWithDictionary:dict];
            [self.listArray addObject:model];
        }
        if (self.listArray.count == 0 && [self.type isEqualToString:@"follow"]) {
            RadioFollowListEmptyView * view = [[[NSBundle mainBundle] loadNibNamed:@"RadioFollowListEmptyView" owner:self options:nil]lastObject];
            view.frame = CGRectMake(0, NavHeight, kScreenW, kScreenH - NavHeight);
            [self.view addSubview:view];
            return ;
        }
        [self.tableView reloadData];
        self.page++;
        [self.tableView.mj_header endRefreshing];
        if (self.tableView.mj_footer.state == MJRefreshStateRefreshing) {
            [self.tableView.mj_footer endRefreshingWithCompletionBlock:^{
                self.tableView.mj_footer.hidden = !list.count;
            }];
        } else {
            self.tableView.mj_footer.hidden = !list.count;
        }
    } bFailure:^(id  _Nullable failure) {
        [self.tableView.mj_footer endRefreshing];
        [self.tableView.mj_header endRefreshing];
        [self showTips:failure[@"msg"]];
    }];
}

- (void)deleteData:(NSInteger)index {
    RadioModel * model = self.listArray[index];
    NSString * url = [self.type isEqualToString:@"follow"] ? @"radio.follow" : @"radio.history.delete";
    NSMutableDictionary * dict = [@{@"radioId":model.radioId}mutableCopy];
    [dict setValue:@(0) forKey:@"type"];
    [RequestUtils commonPostRequestUtils:dict bURL:url bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        
        [self.listArray removeObjectAtIndex:index];
        [self.tableView reloadData];
        
    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)queryRadioHistory {
    NSMutableArray * historyIds = [DBUtil queryRadioHistory];
    if (historyIds.count <= 0) {
        return;
    }
    NSMutableDictionary * dic = [NSMutableDictionary new];
    [dic setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:historyIds options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding] forKey:@"ids"];
    [RequestUtils commonGetRequestUtils:dic bURL:@"radio.list.id" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        [self.listArray removeAllObjects];
        
        for (NSDictionary * dict in suceess[@"list"]) {
            if ([dic isKindOfClass:[NSDictionary class]]) {
                RadioModel * model = [RadioModel initWithDictionary:dict];
                [self.listArray addObject:model];
            }
        }
        [self.tableView reloadData];
    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RadioListTableCell * cell = [RadioListTableCell cellWithTableView:tableView];
    RadioModel *model = self.listArray[indexPath.row];
    model.index = [self.type isEqualToString:@"rank"] ? indexPath.row + 1 : 0 ;
    cell.radioModel = self.listArray[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 75;
}

- (BOOL)tableView:(UITableView*)tableView canEditRowAtIndexPath:(NSIndexPath*)indexPath{
    return ([self.type isEqualToString:@"follow"] || [self.type isEqualToString:@"history"]);
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
   UITableViewRowAction *deleteRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:[self.type isEqualToString:@"follow"] ? SPDStringWithKey(@"取消关注", nil):SPDStringWithKey(@"删除", nil) handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
       if ([self.type isEqualToString:@"history"]) {
           RadioModel * model = self.listArray[indexPath.row];
           [DBUtil deleteRadioHistory:model.radioId];
           [self.listArray removeObject:model];
           [self.tableView reloadData];
           if (self.listArray.count == 0) {
               self.navigationItem.rightBarButtonItem = nil;
           }
           //[MobClick event:@"FM1_9_36"];
       }else{
           //[MobClick event:@"FM1_9_8"];
           [self deleteData:indexPath.row];
       }
   }];
    return @[deleteRowAction];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    RadioModel * model = self.listArray[indexPath.row];
    [self pushToRadioRoom:model.radioId];
}

#pragma mark - Getters

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        if (![self.type isEqualToString:@"history"]) {
            __weak typeof(self) weakSelf = self;
          _tableView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
                       weakSelf.page = 0;
                       [weakSelf requestData];
                   }];
           _tableView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
               [weakSelf requestData];
           }];
        }
        _tableView.tableFooterView = [UIView new];
    }
    return _tableView;
}

- (NSMutableArray *)listArray {
    if (!_listArray) {
        _listArray = [NSMutableArray new];
    }
    return _listArray;
}

@end
