//
//  RadioTypeListViewController.h
//  SimpleDate
//
//  Created by ling Hou on 2020/2/20.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@class RadioTypeModel;

@interface RadioTypeListViewController : BaseViewController

@property (nonatomic, strong) NSMutableArray * typeListArray; // “新闻台”
@property (nonatomic, assign) NSInteger selectedIndex; // 如果是 新闻 曲艺 这样的分类必须给 typeListArray selectedIndex这两个 如果是国家 就什么参数都不用给

@end

NS_ASSUME_NONNULL_END
