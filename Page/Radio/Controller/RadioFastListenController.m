//
//  RadioFastListenController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/6.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioFastListenController.h"
#import "ZegoKitManager.h"
#import "RadioModel.h"

@interface RadioFastListenController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstant;
@property (weak, nonatomic) IBOutlet UILabel *nextRadioTitle;
@property (weak, nonatomic) IBOutlet UILabel *currentRadioTitle;
@property (weak, nonatomic) IBOutlet UIButton *radioFollowBtn;
@property (nonatomic, copy) NSString * radioTypeId;

@end

@implementation RadioFastListenController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.bottomConstant.constant = 10 + (kScreenW/375)*100 + IphoneX_Bottom;
    self.radioTypeId = _radioTypeModel.radioTypeId;
    self.currentIndex = -1;
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
   
    if (self.dataArray.count == 0) {
         [self requestData]; // 上一个页面没请求成功我划走了
    }else if (self.currentIndex != -1 && !ZegoManager.isBackground) {
         [self playRadio];
    }
    // 为了判断bg模式切换过来不间断播放
    if (ZegoManager.isBackground) {
        ZegoManager.isBackground = NO;
    }
}

- (void)setCurrentIndex:(NSInteger)currentIndex {
    _currentIndex = currentIndex;
    if (_currentIndex >= self.dataArray.count || _currentIndex == -1) {
        return;
    }
    [self playRadio];
}

- (void)playRadio{
    [ZegoManager stopPlayRadio];
    RadioModel * model = self.dataArray[_currentIndex];
    [ZegoManager startPlayRadio:model.radioContent];
    [ZegoManager muteRadio:NO];
    [DBUtil recordRadioHistory:model.radioId];
    self.radioFollowBtn.selected = model.follow;
    self.currentRadioTitle.text = model.radioName;
    if (_currentIndex < self.dataArray.count - 1) {
        RadioModel * nextModel = self.dataArray[_currentIndex+1];
        self.nextRadioTitle.text = [NSString stringWithFormat:SPDStringWithKey(@"下一个：%@", nil),nextModel.radioName];
    }else{
        self.nextRadioTitle.text = @"";
    }
    // 给服务器记录数据
    [self requestRadioJoin:model];
}

- (IBAction)nextRadioButtonClicked:(UIButton *)sender {
    if (_currentIndex == self.dataArray.count - 1) {
        return;
    }
    self.currentIndex++;
    //[MobClick event:@"FM1_9_55"];
}

- (IBAction)handleFollow:(UIButton *)sender {
    sender.userInteractionEnabled = NO;
    if (!sender.selected) {
        sender.selected = !sender.selected;
        [self requestRadioFollowWithType:1 sender:sender];
        //[MobClick event:@"FM1_9_49"];
    } else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:SPDStringWithKey(@"提示", nil) message:SPDStringWithKey(@"确定取消关注吗？", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:SPDStringWithKey(@"返回", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            sender.userInteractionEnabled = YES;
        }];
        [alertController addAction:cancelAction];
        UIAlertAction *action = [UIAlertAction actionWithTitle:SPDStringWithKey(@"取消关注", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            sender.selected = !sender.selected;
            [self requestRadioFollowWithType:0 sender:sender];
            //[MobClick event:@"FM1_9_50"];
        }];
        [alertController addAction:action];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


#pragma mark - Request

- (void)requestData {
    NSLog(@"开始请求数据");
    NSMutableDictionary * dict = [NSMutableDictionary new];
    [dict setValue:self.radioTypeId forKey:@"radioTypeId"];
    [RequestUtils commonGetRequestUtils:dict bURL: @"radio.listener.list" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        NSArray *list = suceess[@"list"];
        [self.dataArray removeAllObjects];
        for (NSDictionary * dic in list) {
            RadioModel * model = [RadioModel initWithDictionary:dic];
            [self.dataArray addObject:model];
        }
        self.currentIndex = 0;

    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)requestRadioFollowWithType:(NSInteger)type sender:(UIButton *)sender {
    RadioModel * model = self.dataArray[_currentIndex];
    NSMutableDictionary *params = [@{@"radioId": model.radioId, @"type": @(type)} mutableCopy];
    [RequestUtils commonPostRequestUtils:params bURL:@"radio.follow" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if (type == 1) {
            [self showTips:SPDLocalizedString(@"关注成功")];
        }
        model.follow = sender.selected;
        sender.userInteractionEnabled = YES;
    } bFailure:^(id  _Nullable failure) {
        [self showToast:failure[@"msg"]];
        sender.selected = !sender.selected;
        sender.userInteractionEnabled = YES;
    }];
}

- (void)requestRadioJoin:(RadioModel *)model {
    [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:@{@"radioId":model.radioId?:@""}] bURL:@"radio.join" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        NSLog(@"radio.joinradio.joinradio.joinradio.joinradio.join");
    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}


- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

@end
