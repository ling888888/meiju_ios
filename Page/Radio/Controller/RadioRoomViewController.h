//
//  RadioRoomViewController.h
//  SimpleDate
//
//  Created by 李楠 on 2020/2/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RadioRoomViewController : BaseViewController

@property (nonatomic, copy) NSString *radioId;

- (void)loginRoom;

@end

NS_ASSUME_NONNULL_END
