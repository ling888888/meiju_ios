//
//  RadioRoomContainerController.m
//  SimpleDate
//
//  Created by 李楠 on 2020/2/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioRoomContainerController.h"
#import "RadioRoomViewController.h"
#import "ZegoKitManager.h"
#import "RadioRoomBackgroundRunView.h"

@interface RadioRoomContainerController ()

@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation RadioRoomContainerController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#384490"];
    [self requestRadioRoomInfoList];
    [ZegoManager.radioBackgroundRunView removeFromSuperview];
    ZegoManager.popIndex = self.navigationController.viewControllers.count;
    
    self.menuView.hidden = YES;
    self.cachePolicy = WMPageControllerCachePolicyDisabled;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}

#pragma mark - Private methods

- (void)requestRadioRoomInfoList {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionary] bURL:@"radio.room.info.list" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.dataArray = [suceess[@"list"] mutableCopy];
        NSInteger index = (int)[self.dataArray indexOfObject:self.radioId];
        if (index == NSNotFound || index < 0) {
            [self.dataArray insertObject:self.radioId atIndex:0];
            index = 0;
        }
        self.selectIndex = (int)(index + self.dataArray.count);
        [self reloadData];
        self.menuView.hidden = YES;
    } bFailure:^(id  _Nullable failure) {
        [self.navigationController popToViewController:self.navigationController.viewControllers[ZegoManager.popIndex - 2] animated:NO];
        ZegoManager.popIndex = 0;
        [ZegoManager leaveRoom];
    }];
}

#pragma mark - WMPageControllerDataSource

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.dataArray.count * 3;
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    RadioRoomViewController *vc = [RadioRoomViewController new];
    vc.radioId = self.dataArray[index % self.dataArray.count];
    return vc;
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    return self.view.bounds;
}

#pragma mark - WMPageControllerDelegate

- (void)pageController:(WMPageController *)pageController didEnterViewController:(nonnull __kindof UIViewController *)viewController withInfo:(nonnull NSDictionary *)info {
    [(RadioRoomViewController *)viewController loginRoom];
}

#pragma mark - Setters & Getters

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

@end
