//
//  RadioRoomViewController.m
//  SimpleDate
//
//  Created by 李楠 on 2020/2/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioRoomViewController.h"
#import "ZegoKitManager.h"
#import "RadioModel.h"
#import "RadioRoomRadioInfoView.h"
#import "SPDVoiceLiveUserModel.h"
#import "ChatroomUserCell.h"
#import "RadioRoomOnlineUserView.h"
#import "RadioRoomUserInfoView.h"
#import "HomeModel.h"
#import "KKGiftGivingView.h"
#import "RadioRoomGiftAnimationView.h"
#import "SPDChatRoomSendPresentMessage.h"
#import "RCDLiveInputBar.h"
#import "SPDTextMessage.h"
#import "RadioRoomTextMessageCell.h"
#import "RadioRoomFollowView.h"
#import "RadioRoomFollowCell.h"
#import "ZTWAInfoShareView.h"
#import "SPDUMShareUtils.h"
#import "RadioRoomSlideGuideView.h"

@interface RadioRoomViewController ()<ZegoAudioRoomDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, RadioRoomRadioInfoViewDelegate, RadioRoomOnlineUserViewDelegate, RadioRoomUserInfoViewDelegate, KKGiftGivingViewDelegate, RCTKInputBarControlDelegate, RadioRoomTextMessageCellDelegate, RadioRoomFollowViewDelegate, RadioRoomFollowCellDelegate, ZTWAInfoShareViewDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIView *radioView;
@property (weak, nonatomic) IBOutlet UIButton *radioAvatarButton;
@property (weak, nonatomic) IBOutlet UILabel *radioNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *audioWaveImageView;
@property (weak, nonatomic) IBOutlet UIButton *muteButton;
@property (weak, nonatomic) IBOutlet UIButton *followButton;
@property (weak, nonatomic) IBOutlet UIImageView *recommendAvatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *recomendContentLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *onlineUserCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *onlineNumberButton;
@property (weak, nonatomic) IBOutlet UICollectionView *messageCollectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;

@property (nonatomic, assign) NSTimeInterval joinTimeStamp;
@property (nonatomic, strong) RadioModel *radioInfo;
@property (nonatomic, assign) BOOL playAfterRequest;
@property (nonatomic, strong) NSMutableDictionary *onlineUserDic;
@property (nonatomic, assign) NSInteger onlineNumber;
@property (nonatomic, strong) NSArray<SPDVoiceLiveUserModel *> *sortedOnlineUserArr;
@property (nonatomic, strong) NSMutableArray<RCMessageContent *> *messageArr;
@property (nonatomic, strong) NSMutableArray<NSValue *> *cellSizeArr;
@property (nonatomic, strong) RCCommandMessage *insertFollowCellMessage;

@property (nonatomic, strong) RadioRoomGiftAnimationView *giftAnimationView;
@property (nonatomic, strong) RCDLiveInputBar *inputBar;
@property (nonatomic, strong) UIView *inputBackgroundView;

@end

@implementation RadioRoomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self requestRadioRoomInfoWithRadioId:self.radioId];
    [self configureSubviews];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

#pragma mark - Public methods

- (void)loginRoom {
    if ([self.radioId isEqualToString:ZegoManager.clan_id]) {
        if (!ZegoManager.isBackground) {
            return;
        }
    } else {
        [ZegoManager leaveRoom];
        [self performSelector:@selector(insertFollowCell) withObject:nil afterDelay:1 * 60 inModes:@[NSRunLoopCommonModes]];
        [self performSelector:@selector(showFollowView) withObject:nil afterDelay:3 * 60 inModes:@[NSRunLoopCommonModes]];
    }
    
    ZegoManager.sceneType = SceneTypeRadioRoom;
    ZegoManager.isBackground = NO;
    ZegoManager.clan_id = self.radioId;
    if (self.radioInfo) {
        ZegoManager.clan_cover = self.radioInfo.radioImg;
        [ZegoManager startPlayRadio:self.radioInfo.radioContent];
    } else {
        self.playAfterRequest = YES;
    }
    [ZegoManager muteRadio:!ZegoManager.speakerEnabled];
    self.muteButton.selected = !ZegoManager.speakerEnabled;
    
    [ZegoManager.api setUserStateUpdate:YES];
    [ZegoManager.api setAudioRoomDelegate:self];
    [ZegoAudioRoomApi setUserID:[SPDApiUser currentUser].userId userName:[SPDApiUser currentUser].nickName];
    [ZegoManager.api loginRoom:self.radioId completionBlock:^(int errorCode) {
        if (errorCode == 0) {
            [self requestChatroomUsersWithUsers:@[] updateAll:YES];
        } else {
            [self showToast:SPDStringWithKey(@"加入聊天室失败，请稍后重试", nil)];
            [self exit];
        }
    }];

    
    [[RCIMClient sharedRCIMClient] joinChatRoom:self.radioId messageCount:CHATROOM_MESSAGECOUNT success:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveChatroomMessage:) name:SPDLiveKitChatRoomMessageNotification object:nil];
            self.joinTimeStamp = [[NSDate date] timeIntervalSince1970];
        });
    } error:^(RCErrorCode status) {
        if (status == KICKED_FROM_CHATROOM) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showToast:SPDStringWithKey(@"你已被踢出房间，暂时无法进入此房间", nil)];
            });
        }
    }];
    
    // 历史记录
    [DBUtil recordRadioHistory:self.radioId];
}

#pragma mark - Event rsponses

- (IBAction)clickBackButton:(UIButton *)sender {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"HadShowedRadioRoomSlideGuide"]) {
        [self showSlideGuide];
    } else {
        [self keep];
    }
}

- (IBAction)clickShareButton:(UIButton *)sender {
    ZTWAInfoShareView *view = [ZTWAInfoShareView shareView];
    view.delegate = self;
    [view show];
}

- (IBAction)clickRadioAvatarButton:(UIButton *)sender {
    if (self.radioInfo) {
        RadioRoomRadioInfoView *view = [[NSBundle mainBundle] loadNibNamed:@"RadioRoomRadioInfoView" owner:self options:nil].firstObject;
        view.frame = self.view.bounds;
        view.delegate = self;
        [self.view addSubview:view];
        [view showWithModel:self.radioInfo];
    }
}

- (IBAction)clickExitButton:(UIButton *)sender {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"HadShowedRadioRoomSlideGuide"]) {
        [self showSlideGuide];
    } else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:SPDStringWithKey(@"提示", nil) message:SPDStringWithKey(@"确定退出房间不再收听正在播放的语音？", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:SPDStringWithKey(@"退出", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self exit];
        }];
        [alertController addAction:cancelAction];
        UIAlertAction *action = [UIAlertAction actionWithTitle:SPDStringWithKey(@"最小化", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self keep];
        }];
        [alertController addAction:action];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (IBAction)clickMuteButton:(UIButton *)sender {
    sender.selected = !sender.selected;
    [ZegoManager muteRadio:sender.selected];
}

- (IBAction)clickFollowButton:(UIButton *)sender {
    if (sender.userInteractionEnabled) {
        sender.userInteractionEnabled = NO;
        if (!sender.selected) {
            sender.selected = !sender.selected;
            [self requestRadioFollowWithType:1 sender:sender];
        } else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:SPDStringWithKey(@"提示", nil) message:SPDStringWithKey(@"确定取消关注吗？", nil) preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:SPDStringWithKey(@"返回", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                sender.userInteractionEnabled = YES;
            }];
            [alertController addAction:cancelAction];
            UIAlertAction *action = [UIAlertAction actionWithTitle:SPDStringWithKey(@"取消关注", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                sender.selected = !sender.selected;
                [self requestRadioFollowWithType:0 sender:sender];
            }];
            [alertController addAction:action];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}

- (IBAction)clickOnlineNumberButton:(UIButton *)sender {
    if (self.sortedOnlineUserArr.count) {
        RadioRoomOnlineUserView *view = [[NSBundle mainBundle] loadNibNamed:@"RadioRoomOnlineUserView" owner:self options:nil].firstObject;
        view.frame = self.view.bounds;
        view.delegate = self;
        [self.view addSubview:view];
        [view showWithDataArray:self.sortedOnlineUserArr];
    }
}

- (IBAction)tapInputView:(UITapGestureRecognizer *)sender {
    [self.inputBar setInputBarStatus:RCDLiveBottomBarKeyboardStatus];
    self.inputBackgroundView.hidden = NO;
}

- (void)tapInputBackgroundView:(UITapGestureRecognizer *)sender {
    [self.inputBar setInputBarStatus:RCDLiveBottomBarDefaultStatus];
    self.inputBackgroundView.hidden = YES;
}

#pragma mark - Private methods

- (void)configureSubviews {
    self.topConstraint.constant = StatusBarHeight;
    self.bottomConstraint.constant = 12 + IphoneX_Bottom;
    
    self.radioView.layer.shadowColor = [UIColor colorWithWhite:0 alpha:0.2].CGColor;
    self.radioView.layer.shadowOffset = CGSizeMake(0, 5);
    self.radioView.layer.shadowOpacity = 1;
    self.radioView.layer.shadowRadius = 7;
    self.radioView.layer.cornerRadius = 9;
    
    NSMutableArray *animationImages = [NSMutableArray array];
    for (NSInteger i = 0; i < 8; i++) {
        [animationImages addObject:[UIImage imageNamed:[NSString stringWithFormat:@"audio_wave_%zd", i]]];
    }
    self.audioWaveImageView.animationImages = animationImages;
    self.audioWaveImageView.animationDuration = animationImages.count * 0.1;
        
    [self.onlineUserCollectionView registerNib:[UINib nibWithNibName:@"ChatroomUserCell" bundle:nil] forCellWithReuseIdentifier:@"ChatroomUserCell"];
    [self.messageCollectionView registerNib:[UINib nibWithNibName:@"RadioRoomTextMessageCell" bundle:nil] forCellWithReuseIdentifier:@"RadioRoomTextMessageCell"];
    [self.messageCollectionView registerNib:[UINib nibWithNibName:@"RadioRoomFollowCell" bundle:nil] forCellWithReuseIdentifier:@"RadioRoomFollowCell"];
}

- (void)requestRadioRoomInfoWithRadioId:(NSString *)radioId {
    NSMutableDictionary *params = [@{@"radioId": self.radioId} mutableCopy];
    [RequestUtils commonGetRequestUtils:params bURL:@"radio.room.info" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.radioInfo = [RadioModel initWithDictionary:suceess];
    } bFailure:^(id  _Nullable failure) {

    }];
}

- (void)requestChatroomUsersWithUsers:(NSArray *)users updateAll:(BOOL)updateAll {
    NSMutableArray *userIdArr = [NSMutableArray array];
    for (ZegoUserState *user in users) {
        [userIdArr addObject:user.userID];
    }
    if (updateAll) {
        [userIdArr addObject:[SPDApiUser currentUser].userId];
    }
    NSString *userIds = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:userIdArr options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *params = [@{@"clan_id": self.radioId, @"user_ids": userIds} mutableCopy];
    [RequestUtils commonGetRequestUtils:params bURL:@"chatroom.users" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        if (updateAll) {
            [self.onlineUserDic removeAllObjects];
        }
        for (NSDictionary *dic in suceess[@"users"]) {
            SPDVoiceLiveUserModel *model = [SPDVoiceLiveUserModel initWithDictionary:dic];
            [self.onlineUserDic setObject:model forKey:model._id];
        }
        self.onlineNumber = self.onlineUserDic.count;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.onlineUserCollectionView reloadData];
        });
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestRadioFollowWithType:(NSInteger)type sender:(UIButton *)sender {
    NSMutableDictionary *params = [@{@"radioId": self.radioId, @"type": @(type)} mutableCopy];
    [RequestUtils commonPostRequestUtils:params bURL:@"radio.follow" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.radioInfo.follow = sender.selected;
        if (self.radioInfo.follow) {
            [self deleteFollowCell];
            self.radioInfo.followNum = @(self.radioInfo.followNum.integerValue + 1);
        } else {
            self.radioInfo.followNum = @(self.radioInfo.followNum.integerValue - 1);
        }
        sender.userInteractionEnabled = YES;
    } bFailure:^(id  _Nullable failure) {
        [self showToast:failure[@"msg"]];
        sender.selected = !sender.selected;
        sender.userInteractionEnabled = YES;
    }];
}

- (void)keep {
    [self back];
    ZegoManager.isBackground = YES;
}

- (void)exit {
    [self back];
    [ZegoManager leaveRoom];
}

- (void)back {
    if (ZegoManager.popIndex > 0) {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        [self.navigationController popToViewController:self.navigationController.viewControllers[ZegoManager.popIndex - 2] animated:NO];
        ZegoManager.popIndex = 0;
    }
}

- (void)showUserInfoViewWithUserId:(NSString *)userId {
    RadioRoomUserInfoView *view = [[NSBundle mainBundle] loadNibNamed:@"RadioRoomUserInfoView" owner:self options:nil].firstObject;
    view.frame = self.view.bounds;
    view.delegate = self;
    [self.view addSubview:view];
    [view showWithUserId:userId];
}

- (void)showFollowView {
    if (!self.radioInfo.follow) {
        RadioRoomFollowView *view = [[NSBundle mainBundle] loadNibNamed:@"RadioRoomFollowView" owner:self options:nil].firstObject;
        view.frame = self.view.bounds;
        view.delegate = self;
        [self.view addSubview:view];
        [view showWithModel:self.radioInfo];
    }
}

- (void)insertFollowCell {
    if (!self.radioInfo.follow) {
        self.insertFollowCellMessage = [RCCommandMessage messageWithName:@"insertFollowCell" data:@""];
        [self insertMessage:self.insertFollowCellMessage];
    }
}

- (void)deleteFollowCell {
    if (self.insertFollowCellMessage) {
        NSInteger index = [self.messageArr indexOfObject:self.insertFollowCellMessage];
        if (index != NSNotFound && index >= 0) {
            [self.messageArr removeObjectAtIndex:index];
            [self.messageCollectionView deleteItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:index inSection:0]]];
        }
        self.insertFollowCellMessage = nil;
    }
}

- (void)showSlideGuide {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HadShowedRadioRoomSlideGuide"];
    RadioRoomSlideGuideView *view = [[NSBundle mainBundle] loadNibNamed:@"RadioRoomSlideGuideView" owner:self options:nil].firstObject;
    view.frame = self.view.bounds;
    [self.view addSubview:view];
}

#pragma mark - Message methods

- (void)didReceiveChatroomMessage:(NSNotification *)notification {
    RCMessage *message = notification.object;
    if ([message.targetId isEqualToString:self.radioId]) {
        if ([message.content isKindOfClass:[SPDTextMessage class]]) {
            [self insertMessage:message.content];
        } else if ([message.content isKindOfClass:[SPDChatRoomSendPresentMessage class]]) {
            if ([[NSDate date] timeIntervalSince1970] - self.joinTimeStamp > 2.5) {
                [self.giftAnimationView showAnimationWithMessage:message.content];
            }
        }
    }
}

- (void)sendMessage:(RCMessageContent *)message {
    message.senderUserInfo = [RCIM sharedRCIM].currentUserInfo;
    [[RCIMClient sharedRCIMClient] sendMessage:ConversationType_CHATROOM targetId:self.radioId content:message pushContent:nil pushData:nil success:^(long messageId) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([message isKindOfClass:[SPDTextMessage class]]) {
                [self insertMessage:message];
            }
        });
    } error:^(RCErrorCode nErrorCode, long messageId) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (nErrorCode == 23408) {
                [self showToast:SPDStringWithKey(@"您已经被禁言了哦!", nil)];
            } else if (nErrorCode == 23406) {
                [self showToast:@"Not In Chatroom"];
            }
        });
    }];
}

- (void)insertMessage:(RCMessageContent *)message {
    if ([message isKindOfClass:[SPDTextMessage class]]) {
        SPDTextMessage *textMessage = (SPDTextMessage *)message;
        NSString *text = textMessage.content;
        UIFont *font = [UIFont systemFontOfSize:13];
        CGFloat height = [text boundingRectWithSize:CGSizeMake(kScreenW - 62 - 8 - 8 - 94, CGFLOAT_MAX)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSFontAttributeName: font}
                                            context:nil].size.height;
        CGSize cellSize = CGSizeMake(kScreenW, ceilf(height) + 25 + 8 + 8 + 7);
        [self.cellSizeArr addObject:[NSValue valueWithCGSize:cellSize]];
    } else if ([message isKindOfClass:[RCCommandMessage class]]) {
        RCCommandMessage *commandMessage = (RCCommandMessage *)message;
        if ([commandMessage.name isEqualToString:@"insertFollowCell"]) {
            CGSize cellSize = CGSizeMake(kScreenW, 55);
            [self.cellSizeArr addObject:[NSValue valueWithCGSize:cellSize]];
        } else {
            return;
        }
    } else {
        return;
    }
    
    if (self.messageArr.count >= 100) {
        [self.messageArr removeObjectAtIndex:0];
        [self.messageCollectionView deleteItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:0]]];
    }
    [self.messageArr addObject:message];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:self.messageArr.count - 1 inSection:0];
    [self.messageCollectionView insertItemsAtIndexPaths:@[indexPath]];
    [self.messageCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition: UICollectionViewScrollPositionBottom animated:YES];
}

#pragma mark - ZegoAudioRoomDelegate

- (void)onKickOut:(int)reason roomID:(NSString *)roomID customReason:(NSString *)customReason {
    if ([roomID isEqualToString:self.radioId]) {
        [self showToast:customReason];
    }
}

- (void)onDisconnect:(int)errorCode roomID:(NSString *)roomID {
    if ([roomID isEqualToString:self.radioId]) {
        [self showToast:SPDStringWithKey(@"网络连接已断开,请重新进入", nil)];
        [self exit];
    }
}

- (void)onUserUpdate:(NSArray<ZegoUserState *> *)userList updateType:(ZegoUserUpdateType)type {
    switch (type) {
        case ZEGO_UPDATE_TOTAL:
            [self requestChatroomUsersWithUsers:userList updateAll:YES];
            break;
        case ZEGO_UPDATE_INCREASE: {
            NSMutableArray *joinedUsers = [NSMutableArray array];
            NSMutableArray *leftUsers = [NSMutableArray array];
            for (ZegoUserState *user in userList) {
                switch (user.updateFlag) {
                    case ZEGO_USER_ADD:
                        [joinedUsers addObject:user];
                        break;
                    case ZEGO_USER_DELETE:
                        [leftUsers addObject:user];
                        break;
                    default:
                        break;
                }
            }
            if (joinedUsers.count) {
                [self requestChatroomUsersWithUsers:joinedUsers updateAll:NO];
            }
            if (leftUsers.count) {
                for (ZegoUserState *user in leftUsers) {
                    [self.onlineUserDic removeObjectForKey:user.userID];
                }
                self.onlineNumber = self.onlineUserDic.count;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.onlineUserCollectionView reloadData];
                });
            }
            break;
        }
        default:
            break;
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == self.onlineUserCollectionView) {
        return self.sortedOnlineUserArr.count;
    }  else {
        return self.messageArr.count;
    }
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.onlineUserCollectionView) {
        ChatroomUserCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatroomUserCell" forIndexPath:indexPath];
        cell.model = self.sortedOnlineUserArr[indexPath.row];
        cell.avatarImageView.layer.cornerRadius = 13;
        return cell;
    } else {
        RCMessageContent *message = self.messageArr[indexPath.row];
        if ([message isKindOfClass:[SPDTextMessage class]]) {
            RadioRoomTextMessageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RadioRoomTextMessageCell" forIndexPath:indexPath];
            cell.message = (SPDTextMessage *)message;
            cell.delegate = self;
            return cell;
        } else {
            RadioRoomFollowCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RadioRoomFollowCell" forIndexPath:indexPath];
            cell.delegate = self;
            return cell;
        }
    }
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.onlineUserCollectionView) {
        [self showUserInfoViewWithUserId: self.sortedOnlineUserArr[indexPath.row]._id];
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.onlineUserCollectionView) {
        return CGSizeMake(31, 31);
    } else {
        return self.cellSizeArr[indexPath.row].CGSizeValue;
    }
}

#pragma mark - RadioRoomRadioInfoViewDelegate

- (void)didClickFollowButtonInRadioInfoView {
    [self clickFollowButton:self.followButton];
}

#pragma mark - RadioRoomOnlineUserViewDelegate

- (void)didClickUserInOnlineUserView:(SPDVoiceLiveUserModel *)model {
    [self showUserInfoViewWithUserId:model._id];
}

#pragma mark - RadioRoomUserInfoViewDelegate

- (void)didClickUserInUserInfoView:(HomeModel *)model {
    [self pushToDetailTableViewController:self userId:model._id];
}

- (void)didClickGiftGivingInUserInfoView:(HomeModel *)model {
    KKGiftGivingView *view = [[NSBundle mainBundle] loadNibNamed:@"KKGiftGivingView" owner:self options:nil].firstObject;
    view.frame = self.view.bounds;
    view.receiveGiftUserModel = model;
    view.hideMagic = YES;
    view.delegate = self;
    [self.view addSubview:view];
}

#pragma mark - KKGiftGivingViewDelegate

- (void)didClickRechargeButton {
    [self pushToRechageController];
}

- (void)giftGivingView:(KKGiftGivingView *)view showEffectWithMessage:(RCMessageContent *)message {
    [self.giftAnimationView showAnimationWithMessage:message];
}

#pragma mark - RCTKInputBarControlDelegate

- (void)onTouchSendMessageButton:(NSString *)text enableGlobalMessage:(BOOL)enableGlobalMessage {
    if (text.notEmpty) {
        if (text.length <= 100) {
            SPDTextMessage *textMessage  = [SPDTextMessage new];
            textMessage.content = text;
            textMessage.gender = [SPDApiUser currentUser].gender;
            textMessage.age = [SPDApiUser currentUser].age.stringValue;
            LY_Portrait *portrait = [[LY_Portrait alloc] init];
            portrait.url = [LY_User currentUser].portrait;
            portrait.headwearUrl = [LY_User currentUser].headwearWebp;
            textMessage.portrait = portrait;
            
            [self sendMessage:textMessage];
            [self.inputBar clearInputView];
            [self.inputBar.chatSessionInputBarControl resetSendMessageBtn];
        } else {
            [self showToast:SPDStringWithKey(@"输入字数太多了哦！", nil)];
        }
    }
}

#pragma mark - RadioRoomTextMessageCellDelegate

- (void)didClickUserInTextMessageCell:(SPDTextMessage *)message {
    [self showUserInfoViewWithUserId:message.senderUserInfo.userId];
}

#pragma mark - RadioRoomFollowViewDelegate

- (void)didClickFollowButtonInFollowView {
    if (!self.radioInfo.follow) {
        [self clickFollowButton:self.followButton];
    }
}

#pragma mark - RadioRoomFollowCellDelegate

- (void)didClickFollowButtonInFollowCell {
    if (!self.radioInfo.follow) {
        [self clickFollowButton:self.followButton];
    }
}

#pragma mark - ZTWAInfoShareViewDelegate

- (void)infoShareView:(ZTWAInfoShareView *)infoShareView shareToPlatformType:(SPDUMShareType)sharePlatformType {
    NSString *shareURL = infoShareView.shareDict[@"share_url"];
    NSString *shareTitle = infoShareView.shareDict[@"title"];
    NSString *shareContent = infoShareView.shareDict[@"content"];
    NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:infoShareView.shareDict[@"thumbnail"]]];
    UIImage *image = [[UIImage alloc] initWithData:data];
//    [[SPDUMShareUtils shareInstance] shareToPlatform:sharePlatformType WithShareUrl:shareURL andshareTitle:shareTitle andshareContent:shareContent andImageData:image andShareSourece:@"RadioRoom" andSPDUMShareSuccess:^(id shareSuccess) {
//        [self showToast:SPDStringWithKey(@"分享成功", nil)];
//        [infoShareView dismiss];
//    } presentedController:self andSPDUMShareFailure:^(id shareFailure) {
//
//    }];
}

#pragma mark - Setters & Getters

- (void)setRadioInfo:(RadioModel *)radioInfo {
    _radioInfo = radioInfo;
    
    if (self.playAfterRequest && [self.radioId isEqualToString:ZegoManager.clan_id]) {
        ZegoManager.clan_cover = self.radioInfo.radioImg;
        [ZegoManager startPlayRadio:_radioInfo.radioContent];
    }
    
    [self.backgroundImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _radioInfo.background_image]]];
    [self.radioAvatarButton fp_setBackgroundImageWithURLString:_radioInfo.radioImg forState:UIControlStateNormal];
    self.radioNameLabel.text = _radioInfo.radioName;
    [self.audioWaveImageView startAnimating];
    self.followButton.selected = _radioInfo.follow;
    [self.recommendAvatarImageView fp_setImageWithURLString:_radioInfo.recommendAvatar];
    self.recomendContentLabel.text = _radioInfo.recommendContent;
}

- (void)setOnlineNumber:(NSInteger)onlineNumber {
    _onlineNumber = onlineNumber;
    
    [self.onlineNumberButton setTitle:[NSString stringWithFormat:@"%zd", _onlineNumber] forState:UIControlStateNormal];
    
    NSSortDescriptor *sortDescriptor1 = [NSSortDescriptor sortDescriptorWithKey:@"nobleValue" ascending:NO];
    NSSortDescriptor *sortDescriptor2 = [NSSortDescriptor sortDescriptorWithKey:@"level" ascending:NO];
    self.sortedOnlineUserArr = [[self.onlineUserDic allValues] sortedArrayUsingDescriptors:@[sortDescriptor1, sortDescriptor2]];
}

- (NSMutableDictionary *)onlineUserDic {
    if (!_onlineUserDic) {
        _onlineUserDic = [NSMutableDictionary new];
    }
    return _onlineUserDic;
}

- (NSMutableArray *)messageArr {
    if (!_messageArr) {
        _messageArr = [NSMutableArray new];
    }
    return _messageArr;
}

- (NSMutableArray *)cellSizeArr {
    if (!_cellSizeArr) {
        _cellSizeArr = [NSMutableArray new];
    }
    return _cellSizeArr;
}

- (RadioRoomGiftAnimationView *)giftAnimationView {
    if (!_giftAnimationView) {
        _giftAnimationView = [RadioRoomGiftAnimationView new];
        [self.view addSubview:_giftAnimationView];
        [_giftAnimationView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.onlineNumberButton.mas_bottom).with.offset(10.5);
            make.trailing.and.leading.mas_equalTo(0);
            make.height.mas_equalTo(54 * 3 + 10 * 2);
        }];
    }
    return _giftAnimationView;
}

- (RCDLiveInputBar *)inputBar {
    if (!_inputBar) {
        _inputBar = [[RCDLiveInputBar alloc] initWithFrame:CGRectMake(0, kScreenH, kScreenW, 58)];
        _inputBar.chatSessionInputBarControl.hideSwitch = YES;
        _inputBar.delegate = self;
        [self.view addSubview:_inputBar];
    }
    return _inputBar;
}

- (UIView *)inputBackgroundView {
    if (!_inputBackgroundView) {
        _inputBackgroundView = [[UIView alloc] initWithFrame:self.view.bounds];
        _inputBackgroundView.hidden = YES;
        UITapGestureRecognizer *tap  = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapInputBackgroundView:)];
        [_inputBackgroundView addGestureRecognizer:tap];
        [self.view insertSubview:_inputBackgroundView belowSubview:self.inputBar];
    }
    return _inputBackgroundView;
}

@end
