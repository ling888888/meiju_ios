//
//  RadioRecommendController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/2/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioRecommendController.h"
#import "RadioSearchViewCell.h"
#import "RadioLocationViewCell.h"
#import "RadioTypeViewCell.h"
#import "RadioListTitleViewCell.h"
#import "RadioListViewCell.h"
#import "RadioModel.h"
#import "RadioTypeModel.h"
#import "RadioRecommendListController.h"
#import "RadioTypeListViewController.h"
#import "RadioSearchViewController.h"
#import "RadioFastListenViewCell.h"
#import "RadioFastListenContainerController.h"

#define TypeCellSpace 3
#define RadioCellEdgeInsets (UIEdgeInsetsMake(10, 15, 0, 15))
#define SearchCellSize (CGSizeMake(kScreenW - 30, 40))
#define LocationCellSize (CGSizeMake((kScreenW - 30 - TypeCellSpace)/2, 65))
#define TypeCellSize (CGSizeMake((kScreenW - 15*2 - 3*3)/4, 40))
#define ListCellSize (CGSizeMake((kScreenW - 30), 75))
#define ListTitleSize (CGSizeMake((kScreenW - 30), 44))
#define FastListenSize (CGSizeMake((kScreenW - 30), 60))


#define SearchSection 0
#define LocationSection 1
#define TypeSection 2
#define FastListenSection 3
#define HistorySection 4
#define RecommendSection 5
#define RankSection 6


@interface RadioRecommendController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,RadioListTitleViewCellDelegate>

@property (nonatomic, strong) UICollectionView * collectionView;
@property (nonatomic, strong) NSArray * locationArray;
@property (nonatomic, strong) NSMutableArray * radioTypeArray;
@property (nonatomic, strong) NSMutableArray * recommendArray; // 推荐
@property (nonatomic, strong) NSMutableArray * rankArray; // 排行榜
@property (nonatomic, strong) NSMutableArray * historyArray; // 历史
@property (nonatomic, assign) BOOL isShowAll; // 控制分类

@end

@implementation RadioRecommendController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = SPD_HEXCOlOR(COMMON_PINK);
    [self.view addSubview:self.collectionView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self queryRadioHistory];
    [self requestData];
}

#pragma mark - request

- (void)requestData {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"radio.home" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        [self.radioTypeArray removeAllObjects];
        [self.radioTypeArray removeAllObjects];
        [self.recommendArray removeAllObjects];
        [self.rankArray removeAllObjects];
        
        NSArray * type = suceess[@"type"];
        for (NSDictionary * dict in type) {
            RadioTypeModel * model = [RadioTypeModel initWithDictionary:dict];
            [self.radioTypeArray addObject:model];
        }
        RadioTypeModel * emptyModel = [RadioTypeModel new];
        while (self.radioTypeArray.count % 4 != 0) {
            [self.radioTypeArray addObject:emptyModel];
        }
        RadioTypeModel * smodel = [RadioTypeModel new];
        smodel.radioTypeName = @"收起";
       if (self.radioTypeArray.count > 8) {
           if (![self.radioTypeArray containsObject:emptyModel]) {
               [self.radioTypeArray addObjectsFromArray:@[emptyModel,emptyModel,emptyModel,smodel]];
           }else{
               [self.radioTypeArray replaceObjectAtIndex:self.radioTypeArray.count - 1 withObject:smodel];
           }
       }
        
        NSArray * recommend = suceess[@"recommend"];
        if (recommend.count > 0) {
            [self.recommendArray addObject:@"recommend"];
        }
        for (NSDictionary* dict in recommend) {
            if ([dict isKindOfClass:[NSDictionary class]]) {
                RadioModel * model = [RadioModel initWithDictionary:dict];
                [self.recommendArray addObject:model] ;
            }
        }
        
        NSArray * rank = suceess[@"rank"];
        if (rank.count > 0) {
            [self.rankArray addObject:@"rank"];
        }
        for (NSDictionary* dict in rank) {
            if ([dict isKindOfClass:[NSDictionary class]]) {
                RadioModel * model = [RadioModel initWithDictionary:dict];
                [self.rankArray addObject:model];
            }
        }
        
        [self.collectionView reloadData];
        
    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)queryRadioHistory {
    NSMutableArray * historyIds = [DBUtil queryRadioHistory];
    if (historyIds.count <= 0) {
        [self.historyArray removeAllObjects];
        [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:HistorySection]];
        return;
    }
    NSMutableDictionary * dic = [NSMutableDictionary new];
    [dic setObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:historyIds options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding] forKey:@"ids"];
    [RequestUtils commonGetRequestUtils:dic bURL:@"radio.list.id" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        [self.historyArray removeAllObjects];
        
        for (NSDictionary * dict in suceess[@"list"]) {
            if ([dict isKindOfClass:[NSDictionary class]]) {
                RadioModel * model = [RadioModel initWithDictionary:dict];
                [self.historyArray addObject:model];
            }
        }
        if (self.historyArray.count > 0) {
            [self.historyArray insertObject:@"history" atIndex:0];
        }
        [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:HistorySection]];
    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 7;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    switch (section) {
          case LocationSection:
            return 2;
            break;
          case TypeSection:
            if (self.radioTypeArray.count > 8 && !self.isShowAll) {
                return 8;
            }else{
                return self.radioTypeArray.count;
            }
            break;
        case HistorySection:{
            return (self.historyArray.count>= 6) ? 6: self.historyArray.count;
            break;
        }
        case RecommendSection:{
            return self.recommendArray.count;
            break;
        }
        case RankSection:{
            return self.rankArray.count;
            break;
        }
        default:
            return 1;
            break;
    }
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case SearchSection:{
            RadioSearchViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RadioSearchViewCell" forIndexPath:indexPath];
            return cell;
            break;
        }
        case LocationSection:{
            RadioLocationViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RadioLocationViewCell" forIndexPath:indexPath];
            cell.dict = self.locationArray[indexPath.row];
            return cell;
            break;
        }
        case TypeSection: {
            RadioTypeViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RadioTypeViewCell" forIndexPath:indexPath];
            RadioTypeModel *model = self.radioTypeArray[indexPath.row];
            if (self.radioTypeArray.count > 8) {
                cell.title = (indexPath.row == 7 && !self.isShowAll) ? @"全部" :model.radioTypeName;
            }else{
                cell.title = model.radioTypeName;
            }
            return cell;
            break;
        }
        case FastListenSection:{
            RadioFastListenViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RadioFastListenViewCell" forIndexPath:indexPath];
            return cell;
            break;
        }
        default: {
            NSMutableArray * array = [NSMutableArray new];
            if (indexPath.section == HistorySection) {
               array = self.historyArray;
            }else if (indexPath.section == RecommendSection){
               array = self.recommendArray;
            }else if(indexPath.section == RankSection){
               array = self.rankArray;
            }
            if (indexPath.row == 0) {
                RadioListTitleViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RadioListTitleViewCell" forIndexPath:indexPath];
                cell.type = array[indexPath.row];
                cell.delegate = self;
                return cell;
            }else{
                RadioListViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RadioListViewCell" forIndexPath:indexPath];
                cell.radioModel = array[indexPath.row];
                cell.showCorner = (indexPath.row == array.count -1);
                return cell;
            }
            break;
        }
    }
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case TypeSection:{
            if ((self.radioTypeArray.count > 8 && !self.isShowAll && indexPath.row == 7) || (self.isShowAll && indexPath.row == self.radioTypeArray.count - 1)) {
                self.isShowAll = !self.isShowAll;
                [self.collectionView reloadData];
            }else{
                 RadioTypeModel * model = self.radioTypeArray[indexPath.row];
                if (model.radioTypeName.notEmpty) {
                    RadioTypeListViewController * vc = [RadioTypeListViewController new];
                    vc.selectedIndex = indexPath.row;
                    NSMutableArray * array = [NSMutableArray new];
                    for (RadioTypeModel * model in self.radioTypeArray) {
                        if (model.radioTypeId.notEmpty) {
                            [array addObject:model];
                        }
                    }
                    vc.typeListArray = array;
                    [self.navigationController pushViewController:vc animated:YES];
                    //[MobClick event:@"FM1_9_16"];
                }
            }
            break;
        }
        case LocationSection:{
            RadioTypeListViewController * typeList = [RadioTypeListViewController new];
            RadioRecommendListController * list = [RadioRecommendListController new];
            list.type = @"follow";
            [self.navigationController pushViewController:(indexPath.row == 0)? typeList :list  animated:YES];
            if (indexPath.row == 0) {
                //[MobClick event:@"FM1_9_2"];
            }else{
                //[MobClick event:@"FM1_9_7"];
            }
            break;
        }
        case SearchSection:{
            RadioSearchViewController * searchVC = [[RadioSearchViewController alloc]init];
            [self.navigationController pushViewController:searchVC animated:YES];
            //[MobClick event:@"FM1_9_9"];
            break;
        }
        case FastListenSection:{
            RadioFastListenContainerController * vc = [RadioFastListenContainerController sharedInstance];
            [self.navigationController pushViewController:vc animated:YES];
            //[MobClick event:@"FM1_9_42"];
            break;
        }

        default:{
            if (indexPath.row == 0) {
                return;
            }
            NSMutableArray * array = [NSMutableArray new];
            if (indexPath.section == HistorySection) {
                array = self.historyArray;
                //[MobClick event:[NSString stringWithFormat:@"FM1_9_%ld",30+indexPath.row]];
            }else if (indexPath.section == RecommendSection){
                array = self.recommendArray;
                //[MobClick event:[NSString stringWithFormat:@"FM1_9_%ld",22+indexPath.row]];
            }else if(indexPath.section == RankSection){
                array = self.rankArray;
                //[MobClick event:[NSString stringWithFormat:@"FM1_9_%ld",26+indexPath.row]];
            }
            RadioModel * model = array[indexPath.row];
            [self pushToRadioRoom:model.radioId];
            break;
        }
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case SearchSection:
            return SearchCellSize;
            break;
        case LocationSection:
            return LocationCellSize;
            break;
        case TypeSection:
            return TypeCellSize;
            break;
        case FastListenSection:
            return FastListenSize;
            break;
        default:
            return (indexPath.row == 0) ? ListTitleSize : ListCellSize;
            break;
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section  {
   return RadioCellEdgeInsets;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return (section == TypeSection || section == LocationSection) ? 3: 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return (section == TypeSection || section == LocationSection)? 3: 0;
}

#pragma mark - RadioListTitleViewCellDelegate

- (void)radioListTitleView:(RadioListTitleViewCell *)cell didClickedMore:(NSString *)type {
    RadioRecommendListController * vc = [RadioRecommendListController new];
    vc.type = type;
    [self.navigationController pushViewController:vc animated:YES];
    if ([type isEqualToString:@"recommend"]) {
        //[MobClick event:@"FM1_9_22"];
    }else if ([type isEqualToString:@"rank"]){
        //[MobClick event:@"FM1_9_26"];
    }else{
        //[MobClick event:@"FM1_9_30"];
    }
}

#pragma mark - JXPagerViewListViewDelegate

- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return self.collectionView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *scrollView))callback {
    
}

#pragma mark - Getters

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH - TabBarHeight) collectionViewLayout:[UICollectionViewFlowLayout new]];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerNib:[UINib nibWithNibName:@"RadioSearchViewCell" bundle:nil] forCellWithReuseIdentifier:@"RadioSearchViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"RadioLocationViewCell" bundle:nil] forCellWithReuseIdentifier:@"RadioLocationViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"RadioTypeViewCell" bundle:nil] forCellWithReuseIdentifier:@"RadioTypeViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"RadioListViewCell" bundle:nil] forCellWithReuseIdentifier:@"RadioListViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"RadioListTitleViewCell" bundle:nil] forCellWithReuseIdentifier:@"RadioListTitleViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"RadioFastListenViewCell" bundle:nil] forCellWithReuseIdentifier:@"RadioFastListenViewCell"];
        _collectionView.backgroundColor = SPD_HEXCOlOR(@"f7f7f7");
    }
    return _collectionView;
}

- (NSArray *)locationArray {
    if (!_locationArray) {
        _locationArray = @[@{
                               @"title": @"国家台",
                               @"image": @"ic_fm_guojiatai",
                               @"flag": @"bg_fm_guojiadiantai"
                        },@{
                               @"title": @"关注台",
                               @"image": @"ic_fm_guanzhudiantai",
                               @"flag": @"bg_fm_guanzhudiantai"
                        }];
    }
    return _locationArray;
}

- (NSMutableArray *)radioTypeArray {
    if (!_radioTypeArray) {
        _radioTypeArray = [NSMutableArray new];
    }
    return _radioTypeArray;
}

- (NSMutableArray *)recommendArray {
    if (!_recommendArray) {
        _recommendArray = [NSMutableArray new];
    }
    return _recommendArray;
}

- (NSMutableArray *)historyArray {
    if (!_historyArray) {
        _historyArray = [NSMutableArray new];
    }
    return _historyArray;
}

- (NSMutableArray *)rankArray {
    if (!_rankArray) {
        _rankArray = [NSMutableArray new];
    }
    return _rankArray;
}

@end
