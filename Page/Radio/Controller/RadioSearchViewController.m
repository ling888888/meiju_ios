//
//  RadioSearchViewController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/2/24.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioSearchViewController.h"
#import "RadioListTableCell.h"
#import "RadioModel.h"
#import "RadioSearchNoDataTableCell.h"
#import "RadioSearchNoDataFooterCell.h"
#import "NSString+XXWAddition.h"
#import "RadioRecommendListController.h"

@interface RadioSearchViewController ()<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *historyViewTopCons;
@property (weak, nonatomic) IBOutlet UIView *historyView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *historyViewHeight;
@property (weak, nonatomic) IBOutlet UIView *titleView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * listArray;
@property (nonatomic, assign) BOOL isNoData;
@property (weak, nonatomic) IBOutlet UIView *historyListView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *historyListViewHeightCons;

@end

@implementation RadioSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIView *wrapView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW - 8 * 2, 44)];
    self.searchBar.frame = wrapView.bounds;
    [wrapView addSubview:self.searchBar];
    self.navigationItem.titleView = wrapView;
    self.historyViewTopCons.constant = NavHeight;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(requestSearch)];
    [self.titleView addGestureRecognizer:tap];
    [self.view addSubview:self.tableView];
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = true;
    
    [self configHistoryView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.searchBar becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.searchBar resignFirstResponder];
}

#pragma mark - response

- (void)handleHistoryLabel:(UITapGestureRecognizer *) tap {
    NSMutableArray * array = [DBUtil queryRadioSearchHistory];
    self.searchBar.text = array[tap.view.tag];
    [self requestSearch];
    //[MobClick event:@"FM1_9_12"];
}
- (IBAction)clearHIstory:(id)sender {
    [DBUtil deleteRadioSearchHistory];
    self.historyView.hidden = YES;
    //[MobClick event:@"FM1_9_13"];
}

- (void)configHistoryView {
    NSMutableArray * array = [DBUtil queryRadioSearchHistory];
    if (array.count <= 0) { self.historyView.hidden = YES; return;}
    for (UIView * view in self.historyListView.subviews) {
        [view removeFromSuperview];
    }
    CGFloat space = 15;
    CGFloat y = 0;
    CGFloat x = 15;
    CGFloat h = 30;
       for (NSInteger i = 0; i< array.count; i++) {
           UILabel * label = [UILabel new];
           label.text = array[i];
           label.textColor = [UIColor colorWithRed:26.0/255 green:26.0/255 blue:26.0/255 alpha:0.6];
           label.font = [UIFont systemFontOfSize:15];
           label.backgroundColor = SPD_HEXCOlOR(@"#F7F7F7");
           label.textAlignment = NSTextAlignmentCenter;
           [self.historyListView addSubview:label];
           label.userInteractionEnabled = YES;
           label.tag = i;
           UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleHistoryLabel:)];
           CGFloat width = [NSString sizeWithString:array[i] andFont:[UIFont systemFontOfSize:15] andMaxSize:CGSizeMake(kScreenW - 30 - 40, 30)].width + 40;
           [label addGestureRecognizer:tap];
           [label mas_makeConstraints:^(MASConstraintMaker *make) {
               make.leading.mas_equalTo(x);
               make.top.mas_equalTo(y);
               make.width.mas_equalTo(width);
               make.height.mas_equalTo(30);
           }];
           label.layer.cornerRadius = 15;
           label.clipsToBounds = YES;
           // 判断是不是需要换行
           if (i == array.count - 1) {
               break;
           }
          CGFloat nextWidth = [NSString sizeWithString:array[i +1] andFont:[UIFont systemFontOfSize:15] andMaxSize:CGSizeMake(kScreenW - 30 - 40, 30)].width  + 40;
          if (width + x + nextWidth + space*2 >= kScreenW) {
              x = space;
              y+= h+ space;
          }else{
              x+= width + space;
          }
       }
        // 更新historyview 的高度
     self.historyViewHeight.constant = y + h + 44;
     self.historyListViewHeightCons.constant = y + h;
     [self.historyListView layoutIfNeeded];
     [self.historyView layoutIfNeeded];
}

#pragma mark - request

- (void)requestSearch {
    [self.searchBar resignFirstResponder];
    UIButton *cancelBtn = [self.searchBar valueForKey:@"cancelButton"];
    cancelBtn.enabled = YES;
    [DBUtil recordRadioSearchHistory:self.searchBar.text];
    [RequestUtils commonGetRequestUtils:[@{@"text":self.searchBar.text}mutableCopy] bURL:@"radio.search" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        [self.listArray removeAllObjects];
        self.titleView.hidden = YES;
        self.tableView.hidden = NO;
        NSArray * list = suceess[@"list"];
        NSArray * data =list.count == 0 ? suceess[@"rank"] : list;
        self.isNoData = (list.count == 0);
        for (NSDictionary * dic in data) {
            RadioModel * model = [RadioModel initWithDictionary:dic];
            [self.listArray addObject:model];
        }
        [self.tableView reloadData];
    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 1:{
            return self.listArray.count;
            break;
        }
        default:{
            return self.isNoData ? 1:0;
            break;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:{
            RadioSearchNoDataTableCell * cell = [RadioSearchNoDataTableCell cellWithTableView:tableView];
            return cell;
            break;
        }
        case 2:{
            RadioSearchNoDataFooterCell * cell = [RadioSearchNoDataFooterCell cellWithTableView:tableView];
            return cell;
            break;
        }
        default:{
            RadioListTableCell * cell = [RadioListTableCell cellWithTableView:tableView];
            RadioModel *model = self.listArray[indexPath.row];
            model.index = self.isNoData ? (indexPath.row + 1) : 0 ;
            cell.radioModel = self.listArray[indexPath.row];
            return cell;
            break;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            return 155;
            break;
        default:
            return 75;
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 2:{
            RadioRecommendListController * list = [RadioRecommendListController new];
            list.type = @"rank";
            [self.navigationController pushViewController:list animated:YES];
            //[MobClick event:@"FM1_9_15"];
            break;
        }
        default:{
            if (self.isNoData && indexPath.section == 1) {
                RadioModel * model = self.listArray[indexPath.row];
                [self pushToRadioRoom:model.radioId];
                //[MobClick event:@"FM1_9_14"];
            }else if (!self.isNoData) {
                RadioModel * model = self.listArray[indexPath.row];
                [self pushToRadioRoom:model.radioId];
            }
            break;
        }
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if ([searchText isEqualToString:@""]) {
        self.titleView.hidden = YES;
        self.tableView.hidden = YES;
        self.historyView.hidden = !([DBUtil queryRadioSearchHistory].count > 0);
        [self configHistoryView];
        //[MobClick event:@"FM1_9_11"];
    } else {
        self.titleView.hidden = NO;
        self.tableView.hidden = YES;
        self.historyView.hidden = YES;
        NSMutableAttributedString * title = [[NSMutableAttributedString alloc]initWithString: SPDStringWithKey(@"点击搜索：\"", nil)];
        [title addAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"1A1A1A"],NSFontAttributeName:[UIFont fontWithName:@"PingFang-SC-Medium" size:15]} range:NSMakeRange(0, title.length)];
        NSAttributedString * searchStr = [[NSAttributedString alloc]initWithString:searchText attributes:@{NSFontAttributeName:[UIFont fontWithName:@"PingFang-SC-Medium" size:15],NSForegroundColorAttributeName:SPD_HEXCOlOR(COMMON_PINK)}];
        NSAttributedString * str = [[NSAttributedString alloc]initWithString:@"\"" attributes:@{NSFontAttributeName:[UIFont fontWithName:@"PingFang-SC-Medium" size:15],NSForegroundColorAttributeName:SPD_HEXCOlOR(@"1A1A1A")}];
        [title appendAttributedString:searchStr];
        [title appendAttributedString:str];
        self.titleLabel.attributedText = title;
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    //[MobClick event:@"FM1_9_10"];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self requestSearch];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.searchBar resignFirstResponder];
    UIButton *cancelBtn = [self.searchBar valueForKey:@"cancelButton"];
    cancelBtn.enabled = YES;
}

#pragma mark - setter / getter

- (UISearchBar *)searchBar {
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc] init];
        _searchBar.delegate = self;
        _searchBar.placeholder = SPDStringWithKey(@"输入关键词进行搜索", nil);
        _searchBar.showsCancelButton = YES;
        _searchBar.backgroundImage = [[UIImage alloc] init];
        [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil] setTitle:SPDStringWithKey(@"取消", nil)];
        UITextField *searchField = [_searchBar valueForKey:@"searchField"];
        if (searchField) {
            searchField.backgroundColor = SPD_HEXCOlOR(@"#F3F3F3");
            searchField.layer.masksToBounds = YES;
            if ([[UIDevice currentDevice].systemVersion floatValue] >= 11) {
                searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:SPDStringWithKey(@"输入关键词进行搜索", nil) attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15],NSForegroundColorAttributeName:[UIColor colorWithRed:26.0/255 green:26.0/255 blue:26.0/255 alpha:0.2]}];
            }
        }
        _searchBar.tintColor = [UIColor blackColor];
        if (searchField) {
            if ([[UIDevice currentDevice].systemVersion floatValue] >= 11) {
                searchField.layer.cornerRadius = 18;
            } else {
                searchField.layer.cornerRadius = 14;
            }
        }
    }
    return _searchBar;
}

#pragma mark - Getters

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, NavHeight, kScreenW, kScreenH - NavHeight) style:UITableViewStylePlain];
        _tableView.hidden = YES;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (NSMutableArray *)listArray {
    if (!_listArray) {
        _listArray = [NSMutableArray new];
    }
    return _listArray;
}


@end
