//
//  RadioTypeListViewController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/2/20.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioTypeListViewController.h"
#import "RadioTypeTitleCell.h"
#import "RadioTypeTitleCell.h"
#import "RadioTypeListDropMenuView.h"
#import "RadioListViewCell.h"
#import "RadioTypeModel.h"
#import "RadioModel.h"

@interface RadioTypeListViewController ()<UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource,RadioTypeListDropMenuViewDelegate>

@property (nonatomic, strong) UICollectionView * topCollectionView;
@property (nonatomic, strong) UICollectionView * bottomCollectionView;
@property (nonatomic, strong) NSMutableArray * dataArray;
@property (nonatomic, strong) UIButton * openBtn;
@property (nonatomic, copy) NSString * currentRadioId;
@property (nonatomic, assign) int page;
@property (nonatomic, assign) BOOL isTypeVC;// 是不是电台类型页面

@end

@implementation RadioTypeListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.topCollectionView];
    [self.topCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(8);
        make.trailing.mas_equalTo(-40);
        make.top.mas_equalTo(NavHeight);
        make.height.mas_equalTo(44);
    }];
    
    self.openBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.openBtn setImage:[UIImage imageNamed:@"ic_shengfenzhankai"] forState:UIControlStateNormal];
    [self.view addSubview:self.openBtn];
    [self.openBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavHeight);
        make.trailing.mas_equalTo(0);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(44);
    }];
    [self.openBtn addTarget:self action:@selector(handleDropDownMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.bottomCollectionView];
    [self.bottomCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.trailing.mas_equalTo(0);
        make.top.equalTo(self.topCollectionView.mas_bottom).offset(0);
        make.bottom.mas_equalTo(0);
    }];
    
    if (self.typeListArray.count <= 0) {
        // 代表现在是国家台分类进行数据请求
        self.navigationItem.title = SPDStringWithKey(@"国家台", nil);
        self.isTypeVC = NO;
        [self requestCountryList];
    }else{
        self.navigationItem.title = SPDStringWithKey(@"电台类型", nil);
        self.isTypeVC = YES;
        [self.topCollectionView reloadData];
        [self refreshListDataWithDelay:YES];
    }
}


- (void)handleDropDownMenu:(UIButton *)sender {
    if (self.typeListArray.count <= 0) {
        return;
    }
    RadioTypeListDropMenuView * menu = [[RadioTypeListDropMenuView alloc]initWithFrame:self.view.bounds];
    menu.title = self.isTypeVC ? SPDStringWithKey(@"切换电台类型", nil):SPDStringWithKey(@"切换国家台", nil);
    menu.selectIndex = self.selectedIndex;
    menu.dataArray = self.typeListArray;
    menu.delegate = self;
    [self.view addSubview:menu];
    [menu show];
    //[MobClick event:self.isTypeVC ? @"FM1_9_17": @"FM1_9_5"];

}

// 刷新列表数据
- (void)refreshListDataWithDelay:(BOOL)delay {
    RadioTypeModel * selectedModel = self.typeListArray[self.selectedIndex];
    [self.topCollectionView reloadData];
    CGFloat delayTime = delay ? 0.25:0;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.topCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:self.selectedIndex] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    });
    self.currentRadioId = selectedModel.radioTypeId;
    self.page = 0;
    [self.bottomCollectionView.mj_header beginRefreshing];
}

#pragma mark - Request

- (void)requestData {
    NSMutableDictionary * dict = [NSMutableDictionary new];
    [dict setValue:@(self.page) forKey:@"page"];
    [dict setValue:self.currentRadioId forKey:self.isTypeVC ? @"type":@"countryId"];
    [RequestUtils commonGetRequestUtils:dict bURL:self.isTypeVC? @"radio.list.type":@"radio.list.country" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        NSArray *list = suceess[@"list"];
        if (self.page == 0) {
            [self.dataArray removeAllObjects];
        }
        for (NSDictionary * dic in list) {
            RadioModel * model = [RadioModel initWithDictionary:dic];
            [self.dataArray addObject:model];
        }
        [self.bottomCollectionView reloadData];
        self.page++;
        [self.bottomCollectionView.mj_header endRefreshing];
        if (self.bottomCollectionView.mj_footer.state == MJRefreshStateRefreshing) {
            [self.bottomCollectionView.mj_footer endRefreshingWithCompletionBlock:^{
                self.bottomCollectionView.mj_footer.hidden = !list.count;
            }];
        } else {
            self.bottomCollectionView.mj_footer.hidden = !list.count;
        }
    } bFailure:^(id  _Nullable failure) {
        [self.bottomCollectionView.mj_header endRefreshing];
        [self.bottomCollectionView.mj_footer endRefreshing];
        [self showTips:failure[@"msg"]];
    }];
}

- (void)requestCountryList {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"radio.country" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        [self.typeListArray removeAllObjects];
        NSString * curCountryId = suceess[@"curCountryId"];
        NSArray * list = suceess[@"list"];
        for (NSInteger i = 0; i < list.count ; i++) {
            RadioTypeModel * model = [RadioTypeModel initWithDictionary:list[i]];
            if ([model.radioTypeId isEqualToString:curCountryId] && curCountryId.notEmpty) {
                self.selectedIndex = i;
            }
            [self.typeListArray addObject:model];
        }
        [self.topCollectionView reloadData];
        [self refreshListDataWithDelay:YES];
        
    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.topCollectionView == collectionView ? self.typeListArray.count : 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return collectionView == self.topCollectionView ?  1 : self.dataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.topCollectionView) {
        RadioTypeTitleCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RadioTypeTitleCell" forIndexPath:indexPath];
        RadioTypeModel* model = self.typeListArray[indexPath.section];
        cell.title = model.radioTypeName;
        cell.backgroundColor = (indexPath.section == self.selectedIndex) ? SPD_HEXCOlOR(COMMON_PINK) : [UIColor whiteColor];
        cell.titleLabel.textColor =  (indexPath.section == self.selectedIndex) ? [UIColor whiteColor] : SPD_HEXCOlOR(@"#1A1A1A");
        return cell;
    }else{
        RadioListViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RadioListViewCell" forIndexPath:indexPath];
        cell.radioModel = self.dataArray[indexPath.row];
        return cell;
    }
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.topCollectionView) {
        self.selectedIndex = indexPath.section;
        [collectionView reloadData];
        [self refreshListDataWithDelay:NO];
        //[MobClick event: self.isTypeVC ? @"FM1_9_21":@""];
    }else{
        // to do 进入radio
        RadioModel * model = self.dataArray[indexPath.row];
        [self pushToRadioRoom:model.radioId];
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.topCollectionView) {
        RadioTypeModel * model = self.typeListArray[indexPath.section];
        return CGSizeMake(model.cellWidth, 24);
    }else{
        return CGSizeMake(kScreenW, 75);
    }
}

#pragma maek - RadioTypeListDropMenuViewDelegate

- (void)radioTypeListDropMenuTapIndex:(NSInteger) index {
    self.selectedIndex = index;
    [self refreshListDataWithDelay:YES];
}

#pragma mark - Getters

- (UICollectionView *)topCollectionView {
    if (!_topCollectionView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _topCollectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        [_topCollectionView registerNib:[UINib nibWithNibName:@"RadioTypeTitleCell" bundle:nil] forCellWithReuseIdentifier:@"RadioTypeTitleCell"];
        _topCollectionView.delegate = self;
        _topCollectionView.dataSource = self;
        _topCollectionView.backgroundColor = [UIColor whiteColor];
        _topCollectionView.showsHorizontalScrollIndicator = NO;
    }
    return _topCollectionView;
}

- (UICollectionView *)bottomCollectionView {
    if (!_bottomCollectionView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _bottomCollectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        [_bottomCollectionView registerNib:[UINib nibWithNibName:@"RadioListViewCell" bundle:nil] forCellWithReuseIdentifier:@"RadioListViewCell"];
        _bottomCollectionView.delegate = self;
        _bottomCollectionView.dataSource = self;
        _bottomCollectionView.backgroundColor = [UIColor whiteColor];
        _bottomCollectionView.showsVerticalScrollIndicator = NO;
        __weak typeof(self) weakSelf = self;
        _bottomCollectionView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            weakSelf.page = 0;
            [weakSelf requestData];
        }];
        _bottomCollectionView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            [weakSelf requestData];
        }];
    }
    return _bottomCollectionView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

- (NSMutableArray *)typeListArray {
    if (!_typeListArray) {
        _typeListArray = [NSMutableArray new];
    }
    return _typeListArray;
}

- (NSInteger)selectedIndex {
    if (!_selectedIndex) {
        _selectedIndex = 0;
    }
    return _selectedIndex;
}

@end
