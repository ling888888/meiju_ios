//
//  RadioRoomContainerController.h
//  SimpleDate
//
//  Created by 李楠 on 2020/2/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "WMPageController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RadioRoomContainerController : WMPageController

@property (nonatomic, copy) NSString *radioId;

@end

NS_ASSUME_NONNULL_END
