//
//  RadioRecommendListController.h
//  SimpleDate
//
//  Created by ling Hou on 2020/2/20.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RadioRecommendListController : BaseViewController

@property (nonatomic, copy) NSString * type; // [@"history" @"rank" @"recommend" @"follow"]

@end

NS_ASSUME_NONNULL_END
