//
//  RadioModel.h
//  SimpleDate
//
//  Created by ling Hou on 2020/2/19.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "CommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RadioModel : CommonModel

@property (nonatomic, copy) NSString *radioId;
@property (nonatomic, copy) NSString *radioName; // 电台名称
@property (nonatomic, copy) NSString *radioImg;
@property (nonatomic, copy) NSString *radioOnlineNum;
@property (nonatomic, copy) NSString *radioListenerNum;
@property (nonatomic, copy) NSString *liveContent; // 正在播放的内容
@property (nonatomic, assign) NSInteger index; // 列表里的第几行

@property (nonatomic, copy) NSString *radioContent; // 电台播放链接
@property (nonatomic, assign) BOOL follow;
@property (nonatomic, copy) NSString *recommendAvatar;
@property (nonatomic, copy) NSString *recommendContent;
@property (nonatomic, copy) NSString *radioReferral; // 电台简介
@property (nonatomic, copy) NSNumber *followNum;
@property (nonatomic, copy) NSString *background_image;

@end

NS_ASSUME_NONNULL_END
