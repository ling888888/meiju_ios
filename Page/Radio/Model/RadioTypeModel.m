//
//  RadioTypeModel.m
//  SimpleDate
//
//  Created by ling Hou on 2020/2/19.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioTypeModel.h"
#import "NSString+XXWAddition.h"

@implementation RadioTypeModel

- (void)setValue:(id)value forKey:(NSString *)key {
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"countryName"]) {
        self.radioTypeName = value;
        self.cellWidth = [NSString  sizeWithString:value andFont:[UIFont boldSystemFontOfSize:12] andMaxSize:CGSizeMake(CGFLOAT_MAX, 26)].width + 40;
    }else if ([key isEqualToString:@"countryId"]){
        self.radioTypeId = value;
    }
}

- (void)setRadioTypeName:(NSString *)radioTypeName {
    _radioTypeName = radioTypeName;
    self.cellWidth = [NSString  sizeWithString:_radioTypeName andFont:[UIFont boldSystemFontOfSize:12] andMaxSize:CGSizeMake(CGFLOAT_MAX, 26)].width + 40;
}

@end
