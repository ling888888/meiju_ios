//
//  RadioTypeModel.h
//  SimpleDate
//
//  Created by ling Hou on 2020/2/19.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "CommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RadioTypeModel : CommonModel

@property (nonatomic, copy) NSString * radioTypeId; //电台类别id
@property (nonatomic, copy) NSString * radioTypeName; // 电台类别名称
@property (nonatomic, copy) NSString * radioTypeImg; // 电台背景
@property (nonatomic, assign) NSInteger cellWidth;

@end

NS_ASSUME_NONNULL_END
