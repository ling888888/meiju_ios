//
//  RadioPlayListTableCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/10.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioPlayListTableCell.h"

@interface RadioPlayListTableCell ()


@end

@implementation RadioPlayListTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
   
    NSMutableArray *animationImages = [NSMutableArray array];
    for (NSInteger i = 0; i < 8; i++) {
        [animationImages addObject:[UIImage imageNamed:[NSString stringWithFormat:@"audio_wave_%zd", i]]];
    }
    self.waveImageview.animationImages = animationImages;
    self.waveImageview.animationDuration = animationImages.count * 0.1;
}

- (void)setHighlighted:(BOOL)highlighted {
    
}

- (void)setSelected:(BOOL)selected {
    
}

@end
