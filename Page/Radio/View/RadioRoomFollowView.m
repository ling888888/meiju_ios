//
//  RadioRoomFollowView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/3/6.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioRoomFollowView.h"
#import "RadioModel.h"

@interface RadioRoomFollowView ()

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end

@implementation RadioRoomFollowView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.contentViewBottom.constant = -42 - 320;
    [self layoutIfNeeded];
}

#pragma mark - Public methods

- (void)showWithModel:(RadioModel *)model {
    [self.avatarImageView fp_setImageWithURLString:model.radioImg];
    self.nameLabel.text = model.radioName;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.contentViewBottom.constant = -100 + IphoneX_Bottom;
        [self layoutIfNeeded];
    }];
}

#pragma mark - Event responses

- (IBAction)tapBackground:(UITapGestureRecognizer *)sender {
    [self hide];
}

- (IBAction)clickFollowButton:(UIButton *)sender {
    [self hide];
    if ([self.delegate respondsToSelector:@selector(didClickFollowButtonInFollowView)]) {
        [self.delegate didClickFollowButtonInFollowView];
    }
}

#pragma mark - Private methods

- (void)hide {
    [UIView animateWithDuration:0.25 animations:^{
        self.contentViewBottom.constant = -42 - 320;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ![touch.view isDescendantOfView:self.contentView] && ![touch.view isDescendantOfView:self.avatarImageView];
}

@end
