//
//  RadioSearchNoDataFooterCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/2/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioSearchNoDataFooterCell.h"

@implementation RadioSearchNoDataFooterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
