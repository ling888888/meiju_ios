//
//  RadioRoomOnlineUserViewCell.h
//  SimpleDate
//
//  Created by 李楠 on 2020/2/28.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class SPDVoiceLiveUserModel;

@interface RadioRoomOnlineUserViewCell : UICollectionViewCell

@property (nonatomic, strong) SPDVoiceLiveUserModel *model;

@end

NS_ASSUME_NONNULL_END
