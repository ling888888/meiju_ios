//
//  RadioRoomGiftAnimationView.h
//  SimpleDate
//
//  Created by 李楠 on 2020/3/2.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RadioRoomGiftAnimationView : UIView

- (void)showAnimationWithMessage:(RCMessageContent *)message;

@end

NS_ASSUME_NONNULL_END
