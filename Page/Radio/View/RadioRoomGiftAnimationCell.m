//
//  RadioRoomGiftAnimationCell.m
//  SimpleDate
//
//  Created by 李楠 on 2020/3/2.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioRoomGiftAnimationCell.h"
#import "SPDChatRoomSendPresentMessage.h"

@interface RadioRoomGiftAnimationCell ()

@property (weak, nonatomic) IBOutlet UIImageView *senderAvatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *senderNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *receiverAvatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *receiverNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *giftImageView;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;

@end

@implementation RadioRoomGiftAnimationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setMessage:(RCMessageContent *)message {
    _message = message;
    
    if ([message isKindOfClass:[SPDChatRoomSendPresentMessage class]]) {
        SPDChatRoomSendPresentMessage *presentMessage = (SPDChatRoomSendPresentMessage *)message;
        [self.senderAvatarImageView fp_setImageWithURLString:presentMessage.senderUserInfo.portraitUri];
        self.senderNameLabel.text = presentMessage.senderUserInfo.name;
        [self.receiverAvatarImageView fp_setImageWithURLString:presentMessage.receive_avatar];
        self.receiverNameLabel.text = presentMessage.receive_nickname;
        [self.giftImageView fp_setImageWithURLString:presentMessage.present_url];
        self.countLabel.text = [NSString stringWithFormat:@"x%@", presentMessage.num];
    }
}

@end
