//
//  RadioRoomUserInfoActionCell.h
//  SimpleDate
//
//  Created by 李楠 on 2020/3/2.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RadioRoomUserInfoActionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

NS_ASSUME_NONNULL_END
