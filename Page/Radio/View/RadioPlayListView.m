//
//  RadioPlayListView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/10.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioPlayListView.h"
#import "RadioPlayListTableCell.h"
#import "RadioModel.h"

@interface RadioPlayListView ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UILabel * titleLabel;

@end

@implementation RadioPlayListView

- (id)initWithStyle:(LY_AlertViewStyle)style {
    if (self = [super initWithStyle:style]) {
        self.titleLabel = [[UILabel alloc]init];
        self.titleLabel.font = [UIFont systemFontOfSize:15];
        self.titleLabel.textColor = [UIColor colorWithHexString:@"#333333"];
        [self.contentView addSubview:self.titleLabel];
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(18);
            make.leading.mas_equalTo(18);
        }];
        self.titleLabel.text = @"播放列表".localized;
        
        self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.tableFooterView = [UIView new];
        self.tableView.separatorColor = [UIColor colorWithHexString:@"#EFEFEF"];
        [self.contentView addSubview:self.tableView];
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titleLabel.mas_bottom).mas_offset(18);
            make.leading.mas_equalTo(0);
            make.trailing.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
        }];
    }
    return self;
}

- (void)setDataArray:(NSMutableArray *)dataArray {
    _dataArray = dataArray;
    [self.tableView reloadData];
}

- (CGFloat)containerHeight {
    return 433;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RadioPlayListTableCell * cell = [RadioPlayListTableCell cellWithTableView:tableView];
    RadioModel * model = self.dataArray[indexPath.row];
    cell.titleLabel.text = model.radioName;
    cell.titleLabel.textColor = (indexPath.row == self.currentIndex) ? SPD_HEXCOlOR(@"00D4A0"):SPD_HEXCOlOR(@"333333");
    cell.waveImageview.hidden = !(indexPath.row == self.currentIndex);
    if (indexPath.row == self.currentIndex) {
        [cell.waveImageview startAnimating];
    }else{
        [cell.waveImageview stopAnimating];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.currentIndex = indexPath.row;
    if (self.delegate && [self.delegate respondsToSelector:@selector(radioPlayListView:didSelectIndex:)]) {
        [self.delegate radioPlayListView:self didSelectIndex:indexPath.row];
    }
    [self.tableView reloadData];
}

@end
