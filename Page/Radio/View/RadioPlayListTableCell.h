//
//  RadioPlayListTableCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/10.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RadioPlayListTableCell : BaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *waveImageview;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

NS_ASSUME_NONNULL_END
