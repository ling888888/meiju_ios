//
//  RadioRoomFollowView.h
//  SimpleDate
//
//  Created by 李楠 on 2020/3/6.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class RadioModel;

@protocol RadioRoomFollowViewDelegate <NSObject>

@optional

- (void)didClickFollowButtonInFollowView;

@end

@interface RadioRoomFollowView : UIView

@property (nonatomic, weak) id<RadioRoomFollowViewDelegate> delegate;

- (void)showWithModel:(RadioModel *)model;

@end

NS_ASSUME_NONNULL_END
