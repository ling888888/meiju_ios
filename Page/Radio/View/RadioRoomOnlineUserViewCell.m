//
//  RadioRoomOnlineUserViewCell.m
//  SimpleDate
//
//  Created by 李楠 on 2020/2/28.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioRoomOnlineUserViewCell.h"
#import "SPDVoiceLiveUserModel.h"

@interface RadioRoomOnlineUserViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *genderAgeButton;
@property (weak, nonatomic) IBOutlet UIImageView *vehicleImageView;

@end

@implementation RadioRoomOnlineUserViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(SPDVoiceLiveUserModel *)model {
    _model = model;
    
    [self.avatarImageView fp_setImageWithURLString:_model.avatar];
    self.nickNameLabel.text = _model.nick_name;
    [self.genderAgeButton setTitle:_model.age.stringValue forState:UIControlStateNormal];
    if ([_model.gender isEqualToString:@"female"]) {
        [self.genderAgeButton setBackgroundColor:[UIColor colorWithHexString:@"ff7eca"]];
        [self.genderAgeButton setImage:[UIImage imageNamed:@"chatroomuser_female"] forState:UIControlStateNormal];
    } else if ([_model.gender isEqualToString:@"male"]) {
        [self.genderAgeButton setBackgroundColor:[UIColor colorWithHexString:@"5db4fd"]];
        [self.genderAgeButton setImage:[UIImage imageNamed:@"chatroomuser_male"] forState:UIControlStateNormal];
    }
    if (_model.driveVehicle.notEmpty) {
        self.vehicleImageView.hidden = NO;
        [self.vehicleImageView fp_setImageWithURLString:_model.driveVehicle];
    } else {
        self.vehicleImageView.hidden = YES;
    }
}

@end
