//
//  RadioRoomGiftAnimationView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/3/2.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioRoomGiftAnimationView.h"
#import "RadioRoomGiftAnimationCell.h"

@interface RadioRoomGiftAnimationView ()

@property (nonatomic, assign) NSInteger trackCount;
@property (nonatomic, strong) NSMutableArray *waitingMessageArr;
@property (nonatomic, strong) NSMutableDictionary *animationStateDic;
@property (nonatomic, strong) NSMutableDictionary *animationCellDic;

@end

@implementation RadioRoomGiftAnimationView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initProperties];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self initProperties];
}

#pragma mark - Public methods

- (void)showAnimationWithMessage:(RCMessageContent *)message {
    for (NSInteger i = 0; i < self.trackCount; i++) {
        if (![self.animationStateDic[@(i)] boolValue]) {
            [self startAnimationWithMessage:message onTrack:@(i)];
            return;
        }
    }
    [self.waitingMessageArr addObject:message];
}

#pragma mark - Private methods

- (void)initProperties {
    self.userInteractionEnabled = NO;
    self.trackCount = 3;
    for (NSInteger i = 0; i < self.trackCount; i++) {
        [self.animationStateDic setObject:@(NO) forKey:@(i)];
    }
}

- (void)startAnimationWithMessage:(RCMessageContent *)message onTrack:(NSNumber *)track {
    [self.animationStateDic setObject:@(YES) forKey:track];
    
    RadioRoomGiftAnimationCell *cell = self.animationCellDic[track];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"RadioRoomGiftAnimationCell" owner:self options:nil].firstObject;
        [self addSubview:cell];
        [cell mas_makeConstraints:^(MASConstraintMaker *make) {
            make.trailing.mas_equalTo(-kScreenW);
            make.top.mas_equalTo((54 + 10) * track.integerValue);
            make.width.mas_equalTo(kScreenW);
            make.height.mas_equalTo(54);
        }];
        [self layoutIfNeeded];
        [self.animationCellDic setObject:cell forKey:track];
    }
    cell.message = message;
    
    [UIView animateWithDuration:0.5 animations:^{
        [cell mas_updateConstraints:^(MASConstraintMaker *make) {
            make.trailing.mas_equalTo(0);
        }];
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self performSelector:@selector(stopAnimationOnTrack:) withObject:track afterDelay:3 inModes:@[NSRunLoopCommonModes]];
    }];
}

- (void)stopAnimationOnTrack:(NSNumber *)track {
    RadioRoomGiftAnimationCell *cell = self.animationCellDic[track];
    [UIView animateWithDuration:0.5 animations:^{
        cell.alpha = 0;
    } completion:^(BOOL finished) {
        [cell mas_updateConstraints:^(MASConstraintMaker *make) {
            make.trailing.mas_equalTo(-kScreenW);
        }];
        cell.alpha = 1;
        
        if (self.waitingMessageArr.count) {
            [self startAnimationWithMessage:self.waitingMessageArr[0] onTrack:track];
            [self.waitingMessageArr removeObjectAtIndex:0];
        } else {
            [self.animationStateDic setObject:@(NO) forKey:track];
        }
    }];
}

#pragma mark - Setters & Getters

- (NSMutableArray *)waitingMessageArr {
    if (!_waitingMessageArr) {
        _waitingMessageArr = [NSMutableArray new];
    }
    return _waitingMessageArr;
}

- (NSMutableDictionary *)animationStateDic {
    if (!_animationStateDic) {
        _animationStateDic = [NSMutableDictionary new];
    }
    return _animationStateDic;
}

- (NSMutableDictionary *)animationCellDic {
    if (!_animationCellDic) {
        _animationCellDic = [NSMutableDictionary new];
    }
    return _animationCellDic;
}

@end
