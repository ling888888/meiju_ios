//
//  RadioListTableCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/2/20.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioListTableCell.h"
#import "RadioModel.h"

@interface RadioListTableCell ()

@property (weak, nonatomic) IBOutlet UIImageView *radioImageView;
@property (weak, nonatomic) IBOutlet UILabel *radioTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *radioLiveContentLabel;
@property (weak, nonatomic) IBOutlet UILabel *radioListnerNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *radioOnlineNumLabel;
@property (weak, nonatomic) IBOutlet UIImageView *radioListenerIcon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *radioListenerNumLabelTopCons;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *radioTitleTopCons;
@property (weak, nonatomic) IBOutlet UILabel *indexTitleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *coverLeadingCons;

@end

@implementation RadioListTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.indexTitleLabel.font = [UIFont fontWithName:@"Helvetica-Oblique" size:18];
}

- (void)setRadioModel:(RadioModel *)radioModel {
    _radioModel = radioModel;
    [self.radioImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,_radioModel.radioImg]]];
    self.radioTitleLabel.text = _radioModel.radioName;
    self.radioOnlineNumLabel.text = _radioModel.radioOnlineNum;
    self.radioListnerNumLabel.text = [SPDCommonTool transformIntegerToBriefStr:[_radioModel.radioListenerNum integerValue]];
    self.radioLiveContentLabel.text = _radioModel.liveContent.notEmpty ? [NSString stringWithFormat:SPDStringWithKey(@"正在直播：%@", nil),_radioModel.liveContent] :@"";
    self.radioListenerNumLabelTopCons.constant = _radioModel.liveContent.notEmpty ? 30 : 10;
    self.radioTitleTopCons.constant = _radioModel.liveContent.notEmpty ? 3 : 15;
    self.coverLeadingCons.constant = _radioModel.index != 0 ? 39: 10;
    self.indexTitleLabel.text = [NSString stringWithFormat:@"%@",@(_radioModel.index)];
    self.indexTitleLabel.hidden = !(_radioModel.index !=0);
    switch (_radioModel.index) {
        case 1:
            self.indexTitleLabel.textColor = SPD_HEXCOlOR(@"#FFD249");
            break;
        case 2:
            self.indexTitleLabel.textColor = SPD_HEXCOlOR(@"#C6C6C6");
            break;
        case 3:
            self.indexTitleLabel.textColor = SPD_HEXCOlOR(@"#E1A571");
            break;
        default:
            self.indexTitleLabel.textColor = SPD_HEXCOlOR(@"#1A1A1A");
            break;
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
