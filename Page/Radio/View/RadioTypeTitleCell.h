//
//  RadioTypeTitleCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/2/20.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RadioTypeTitleCell : UICollectionViewCell

@property (nonatomic, copy) NSString * title;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

NS_ASSUME_NONNULL_END
