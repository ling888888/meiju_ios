//
//  RadioRoomTextMessageCell.m
//  SimpleDate
//
//  Created by 李楠 on 2020/3/3.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioRoomTextMessageCell.h"
#import "SPDTextMessage.h"
#import "LiveRoomTextMessage.h"
#import "SVGA.h"
#import "UIImage+LY.h"

@interface RadioRoomTextMessageCell ()

@property (weak, nonatomic) IBOutlet UIButton *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *genderAgeButton;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *nobleImageView;
@property (weak, nonatomic) IBOutlet UIImageView *specialNumImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vipLeadingCons;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *specialNumLeadingCons;
@property (weak, nonatomic) IBOutlet UIButton *userLevelButton;
@property (weak, nonatomic) IBOutlet UIButton *anchorButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nobleLeadingCons;
@property (weak, nonatomic) IBOutlet SVGAPlayer *vipPlayer;
@property (weak, nonatomic) IBOutlet UIView *fansView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fansViewLeading;
@property (weak, nonatomic) IBOutlet UIImageView *fansLevelImage;
@property (weak, nonatomic) IBOutlet UILabel *fansNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *fansBgView;
@property (weak, nonatomic) IBOutlet UILabel *fansLevelLabel;
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;

@end

@implementation RadioRoomTextMessageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.vipPlayer.contentMode = UIViewContentModeScaleAspectFit;
    self.vipPlayer.loops = 0;
    self.vipPlayer.clearsAfterStop = YES;
    SVGAParser *parser = [[SVGAParser alloc] init];
    [parser parseWithNamed:@"vip_member" inBundle:[NSBundle mainBundle] completionBlock:^(SVGAVideoEntity * _Nonnull videoItem) {
        self.vipPlayer.videoItem = videoItem;
        [self.vipPlayer startAnimation];
    } failureBlock:^(NSError * _Nonnull error) {
        
    }];
    UILongPressGestureRecognizer *longPressAvatarImageView = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressAvatarImageView:)];
    [self.avatarImageView addGestureRecognizer:longPressAvatarImageView];
}

- (IBAction)clickAvatarButton:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickUserInTextMessageCell:)]) {
        [self.delegate didClickUserInTextMessageCell:self.message];
    }
    if (_textMessage && _textMessage.senderUserInfo.userId.notEmpty) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LiveRoomTapAvatar" object:nil userInfo:@{@"userId":_textMessage.senderUserInfo.userId}];
    }
}

- (void)setMessage:(SPDTextMessage *)message {
    _message = message;
    
    [self.coverImageView fp_setImageWithURLString:_message.senderUserInfo.portraitUri];
    self.nickNameLabel.text = _message.senderUserInfo.name;
    [self.genderAgeButton setTitle:_message.age forState:UIControlStateNormal];
    if ([_message.gender isEqualToString:@"female"]) {
        [self.genderAgeButton setBackgroundColor:[UIColor colorWithHexString:@"ff7eca"]];
        [self.genderAgeButton setImage:[UIImage imageNamed:@"chatroomuser_female"] forState:UIControlStateNormal];
    } else if ([_message.gender isEqualToString:@"male"]) {
        [self.genderAgeButton setBackgroundColor:[UIColor colorWithHexString:@"5db4fd"]];
        [self.genderAgeButton setImage:[UIImage imageNamed:@"chatroomuser_male"] forState:UIControlStateNormal];
    }
    self.contentLabel.text = _message.content;
    
    self.nobleImageView.hidden = YES;
    self.specialNumImageView.hidden = YES;
    self.vipPlayer.hidden = YES;
    self.userLevelButton.hidden = YES;
    self.anchorButton.hidden = YES;
    self.fansView.hidden = YES;
}

- (void)setTextMessage:(LiveRoomTextMessage *)textMessage {
    _textMessage = textMessage;    
    self.nickNameLabel.text = _textMessage.senderUserInfo.name;
    [self.genderAgeButton setTitle:_textMessage.age forState:UIControlStateNormal];
    if ([_textMessage.gender isEqualToString:@"female"]) {
        [self.genderAgeButton setBackgroundColor:[UIColor colorWithHexString:@"ff7eca"]];
        [self.genderAgeButton setImage:[UIImage imageNamed:@"chatroomuser_female"] forState:UIControlStateNormal];
    } else if ([_textMessage.gender isEqualToString:@"male"]) {
        [self.genderAgeButton setBackgroundColor:[UIColor colorWithHexString:@"5db4fd"]];
        [self.genderAgeButton setImage:[UIImage imageNamed:@"chatroomuser_male"] forState:UIControlStateNormal];
    }
   
    [self.coverImageView fp_setImageWithURLString:_textMessage.senderUserInfo.portraitUri];
    self.contentLabel.textColor = SPD_HEXCOlOR(@"#ffffff");
    
    if (_textMessage.mentionedType.notEmpty) {
        if ([_textMessage.senderUserInfo.userId isEqualToString:[SPDApiUser currentUser].userId]) {
               self.contentLabel.textColor = SPD_HEXCOlOR(@"#FFD265");
        }
        for (NSDictionary *dic in _textMessage.mentionedList) {
            if ([dic[@"user_id"] isEqualToString:[SPDApiUser currentUser].userId]) {
                self.contentLabel.textColor = SPD_HEXCOlOR(@"#FFD265");
            }
        }
    }
    self.contentLabel.text = _textMessage.content;
    
    CGFloat leading = self.isAnchor ? 5 : -33;
    
    self.nobleLeadingCons.constant = leading;
    self.anchorButton.hidden = !self.isAnchor;
    self.userLevelButton.hidden = YES;
    self.nobleImageView.hidden = !_textMessage.medal.notEmpty;
    if (_textMessage.medal.notEmpty) {
        leading += 16 + 5;
    }
    self.vipLeadingCons.constant =  leading ;
    
    [self.nobleImageView fp_setImageWithURLString:_textMessage.medal];
    self.vipPlayer.hidden = ![_textMessage.isVip isEqualToString:@"true"];
    if ([_textMessage.isVip isEqualToString:@"true"]) {
        leading += 16 + 5;
    }
    self.specialNumLeadingCons.constant = leading;
    self.specialNumImageView.hidden =!_textMessage.specialNum.notEmpty;
    if (_textMessage.specialNum.notEmpty) {
        leading += 16 + 5;
    }

    if (_textMessage.fansLevel.notEmpty && _textMessage.fansName.notEmpty) {
        self.fansViewLeading.constant = leading + 5;
        self.fansView.hidden = NO;
        self.fansNameLabel.text = _textMessage.fansName;
        self.fansLevelImage.image = [UIImage imageNamed:[self getFansIcon:_textMessage.fansLevel]];
        self.fansBgView.image = [UIImage imageNamed:[self getFansbg:_textMessage.fansLevel]];
        self.fansLevelLabel.text = _textMessage.fansLevel;
    }else{
        self.fansView.hidden = YES;
    }
}

- (void)longPressAvatarImageView:(UILongPressGestureRecognizer *)longPress {
    if ([self.textMessage isKindOfClass:[LiveRoomTextMessage class]] && self.textMessage) {
        RCUserInfo * user = self.textMessage.senderUserInfo;
        if (user && longPress.state == UIGestureRecognizerStateBegan) {
            NSDictionary *userInfo = @{@"user_id": user.userId ?:@"", @"nick_name": user.name?:@"", @"avatar": user.portraitUri?:@""};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LiveLongPressAvatar" object:nil userInfo:userInfo];
        }
    }
}

- (NSString *)getFansIcon:(NSString *) level {
    NSInteger fansLevel = [level integerValue];
    if (fansLevel >=1 && fansLevel <= 4) {
        return @"ic_zhibojian_fensixunzhang1-4";
    }else if (fansLevel>= 5 && fansLevel <=9){
        return @"ic_zhibojian_fensixunzhang5-9";
    }else{
        return @"ic_zhibojian_fensixunzhang";
    }
}

- (NSString *)getFansbg:(NSString *) level {
    NSInteger fansLevel = [level integerValue];
    if (fansLevel >=1 && fansLevel <= 4) {
        return @"bg_fensixunzhang1_10";
    }else if (fansLevel>= 5 && fansLevel <=9){
        return @"bg_fensixunzhang_11_20";
    }else{
        return @"bg_fensixunzhang_21_30";
    }
}


@end
