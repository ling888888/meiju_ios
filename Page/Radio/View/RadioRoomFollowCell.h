//
//  RadioRoomFollowCell.h
//  SimpleDate
//
//  Created by 李楠 on 2020/3/9.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol RadioRoomFollowCellDelegate <NSObject>

@optional

- (void)didClickFollowButtonInFollowCell;

@end

@interface RadioRoomFollowCell : UICollectionViewCell

@property (nonatomic, weak) id<RadioRoomFollowCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
