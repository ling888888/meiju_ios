//
//  RadioListTableCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/2/20.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class RadioModel;

@interface RadioListTableCell : BaseTableViewCell

@property (nonatomic, strong) RadioModel *radioModel;

@end

NS_ASSUME_NONNULL_END
