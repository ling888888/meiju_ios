//
//  RadioRoomRadioInfoView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/2/27.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioRoomRadioInfoView.h"
#import "RadioModel.h"

@interface RadioRoomRadioInfoView ()<UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *liveContentLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *numberImageViewTop;
@property (weak, nonatomic) IBOutlet UILabel *listenerNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *followNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *referralLabel;
@property (weak, nonatomic) IBOutlet UIButton *followButton;

@end

@implementation RadioRoomRadioInfoView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.contentViewBottom.constant = -366;
    [self layoutIfNeeded];
}

#pragma mark - Public methods

- (void)showWithModel:(RadioModel *)model {
    [self.avatarImageView fp_setImageWithURLString:model.radioImg];
    self.nameLabel.text = model.radioName;
    if (model.liveContent.notEmpty) {
        self.liveContentLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"正在直播：%@", nil), model.liveContent];
    } else {
        self.liveContentLabel.hidden = YES;
        self.numberImageViewTop.constant = 5.5;
    }
    self.listenerNumLabel.text = [SPDCommonTool transformIntegerToBriefStr:model.radioListenerNum.integerValue];
    self.followNumLabel.text = [SPDCommonTool transformIntegerToBriefStr:model.followNum.integerValue];
    self.referralLabel.text = model.radioReferral;
    if (model.follow) {
        [self.followButton setTitle:SPDStringWithKey(@"取消关注", nil) forState:UIControlStateNormal];
        [self.followButton setTitleColor:[UIColor colorWithHexString:@"#F8145B"] forState:UIControlStateNormal];
        self.followButton.superview.backgroundColor = [UIColor colorWithHexString:@"#E8E8E8"];
    } else {
        [self.followButton setTitle:SPDStringWithKey(@"关注", nil) forState:UIControlStateNormal];
        [self.followButton setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:UIControlStateNormal];
        [self.followButton setImage:[UIImage imageNamed:@"radio_room_follow"] forState:UIControlStateNormal];
        self.followButton.titleEdgeInsets = UIEdgeInsetsMake(0, 4.5, 0, 0);
        self.followButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 4.5);
        self.followButton.mirrorEdgeInsets = YES;
        self.followButton.superview.backgroundColor = [UIColor colorWithHexString:COMMON_PINK];
    }
    
    [UIView animateWithDuration:0.25 animations:^{
        self.contentViewBottom.constant = -100 + IphoneX_Bottom;
        [self layoutIfNeeded];
    }];
}

#pragma mark - Event responses

- (IBAction)tapBackground:(UITapGestureRecognizer *)sender {
    [self hide];
}

- (IBAction)clickFollowButton:(UIButton *)sender {
    [self hide];
    if ([self.delegate respondsToSelector:@selector(didClickFollowButtonInRadioInfoView)]) {
        [self.delegate didClickFollowButtonInRadioInfoView];
    }
}

#pragma mark - Private methods

- (void)hide {
    [UIView animateWithDuration:0.25 animations:^{
        self.contentViewBottom.constant = -366;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ![touch.view isDescendantOfView:self.contentView];
}

@end
