//
//  RadioListViewCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/2/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class RadioModel;
@interface RadioListViewCell : UICollectionViewCell

@property (nonatomic, strong) RadioModel * radioModel;
@property (nonatomic, assign) BOOL showCorner; //是否显示圆角

@end

NS_ASSUME_NONNULL_END
