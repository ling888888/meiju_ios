//
//  RadioRoomUserInfoView.h
//  SimpleDate
//
//  Created by 李楠 on 2020/2/28.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class HomeModel;

@protocol RadioRoomUserInfoViewDelegate <NSObject>

@optional

- (void)didClickUserInUserInfoView:(HomeModel *)model;
- (void)didClickGiftGivingInUserInfoView:(HomeModel *)model;

@end


@interface RadioRoomUserInfoView : UIView

@property (nonatomic, weak) id<RadioRoomUserInfoViewDelegate> delegate;

- (void)showWithUserId:(NSString *)userId;

@end

NS_ASSUME_NONNULL_END
