//
//  RadioRoomRadioInfoView.h
//  SimpleDate
//
//  Created by 李楠 on 2020/2/27.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class RadioModel;

@protocol RadioRoomRadioInfoViewDelegate <NSObject>

@optional

- (void)didClickFollowButtonInRadioInfoView;

@end

@interface RadioRoomRadioInfoView : UIView

@property (nonatomic, weak) id<RadioRoomRadioInfoViewDelegate> delegate;

- (void)showWithModel:(RadioModel *)model;

@end

NS_ASSUME_NONNULL_END
