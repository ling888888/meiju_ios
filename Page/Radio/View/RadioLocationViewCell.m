//
//  RadioLocationViewCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/2/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioLocationViewCell.h"

@interface RadioLocationViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UIImageView *flagImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelCenterX;

@end

@implementation RadioLocationViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.labelCenterX.constant = [SPDCommonTool currentVersionIsArbic] ? -29 : 29;
}


- (void)setDict:(NSDictionary *)dict {
    _dict = dict;
    self.coverImageView.image = [UIImage imageNamed:_dict[@"image"]];
    self.titleLabel.localizedKey = _dict[@"title"];
    self.flagImageView.image = [UIImage imageNamed:_dict[@"flag"]];
}

@end
