//
//  RadioListTitleViewCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/2/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioListTitleViewCell.h"

@interface RadioListTitleViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *bgCoverImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation RadioListTitleViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(5,5)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
}

- (void)setType:(NSString *)type {
    _type = type;
    if ([_type isEqualToString:@"recommend"]) {
        self.bgCoverImageView.image = [UIImage imageNamed:@"img_fm_bendituijian"];
        self.titleLabel.text = SPDLocalizedString(@"精选推荐");
    }else if ([_type isEqualToString:@"history"]){
        self.bgCoverImageView.image = [UIImage imageNamed:@"img_fm_zuijinshouting"];
         self.titleLabel.text = SPDLocalizedString(@"最近收听");
    }else if ([_type isEqualToString:@"rank"]){
        self.bgCoverImageView.image = [UIImage imageNamed:@"img_fm_paihangbang"];
         self.titleLabel.text = SPDLocalizedString(@"排行榜");
    }
    self.bgCoverImageView.mirrorImage = YES;
}

- (IBAction)moreButtonClicked:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(radioListTitleView:didClickedMore:)]) {
        [self.delegate radioListTitleView:self didClickedMore:self.type];
    }
}

@end
