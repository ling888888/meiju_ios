//
//  RadioTypeViewCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/2/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RadioTypeViewCell : UICollectionViewCell

@property (nonatomic, copy) NSString * title;

@end

NS_ASSUME_NONNULL_END
