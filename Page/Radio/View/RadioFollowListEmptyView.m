//
//  RadioFollowListEmptyView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/2/28.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioFollowListEmptyView.h"

@interface RadioFollowListEmptyView ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation RadioFollowListEmptyView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.titleLabel.attributedText = [[NSAttributedString alloc]initWithString:SPDStringWithKey(@"关注电台列表为空，\n去发现你喜欢的电台吧～", nil)];
   
}

@end
