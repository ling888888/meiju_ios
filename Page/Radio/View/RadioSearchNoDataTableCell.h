//
//  RadioSearchNoDataTableCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/2/24.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RadioSearchNoDataTableCell : BaseTableViewCell

@end

NS_ASSUME_NONNULL_END
