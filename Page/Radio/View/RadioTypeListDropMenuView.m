//
//  RadioTypeListDropMenuView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/2/21.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioTypeListDropMenuView.h"
#import "RadioTypeModel.h"

@interface RadioTypeListDropMenuView ()<UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIView * bgView; //
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIView * lineView; //分割线
@property (nonatomic, strong) UIButton * actionBtn;
@property (nonatomic, assign) CGFloat height;

@end

@implementation RadioTypeListDropMenuView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        [self setUI];
    }
    return self;
}

- (void)setUI {
    
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.trailing.mas_equalTo(0);
        make.top.mas_equalTo(-400);
        make.height.mas_equalTo(400);
    }];
    
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    self.titleLabel.textColor = SPD_HEXCOlOR(@"#1A1A1A");
    [self.bgView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(44);
    }];
    
    self.actionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.actionBtn setImage:[UIImage imageNamed:@"ic_shengfenshouqi"] forState:UIControlStateNormal];
    [self.bgView addSubview:self.actionBtn];
    [self.actionBtn addTarget:self action:@selector(hide) forControlEvents:UIControlEventTouchUpInside];
    [self.actionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(0);
        make.top.and.mas_equalTo(0);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(44);
    }];
    
    self.lineView = [UIView new];
    self.lineView.backgroundColor = SPD_HEXCOlOR(@"EDEDED");
    [self.bgView addSubview:self.lineView];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(44);
        make.leading.and.trailing.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hide)];
    tap.delegate = self;
    [self addGestureRecognizer:tap];
}

- (void)setDataArray:(NSMutableArray *)dataArray {
    _dataArray = dataArray;
    CGFloat space = 15;
    CGFloat y = space + 44;
    CGFloat x = 15;
    for (NSInteger i = 0; i< _dataArray.count; i++) {
        RadioTypeModel * model = _dataArray[i];
        UILabel * label = [UILabel new];
        label.text = model.radioTypeName;
        label.textColor = (i == self.selectIndex)? [UIColor whiteColor]:SPD_HEXCOlOR(@"#1A1A1A");
        label.font = [UIFont boldSystemFontOfSize:12];
        label.backgroundColor = (i == self.selectIndex) ? SPD_HEXCOlOR(COMMON_PINK):SPD_HEXCOlOR(@"EDEDED");
        label.textAlignment = NSTextAlignmentCenter;
        [self.bgView addSubview:label];
        label.tag = i;
        label.userInteractionEnabled = YES;
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handle:)];
        [label addGestureRecognizer:tap];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo(x);
            make.top.mas_equalTo(y);
            make.width.mas_equalTo(model.cellWidth);
            make.height.mas_equalTo(24);
        }];
        label.layer.cornerRadius = 12;
        label.clipsToBounds = YES;
        // 判断是不是需要换行
        if (i == _dataArray.count - 1) {
            continue;
        }
       RadioTypeModel * model1 = _dataArray[i +1];
       if (model.cellWidth + x + model1.cellWidth + space*2 >= kScreenW) {
           x = space;
           y+= 24+ space;
       }else{
           x+= model.cellWidth + space;
       }
    }
     self.height =  y;
     [self.bgView mas_updateConstraints:^(MASConstraintMaker *make) {
           make.top.mas_equalTo( -self.height - 44);
           make.height.mas_equalTo(self.height + 44);
     }];
    [self layoutIfNeeded];
}

- (void)show {
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [self.bgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(kScreenH>=812 ? 84:64);
            make.height.mas_equalTo(self.height + 44);
        }];
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)hide {
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.bgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo( -self.height - 44);
        }];
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
    //[MobClick event:@"FM1_9_6"];
}

- (void)handle:(UITapGestureRecognizer *)tap {
    NSInteger tag = tap.view.tag;
    if (self.delegate && [self.delegate respondsToSelector:@selector(radioTypeListDropMenuTapIndex:)]) {
        [self hide];
        [self.delegate radioTypeListDropMenuTapIndex:tag];
    }
}

-(void)setTitle:(NSString *)title {
    _title = title;
    self.titleLabel.text = _title;
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ![touch.view isDescendantOfView:self.bgView];
}


@end
