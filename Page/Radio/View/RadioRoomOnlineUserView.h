//
//  RadioRoomOnlineUserView.h
//  SimpleDate
//
//  Created by 李楠 on 2020/2/28.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class SPDVoiceLiveUserModel;

@protocol RadioRoomOnlineUserViewDelegate <NSObject>

@optional

- (void)didClickUserInOnlineUserView:(SPDVoiceLiveUserModel *)model;

@end

@interface RadioRoomOnlineUserView : UIView

@property (nonatomic, weak) id<RadioRoomOnlineUserViewDelegate> delegate;

- (void)showWithDataArray:(NSArray *)dataArray;

@end

NS_ASSUME_NONNULL_END
