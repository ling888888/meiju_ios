//
//  RadioFastListenTypeCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/10.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioFastListenTypeCell.h"
#import "RadioTypeModel.h"

@interface RadioFastListenTypeCell ()

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UIImageView *waveImageView;
@property(nonatomic, strong) CADisplayLink *link;

@end



@implementation RadioFastListenTypeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
  
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.bgView.layer.cornerRadius = (kScreenW - 120 - 60)/2;
    self.bgView.clipsToBounds = YES;
    self.bgView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.2].CGColor;
    self.bgView.layer.shadowOffset = CGSizeMake(0,0);
    self.bgView.layer.shadowOpacity = 1;
    self.bgView.layer.shadowRadius = 15;
    self.coverImageView.layer.cornerRadius = (kScreenW - 120 - 60 - 10)/2;
    self.coverImageView.clipsToBounds = YES;
}

- (void)setModel:(RadioTypeModel *)model {
    _model = model;
    [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,model.radioTypeImg]] placeholderImage:[UIImage imageNamed:@"img_fm_yijianshouting_moren"]];
}

- (void)setAnimating:(BOOL)animating {
    _animating = animating;
    if (_animating) {
        if (!self.link) {
            self.link = [CADisplayLink displayLinkWithTarget:self selector:@selector(rotation)];
            self.link.frameInterval = 1;
            [self.link addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        }
        self.link.paused = NO;
        [self waveDark];
    }else{
        self.link.paused = YES;
    }
}

- (void)waveLight{
    if (!self.animating) {
        return;
    }
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.waveImageView.alpha = 1;
    } completion:^(BOOL finished) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self waveDark];
        });
    }];
}

- (void)waveDark{
    if (!self.animating) {
        return;
    }
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.waveImageView.alpha = 0;
    } completion:^(BOOL finished) {
        [self waveLight];
    }];
}

-(void)rotation{
    self.bgView.layer.transform = CATransform3DRotate(self.bgView.layer.transform, 2*M_PI /(25*60), 0, 0, 1);
    NSLog(@"link ----- %@",self.model.radioTypeId);
}

- (void)invalidateLink {
    if (self.link) {
        [self.link invalidate];
        self.link = nil;
        NSLog(@"销毁销毁");
    }
}


@end
