//
//  RadioRoomUserInfoView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/2/28.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioRoomUserInfoView.h"
#import "HomeModel.h"
#import "RadioRoomUserInfoActionCell.h"

@interface RadioRoomUserInfoView ()<UICollectionViewDataSource, UICollectionViewDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet UIButton *avatarButton;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *genderAgeButton;
@property (weak, nonatomic) IBOutlet UILabel *idLabel;
@property (weak, nonatomic) IBOutlet UIImageView *vehicleImageView;
@property (weak, nonatomic) IBOutlet UICollectionView *actionCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *actionFlowLayout;

@property (nonatomic, strong) HomeModel *userInfo;
@property (nonatomic, strong) NSMutableArray *actionArray;

@end

@implementation RadioRoomUserInfoView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.actionCollectionView registerNib:[UINib nibWithNibName:@"RadioRoomUserInfoActionCell" bundle:nil] forCellWithReuseIdentifier:@"RadioRoomUserInfoActionCell"];
    
    self.contentViewBottom.constant = -44 - 303.5;
    [self layoutIfNeeded];
}

#pragma mark - Public methods

- (void)showWithUserId:(NSString *)userId {
    [MBProgressHUD showHUDAddedTo:self animated:YES];
    
    NSMutableDictionary *params = [@{@"user_id": userId} mutableCopy];
    [RequestUtils commonGetRequestUtils:params bURL:@"user.info" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        self.userInfo = [HomeModel initWithDictionary:suceess];
        [self.avatarButton fp_setBackgroundImageWithURLString:self.userInfo.avatar forState:UIControlStateNormal];
        self.nickNameLabel.text = self.userInfo.nick_name;
        
        if ([self.userInfo.gender isEqualToString:@"male"]) {
            self.genderAgeButton.backgroundColor = [UIColor colorWithHexString:@"3c91f1"];
            [self.genderAgeButton setImage:[UIImage imageNamed:@"chatroomuser_male"] forState:UIControlStateNormal];
        } else {
            self.genderAgeButton.backgroundColor = [UIColor colorWithHexString:@"fe69a1"];
            [self.genderAgeButton setImage:[UIImage imageNamed:@"chatroomuser_female"] forState:UIControlStateNormal];
        }
        [self.genderAgeButton setTitle:[NSString stringWithFormat:@"%@", self.userInfo.age] forState:UIControlStateNormal];
        
        for (NSDictionary *dic in suceess[@"vehicleList"]) {
            if ([dic[@"is_drive"] boolValue]) {
                [self.vehicleImageView fp_setImageWithURLString:dic[@"image"]];
            }
        }
        self.idLabel.text = [NSString stringWithFormat:@"ID:%@", self.userInfo.invite_code];

        [self.actionArray addObject:@"gift"];
        if (![userId isEqualToString:[SPDApiUser currentUser].userId] && !self.userInfo.is_friend) {
            [self.actionArray addObject:@"friend"];
        }
        CGFloat inset = (kScreenW - self.actionFlowLayout.itemSize.width * self.actionArray.count - self.actionFlowLayout.minimumLineSpacing * (self.actionArray.count - 1)) / 2.0;
        self.actionFlowLayout.sectionInset = UIEdgeInsetsMake(0, inset, 0, inset);
        [self.actionCollectionView reloadData];
        
        [MBProgressHUD hideHUDForView:self animated:YES];
        [self show];
    } bFailure:^(id  _Nullable failure) {
        [MBProgressHUD hideHUDForView:self animated:YES];
        [self removeFromSuperview];
    }];
}

#pragma mark - Event responses

- (IBAction)tapBackground:(UITapGestureRecognizer *)sender {
    [self hide];
}

- (IBAction)clickAvatarButton:(UIButton *)sender {
    [self hide];
    if ([self.delegate respondsToSelector:@selector(didClickUserInUserInfoView:)]) {
        [self.delegate didClickUserInUserInfoView:self.userInfo];
    }
}

#pragma mark - Private methods

- (void)show {
    [UIView animateWithDuration:0.25 animations:^{
        self.contentViewBottom.constant = -100 + IphoneX_Bottom;
        [self layoutIfNeeded];
    }];
}

- (void)hide {
    [UIView animateWithDuration:0.25 animations:^{
        self.contentViewBottom.constant = -44 - 303.5;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.actionArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RadioRoomUserInfoActionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RadioRoomUserInfoActionCell" forIndexPath:indexPath];
    cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"room_user_info_%@", self.actionArray[indexPath.row]]];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
            [self hide];
            if ([self.delegate respondsToSelector:@selector(didClickGiftGivingInUserInfoView:)]) {
                [self.delegate didClickGiftGivingInUserInfoView:self.userInfo];
            }
            break;
        case 1:
            if ([self.actionArray[1] isEqualToString:@"friend"]) {
                NSDictionary *params = @{@"type": @"request", @"to": self.userInfo._id};
                [RequestUtils commonPostRequestUtils:[NSMutableDictionary dictionaryWithDictionary:params] bURL:@"friend" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
                    [SPDCommonTool showWindowToast:SPDStringWithKey(@"请求发送成功，请等待对方回应", nil)];
                    [self.actionArray replaceObjectAtIndex:1 withObject:@"friend_sent"];
                    [self.actionCollectionView reloadData];
                } bFailure:^(id  _Nullable failure) {
                    [SPDCommonTool showWindowToast:failure[@"msg"]];
                }];
            }
            break;
        default:
            break;
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ![touch.view isDescendantOfView:self.contentView];
}

#pragma mark - Setters & Getters

- (NSMutableArray *)actionArray {
    if (!_actionArray) {
        _actionArray = [NSMutableArray array];
    }
    return _actionArray;
}

@end
