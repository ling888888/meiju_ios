//
//  RadioRoomFollowCell.m
//  SimpleDate
//
//  Created by 李楠 on 2020/3/9.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioRoomFollowCell.h"

@implementation RadioRoomFollowCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (IBAction)clickFollowButton:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickFollowButtonInFollowCell)]) {
        [self.delegate didClickFollowButtonInFollowCell];
    }
}

@end
