//
//  RadioTypeTitleCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/2/20.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioTypeTitleCell.h"

@interface RadioTypeTitleCell ()
@end

@implementation RadioTypeTitleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setTitle:(NSString *)title {
    _title = title;
    self.titleLabel.text = _title;
}



@end
