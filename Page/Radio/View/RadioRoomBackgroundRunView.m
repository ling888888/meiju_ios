//
//  RadioRoomBackgroundRunView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/3/4.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioRoomBackgroundRunView.h"
#import <SVGAPlayer/SVGA.h>
#import "ZegoKitManager.h"
#import "TimerPowerOffMgr.h"
#import "LiveRoomViewController.h"
#import "LiveModel.h"
#import "YYWebImage.h"

@interface RadioRoomBackgroundRunView ()

@property (weak, nonatomic) IBOutlet UIButton *avatarButton;
@property (weak, nonatomic) IBOutlet SVGAPlayer *svgaPlayer;
@property (weak, nonatomic) IBOutlet YYAnimatedImageView *backImageView;

@end

@implementation RadioRoomBackgroundRunView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.layer.shadowColor = [UIColor colorWithRed:26 / 255.0 green:26 / 255.0 blue:26 / 255.0 alpha:0.3].CGColor;
    self.layer.shadowOffset = CGSizeMake(0, 0);
    self.layer.shadowOpacity = 1;
    self.layer.shadowRadius = 10;
    
    [[SVGAParser new] parseWithNamed:@"radio_background_run" inBundle:[NSBundle mainBundle] completionBlock:^(SVGAVideoEntity * _Nonnull videoItem) {
        self.svgaPlayer.videoItem = videoItem;
        [self.svgaPlayer startAnimation];
    } failureBlock:^(NSError * _Nonnull error) {
        
    }];
    self.backImageView.yy_imageURL = [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"audio_live_little" ofType:@"webp"]];

}

#pragma mark - Event responses

- (IBAction)pan:(UIPanGestureRecognizer *)sender {
    switch (sender.state) {
        case UIGestureRecognizerStateChanged: {
            CGPoint offset = [sender translationInView:self.superview];
            CGPoint center = CGPointMake(self.center.x + offset.x, self.center.y + offset.y);
            self.center = center;
            break;
        }
        case UIGestureRecognizerStateEnded: {
            sender.enabled = NO;
            CGFloat endedCenterX, endedCenterY;
            if (self.center.x <kScreenW / 2.0) {
                endedCenterX = 15.5 + self.bounds.size.width / 2.0;
            } else {
                endedCenterX = kScreenW - 15.5 - self.bounds.size.width / 2.0;
            }
            if (self.center.y < NavHeight + 16 + self.bounds.size.height / 2.0) {
                endedCenterY = NavHeight + 16 + self.bounds.size.height / 2.0;
            } else if (self.center.y > kScreenH - TabBarHeight - 16 - self.bounds.size.height / 2.0) {
                endedCenterY = kScreenH - TabBarHeight - 16 - self.bounds.size.height / 2.0;
            } else {
                endedCenterY = self.center.y;
            }
            [UIView animateWithDuration:0.1 animations:^{
                self.center = CGPointMake(endedCenterX, endedCenterY);
            } completion:^(BOOL finished) {
                sender.enabled = YES;
            }];
            break;
        }
        default:
            break;
    }
    [sender setTranslation:CGPointMake(0, 0) inView:self.superview];
}

- (IBAction)clickAvatarButton:(UIButton *)sender {
    switch (ZegoManager.sceneType) {
        case SceneTypeRadioRoom: {
//            RadioRoomContainerController *vc = [RadioRoomContainerController new];
//            vc.radioId = self.radioId;
//            UITabBarController *tabBarController = (UITabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
//            [(UINavigationController *)tabBarController.selectedViewController pushViewController:vc animated:YES];
            break;
        }
        case SceneTypeListenRadio: {
//            UITabBarController *tabBarController = (UITabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
//            [(UINavigationController *)tabBarController.selectedViewController pushViewController:[RadioFastListenContainerController sharedInstance] animated:YES];
            break;
        }
        case SceneTypeLiveRoom: {
            [ZegoManager joinLiveRoomWithliveInfo:self.liveInfo isAnchor:NO];
            break;
        }
        default:
            break;
    }
}

- (IBAction)clickCloseButton:(UIButton *)sender {
    switch (ZegoManager.sceneType) {
        case SceneTypeListenRadio:
            [ZegoManager exitListenRadio];
            break;
        default:
            [ZegoManager leaveRoom];
            break;
    }
}

#pragma mark - Setters & Getters

- (void)setRadioAvatar:(NSString *)radioAvatar {
    _radioAvatar = radioAvatar;
    
    [self.avatarButton fp_setBackgroundImageWithURLString:_radioAvatar forState:UIControlStateNormal];
}

- (void)setLiveInfo:(LiveModel *)liveInfo {
    _liveInfo = liveInfo;
    
    [self.avatarButton fp_setBackgroundImageWithURLString:_liveInfo.liveCover forState:UIControlStateNormal];
}

@end
