//
//  RadioTypeViewCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/2/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioTypeViewCell.h"
#import "UIButton+Additions.h"

@interface RadioTypeViewCell ()

@property (weak, nonatomic) IBOutlet UIButton *titleButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation RadioTypeViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setTitle:(NSString *)title {
    _title = title;
    [self.titleButton setImage:nil forState:UIControlStateNormal];
    if ([_title isEqualToString:@"全部"] || [_title isEqualToString:@"收起"]) {
        self.titleButton.hidden = NO;
        self.titleLabel.hidden = YES;
        [self.titleButton setTitle:SPDLocalizedString(_title) forState:UIControlStateNormal];
        [self.titleButton setImage:[UIImage imageNamed:[_title isEqualToString:@"全部"] ? @"ic_fm_fenlei_zhankai" : @"ic_fm_fenlei_shouqi"] forState:UIControlStateNormal];
        [self.titleButton setImagePosition:HHImagePositionRight spacing:3];
        [self.titleButton setMirrorEdgeInsets:YES];
    }else{
        self.titleLabel.hidden = !_title.notEmpty;
        self.titleButton.hidden = YES;
        self.titleLabel.text = _title;
    }
}

@end
