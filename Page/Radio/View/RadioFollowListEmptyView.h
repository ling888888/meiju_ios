//
//  RadioFollowListEmptyView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/2/28.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RadioFollowListEmptyView : UIView

@end

NS_ASSUME_NONNULL_END
