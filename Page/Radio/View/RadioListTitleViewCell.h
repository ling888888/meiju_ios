//
//  RadioListTitleViewCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/2/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class RadioListTitleViewCell;

@protocol RadioListTitleViewCellDelegate <NSObject>

- (void)radioListTitleView:(RadioListTitleViewCell *)cell didClickedMore:(NSString *)type;

@end

@interface RadioListTitleViewCell : UICollectionViewCell

@property (nonatomic, copy) NSString * type;

@property (nonatomic, weak)id<RadioListTitleViewCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
