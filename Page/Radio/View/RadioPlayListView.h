//
//  RadioPlayListView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/10.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class RadioPlayListView,LY_AlertView;

@protocol RadioPlayListViewDelegate <NSObject>

- (void)radioPlayListView:(RadioPlayListView *)view didSelectIndex:(NSInteger)currentIndex;

@end

@interface RadioPlayListView : LY_AlertView

@property (nonatomic, strong) NSMutableArray * dataArray;
@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, weak) id<RadioPlayListViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
