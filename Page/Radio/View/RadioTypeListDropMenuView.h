//
//  RadioTypeListDropMenuView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/2/21.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol RadioTypeListDropMenuViewDelegate <NSObject>

@optional
- (void)radioTypeListDropMenuTapIndex:(NSInteger) index;

@end

@interface RadioTypeListDropMenuView : UIView

@property (nonatomic, copy) NSString * title; // title

@property (nonatomic, assign) NSInteger selectIndex;// 在数组之前给值

@property (nonatomic, strong) NSMutableArray * dataArray;

@property (nonatomic, weak) id<RadioTypeListDropMenuViewDelegate>delegate;

- (void)show;

@end

NS_ASSUME_NONNULL_END
