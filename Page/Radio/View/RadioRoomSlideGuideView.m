//
//  RadioRoomSlideGuideView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/2.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioRoomSlideGuideView.h"

@implementation RadioRoomSlideGuideView

- (IBAction)hide:(UIButton *)sender {
    [self removeFromSuperview];
}


@end
