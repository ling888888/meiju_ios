//
//  RadioRoomBackgroundRunView.h
//  SimpleDate
//
//  Created by 李楠 on 2020/3/4.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LiveModel;

@interface RadioRoomBackgroundRunView : UIView

@property (nonatomic, copy) NSString *radioId;
@property (nonatomic, copy) NSString *radioAvatar;

@property (nonatomic, strong) LiveModel *liveInfo;

@end

NS_ASSUME_NONNULL_END
