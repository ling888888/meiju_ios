//
//  RadioRoomOnlineUserView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/2/28.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioRoomOnlineUserView.h"
#import "RadioRoomOnlineUserViewCell.h"

@interface RadioRoomOnlineUserView ()<UICollectionViewDataSource, UICollectionViewDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet UILabel *onlineNumberLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;

@property (nonatomic, copy) NSArray *dataArray;

@end

@implementation RadioRoomOnlineUserView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"RadioRoomOnlineUserViewCell" bundle:nil] forCellWithReuseIdentifier:@"RadioRoomOnlineUserViewCell"];
    self.flowLayout.itemSize = CGSizeMake(kScreenW, 70);
    
    self.contentViewBottom.constant = -480;
    [self layoutIfNeeded];
}

#pragma mark - Public methods

- (void)showWithDataArray:(NSArray *)dataArray {
    self.dataArray = dataArray;
    self.onlineNumberLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"在线：%ld人", nil), self.dataArray .count];
    [self.collectionView reloadData];
    
    [UIView animateWithDuration:0.25 animations:^{
        self.contentViewBottom.constant = -100 + IphoneX_Bottom;
        [self layoutIfNeeded];
    }];
}

#pragma mark - Event responses

- (IBAction)tapBackground:(UITapGestureRecognizer *)sender {
    [self hide];
}

#pragma mark - Private methods

- (void)hide {
    [UIView animateWithDuration:0.25 animations:^{
        self.contentViewBottom.constant = -480;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RadioRoomOnlineUserViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RadioRoomOnlineUserViewCell" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self hide];
    if ([self.delegate respondsToSelector:@selector(didClickUserInOnlineUserView:)]) {
        [self.delegate didClickUserInOnlineUserView:self.dataArray[indexPath.row]];
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ![touch.view isDescendantOfView:self.contentView];
}

@end
