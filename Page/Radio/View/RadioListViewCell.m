//
//  RadioListViewCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/2/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "RadioListViewCell.h"
#import "RadioModel.h"

@interface RadioListViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *radioImageView;
@property (weak, nonatomic) IBOutlet UILabel *radioTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *radioLiveContentLabel;
@property (weak, nonatomic) IBOutlet UILabel *radioListnerNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *radioOnlineNumLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *radioListenerNumLabelTopCons;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *radioTitleTopCons;
@property (weak, nonatomic) IBOutlet UIImageView *radioListenerIcon;

@end

@implementation RadioListViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setRadioModel:(RadioModel *)radioModel {
    _radioModel = radioModel;
    [self.radioImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,_radioModel.radioImg]]];
    self.radioTitleLabel.text = _radioModel.radioName;
    self.radioOnlineNumLabel.text = _radioModel.radioOnlineNum;
    self.radioListnerNumLabel.text = [SPDCommonTool transformIntegerToBriefStr:[_radioModel.radioListenerNum integerValue]];
    self.radioLiveContentLabel.text = _radioModel.liveContent.notEmpty ? [NSString stringWithFormat:SPDStringWithKey(@"正在直播：%@", nil),_radioModel.liveContent] :@"";
    self.radioListenerNumLabelTopCons.constant = _radioModel.liveContent.notEmpty ? 30 : 10;
    self.radioTitleTopCons.constant = _radioModel.liveContent.notEmpty ? 3 : 15;
}

-(void)setShowCorner:(BOOL)showCorner {
    _showCorner = showCorner;
    if (_showCorner) {
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5,5)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.bounds;
        maskLayer.path = maskPath.CGPath;
        self.layer.mask = maskLayer;
    }
}
@end
