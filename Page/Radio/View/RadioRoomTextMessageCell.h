//
//  RadioRoomTextMessageCell.h
//  SimpleDate
//
//  Created by 李楠 on 2020/3/3.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class SPDTextMessage,LiveRoomTextMessage;

@protocol RadioRoomTextMessageCellDelegate <NSObject>

@optional

- (void)didClickUserInTextMessageCell:(SPDTextMessage *)message;

@end

@interface RadioRoomTextMessageCell : UICollectionViewCell

@property (nonatomic, strong) SPDTextMessage *message;
@property (nonatomic, weak) id<RadioRoomTextMessageCellDelegate> delegate;

@property (nonatomic, assign) BOOL isAnchor;
@property (nonatomic, strong) LiveRoomTextMessage * textMessage; // 语音直播聊天消息
@end

NS_ASSUME_NONNULL_END
