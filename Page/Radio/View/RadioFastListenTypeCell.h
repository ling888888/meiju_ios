//
//  RadioFastListenTypeCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/10.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class RadioTypeModel;

@interface RadioFastListenTypeCell : UICollectionViewCell

@property (nonatomic, strong) RadioTypeModel * model;

@property (nonatomic, assign) BOOL animating;

- (void)invalidateLink;

@end

NS_ASSUME_NONNULL_END
