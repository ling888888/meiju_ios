//
//  LY_PortraitView.h
//  SimpleDate
//
//  Created by 杨宝龙 on 2020/6/2.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LY_Portrait.h"
#import "YYImage.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_PortraitView : UIView

/// 头像
@property(nonatomic, strong) UIImageView *imageView;

/// 头饰
@property(nonatomic, strong) YYAnimatedImageView *headwearImageView;

@property(nonatomic, strong) UIView *borderView;

/// 数据源对象
@property(nonatomic, strong) LY_Portrait *portrait;

///// 头像边框宽度
@property(nonatomic, assign) CGFloat borderWidth;

@property(nonatomic, strong) UIColor* borderColor;

@property(nonatomic, assign) BOOL showBorder;

/// 隐藏头饰
@property(nonatomic, assign) BOOL headwearHidden;


@end

NS_ASSUME_NONNULL_END
