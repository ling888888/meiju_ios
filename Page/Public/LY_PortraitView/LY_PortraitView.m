//
//  LY_PortraitView.m
//  SimpleDate
//
//  Created by 杨宝龙 on 2020/6/2.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_PortraitView.h"
#import "YYWebImage.h"

@interface LY_PortraitView ()

@end

@implementation LY_PortraitView

- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _imageView;
}
- (UIView *)borderView {
    if (!_borderView) {
        _borderView = [UIView new];
        _borderView.backgroundColor = [UIColor whiteColor];
        _borderView.hidden = YES;
    }
    return _borderView;
}

- (YYAnimatedImageView *)headwearImageView {
    if (!_headwearImageView) {
        _headwearImageView = [[YYAnimatedImageView alloc] init];
        _headwearImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _headwearImageView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.borderColor = [UIColor whiteColor];
        self.borderWidth = 3;
        [self setupUI];
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.borderView];
    [self addSubview:self.imageView];
    [self addSubview:self.headwearImageView];
}

- (void)setupUIFrame {
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.mas_width).dividedBy(1.5);
        make.center.mas_equalTo(self);
    }];
    [self.headwearImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
    [self.borderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
        make.top.equalTo(self.imageView.mas_top).offset(-_borderWidth);
        make.bottom.equalTo(self.imageView.mas_bottom).offset(_borderWidth);
        make.leading.equalTo(self.imageView.mas_leading).offset(-_borderWidth);
        make.trailing.equalTo(self.imageView.mas_trailing).offset(_borderWidth);
    }];
}

- (void)setShowBorder:(BOOL)showBorder {
    _showBorder = showBorder;
    self.borderView.hidden = !showBorder;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.imageView.layer.cornerRadius = self.imageView.frame.size.width / 2;
    self.imageView.layer.masksToBounds = true;
    self.borderView.layer.cornerRadius = self.borderView.frame.size.width / 2;
    self.borderView.layer.masksToBounds = true;
}

- (void)setPortrait:(LY_Portrait *)portrait {
    _portrait = portrait;
    
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:portrait.url]] placeholderImage:[UIImage imageNamed:@"img_portrait_placeholder"]];
    self.headwearImageView.hidden = !portrait.headwearUrl.notEmpty;
    
    if (!self.headwearHidden) {
        NSURL *url = [NSURL URLWithString:portrait.headwearUrl];
        self.headwearImageView.yy_imageURL = url;
    }
}

//- (void)setBorderWidth:(CGFloat)borderWidth {
//    self.imageView.layer.borderWidth = borderWidth;
//}

- (void)setBorderColor:(UIColor *)borderColor {
    self.borderView.backgroundColor = borderColor;
}

//- (CGFloat)borderWidth {
//    return self.imageView.layer.borderWidth;
//}
//
//- (CGColorRef)borderColor {
//    return self.imageView.layer.borderColor;
//}

- (void)setHeadwearHidden:(BOOL)headwearHidden {
    _headwearHidden = headwearHidden;
    
    self.headwearImageView.hidden = headwearHidden;
}

@end
