//
//  LY_Portrait.h
//  SimpleDate
//
//  Created by 杨宝龙 on 2020/6/2.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_Portrait : NSObject

@property(nonatomic, copy) NSString *url;

@property(nonatomic, copy) NSString *headwearUrl;

@end

NS_ASSUME_NONNULL_END
