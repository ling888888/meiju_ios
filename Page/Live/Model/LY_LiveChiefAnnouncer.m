//
//  ChiefAnnouncer.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_LiveChiefAnnouncer.h"

@implementation LY_LiveChiefAnnouncer

+ (NSDictionary *)modelCustomPropertyMapper {
   // 映射
    return @{@"portraitUrl":@"avatar",
             @"nickname":@"name",
             @"living":@"isLive",
    };
}

- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    
    LY_Portrait *portrait = [[LY_Portrait alloc] init];
    portrait.url = dic[@"avatar"];
    portrait.headwearUrl = dic[@"headwearWebp"];
    self.portrait = portrait;
    
    return YES;
}



@end
