//
//  LiveUserModel.m
//  SimpleDate
//
//  Created by 李楠 on 2020/3/30.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveUserModel.h"

@implementation LiveUserModel

- (void)setValue:(id)value forKey:(NSString *)key {
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"avatar"]) {
        self.portrait.url = value;
    } else if ([key isEqualToString:@"headwearWebp"]) {
        self.portrait.headwearUrl = value;
    }
    
    if ([key isEqualToString:@"fansLevel"]) {
        self.fansTag.level = value;
    } else if ([key isEqualToString:@"fansName"]) {
        self.fansTag.name = value;
    }
}

- (void)setDiyLabel:(NSDictionary *)diyLabel {
    self.customTag = [LY_CustomTag yy_modelWithDictionary:diyLabel];
}

- (LY_Portrait *)portrait{
    if (!_portrait) {
        _portrait = [[LY_Portrait alloc] init];
    }
    return _portrait;
}

- (LY_CustomTag *)customTag {
    if (!_customTag) {
        _customTag = [[LY_CustomTag alloc] init];
    }
    return _customTag;
}

- (LY_FansTag *)fansTag {
    if (!_fansTag) {
        _fansTag = [[LY_FansTag alloc] init];
    }
    return _fansTag;
}

@end
