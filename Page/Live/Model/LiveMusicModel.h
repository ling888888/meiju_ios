//
//  LiveMusicModel.h
//  SimpleDate
//
//  Created by 李楠 on 2020/3/26.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "CommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveMusicModel : CommonModel

@property (nonatomic, copy) NSString *cover;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *singer;
@property (nonatomic, copy) NSString *musicUrl;

@end

NS_ASSUME_NONNULL_END
