//
//  LY_LiveChiefAnnouncer.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_LiveChiefAnnouncer : NSObject

/// 用户id
@property(nonatomic, copy) NSString *userId;

///// 头像
//@property(nonatomic, copy) NSString *portraitUrl;

/// 头像
@property(nonatomic, strong) LY_Portrait *portrait;

/// 昵称
@property(nonatomic, copy) NSString *nickname;

/// 是否正在直播
@property(nonatomic, assign) BOOL living;

/// 直播id
@property(nonatomic, copy) NSString *liveId;


@end

NS_ASSUME_NONNULL_END
