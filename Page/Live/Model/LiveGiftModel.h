//
//  LiveGiftModel.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "CommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveGiftModel : CommonModel

@property (nonatomic, copy) NSString * giftId;
@property (nonatomic, copy) NSString * giftCover;
@property (nonatomic, copy) NSString * giftName;
@property (nonatomic, copy) NSString * giftCost; // 多少贝壳
@property (nonatomic, assign) BOOL needPlay;
@property (nonatomic, assign) BOOL isCombo; // 连击礼物
@property (nonatomic, copy) NSString * labelColour; // 礼物盒标签颜色
//@property (nonatomic, copy) NSDictionary * labelValue; // 礼物盒标签文案内容
@property (nonatomic, copy) NSString * labelValue; // 礼物盒标签颜色
@property (nonatomic, assign) CGFloat labelValueWidth;

@property (nonatomic, assign)NSInteger giftNumber;//用户拥有该礼物的数量
@property (nonatomic, assign) NSInteger endTime;// 剩余时间差值
@property (nonatomic, copy) NSString * type;//[@"package",@"gift"]
@end

NS_ASSUME_NONNULL_END
