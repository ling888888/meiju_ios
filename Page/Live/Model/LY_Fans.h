//
//  LY_Fans.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/22.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_Fans : NSObject

//用户ID
@property(nonatomic, copy) NSString *userId;

//用户昵称
@property(nonatomic, copy) NSString *nick_name;

//粉丝勋章
@property(nonatomic, copy) NSString *fan_male;

//粉丝等级
@property(nonatomic, assign) NSInteger fan_level;

//亲密度
@property(nonatomic, copy) NSString *intimacy;

//头像
@property(nonatomic, copy) NSString *avatar;

@property(nonatomic, strong) LY_Portrait *portrait;

//排名只有自己信息有
@property(nonatomic, assign) NSInteger rank;

@end

NS_ASSUME_NONNULL_END
