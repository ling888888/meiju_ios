//
//  LY_LiveAudience.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LY_CustomTag.h"
#import "LY_FansTag.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_LiveAudience : NSObject

/// 排名
@property(nonatomic, assign) int ranking;

/// id
@property(nonatomic, copy) NSString *userId;

/// 头像
@property(nonatomic, strong) LY_Portrait *portrait;

/// 昵称
@property(nonatomic, copy) NSString *nickname;
/// 昵称
@property(nonatomic, copy) NSString *userName;
@property(nonatomic, assign) NSInteger score;

/// 性别
@property(nonatomic, assign) NSString *gender;

/// 年龄
@property(nonatomic, copy) NSString *age;

/// 等级
@property(nonatomic, assign) int level;

/// 贵族
@property(nonatomic, copy) NSString *noble;

/// 是否是VIP
@property(nonatomic, assign) BOOL isVip;

/// 靓号，没有返回""
@property(nonatomic, copy) NSString *specialNum;

//
@property(nonatomic, copy) NSString *wealthLevel;

/// 贝壳数
@property(nonatomic, assign) int conchNum;

// 自定义标签对象
@property(nonatomic, strong) LY_CustomTag *customTag;

@property(nonatomic, strong) LY_FansTag *fansTag;

@end

NS_ASSUME_NONNULL_END
