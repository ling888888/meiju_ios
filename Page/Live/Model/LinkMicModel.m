//
//  LinkMicModel.m
//  SimpleDate
//
//  Created by 李楠 on 2020/4/27.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LinkMicModel.h"

@implementation LinkMicModel

- (void)setValue:(id)value forKey:(NSString *)key {
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"avatar"]) {
        self.portrait.url = value;
    } else if ([key isEqualToString:@"headwearWebp"]) {
        self.portrait.headwearUrl = value;
    }
}

- (LY_Portrait *)portrait {
    if (!_portrait) {
        _portrait = [[LY_Portrait alloc] init];
    }
    return _portrait;
}
@end
