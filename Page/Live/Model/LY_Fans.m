//
//  LY_Fans.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/22.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_Fans.h"

@implementation LY_Fans

- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    
    LY_Portrait *portrait = [[LY_Portrait alloc] init];
    portrait.url = dic[@"avatar"];
    portrait.headwearUrl = dic[@"headwearWebp"];
    self.portrait = portrait;
    
    return YES;
}

@end
