//
//  LinkMicNewModel.h
//  SimpleDate
//
//  Created by ling Hou on 2020/8/26.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LinkMicNewModel : CommonModel

@property (nonatomic, copy) NSString *site; // begin from 1
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString * inSilence; // 1 静音
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, strong) LY_Portrait *portrait;


@end

NS_ASSUME_NONNULL_END
