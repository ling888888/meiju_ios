//
//  LiveGiftRecordModel.h
//  SimpleDate
//
//  Created by 李楠 on 2020/3/31.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "CommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveGiftRecordModel : CommonModel

@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *userAvatar;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *timeStr;
@property (nonatomic, copy) NSNumber *giftValue;
@property (nonatomic, copy) NSDictionary *giftName;
@property (nonatomic, copy) NSString *giftNameStr;
@property (nonatomic, copy) NSNumber *sendNumber;

@end

NS_ASSUME_NONNULL_END
