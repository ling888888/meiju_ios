//
//  LiveModel.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "CommonModel.h"
#import "LY_FansTag.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveModel : CommonModel

@property (nonatomic, copy) NSString * userId; //主播的userId
@property (nonatomic, copy) NSString * userName;
@property (nonatomic, assign) NSInteger userNameWidth; // 主播名字宽度

@property (nonatomic, strong) LY_Portrait *portrait;

@property (nonatomic, copy) NSString * avatar;
@property (nonatomic, copy) NSString * liveId;
@property (nonatomic, copy) NSString * liveTitle;
@property (nonatomic, copy) NSString * liveCover;
@property (nonatomic, copy) NSString * listenerNum; // 当前直播在线人数
@property (nonatomic, copy) NSString * followNum; // 关注主播的人数
@property (nonatomic, copy) NSString * liveBackground; // 北京
@property (nonatomic, assign) BOOL isFollow; //本人是不是关注了主播
@property (nonatomic, assign) BOOL isPk; // 是不是正在pk

@property (nonatomic, assign) BOOL isBroadcast;
@property (nonatomic, copy) NSString *lastTime;
@property (nonatomic, copy) NSString *duration;

@property (nonatomic, assign) NSInteger heartRange;
@property (nonatomic, assign) BOOL isFans; //true、false 是否加入粉丝团
@property (nonatomic, copy) NSString *fansName;
@property (nonatomic, assign) BOOL haveFansGroup; //true、false 是否拥有粉丝团
@property (nonatomic, assign) BOOL isLock; //true、false 是否冻结（true：冻结）
@property (nonatomic, assign) BOOL canFinishTask; //true、false 是否可以做任务（true：可以）
@property (nonatomic, assign) NSInteger fansLevel; //粉丝团等级（冻结或者不是粉丝团，返回空字符串）
@property (nonatomic, assign) NSInteger fansNum; //粉丝团成员数量

@property(nonatomic, strong) LY_FansTag *fansTag;

@property (nonatomic, copy) NSString *wealthLevel; // 我的财富等级
@property (nonatomic, assign) BOOL openChat; // 打开连麦开关
@property (nonatomic, assign) BOOL openSwitch; // 用户自动上麦

@property (nonatomic, assign) BOOL hasRedPackage; // 此直播间是否拥有红包

@end

NS_ASSUME_NONNULL_END
