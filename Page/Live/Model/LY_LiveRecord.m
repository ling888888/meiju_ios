//
//  LY_LiveRecord.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_LiveRecord.h"

@implementation LY_LiveRecord

+ (NSDictionary *)modelCustomPropertyMapper {
    // 映射
    return @{@"time":@"dayTime",
             @"giftNumber":@"giftValue",
             @"personNumber":@"listenNum",
             @"followerNumber":@"newFollow"
    };
}


@end
