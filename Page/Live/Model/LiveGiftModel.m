//
//  LiveGiftModel.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveGiftModel.h"
#import "NSString+XXWAddition.h"

@implementation LiveGiftModel

- (void)setValue:(id)value forKey:(NSString *)key {
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"labelValue"]) {
        NSDictionary * dict = [NSString parseJSONStringToNSDictionary:value];
        NSArray * array = [[SPDCommonTool getFamyLanguage] componentsSeparatedByString:@"-"];
        self.labelValue = dict[array[0]];
        self.labelValueWidth = [NSString sizeWithString:self.labelValue andFont:[UIFont mediumFontOfSize:9] andMaxSize:CGSizeMake(CGFLOAT_MAX, 16)].width + 14;
    }else if ([key isEqualToString:@"gifId"]){
        self.giftId = value;
    }
}
@end
