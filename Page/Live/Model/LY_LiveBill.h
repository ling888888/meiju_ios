//
//  LY_LiveBill.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_LiveBill : NSObject

@property(nonatomic, copy) NSString *title;

@property(nonatomic, copy) NSString *time;

@property(nonatomic, copy) NSString *number;


@end

NS_ASSUME_NONNULL_END
