//
//  LiveRedPackModel.m
//  SimpleDate
//
//  Created by ling Hou on 2020/10/10.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveRedPackModel.h"

@implementation LiveRedPackModel

- (void)setValue:(id)value forKey:(NSString *)key {
    if ([key isEqualToString:@"id"]) {
        self._id = value;
    }else{
        [super setValue:value forKey:key];
    }
}

@end
