//
//  LiveModel.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveModel.h"
#import "NSString+XXWAddition.h"
@implementation LiveModel

- (void)setValue:(id)value forKey:(NSString *)key {
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"lastTime"]) {
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
           [format setTimeZone:[NSTimeZone localTimeZone]];
           [format setDateFormat:@"yyyy-MM-dd HH:mm"];
           NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:[value longLongValue] / 1000];
           self.lastTime = [format stringFromDate:date];
    }else if ([key isEqualToString:@"status"]){
        self.isBroadcast = [value integerValue] == 1;
    }
    if ([key isEqualToString:@"avatar"]) {
        self.portrait.url = value;
    } else if ([key isEqualToString:@"headwearWebp"]) {
        self.portrait.headwearUrl = value;
    }
    if ([key isEqualToString:@"fansLevel"]) {
        self.fansTag.level = value;
    } else if ([key isEqualToString:@"fansName"]) {
        self.fansTag.name = value;
    }
}

- (void)setUserName:(NSString *)userName {
    _userName = userName;
    self.userNameWidth = [NSString sizeWithString:_userName andFont:[UIFont systemFontOfSize:12] andMaxSize:CGSizeMake(kScreenW < 375? 50:97, 13)].width;
}

- (LY_Portrait *)portrait{
    if (!_portrait) {
        _portrait = [[LY_Portrait alloc] init];
    }
    return _portrait;
}

- (LY_FansTag *)fansTag {
    if (!_fansTag) {
        _fansTag = [[LY_FansTag alloc] init];
    }
    return _fansTag;
}

@end
