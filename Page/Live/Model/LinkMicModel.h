//
//  LinkMicModel.h
//  SimpleDate
//
//  Created by 李楠 on 2020/4/27.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "CommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LinkMicModel : CommonModel

@property (nonatomic, copy) NSString *_id;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, strong) LY_Portrait *portrait;
@property (nonatomic, copy) NSString *nick_name;
@property (nonatomic, assign) NSTimeInterval startTimeStamp;

@end

NS_ASSUME_NONNULL_END
