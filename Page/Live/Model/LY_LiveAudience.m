//
//  LY_LiveAudience.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_LiveAudience.h"

@implementation LY_LiveAudience

+ (NSDictionary *)modelCustomPropertyMapper {
   // 映射
    return @{@"portraitUrl":@"avatar",
             @"nickname":@"name",
             @"customTag":@"diyLabel",
//             @""
    };
}

// 声明自定义类参数类型
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
        @"customTag":[LY_CustomTag class],
        @"fansTag":[LY_FansTag class],
    };
}

- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    
    LY_Portrait *portrait = [[LY_Portrait alloc] init];
    portrait.url = dic[@"avatar"];
    portrait.headwearUrl = dic[@"headwearWebp"];
    if (!portrait.headwearUrl.notEmpty) {
        portrait.headwearUrl = dic[@"headwear"];
    }
    self.portrait = portrait;
    
    /*
     if ([key isEqualToString:@"fansLevel"]) {
         self.fansTag.level = value;
     } else if ([key isEqualToString:@"fansNum"]) {
         self.fansTag.name = value;
     }
     */
    
    return YES;
}

//name:"",//用户名
//avatar:"",//头像
//
//
//noble: "", // 贵族
//isVip:"true/false",//是否是vip
//specialNum:"",//靓号，没有返回""

@end
