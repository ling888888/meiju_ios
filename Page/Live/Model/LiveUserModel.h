//
//  LiveUserModel.h
//  SimpleDate
//
//  Created by 李楠 on 2020/3/30.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "CommonModel.h"
#import "LY_FansTag.h"
#import "LY_CustomTag.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveUserModel : CommonModel

@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, strong) LY_Portrait *portrait;
@property (nonatomic, strong) LY_FansTag *fansTag;
@property (nonatomic, strong) LY_CustomTag *customTag;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSString *age;
@property (nonatomic, copy) NSString *level;
@property (nonatomic, copy) NSString *noble;
@property (nonatomic, copy) NSString *fansLevel;
@property (nonatomic, copy) NSString *fansName;
@property (nonatomic, copy) NSString *followNum;
@property (nonatomic, copy) NSString *specialNum;
@property (nonatomic, copy) NSString *personalDesc;
@property (nonatomic, copy) NSString *heartRange;
@property (nonatomic, copy) NSString *fansNum;
@property (nonatomic, copy) NSString *wealthLevel;
@property (nonatomic, copy) NSString *upgradeNum;
@property (nonatomic, copy) NSString *nextLevelNum;
@property (nonatomic, copy) NSString *weekContribution;
@property (nonatomic, assign) BOOL isVip;
@property (nonatomic, assign) BOOL canChat;
@property (nonatomic, assign) BOOL isFollow;
@property (nonatomic, assign) BOOL isFriend;
@property (nonatomic, assign) BOOL isAnchor;

@end

NS_ASSUME_NONNULL_END
