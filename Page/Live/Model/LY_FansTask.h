//
//  LY_FansTask.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/23.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_FansTask : NSObject

// 任务编码（liveListen：听直播；liveSendMsg：发送消息；liveShare：分享直播间；liveSendGift：送礼物）
@property(nonatomic, copy) NSString *code;

// 任务封面
@property(nonatomic, copy) NSString *cover;

// 任务标题
@property(nonatomic, copy) NSString *title;

// 任务描述
@property(nonatomic, copy) NSString *desc;

// 任务类型（0：按次数；1：按上限）
@property(nonatomic, copy) NSString *type;

// 最多完成的数量
@property(nonatomic, copy) NSString *number;

// 用户完成的数量
@property(nonatomic, copy) NSString *finishNum;

// 完成任务可以获得的亲密度（按上限的该字段为获得亲密度上限）
@property(nonatomic, copy) NSString *intimacy;

// 按钮文案
@property(nonatomic, copy) NSString *buttonText;

// 任务完成情况（0：未完成；1：完成）
@property(nonatomic, copy) NSString *status;

@end

NS_ASSUME_NONNULL_END
