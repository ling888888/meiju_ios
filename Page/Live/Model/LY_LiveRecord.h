//
//  LY_LiveRecord.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_LiveRecord : NSObject


@property(nonatomic, copy) NSString *time;

@property(nonatomic, copy) NSString *duration;

@property(nonatomic, copy) NSString *giftNumber;

@property(nonatomic, copy) NSString *personNumber;

@property(nonatomic, copy) NSString *followerNumber;

@end

NS_ASSUME_NONNULL_END
