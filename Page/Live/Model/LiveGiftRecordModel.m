//
//  LiveGiftRecordModel.m
//  SimpleDate
//
//  Created by 李楠 on 2020/3/31.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveGiftRecordModel.h"

@implementation LiveGiftRecordModel

- (void)setGiftName:(NSDictionary *)giftName {
    _giftName = giftName;
    
    NSString *currentLanguage = [[SPDLanguageTool sharedInstance].currentLanguage substringToIndex:2];
    self.giftNameStr = _giftName[currentLanguage];
}

- (void)setTimeStr:(NSString *)timeStr {
    NSDateFormatter *dateFormater = [NSDateFormatter new];
    dateFormater.dateFormat = @"HH:mm:ss";
    dateFormater.timeZone = [NSTimeZone localTimeZone];
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:timeStr.longLongValue / 1000];
    _timeStr = [dateFormater stringFromDate:date];
}

@end
