//
//  LiveRedPackModel.h
//  SimpleDate
//
//  Created by ling Hou on 2020/10/10.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "CommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveRedPackModel : CommonModel

@property (nonatomic, copy) NSString * _id; // 红包id
@property (nonatomic, copy) NSString * type; //0普通红包，1口令红包
@property (nonatomic, copy) NSString * msg;//"口令消息内容"
@property (nonatomic, copy) NSString * avatar;
@property (nonatomic, copy) NSString * userName;

@end

NS_ASSUME_NONNULL_END
