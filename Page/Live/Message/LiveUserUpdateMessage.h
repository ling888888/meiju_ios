//
//  LiveUserUpdateMessage.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/27.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveUserUpdateMessage : RCMessageContent

/**
 rankFirst: {
 "userId":"",
 "avatar":"",
 "nickName":""
 }
 */

@property (nonatomic, copy) NSDictionary * rankFirst;
@property (nonatomic, copy) NSDictionary * rankSecond;
@property (nonatomic, copy) NSDictionary * rankThird;
@property (nonatomic, copy) NSString * userNum; // 参与人数
@property (nonatomic, copy) NSString * onlineNum; // 在线人数

@end

NS_ASSUME_NONNULL_END
