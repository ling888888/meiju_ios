//
//  LiveApplyMicMessage.h
//  SimpleDate
//
//  Created by ling Hou on 2020/8/27.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>
#import "LY_LiveBaseMessageContent.h"

NS_ASSUME_NONNULL_BEGIN

// 主播收到连麦申请消息

@interface LiveApplyMicMessage : LY_LiveBaseMessageContent

@property (nonatomic, copy) NSString * userName;
@property (nonatomic, copy) NSString * userId;
@property (nonatomic, copy) NSString * headImg;
@property (nonatomic, copy) NSString * position;

@end

NS_ASSUME_NONNULL_END
