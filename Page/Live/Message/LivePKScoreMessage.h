//
//  LivePKScoreMessage.h
//  SimpleDate
//
//  Created by ling Hou on 2020/6/29.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

NS_ASSUME_NONNULL_BEGIN

@interface LivePKScoreMessage : RCMessageContent

@property (nonatomic, copy) NSArray * opponentSupportIds;
@property (nonatomic, assign) NSInteger opponentScore;
@property (nonatomic, copy) NSString *  opponentLiveId;

@property (nonatomic, copy) NSArray * mySupportIds;
@property (nonatomic, assign) NSInteger myScore;
@property (nonatomic, copy) NSString *  myLiveId;// 这个明明服务器不知道哪个是我的 哪个是别人的 我不知道他们为啥这么取名字。。。

@end

NS_ASSUME_NONNULL_END
