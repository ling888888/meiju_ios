//
//  LivePKInviteMessage.m
//  SimpleDate
//
//  Created by ling Hou on 2020/6/23.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LivePKInviteMessage.h"
#import "NSObject+Coding.h"

@implementation LivePKInviteMessage

 // 消息的类型名
+ (NSString *)getObjectName {
    return @"Live:InvitePk";
}

// 消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return MessagePersistent_STATUS;
}

// NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        [self hl_decodeWithDeCoder:aDecoder];
    }
    return self;
}

// NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [self hl_encodeWithCoder:aCoder];
}

// 将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDic = [NSMutableDictionary new];
    [dataDic setValue:@(self.pkId) forKey:@"pkId"];
    [dataDic setValue:self.userId forKey:@"userId"];
    [dataDic setValue:self.userName forKey:@"userName"];
    [dataDic setValue:self.liveId forKey:@"liveId"];

    if (self.senderUserInfo) {
        NSMutableDictionary *userInfoDic = [NSMutableDictionary new];
        if (self.senderUserInfo.name) {
            [userInfoDic setObject:self.senderUserInfo.name forKeyedSubscript:@"name"];
        }
        if (self.senderUserInfo.portraitUri) {
            [userInfoDic setObject:self.senderUserInfo.portraitUri forKeyedSubscript:@"icon"];
        }
        if (self.senderUserInfo.userId) {
            [userInfoDic setObject:self.senderUserInfo.userId forKeyedSubscript:@"id"];
        }
        [dataDic setObject:userInfoDic forKey:@"user"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDic options:kNilOptions error:nil];
    return data;
}

// 将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        if (dictionary) {
            self.pkId = [dictionary[@"pkId"] integerValue];
            self.liveId = dictionary[@"liveId"];
            self.userName = dictionary[@"userName"];
            self.userId = dictionary[@"userId"];
            NSDictionary *userinfoDic = dictionary[@"user"];
            [self decodeUserInfo:userinfoDic];
        }
    }
}

// 会话列表中显示的摘要
- (NSString *)conversationDigest {
    return @"";
}


@end
