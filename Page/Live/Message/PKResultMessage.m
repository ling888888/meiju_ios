//
//  PKResultMessage.m
//  SimpleDate
//
//  Created by ling Hou on 2020/7/1.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PKResultMessage.h"
#import "NSObject+Coding.h"

@implementation PKResultMessage

// 消息的类型名
+ (NSString *)getObjectName {
    return @"Live:PkResultMsg";
}

// 消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return MessagePersistent_STATUS;
}

// NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        [self hl_decodeWithDeCoder:aDecoder];
    }
    return self;
}

// NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [self hl_encodeWithCoder:aCoder];
}

// 将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        if (dictionary) {
            NSLog(@"[pk live]--- %@",dictionary);
            self.isKill = [dictionary[@"isKill"] boolValue];
            self.isBalance = [dictionary[@"isBalance"] boolValue];
            self.continuityWinTimes = [dictionary[@"continuityWinTimes"] stringValue];
            
            self.winLiveId = dictionary[@"winLiveId"];
            self.launchUId = dictionary[@"launchId"];
            self.launchScore = [dictionary[@"launchScore"] integerValue];
            self.launchMvpUserId = dictionary[@"launchMvpUserId"];
            self.launchMvpUserAvatar = dictionary[@"launchMvpUserAvatar"];
            self.launchMvpUserName = dictionary[@"launchMvpUserName"];
            self.launchMvpScore = [dictionary[@"launchMvpScore"] integerValue];
            self.type = [dictionary[@"type"] intValue];
            self.acceptId = dictionary[@"acceptId"];
            self.acceptScore = [dictionary[@"acceptScore"] integerValue];
            self.acceptMvpUserId = dictionary[@"acceptMvpUserId"];
            self.acceptMvpScore = [dictionary[@"acceptMvpScore"] integerValue];
            self.acceptMvpUserName = dictionary[@"acceptMvpUserName"];
            self.acceptMvpUserAvatar = dictionary[@"acceptMvpUserAvatar"];
            NSDictionary *userinfoDic = dictionary[@"user"];
            [self decodeUserInfo:userinfoDic];
            
        }
    }
}

@end
