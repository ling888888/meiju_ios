//
//  LivePKScoreMessage.m
//  SimpleDate
//
//  Created by ling Hou on 2020/6/29.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LivePKScoreMessage.h"
#import "NSObject+Coding.h"

@implementation LivePKScoreMessage

// 消息的类型名
+ (NSString *)getObjectName {
    return @"Live:PkScoreMsg";
}

// 消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return MessagePersistent_STATUS;
}

// NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        [self hl_decodeWithDeCoder:aDecoder];
    }
    return self;
}

// NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [self hl_encodeWithCoder:aCoder];
}

// 将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDic = [NSMutableDictionary new];

    [dataDic setValue:self.opponentSupportIds forKey:@"opponentSupportIds"];
    [dataDic setValue:@(self.opponentScore) forKey:@"opponentScore"];
    [dataDic setValue:self.opponentLiveId forKey:@"opponentLiveId"];
    
    [dataDic setValue:self.myLiveId forKey:@"myLiveId"];
    [dataDic setValue:@(self.myScore) forKey:@"myScore"];
    [dataDic setValue:self.mySupportIds forKey:@"mySupportId"];

    if (self.senderUserInfo) {
        NSMutableDictionary *userInfoDic = [NSMutableDictionary new];
        if (self.senderUserInfo.name) {
            [userInfoDic setObject:self.senderUserInfo.name forKeyedSubscript:@"name"];
        }
        if (self.senderUserInfo.portraitUri) {
            [userInfoDic setObject:self.senderUserInfo.portraitUri forKeyedSubscript:@"icon"];
        }
        if (self.senderUserInfo.userId) {
            [userInfoDic setObject:self.senderUserInfo.userId forKeyedSubscript:@"id"];
        }
        [dataDic setObject:userInfoDic forKey:@"user"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDic options:kNilOptions error:nil];
    return data;
}

// 将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        if (dictionary) {
            self.opponentSupportIds = dictionary[@"opponentSupportIds"];
            self.opponentScore = [dictionary[@"opponentScore"] integerValue];
            self.opponentLiveId = dictionary[@"opponentLiveId"];
            
            self.mySupportIds = dictionary[@"mySupportIds"];
            self.myScore = [dictionary[@"myScore"] integerValue];
            self.myLiveId = dictionary[@"myLiveId"];
            
            NSDictionary *userinfoDic = dictionary[@"user"];
            [self decodeUserInfo:userinfoDic];
        }
    }
}

@end
