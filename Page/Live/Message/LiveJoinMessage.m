//
//  LiveJoinMessage.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/24.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveJoinMessage.h"
#import "NSObject+Coding.h"

@implementation LiveJoinMessage

///消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return    MessagePersistent_ISCOUNTED;
}

///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [self encodeBaseDict];
    if (self.specialNum) {
        [dataDict setObject:self.specialNum forKey:@"specialNum"];
    }
    if (self.medal) {
        [dataDict setObject:self.medal forKey:@"medal"];
    }
    if (self.isVip) {
        [dataDict setObject:self.isVip forKey:@"isVip"];
    }
    if (self.userName) {
        [dataDict setObject:self.userName forKey:@"userName"];
    }
    if (self.userId) {
        [dataDict setObject:self.userId forKey:@"userId"];
    }
    if (self.isFans) {
        [dataDict setObject:[NSString stringWithFormat:@"%d", self.isFans] forKey:@"isFans"];
    }
    if (self.noble) {
        [dataDict setObject:self.noble forKey:@"nobleName"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        
        __autoreleasing NSError *error = nil;
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        if (dictionary) {
            [self decodeWithDict:dictionary];
            self.userIdentity = LY_MessageSenderUserIdentityAnchor;
            if (dictionary[@"isVip"] && [dictionary[@"isVip"] isKindOfClass:[NSString class]]) {
                self.isVip = dictionary[@"isVip"];
            }

            if (dictionary[@"specialNum"]) {
                self.specialNum = dictionary[@"specialNum"];
            }
            if (dictionary[@"medal"]) {
                self.medal = dictionary[@"medal"];
            }
            if (dictionary[@"userId"]) {
                self.userId = dictionary[@"userId"];
            }
            if (dictionary[@"userName"]) {
                self.userName = dictionary[@"userName"];
            }
            if (dictionary[@"isFans"]) {
                self.isFans = [dictionary[@"isFans"] boolValue];
            }
            if (dictionary[@"wealthLevel"]) {
                self.wealthLevel = [NSString stringWithFormat:@"%@",dictionary[@"wealthLevel"]];
            }
            if (dictionary[@"nobleName"]) {
                self.noble = dictionary[@"nobleName"];
            }
            self.avatar = dictionary[@"avatar"]?:@"";
            self.portrait.url = self.avatar;
            
            if (dictionary[@"vehicleId"]) {
                self.vehicleId = dictionary[@"vehicleId"];
            }
        }
    }
}

///消息的类型名
+ (NSString *)getObjectName {
    return @"Live:JoinMsg";
}

- (NSValue *)cellSize {
    return [NSValue valueWithCGSize: CGSizeMake(kScreenW, 25)];
}


@end
