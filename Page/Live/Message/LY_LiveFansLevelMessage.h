//
//  LY_LiveFansLevelMessage.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/26.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_LiveFansLevelMessage : RCMessageContent

// 粉丝用户id
@property(nonatomic, copy) NSString *fansId;

// 粉丝等级
@property (nonatomic, assign) NSInteger fansLevel;

@end

NS_ASSUME_NONNULL_END
