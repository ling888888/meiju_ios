//
//  LiveLuckyNumberMessage.h
//  SimpleDate
//
//  Created by ling Hou on 2020/9/22.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_LiveBaseMessageContent.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveLuckyNumberMessage : LY_LiveBaseMessageContent


@property (nonatomic, copy) NSString *content;

@property (nonatomic, copy) NSString *age;

@property (nonatomic, copy) NSString *gender;

@property (nonatomic, copy) NSString * userLevel;// 个人等级

@property (nonatomic, copy) NSString * medal;// 勋章 url

@property (nonatomic, copy) NSString * isVip; // true false

@property (nonatomic, copy) NSString *specialNum; // 靓号

@property (nonatomic, strong) NSString *mentionedType; // @类型 "1": @全部用户, "2": @部分用户

@property (nonatomic, strong) NSArray *mentionedList; // @用户列表 [{"user_id": "", "nick_name": "", "avatar": "" // 地址全称}]

@property (nonatomic, copy) NSDictionary *fans; // 粉丝

@property (nonatomic, copy) NSString *fansName; // 粉丝

@property (nonatomic, copy) NSString *fansLevel; // 粉丝

//@property (nonatomic, copy) NSString * wealthLevel; // 财富等级

//@property (nonatomic, copy) NSString * noble;// 爵位 [@"baron",@"viscount",@"count"....]
/*
 bubble :{
 @"usecover_ios": @"",
 @"usecover_ios_ar": @"",
 @"usecover_android": @"",
 @"usecover_android_ar": @"",
 “text_color”: @""
 }
 */
@property (nonatomic, copy) NSDictionary * bubble; // qipao

@property (nonatomic, copy) NSString *lucky_number; // 幸运数字


@property (nonatomic, assign) CGFloat cellHeight; // cell的高度

@property (nonatomic, weak) NSValue *cellSize;

@end

NS_ASSUME_NONNULL_END
