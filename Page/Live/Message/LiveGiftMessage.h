//
//  LiveGiftMessage.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_LiveBaseMessageContent.h"

NS_ASSUME_NONNULL_BEGIN

// 直播间赠送礼物消息
@interface LiveGiftMessage : LY_LiveBaseMessageContent// senderuseronfo

@property (nonatomic, copy) NSString *senderUserId;
@property (nonatomic, copy) NSString *senderUserName;
@property (nonatomic, copy) NSString *senderUserAvatar;

@property (nonatomic, copy) NSString *giftId; // 礼物的名字
@property (nonatomic, copy) NSDictionary *giftName; // 礼物的名字 {@"zh":@"",@"ar":@"",@"en":@""}
@property (nonatomic, copy) NSString *giftCover; // 整个的url 如果url是svga结尾的 就要播放svga 如果不是的话就是展示png
@property (nonatomic, copy) NSString *num;

@property (nonatomic, copy) NSString *giftAnimationUrl; //svga
@property (nonatomic, copy) NSString *age;
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSString *userLevel;
@property (nonatomic, copy) NSString *medal;
@property (nonatomic, assign) BOOL isVip; 
@property (nonatomic, copy) NSString *specialNum;
@property (nonatomic, copy) NSString *giftSvgaUrl; //小SVGA图  如果为空则展示giftCover
@property (nonatomic, assign) NSInteger heartRange;
@property (nonatomic, copy) NSString *fansName;
@property (nonatomic, copy) NSString *fansLevel;
@property (nonatomic, copy) NSString * wealthLevel; // 财富等级
@property (nonatomic, assign) BOOL isCombo; // 是否是礼物连击
@property (nonatomic, copy) NSString *msgId;  //消息唯一编号，仅连击礼物存在该字段
@property (nonatomic, assign) NSInteger comboValue; //本次送礼的连击次数，累计第几次送礼

@property (nonatomic, assign) NSInteger isAdmin; // 直播间管理员

@property (nonatomic, assign) CGFloat contentHeight;
@property (nonatomic, copy) NSValue* cellSize;

@end

NS_ASSUME_NONNULL_END
