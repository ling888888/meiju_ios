//
//  LY_LiveFansInviteMessage.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/23.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>
#import "LY_LiveBaseMessageContent.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_LiveFansInviteMessage : LY_LiveBaseMessageContent

@property(nonatomic, copy) NSString *userId;

@property(nonatomic, copy) NSString *userName;

//@property(nonatomic, copy) NSString *wealthLevel;

// 年龄
@property (nonatomic, copy) NSString *age;

// 性别
@property (nonatomic, copy) NSString *gender;

// 个人等级
@property (nonatomic, copy) NSString * userLevel;

// 勋章 url
//@property (nonatomic, copy) NSString * medal;

// 是否是vip
@property (nonatomic, copy) NSString * isVip;

// 靓号
@property (nonatomic, copy) NSString *specialNum;

// 文本内容
@property(nonatomic, copy) NSString *content;

// 按钮类型
@property(nonatomic, copy) NSString *type;

// 是否是主播
@property(nonatomic, copy) NSString *identity;

// 内容高度
@property (nonatomic, assign) CGFloat contentHeight;

@property (nonatomic, copy) NSValue * cellSize;

@end

NS_ASSUME_NONNULL_END

