//
//  LiveWealthLevelMessage.m
//  SimpleDate
//
//  Created by ling Hou on 2020/5/9.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveWealthLevelMessage.h"
#import "NSObject+Coding.h"

@implementation LiveWealthLevelMessage

///消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return MessagePersistent_ISCOUNTED;
}

/// NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        [self hl_decodeWithDeCoder:aDecoder];
    }
    return self;
}


/// NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [self hl_encodeWithCoder:aCoder];
}

///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [super encodeBaseDict];
    if (self.content) {
        [dataDict setObject:self.content forKey:@"ioscontent"];
    }
    if (self.userName) {
        [dataDict setObject:self.userName forKey:@"name"];
    }
//    if (self.type) {
//        [dataDict setObject:self.type forKey:@"type"];
//    }
    if (self.age) {
        [dataDict setObject:self.age forKey:@"age"];
    }
    if (self.gender) {
        [dataDict setObject:self.gender forKey:@"gender"];
    }
//    if (self.userLevel) {
//        [dataDict setObject:self.userLevel forKey:@"userLevel"];
//    }
    if (self.medal) {
        [dataDict setObject:self.medal forKey:@"medal"];
    }
//    if (self.isVip) {
//        [dataDict setObject:self.isVip forKey:@"isVip"];
//    }
//    if (self.specialNum) {
//        [dataDict setObject:self.specialNum forKey:@"specialNum"];
//    }
    if (self.wealthLevel) {
        [dataDict setObject:self.wealthLevel forKey:@"wealthLevel"];
    }
    if (self.senderUserInfo) {
        NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc] init];
        if (self.senderUserInfo.name) {
            [userInfoDic setObject:self.senderUserInfo.name
                 forKeyedSubscript:@"name"];
        }
        if (self.senderUserInfo.portraitUri) {
            [userInfoDic setObject:self.senderUserInfo.portraitUri
                 forKeyedSubscript:@"icon"];
        }
        if (self.senderUserInfo.userId) {
            [userInfoDic setObject:self.senderUserInfo.userId
                 forKeyedSubscript:@"id"];
        }
        [dataDict setObject:userInfoDic forKey:@"user"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        
        if (dictionary) {
            [self decodeWithDict:dictionary];
            self.userIdentity = LY_MessageSenderUserIdentityAnchor;
            NSArray * array = [[SPDCommonTool getFamyLanguage] componentsSeparatedByString:@"-"];
            if ([dictionary[@"content"] isKindOfClass:[NSDictionary class]]) {
                self.content = dictionary[@"content"][array[0]];
            }
          
            self.userName = dictionary[@"name"];
//            self.type = dictionary[@"type"];
            if (dictionary[@"age"]) {
                self.age = dictionary[@"age"];
            }
            if (dictionary[@"gender"]) {
                self.gender = dictionary[@"gender"];
            }
//            if (dictionary[@"userLevel"]) {
//                self.userLevel = dictionary[@"userLevel"];
//            }
            if (dictionary[@"medal"]) {
                self.medal = dictionary[@"medal"];
            }
//            if (dictionary[@"isVip"]) {
//                self.isVip = dictionary[@"isVip"];
//            }
//            if (dictionary[@"specialNum"]) {
//                self.specialNum = dictionary[@"specialNum"];
//            }
            if (dictionary[@"wealthLevel"]) {
                self.wealthLevel = dictionary[@"wealthLevel"];
            }
            if (dictionary[@"userWealthLevel"]) {
                self.userWealthLevel = dictionary[@"userWealthLevel"];
            }
            if (dictionary[@"userId"]) {
                self.userId = dictionary[@"userId"];
            }
        }
    }
}

/// 会话列表中显示的摘要
- (NSString *)conversationDigest {
    return self.content;
}

///消息的类型名
+ (NSString *)getObjectName {
    return @"Live:WealthLevel";
}

- (void)setContent:(NSString *)content {
    _content = content;
    if ([_content isKindOfClass:[NSString class]]) {
        CGFloat heigt = [content sizeWithFont:[UIFont mediumFontOfSize:13] andMaxSize:CGSizeMake(BASECONTENTWIDTH, CGFLOAT_MAX)].height;
        heigt += 25 + 8 + 8 + 7;
        self.contentHeight = heigt;
    }
}

- (NSValue *)cellSize {
    return [NSValue valueWithCGSize:CGSizeMake(kScreenW, self.contentHeight)];
}

@end
