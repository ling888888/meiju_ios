//
//  LivePKTimeMessage.h
//  SimpleDate
//
//  Created by ling Hou on 2020/6/29.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

NS_ASSUME_NONNULL_BEGIN

@interface LivePKTimeMessage : RCMessageContent

@property (nonatomic, assign) NSInteger time;
@property (nonatomic, assign) double startTime;
@property (nonatomic, assign) NSInteger round;
@property (nonatomic, copy) NSString * pkId;

@end

NS_ASSUME_NONNULL_END
