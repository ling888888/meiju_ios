//
//  LiveCancelInvitePk.h
//  SimpleDate
//
//  Created by ling Hou on 2020/9/10.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveCancelInvitePk : RCMessageContent

@property (nonatomic, copy) NSString * pkId;
@property (nonatomic, copy) NSString * userId;

@end

NS_ASSUME_NONNULL_END
