//
//  LiveRoomTextMessage.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/24.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomTextMessage.h"
#import "NSObject+Coding.h"

@implementation LiveRoomTextMessage

///消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return    MessagePersistent_ISCOUNTED;
}

///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [super encodeBaseDict];
    [dataDict setObject:self.content forKey:@"content"];
    if (self.age) {
        [dataDict setObject:self.age forKey:@"age"];
    }
    if (self.gender) {
        [dataDict setObject:self.gender forKey:@"gender"];
    }
    if (self.userLevel) {
        [dataDict setObject:self.userLevel forKey:@"userLevel"];
    }
    if (self.specialNum) {
        [dataDict setObject:self.specialNum forKey:@"specialNum"];
    }
    if (self.medal) {
        [dataDict setObject:self.medal forKey:@"medal"];
    }
    if (self.isVip) {
        [dataDict setObject:self.isVip forKey:@"isVip"];
    }
    if (self.mentionedList) {
        [dataDict setObject:self.mentionedList forKey:@"mentionedList"];
    }
    if (self.mentionedType) {
        [dataDict setObject:self.mentionedType forKey:@"mentionedType"];
    }
    if (self.fansLevel.notEmpty && self.fansName.notEmpty) {
        NSDictionary * dict = @{
            @"fansName":self.fansName,
            @"fansLevel": self.fansLevel
        };
        [dataDict setObject:dict forKey:@"fans"];
    }
    if (self.fans) {
        [dataDict setObject:self.fans forKey:@"fans"];
        self.fansLevel = self.fans[@"fansLevel"];
        self.fansName = self.fans[@"fansName"];
    }
    [dataDict setObject:@(self.isAdmin) forKey:@"isAdmin"];

    if (self.bubble) {
        [dataDict setObject:self.bubble forKey:@"bubble"];
    }

    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        if (dictionary) {
            [self decodeWithDict:dictionary];
            self.content = dictionary[@"content"];
            if (dictionary[@"age"]) {
                self.age = dictionary[@"age"];
            }
            if (dictionary[@"gender"]) {
                self.gender = dictionary[@"gender"];
            }
            if (dictionary[@"isVip"]) {
                self.isVip = dictionary[@"isVip"];
            }
            self.isAdmin = [dictionary[@"isAdmin"] boolValue];

            if (dictionary[@"userLevel"]) {
                self.userLevel = dictionary[@"userLevel"];
            }
            if (dictionary[@"specialNum"]) {
                self.specialNum = dictionary[@"specialNum"];
            }
            if (dictionary[@"medal"]) {
                self.medal = dictionary[@"medal"];
            }
            if (dictionary[@"mentionedType"]) {
                self.mentionedType = dictionary[@"mentionedType"];
            }
            if (dictionary[@"mentionedList"]) {
                self.mentionedList = dictionary[@"mentionedList"];
            }
//            if (dictionary[@"wealthLevel"]) {
//                self.wealthLevel = dictionary[@"wealthLevel"];
//            }
            if (dictionary[@"fans"]) {
                self.fans = dictionary[@"fans"];
                if ([self.fans isKindOfClass:[NSDictionary class]]) {
                    self.fansName = self.fans[@"fansName"];
                    self.fansLevel = self.fans[@"fansLevel"];
                }
            }
//            if (dictionary[@"noble"]) {
//                self.noble = dictionary[@"noble"];
//            }
            if (dictionary[@"bubble"] && [dictionary[@"bubble"] isKindOfClass:[NSDictionary class]]) {
                self.bubble = dictionary[@"bubble"];
            }
//            NSDictionary *userinfoDic = dictionary[@"user"];
//            [self decodeUserInfo:userinfoDic];

        }
    }
}

/// 会话列表中显示的摘要
- (NSString *)conversationDigest {
    return self.content;
}

///消息的类型名
+ (NSString *)getObjectName {
    return @"Live:TextMsg";
}

- (void)setContent:(NSString *)content {
    _content = content;
    
    self.cellHeight = [content sizeWithFont:[UIFont mediumFontOfSize:13] andMaxSize:CGSizeMake(BASECONTENTWIDTH, CGFLOAT_MAX)].height;
    
    self.cellHeight += 25 + 8 + 8 + 7;
//    UIFont *font = ;
//    CGFloat height = [_content boundingRectWithSize:
//                                        options:NSStringDrawingUsesLineFragmentOrigin
//                                     attributes:@{NSFontAttributeName: font}
//                                        context:nil].size.height;
//    self.cellHeight = ceilf(height) + 25 + 8 + 8 + 7;
}

- (NSValue *)cellSize {
    return [NSValue valueWithCGSize:CGSizeMake(kScreenW, self.cellHeight)];
}


@end
