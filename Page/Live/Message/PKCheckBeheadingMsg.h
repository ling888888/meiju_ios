//
//  PKCheckBeheadingMsg.h
//  SimpleDate
//
//  Created by ling Hou on 2020/6/30.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

NS_ASSUME_NONNULL_BEGIN

@interface PKCheckBeheadingMsg : RCMessageContent

@property (nonatomic, assign) BOOL isKill;
@property (nonatomic, assign) int time;
@property (nonatomic, assign) double beheadingBeganTime;
@property (nonatomic, copy) NSString * leadLiveId;
@property (nonatomic, assign) int beheadingScore; // 斩杀线

@end

NS_ASSUME_NONNULL_END
