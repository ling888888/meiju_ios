//
//  LiveRedPackMessage.h
//  SimpleDate
//
//  Created by ling Hou on 2020/10/13.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveRedPackMessage : RCMessageContent

@property (nonatomic, copy) NSString *_id;

@property (nonatomic, copy) NSString *type; // type = 0 

@property (nonatomic, copy) NSString *msg;

@property (nonatomic, copy) NSString * action; // 【“finish”】

@end

NS_ASSUME_NONNULL_END
