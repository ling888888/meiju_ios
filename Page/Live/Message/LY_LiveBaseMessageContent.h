//
//  LY_LiveBaseMessageContent.h
//  SimpleDate
//
//  Created by 杨宝龙 on 2020/6/2.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>
#import "LY_CustomTag.h"
#import "LY_FansTag.h"

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    LY_MessageSenderUserIdentityAudience,
    LY_MessageSenderUserIdentityAnchor,
    LY_MessageSenderUserIdentitySystem,
} LY_MessageSenderUserIdentity;

#define BASECONTENTPADDING 10

#define BASECONTENTWIDTH kScreenW - 62 - 8 - 8 - 94

@interface LY_LiveBaseMessageContent : RCMessageContent

// 头像对象
@property(nonatomic, strong) LY_Portrait *portrait;

// 自定义标签对象
@property(nonatomic, strong) LY_CustomTag *customTag;

// 粉丝标签
@property(nonatomic, strong) LY_FansTag *fansTag;

//
@property(nonatomic, copy) NSString *noble;

// 贵族标签
@property(nonatomic, copy) NSString *medalUrl;

// 消费等级标签
@property(nonatomic, copy) NSString *wealthLevel;

@property (nonatomic, assign) BOOL isAdmin;

// 消息发送者身份（主播、观众），默认是观众
@property(nonatomic, assign) LY_MessageSenderUserIdentity userIdentity;


- (NSMutableDictionary *)encodeBaseDict;


- (void)decodeWithDict:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
