//
//  LiveStateMessage.h
//  SimpleDate
//
//  Created by 李楠 on 2020/4/29.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveStateMessage : RCMessageContent

@property (nonatomic, assign) NSInteger status;
@property (nonatomic, copy) NSString *lastTime;
@property (nonatomic, copy) NSString *duration;

@end

NS_ASSUME_NONNULL_END
