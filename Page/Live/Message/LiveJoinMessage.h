//
//  LiveJoinMessage.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/24.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>
#import "LY_LiveBaseMessageContent.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveJoinMessage : LY_LiveBaseMessageContent

//@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *medal; // 整个的url
@property (nonatomic, copy) NSString *isVip; // true/false
@property (nonatomic, copy) NSString *specialNum;
@property (nonatomic, assign) BOOL isFans;
@property (nonatomic, copy) NSString * wealthLevel; // 财富等级
@property (nonatomic, copy) NSValue *cellSize;
@property (nonatomic, strong) NSString * avatar; // 用户头像
@property (nonatomic, strong) NSString * noble;// 贵族
@property (nonatomic, strong) NSString * vehicleId;// 座驾id

@end

NS_ASSUME_NONNULL_END
