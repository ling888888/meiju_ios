//
//  LY_LiveFansInviteMessage.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/23.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_LiveFansInviteMessage.h"

@implementation LY_LiveFansInviteMessage

///消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return MessagePersistent_ISCOUNTED;
}

///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [super encodeBaseDict];
    if (self.content) {
        [dataDict setObject:self.content forKey:@"ioscontent"];
    }
    if (self.userName) {
        [dataDict setObject:self.userName forKey:@"name"];
    }
    if (self.type) {
        [dataDict setObject:self.type forKey:@"type"];
    }
    if (self.age) {
        [dataDict setObject:self.age forKey:@"age"];
    }
    if (self.gender) {
        [dataDict setObject:self.gender forKey:@"gender"];
    }
    if (self.userLevel) {
        [dataDict setObject:self.userLevel forKey:@"userLevel"];
    }
//    if (self.medal) {
//        [dataDict setObject:self.medal forKey:@"medal"];
//    }
    if (self.isVip) {
        [dataDict setObject:self.isVip forKey:@"isVip"];
    }
    if (self.specialNum) {
        [dataDict setObject:self.specialNum forKey:@"specialNum"];
    }
    if (self.identity) {
        [dataDict setObject:self.identity forKey:@"identity"];
    }

    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        
        if (dictionary) {
            [self decodeWithDict:dictionary];
            NSArray * array = [[SPDCommonTool getFamyLanguage] componentsSeparatedByString:@"-"];
            NSString *iosContent = dictionary[@"ioscontent"][array[0]];
            if (iosContent.notEmpty) {
                self.content = [iosContent stringByReplacingOccurrencesOfString:@"%@" withString:dictionary[@"name"]];
            }
            self.userName = dictionary[@"name"];
            self.type = dictionary[@"type"];
            if (dictionary[@"age"]) {
                self.age = dictionary[@"age"];
            }
            if (dictionary[@"gender"]) {
                self.gender = dictionary[@"gender"];
            }
            if (dictionary[@"userLevel"]) {
                self.userLevel = dictionary[@"userLevel"];
            }
//            if (dictionary[@"medal"]) {
//                self.medal = dictionary[@"medal"];
//            }
            if (dictionary[@"isVip"]) {
                self.isVip = dictionary[@"isVip"];
            }
            if (dictionary[@"specialNum"]) {
                self.specialNum = dictionary[@"specialNum"];
            }
            if (dictionary[@"identity"]) {
                self.identity = dictionary[@"identity"];
            }
            if ([self.identity isEqualToString:@"system"]) {
                self.userIdentity = LY_MessageSenderUserIdentitySystem;
            } else {
                self.userIdentity = LY_MessageSenderUserIdentityAnchor;
            }
            if (dictionary[@"wealthLevel"]) {
                self.wealthLevel = [NSString stringWithFormat:@"%@",dictionary[@"wealthLevel"]];
            }
//            NSDictionary *userinfoDic = dictionary[@"user"];
//            [self decodeUserInfo:userinfoDic];
        }
    }
}

/// 会话列表中显示的摘要
- (NSString *)conversationDigest {
    return self.content;
}

///消息的类型名
+ (NSString *)getObjectName {
    return @"Live:TxtMsg";
}

- (void)setContent:(NSString *)content {
    _content = content;
    if (![content isKindOfClass:[NSString class]]) {
        return;
    }
    CGFloat heigt = [content sizeWithFont:[UIFont mediumFontOfSize:13] andMaxSize:CGSizeMake(BASECONTENTWIDTH, 100)].height;
    heigt += 25 + 8 + 8 + 7;
    self.contentHeight = heigt;
}

- (NSValue *)cellSize {
    return [NSValue valueWithCGSize:CGSizeMake(kScreenW, self.contentHeight)];
}

@end
