//
//  LiveBeginNotification.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/26.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveBeginNotificationMessage : RCMessageContent

@property (nonatomic, copy) NSString * liveId;
@property (nonatomic, copy) NSString * anchorId; // 主播id
@property (nonatomic, copy) NSString * liveTitle;// 直播标题
@property (nonatomic, copy) NSString * liveCover; // 直播封面 页面需要展示发送这的地方要从消息体中‘user’字段取
@property (nonatomic, copy) NSDictionary * slogan;// 标语 引导语 服务器配置的

@end

NS_ASSUME_NONNULL_END
