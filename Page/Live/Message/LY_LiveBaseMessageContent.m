//
//  LY_LiveBaseMessageContent.m
//  SimpleDate
//
//  Created by 杨宝龙 on 2020/6/2.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_LiveBaseMessageContent.h"
#import "NSObject+Coding.h"

@implementation LY_LiveBaseMessageContent

/// NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        [self hl_decodeWithDeCoder:aDecoder];
    }
    return self;
}


/// NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [self hl_encodeWithCoder:aCoder];
}

/// 将消息内容编码成dict供子类使用
- (NSMutableDictionary *)encodeBaseDict {
    NSMutableDictionary *dataDict = [NSMutableDictionary dictionary];
    
    if (self.senderUserInfo) {
        NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc] init];
        if (self.senderUserInfo.name) {
            [userInfoDic setObject:self.senderUserInfo.name
                 forKeyedSubscript:@"name"];
        }
        if (self.senderUserInfo.portraitUri) {
            [userInfoDic setObject:self.senderUserInfo.portraitUri
                 forKeyedSubscript:@"icon"];
        }
        if (self.senderUserInfo.userId) {
            [userInfoDic setObject:self.senderUserInfo.userId
                 forKeyedSubscript:@"id"];
        }
        [dataDict setObject:userInfoDic forKey:@"user"];
    }
    if (self.portrait.headwearUrl.notEmpty) {
        [dataDict setObject:self.portrait.headwearUrl forKey:@"headwearWebp"];
    }
    if (self.customTag.url.notEmpty) {
        NSMutableDictionary *customTagDict = [NSMutableDictionary dictionary];
        [customTagDict setObject:self.customTag.labelType forKey:@"labelType"];
        [customTagDict setObject:self.customTag.arUrl forKey:@"arUrl"];
        [customTagDict setObject:self.customTag.enUrl forKey:@"enUrl"];
        [customTagDict setObject:self.customTag.zhUrl forKey:@"zhUrl"];
        [dataDict setObject:customTagDict forKey:@"diyLabel"];
    }
    if (self.medalUrl) {
        [dataDict setObject:self.medalUrl forKey:@"medal"];
    }
    if (self.noble) {
        [dataDict setObject:self.noble forKey:@"noble"];
    }
    if (self.wealthLevel) {
        [dataDict setObject:self.wealthLevel forKey:@"wealthLevel"];
    }
    [dataDict setObject:@(self.isAdmin) forKey:@"isAdmin"];
    return dataDict;
}

/// 将dict解码生成属性
- (void)decodeWithDict:(NSDictionary *)dict {
    LY_Portrait *portrait = [[LY_Portrait alloc] init];
    portrait.url = [dict[@"user"] valueForKey:@"icon"];
    if (!portrait.url.notEmpty) {
        portrait.url = [dict[@"user"] valueForKey:@"portrait"]; // fix 
    }
    portrait.headwearUrl = dict[@"headwearWebp"];
    self.portrait = portrait;
    
    if (dict[@"diyLabel"]) {
        self.customTag = [LY_CustomTag yy_modelWithDictionary:dict[@"diyLabel"]];
    }
    if (dict[@"medal"]) {
        self.medalUrl = dict[@"medal"];
    }
    if (dict[@"noble"]) {
        self.noble = dict[@"noble"];
    }
    if (dict[@"wealthLevel"]) {
        self.wealthLevel = dict[@"wealthLevel"];
    }
    if (dict[@"fans"]) {
        NSDictionary *fansDict = dict[@"fans"];
        self.fansTag = [LY_FansTag yy_modelWithDictionary:fansDict];
    }
    if ([dict valueForKey:@"isAdmin"]) {
        self.isAdmin = [dict[@"isAdmin"] boolValue];
    }
    
    [self decode:dict];
    
    NSDictionary *userinfoDic = dict[@"user"];
    [self decodeUserInfo:userinfoDic];
}


- (void)decode:(NSDictionary *)dict {
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList([self class], &outCount);
    NSMutableArray * keys = [NSMutableArray new];
    for (i = 0; i < outCount; i++) {
        objc_property_t property = properties[i];
        NSString *propertyName = [[NSString alloc] initWithCString:property_getName(property) encoding:NSUTF8StringEncoding];
        [keys addObject:propertyName];
    }
    free(properties);
    for (int i = 0; i< keys.count; i++) {
        [self setValue:dict[keys[i]]?:@"" forKey:keys[i]];
    }
}

@end
