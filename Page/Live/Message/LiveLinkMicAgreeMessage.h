//
//  LiveLinkMicAgreeMessage.h
//  SimpleDate
//
//  Created by ling Hou on 2020/8/27.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>
#import "LY_LiveBaseMessageContent.h"

NS_ASSUME_NONNULL_BEGIN


@interface LiveLinkMicAgreeMessage : LY_LiveBaseMessageContent

@property (nonatomic, copy) NSString *userId; // 用户idz座位号

@property (nonatomic, copy) NSString *number; // 座位号

@property (nonatomic, copy) NSString *type; // agree refuse

@end

NS_ASSUME_NONNULL_END
