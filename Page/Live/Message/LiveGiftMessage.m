//
//  LiveGiftMessage.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveGiftMessage.h"
#import "NSObject+Coding.h"

@implementation LiveGiftMessage

///消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return    MessagePersistent_ISCOUNTED;
}

///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [self encodeBaseDict];
    if (self.giftName) {
        [dataDict setObject:self.giftName forKey:@"giftName"];
    }
    if (self.giftCover) {
        [dataDict setObject:self.giftCover forKey:@"giftCover"];
    }
    if (self.num) {
        [dataDict setObject:self.num forKey:@"num"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        if (dictionary) {
            [self decodeWithDict:dictionary];
            if (dictionary[@"giftCover"]) {
                self.giftCover = dictionary[@"giftCover"];
            }
            if (dictionary[@"giftName"]) {
                self.giftName = dictionary[@"giftName"];
            }
            if (dictionary[@"num"]) {
                self.num= dictionary[@"num"];
            }
            if (dictionary[@"giftAnimationUrl"]) {
                self.giftAnimationUrl = dictionary[@"giftAnimationUrl"];
            }
            if (dictionary[@"senderUserId"]) {
                self.senderUserId = dictionary[@"senderUserId"];
            }
            if (dictionary[@"senderUserName"]) {
                self.senderUserName = dictionary[@"senderUserName"];
            }
            if (dictionary[@"senderUserAvatar"]) {
                self.senderUserAvatar = dictionary[@"senderUserAvatar"];
            }
            if (dictionary[@"age"]) {
                self.age = dictionary[@"age"];
            }
            if (dictionary[@"gender"]) {
                self.gender = dictionary[@"gender"];
            }
            if (dictionary[@"userLevel"]) {
                self.userLevel = dictionary[@"userLevel"];
            }else{
                self.userLevel = @"1";
            }
            if (dictionary[@"medal"]) {
                self.medal = dictionary[@"medal"];
            }
            if (dictionary[@"isVip"]) {
                self.isVip = [dictionary[@"isVip"] boolValue];
            }
            if (dictionary[@"specialNum"]) {
                self.specialNum = dictionary[@"specialNum"];
            }
            if (dictionary[@"giftSvgaUrl"]) {
                self.giftSvgaUrl = dictionary[@"giftSvgaUrl"];
            }
            if (dictionary[@"wealthLevel"]) {
                NSNumber *level = dictionary[@"wealthLevel"];
                self.wealthLevel = [level stringValue];
            }
            if (dictionary[@"heartRange"]) {
                self.heartRange = [dictionary[@"heartRange"] integerValue];
            }
            if (dictionary[@"isCombo"]) {
                self.isCombo = [dictionary[@"isCombo"] boolValue];
            }
            if (dictionary[@"msgId"]) {
                self.msgId =dictionary[@"msgId"] ;
            }
            if (dictionary[@"fans"]) {
                self.fansLevel =[NSString stringWithFormat:@"%@",dictionary[@"fans"][@"fansLevel"]]?:@"1" ;
                self.fansName =[NSString stringWithFormat:@"%@",dictionary[@"fans"][@"fansName"]]?:@"fansName" ;
            }
            if (dictionary[@"comboValue"]) {
                self.comboValue = [dictionary[@"comboValue"] integerValue];
            }
            self.isAdmin = [dictionary[@"isAdmin"] boolValue];
            
            NSArray * array = [[SPDCommonTool getFamyLanguage] componentsSeparatedByString:@"-"];
            NSString *contentText = [NSString stringWithFormat:SPDLocalizedString(@"送出%@%@"), self.giftName[array[0]], [NSString stringWithFormat:@"x%@", self.num?:@""]];
            
            CGFloat contentTextHeight = [contentText sizeWithFont:[UIFont mediumFontOfSize:13] andMaxSize:CGSizeMake(BASECONTENTWIDTH - 25, 100)].height;
            contentTextHeight += 25 + 8 + 8 + 7;
            self.contentHeight = contentTextHeight;
        }
    }
}

///消息的类型名
+ (NSString *)getObjectName {
    return @"Live:GiftMsg";
}

- (NSValue *)cellSize {
    return [NSValue valueWithCGSize:CGSizeMake(kScreenW, self.contentHeight)];
}


@end
