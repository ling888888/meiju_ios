//
//  LivePKInviteRefuseMessage.h
//  SimpleDate
//
//  Created by ling Hou on 2020/6/24.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

NS_ASSUME_NONNULL_BEGIN

@interface LivePKInviteRefuseMessage : RCMessageContent

@property (nonatomic, assign) NSInteger pkId;

@property (nonatomic, copy) NSString * userId;

@property (nonatomic, assign) NSInteger inChat; // [ 0,1,2] // 1 连麦中被拒绝 0 用户主动拒绝 2 超时被动拒绝

@end

NS_ASSUME_NONNULL_END
