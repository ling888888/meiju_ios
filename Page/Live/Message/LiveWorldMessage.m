//
//  LiveWorldMessage.m
//  SimpleDate
//
//  Created by ling Hou on 2020/8/24.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveWorldMessage.h"
#import "NSObject+Coding.h"

@implementation LiveWorldMessage

// 消息的类型名
+ (NSString *)getObjectName {
    return @"Live:WorldMsg";
}

// 消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return MessagePersistent_STATUS;
}

// NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        [self hl_decodeWithDeCoder:aDecoder];
    }
    return self;
}

// NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [self hl_encodeWithCoder:aCoder];
}


// 将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        if (dictionary) {
            [self decodeWithDict:dictionary];
        }
    }
}



@end
