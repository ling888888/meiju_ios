//
//  LivePKBeginMessage.h
//  SimpleDate
//
//  Created by ling Hou on 2020/6/24.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

NS_ASSUME_NONNULL_BEGIN

@interface LivePKBeginMessage : RCMessageContent

@property (nonatomic, strong) NSString * launchUserId;
@property (nonatomic, strong) NSString * acceptUserId;
@property (nonatomic, strong) NSString * launchLiveId;
@property (nonatomic, strong) NSString * acceptLiveId;

@end

NS_ASSUME_NONNULL_END
