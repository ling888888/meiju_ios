//
//  PKResultMessage.h
//  SimpleDate
//
//  Created by ling Hou on 2020/7/1.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

NS_ASSUME_NONNULL_BEGIN

// pk 结果消息

@interface PKResultMessage : RCMessageContent

@property (nonatomic, copy) NSString *winLiveId;
@property (nonatomic, assign)   BOOL isBalance; // 是不是平
@property (nonatomic, assign)   BOOL isKill;
@property (nonatomic, assign) int type; // 0 1
@property (nonatomic, copy) NSString * continuityWinTimes;// 连胜场数

@property (nonatomic, copy) NSString *launchUId;
@property (nonatomic, assign) NSInteger launchScore;
@property (nonatomic, copy) NSString* launchMvpUserId;
@property (nonatomic, copy) NSString *launchMvpUserAvatar;
@property (nonatomic, copy) NSString *launchMvpUserName;
@property (nonatomic, assign) NSInteger launchMvpScore;

@property (nonatomic, copy) NSString * acceptId;
@property (nonatomic, assign)  NSInteger acceptScore;
@property (nonatomic, copy) NSString * acceptMvpUserId;
@property (nonatomic, copy) NSString * acceptMvpUserAvatar;
@property (nonatomic, copy) NSString * acceptMvpUserName;
@property (nonatomic, assign) NSInteger acceptMvpScore;
@end

NS_ASSUME_NONNULL_END
