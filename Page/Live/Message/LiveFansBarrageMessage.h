//
//  LiveFansBarrageMessage.h
//  SimpleDate
//
//  Created by ling Hou on 2020/4/26.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_LiveBaseMessageContent.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveFansBarrageMessage : LY_LiveBaseMessageContent

@property (nonatomic, copy) NSString *msg;

@end

NS_ASSUME_NONNULL_END
