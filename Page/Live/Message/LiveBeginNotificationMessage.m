//
//  LiveBeginNotification.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/26.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveBeginNotificationMessage.h"
#import "NSObject+Coding.h"

@implementation LiveBeginNotificationMessage

///消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return MessagePersistent_ISCOUNTED;
}

/// NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        [self hl_decodeWithDeCoder:aDecoder];
    }
    return self;
}


/// NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [self hl_encodeWithCoder:aCoder];
}


///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [NSMutableDictionary dictionary];
    if (self.liveId) {
        [dataDict setObject:self.liveId forKey:@"liveId"];
    }
    if (self.liveTitle) {
        [dataDict setObject:self.liveTitle forKey:@"liveTitle"];
    }
    if (self.liveCover) {
        [dataDict setObject:self.liveCover forKey:@"liveCover"];
    }
    if (self.slogan) {
        [dataDict setObject:self.slogan forKey:@"slogan"];
    }

    if (self.senderUserInfo) {
        NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc] init];
        if (self.senderUserInfo.name) {
            [userInfoDic setObject:self.senderUserInfo.name
                 forKeyedSubscript:@"name"];
        }
        if (self.senderUserInfo.portraitUri) {
            [userInfoDic setObject:self.senderUserInfo.portraitUri
                 forKeyedSubscript:@"icon"];
        }
        if (self.senderUserInfo.userId) {
            [userInfoDic setObject:self.senderUserInfo.userId
                 forKeyedSubscript:@"id"];
        }
        [dataDict setObject:userInfoDic forKey:@"user"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        if (dictionary) {
            if (dictionary[@"liveCover"]) {
                self.liveCover = dictionary[@"liveCover"];
            }
            if (dictionary[@"liveTitle"]) {
                self.liveTitle = dictionary[@"liveTitle"];
            }
            if (dictionary[@"anchorId"]) {
                self.anchorId = dictionary[@"anchorId"];
            }
            if (dictionary[@"liveId"]) {
                self.liveId = dictionary[@"liveId"];
            }
            if (dictionary[@"slogan"]) {
                self.slogan = dictionary[@"slogan"];
            }
            NSDictionary *userinfoDic = dictionary[@"user"];
            [self decodeUserInfo:userinfoDic];
        }
    }
}

///消息的类型名
+ (NSString *)getObjectName {
    return @"Live:NotifyFansMsg";
}

- (NSString *)conversationDigest {
    NSArray * array = [[SPDCommonTool getFamyLanguage] componentsSeparatedByString:@"-"];
    return SPDStringWithKey(self.slogan[array[0]], nil);
}

@end
