//
//  LY_LiveFollowedMessage.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/26.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_LiveFollowedMessage.h"
#import "NSObject+Coding.h"

@implementation LY_LiveFollowedMessage

///消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return MessagePersistent_ISCOUNTED;
}

///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [self encodeBaseDict];
    if (self.roomId) {
        [dataDict setObject:self.roomId forKey:@"roomId"];
    }
    if (self.medal) {
        [dataDict setObject:self.medal forKey:@"medal"];
    }
    if (self.content) {
        [dataDict setObject:self.content forKey:@"content"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        
        if (dictionary) {
            [super decodeWithDict:dictionary];
            if (dictionary[@"roomId"]) {
                self.roomId = dictionary[@"roomId"];
            }
            if (dictionary[@"medal"]) {
                self.medal = dictionary[@"medal"];
            }
            if (dictionary[@"content"]) {
                self.content = dictionary[@"content"];
                NSArray * array = [[SPDCommonTool getFamyLanguage] componentsSeparatedByString:@"-"];
                self.langContent = dictionary[@"content"][array[0]];
            }
//            if (dictionary[@"wealthLevel"]) {
//                self.wealthLevel = [NSString stringWithFormat:@"%@",dictionary[@"wealthLevel"]];
//            }
            self.userIdentity = LY_MessageSenderUserIdentityAnchor;
//            [self decodeWithDict:dictionary];
        }
    }
}

/// 会话列表中显示的摘要
- (NSString *)conversationDigest {
    return self.content;
}

///消息的类型名
+ (NSString *)getObjectName {
    return @"Live:FollowMsgCustom";
}

- (NSValue *)cellSize {
    return [NSValue valueWithCGSize:CGSizeMake(kScreenW, self.contentHeight)];
}

- (void)setLangContent:(NSString *)langContent {
    _langContent = langContent;
    
    CGFloat heigt = [langContent sizeWithFont:[UIFont mediumFontOfSize:13] andMaxSize:CGSizeMake(BASECONTENTWIDTH, 100)].height;
    heigt += 25 + 8 + 8 + 7;
    self.contentHeight = heigt;
}

@end
