//
//  LiveImageMessage.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/27.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_LiveBaseMessageContent.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveImageMessage : LY_LiveBaseMessageContent

@property (nonatomic, copy) NSString * imageUrl;

@property (nonatomic, copy) NSString *age;

@property (nonatomic, copy) NSString *gender;

@property (nonatomic, copy) NSString * userLevel;// 个人等级

//@property (nonatomic, copy) NSString * noble;// 他的爵位

@property (nonatomic, copy) NSString * medal;// 勋章 url

@property (nonatomic, copy) NSString * isVip; // true false

@property (nonatomic, copy) NSString *specialNum; // 靓号

@property (nonatomic, copy) NSString *fansName;
@property (nonatomic, copy) NSString *fansLevel;
@property (nonatomic, copy) NSString * wealthLevel; // 财富等级
@property (nonatomic, copy) NSString *noble; //

@property (nonatomic, copy) NSValue* cellSize;

@property (nonatomic, copy) NSValue* imageSize; // 图片的宽高

@end

NS_ASSUME_NONNULL_END
