//
//  LiveFollowMessage.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/31.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveFollowMessage : RCMessageContent

@property (nonatomic, copy) NSString *num;

@end

NS_ASSUME_NONNULL_END
