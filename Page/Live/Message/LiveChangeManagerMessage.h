//
//  LiveChangeManagerMessage.h
//  SimpleDate
//
//  Created by ling Hou on 2020/9/16.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>
#import "LY_LiveBaseMessageContent.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveChangeManagerMessage : LY_LiveBaseMessageContent

@property (nonatomic, copy) NSString * type;// set remove
@property (nonatomic, copy) NSString * userId;

@end

NS_ASSUME_NONNULL_END
