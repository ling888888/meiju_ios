//
//  LiveImageMessage.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/27.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveImageMessage.h"
#import "NSObject+Coding.h"

@implementation LiveImageMessage

///消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return    MessagePersistent_ISCOUNTED;
}


///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [self encodeBaseDict];
    if (self.imageUrl) {
        [dataDict setObject:self.imageUrl forKey:@"imageUrl"];
    }
    if (self.age) {
           [dataDict setObject:self.age forKey:@"age"];
       }
       if (self.gender) {
           [dataDict setObject:self.gender forKey:@"gender"];
       }
       if (self.userLevel) {
           [dataDict setObject:self.userLevel forKey:@"userLevel"];
       }
       if (self.specialNum) {
           [dataDict setObject:self.specialNum forKey:@"specialNum"];
       }
       if (self.medal) {
           [dataDict setObject:self.medal forKey:@"medal"];
       }
       if (self.isVip) {
           [dataDict setObject:self.isVip forKey:@"isVip"];
       }
        if (self.fansName) {
            [dataDict setObject:self.fansName forKey:@"fansName"];
        }
        if (self.fansLevel) {
            [dataDict setObject:self.fansLevel forKey:@"fansLevel"];
        }
        if (self.wealthLevel) {
            [dataDict setObject:self.wealthLevel forKey:@"wealthLevel"];
        }
        if (self.noble) {
            [dataDict setObject:self.noble forKey:@"noble"];
        }
        CGSize size = [self getImageSize:self.imageUrl];
        self.imageSize = [NSValue valueWithCGSize:CGSizeMake(size.width, size.height)];
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        if (dictionary) {
            [self decodeWithDict:dictionary];
            self.userIdentity = LY_MessageSenderUserIdentityAnchor;
            if (dictionary[@"imageUrl"]) {
                self.imageUrl = dictionary[@"imageUrl"];
            }
            if (dictionary[@"age"]) {
                self.age = dictionary[@"age"];
            }
            if (dictionary[@"gender"]) {
                self.gender = dictionary[@"gender"];
            }
            if (dictionary[@"isVip"]) {
                self.isVip = dictionary[@"isVip"];
            }

            if (dictionary[@"userLevel"]) {
                self.userLevel = dictionary[@"userLevel"];
            }
            if (dictionary[@"specialNum"]) {
                self.specialNum = dictionary[@"specialNum"];
            }
            if (dictionary[@"medal"]) {
                self.medal = dictionary[@"medal"];
            }
            if (dictionary[@"wealthLevel"]) {
                
//                NSNumber *level = dictionary[@"wealthLevel"];
                self.wealthLevel = dictionary[@"wealthLevel"];
            }
            if (dictionary[@"fans"]) {
                self.fansLevel =[NSString stringWithFormat:@"%@",dictionary[@"fans"][@"fansLevel"]]?:@"1" ;
                self.fansName =[NSString stringWithFormat:@"%@",dictionary[@"fans"][@"fansName"]]?:@"fansName" ;
            }
            if (dictionary[@"noble"]) {
                self.noble = dictionary[@"noble"];
            }
            CGSize size = [self getImageSize:self.imageUrl];
            self.imageSize = [NSValue valueWithCGSize:CGSizeMake(size.width, size.height)];

        }
    }
}


///消息的类型名
+ (NSString *)getObjectName {
    return @"Live:ImageMsg";
}

- (NSValue *)cellSize {
    return [NSValue valueWithCGSize: CGSizeMake(kScreenW, 180)];
}


- (CGSize)getImageSize:(NSString *)url {
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
    UIImage * image = [UIImage imageWithData:data];
    return image.size;
}


@end
