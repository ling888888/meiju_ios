//
//  LiveRoomSystemMessage.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/24.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomSystemMessage.h"
#import "NSObject+Coding.h"
#import "NSString+XXWAddition.h"

@implementation LiveRoomSystemMessage

///消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return    MessagePersistent_ISCOUNTED;
}

/// NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        [self hl_decodeWithDeCoder:aDecoder];
    }
    return self;
}


/// NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [self hl_encodeWithCoder:aCoder];
}


///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [NSMutableDictionary dictionary];
    [dataDict setObject:self.content forKey:@"content"];
    
    if (self.senderUserInfo) {
        NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc] init];
        if (self.senderUserInfo.name) {
            [userInfoDic setObject:self.senderUserInfo.name
                 forKeyedSubscript:@"name"];
        }
        if (self.senderUserInfo.portraitUri) {
            [userInfoDic setObject:self.senderUserInfo.portraitUri
                 forKeyedSubscript:@"icon"];
        }
        if (self.senderUserInfo.userId) {
            [userInfoDic setObject:self.senderUserInfo.userId
                 forKeyedSubscript:@"id"];
        }
        [dataDict setObject:userInfoDic forKey:@"user"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        
        if (dictionary) {
            self.content = dictionary[@"content"];
            NSDictionary *userinfoDic = dictionary[@"user"];
            [self decodeUserInfo:userinfoDic];
            self.contentHeight = [NSString sizeWithString:_content andFont:[UIFont mediumFontOfSize:12] andMaxSize:CGSizeMake(kScreenW - 15 -0, CGFLOAT_MAX)].height;

//            self.contentHeight = [NSString getSpaceLabelHeightWithStr:self.content withSpeace:3 withFont:[UIFont mediumFontOfSize:12] withWidth:kScreenW - 15- 96.5];
        }
    }
}

/// 会话列表中显示的摘要
- (NSString *)conversationDigest {
    return self.content;
}

- (void)setContent:(NSString *)content {
    _content = content;
    self.contentHeight = [NSString getSpaceLabelHeightWithStr:_content withSpeace:3 withFont:[UIFont mediumFontOfSize:12] withWidth:kScreenW - 15- 96.5];
}

///消息的类型名
+ (NSString *)getObjectName {
    return @"SPD:SystemMsg";
}

- (NSValue *)cellSize {
    return [NSValue valueWithCGSize:CGSizeMake(kScreenW, self.contentHeight)];
}

@end
