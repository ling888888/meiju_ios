//
//  LY_LiveFansLevelMessage.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/26.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_LiveFansLevelMessage.h"
#import "NSObject+Coding.h"

@implementation LY_LiveFansLevelMessage


///消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return MessagePersistent_ISCOUNTED;
}

/// NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        [self hl_decodeWithDeCoder:aDecoder];
    }
    return self;
}


/// NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [self hl_encodeWithCoder:aCoder];
}

///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [NSMutableDictionary dictionary];
    if (self.fansId) {
        [dataDict setObject:self.fansId forKey:@"fansId"];
    }
    if (self.fansLevel) {
        [dataDict setObject:[NSString stringWithFormat:@"%ld", self.fansLevel] forKey:@"fansLevel"];
    }
    if (self.senderUserInfo) {
        NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc] init];
        if (self.senderUserInfo.name) {
            [userInfoDic setObject:self.senderUserInfo.name
                 forKeyedSubscript:@"name"];
        }
        if (self.senderUserInfo.portraitUri) {
            [userInfoDic setObject:self.senderUserInfo.portraitUri
                 forKeyedSubscript:@"icon"];
        }
        if (self.senderUserInfo.userId) {
            [userInfoDic setObject:self.senderUserInfo.userId
                 forKeyedSubscript:@"id"];
        }
        [dataDict setObject:userInfoDic forKey:@"user"];
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        
        if (dictionary) {
            if (dictionary[@"fansId"]) {
                self.fansId = dictionary[@"fansId"];
            }
            if (dictionary[@"fansLevel"]) {
                self.fansLevel = [dictionary[@"fansLevel"] integerValue];
            }
            NSDictionary *userinfoDic = dictionary[@"user"];
            [self decodeUserInfo:userinfoDic];
        }
    }
}

/// 会话列表中显示的摘要
- (NSString *)conversationDigest {
    return self.fansId;
}

///消息的类型名
+ (NSString *)getObjectName {
    return @"Live:FansLevel";
}

@end
