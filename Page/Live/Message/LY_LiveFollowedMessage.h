//
//  LY_LiveFollowedMessage.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/26.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_LiveBaseMessageContent.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_LiveFollowedMessage : LY_LiveBaseMessageContent

//直播间id
@property(nonatomic, copy) NSString *roomId;

//主播勋章图片
@property(nonatomic, copy) NSString *medal;

//
@property(nonatomic, copy) NSString *content;

//
@property(nonatomic, copy) NSString *langContent;

// 内容高度
@property (nonatomic, assign) CGFloat contentHeight;

@property (nonatomic, copy) NSValue * cellSize;


//主播勋章名称
@property(nonatomic, copy) NSString *noble;

//主播vip状态
@property(nonatomic, copy) NSString *isVip;

//主播靓号
@property(nonatomic, copy) NSString *specialNum;

//主播等级
@property(nonatomic, copy) NSString *userLevel;

//主播性别
@property(nonatomic, copy) NSString *gender;

//主播年龄
@property(nonatomic, copy) NSString *age;

@property(nonatomic, copy) NSString *wealthLevel;

@end

NS_ASSUME_NONNULL_END
