//
//  LiveWealthLevelMessage.h
//  SimpleDate
//
//  Created by ling Hou on 2020/5/9.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_LiveBaseMessageContent.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveWealthLevelMessage : LY_LiveBaseMessageContent

@property(nonatomic, copy) NSString *userId;

@property(nonatomic, copy) NSString *portraitUrl;

@property(nonatomic, copy) NSString *userName;

@property(nonatomic, copy) NSString *userWealthLevel; //升级的用户财富等级

// 年龄
@property (nonatomic, copy) NSString *age;

// 性别
@property (nonatomic, copy) NSString *gender;

// 勋章 url
@property (nonatomic, copy) NSString * medal;

// 文本内容
@property(nonatomic, copy) NSString *content;


@property (nonatomic, copy) NSString * wealthLevel;//主播财富等级

// 内容高度
@property (nonatomic, assign) CGFloat contentHeight;

@property (nonatomic, copy) NSValue * cellSize;

@end

NS_ASSUME_NONNULL_END
