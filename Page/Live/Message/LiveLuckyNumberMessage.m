//
//  LiveLuckyNumberMessage.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/22.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveLuckyNumberMessage.h"
#import "NSObject+Coding.h"

@interface LiveLuckyNumberMessage ()

@end

@implementation LiveLuckyNumberMessage


///消息是否存储，是否计入未读数
+ (RCMessagePersistent)persistentFlag {
    return    MessagePersistent_ISCOUNTED;
}

///将消息内容编码成json
- (NSData *)encode {
    NSMutableDictionary *dataDict = [super encodeBaseDict];
    [dataDict setObject:self.content forKey:@"content"];
    if (self.age) {
        [dataDict setObject:self.age forKey:@"age"];
    }
    if (self.gender) {
        [dataDict setObject:self.gender forKey:@"gender"];
    }
    if (self.userLevel) {
        [dataDict setObject:self.userLevel forKey:@"userLevel"];
    }
    if (self.specialNum) {
        [dataDict setObject:self.specialNum forKey:@"specialNum"];
    }
    if (self.medal) {
        [dataDict setObject:self.medal forKey:@"medal"];
    }
    if (self.isVip) {
        [dataDict setObject:self.isVip forKey:@"isVip"];
    }
    if (self.mentionedList) {
        [dataDict setObject:self.mentionedList forKey:@"mentionedList"];
    }
    if (self.mentionedType) {
        [dataDict setObject:self.mentionedType forKey:@"mentionedType"];
    }
    [dataDict setObject:self.lucky_number forKey:@"lucky_number"];

    if (self.fansLevel.notEmpty && self.fansName.notEmpty) {
        NSDictionary * dict = @{
            @"fansName":self.fansName,
            @"fansLevel": self.fansLevel
        };
        [dataDict setObject:dict forKey:@"fans"];
    }
    if (self.fans) {
        [dataDict setObject:self.fans forKey:@"fans"];
        self.fansLevel = self.fans[@"fansLevel"];
        self.fansName = self.fans[@"fansName"];
    }
    [dataDict setObject:@(self.isAdmin) forKey:@"isAdmin"];

    if (self.bubble) {
        [dataDict setObject:self.bubble forKey:@"bubble"];
    }

    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict
                                                   options:kNilOptions
                                                     error:nil];
    return data;
}

///将json解码生成消息内容
- (void)decodeWithData:(NSData *)data {
    if (data) {
        __autoreleasing NSError *error = nil;
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions
                                          error:&error];
        if (dictionary) {
            [self decodeWithDict:dictionary];
            self.content = dictionary[@"content"];
            if (dictionary[@"age"]) {
                self.age = dictionary[@"age"];
            }
            if (dictionary[@"gender"]) {
                self.gender = dictionary[@"gender"];
            }
            if (dictionary[@"isVip"]) {
                self.isVip = dictionary[@"isVip"];
            }
            self.isAdmin = [dictionary[@"isAdmin"] boolValue];

            if (dictionary[@"userLevel"]) {
                self.userLevel = dictionary[@"userLevel"];
            }
            if (dictionary[@"lucky_number"]) {
                self.lucky_number = dictionary[@"lucky_number"];
            }
            if (dictionary[@"specialNum"]) {
                self.specialNum = dictionary[@"specialNum"];
            }
            if (dictionary[@"medal"]) {
                self.medal = dictionary[@"medal"];
            }
            if (dictionary[@"mentionedType"]) {
                self.mentionedType = dictionary[@"mentionedType"];
            }
            if (dictionary[@"mentionedList"]) {
                self.mentionedList = dictionary[@"mentionedList"];
            }
//            if (dictionary[@"wealthLevel"]) {
//                self.wealthLevel = dictionary[@"wealthLevel"];
//            }
            if (dictionary[@"fans"]) {
                self.fans = dictionary[@"fans"];
                if ([self.fans isKindOfClass:[NSDictionary class]]) {
                    self.fansName = self.fans[@"fansName"];
                    self.fansLevel = self.fans[@"fansLevel"];
                }
            }
//            if (dictionary[@"noble"]) {
//                self.noble = dictionary[@"noble"];
//            }
            if (dictionary[@"bubble"] && [dictionary[@"bubble"] isKindOfClass:[NSDictionary class]]) {
                self.bubble = dictionary[@"bubble"];
            }
//            NSDictionary *userinfoDic = dictionary[@"user"];
//            [self decodeUserInfo:userinfoDic];

        }
    }
}

/// 会话列表中显示的摘要
- (NSString *)conversationDigest {
    return self.content;
}

///消息的类型名
+ (NSString *)getObjectName {
    return @"Live:LuckNumberTxtMsg";
}

- (void)setContent:(NSString *)content {
    _content = content;
    self.cellHeight = [content sizeWithFont:[UIFont mediumFontOfSize:13] andMaxSize:CGSizeMake(BASECONTENTWIDTH, CGFLOAT_MAX)].height;
    self.cellHeight += 25 + 8 + 8 + 7;
}

- (void)setLucky_number:(NSString *)lucky_number {
    _lucky_number = lucky_number;
    NSMutableAttributedString * str = [[NSMutableAttributedString alloc]initWithString:@"幸运数字".localized];
    [str addAttributes:@{NSForegroundColorAttributeName:[UIColor cyanColor]} range:NSMakeRange(0, str.length)];
    NSAttributedString * numStr = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@",lucky_number] attributes:@{NSForegroundColorAttributeName:SPD_HEXCOlOR(@"FFDA01"),
                                                                                NSFontAttributeName:[UIFont boldSystemFontOfSize:30],
                                                                               NSStrokeColorAttributeName:SPD_HEXCOlOR(@"865015")
    }];
    [str appendAttributedString:numStr];
    CGSize attSize = [str boundingRectWithSize:CGSizeMake(375, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size;
    self.cellHeight = attSize.height;
    self.cellHeight += 25 + 8 + 8 + 7;
}

- (NSValue *)cellSize {
    return [NSValue valueWithCGSize:CGSizeMake(kScreenW, self.cellHeight)];
}


@end
