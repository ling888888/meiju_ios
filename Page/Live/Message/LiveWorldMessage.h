//
//  LiveWorldMessage.h
//  SimpleDate
//
//  Created by ling Hou on 2020/8/24.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>
#import "LY_LiveBaseMessageContent.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveWorldMessage : LY_LiveBaseMessageContent

@property (nonatomic, strong) NSString * worldMsgType;//": "gift"           //直播间世界消息类型，礼物类型：gift，红包类型：redPackage，关注：star，粉丝团：fans
@property (nonatomic, assign) NSInteger  msgLevel;   //[0 1 2] 消息样式0，1，2代表不同好看程度的消息，数值越高，则越好看

@property (nonatomic, strong) NSString * jumpId;
@property (nonatomic, strong) NSString * jumpType;

@property (nonatomic, strong) NSString * senderUserAvatar;
@property (nonatomic, strong) NSString * senderUserName;
@property (nonatomic, strong) NSString * isVip;//": "false",                //发送人是否是会员
@property (nonatomic, strong) NSString * receiverUserName;//": "反反复复发的话题了个逼的话题了吗我也是啊",        //接受人用户名称，礼物外的其他消息类型可能没有这个key
@property (nonatomic, strong) NSString * giftCover;//": "https://dev-famy.17jianyue.cn/svga/1587990680869.svga",        //礼物图片，礼物外的其他消息类型可能没有这个key
@property (nonatomic, strong) NSString * medal;                     //贵族标签
@property (nonatomic, assign) NSInteger giftNumber;//": 1,                 //礼物数量，礼物外的其他消息类型可能没有这个key
@property (nonatomic, copy) NSDictionary *giftName;
@property (nonatomic, strong) NSDictionary * msgInfo;//消息
@property (nonatomic, strong) NSString * senderHeadwearWebp; //发送者头饰
@property (nonatomic, strong) NSString * receiveHeadwearWebp; //接收者头饰
@property (nonatomic, strong) NSString * headwearWebp;//发送者头饰
@property (nonatomic, strong) NSString * sendGender; //发送者性别
@property (nonatomic, strong) NSString * receiveGender; // 接受者性别
@property (nonatomic, strong) NSString * senderId;//":""123"      //发送人的用户id
@property (nonatomic, strong) NSNumber * rewardNumber; //
@property (nonatomic, strong) NSString * resource; //live clan

@end

NS_ASSUME_NONNULL_END
