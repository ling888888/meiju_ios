//
//  LiveRoomSystemMessage.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/24.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <RongIMLib/RongIMLib.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveRoomSystemMessage : RCMessageContent

@property (nonatomic, copy) NSString * content; // 系统消息的内容

@property (nonatomic, assign) CGFloat contentHeight;//内容高度

@property (nonatomic, copy) NSValue * cellSize;

@end

NS_ASSUME_NONNULL_END
