//
//  LY_LiveRecordTableViewCell.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_LiveRecordTableViewCell.h"

@interface LY_LiveRecordTableViewCell ()

@property(nonatomic, strong) UILabel *timeKeyLabel;

@property(nonatomic, strong) UILabel *timeValueLabel;

@property(nonatomic, strong) UILabel *durationKeyLabel;

@property(nonatomic, strong) UILabel *durationValueLabel;

@property(nonatomic, strong) UILabel *giftNumberKeyLabel;

@property(nonatomic, strong) UILabel *giftNumberValueLabel;

@property(nonatomic, strong) UILabel *personNumberKeyLabel;

@property(nonatomic, strong) UILabel *personNumberValueLabel;

@property(nonatomic, strong) UILabel *followerNumberKeyLabel;

@property(nonatomic, strong) UILabel *followerNumberValueLabel;



@end

@implementation LY_LiveRecordTableViewCell

- (UILabel *)timeKeyLabel {
    if (!_timeKeyLabel) {
        _timeKeyLabel = [[UILabel alloc] init];
        _timeKeyLabel.textColor = [UIColor colorWithHexString:@"#232426"];
        _timeKeyLabel.font = [UIFont mediumFontOfSize:14];
        _timeKeyLabel.text = @"开播时间：".localized;
    }
    return _timeKeyLabel;
}

- (UILabel *)timeValueLabel {
    if (!_timeValueLabel) {
        _timeValueLabel = [[UILabel alloc] init];
        _timeValueLabel.textColor = [UIColor colorWithHexString:@"#232426"];
        _timeValueLabel.font = [UIFont mediumFontOfSize:14];
    }
    return _timeValueLabel;
}

- (UILabel *)durationKeyLabel {
    if (!_durationKeyLabel) {
        _durationKeyLabel = [[UILabel alloc] init];
        _durationKeyLabel.textColor = [UIColor colorWithHexString:@"#232426"];
        _durationKeyLabel.font = [UIFont mediumFontOfSize:14];
        _durationKeyLabel.text = @"直播时长：".localized;
    }
    return _durationKeyLabel;
}

- (UILabel *)durationValueLabel {
    if (!_durationValueLabel) {
        _durationValueLabel = [[UILabel alloc] init];
        _durationValueLabel.textColor = [UIColor colorWithHexString:@"#232426"];
        _durationValueLabel.font = [UIFont mediumFontOfSize:14];
    }
    return _durationValueLabel;
}

- (UILabel *)giftNumberKeyLabel {
    if (!_giftNumberKeyLabel) {
        _giftNumberKeyLabel = [[UILabel alloc] init];
        _giftNumberKeyLabel.textColor = [UIColor colorWithHexString:@"#232426"];
        _giftNumberKeyLabel.font = [UIFont mediumFontOfSize:14];
        _giftNumberKeyLabel.text = @"收到礼物".localized;
    }
    return _giftNumberKeyLabel;
}

- (UILabel *)giftNumberValueLabel {
    if (!_giftNumberValueLabel) {
        _giftNumberValueLabel = [[UILabel alloc] init];
        _giftNumberValueLabel.textColor = [UIColor colorWithHexString:@"#232426"];
        _giftNumberValueLabel.font = [UIFont mediumFontOfSize:18];
    }
    return _giftNumberValueLabel;
}

- (UILabel *)personNumberKeyLabel {
    if (!_personNumberKeyLabel) {
        _personNumberKeyLabel = [[UILabel alloc] init];
        _personNumberKeyLabel.textColor = [UIColor colorWithHexString:@"#232426"];
        _personNumberKeyLabel.font = [UIFont mediumFontOfSize:14];
        _personNumberKeyLabel.text = @"收听人数".localized;
    }
    return _personNumberKeyLabel;
}

- (UILabel *)personNumberValueLabel {
    if (!_personNumberValueLabel) {
        _personNumberValueLabel = [[UILabel alloc] init];
        _personNumberValueLabel.textColor = [UIColor colorWithHexString:@"#232426"];
        _personNumberValueLabel.font = [UIFont mediumFontOfSize:18];
    }
    return _personNumberValueLabel;
}

- (UILabel *)followerNumberKeyLabel {
    if (!_followerNumberKeyLabel) {
        _followerNumberKeyLabel = [[UILabel alloc] init];
        _followerNumberKeyLabel.textColor = [UIColor colorWithHexString:@"#232426"];
        _followerNumberKeyLabel.font = [UIFont mediumFontOfSize:14];
        _followerNumberKeyLabel.text = @"新增关注".localized;
    }
    return _followerNumberKeyLabel;
}

- (UILabel *)followerNumberValueLabel {
    if (!_followerNumberValueLabel) {
        _followerNumberValueLabel = [[UILabel alloc] init];
        _followerNumberValueLabel.textColor = [UIColor colorWithHexString:@"#232426"];
        _followerNumberValueLabel.font = [UIFont mediumFontOfSize:18];
    }
    return _followerNumberValueLabel;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self setupUI];
        
        [self setupUIFrame];
        
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.timeKeyLabel];
    [self.contentView addSubview:self.timeValueLabel];
    [self.contentView addSubview:self.durationKeyLabel];
    [self.contentView addSubview:self.durationValueLabel];
    [self.contentView addSubview:self.giftNumberKeyLabel];
    [self.contentView addSubview:self.giftNumberValueLabel];
    [self.contentView addSubview:self.personNumberKeyLabel];
    [self.contentView addSubview:self.personNumberValueLabel];
    [self.contentView addSubview:self.followerNumberKeyLabel];
    [self.contentView addSubview:self.followerNumberValueLabel];
}

- (void)setupUIFrame {
    [self.timeKeyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(13);
        make.leading.mas_equalTo(15);
    }];
    
    [self.timeValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.timeKeyLabel);
        make.leading.mas_equalTo(self.timeKeyLabel.mas_trailing);
    }];
    
    [self.durationKeyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.timeKeyLabel.mas_bottom).offset(10);
        make.leading.mas_equalTo(15);
    }];
    
    [self.durationValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.durationKeyLabel);
        make.leading.mas_equalTo(self.durationKeyLabel.mas_trailing);
    }];
    
    [self.giftNumberKeyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.bottom.mas_equalTo(-13);
    }];
    
    [self.giftNumberValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.bottom.mas_equalTo(self.giftNumberKeyLabel.mas_top);
    }];
    
    [self.personNumberKeyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.giftNumberKeyLabel.mas_trailing).offset(40);
        make.bottom.mas_equalTo(-13);
    }];
    
    [self.personNumberValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.personNumberKeyLabel);
        make.bottom.mas_equalTo(self.personNumberKeyLabel.mas_top);
    }];
    
    [self.followerNumberKeyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.personNumberKeyLabel.mas_trailing).offset(40);
        make.bottom.mas_equalTo(-13);
    }];
    
    [self.followerNumberValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.followerNumberKeyLabel);
        make.bottom.mas_equalTo(self.personNumberKeyLabel.mas_top);
    }];
}

- (void)setRecord:(LY_LiveRecord *)record {
    _record = record;
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setTimeZone:[NSTimeZone localTimeZone]];
    format.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//    format.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [format setTimeZone:[NSTimeZone localTimeZone]];
    [format setDateFormat:@"yyyy.MM.dd HH:mm"];
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:[record.time longLongValue] / 1000];
    
    self.timeValueLabel.text = [format stringFromDate:date];
    self.durationValueLabel.text = record.duration;
    self.giftNumberValueLabel.text = record.giftNumber;
    self.personNumberValueLabel.text = record.personNumber;
    self.followerNumberValueLabel.text = record.followerNumber;
}

@end
