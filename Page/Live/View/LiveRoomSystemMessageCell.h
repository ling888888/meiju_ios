//
//  LiveRoomSystemMessageCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/24.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveRoomSystemMessage.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveRoomSystemMessageCell : UICollectionViewCell

@property (nonatomic, strong) LiveRoomSystemMessage * message;

@end

NS_ASSUME_NONNULL_END
