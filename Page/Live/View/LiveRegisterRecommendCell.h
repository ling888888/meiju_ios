//
//  LiveRegisterRecommendCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/4/23.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveRegisterRecommendCell : UICollectionViewCell

@property (nonatomic, copy) NSDictionary * dict;

@property (nonatomic, assign) BOOL cellSelected;

@end

NS_ASSUME_NONNULL_END
