//
//  LY_FansLevelView.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/22.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_FansLevelView.h"

@interface LY_FansLevelView ()

@property(nonatomic, strong) UIButton *levelBtn;

@property(nonatomic, strong) UIImageView *nameBgImgView;

@property(nonatomic, strong) UILabel *nameLabel;

@property(nonatomic, assign) NSInteger level;

@property(nonatomic, copy) NSString *name;

@end

@implementation LY_FansLevelView

- (UIButton *)levelBtn {
    if (!_levelBtn) {
        _levelBtn = [[UIButton alloc] init];
        [_levelBtn setTitleColor:[UIColor colorWithHexString:@"#CC72ED"] forState:UIControlStateNormal];
        _levelBtn.contentMode = UIViewContentModeScaleAspectFit;
        _levelBtn.titleLabel.font = [UIFont mediumFontOfSize:9];
    }
    return _levelBtn;
}

- (UIImageView *)nameBgImgView {
    if (!_nameBgImgView) {
        _nameBgImgView = [[UIImageView alloc] init];
        _nameBgImgView.contentMode = UIViewContentModeScaleAspectFill;
        _nameBgImgView.layer.masksToBounds = true;
        _nameBgImgView.layer.cornerRadius = 7;
    }
    return _nameBgImgView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = [UIColor whiteColor];
        _nameLabel.font = [UIFont mediumFontOfSize:12];
//        _nameLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _nameLabel;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
        [self setupDefaultData];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.nameBgImgView];
    [self addSubview:self.levelBtn];
    [self addSubview:self.nameLabel];
}

- (void)setupUIFrame {
    [self.nameBgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 3, 0, 0));
    }];
    [self.levelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.top.bottom.mas_equalTo(self);
        make.width.mas_equalTo(17);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.levelBtn.mas_trailing).offset(3);
        make.top.bottom.mas_equalTo(self);
        make.trailing.mas_equalTo(self);
    }];
}

- (void)setupDefaultData {
    [self setLevel:0 andName:@"爱情首恶网这"];
}

- (void)setLevel:(NSInteger)level andName:(NSString *)name {
    if (self.level == level && [self.name isEqualToString:name]) {
        return;
    }
    self.level = level;
    self.name = name;
    
    // 获取文本Size
    CGSize size = [name sizeWithFont:[UIFont mediumFontOfSize:12] andMaxSize:CGSizeMake(kScreenW - 8, 30)];
    
    // 设置名字
    self.nameLabel.text = name;

    // 更新名字按钮宽度
    [self.nameBgImgView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(size.width + 17 + 5);
    }];
    
    // 设置等级数
    [self.levelBtn setTitle:[NSString stringWithFormat:@"%ld", (long)level] forState:UIControlStateNormal];
    // 生成渐变背景图片
    // 1.渐变色数组
    NSArray *colors;
    NSString *levelBgImgName;
    if (level < 5) {
        colors = @[[UIColor colorWithHexString:@"#D075EE"], [UIColor colorWithHexString:@"#FF99C8"]];
        levelBgImgName = @"img_zhibo_fensituan_biaoqian1_10";
    } else if (level < 10) {
        colors = @[[UIColor colorWithHexString:@"#A553DC"], [UIColor colorWithHexString:@"#D45FE3"]];
        levelBgImgName = @"img_zhibo_fensituan_biaoqian11_20";
    } else {
        colors = @[[UIColor colorWithHexString:@"#6A2CF7"], [UIColor colorWithHexString:@"#A046FB"]];
        levelBgImgName = @"img_zhibo_fensituan_biaoqian21_30";
    }
    // 生成渐变图片
    UIImage *nameBgImg = [[UIImage alloc] initWithGradient:colors size:CGSizeMake(size.width / 2, 15) direction:UIImageGradientColorsDirectionHorizontal];
    // 设置名字背景图片
    self.nameBgImgView.image = nameBgImg;
    // 设置等级背景图片
    [self.levelBtn setBackgroundImage:[UIImage imageNamed:levelBgImgName] forState:UIControlStateNormal];
}

@end
