//
//  LY_LivePlayMusicView.h
//  SimpleDate
//
//  Created by 杨宝龙 on 2020/5/28.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LYPCustomActionSheetView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_LivePlayMusicView : LYPCustomActionSheetView

@end

NS_ASSUME_NONNULL_END
