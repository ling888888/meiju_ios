//
//  HWWaveView.h
//  HWProgress
//
//  Created by sxmaps_w on 2017/3/3.
//  Copyright © 2017年 hero_wqb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWWaveView : UIView

@property (nonatomic, assign) CGFloat progress;

@property (nonatomic, strong) UIColor * fillColor;//填充颜色
@property (nonatomic, strong) UIColor * topColor;//前面波浪颜色
@property (nonatomic, strong) UIColor * bottomColor;//后面波浪颜色

- (void)removeDisplayLinkAction;

@end
