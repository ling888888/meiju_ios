//
//  RocketWaveView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/9/3.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, RocketWaveViewType) {
    RocketWaveViewTypeLive,
    RocketWaveViewTypeChatroom
};

@interface RocketWaveView : UIView

@property (nonatomic, assign) CGFloat progress;// 0~1

@property (nonatomic, assign) RocketWaveViewType type;
- (void)removeDisplayLinkAction;

@end

NS_ASSUME_NONNULL_END
