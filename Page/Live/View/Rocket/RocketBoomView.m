//
//  RocketBoomView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/4.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "RocketBoomView.h"
#import "LY_PortraitView.h"
#import "LiveWorldMessage.h"
#import "SVGA.h"

@interface RocketBoomView ()<SVGAPlayerDelegate>

@property (nonatomic, strong) LY_PortraitView * portraitView; // 头像
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * descLabel; //“引爆直播间幸运火箭”
@property (nonatomic, strong) UILabel * sumLabel;
@property (nonatomic, strong) UILabel * sumDesLabel;
@property (nonatomic, strong) SVGAImageView * player;
@property (nonatomic, copy) NSMutableAttributedString * sumStr;
@end

@implementation RocketBoomView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = NO;
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews {
    
    self.player = [SVGAImageView new];
    [self addSubview:self.player];
    self.player.autoPlay = YES;
    self.player.delegate = self;
    self.player.loops = 1;
    self.player.fillMode = @"";
    self.player.contentMode = UIViewContentModeScaleAspectFit;
    [self.player mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
    
    [self addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(54.5/667 *kScreenH);
        make.size.mas_equalTo(145.0f/667 *kScreenH);
    }];
    [self addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(self.portraitView.mas_bottom).offset(0);
        make.leading.mas_offset(10);
        make.trailing.mas_offset(-10);
    }];
    
    [self addSubview:self.descLabel];
    [self.descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(self.nameLabel.mas_bottom).offset(14);
    }];
    
    self.sumLabel = [[UILabel alloc] init];
    self.sumLabel.numberOfLines = 0;
    self.sumLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.sumLabel];
    [self.sumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(self.nameLabel.mas_bottom).offset(32);
        make.leading.mas_offset(10);
        make.trailing.mas_equalTo(-10);
    }];
    
    self.sumDesLabel = [[UILabel alloc] init];
    [self addSubview:self.sumDesLabel];
    [self.sumDesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(self.sumLabel.mas_bottom).offset(44);
    }];
    self.sumLabel.transform = CGAffineTransformScale(self.transform, 0, 0);
    self.sumDesLabel.transform = CGAffineTransformScale(self.transform, 0, 0);

}


- (void)setLiveWorldMsg:(LiveWorldMessage *)liveWorldMsg {
    _liveWorldMsg = liveWorldMsg;
    [self.portraitView.imageView fp_setImageWithURLString:_liveWorldMsg.senderUserAvatar];
    self.portraitView.headwearImageView.image = [UIImage imageNamed:@"ic_yinbaohuojian_toushi"];
    self.nameLabel.text = _liveWorldMsg.senderUserName;
    self.descLabel.text = [_liveWorldMsg.resource isEqualToString:@"liveRoom"] ? @"引爆直播间幸运火箭".localized: @"引爆聊天室幸运火箭".localized;
    self.player.imageName = [_liveWorldMsg.resource isEqualToString:@"liveRoom"] ? @"live_rocket_boom": @"chatroom_rocket_boom";
    
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowBlurRadius = 9;
    shadow.shadowColor = [UIColor colorWithRed:254/255.0 green:252/255.0 blue:114/255.0 alpha:1.0];
    shadow.shadowOffset = CGSizeMake(0,3);
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",_liveWorldMsg.rewardNumber] attributes:@{NSFontAttributeName: [UIFont fontWithName:@"Arial-BoldItalicMT" size:56],NSForegroundColorAttributeName: [UIColor colorWithRed:252/255.0 green:234/255.0 blue:16/255.0 alpha:1.0], NSShadowAttributeName: shadow, NSStrokeWidthAttributeName:@-2,NSStrokeColorAttributeName: [UIColor colorWithRed:190/255.0 green:121/255.0 blue:39/255.0 alpha:1.0]
    }];
    self.sumLabel.attributedText = string;
   
    
    NSMutableAttributedString *string1 = [[NSMutableAttributedString alloc] initWithString:@"金币".localized attributes:@{NSFontAttributeName: [UIFont fontWithName:@"Arial-BoldItalicMT" size:56],NSForegroundColorAttributeName: [UIColor colorWithRed:252/255.0 green:234/255.0 blue:16/255.0 alpha:1.0], NSShadowAttributeName: shadow, NSStrokeWidthAttributeName:@-2,NSStrokeColorAttributeName: [UIColor colorWithRed:190/255.0 green:121/255.0 blue:39/255.0 alpha:1.0]
    }];
    self.sumDesLabel.attributedText = string1;

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.3 animations:^{
            self.sumLabel.transform = CGAffineTransformScale(self.transform, 1, 1);
            self.sumDesLabel.transform = CGAffineTransformScale(self.transform, 1, 1);
            
        }];
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self rain];
    });

}

- (void)setChatroomWorldMsg:(SPDWorldChatMessage *)chatroomWorldMsg {
    _chatroomWorldMsg = chatroomWorldMsg;
    [self.portraitView.imageView fp_setImageWithURLString:_chatroomWorldMsg.senderUserInfo.portraitUri];
     self.portraitView.headwearImageView.image = [UIImage imageNamed:@"ic_yinbaohuojian_toushi"];
     self.nameLabel.text = _chatroomWorldMsg.senderUserInfo.name;
     self.descLabel.text = [_chatroomWorldMsg.resource isEqualToString:@"liveRoom"] ? @"引爆直播间幸运火箭".localized: @"引爆聊天室幸运火箭".localized;
     self.player.imageName = [_chatroomWorldMsg.resource isEqualToString:@"liveRoom"] ? @"live_rocket_boom": @"chatroom_rocket_boom";
     
     NSShadow *shadow = [[NSShadow alloc] init];
     shadow.shadowBlurRadius = 9;
     shadow.shadowColor = [UIColor colorWithRed:254/255.0 green:252/255.0 blue:114/255.0 alpha:1.0];
     shadow.shadowOffset = CGSizeMake(0,3);
     NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",_chatroomWorldMsg.rewardNumber] attributes:@{NSFontAttributeName: [UIFont fontWithName:@"Arial-BoldItalicMT" size:56],NSForegroundColorAttributeName: [UIColor colorWithRed:252/255.0 green:234/255.0 blue:16/255.0 alpha:1.0], NSShadowAttributeName: shadow, NSStrokeWidthAttributeName:@-2,NSStrokeColorAttributeName: [UIColor colorWithRed:190/255.0 green:121/255.0 blue:39/255.0 alpha:1.0]}];
     self.sumLabel.attributedText = string;
     
     NSMutableAttributedString *string1 = [[NSMutableAttributedString alloc] initWithString:@"金币".localized attributes:@{NSFontAttributeName: [UIFont fontWithName:@"Arial-BoldItalicMT" size:56],NSForegroundColorAttributeName: [UIColor colorWithRed:252/255.0 green:234/255.0 blue:16/255.0 alpha:1.0], NSShadowAttributeName: shadow, NSStrokeWidthAttributeName:@-2,NSStrokeColorAttributeName: [UIColor colorWithRed:190/255.0 green:121/255.0 blue:39/255.0 alpha:1.0]
     }];
     self.sumDesLabel.attributedText = string1;

     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
         [UIView animateWithDuration:0.3 animations:^{
             self.sumLabel.transform = CGAffineTransformScale(self.transform, 1, 1);
             self.sumDesLabel.transform = CGAffineTransformScale(self.transform, 1, 1);
             
         }];
     });
     
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
         [self rain];
     });
}

- (void)hide {
    self.nameLabel.text = @"";
    self.sumLabel.text = @"";
    self.sumDesLabel.text = @"";
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - svga delegate

- (void)svgaPlayerDidFinishedAnimation:(SVGAPlayer *)player {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self hide];
    });
}

- (void)rain{
//    1、设置CAEmitterLayer
    CAEmitterLayer *rainLayer = [CAEmitterLayer layer];
    
//    2、在背景图上添加粒子图层
    [self.player.layer addSublayer:rainLayer];
//    self.rainlayer = rainLayer;
    
//    3、发射形状--线性
    rainLayer.emitterShape = kCAEmitterLayerLine;
    //发射模式
    rainLayer.emitterMode = kCAEmitterLayerSurface;
    //发射形状的大小
    rainLayer.emitterSize = self.frame.size;
    //发射的中心点位置
    rainLayer.emitterPosition = CGPointMake(self.bounds.size.width*0.5, -10);
    
//    4、配置cell
    CAEmitterCell *cell = [CAEmitterCell emitterCell];
    //粒子图片
    cell.contents = (id)[[UIImage imageNamed:@"boom_rocket_coin"] CGImage];
    //每秒钟创建的粒子对象，默认是0
    cell.birthRate = 8.0;
    //粒子的生存周期，以s为单位，默认是0
    cell.lifetime = 30;
    //粒子发射的速率，默认是1
    cell.speed = 1;
    //粒子的初始平均速度及范围，默认为0
//    cell.velocity = 5.0f;
//    cell.velocityRange = 10.0f;
    //y方向的加速度矢量,默认是0
    cell.yAcceleration = 200;
    //粒子的缩放比例及范围，默认是[1,0]
    cell.scale = 0.5;
    cell.scaleRange = 0.0f;
    
//    5、添加到图层上
    rainLayer.emitterCells = @[cell];
}

#pragma mark - getters

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [LY_PortraitView new];
    }
    return _portraitView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.textColor = SPD_HEXCOlOR(@"#FFFA57");
        _nameLabel.font = [UIFont boldSystemFontOfSize:18];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _nameLabel;
}

- (UILabel *)descLabel {
    if (!_descLabel) {
        _descLabel = [UILabel new];
        _descLabel.textColor = SPD_HEXCOlOR(@"#FCEA10");
        _descLabel.font = [UIFont boldSystemFontOfSize:15];
    }
    return _descLabel;
}

- (void)dealloc
{
    NSLog(@"boom dealloc");
}

@end
