//
//  RocketWaveView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/3.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "RocketWaveView.h"
#import "HWWaveView.h"
#import "UIView+RGSize.h"

@interface RocketWaveView ()

@property (nonatomic, strong) UIImageView * bgImageView;
@property (nonatomic, strong) HWWaveView * waveView;
@property (nonatomic, strong) UIImageView * centerImageView;

@end

@implementation RocketWaveView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews {
    [self addSubview:self.waveView];

    [self addSubview:self.bgImageView];
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self addSubview:self.centerImageView];
    [self.centerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(28, 36));
    }];
}

- (void)setProgress:(CGFloat)progress {
    if (progress >= 0.8 && _progress < 0.8) {
        [self startIn];
    }
    _progress = progress;
    _waveView.progress = _progress == 0 ? 0.1 :_progress;
}

- (void)removeDisplayLinkAction {
    [self.waveView removeDisplayLinkAction];
}

- (void)startIn {
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.centerImageView.transform = CGAffineTransformMakeTranslation(0, 1);
    } completion:^(BOOL finished) {
        self.centerImageView.transform = CGAffineTransformIdentity;
        if (_progress >=0.8) {
            [self startIn];
        }
    }];
}

- (void)setType:(RocketWaveViewType)type {
    _type = type;
    switch (_type) {
        case RocketWaveViewTypeLive:{
            self.bgImageView.image = [UIImage imageNamed:@"btn_xingyunhuojian_zhibojian"];
            self.centerImageView.image = [UIImage imageNamed:@"ic_zhibojian_xingyunhuojian"];
            self.waveView.fillColor = [UIColor colorWithRed:201/255.0 green:237/255.0 blue:255/255.0 alpha:1.0f];
            self.waveView.topColor = [UIColor colorWithRed:217/255.0 green:45/255.0 blue:255/255.0 alpha:1.0f];
            self.waveView.bottomColor = [UIColor colorWithRed:217/255.0 green:45/255.0 blue:255/255.0 alpha:0.5];
            break;
        }
        case RocketWaveViewTypeChatroom:{
            self.bgImageView.image = [UIImage imageNamed:@"btn_liaotianshi_xingyunhuojian"];
            self.centerImageView.image = [UIImage imageNamed:@"ic_liaotianshi_xingyunhuojian"];
            self.waveView.fillColor = [UIColor colorWithRed:201/255.0 green:237/255.0 blue:255/255.0 alpha:1.0f];
            self.waveView.topColor = [UIColor colorWithRed:0/255.0 green:132/255.0 blue:255/255.0 alpha:1.0f];
            self.waveView.bottomColor = [UIColor colorWithRed:0/255.0 green:132/255.0 blue:255/255.0 alpha:0.5];
            break;
        }
        default:
            break;
    }
}

- (UIImageView *)bgImageView {
    if (!_bgImageView) {
        _bgImageView = [UIImageView new];
    }
    return _bgImageView;
}

- (HWWaveView *)waveView {
    if (!_waveView) {
        _waveView = [[HWWaveView alloc]initWithFrame:CGRectMake(3, 3, 39, 39)];
    }
    return _waveView;
}

- (UIImageView *)centerImageView {
    if (!_centerImageView) {
        _centerImageView = [UIImageView new];
    }
    return _centerImageView;
}

@end
