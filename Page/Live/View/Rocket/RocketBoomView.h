//
//  RocketBoomView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/9/4.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveWorldMessage.h"
#import "SPDWorldChatMessage.h"

NS_ASSUME_NONNULL_BEGIN

@interface RocketBoomView : UIView

@property (nonatomic, strong) LiveWorldMessage * liveWorldMsg; // 直播间给这个值
@property (nonatomic, strong) SPDWorldChatMessage * chatroomWorldMsg; // 聊天室给这个值

@end


NS_ASSUME_NONNULL_END
