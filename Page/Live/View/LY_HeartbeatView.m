//
//  LY_HeartbeatView.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/16.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_HeartbeatView.h"

@interface LY_HeartbeatView ()

@property(nonatomic, strong) UILabel *heartbeatKeyLabel;

@property(nonatomic, strong) UILabel *heartbeatValueLabel;

//@property(nonatomic, strong) UILabel *addValueLabel;

@property(nonatomic, assign) NSInteger totalValue;

@end

@implementation LY_HeartbeatView

- (UILabel *)heartbeatKeyLabel {
    if (!_heartbeatKeyLabel) {
        _heartbeatKeyLabel = [[UILabel alloc] init];
        _heartbeatKeyLabel.textColor = [UIColor whiteColor];
        _heartbeatKeyLabel.font = [UIFont mediumFontOfSize:11];
        _heartbeatKeyLabel.text = @"心动值".localized;
    }
    return _heartbeatKeyLabel;
}

- (UILabel *)heartbeatValueLabel {
    if (!_heartbeatValueLabel) {
        _heartbeatValueLabel = [[UILabel alloc] init];
        _heartbeatValueLabel.textColor = [UIColor whiteColor];
        _heartbeatValueLabel.font = [UIFont mediumFontOfSize:11];
        _heartbeatValueLabel.text = @"0";
    }
    return _heartbeatValueLabel;
}

//- (UILabel *)addValueLabel {
//    if (!_addValueLabel) {
//        _addValueLabel = [[UILabel alloc] init];
//        _addValueLabel
//    }
//    return _addValueLabel;
//}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
//    self.backgroundColor = [[UIColor colorWithHexString:@"#FF6B93"] colorWithAlphaComponent:0.1];
    
    
    [self addSubview:self.heartbeatKeyLabel];
    [self addSubview:self.heartbeatValueLabel];
}

- (void)setupUIFrame {
    [self.heartbeatKeyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(10);
        make.centerY.mas_equalTo(self);
    }];
    
    [self.heartbeatValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.heartbeatKeyLabel.mas_trailing).offset(5);
        make.trailing.mas_equalTo(-10);
        make.centerY.mas_equalTo(self);
    }];
}

/// 设置初始值
- (void)setInitialValue:(NSInteger)value {
    self.totalValue = value;
    self.heartbeatValueLabel.text = [SPDCommonTool transformIntegerToBriefStr:value];
}

/// 增加值
- (void)addValue:(NSInteger)value {
    [self setInitialValue:self.totalValue + value];
    
    UILabel *addValueLabel = [[UILabel alloc] init];
    addValueLabel.textColor = [UIColor colorWithHexString:@"#E9BF87"];
    addValueLabel.font = [UIFont mediumFontOfSize:12];
    
    [self addSubview:addValueLabel];
    
    NSString *text = [NSString stringWithFormat:@"+%@", [SPDCommonTool transformIntegerToBriefStr:value]];
    
    addValueLabel.text = text;
    
    CGSize textSize = [text sizeWithAttributes:@{NSFontAttributeName: addValueLabel.font}];
    
    addValueLabel.frame = CGRectMake(self.heartbeatValueLabel.frame.origin.x, self.heartbeatValueLabel.frame.origin.y-12, textSize.width, textSize.height);
    
    [UIView animateWithDuration:0.4 animations:^{
        addValueLabel.frame = CGRectMake(self.heartbeatValueLabel.frame.origin.x, self.heartbeatValueLabel.frame.origin.y - 32, textSize.width, textSize.height);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 animations:^{
            addValueLabel.alpha = 0.0;
        } completion:^(BOOL finished) {
            [addValueLabel removeFromSuperview];
        }];
    }];
    
}

@end
