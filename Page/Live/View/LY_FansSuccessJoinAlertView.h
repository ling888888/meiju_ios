//
//  LY_FansSuccessJoinAlertView.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/22.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LYPCustomAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_FansSuccessJoinAlertView : LYPCustomAlertView

@end

NS_ASSUME_NONNULL_END
