//
//  QuickMessageBoardView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/4/26.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


@interface QuickMessageBoardView : UIView

@property (nonatomic, copy) NSArray * dataArray;
@end

NS_ASSUME_NONNULL_END
