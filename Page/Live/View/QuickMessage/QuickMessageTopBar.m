//
//  QuickMessageTopBar.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/24.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "QuickMessageTopBar.h"
#import "QuickMessageCell.h"
#import "NSString+XXWAddition.h"

@interface QuickMessageTopBar ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView * collectionView;

@property (nonatomic, strong) UIButton * statusButton;

@end

@implementation QuickMessageTopBar

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (void)setDataArray:(NSMutableArray *)dataArray {
    _dataArray = dataArray;
    [self.collectionView reloadData];
    if (kIsMirroredLayout) {
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
    }
}

- (void)initSubViews {
    [self addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.leading.mas_offset(0);
        make.bottom.mas_equalTo(0);
        make.width.mas_equalTo(kScreenW - 50);
    }];
    [self addSubview:self.statusButton];
    [self.statusButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.collectionView.mas_trailing).offset(0);
        make.bottom.mas_equalTo(0);
        make.width.mas_equalTo(50);
        make.top.mas_equalTo(0);
    }];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.dataArray.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
   return 1;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    QuickMessageCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QuickMessageCell" forIndexPath:indexPath];
    cell.contentLabel.text = self.dataArray[indexPath.section];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = [NSString sizeWithString:self.dataArray[indexPath.section] andFont:[UIFont mediumFontOfSize:12] andMaxSize:CGSizeMake(kScreenW, self.bounds.size.height)].width + 24;
    return CGSizeMake(width, self.bounds.size.height);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 5, 0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString * message = self.dataArray[indexPath.section];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"QucikMessageNotification" object:nil userInfo:@{@"message": message}];
}

- (void)didSelectStatusButton:(UIButton *)sender {
    [self.delegate quickMessageTopBar:self didClickedShowMoreButton:sender];
    sender.selected = !sender.selected;
}

#pragma mark - getter

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout new];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumInteritemSpacing = 5;
        layout.minimumLineSpacing = 5;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerNib:[UINib nibWithNibName:@"QuickMessageCell" bundle:nil] forCellWithReuseIdentifier:@"QuickMessageCell"];
        _collectionView.alwaysBounceHorizontal = YES;
        _collectionView.backgroundColor = [UIColor clearColor];
    }
    return _collectionView;
}

- (UIButton *)statusButton {
    if (!_statusButton) {
        _statusButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_statusButton setImage:[UIImage imageNamed:@"ic_zhibo_yonghu_gengduo"] forState:UIControlStateNormal];
        [_statusButton setImage:[UIImage imageNamed:@"ic_zhibo_yonghu_jianpan"] forState:UIControlStateSelected];
        [_statusButton addTarget:self action:@selector(didSelectStatusButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _statusButton;
}

@end
