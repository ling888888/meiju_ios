//
//  QuickMessageBoardView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/26.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "QuickMessageBoardView.h"
#import "QuickMessageCell.h"
#import "NSString+XXWAddition.h"

@interface QuickMessageBoardView ()

@property (nonatomic, strong) UIScrollView * scrollView;

@end

@implementation QuickMessageBoardView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (void)setDataArray:(NSArray *)dataArray {
    _dataArray = dataArray;
    CGFloat space = 20;
    CGFloat x = 5;
    CGFloat y = 5;
    CGFloat height = 26;
    CGFloat spaceY = 15;

    for (int i =0;i<_dataArray.count; i++) {
        UILabel * label = [UILabel new];
        label.text = _dataArray[i];
        label.font = [UIFont mediumFontOfSize:12];
        label.backgroundColor = [SPD_HEXCOlOR(@"1A1A1A") colorWithAlphaComponent:0.07];
        label.textColor = SPD_HEXCOlOR(@"1A1A1A");
        label.textAlignment = NSTextAlignmentCenter;
        label.layer.cornerRadius = 13;
        label.clipsToBounds = YES;
        label.tag = i;
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handle:)];
        [label addGestureRecognizer:tap];
        label.userInteractionEnabled = YES;
        [self.scrollView addSubview:label];
        
        CGFloat width = [NSString sizeWithString:self.dataArray[i] andFont:[UIFont mediumFontOfSize:12] andMaxSize:CGSizeMake(kScreenW - space*2 - 24, self.bounds.size.height)].width + 24;
        if (x + width + space >= kScreenW) {
            x= 5;
            y+= height + spaceY;
        }
        [self.scrollView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
              make.top.mas_equalTo(y);
              make.leading.mas_equalTo(x);
              make.width.mas_equalTo(width);
              make.height.mas_equalTo(height);
        }];
        x+= width +space;// interval
    }
    self.scrollView.contentSize = CGSizeMake(kScreenW, y+ height + 10);

}

-(void)handle:(UITapGestureRecognizer *)tap {
    NSString * message = self.dataArray[tap.view.tag];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"QucikMessageNotification" object:nil userInfo:@{@"message": message}];
}

- (void)initSubViews {
    [self addSubview:self.scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
       make.top.mas_equalTo(0);
       make.bottom.mas_equalTo(-IphoneX_Bottom);
       make.width.mas_equalTo(kScreenW);
   }];
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]init];
        _scrollView.backgroundColor = [UIColor whiteColor];
    }
    return _scrollView;
}

@end
