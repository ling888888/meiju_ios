//
//  QuickMessageTopBar.h
//  SimpleDate
//
//  Created by ling Hou on 2020/4/24.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class QuickMessageTopBar;
@protocol QuickMessageTopBarDeleagate <NSObject>

- (void)quickMessageTopBar:(QuickMessageTopBar *)topBar didClickedShowMoreButton:(UIButton *)sender;


@end

@interface QuickMessageTopBar : UIView

@property (nonatomic, strong) NSMutableArray * dataArray;
@property (nonatomic, weak)id<QuickMessageTopBarDeleagate>delegate;

@end

NS_ASSUME_NONNULL_END
