//
//  LiveRoomAnchorBottomViewCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/6/20.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveRoomAnchorBottomViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView * itemImageView ;
@property (nonatomic, strong) YYAnimatedImageView * anniImageView ;

@property (nonatomic, strong) UILabel * titleLabel ;

- (void)bindCover:(NSString * )cover title:(NSString *)titleName;

@property (nonatomic, assign) BOOL isPKItem;
@property (nonatomic, strong) UIImageView *dotImageView; // 连麦的红点点

@end

NS_ASSUME_NONNULL_END
