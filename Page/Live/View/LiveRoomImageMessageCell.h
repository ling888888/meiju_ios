//
//  LiveRoomImageMessageCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/27.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LY_BaseMessageCell.h"

NS_ASSUME_NONNULL_BEGIN

@class LiveImageMessage;

@protocol LiveRoomImageMessageCellDelegate <NSObject>

- (void)liveRoomImageMessageCell:(LiveImageMessage *)message;

@end

@interface LiveRoomImageMessageCell : LY_BaseMessageCell

//@property (nonatomic, strong) LiveImageMessage* message;
@property (nonatomic, weak)id <LiveRoomImageMessageCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
