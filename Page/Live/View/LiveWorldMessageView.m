//
//  LiveWorldMessageView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/8/24.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveWorldMessageView.h"
#import "LiveWorldTextMessageView.h"
#import "LiveWorldMessage.h"
#import "UIView+RGSize.h"
#import "LiveWorldGiftMessageView.h"
#import "SPDHelpController.h"
#import "ZegoKitManager.h"
#import "LiveModel.h"

#define CellWidth 344.0f
#define CellHeight 100.0f

@interface LiveWorldMessageView ()<UIGestureRecognizerDelegate>

@property (nonatomic, strong) NSMutableArray * messageArray; // 消息数组
@property (nonatomic, strong) NSMutableDictionary * reuseableViews;
@property (nonatomic, strong) NSMutableSet * allViews;
@property (nonatomic, assign) BOOL isBackgroundMode;

@end

@implementation LiveWorldMessageView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        self.isBackgroundMode = NO;
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapAction:)];
        tap.delegate = self;
        [self addGestureRecognizer:tap];
//        self.backgroundColor = [UIColor blueColor];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    return self;
}

- (void)enterForeground {
    self.isBackgroundMode = NO;
}

- (void)enterBackground {
    self.isBackgroundMode = YES;
}

- (void)handleTapAction:(UITapGestureRecognizer *)tap {
    if ( self.messageArray.count ==0 ) {
        return;
    }
    LiveWorldMessage * msg = self.messageArray[0];
    if ([msg.jumpType isEqualToString:@"liveRoom"]) {
        if (!ZegoManager.isAnchor && (![ZegoManager.liveInfo.liveId isEqualToString:msg.jumpId])) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"JoinInOtherLiveRoomNotification" object:nil userInfo:@{@"liveId":msg.jumpId}];
        }else{
            if (ZegoManager.isAnchor) {
                [self showTips:@"直播期间无法跳转".localized];
            }
        }
    }else if ([msg.jumpType isEqualToString:@"web"]){
        SPDHelpController * helpVC = [[SPDHelpController alloc]init];
        helpVC.url = msg.jumpId;
        [self.VC.navigationController pushViewController:helpVC animated:YES];
    }else if ([msg.jumpType isEqualToString:@"user"]){
        [SPDCommonTool pushToDetailTableViewController:self.VC userId:msg.jumpId];
    }
}

- (void)setLiveWorldMessage:(LiveWorldMessage *)liveWorldMessage {
    if (!self.isBackgroundMode) {
        if ([liveWorldMessage.worldMsgType isEqualToString:@"txt"] || [liveWorldMessage.worldMsgType isEqualToString:@"gift"]||[liveWorldMessage.worldMsgType isEqualToString:@"rocket"]) {
            [self.messageArray addObject:liveWorldMessage];
            if (self.messageArray.count == 1) {
                self.hidden = NO;
                [self initViewWithLiveWorldMessage:liveWorldMessage];
            }
        }
    }
}

- (void)initViewWithLiveWorldMessage:(LiveWorldMessage *)liveWorldMessage {
    if ([liveWorldMessage.worldMsgType isEqualToString:@"txt"] || [liveWorldMessage.worldMsgType isEqualToString:@"rocket"]) {
        // 文本消息
        LiveWorldTextMessageView *globalMessageCell;
                
        globalMessageCell.backgroundColor = [UIColor blueColor];
        NSMutableSet *set = self.reuseableViews[NSStringFromClass([LiveWorldTextMessageView class])];
        if (set.count) {
            globalMessageCell = [set anyObject];
            [set removeObject:globalMessageCell];
        } else {
            globalMessageCell = [[LiveWorldTextMessageView alloc] initWithFrame:CGRectMake(self.width, 0, CellWidth, CellHeight)];
            [self.allViews addObject:globalMessageCell];
        }
        [self addSubview:globalMessageCell];
        globalMessageCell.worldMessage = liveWorldMessage;
        [self beginScrollWithView:globalMessageCell];
        
    }else if ([liveWorldMessage.worldMsgType isEqualToString:@"gift"]) {
        // 礼物世界消息
        LiveWorldGiftMessageView *globalMessageCell;
        globalMessageCell.backgroundColor = [UIColor blueColor];
        NSMutableSet *set = self.reuseableViews[NSStringFromClass([LiveWorldGiftMessageView class])];
        if (set.count) {
            globalMessageCell = [set anyObject];
            [set removeObject:globalMessageCell];
        } else {
            globalMessageCell = [[LiveWorldGiftMessageView alloc] initWithFrame:CGRectMake(self.width, 0, CellWidth, CellHeight)];
            [self.allViews addObject:globalMessageCell];
        }
        [self addSubview:globalMessageCell];
        globalMessageCell.worldMessage = liveWorldMessage;
        [self beginScrollWithView:globalMessageCell];
        
    } else{
        [self.messageArray removeObjectAtIndex:0];
    }
}


- (void)finishAnimation:(UIView *)view {
    if (self.messageArray.count > 0) {
        [self.messageArray removeObjectAtIndex:0];
    }
    [self stopScrollWithView:view];
}

- (void)beginScrollWithView:(UIView *)view {
    [UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        CGRect vFrame = view.frame;
        vFrame.origin.x = (self.width - CellWidth)/2.0f;
        view.frame = vFrame;
    } completion:^(BOOL finished) {
       [self performSelector:@selector(finishAnimation:) withObject:view afterDelay:5];
    }];
}

-(void)stopScrollWithView:(UIView *)view {
    [UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        CGRect vFrame = view.frame;
        vFrame.origin.x = -CellWidth;
        view.frame = vFrame;
    } completion:^(BOOL finished) {
        [self resetView:view];
    }];
}

- (void)resetView:(UIView *)view {
    CGRect vFrame = view.frame;
    vFrame.origin.x = kScreenW;
    view.frame = vFrame;
    if ([view isKindOfClass:[LiveWorldGiftMessageView class]]) {
        LiveWorldGiftMessageView * v = (LiveWorldGiftMessageView *)view;
        [v.backgroundImageView stopAnimation];
    }
    if (self.messageArray.count > 0) {
        LiveWorldMessage * msg = (LiveWorldMessage *)self.messageArray[0];
        [self initViewWithLiveWorldMessage:msg];
    }
    if (self.messageArray.count == 0) {
        self.hidden = YES;
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isKindOfClass:[LiveWorldTextMessageView class]] || [touch.view isKindOfClass:[LiveWorldGiftMessageView class]] || [SVGAImageView class] ) {
        return YES;
    }
    return NO;
}

- (NSMutableSet *)allViews {
    if (!_allViews) {
        _allViews = [NSMutableSet set];
    }
    return _allViews;
}

- (NSMutableArray *)messageArray {
    if (!_messageArray) {
        _messageArray = [NSMutableArray new];
    }
    return _messageArray;
}

- (NSMutableDictionary *)reuseableViews {
    if (!_reuseableViews) {
        _reuseableViews = [NSMutableDictionary dictionary];
        [_reuseableViews setObject:[NSMutableSet set] forKey:NSStringFromClass([LiveWorldTextMessageView class])];
        [_reuseableViews setObject:[NSMutableSet set] forKey:NSStringFromClass([LiveWorldGiftMessageView class])];
    }
    return _reuseableViews;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"LiveWorldMessageView dealloc");
}

@end
