//
//  LiveRedPackIconView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/10/10.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveRedPackIconView.h"
#import "LiveRedPackModel.h"
#import "LiveRedPackView.h"

@interface LiveRedPackIconView ()<CAAnimationDelegate>

@property (nonatomic, strong) UIImageView * bgImageView;
@property (nonatomic, strong) UIImageView * avatarImageView;

@end


@implementation LiveRedPackIconView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapIcon:)];
        [self addGestureRecognizer:tap];
        [self addAnimation];
    }
    return self;
}

- (void)initSubviews {
    [self addSubview:self.bgImageView];
    self.bgImageView.image = [UIImage imageNamed:@"img_zhibojian_hongbao"];
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    [self addSubview:self.avatarImageView];
    [self.avatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.centerY.mas_equalTo(0);
        make.size.mas_equalTo(24);
    }];
}

- (void)setModel:(LiveRedPackModel *)model {
    _model = model;
    [self.avatarImageView fp_setImageWithURLString:_model.avatar];
}

- (void)addAnimation{
    CAKeyframeAnimation *shakeAnim = [CAKeyframeAnimation animation];
    shakeAnim.keyPath = @"transform.translation.x";
    shakeAnim.duration = 0.25;
    shakeAnim.delegate = self;
    shakeAnim.repeatCount = 4;
    CGFloat delta = 3;
    shakeAnim.values = @[@0,@(-delta),@(delta),@0];
    [self.layer addAnimation:shakeAnim forKey:@"shake"];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    if (flag) {
        [self performSelector:@selector(addAnimation) withObject:nil afterDelay:10];
    }
}

#pragma mark - 弹出领取view

- (void)handleTapIcon:(UITapGestureRecognizer *)tap {
    LiveRedPackView * view = [[LiveRedPackView alloc]initWithFrame:CGRectMake(0, 0, 255, 325)];
    view.model = _model;
    [view present];
}

#pragma mark - getters

- (UIImageView *)bgImageView {
    if (!_bgImageView) {
        _bgImageView = [UIImageView new];
    }
    return _bgImageView;
}

- (UIImageView *)avatarImageView {
    if (!_avatarImageView) {
        _avatarImageView = [UIImageView new];
        _avatarImageView.layer.cornerRadius =12;
        _avatarImageView.clipsToBounds = YES;
        _avatarImageView.layer.borderWidth = 1;
        _avatarImageView.layer.borderColor = SPD_HEXCOlOR(@"FDE9B3").CGColor;
    }
    return _avatarImageView;
}

- (void)dealloc
{
    NSLog(@"deallocdeallocdealloc");
   [self.layer removeAllAnimations];
}

@end
