//
//  LiveRedPackReceivedCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/10/10.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveRedPackReceivedCell : BaseTableViewCell

@property (nonatomic, copy) NSDictionary * dic;

@end

NS_ASSUME_NONNULL_END
