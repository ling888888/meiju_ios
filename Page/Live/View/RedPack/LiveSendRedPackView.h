//
//  LiveSendRedPackView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/10/12.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LYPCustomActionSheetView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveSendRedPackView : UIView

- (void)show ;

@end

NS_ASSUME_NONNULL_END
