//
//  LiveRedPackView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/10/10.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveRedPackView.h"
#import "LiveRedPackModel.h"
#import "LiveRedPackReceivedCell.h"
#import "ZegoKitManager.h"
#import "LiveModel.h"

@interface LiveRedPackView ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (nonatomic, strong) UIImageView * bgImageView; // 背景
@property (nonatomic, strong) UIView * receiveRedPacketView; // 领取红包页面

@property (nonatomic, strong) UIImageView * senderAvatarImageView;
@property (nonatomic, strong) UILabel * senderNameLabel;
@property (nonatomic, strong) UILabel * tipsLabel;// 派发红包中
@property (nonatomic, strong) UILabel * wordTitleLabel;// 发送口令，领取红包
@property (nonatomic, strong) UILabel * wordLabel;// 口令label
@property (nonatomic, strong) UIButton * wordPacketReceiveButton;
@property (nonatomic, strong) UIView * resultView;
@property (nonatomic, strong) UIButton * getAndSendButton; // 发送并领取
@property (nonatomic, strong) UIButton * getButton; // 领取大按钮

// 结果页面的UI

@property (nonatomic, strong) UIImageView * resultAvatarImageView;
@property (nonatomic, strong) UILabel * resultNameLabel;
@property (nonatomic, strong) UIImageView * conchImageView;
@property (nonatomic, strong) UILabel * conchLabel; // 贝壳
@property (nonatomic, strong) UILabel * numLabel; // 个数
@property (nonatomic, strong) UITableView * tableView; // tableview
@property (nonatomic, strong) NSMutableArray * robDataArray;

@end

@implementation LiveRedPackView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.containerView.backgroundColor = [UIColor clearColor];
        // 初始化
        [self initSubviews];
        // 先请求数据

    }
    return self;
}

- (void)initSubviews {
    
    [self.containerView addSubview:self.bgImageView];
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    
    // 结果
    [self.containerView addSubview:self.resultView];
    [self.resultView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(255, 325));
    }];
    
    
    // 领取的页面
    [self.containerView addSubview:self.receiveRedPacketView];
    [self.receiveRedPacketView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(255, 325));
    }];
    
    [self.receiveRedPacketView addSubview:self.senderAvatarImageView];
    [self.senderAvatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(23);
        make.size.mas_equalTo(66);
    }];
    
    [self.receiveRedPacketView addSubview:self.senderNameLabel];
    [self.senderNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.senderAvatarImageView.mas_bottom).offset(10);
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(21);
    }];
    
    [self.receiveRedPacketView addSubview:self.getButton];
    [self.getButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(108);
        make.top.equalTo(self.senderNameLabel.mas_bottom).offset(21);
    }];
    
//    [self.receiveRedPacketView addSubview:self.senderNameLabel];
//    [self.senderNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.senderAvatarImageView.mas_bottom).offset(10);
//        make.centerX.mas_equalTo(0);
//    }];
    
    [self.receiveRedPacketView addSubview:self.tipsLabel];
    [self.tipsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.senderNameLabel.mas_bottom).offset(10);
        make.centerX.mas_equalTo(0);
    }];
    
    [self.receiveRedPacketView addSubview:self.wordTitleLabel];
    [self.wordTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tipsLabel.mas_bottom).offset(47);
        make.centerX.mas_equalTo(0);
        make.leading.mas_equalTo(10);
        make.trailing.mas_equalTo(-10);
        make.height.mas_equalTo(20);
    }];
    
    [self.receiveRedPacketView addSubview:self.wordLabel];
    [self.wordLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.wordTitleLabel.mas_bottom).offset(21);
        make.centerX.mas_equalTo(0);
        make.leading.mas_equalTo(10);
        make.trailing.mas_equalTo(-10);
        make.height.mas_equalTo(20);
    }];
    
    [self.receiveRedPacketView addSubview:self.getAndSendButton];
    [self.getAndSendButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(160, 37));
        make.top.equalTo(self.wordLabel.mas_bottom).offset(27);
        make.bottom.mas_equalTo(-28);
    }];
    
    
}

- (void)setModel:(LiveRedPackModel *)model {
    _model = model;
    [self requestPackageList];
}

#pragma mark - reponse

- (void)handleRobWordRedpacket:(UIButton *)sender {
    sender.userInteractionEnabled = NO;
    if ([_model.type isEqualToString:@"0"]) {
        [_getButton setBackgroundImage:[UIImage imageNamed:@"btn_zhibojian_qiang_zhongwen(1)"] forState:UIControlStateNormal];
        CABasicAnimation* rotationAnimation;
        rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.y"];
        rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2];
        rotationAnimation.duration = 0.35;
        rotationAnimation.removedOnCompletion = YES;
        rotationAnimation.cumulative = YES;
        rotationAnimation.repeatCount = 2;
        [_getButton.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];//开始动画
        
    }
    [RequestUtils POST:URL_NewServer(@"live/live.red.package.rob") parameters:@{@"liveId":ZegoManager.liveInfo.liveId,@"type":[_model.type isEqualToString:@"0"] ? @(0):@(1) ,@"id":_model._id} success:^(id  _Nullable suceess) {
        self.resultView.hidden = NO;
        [UIView animateWithDuration:0.25 animations:^{
            self.receiveRedPacketView.alpha = 0;
        }];
        if ([suceess[@"type"] integerValue] == 1) {
            self.conchImageView.hidden = YES;
            [self.conchLabel mas_updateConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(0);
            }];
            self.conchLabel.text = @"手慢了，没抢到".localized;
        }else{
            if ([_model.type isEqualToString:@"1"]) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SendMessage" object:nil     userInfo:@{@"msg":_model.msg}];
            }else{
              //  我在%1s的红包中抢到了%2s贝壳,感谢！
                if ([[SPDCommonTool getFamyLanguage] containsString:@"zh"]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"SendMessage" object:nil     userInfo:@{@"msg":[NSString stringWithFormat:@"我在%@的红包中抢到了%@贝壳，感谢！".localized,suceess[@"senderName"],suceess[@"value"]]}];

                }else{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"SendMessage" object:nil     userInfo:@{@"msg":[NSString stringWithFormat:@"我在%@的红包中抢到了%@贝壳，感谢！".localized,suceess[@"value"],suceess[@"senderName"]]}];

                }
            }
            self.conchLabel.text = [NSString stringWithFormat:@"%@",suceess[@"value"]];
        }
        self.numLabel.text = [NSString stringWithFormat:@"%@ %d/%d",@"已领取".localized,[suceess[@"hasRobNumber"] intValue],[suceess[@"totalNumber"] intValue]];
        self.bgImageView.image = [UIImage imageNamed:@"img_zhibo_lingquhongbao"];
        self.resultNameLabel.text = suceess[@"senderName"];
        [self.resultAvatarImageView fp_setImageWithURLString:suceess[@"avatar"]];
        self.robDataArray = [suceess[@"users"] mutableCopy];
        [self.tableView reloadData];
        
    } failure:^(id  _Nullable failure) {
        sender.userInteractionEnabled = YES;
        [self showTips:failure[@"msg"]];
    }];
}

// request
- (void)requestPackageList {
    [RequestUtils GET:URL_NewServer(@"live/live.red.package.list") parameters:@{@"id":_model._id,@"userId":[SPDApiUser currentUser].userId} success:^(id  _Nullable suceess) {
        BOOL type = [suceess[@"type"] boolValue]; // 0用户未领取，1用户已领取
        if (type) {
            self.resultView.hidden = NO;
            self.receiveRedPacketView.hidden = YES;
            self.numLabel.text = [NSString stringWithFormat:@"已领取 %d/%d".localized,[suceess[@"hasRobNumber"] intValue],[suceess[@"totalNumber"] intValue]];
            self.bgImageView.image = [UIImage imageNamed:@"img_zhibo_lingquhongbao"];
            self.resultNameLabel.text = suceess[@"senderName"];
            [self.resultAvatarImageView fp_setImageWithURLString:suceess[@"senderAvatar"]];
            self.robDataArray = [suceess[@"list"] mutableCopy];
            [self.tableView reloadData];
            self.conchLabel.text = [suceess[@"myNumber"] stringValue];
        }else{
            // wei ling qu
            self.resultView.hidden = YES;
            self.receiveRedPacketView.hidden = NO;
            [self.senderAvatarImageView fp_setImageWithURLString:_model.avatar];
            self.senderNameLabel.text = _model.userName;
            [self.senderNameLabel sizeToFit];
            self.wordLabel.text = _model.msg;
            BOOL isCommonRedPack = [_model.type isEqualToString:@"0"];
            self.tipsLabel.hidden = isCommonRedPack;
            self.wordLabel.hidden = NO;
            self.wordTitleLabel.hidden = isCommonRedPack;
            self.getAndSendButton.hidden = isCommonRedPack;
            self.getButton.hidden = !isCommonRedPack;
            self.bgImageView.image = isCommonRedPack ? [UIImage imageNamed:@"img_zhibo_qianghongbao"] : [UIImage imageNamed:@"img_zhibo_qianghongbao"];
        }
     } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

#pragma mark - tableview

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.robDataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LiveRedPackReceivedCell  * cell = [LiveRedPackReceivedCell cellWithTableView:tableView];
    cell.dic = self.robDataArray[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 47;
}

#pragma mark - getters

- (UIView *)receiveRedPacketView {
    if (!_receiveRedPacketView) {
        _receiveRedPacketView = [UIView new];
        _receiveRedPacketView.backgroundColor = [UIColor clearColor];
        _receiveRedPacketView.hidden = YES;
    }
    return _receiveRedPacketView;
}

- (UILabel *)senderNameLabel {
    if (!_senderNameLabel) {
        _senderNameLabel = [UILabel new];
        _senderNameLabel.font = [UIFont mediumFontOfSize:16];
        _senderNameLabel.textAlignment = NSTextAlignmentCenter;
        _senderNameLabel.textColor = SPD_HEXCOlOR(@"ffffff");
    }
    return _senderNameLabel;
}

- (UILabel *)tipsLabel {
    if (!_tipsLabel) {
        _tipsLabel = [UILabel new];
        _tipsLabel.font = [UIFont mediumFontOfSize:13];
        _tipsLabel.text = @"派发红包中".localized;
        _tipsLabel.textColor = [UIColor whiteColor];
    }
    return _tipsLabel;
}

- (UILabel *)wordTitleLabel {
    if (!_wordTitleLabel) {
        _wordTitleLabel = [UILabel new];
        _wordTitleLabel.font = [UIFont mediumFontOfSize:12];
        _wordTitleLabel.text = @"发送口令，领取红包".localized;
        _wordTitleLabel.textColor = [UIColor whiteColor];
        _wordTitleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _wordTitleLabel;
}

- (UILabel *)wordLabel {
    if (!_wordLabel) {
        _wordLabel = [UILabel new];
        _wordLabel.font = [UIFont mediumFontOfSize:16];
        _wordLabel.textColor = SPD_HEXCOlOR(@"FFF476");
        _wordLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _wordLabel;
}

- (UIImageView *)bgImageView {
    if (!_bgImageView) {
        _bgImageView = [UIImageView new];
    }
    return _bgImageView;
}

- (UIImageView *)senderAvatarImageView {
    if (!_senderAvatarImageView) {
        _senderAvatarImageView = [UIImageView new];
        _senderAvatarImageView.layer.cornerRadius = 33;
        _senderAvatarImageView.clipsToBounds = YES;
    }
    return _senderAvatarImageView;
}

- (UIButton *)getButton {
    if (!_getButton) {
        _getButton = [UIButton buttonWithType:UIButtonTypeCustom];
        NSArray * array = [[SPDCommonTool getFamyLanguage] componentsSeparatedByString:@"-"];
        [_getButton setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"btn_zhibojian_qiang_%@",array[0]]] forState:UIControlStateNormal];
        [_getButton addTarget:self action:@selector(handleRobWordRedpacket:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _getButton;
}

- (UIButton *)getAndSendButton {
    if (!_getAndSendButton) {
        _getAndSendButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_getAndSendButton setBackgroundImage:[UIImage imageNamed:@"btn_zhibo_koulinghongbao"] forState:UIControlStateNormal];
        [_getAndSendButton setTitle:@"发送并领取".localized forState:UIControlStateNormal];
        [_getAndSendButton setTitleColor:SPD_HEXCOlOR(@"#F8145B") forState:UIControlStateNormal];
        _getAndSendButton.titleLabel.font = [UIFont mediumFontOfSize:15];
        [_getAndSendButton addTarget:self action:@selector(handleRobWordRedpacket:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _getAndSendButton;
}

- (UIView *)resultView {
    if (!_resultView) {
        _resultView = [UIView new];
        _resultView.hidden = YES;
        [_resultView addSubview:self.resultAvatarImageView];
        [self.resultAvatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(22);
            make.size.mas_equalTo(CGSizeMake(65, 65));
            make.centerX.mas_equalTo(0);
        }];
        
        [_resultView addSubview:self.resultNameLabel];
        self.resultNameLabel.textAlignment = NSTextAlignmentCenter;
        [self.resultNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.resultAvatarImageView.mas_bottom).offset(10);
            make.centerX.mas_equalTo(0);
            make.leading.mas_equalTo(10);
            make.trailing.mas_equalTo(-10);
        }];
        
        [_resultView addSubview:self.conchLabel];
        [self.conchLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.resultNameLabel.mas_bottom).offset(10);
            make.centerX.mas_equalTo(!kIsMirroredLayout ? - 11: 11);
        }];
        
        _conchImageView = [UIImageView new];
        _conchImageView.image = [UIImage imageNamed:@"ic_zhibo_hongbao_beike_da"];
        [_resultView addSubview:_conchImageView];
        [_conchImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(self.conchLabel.mas_trailing).offset(6);
            make.size.mas_equalTo(CGSizeMake(15, 12));
            make.centerY.equalTo(self.conchLabel.mas_centerY).offset(0);
        }];
        
        [_resultView addSubview:self.numLabel];
        [self.numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo(15);
            make.top.equalTo(_conchLabel.mas_bottom).offset(15);
            make.height.mas_equalTo(25);
        }];
        [_resultView addSubview:self.tableView];
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.and.bottom.and.trailing.mas_equalTo(0);
            make.top.equalTo(self.numLabel.mas_bottom).offset(0);
        }];
    }
    return _resultView;
}

- (UIImageView *)resultAvatarImageView {
    if (!_resultAvatarImageView) {
        _resultAvatarImageView = [UIImageView new];
        _resultAvatarImageView.layer.cornerRadius = 33;
        _resultAvatarImageView.clipsToBounds = YES;
    }
    return _resultAvatarImageView;
}

- (UILabel *)resultNameLabel {
    if (!_resultNameLabel) {
        _resultNameLabel = [UILabel new];
        _resultNameLabel.textColor = SPD_HEXCOlOR(@"1A1A1A");
        _resultNameLabel.font = [UIFont mediumFontOfSize:16];
    }
    return _resultNameLabel;
}

- (UILabel *)conchLabel {
    if (!_conchLabel) {
        _conchLabel = [UILabel new];
        _conchLabel.textColor = SPD_HEXCOlOR(@"F8145C");
        _conchLabel.font = [UIFont mediumFontOfSize:16];
    }
    return _conchLabel;
}

- (UILabel *)numLabel {
    if (!_numLabel) {
        _numLabel = [UILabel new];
        _numLabel.textColor = SPD_HEXCOlOR(@"1A1A1A");
        _numLabel.font = [UIFont mediumFontOfSize:11];
    }
    return _numLabel;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (NSMutableArray *)robDataArray {
    if (!_robDataArray) {
        _robDataArray = [NSMutableArray new];
    }
    return _robDataArray;
}

@end
