//
//  LiveRedPackReceivedCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/10/10.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveRedPackReceivedCell.h"

@interface LiveRedPackReceivedCell ()
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *conchImageView;
@property (weak, nonatomic) IBOutlet UILabel *conchLabel;

@end

@implementation LiveRedPackReceivedCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDic:(NSDictionary *)dic {
    _dic = dic;
    [self.avatarImageView fp_setImageWithURLString:_dic[@"avatar"]];
    self.nameLabel.text = _dic[@"userName"];
    self.conchLabel.text = [_dic[@"value"] stringValue];
}

@end
