//
//  LiveRedPackIconView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/10/10.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class  LiveRedPackModel;

@interface LiveRedPackIconView : UIView


@property (nonatomic, strong) LiveRedPackModel * model;

@end

NS_ASSUME_NONNULL_END
