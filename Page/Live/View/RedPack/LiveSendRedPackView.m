//
//  LiveSendRedPackView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/10/12.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveSendRedPackView.h"
#import "UIButton+HXExtension.h"
#import "ZegoKitManager.h"
#import "LiveModel.h"
#import "LY_LiveRechargeConchView.h"

@interface LiveSendRedPackView ()<UIGestureRecognizerDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (nonatomic, strong) UIView * containerView;
@property (nonatomic, strong) UILabel * titleLabel; // 发送红包
@property (nonatomic, strong) UIView * conchView;// 贝壳数view
@property (nonatomic, strong) UITextField * conchTextFeild;
@property (nonatomic, strong) UIView * packetView;// 红包数view
@property (nonatomic, strong) UITextField * packetTextFeild;
@property (nonatomic, strong) UILabel * tipsLabel;
@property (nonatomic, strong) UIButton * openWordRedPackButton;// 开启口令红包按钮
@property (nonatomic, strong) UIButton * sendButton; // 发红包
@property (nonatomic, strong) UILabel * balanceLabel;// 贝壳余额
@property (nonatomic, strong) UILabel * desc;

@property (nonatomic, strong) UIView * wordTextView; // 口令红包
@property (nonatomic, strong) UILabel * selectedWordTextLabel; // 所选的口令label
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * dataArray;
@property (nonatomic, strong) UIButton * numTipsButton;
@property (nonatomic, strong) UIButton * conchTipsButton;
@property (nonatomic, strong) NSMutableDictionary * configDict;
@property (nonatomic, assign) NSInteger banlance;

@end

@implementation LiveSendRedPackView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hanleViewTap:)];
        tap.delegate = self;
        [self addGestureRecognizer:tap];
        [self initSubViews];
        [self requestLiveMessage];
        [self requestConfig];
        [self requestConch];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.containerView addRounded:UIRectCornerTopLeft|UIRectCornerTopRight withRadius:20];
}

#pragma mark - request

- (void)requestLiveMessage {
    [RequestUtils GET:URL_NewServer(@"live/live.message.rec") parameters:@{@"type":@"1"} success:^(id  _Nullable suceess) {
        self.dataArray = suceess[@"messages"];
        self.selectedWordTextLabel.text = self.dataArray.count > 0 ? self.dataArray[0] :@"hello";
        [self.tableView reloadData];
    } failure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestConfig {
    [RequestUtils GET:URL_NewServer(@"live/live.red.package.config") parameters:@{} success:^(id  _Nullable suceess) {
        self.configDict = suceess;
    } failure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestSendRedPack {
    NSMutableDictionary * dict  = [NSMutableDictionary dictionaryWithDictionary:@{@"liveId":ZegoManager.liveInfo.liveId,@"amount":self.conchTextFeild.text,@"peopleNum":self.packetTextFeild.text}];
    if (self.openWordRedPackButton.selected) {
        [dict setValue:@"1" forKey:@"type"];
        [dict setValue:self.selectedWordTextLabel.text?:@"hello" forKey:@"msg"];
    }else{
        [dict setValue:@"0" forKey:@"type"];
    }
    [RequestUtils POST:URL_NewServer(@"live/live.red.package.send") parameters:dict success:^(id  _Nullable suceess) {
        self.sendButton.userInteractionEnabled = YES;
        //发送一个消息
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SendMessage" object:nil userInfo:@{@"msg":@"我在直播间里发了个红包！快来抢！".localized}];
    } failure:^(id  _Nullable failure) {
        self.sendButton.userInteractionEnabled = YES;
        [self showTips:failure[@"msg"]];
    }];
}

- (void)requestConch {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.balance" bAnimated:NO bAnimatedViewController:nil bSuccessDoSomething:^(id  _Nullable suceess) {
        self.balanceLabel.text = [NSString stringWithFormat:@"%@%@",@"贝壳余额：".localized,[suceess[@"conch"] stringValue]];
        self.banlance = [suceess[@"conch"] integerValue];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

#pragma mark - response

- (void)show {
    self.frame = CGRectMake(0, 0, kScreenW, kScreenH);
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.25 animations:^{
        [self.containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(kScreenH - (CGRectGetHeight(self.containerView.frame)));
            make.leading.mas_equalTo(0);
            make.width.mas_equalTo(kScreenW);
        }];
        [self layoutIfNeeded];
    }];
}

- (void)dissmiss {
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.25 animations:^{
        [self.containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(kScreenH);
            make.leading.mas_equalTo(0);
            make.width.mas_equalTo(kScreenW);
        }];
        [self layoutIfNeeded];
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)handleWordText:(UITapGestureRecognizer *)tap {
    self.tableView.hidden = !self.tableView.hidden;
    [self.tableView reloadData];
}

- (void)hanleViewTap:(UITapGestureRecognizer *)tap {
    [self dissmiss];
}

- (void)handleBalance:(UIButton *)sender {
    if ([self.conchTextFeild.text integerValue] > self.banlance) {
        [self dissmiss];
        dispatch_after(0.27, dispatch_get_main_queue(), ^{
            [[LY_LiveRechargeConchView new] present];
        });
    }
}


// 开启口令红包
- (void)handleOpenWordButton:(UIButton *)sender {
    sender.selected = !sender.selected;
    self.wordTextView.hidden = !sender.selected;
    if (self.wordTextView.hidden && !self.tableView.hidden) {
        self.tableView.hidden = YES;
    }
    [self endEditing:YES];// 回收键盘
    [UIView animateWithDuration:0.25 animations:^{
        [self.sendButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_tipsLabel.mas_bottom).offset(sender.selected ? 85: 30);
        }];
        [self layoutIfNeeded];
        
        [self.containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(kScreenH - (CGRectGetHeight(self.containerView.frame)));
            make.leading.mas_equalTo(0);
            make.width.mas_equalTo(kScreenW);
        }];
        [self layoutIfNeeded];
    }completion:^(BOOL finished) {
    }];
}

- (void)handleSendRedPack:(UIButton *)sender {
    [self endEditing:YES];
    sender.userInteractionEnabled = NO;
    [self dissmiss];
    [self requestSendRedPack];
}

- (void)initSubViews {
    [self addSubview:self.containerView];
    self.containerView.backgroundColor = SPD_HEXCOlOR(@"F8145B");
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kScreenH);
        make.leading.mas_equalTo(0);
        make.width.mas_equalTo(kScreenW);
    }];
    
    [self.containerView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(25);
        make.centerX.mas_equalTo(0);
    }];
    [self.containerView addSubview:self.conchView];
    [self.conchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(kScreenW < 375 ? (315.0f/375*kScreenW):315, 52));
        make.top.equalTo(self.titleLabel.mas_bottom).offset(25);
    }];
    [self.containerView addSubview:self.packetView];
    [self.packetView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(kScreenW < 375 ? (315.0f/375*kScreenW):315, 52));
        make.top.equalTo(self.conchView.mas_bottom).offset(25);
    }];
     
    // tips button
    [self.containerView addSubview:self.conchTipsButton];
    [self.conchTipsButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.conchView.mas_bottom).offset(-1);
        make.trailing.equalTo(self.conchView.mas_trailing).offset(0);;
    }];
    
    [self.containerView addSubview:self.numTipsButton];
    [self.numTipsButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.packetView.mas_bottom).offset(-1);
        make.trailing.equalTo(self.packetView.mas_trailing).offset(0);;
    }];
    
    [self.containerView addSubview:self.openWordRedPackButton];
    [self.openWordRedPackButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.packetView.mas_leading).offset(0);
        make.top.equalTo(self.packetView.mas_bottom).offset(20);
    }];
    
    _tipsLabel= [UILabel new];
    _tipsLabel.textColor = [UIColor whiteColor];
    _tipsLabel.text = @"需要发送对应文字才能领取红包".localized;
    _tipsLabel.font = [UIFont mediumFontOfSize:12];
    [self.containerView addSubview:_tipsLabel];
    [_tipsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.openWordRedPackButton.mas_leading).offset(12);
        make.top.equalTo(self.openWordRedPackButton.mas_bottom).offset(7);
    }];
    
    [self.containerView addSubview:self.sendButton];
    [self.sendButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(_tipsLabel.mas_bottom).offset(30);
        make.size.mas_equalTo(CGSizeMake(kScreenW < 375 ? (315.0f/375*kScreenW):315, 52));
    }];
    
    [self.containerView addSubview:self.balanceLabel];
    [self.balanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(_sendButton.mas_bottom).offset(10);
    }];
    self.balanceLabel.text = @"贝壳余额：1000"; //
    
    [self.containerView addSubview:self.desc];
    [self.desc mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(self.balanceLabel.mas_bottom).offset(7);
//        make.height.mas_equalTo(28);
        make.leading.mas_equalTo(10);
        make.trailing.mas_equalTo(-10);
        make.bottom.mas_equalTo(-13);
    }];
    
    [self.containerView addSubview:self.wordTextView];
    self.wordTextView.hidden = YES;
    [self.wordTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(kScreenW < 375 ? (315.0f/375*kScreenW):315, 47));
        make.top.equalTo(_tipsLabel.mas_bottom).offset(10);
    }];
    self.wordTextView.userInteractionEnabled = YES;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleWordText:)];
    [self.wordTextView addGestureRecognizer:tap];
    
    self.tableView.hidden = YES;
    [self.containerView addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(315, 141));
        make.top.equalTo(self.wordTextView.mas_bottom).offset(-2);
        make.centerX.mas_equalTo(0);
    }];
}
#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (touch.view == self.containerView) {
        [self endEditing:YES];
    }
    return touch.view == self;
}

#pragma mark - UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [[UITableViewCell alloc]init];
    cell.textLabel.text = self.dataArray[indexPath.row];
    cell.textLabel.font = [UIFont mediumFontOfSize:14];
    cell.textLabel.textColor = [self.selectedWordTextLabel.text isEqualToString:self.dataArray[indexPath.row]]? SPD_HEXCOlOR(@"F8145B"):SPD_HEXCOlOR(@"1A1A1A");
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.tableView.hidden = YES;
    self.selectedWordTextLabel.text = self.dataArray[indexPath.row];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidChange:(UITextField *)sender{
    NSInteger num = [_packetTextFeild.text integerValue];
    NSInteger packetStandardNum = [self.configDict[@"peopleNumLimit"] integerValue];
    
    NSInteger amountLimit = [self.configDict[@"amountLimit"] integerValue];
    NSInteger amountHeadLimit = [self.configDict[@"amountHeadLimit"] integerValue];
    NSInteger current = [_conchTextFeild.text integerValue];
    
    self.sendButton.selected = (num <= packetStandardNum && current >= amountLimit && current <= amountHeadLimit && num != 0 && current != 0);
    self.sendButton.userInteractionEnabled = (num <= packetStandardNum && current >= amountLimit && current <= amountHeadLimit && num != 0 && current != 0);
    
    if (sender == _packetTextFeild) {
        if (num > packetStandardNum ) {
            [self.numTipsButton setTitle:[NSString stringWithFormat:@"单次最多发%@个红包".localized,self.configDict[@"peopleNumLimit"]]  forState:UIControlStateNormal];
            self.numTipsButton.hidden = NO;
        }else{
            [self.numTipsButton setTitle:@""  forState:UIControlStateNormal];
            self.numTipsButton.hidden = YES;
        }
        
    }else{
        if (current < amountLimit) {
            [self.conchTipsButton setTitle:[NSString stringWithFormat:@"单个红包最少%@贝壳".localized,self.configDict[@"amountLimit"]]  forState:UIControlStateNormal];
            self.conchTipsButton.hidden = (current == 0);
        }else{
            if (current > amountHeadLimit) {
                [self.conchTipsButton setTitle:[NSString stringWithFormat:@"富豪你好，一次最多发%@贝壳".localized,self.configDict[@"amountHeadLimit"]]  forState:UIControlStateNormal];
                self.conchTipsButton.hidden = NO;
            }else{
                self.conchTipsButton.hidden = YES;
            }
        }
        if (current > self.banlance && current != 0) {
            self.conchTipsButton.hidden = NO;
            [self.conchTipsButton setTitle:@"余额不足，请充值>".localized  forState:UIControlStateNormal];
        }
    }
    
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (range.location == 0 && range.length == 0 && [string isEqualToString:@"0"]) {
        return NO;
    }
    if (textField == self.packetTextFeild) {
        if (self.packetTextFeild.text.length == 0 && [string isEqualToString:@"0"]) {
            return NO;
        }
        if (self.packetTextFeild.text.length >= 3) {
            textField.text = [textField.text substringToIndex:2];
        }// 产品需求 😂
    }else{
        if (self.conchTextFeild.text.length == 0 && [string isEqualToString:@"0"]) {
            return NO;
        }
        if (self.conchTextFeild.text.length >= 6) {
            textField.text = [textField.text substringToIndex:5];
        }// 产品需求 😂
    }
    return [self validateNumber:string];
}

- (BOOL)validateNumber:(NSString*)number {
    BOOL res = YES;
    NSCharacterSet* tmpSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    int i = 0;
    while (i < number.length) {
        NSString * string = [number substringWithRange:NSMakeRange(i, 1)];
        NSRange range = [string rangeOfCharacterFromSet:tmpSet];
        if (range.length == 0) {
            res = NO;
            break;
        }
        i++;
    }
    return res;
}

#pragma mark - keyboard

- (void)KeyboardWillShow:(NSNotification *)notification {
    CGRect keyboardBounds = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat animationDuration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^{
        [self.containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(kScreenH - (CGRectGetHeight(self.containerView.frame)) - keyboardBounds.size.height);
            make.leading.mas_equalTo(0);
            make.width.mas_equalTo(kScreenW);
        }];
        [self layoutIfNeeded];
    }];
}

- (void)KeyboardWillHide:(NSNotification *)notification {
//    UIViewAnimationCurve curve = [[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    CGFloat animationDuration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^{
        [self.containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(kScreenH - (CGRectGetHeight(self.containerView.frame)));
            make.leading.mas_equalTo(0);
            make.width.mas_equalTo(kScreenW);
        }];
        [self layoutIfNeeded];
    }];
}

#pragma mark - getters
 
- (UILabel *)desc {
    if (!_desc) {
        _desc= [UILabel new];
        _desc.text = @"单个红包金额随机发放。如有疑问，请联系客服。".localized;
        _desc.font = [UIFont mediumFontOfSize:12];
        _desc.textColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
        _desc.numberOfLines = 0;
        _desc.textAlignment = NSTextAlignmentCenter;
    }
    return _desc;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.font = [UIFont mediumFontOfSize:24];
        _titleLabel.text = @"发送红包".localized;
    }
    return _titleLabel;
}

- (UIView *)conchView {
    if (!_conchView) {
        _conchView = [UIView new];
        _conchView.backgroundColor = [UIColor whiteColor];
        _conchView.layer.cornerRadius = 26;
        UILabel * title = [UILabel new];
        title.textColor = SPD_HEXCOlOR(@"#1A1A1A");
        title.text = @"贝壳数".localized;
        title.font = [UIFont mediumFontOfSize:15];
        [_conchView addSubview:title];
        [title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(0);
            make.leading.mas_equalTo(20);
            make.width.mas_equalTo(70);
        }];
        [_conchView addSubview:self.conchTextFeild];
        [self.conchTextFeild mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.and.bottom.mas_equalTo(0);
            make.trailing.mas_equalTo(-15);
            make.leading.equalTo(title.mas_trailing).offset(10);
        }];
    }
    return _conchView;
}


- (UIView *)packetView {
    if (!_packetView) {
        _packetView = [UIView new];
        _packetView.backgroundColor = [UIColor whiteColor];
        _packetView.layer.cornerRadius = 26;
        UILabel * title = [UILabel new];
        title.textColor = SPD_HEXCOlOR(@"#1A1A1A");
        title.text = @"红包数".localized;
        title.font = [UIFont mediumFontOfSize:15];
        [_packetView addSubview:title];
        [title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(0);
            make.leading.mas_equalTo(20);
            make.width.mas_equalTo(70);
        }];
        [_packetView addSubview:self.packetTextFeild];
        [self.packetTextFeild mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.and.bottom.mas_equalTo(0);
            make.trailing.mas_equalTo(-15);
            make.leading.equalTo(title.mas_trailing).offset(10);
        }];
    }
    return _packetView;
}

- (UITextField *)conchTextFeild {
    if (!_conchTextFeild) {
        _conchTextFeild = [[UITextField alloc]init];
        _conchTextFeild.placeholder = @"填写个数".localized;
        _conchTextFeild.font = [UIFont mediumFontOfSize:22];
        _conchTextFeild.textColor = SPD_HEXCOlOR(@"F8145B");
        _conchTextFeild.delegate = self;
        _conchTextFeild.keyboardType = UIKeyboardTypeNumberPad;
//        _conchTextFeild.backgroundColor = [UIColor greenColor];
        _conchTextFeild.textAlignment = kIsMirroredLayout ? NSTextAlignmentLeft: NSTextAlignmentRight;
        _conchTextFeild.attributedPlaceholder = [[NSAttributedString alloc] initWithString:SPDStringWithKey(@"填写个数", nil) attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"BFBFBF"]}];
        [_conchTextFeild addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _conchTextFeild;
}

- (UITextField *)packetTextFeild {
    if (!_packetTextFeild) {
        _packetTextFeild = [[UITextField alloc]init];
        _packetTextFeild.placeholder = @"填写个数".localized;
        _packetTextFeild.font = [UIFont mediumFontOfSize:22];
        _packetTextFeild.textColor = SPD_HEXCOlOR(@"F8145B");
        _packetTextFeild.keyboardType = UIKeyboardTypeNumberPad;
//        _packetTextFeild.backgroundColor = [UIColor greenColor];
        _packetTextFeild.delegate = self;
        _packetTextFeild.textAlignment = kIsMirroredLayout ? NSTextAlignmentLeft: NSTextAlignmentRight;
        _packetTextFeild.attributedPlaceholder = [[NSAttributedString alloc] initWithString:SPDStringWithKey(@"填写个数", nil) attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"BFBFBF"]}];
        [_packetTextFeild addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _packetTextFeild;
}

- (UIButton *)openWordRedPackButton {
    if (!_openWordRedPackButton) {
        _openWordRedPackButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_openWordRedPackButton setTitle:@"开启口令红包".localized forState:UIControlStateNormal];
        [_openWordRedPackButton setImage:[UIImage imageNamed:@"ic_zhibo_hongbao_weikaiqi"] forState:UIControlStateNormal];
        [_openWordRedPackButton setImage:[UIImage imageNamed:@"ic_zhibo_hongbao_kaiqikoulinghongbao"] forState:UIControlStateSelected];
        _openWordRedPackButton.titleLabel.font = [UIFont mediumFontOfSize:12];
        [_openWordRedPackButton hx_setEnlargeEdgeWithTop:20 right:20 bottom:20 left:20];
        [_openWordRedPackButton setImagePosition:HHImagePositionLeft spacing:kIsMirroredLayout ?-5: 5];
        _openWordRedPackButton.selected = NO;
        [_openWordRedPackButton addTarget:self action:@selector(handleOpenWordButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _openWordRedPackButton;
}

- (UIButton *)sendButton {
    if (!_sendButton) {
        _sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_sendButton setTitle:@"发红包".localized forState:UIControlStateNormal];
        _sendButton.titleLabel.font = [UIFont mediumFontOfSize:20];
        [_sendButton setTitleColor:SPD_HEXCOlOR(@"#ffffff") forState:UIControlStateNormal];
        [_sendButton setBackgroundImage:[UIImage imageNamed:@"btn_zhibo_fahongbao"] forState:UIControlStateNormal];
        [_sendButton setBackgroundImage:[UIImage imageNamed:@"btn_zhibo_fahongbao_light"] forState:UIControlStateSelected];
        [_sendButton addTarget:self action:@selector(handleSendRedPack:) forControlEvents:UIControlEventTouchUpInside];
        _sendButton.layer.cornerRadius = 27;
        _sendButton.clipsToBounds = YES;
        _sendButton.userInteractionEnabled = NO;
    }
    return _sendButton;
}

- (UILabel *)balanceLabel {
    if (!_balanceLabel) {
        _balanceLabel = [UILabel new];
        _balanceLabel.textColor = SPD_HEXCOlOR(@"ffffff");
        _balanceLabel.font = [UIFont mediumFontOfSize:12];
    }
    return _balanceLabel;
}

- (UIView *)wordTextView {
    if (!_wordTextView) {
        _wordTextView = [UIView new];
        _wordTextView.layer.cornerRadius = 5;
        _wordTextView.clipsToBounds = YES;
        _wordTextView.backgroundColor = SPD_HEXCOlOR(@"FA4D86");
        [_wordTextView addSubview:self.selectedWordTextLabel];
        [self.selectedWordTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo(15);
            make.centerY.mas_equalTo(0);
        }];
        UIImageView * img = [UIImageView new];
        img.image = [UIImage imageNamed:@"ic_zhibo_fahongbao_jiantou"];
        [_wordTextView addSubview:img];
        [img mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(0);
            make.trailing.mas_equalTo(-10);
            make.size.mas_equalTo(CGSizeMake(8, 16));
        }];
    }
    return _wordTextView;
}

- (UILabel *)selectedWordTextLabel {
    if (!_selectedWordTextLabel) {
        _selectedWordTextLabel = [UILabel new];
        _selectedWordTextLabel.textColor = [UIColor whiteColor];
        _selectedWordTextLabel.font = [UIFont mediumFontOfSize:14];
    }
    return _selectedWordTextLabel;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.layer.cornerRadius = 2;
        _tableView.clipsToBounds = YES;
    }
    return _tableView;
}

- (UIView *)containerView {
    if (!_containerView) {
        _containerView = [UIView new];
    }
    return _containerView;
}

- (NSMutableDictionary *)configDict {
    if (!_configDict) {
        _configDict = [NSMutableDictionary new];
    }
    return _configDict;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

- (UIButton *)numTipsButton {
    if (!_numTipsButton) {
        _numTipsButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _numTipsButton.titleLabel.font = [UIFont mediumFontOfSize:12];
        [_numTipsButton setTitleColor:SPD_HEXCOlOR(@"#FFDD66") forState:UIControlStateNormal];
    }
    return _numTipsButton;
}

- (UIButton *)conchTipsButton {
    if (!_conchTipsButton) {
        _conchTipsButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _conchTipsButton.titleLabel.font = [UIFont mediumFontOfSize:12];
        [_conchTipsButton setTitleColor:SPD_HEXCOlOR(@"#FFDD66") forState:UIControlStateNormal];
        [_conchTipsButton addTarget:self action:@selector(handleBalance:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _conchTipsButton;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"LiveSendRedPackView dealloc");
}
@end
