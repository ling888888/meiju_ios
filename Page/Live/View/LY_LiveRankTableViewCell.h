//
//  LY_LiveRankTableViewCell.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/2.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_TableViewCell.h"
#import "LY_LiveAudience.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_LiveRankTableViewCell : LY_TableViewCell

@property(nonatomic, strong) LY_LiveAudience *liveAudience;

@end

NS_ASSUME_NONNULL_END
