//
//  LiveRoomGiftComboAnimationViewCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/1.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomGiftComboAnimationViewCell.h"
#import "LiveGiftMessage.h"
#import "SVGA.h"
#import "UIImage+LY.h"
#import "UIView+LY.h"


@interface LiveRoomGiftComboAnimationViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *giftNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *numLabel;
@property (weak, nonatomic) IBOutlet UIImageView *giftImageView;
@property (weak, nonatomic) IBOutlet SVGAImageView *player;
@property (weak, nonatomic) IBOutlet SVGAImageView *bgContentView;
@property (weak, nonatomic) IBOutlet SVGAImageView *svgaPlayer;
@property (nonatomic, strong) CAGradientLayer *gradientLayer;

@property(nonatomic, strong) LY_PortraitView *portraitView;

@end

@implementation LiveRoomGiftComboAnimationViewCell

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
        _portraitView.headwearHidden = true;
    }
    return _portraitView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.avatarImageView addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(self.avatarImageView).multipliedBy(1.5);
        make.height.mas_equalTo(self.avatarImageView).multipliedBy(1.5);
        make.center.mas_equalTo(self.avatarImageView);
    }];
}

- (void)setMessage:(LiveGiftMessage *)message {
    _message = message;
    self.svgaPlayer.hidden = !_message.isCombo;
    self.bgContentView.imageName = kIsMirroredLayout ? @"liwuxiaoxi_ar" :@"liwuxiaoxi";
//    [self.avatarImageView fp_setImageWithURLString:_message.senderUserAvatar];
    
    self.portraitView.portrait = message.portrait;
    
    self.nameLabel.text = _message.senderUserName;
    NSArray * lang = [[SPDCommonTool getFamyLanguage] componentsSeparatedByString:@"-"];
    self.giftNameLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"赠送 %@", nil),_message.giftName[lang[0]]];
    [self.giftImageView fp_setImageWithURLString:_message.giftCover];
    self.numLabel.text = [NSString stringWithFormat:@"x%@",_message.num];
    self.player.hidden = !_message.giftSvgaUrl.notEmpty;
    self.giftImageView.hidden = _message.giftSvgaUrl.notEmpty;
    if (_message.giftSvgaUrl.notEmpty) {
        self.player.imageName = _message.giftSvgaUrl;
    }

}

- (void)updateCountWithMessage:(LiveGiftMessage *)message{

    self.numLabel.text = [NSString stringWithFormat:@"x%@", message.isCombo ? @(message.comboValue):message.num];
    [self changeBackgroundColorWithCount:message.comboValue];
    self.svgaPlayer.hidden = !_message.isCombo;
    if (_message.isCombo) {
        self.svgaPlayer.imageName = @"chatroom_gift_combo";
    }
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = 0.5;
    animation.fillMode = kCAFillModeForwards;
    animation.values = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(2.0, 2.0, 1.0)],
                         [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.5, 0.5, 1.0)],
                         [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.2, 1.2, 1.0)],
                         [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.keyTimes = @[@0.0, @0.6, @0.9, @1.0];
    [self.numLabel.layer addAnimation:animation forKey:nil];
}

- (void)changeBackgroundColorWithCount:(NSInteger)count {
    if (count >= 297) {
        self.gradientLayer.colors = @[(__bridge id)[UIColor colorWithRed:110 / 255.0 green:86 / 255.0 blue:255 / 255.0 alpha:0.94].CGColor, (__bridge id)[UIColor colorWithRed:85 / 255.0 green:145 / 255.0 blue:255 / 255.0 alpha:0.94].CGColor];
    } else if (count >= 197) {
        self.gradientLayer.colors = @[(__bridge id)[UIColor colorWithRed:152 / 255.0 green:83 / 255.0 blue:255 / 255.0 alpha:0.94].CGColor, (__bridge id)[UIColor colorWithRed:221 / 255.0 green:143 / 255.0 blue:255 / 255.0 alpha:0.94].CGColor];
    } else if (count >= 97) {
        self.gradientLayer.colors = @[(__bridge id)[UIColor colorWithRed:255 / 255.0 green:181 / 255.0 blue:87 / 255.0 alpha:0.94].CGColor, (__bridge id)[UIColor colorWithRed:255 / 255.0 green:215 / 255.0 blue:103 / 255.0 alpha:0.94].CGColor];
    } else if (count >= 27) {
        self.gradientLayer.colors = @[(__bridge id)[UIColor colorWithRed:255 / 255.0 green:107 / 255.0 blue:147 / 255.0 alpha:0.94].CGColor, (__bridge id)[UIColor colorWithRed:255 / 255.0 green:149 / 255.0 blue:119 / 255.0 alpha:0.94].CGColor];
    } else {
        self.gradientLayer.colors = @[(__bridge id)[UIColor colorWithRed:26 / 255.0 green:26 / 255.0 blue:26 / 255.0 alpha:0.8].CGColor, (__bridge id)[UIColor colorWithRed:26 / 255.0 green:26 / 255.0 blue:26 / 255.0 alpha:0.8].CGColor];
    }
}

@end
