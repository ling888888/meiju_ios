//
//  LY_HeartbeatView.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/16.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_HeartbeatView : UIControl

/// 设置初始值
- (void)setInitialValue:(NSInteger)value;

/// 增加值
- (void)addValue:(NSInteger)value;

@end

NS_ASSUME_NONNULL_END
