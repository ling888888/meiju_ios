//
//  LY_FansJoinedView.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/20.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_FansJoinedView.h"
#import "LY_FansTaskTableViewCell.h"
#import "NSString+XXWAddition.h"

@interface LY_FansJoinedView () <UITableViewDataSource, UITableViewDelegate, LY_FansTaskTableViewCellDelegate, LY_FansJoinedViewHeaderViewDelegate>

@property(nonatomic, strong) LY_FansJoinedViewHeaderView *headerView;

@property(nonatomic, strong) UITableView *tableView;

@property(nonatomic, strong) NSArray *dataList;

@end

@implementation LY_FansJoinedView

- (LY_FansJoinedViewHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[LY_FansJoinedViewHeaderView alloc] init];
        _headerView.delegate = self;
    }
    return _headerView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.rowHeight = 75;
        [_tableView registerClass:[LY_FansTaskTableViewCell class] forCellReuseIdentifier:@"FansTaskIdentifier"];
    }
    return _tableView;
}

- (instancetype)initWithLiveId:(NSString *)liveId
{
    self = [super initWithFrame:CGRectZero];
    if (self) {
        self.liveId = liveId;
        
        [self setupUI];
        
        [self setupUIFrame];
        
        [self setupData];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.tableView];
    self.tableView.tableHeaderView = self.headerView;
    self.headerView.frame = CGRectMake(0, 0, kScreenW - 8, 115);
}

- (void)setupUIFrame {
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
}

- (void)setupData {
    NSDictionary *params = @{@"liveId": self.liveId};
    [LY_Network getRequestWithURL:LY_Api.live_fansGroup_taskList parameters:params success:^(id  _Nullable response) {
        NSMutableArray *models = [NSMutableArray array];
        for (NSDictionary *dict in response[@"list"]) {
            LY_FansTask *fansTask = [LY_FansTask yy_modelWithDictionary:dict];
            [models addObject:fansTask];
        }
        self.dataList = models;
        [self.tableView reloadData];
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

- (void)setLevel:(NSInteger)level andName:(NSString *)name {
    [self.headerView setLevel:level andName:name];
}

- (void)setLevel:(NSInteger)level nextLevel:(NSInteger)nextLevel intimacy:(NSInteger)intimacy nextIntimacy:(NSInteger)nextIntimacy progress:(CGFloat)progress {
    [self.headerView setLevel:level nextLevel:nextLevel intimacy:intimacy nextIntimacy:nextIntimacy progress:progress];
}

- (void)setText:(NSString *)text {
    _text = text;
    
    self.headerView.text = text;
}

- (void)setIsValid:(BOOL)isValid {
    _isValid = isValid;
    
    self.headerView.isValid = isValid;
    // 判断粉丝团是否有效
    if (isValid) {
        // 有效
        self.headerView.frame = CGRectMake(0, 0, kScreenW - 8, 130);
        [self.tableView reloadData];
    } else {
        // 失效
        self.headerView.frame = CGRectMake(0, 0, kScreenW - 8, 270);
        [self.tableView reloadData];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LY_FansTaskTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FansTaskIdentifier" forIndexPath:indexPath];
    
    cell.delegate = self;
    cell.isValid = self.isValid;
    cell.task = self.dataList[indexPath.row];
    
    
    
    return cell;
}

- (void)fansTaskTableViewCell:(LY_FansTaskTableViewCell *)fansTaskTableViewCell clickToFinishBtn:(UIButton *)toFinishBtn {
    // 判断任务类型
    if ([fansTaskTableViewCell.task.code isEqualToString:@"liveListen"]) {
        // 听直播任务
        if ([self.delegate respondsToSelector:@selector(fansJoinedView:clickHearLiveBtn:)]) {
            [self.delegate fansJoinedView:self clickHearLiveBtn:toFinishBtn];
        }
    } else if ([fansTaskTableViewCell.task.code isEqualToString:@"liveSendGift"]) {
        // 送礼物
        if ([self.delegate respondsToSelector:@selector(fansJoinedView:clickSendGiftBtn:)]) {
            [self.delegate fansJoinedView:self clickSendGiftBtn:toFinishBtn];
        }
    } else if ([fansTaskTableViewCell.task.code isEqualToString:@"liveSendMsg"]) {
        // 发消息
        if ([self.delegate respondsToSelector:@selector(fansJoinedView:clickSendMessageeBtn:)]) {
            [self.delegate fansJoinedView:self clickSendMessageeBtn:toFinishBtn];
        }
    } else if ([fansTaskTableViewCell.task.code isEqualToString:@"liveShare"]) {
        // 分享直播
        if ([self.delegate respondsToSelector:@selector(fansJoinedView:clickShareLiveBtn:)]) {
            [self.delegate fansJoinedView:self clickShareLiveBtn:toFinishBtn];
        }
    }
}

- (void)joinedViewHeaderView:(LY_FansJoinedViewHeaderView *)joinedViewHeaderView clickSendGiftBtn:(UIButton *)btn {
    // 送礼物
    if ([self.delegate respondsToSelector:@selector(fansJoinedView:clickSendGiftBtn:)]) {
        [self.delegate fansJoinedView:self clickSendGiftBtn:btn];
    }
}

@end

@interface LY_FansJoinedViewHeaderView ()

@property(nonatomic, strong) UIButton *levelBtn;

@property(nonatomic, strong) UIButton *nameBtn;

@property(nonatomic, strong) UIView *progressTrackView;

@property(nonatomic, strong) UIView *progressView;

@property(nonatomic, strong) UILabel *levelLabel;

@property(nonatomic, strong) UILabel *nextLevelLabel;

@property(nonatomic, strong) UILabel *textLabel;

@property(nonatomic, strong) UIButton *sendGiftBtn;

@property(nonatomic, strong) UILabel *fansTaskTitleLabel;

@end

@implementation LY_FansJoinedViewHeaderView

- (UIButton *)levelBtn {
    if (!_levelBtn) {
        _levelBtn = [[UIButton alloc] init];
//        [_levelBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [_levelBtn setTitleColor:[UIColor colorWithHexString:@"#CC72ED"] forState:UIControlStateNormal];
        _levelBtn.titleLabel.font = [UIFont mediumFontOfSize:12];
    }
    return _levelBtn;
}

- (UIButton *)nameBtn {
    if (!_nameBtn) {
        _nameBtn = [[UIButton alloc] init];
        [_nameBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _nameBtn.titleLabel.font = [UIFont mediumFontOfSize:15];
        _nameBtn.layer.cornerRadius = 15;
        _nameBtn.layer.masksToBounds = true;
//        _nameBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 38, 0, 15);
    }
    return _nameBtn;
}

- (UIView *)progressTrackView {
    if (!_progressTrackView) {
        _progressTrackView = [[UIView alloc] init];
        _progressTrackView.backgroundColor = [UIColor colorWithHexString:@"#EBEBEB"];
        _progressTrackView.layer.cornerRadius = 4;
        _progressTrackView.layer.masksToBounds = true;
    }
    return _progressTrackView;
}

- (UIView *)progressView {
    if (!_progressView) {
        _progressView = [[UIView alloc] init];
        _progressView.backgroundColor = [UIColor colorWithHexString:@"#FF6B93"];
        _progressView.layer.cornerRadius = 4;
        _progressView.layer.masksToBounds = true;
    }
    return _progressView;
}

- (UILabel *)levelLabel {
    if (!_levelLabel) {
        _levelLabel = [[UILabel alloc] init];
        _levelLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _levelLabel.font = [UIFont mediumFontOfSize:12];
    }
    return _levelLabel;
}

- (UILabel *)nextLevelLabel {
    if (!_nextLevelLabel) {
        _nextLevelLabel = [[UILabel alloc] init];
        _nextLevelLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _nextLevelLabel.font = [UIFont mediumFontOfSize:12];
    }
    return _nextLevelLabel;
}

//- (UILabel *)levelDescLabel {
//    if (!_levelDescLabel) {
//        _levelDescLabel = [[UILabel alloc] init];
//        _levelDescLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
//        _levelDescLabel.font = [UIFont mediumFontOfSize:12];
//        _levelDescLabel.textAlignment = NSTextAlignmentCenter;
//    }
//    return _levelDescLabel;
//}

- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] init];
        _textLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _textLabel.font = [UIFont mediumFontOfSize:15];
        _textLabel.numberOfLines = 0;
    }
    return _textLabel;
}

- (UIButton *)sendGiftBtn {
    if (!_sendGiftBtn) {
        _sendGiftBtn = [[UIButton alloc] init];
        [_sendGiftBtn setTitle:@"去送礼".localized forState:UIControlStateNormal];
        [_sendGiftBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _sendGiftBtn.titleLabel.font = [UIFont mediumFontOfSize:18];
        _sendGiftBtn.backgroundColor = [UIColor colorWithHexString:@"#6A2CF7"];
        _sendGiftBtn.layer.cornerRadius = 22.5;
        _sendGiftBtn.layer.masksToBounds = true;
        [_sendGiftBtn addTarget:self action:@selector(sendGiftBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _sendGiftBtn;
}

- (UILabel *)fansTaskTitleLabel {
    if (!_fansTaskTitleLabel) {
        _fansTaskTitleLabel = [[UILabel alloc] init];
        _fansTaskTitleLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _fansTaskTitleLabel.font = [UIFont mediumFontOfSize:15];
        _fansTaskTitleLabel.text = @"粉丝任务".localized;
    }
    return _fansTaskTitleLabel;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.nameBtn];
    [self addSubview:self.levelBtn];
    [self addSubview:self.progressTrackView];
    [self.progressTrackView addSubview:self.progressView];
    [self addSubview:self.levelLabel];
    [self addSubview:self.nextLevelLabel];
    [self addSubview:self.fansTaskTitleLabel];
}

- (void)setupUIFrame {
    [self.nameBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(10);
        make.height.mas_equalTo(30);
    }];
    [self.levelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.nameBtn).offset(-3);
        make.centerY.mas_equalTo(self.nameBtn);
        make.size.mas_equalTo(CGSizeMake(34, 30));
    }];
    [self.progressTrackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(10);
        make.trailing.mas_equalTo(-10);
        make.height.mas_equalTo(9);
        make.top.mas_equalTo(self.nameBtn.mas_bottom).offset(15);
    }];
    [self.progressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.bottom.mas_equalTo(self.progressTrackView);
        make.width.mas_equalTo(self.progressTrackView).multipliedBy(0.0);
    }];
    [self.levelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.progressTrackView.mas_bottom).offset(10);
        make.leading.mas_equalTo(self.progressTrackView);
    }];
    [self.nextLevelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.progressTrackView.mas_bottom).offset(10);
        make.trailing.mas_equalTo(self.progressTrackView);
    }];
    [self.fansTaskTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(10);
        make.bottom.mas_equalTo(-5);
    }];
}

- (void)setLevel:(NSInteger)level andName:(NSString *)name {
    [self.nameBtn setTitle:name forState:UIControlStateNormal];
    CGSize size = [NSString sizeWithString:name andFont:[UIFont mediumFontOfSize:15] andMaxSize:CGSizeMake(kScreenW - 8, 30)];
    [self.nameBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(size.width + 51);
    }];
    if (kIsMirroredLayout) {
        [self.nameBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -(size.width / 2 - 13), 0, 0)];
    } else {
        [self.nameBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -(size.width / 2 - 13))];
    }
    
    [self updateLevelViewWith:level];
}

- (void)setLevel:(NSInteger)level nextLevel:(NSInteger)nextLevel intimacy:(NSInteger)intimacy nextIntimacy:(NSInteger)nextIntimacy progress:(CGFloat)progress {
    
    [self.progressView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.leading.bottom.mas_equalTo(self.progressTrackView);
        make.width.mas_equalTo(self.progressTrackView).multipliedBy(progress);
    }];
    
    if (level == nextLevel) {
        self.levelLabel.text = [NSString stringWithFormat:@"LV.%ld (%ld/%@)", (long)level, (long)intimacy, @"已满级".localized];
        [self.nextLevelLabel setHidden:true];
    } else {
        self.levelLabel.text = [NSString stringWithFormat:@"LV.%ld (%ld/%ld)", (long)level, (long)intimacy, (long)nextIntimacy];
        [self.nextLevelLabel setHidden:false];
        self.nextLevelLabel.text = [NSString stringWithFormat:@"LV.%ld", (long)nextLevel];
    }
    
    
}

- (void)setText:(NSString *)text {
    _text = text;
    
    // 判断粉丝团是否有效
    if (self.isValid) {
        // 有效
    } else {
        // 失效
        self.textLabel.text = text;
    }
}

- (void)setIsValid:(BOOL)isValid {
    _isValid = isValid;
    
    // 判断粉丝团是否有效
    if (isValid) {
        // 有效
        self.progressView.backgroundColor = [UIColor colorWithHexString:@"#FF6B93"];
        if (_textLabel) {
            [_textLabel removeFromSuperview];
        }
        if (_sendGiftBtn) {
            [_sendGiftBtn removeFromSuperview];
        }
    } else {
        // 失效
        self.progressView.backgroundColor = [UIColor colorWithHexString:@"#D5D5D5"];
        
        [self addSubview:self.textLabel];
        [self addSubview:self.sendGiftBtn];
        [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo(10);
            make.trailing.mas_equalTo(-10);
            make.top.mas_equalTo(self.levelLabel.mas_bottom).offset(14);
        }];
        [self.sendGiftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(175, 45));
            make.top.mas_equalTo(self.textLabel.mas_bottom).offset(25);
            make.centerX.mas_equalTo(self);
        }];
        self.textLabel.text = self.text;
    }
}

- (void)updateLevelViewWith:(NSInteger)level {
    
    NSArray *colors;
    NSString *levelBgImgName;
    if (level < 5) {
        colors = @[[UIColor colorWithHexString:@"#D075EE"], [UIColor colorWithHexString:@"#FF99C8"]];
        levelBgImgName = @"img_zhibo_fensituan_biaoqian1_10";
    } else if (level < 10) {
        colors = @[[UIColor colorWithHexString:@"#A553DC"], [UIColor colorWithHexString:@"#D45FE3"]];
        levelBgImgName = @"img_zhibo_fensituan_biaoqian11_20";
    } else {
        colors = @[[UIColor colorWithHexString:@"#6A2CF7"], [UIColor colorWithHexString:@"#A046FB"]];
        levelBgImgName = @"img_zhibo_fensituan_biaoqian21_30";
    }
    // 判断粉丝团是否有效
    if (!self.isValid) {
        // 失效
        colors = @[[UIColor colorWithHexString:@"#C1C1C1"], [UIColor colorWithHexString:@"#DCDCDC"]];
        levelBgImgName = [NSString stringWithFormat:@"%@-1", levelBgImgName];
        [self.levelBtn setTitleColor:[UIColor colorWithHexString:@"#C1C1C1"] forState:UIControlStateNormal];
    } else {
        // 有效
        [self.levelBtn setTitleColor:[UIColor colorWithHexString:@"#CC72ED"] forState:UIControlStateNormal];
    }
    UIImage *nameBgImg = [[UIImage alloc] initWithGradient:colors size:self.nameBtn.frame.size direction:UIImageGradientColorsDirectionHorizontal];
    [self.nameBtn setBackgroundImage:nameBgImg forState:UIControlStateNormal];
    [self.levelBtn setBackgroundImage:[UIImage imageNamed:levelBgImgName] forState:UIControlStateNormal];
    
    [self.levelBtn setTitle:[NSString stringWithFormat:@"%ld", (long)level] forState:UIControlStateNormal];
}

- (void)sendGiftBtnAction {
    if ([self.delegate respondsToSelector:@selector(joinedViewHeaderView:clickSendGiftBtn:)]) {
        [self.delegate joinedViewHeaderView:self clickSendGiftBtn:self.sendGiftBtn];
    }
}

@end
