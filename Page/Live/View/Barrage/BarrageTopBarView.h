//
//  BarrageTopBarView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/4/26.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, BarrageType){
    BarrageType_Default = -1,
    BarrageType_Common = 0,
    BarrageType_Fans
};

@class BarrageTopBarView;

@protocol BarrageTopBarViewDelegate <NSObject>

@optional
- (void)barrageTopBarView:(BarrageTopBarView *)view didChangeBarrageType:(BarrageType)type;

@end

@interface BarrageTopBarView : UIView

@property (nonatomic, strong) LiveModel * liveModel;

@property (nonatomic, assign) BarrageType currentBarrageType;

@property (nonatomic, weak)id<BarrageTopBarViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
