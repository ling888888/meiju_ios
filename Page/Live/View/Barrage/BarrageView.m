//
//  BarrageView.m
//  Seeky
//
//  Created by 李楠 on 2019/8/15.
//  Copyright © 2019 简约互动. All rights reserved.
//

#import "BarrageView.h"
#import "BarrageSpriteProtocol.h"
#import "UIView+BarrageView.h"

@interface BarrageView ()<BarrageViewProtocol>

//@property (nonatomic, strong) CAGradientLayer *gradientLayer;
@property (nonatomic, strong) UIImageView *avatarImageView;
@property (nonatomic, strong) UILabel *nickNameLabel;
@property (nonatomic, strong) UILabel *colonLabel;
@property (nonatomic, strong) UILabel *contentLabel;

@end

@implementation BarrageView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews {
    self.clipsToBounds = YES;
    
//    self.gradientLayer = [CAGradientLayer layer];
//    self.gradientLayer.colors = @[(id)[UIColor colorWithHexString:@"1B1B1B"].CGColor, (id)[UIColor clearColor].CGColor];
//    self.gradientLayer.startPoint = CGPointMake([SPDCommonTool currentVersionIsArbic], 0.5);
//    self.gradientLayer.endPoint = CGPointMake(![SPDCommonTool currentVersionIsArbic], 0.5);
//    self.gradientLayer.cornerRadius = 11.5;
//    [self.layer addSublayer:self.gradientLayer];
    
    self.avatarImageView = [UIImageView new];
    self.avatarImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.avatarImageView.clipsToBounds = YES;
    self.avatarImageView.layer.cornerRadius = 12;
    [self addSubview:self.avatarImageView];
    [self.avatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(1);
        make.centerY.mas_equalTo(0);
        make.width.height.mas_equalTo(24);
    }];
    
    self.nickNameLabel = [UILabel new];
    self.nickNameLabel.font = [UIFont mediumFontOfSize:12];
    [self addSubview:self.nickNameLabel];
    [self.nickNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.avatarImageView.mas_trailing).offset(5);
        make.centerY.mas_equalTo(0);
    }];
    
    self.colonLabel = [UILabel new];
    self.colonLabel.text = @":";
    self.colonLabel.font = [UIFont mediumFontOfSize:12];
    self.colonLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.colonLabel];
    [self.colonLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.nickNameLabel.mas_trailing);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(10);
    }];
    
    self.contentLabel = [UILabel new];
    self.contentLabel.font = [UIFont mediumFontOfSize:12];
    [self addSubview:self.contentLabel];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.colonLabel.mas_trailing);
        make.centerY.mas_equalTo(0);
    }];
    self.layer.cornerRadius = 13;
    self.clipsToBounds = YES;
}

- (CGSize)sizeThatFits:(CGSize)size {
    [self.nickNameLabel sizeToFit];
    [self.contentLabel sizeToFit];
    CGFloat width = 1 + 21 + 5 + self.nickNameLabel.bounds.size.width + 10 + self.contentLabel.bounds.size.width + 25;
//    self.gradientLayer.frame = CGRectMake(0, 0, width, 23);
    return CGSizeMake(width, 26);
}

#pragma mark - BarrageViewProtocol

- (void)prepareForReuse {

}

- (void)configureWithParams:(NSDictionary *)params {
    [self.avatarImageView sd_setImageShowingActivityIndicatorWithURL:[NSURL URLWithString:params[@"avatar"]]];
    self.nickNameLabel.text = params[@"nick_name"];
    self.nickNameLabel.textColor = [UIColor colorWithHexString:[params[@"user_id"] isEqualToString:[SPDApiUser currentUser].userId] ? @"F52950" : @"C5B394"];
    self.contentLabel.text = params[@"content"];
    BOOL isFansBarrage = [params[@"isFansBarrage"] boolValue];
    self.nickNameLabel.textColor = SPD_HEXCOlOR(isFansBarrage ? @"ffffff":@"#1A1A1A");
    self.colonLabel.textColor = SPD_HEXCOlOR(isFansBarrage ? @"ffffff":@"#1A1A1A");
    self.contentLabel.textColor = SPD_HEXCOlOR(isFansBarrage ? @"ffffff":@"#1A1A1A");
    self.backgroundColor = isFansBarrage ? [SPD_HEXCOlOR(@"#FF6B93") colorWithAlphaComponent:0.9] : [SPD_HEXCOlOR(@"#FFFFFF") colorWithAlphaComponent:0.9];

}

@end
