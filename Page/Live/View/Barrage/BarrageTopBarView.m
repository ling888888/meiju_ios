//
//  BarrageTopBarView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/26.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "BarrageTopBarView.h"

@interface BarrageTopBarView ()
@property (weak, nonatomic) IBOutlet UIButton *commonBarrageBtn;
@property (weak, nonatomic) IBOutlet UIButton *fansBarrageBtn;
@property (weak, nonatomic) IBOutlet UIView *commonBarrageView;
@property (weak, nonatomic) IBOutlet UIView *fansBarrageView;
@property (weak, nonatomic) IBOutlet UILabel *commonLabel;
@property (weak, nonatomic) IBOutlet UILabel *fansLabel;
@property (weak, nonatomic) IBOutlet UIImageView *commonIcon;
@property (weak, nonatomic) IBOutlet UIImageView *fansIcon;

@end

@implementation BarrageTopBarView


- (void)awakeFromNib {
    [super awakeFromNib];
    self.commonBarrageBtn.selected = YES;
}

- (IBAction)commonBtn:(UIButton *)sender {
    if (sender.selected) {
           return;
    }
    sender.selected = ! sender.selected;
    self.fansBarrageBtn.selected = NO;
    [self resetBtnStyle];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(barrageTopBarView:didChangeBarrageType:)]) {
        [self.delegate barrageTopBarView:self didChangeBarrageType:BarrageType_Common];
    }
    self.currentBarrageType = BarrageType_Common;
}


- (IBAction)fansButton:(UIButton *)sender {
    // 需要根据粉丝等级判断 >=5 才可以发粉丝团弹幕
    
    if (self.liveModel.isLock) {
        [self showTips:@"当前粉丝团已冻结，恢复后可使用".localized];
        return;
    }
    
    if (self.liveModel.fansLevel < 5) {
        [self showTips:@"粉丝团到达5级即可免费发送".localized];
        return;
    }
    
    if (sender.selected) {
        return;
    }
    
    sender.selected = !sender.selected;
    
    self.commonBarrageBtn.selected = NO;
    [self resetBtnStyle];

    if (self.delegate && [self.delegate respondsToSelector:@selector(barrageTopBarView:didChangeBarrageType:)]) {
        [self.delegate barrageTopBarView:self didChangeBarrageType:BarrageType_Fans];
    }
    self.currentBarrageType = BarrageType_Fans;
}

- (void)resetBtnStyle {
    self.fansIcon.image = [UIImage imageNamed:self.fansBarrageBtn.selected ? @"ic_zhibo_fensidanmu_gaoliang":@"ic_zhibo_fensidanmu_zhihui"];
    [self.fansBarrageView setBackgroundColor:self.fansBarrageBtn.selected ? SPD_HEXCOlOR(@"FF6B93"):SPD_HEXCOlOR(@"ffffff")];
    self.commonLabel.textColor = !self.commonBarrageBtn.selected ? SPD_HEXCOlOR(@"1a1a1a"): SPD_HEXCOlOR(@"ffffff");

    self.commonIcon.image = [UIImage imageNamed:self.commonBarrageBtn.selected ? @"ic_zhibo_putongdanmu_gaoliang":@"ic_zhibo_putongdanmu_zhihui"];
    [self.commonBarrageView setBackgroundColor:self.commonBarrageBtn.selected ? SPD_HEXCOlOR(@"00d4a0"):SPD_HEXCOlOR(@"ffffff")];
    self.fansLabel.textColor = !self.fansBarrageBtn.selected ?SPD_HEXCOlOR(@"1a1a1a"): SPD_HEXCOlOR(@"ffffff");
}

@end
