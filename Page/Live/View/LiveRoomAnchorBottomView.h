//
//  LiveRoomAnchorBottomView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/23.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LiveRoomAnchorBottomView;

typedef NS_ENUM(NSInteger,LiveRoomAnchorBottomViewClickedItemType){
    LiveRoomAnchorBottomViewClickedType_SendMsg = 0,
    LiveRoomAnchorBottomViewClickedType_SoundEffect,
    LiveRoomAnchorBottomViewClickedType_PK,
    LiveRoomAnchorBottomViewClickedType_Music,
    LiveRoomAnchorBottomViewClickedType_Tool,
};

typedef NS_ENUM(NSInteger,LiveRoomAnchorPKStatus){
    LiveRoomAnchorPKDefalut = 0, // defalut
    LiveRoomAnchorPKMatchingStatus, // 匹配中
    LiveRoomAnchorPKInvitingStatus, // 正在邀请某些人
    LiveRoomAnchorPKInProcessStatus, // pk进行中
    LiveRoomAnchorPKInProcessUnable // pk状态不可用
};// 乱重新整理

@protocol LiveRoomAnchorBottomViewDelegate <NSObject>

@optional
-(void)liveRoomAnchorBottomView:(LiveRoomAnchorBottomView *)view didClickedWithType:(LiveRoomAnchorBottomViewClickedItemType)type;

@end

@interface LiveRoomAnchorBottomView : UIView

@property (nonatomic, weak)id<LiveRoomAnchorBottomViewDelegate>delegate;

@property (nonatomic, assign) BOOL showingAnchorView;

@property (nonatomic, assign) LiveRoomAnchorPKStatus pkStatus;

@end

NS_ASSUME_NONNULL_END
