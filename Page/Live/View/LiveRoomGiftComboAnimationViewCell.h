//
//  LiveRoomGiftComboAnimationViewCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/4/1.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LiveGiftMessage;
@interface LiveRoomGiftComboAnimationViewCell : UIView

@property (nonatomic, strong)LiveGiftMessage * message;
- (void)updateCountWithMessage:(LiveGiftMessage *)message;

@end

NS_ASSUME_NONNULL_END
