//
//  LiveRoomSystemMessageCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/24.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomSystemMessageCell.h"

@interface LiveRoomSystemMessageCell ()
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@end

@implementation LiveRoomSystemMessageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setMessage:(LiveRoomSystemMessage *)message {
    _message = message;
    NSMutableAttributedString * content = [[NSMutableAttributedString alloc]initWithString:_message.content];
    NSMutableParagraphStyle* paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineSpacing = 3;
    [content addAttribute:NSParagraphStyleAttributeName value:paraStyle range:NSMakeRange(0, content.length)];
    self.messageLabel.attributedText = content;
}

@end
