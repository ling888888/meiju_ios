//
//  LiveWorldTextMessageView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/8/24.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveWorldTextMessageView.h"
#import "LY_PortraitView.h"
#import "LiveWorldMessage.h"
#import "MarqueeLabel.h"
#import "LY_HPTagView.h"
#import "SVGAImageView.h"
@interface LiveWorldTextMessageView ()
@property (nonatomic, strong) UIImageView * backgroundView;
@property (nonatomic, strong) SVGAImageView * svgaBackView;
@property (nonatomic, strong) LY_PortraitView * portrait;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) MarqueeLabel * contentLabel;
@property (nonatomic,strong) UIImageView * medalImageView;

@end

@implementation LiveWorldTextMessageView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews {
    [self addSubview:self.svgaBackView];
    [self addSubview:self.backgroundView];
    [self addSubview:self.portrait];
    [self addSubview:self.nameLabel];
    [self addSubview:self.contentLabel];
    [self addSubview:self.medalImageView];

    [self.svgaBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    self.svgaBackView.hidden = YES;
    
    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    [self.portrait mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(2.5);
        make.top.mas_equalTo(40);
        make.size.mas_equalTo(60);
    }];
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.portrait.mas_trailing).offset(2);
        make.top.equalTo(self.portrait.mas_top).offset(10);
        make.trailing.mas_lessThanOrEqualTo(-12 - 5 - 5- 70 - 20);
    }];
    
    [self.medalImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.portrait.mas_top).offset(10);
        make.leading.mas_equalTo(self.nameLabel.mas_trailing).offset(5);
        make.centerY.equalTo(self.nameLabel.mas_centerY).offset(0);
        make.size.mas_equalTo(12);
    }];
    
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.nameLabel.mas_leading).offset(0);
        make.top.equalTo(self.nameLabel.mas_bottom).offset(10);
        make.trailing.mas_equalTo(-40);
    }];
}

- (void)setWorldMessage:(LiveWorldMessage *)worldMessage {
    _worldMessage = worldMessage;
    self.portrait.portrait = _worldMessage.portrait;
    self.nameLabel.text = _worldMessage.senderUserName;
    [self.medalImageView fp_setImageWithURLString:_worldMessage.medal];
    self.medalImageView.hidden = !_worldMessage.medal.notEmpty;
    NSString * str = [SPDCommonTool getFamyLanguage];
    NSArray * array = [str componentsSeparatedByString:@"-"];
    self.contentLabel.text = _worldMessage.msgInfo[array[0]];
    self.contentLabel.scrollDuration = 3.5f; // 时间比停留时间要少一些
    if ([_worldMessage.worldMsgType isEqualToString:@"rocket"]) {
        self.svgaBackView.hidden = YES;
        self.backgroundView.image = [UIImage imageNamed:[SPDCommonTool getArImageNameWithImage:@"img_zhibojian_shijiexiaoxi_huojian"]];
    }else{
        self.svgaBackView.hidden = !(_worldMessage.msgLevel == 1 || _worldMessage.msgLevel == 2);
        self.backgroundView.hidden = !(_worldMessage.msgLevel == 0);
        self.backgroundView.image = [UIImage imageNamed:@"img_zhibo_shijiexiaoxi_≥5000"];
        self.svgaBackView.imageName = worldMessage.msgLevel == 1 ?@"5000":@"10000";
    }
}

- (LY_PortraitView *)portrait {
    if (!_portrait) {
        _portrait = [LY_PortraitView new];
    }
    return _portrait;
}

- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.font = [UIFont mediumFontOfSize:11];
        _nameLabel.textColor = [UIColor whiteColor];
    }
    return _nameLabel;
}

- (MarqueeLabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [[MarqueeLabel alloc]init];
        _contentLabel.font = [UIFont boldSystemFontOfSize:11];
        _contentLabel.textColor = [UIColor whiteColor];
    }
    return _contentLabel;
}

- (UIImageView *)backgroundView {
    if (!_backgroundView) {
        _backgroundView = [UIImageView new];
        _backgroundView.image = [UIImage imageNamed:@"img_zhibo_shijiexiaoxi_≥5000"];
    }
    return _backgroundView;
}

- (SVGAImageView *)svgaBackView {
    if (!_svgaBackView) {
        _svgaBackView = [SVGAImageView new];
        _svgaBackView.autoPlay = YES;
    }
    return _svgaBackView;
}

- (UIImageView *)medalImageView {
    if (!_medalImageView) {
        _medalImageView = [UIImageView new];
    }
    return _medalImageView;
}

@end
