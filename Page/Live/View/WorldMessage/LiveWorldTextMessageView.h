//
//  LiveWorldTextMessageView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/8/24.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LiveWorldMessage;

@interface LiveWorldTextMessageView : UIView

@property(nonatomic, strong)LiveWorldMessage * worldMessage;

@end

NS_ASSUME_NONNULL_END
