//
//  LiveWorldGiftMessageView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/8/25.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveWorldGiftMessageView.h"
#import "LY_PortraitView.h"
#import "LiveWorldMessage.h"
@interface LiveWorldGiftMessageView ()

@property (nonatomic, strong) LY_PortraitView * portraitView;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * contentLabel;
@property (nonatomic, strong) UIImageView * giftImageView;
@property (nonatomic, strong) UILabel * giftLabel;// 玫瑰花
@property (nonatomic, strong) UILabel * numberLabel;// 数量label
@property (nonatomic,strong) UIImageView * medalImageView;

@end

@implementation LiveWorldGiftMessageView
 
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews {
//    self.backgroundColor = [UIColor blueColor];
    
    [self addSubview:self.backgroundImageView];
    [self addSubview:self.portraitView];
    [self addSubview:self.nameLabel];
    [self addSubview:self.contentLabel];
    [self addSubview:self.giftLabel];
    [self addSubview:self.numberLabel];
    [self addSubview:self.giftImageView];
    [self addSubview:self.medalImageView];

    [self.backgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(2.5);
        make.top.mas_equalTo(40);
        make.size.mas_equalTo(60);
    }];
 
    [self.giftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(70);
        make.top.mas_equalTo(30);
        make.trailing.mas_equalTo(-9);
    }];
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-11);
        make.trailing.mas_lessThanOrEqualTo(-90);
    }];
    
    [self.giftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(self.numberLabel.mas_leading).offset(-5);
        make.bottom.mas_equalTo(-13);
    }];
    
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.nameLabel.mas_leading).offset(0);
        make.trailing.equalTo(self.giftLabel.mas_leading).offset(-5);
        make.bottom.mas_equalTo(-13);
    }];
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.portraitView.mas_trailing).offset(0);
        make.top.equalTo(self.portraitView.mas_top).offset(10);
        make.trailing.mas_lessThanOrEqualTo(-12 - 5 - 5- 70 - 20);
    }];
    
    [self.medalImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.nameLabel.mas_trailing).offset(5);
        make.size.mas_equalTo(12);
        make.centerY.equalTo(self.nameLabel.mas_centerY).offset(0);
    }];
    
}

- (void)setWorldMessage:(LiveWorldMessage *)worldMessage {
    _worldMessage = worldMessage;
    self.portraitView.portrait  = _worldMessage.portrait;
    self.nameLabel.text = _worldMessage.senderUserName;
    self.contentLabel.text = [NSString stringWithFormat:@"%@ %@".localized,@"送给".localized,_worldMessage.receiverUserName];
    NSString * str = [SPDCommonTool getFamyLanguage];
    NSArray * array = [str componentsSeparatedByString:@"-"];
    self.giftLabel.text = _worldMessage.giftName[array[0]];
    self.numberLabel.text = [NSString stringWithFormat:@"x%@",@(worldMessage.giftNumber)];
    self.backgroundImageView.imageName = worldMessage.msgLevel == 1 ?@"5000":@"10000";
    [self.giftImageView fp_setImageWithURLString:worldMessage.giftCover];
    [self.medalImageView fp_setImageWithURLString:_worldMessage.medal];
    self.medalImageView.hidden = !_worldMessage.medal.notEmpty;
}

- (UIImageView *)giftImageView {
    if (!_giftImageView) {
        _giftImageView = [[UIImageView alloc]init];
    }
    return _giftImageView;
}

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [LY_PortraitView new];
    }
    return _portraitView;
}

- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.font = [UIFont mediumFontOfSize:11];
        _nameLabel.textColor = [UIColor whiteColor];
        _nameLabel.textAlignment = !kIsMirroredLayout ? NSTextAlignmentLeft:NSTextAlignmentRight;
    }
    return _nameLabel;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [UILabel new];
        _contentLabel.font = [UIFont mediumFontOfSize:11];
        _contentLabel.textColor = [UIColor whiteColor];
    }
    return _contentLabel;
}

- (UILabel *)giftLabel {
    if (!_giftLabel) {
        _giftLabel = [UILabel new];
        _giftLabel.font = [UIFont mediumFontOfSize:11];
        _giftLabel.textColor = SPD_HEXCOlOR(@"FFF21E");
    }
    return _giftLabel;
}

- (UILabel *)numberLabel {
    if (!_numberLabel) {
        _numberLabel = [UILabel new];
        _numberLabel.font = [UIFont mediumFontOfSize:16];
        _numberLabel.textColor = SPD_HEXCOlOR(@"FFF21E");
    }
    return _numberLabel;
}

- (SVGAImageView *)backgroundImageView {
    if (!_backgroundImageView) {
        _backgroundImageView = [SVGAImageView new];
        _backgroundImageView.autoPlay = YES;
    }
    return _backgroundImageView;
}

- (UIImageView *)medalImageView {
    if (!_medalImageView) {
        _medalImageView = [UIImageView new];
    }
    return _medalImageView;
}

@end
