//
//  LiveWorldGiftMessageView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/8/25.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SVGAImageView.h"

@class LiveWorldMessage;

NS_ASSUME_NONNULL_BEGIN

@interface LiveWorldGiftMessageView : UIView

@property (nonatomic, strong) LiveWorldMessage * worldMessage;
@property (nonatomic, strong) SVGAImageView * backgroundImageView;

@end

NS_ASSUME_NONNULL_END
