//
//  LiveRoomAudienceBottomView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/23.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveRoomAudienceBottomView.h"
#import "ZegoKitManager.h"
#import "LiveFreeGiftIconView.h"
#import "LiveFreeGiftTimerManager.h"
#import "LiveFreeGiftReceiveSuccess.h"
#import "LiveFreeGiftViewController.h"
#import "LY_HalfNavigationController.h"
#import "SVGA.h"
#import "LiveAudiencePlayBottomView.h"

@interface LiveRoomAudienceBottomView ()<SVGAPlayerDelegate>

@property (nonatomic, strong) SVGAPlayer * sendGiftView;
@property (nonatomic, strong) UIView * redIcon;
@property (nonatomic, strong) UIButton * shareButton;
@property (nonatomic, strong) UIView * inputView;

@property (nonatomic, strong) UIButton *linkMicButton;
@property (nonatomic, strong) NSTimer *linkMicDurationTimer;

@property (nonatomic, strong) LiveFreeGiftIconView * freeGiftView;
@property (nonatomic, strong) LiveFreeGiftReceiveSuccess *receiveFreeGiftSuccessView;
@property (nonatomic, strong) UIButton * playButton;//
@end

@implementation LiveRoomAudienceBottomView

- (instancetype)init {
    if (self = [super init]) {
        [self initSubViews];
        [self initNotification];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)initNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handle:) name:@"PackageNewNumChanged" object:nil];
}

- (void)initSubViews {
    
    CGFloat space = 15.0f;
    
    self.sendGiftView = [[SVGAPlayer alloc]init];
    [self addSubview:self.sendGiftView];
    self.sendGiftView.contentMode = UIViewContentModeScaleAspectFit;
    self.sendGiftView.loops = 0;
    self.sendGiftView.clearsAfterStop = YES;
    self.sendGiftView.delegate = self;
    SVGAParser *parser = [[SVGAParser alloc] init];
    [parser parseWithNamed:@"live_gift" inBundle:[NSBundle mainBundle] completionBlock:^(SVGAVideoEntity * _Nonnull videoItem) {
        self.sendGiftView.videoItem = videoItem;
        [self.sendGiftView startAnimation];
    } failureBlock:^(NSError * _Nonnull error) {
        
    }];
    [self.sendGiftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_offset(-20);
        make.top.mas_offset(10);
        make.bottom.mas_offset(-10);
        make.width.equalTo(self.sendGiftView.mas_height).multipliedBy(1.0f);
    }];
    
    // 免费礼物
    self.freeGiftView = [[LiveFreeGiftIconView alloc]init];
    [self addSubview:self.freeGiftView];
    [self.freeGiftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(self.sendGiftView.mas_leading).offset(-space);
        make.top.mas_offset(10);
        make.bottom.mas_offset(-10);
        make.width.equalTo(self.freeGiftView.mas_height).multipliedBy(1.0f);
    }];
    
    // play
    [self addSubview:self.playButton];
    [self.playButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(self.freeGiftView.mas_leading).offset(-space);
        make.top.mas_offset(10);
        make.bottom.mas_offset(-10);
        make.width.equalTo(self.playButton.mas_height).multipliedBy(1.0f);
    }];
    
    self.shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.shareButton];
    self.shareButton.tag = 2;
    [self.shareButton addTarget:self action:@selector(handleButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.shareButton setImage:[UIImage imageNamed:@"ic_zhibo_yonghufenxiang"] forState:UIControlStateNormal];
    [self.shareButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(self.playButton.mas_leading).offset(-space);
        make.top.mas_offset(10);
        make.bottom.mas_offset(-10);
        make.width.equalTo(self.shareButton.mas_height).multipliedBy(1.0f);
    }];
    
    self.receiveFreeGiftSuccessView = [[[NSBundle mainBundle]loadNibNamed:@"LiveFreeGiftReceiveSuccess" owner:self options:nil]lastObject];
    [self addSubview:self.receiveFreeGiftSuccessView];
    [self.receiveFreeGiftSuccessView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kScreenW);
        make.height.mas_equalTo(kScreenH);
        make.centerX.equalTo(self.mas_centerX);
    }];
    self.receiveFreeGiftSuccessView.hidden = YES;
    
    
    self.sendGiftView.userInteractionEnabled = YES;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(touchSendGroupGift:)];
    self.sendGiftView.tag = 3;
    [self.sendGiftView addGestureRecognizer:tap];
    
    self.inputView = [UIView new];
    self.inputView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.inputView];
    [self.inputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(11.5);
        make.bottom.mas_equalTo(-11.5);
        make.trailing.mas_equalTo(self.shareButton.mas_leading).with.offset(-space);
    }];
    UIImageView * img = [UIImageView new];
    img.image = [UIImage imageNamed:@"radio_room_edit"];
    [self.inputView addSubview:img];
    [img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(14);
        make.height.mas_equalTo(16);
    }];
    
    UILabel * titleLabel = [UILabel new];
    titleLabel.text = SPDStringWithKey(@"说点什么", nil);
    titleLabel.font = [UIFont systemFontOfSize:15];
    titleLabel.textColor = [UIColor colorWithRed:26.0/255 green:26.0/255 blue:26.0/255 alpha:0.8];
    [self.inputView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(img.mas_trailing).offset(5);
        make.centerY.mas_equalTo(0);
    }];
    self.inputView.userInteractionEnabled = YES;
    UITapGestureRecognizer * inputViewTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(touchSendGroupGift:)];
    self.inputView.tag = 0;
    [self.inputView addGestureRecognizer:inputViewTap];
    
    self.redIcon = [UIView new];
    [self.redIcon setBackgroundColor:[UIColor redColor]];
    self.redIcon.layer.cornerRadius = 3;
    self.redIcon.clipsToBounds = YES;
    self.redIcon.hidden = YES;
    [self addSubview:self.redIcon];
    [self.redIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.and.height.mas_equalTo(6);
        make.top.equalTo(self.sendGiftView.mas_top).offset(0);
        make.trailing.equalTo(self.sendGiftView.mas_trailing).offset(0);
    }];
        
}

- (void)handle:(NSNotification *)notify {
    [self updateStatusIcon:[notify.object boolValue]];
}

- (void)updateStatusIcon:(BOOL) show {
    self.redIcon.hidden = !show;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.inputView.layer.cornerRadius = CGRectGetHeight(self.inputView.frame)/2;
    self.inputView.clipsToBounds = YES;
}

- (void)touchSendGroupGift:(UITapGestureRecognizer *)tap {
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveRoomAudienceBottomView:didTapViewWithTag:)]) {
        [self.delegate liveRoomAudienceBottomView:self didTapViewWithTag:tap.view.tag];
    }
}

- (void)handleButtonClicked:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveRoomAudienceBottomView:didTapViewWithTag:)]) {
        [self.delegate liveRoomAudienceBottomView:self didTapViewWithTag:sender.tag];
    }
}

- (void)playButtonClicked:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveRoomAudienceBottomView:didTapViewWithTag:)]) {
        [self.delegate liveRoomAudienceBottomView:self didTapViewWithTag:sender.tag];
    }
}

#pragma mark - getters

- (UIButton *)playButton {
    if (!_playButton) {
        _playButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_playButton setImage:[UIImage imageNamed:@"ic_zhibo_yonghuyule"] forState:UIControlStateNormal];
        [_playButton addTarget:self action:@selector(playButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        _playButton.tag = 5;
    }
    return _playButton;
}

@end

