//
//  LY_LiveFollowerView.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/26.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_LiveFollowerView : UIControl

- (void)loadNetData;

@end

NS_ASSUME_NONNULL_END
