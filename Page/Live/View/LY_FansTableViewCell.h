//
//  LY_FansTableViewCell.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LY_Fans.h" 

NS_ASSUME_NONNULL_BEGIN

@interface LY_FansTableViewCell : UITableViewCell

@property(nonatomic, strong) LY_Fans *fans;

@property(nonatomic, assign) NSInteger rank;

@end

NS_ASSUME_NONNULL_END
