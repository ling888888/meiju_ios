//
//  LiveOnlineUserNumView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/20.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveOnlineUserNumView.h"

@interface LiveOnlineUserNumView ()

@property (nonatomic, strong) UILabel * numLabel;
@property (nonatomic, strong) UIImageView * arrowImageView;
//@property (nonatomic, strong) UIImageView * placeholderImageView;
@property (nonatomic, strong) NSMutableArray * imageViewArray;

@property (nonatomic, strong) UIView * bgView;
@end

@implementation LiveOnlineUserNumView

- (instancetype)init {
    if (self = [super init]) {
        [self initSubviews];
        self.userInteractionEnabled = YES;
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

- (void)initSubviews {
    
    self.bgView = [[UIView alloc]init];
    self.bgView.userInteractionEnabled = NO;
    self.bgView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.2];
    [self addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(0);
        make.height.mas_equalTo(20);
        make.centerY.mas_equalTo(self.mas_centerY);
    }];
    self.bgView.layer.cornerRadius = 10;
    self.bgView.clipsToBounds = YES;
    
    self.arrowImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_zhibo_zhubo_jiantou"]];
    [self.bgView addSubview:self.arrowImageView];
    self.arrowImageView.mirrorImage = YES;
    [self.arrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.trailing.equalTo(self.bgView.mas_trailing).offset(-8);
        make.width.mas_equalTo(5);
        make.height.mas_equalTo(8);
    }];
    
    self.numLabel = [[UILabel alloc]init];
    self.numLabel.font = [UIFont boldSystemFontOfSize:12];
    self.numLabel.textColor = [UIColor whiteColor];
    [self.bgView addSubview:self.numLabel];
    self.numLabel.userInteractionEnabled = NO;
    self.numLabel.text = @"观众".localized;
    [self.numLabel sizeToFit];
    [self.numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       make.centerY.mas_equalTo(0);
       make.leading.equalTo(self.bgView.mas_leading).offset(8);
       make.trailing.equalTo(self.arrowImageView.mas_leading).offset(-5);
    }];
    
    
  
}

- (void)setMessage:(LiveUserUpdateMessage *)message {
    _message = message;
    
    NSMutableArray * array = [NSMutableArray new];
    if (_message.rankFirst && _message.rankFirst.allKeys.count >0) {
        [array addObject:_message.rankFirst];
    }
    if (_message.rankSecond && _message.rankSecond.allKeys.count >0) {
        [array addObject:_message.rankSecond];
    }
    if (_message.rankThird && _message.rankThird.allKeys.count >0) {
        [array addObject:_message.rankThird];
    }
    for (UIImageView * img in self.imageViewArray) {
        [img removeFromSuperview];
    }
    [self.imageViewArray removeAllObjects];
    for (UIView * v in self.subviews) {
        if ([v isKindOfClass:[UIImageView class]] && v != self.arrowImageView) {
            [v removeFromSuperview];
        }
    }

    for (NSInteger i = 0; i< array.count; i++) {
        UIImageView * img = [UIImageView new];
        [self addSubview:img];
        img.userInteractionEnabled = NO;
        [img mas_makeConstraints:^(MASConstraintMaker *make) {
            make.trailing.equalTo(self.arrowImageView.mas_leading).offset(-6 - i * 18);
            make.centerY.mas_equalTo(0);
            make.width.and.height.mas_equalTo(26);
        }];
        [self.imageViewArray addObject:img];
        img.layer.cornerRadius = 13;
        img.clipsToBounds = YES;
        img.layer.borderWidth = 1;
        img.layer.borderColor = [UIColor whiteColor].CGColor;
        [img fp_setImageWithURLString:array[array.count - i - 1][@"avatar"]];
        UIImageView * headImg = [UIImageView new];
        [self addSubview:headImg];
        [headImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(img.mas_centerY).offset(0);
            make.centerX.equalTo(img.mas_centerX).offset(0);
            make.width.and.height.mas_equalTo(26*1.5);
        }];
        headImg.image = [UIImage imageNamed:[NSString stringWithFormat:@"img_zhibojian_bangdan_toushi%ld",array.count-i]];

    }

    
    if (self.imageViewArray.count == 0) {
        self.numLabel.hidden = NO;
    }else{
        self.numLabel.hidden = YES;
        [self.arrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(0);
            make.leading.equalTo(self.bgView.mas_leading).offset(8 + 26);
            make.trailing.equalTo(self.bgView.mas_trailing).offset(-8);
            make.width.mas_equalTo(5);
            make.height.mas_equalTo(8);
        }];
    }
    
}

- (void)handleTap:(UITapGestureRecognizer *)tap {
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveOnlineUserNumViewClickedOnlineUser:)]) {
        [self.delegate liveOnlineUserNumViewClickedOnlineUser:self];
    }
}

- (NSMutableArray *)imageViewArray {
    if (!_imageViewArray) {
        _imageViewArray = [NSMutableArray new];
    }
    return _imageViewArray;
}

@end
