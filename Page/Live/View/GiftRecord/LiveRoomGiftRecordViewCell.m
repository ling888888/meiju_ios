//
//  LiveRoomGiftRecordViewCell.m
//  SimpleDate
//
//  Created by 李楠 on 2020/3/31.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomGiftRecordViewCell.h"
#import "LiveGiftRecordModel.h"

@interface LiveRoomGiftRecordViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@property (weak, nonatomic) IBOutlet UILabel *giftLabel;

@end

@implementation LiveRoomGiftRecordViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(LiveGiftRecordModel *)model {
    _model = model;
    
    [self.avatarImageView fp_setImageWithURLString:_model.userAvatar];
    self.nameLabel.text = _model.userName;
    self.timeLabel.text = _model.timeStr;
    self.valueLabel.text = _model.giftValue.stringValue;
    self.giftLabel.text = [NSString stringWithFormat:@"%@ X%@", _model.giftNameStr, _model.sendNumber];
}

@end
