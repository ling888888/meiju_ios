//
//  LiveRoomGiftRecordButton.m
//  SimpleDate
//
//  Created by 李楠 on 2020/3/31.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomGiftRecordButton.h"

@interface LiveRoomGiftRecordButton ()

@property (weak, nonatomic) IBOutlet UILabel *badgeLabel;

@end

@implementation LiveRoomGiftRecordButton

- (void)setBadgeValue:(NSInteger)badgeValue {
    _badgeValue = badgeValue;
    
    if (badgeValue > 0) {
        self.badgeLabel.hidden = NO;
        self.badgeLabel.text = [NSString stringWithFormat:@"%zd", _badgeValue];
    } else {
        self.badgeLabel.hidden = YES;
    }
}

@end
