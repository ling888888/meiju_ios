//
//  LiveRoomGiftRecordView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/3/31.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomGiftRecordView.h"
#import "LiveRoomGiftRecordViewCell.h"
#import "LiveGiftRecordModel.h"

@interface LiveRoomGiftRecordView ()<UICollectionViewDataSource, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;

@property (nonatomic, assign) CGFloat contentHeight;
@property (nonatomic, assign) CGFloat bottomHeight;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation LiveRoomGiftRecordView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.contentHeight = kScreenH * 565.0f / 667.0f;
    self.bottomHeight = 100;
    self.contentViewBottom.constant = -self.contentHeight - self.bottomHeight;
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"LiveRoomGiftRecordViewCell" bundle:nil] forCellWithReuseIdentifier:@"LiveRoomGiftRecordViewCell"];
//    __weak typeof(self) weakSelf = self;
//    self.collectionView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
//        weakSelf.page = 0;
//        [weakSelf requestLiveGiftRecord];
//    }];
//    self.collectionView.mj_header.hidden = YES;
//    self.collectionView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
//        [weakSelf requestLiveGiftRecord];
//    }];
//    self.collectionView.mj_footer.hidden = YES;
    self.flowLayout.itemSize = CGSizeMake(kScreenW, 60);
    
    [self layoutIfNeeded];
}

#pragma mark - Public methods

- (void)showWithLiveId:(NSString *)liveId {
    [self requestLiveGiftRecordWithLiveId:liveId];
    
    [UIView animateWithDuration:0.25 animations:^{
        self.contentViewBottom.constant = -self.bottomHeight;
        [self layoutIfNeeded];
    }];
}

#pragma mark - Event responses

- (IBAction)tapBackground:(UITapGestureRecognizer *)sender {
    [self hide];
}

#pragma mark - Private methods

- (void)requestLiveGiftRecordWithLiveId:(NSString *)liveId {
    NSDictionary *params = @{@"liveId": liveId};
    [RequestUtils GET:URL_NewServer(@"live/live.gift.record") parameters:params success:^(id  _Nullable suceess) {
        if (self.page == 0) {
            [self.dataArray removeAllObjects];
            self.valueLabel.text = [NSString stringWithFormat:@"%@%@", SPDLocalizedString(@"本次直播礼物总值"), suceess[@"giftValue"] ? : @"0"];
        }
        
        NSArray *list = suceess[@"list"];
        for (NSDictionary *dic in list) {
            LiveGiftRecordModel *model = [LiveGiftRecordModel initWithDictionary:dic];
            [self.dataArray addObject:model];
        }
        [self.collectionView reloadData];
        self.page++;
        
        self.collectionView.mj_footer.hidden = !list.count;
        self.collectionView.mj_header.hidden = NO;
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
    } failure:^(id  _Nullable failure) {
        self.collectionView.mj_header.hidden = NO;
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
    }];
}

- (void)hide {
    [UIView animateWithDuration:0.25 animations:^{
        self.contentViewBottom.constant = -self.contentHeight - self.bottomHeight;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LiveRoomGiftRecordViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveRoomGiftRecordViewCell" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ![touch.view isDescendantOfView:self.contentView];
}

#pragma mark - Setters & Getters

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
@end
