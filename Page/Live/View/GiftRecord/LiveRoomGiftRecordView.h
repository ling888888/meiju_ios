//
//  LiveRoomGiftRecordView.h
//  SimpleDate
//
//  Created by 李楠 on 2020/3/31.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveRoomGiftRecordView : UIView

- (void)showWithLiveId:(NSString *)liveId;

@end

NS_ASSUME_NONNULL_END
