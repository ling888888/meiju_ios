//
//  LiveRoomGiftRecordViewCell.h
//  SimpleDate
//
//  Created by 李楠 on 2020/3/31.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LiveGiftRecordModel;

@interface LiveRoomGiftRecordViewCell : UICollectionViewCell

@property (nonatomic, strong) LiveGiftRecordModel *model;

@end

NS_ASSUME_NONNULL_END
