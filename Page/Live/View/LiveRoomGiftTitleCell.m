//
//  LiveRoomGiftTitleCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveRoomGiftTitleCell.h"

@interface LiveRoomGiftTitleCell ()
@property (weak, nonatomic) IBOutlet UIView *arrowView;
@end

@implementation LiveRoomGiftTitleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setIsSelected:(BOOL)isSelected {
    _isSelected = isSelected;
    self.titleLabel.textColor = _isSelected ? SPD_HEXCOlOR(@"F5F5F5"):[SPD_HEXCOlOR(@"#F5F5F5") colorWithAlphaComponent:0.6];
    self.arrowView.hidden = !_isSelected;
}

@end
