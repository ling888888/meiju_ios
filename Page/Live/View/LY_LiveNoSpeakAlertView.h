//
//  LY_LiveNoSpeakAlertView.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/23.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LYPSystemAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@class LiveUserModel;

@protocol LY_LiveNoSpeakAlertViewDelegate <NSObject>

- (void)liveNoSpeakMinute:(int)minute toUser:(LiveUserModel *)userInfo;

@end

@interface LY_LiveNoSpeakAlertView : LYPSystemAlertView

@property (nonatomic, strong) LiveUserModel *userInfo;
@property (nonatomic, weak) id<LY_LiveNoSpeakAlertViewDelegate> delegate;

@end

@interface LY_LiveNoSpeakCell : UICollectionViewCell

@property(nonatomic, copy) NSString *title;



@property(nonatomic, assign) BOOL isSelected;

@end

NS_ASSUME_NONNULL_END
