//
//  LY_LiveNoSpeakAlertView.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/23.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_LiveNoSpeakAlertView.h"

@interface LY_LiveNoSpeakAlertView () <UICollectionViewDataSource, UICollectionViewDelegate>

@property(nonatomic, strong) UICollectionView *radioBtnCollectionView;

@property(nonatomic, strong) NSArray *radioBtnTitleArray;

@property(nonatomic, assign) int currentSelectedMinute;

@end

@implementation LY_LiveNoSpeakAlertView

- (UICollectionView *)radioBtnCollectionView {
    if (!_radioBtnCollectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        
        flowLayout.itemSize = CGSizeMake(137, 30);
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.minimumLineSpacing = 5;
        _radioBtnCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _radioBtnCollectionView.dataSource = self;
        _radioBtnCollectionView.delegate = self;
        _radioBtnCollectionView.backgroundColor = [UIColor whiteColor];
        [_radioBtnCollectionView registerClass:[LY_LiveNoSpeakCell class] forCellWithReuseIdentifier:@"LiveNoSpeakIdentifier"];
    }
    return _radioBtnCollectionView;
}

- (NSArray *)radioBtnTitleArray {
    if (!_radioBtnTitleArray) {
        _radioBtnTitleArray = @[[NSString stringWithFormat:@"%@分钟".localized, @"5"],
                                [NSString stringWithFormat:@"%@分钟".localized, @"10"],
                                [NSString stringWithFormat:@"%@分钟".localized, @"30"],
                                [NSString stringWithFormat:@"%@小时".localized, @"1"],
                                [NSString stringWithFormat:@"%@小时".localized, @"3"],
                                [NSString stringWithFormat:@"%@小时".localized, @"24"]
        ];
    }
    return _radioBtnTitleArray;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.radioBtnCollectionView];
        [self.radioBtnCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self.contentView);
            make.height.mas_equalTo(105);
        }];
    }
    return self;
}

- (void)actionViewDidClickCancelAction {
    [super actionViewDidClickCancelAction];
}

- (void)actionViewDidClickDoneAction {
    if (self.currentSelectedMinute != 0) {
        [super actionViewDidClickDoneAction];
        if ([self.delegate respondsToSelector:@selector(liveNoSpeakMinute:toUser:)]) {
            [self.delegate liveNoSpeakMinute:self.currentSelectedMinute toUser:self.userInfo];
        }
    }
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.radioBtnTitleArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LY_LiveNoSpeakCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveNoSpeakIdentifier" forIndexPath:indexPath];
    cell.title = self.radioBtnTitleArray[indexPath.row];
    
    return cell; 
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    for (LY_LiveNoSpeakCell *cell in [collectionView visibleCells]) {
        cell.isSelected = false;
    }
    LY_LiveNoSpeakCell * cell = (LY_LiveNoSpeakCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.isSelected = true;
    
    switch (indexPath.row) {
        case 0: {
            self.currentSelectedMinute = 5;
            break;
        }
        case 1: {
            self.currentSelectedMinute = 10;
            break;
        }
        case 2: {
            self.currentSelectedMinute = 30;
            break;
        }
        case 3: {
            self.currentSelectedMinute = 60;
            break;
        }
        case 4: {
            self.currentSelectedMinute = 180;
            break;
        }
        case 5: {
            self.currentSelectedMinute = 1440;
            break;
        }
        default:
            break;
    }
}


@end

@interface LY_LiveNoSpeakCell ()

@property(nonatomic, strong) UIImageView *iconView;

@property(nonatomic, strong) UILabel *titleLabel;

@end

@implementation LY_LiveNoSpeakCell

- (UIImageView *)iconView {
    if (!_iconView) {
        _iconView = [[UIImageView alloc] init];
        _iconView.image = [UIImage imageNamed:@"ic_fenxiang_duoxuan"];
    }
    return _iconView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont mediumFontOfSize:18];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
    }
    return _titleLabel;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.iconView];
        [self.contentView addSubview:self.titleLabel];
        
        [self.iconView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(19);
            make.centerY.mas_equalTo(self.contentView);
            make.leading.mas_equalTo(25);
        }];
        
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo(self.iconView.mas_trailing).offset(10);
            make.centerY.mas_equalTo(self.contentView);
        }];
    }
    return self;
}

- (void)setTitle:(NSString *)title {
    _title = title;
    
    self.titleLabel.text = title;
}

- (void)setIsSelected:(BOOL)isSelected {
    _isSelected = isSelected;
    if (isSelected) {
        self.iconView.image = [UIImage imageNamed:@"ic_fenxiang_duoxuan_s"];
    } else {
        self.iconView.image = [UIImage imageNamed:@"ic_fenxiang_duoxuan"];
    }
}
@end
