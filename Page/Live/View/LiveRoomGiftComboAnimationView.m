//
//  LiveRoomGiftComboAnimationView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/1.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomGiftComboAnimationView.h"
#import "LiveGiftMessage.h"
#import "LiveRoomGiftComboAnimationViewCell.h"

@interface LiveRoomGiftComboAnimationView ()

@property (nonatomic, assign) NSInteger trackCount;
@property (nonatomic, strong) NSMutableArray *waitingMessageArr;
@property (nonatomic, strong) NSMutableDictionary *animatingMessageDic;
@property (nonatomic, strong) NSMutableDictionary *animationCellDic;

@end

@implementation LiveRoomGiftComboAnimationView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initProperties];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self initProperties];
}

#pragma mark - Public methods

- (void)showAnimationWithMessage:(LiveGiftMessage *)message {
    NSNumber *idleTrack = nil;
//    for (NSNumber *track = @(0);  track.integerValue < self.trackCount; track = @(track.integerValue + 1)) {
//         LiveGiftMessage *animatingMessage = self.animatingMessageDic[track];
//        if (!animatingMessage) {
//            [self startAnimationWithMessage:message onTrack:track];
//            return;
//        }
//    }
//
//    [self.waitingMessageArr addObject:message];

//
    for (NSNumber *track = @(self.trackCount - 1); track.integerValue >= 0; track = @(track.integerValue - 1)) {
        LiveGiftMessage *animatingMessage = self.animatingMessageDic[track];
        if (animatingMessage) {
            if (message.isCombo && message.msgId.notEmpty && [message.msgId isEqualToString:animatingMessage.msgId]) {
                [self updateAnimationWithMessage:message onTrack:track];
                return;
            }
        } else {
            idleTrack = track;
        }
    }

    if (idleTrack) {
        [self startAnimationWithMessage:message onTrack:idleTrack];
    } else {
        for (LiveGiftMessage *waitingMessage in self.waitingMessageArr) {
            if (message.isCombo && message.msgId.notEmpty && [message.msgId isEqualToString:waitingMessage.msgId]) {
                waitingMessage.num = message.num;;
                return;
            }
        }
        [self.waitingMessageArr addObject:message];
    }
}

#pragma mark - Private methods

- (void)initProperties {
    self.userInteractionEnabled = NO;
    self.trackCount = 3;
}

- (void)startAnimationWithMessage:(LiveGiftMessage *)message onTrack:(NSNumber *)track {
    [self.animatingMessageDic setObject:message forKey:track];
    
    LiveRoomGiftComboAnimationViewCell *cell = self.animationCellDic[track];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"LiveRoomGiftComboAnimationViewCell" owner:self options:nil].firstObject;
        [self addSubview:cell];
        [cell mas_makeConstraints:^(MASConstraintMaker *make) {
            make.trailing.mas_equalTo(kScreenW/2);
            make.top.mas_equalTo((54 + 10) * track.integerValue);
            make.width.mas_equalTo(kScreenW);
            make.height.mas_equalTo(50);
        }];
        cell.alpha = 0;
        [self layoutIfNeeded];
        [self.animationCellDic setObject:cell forKey:track];
    }
    cell.message = message;
    [UIView animateWithDuration:0.5 animations:^{
        [cell mas_updateConstraints:^(MASConstraintMaker *make) {
            make.trailing.mas_equalTo(0);
        }];
        cell.alpha = 1;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self performSelector:@selector(stopAnimationOnTrack:) withObject:track afterDelay:4 inModes:@[NSRunLoopCommonModes]];
    }];
}

- (void)updateAnimationWithMessage:(LiveGiftMessage *)message onTrack:(NSNumber *)track {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(stopAnimationOnTrack:) object:track];
    LiveRoomGiftComboAnimationViewCell *cell = self.animationCellDic[track];
    [cell updateCountWithMessage:message];
    [self performSelector:@selector(stopAnimationOnTrack:) withObject:track afterDelay:3 inModes:@[NSRunLoopCommonModes]];
}

- (void)stopAnimationOnTrack:(NSNumber *)track {
    LiveRoomGiftComboAnimationViewCell *cell = self.animationCellDic[track];
    [UIView animateWithDuration:0.5 animations:^{
        cell.alpha = 0;
    } completion:^(BOOL finished) {
        [cell mas_updateConstraints:^(MASConstraintMaker *make) {
            make.trailing.mas_equalTo(kScreenW/2);
        }];
        [self layoutIfNeeded];
        if (self.waitingMessageArr.count) {
            [self startAnimationWithMessage:self.waitingMessageArr[0] onTrack:track];
            [self.waitingMessageArr removeObjectAtIndex:0];
        } else {
            [self.animatingMessageDic removeObjectForKey:track];
        }
    }];
}

#pragma mark - Setters & Getters

- (NSMutableArray *)waitingMessageArr {
    if (!_waitingMessageArr) {
        _waitingMessageArr = [NSMutableArray new];
    }
    return _waitingMessageArr;
}

- (NSMutableDictionary *)animatingMessageDic {
    if (!_animatingMessageDic) {
        _animatingMessageDic = [NSMutableDictionary new];
    }
    return _animatingMessageDic;
}

- (NSMutableDictionary *)animationCellDic {
    if (!_animationCellDic) {
        _animationCellDic = [NSMutableDictionary new];
    }
    return _animationCellDic;
}


@end
