//
//  LY_LiveFollowedMessageCell.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/26.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_BaseMessageCell.h"
#import "LY_LiveFollowedMessage.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_LiveFollowedMessageCell : LY_BaseMessageCell

//@property (nonatomic, strong) LY_LiveFollowedMessage *message;

@end

NS_ASSUME_NONNULL_END
