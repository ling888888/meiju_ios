//
//  LiveRoomGiftAnimationView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/30.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveRoomGiftAnimationView : UIView

@property (nonatomic, strong) RCMessageContent * message;

@end

NS_ASSUME_NONNULL_END
