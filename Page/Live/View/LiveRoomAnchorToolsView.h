//
//  LiveRoomAnchorToolView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/26.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_AlertView.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,LiveRoomAnchorToolsViewItemType) {
    LiveRoomAnchorToolsViewItemType_SendMessage = 0,
    LiveRoomAnchorToolsViewItemType_SendImage,
    LiveRoomAnchorToolsViewItemType_CloseMic,
    LiveRoomAnchorToolsViewItemType_LiveManager, // 直播间管理员
    LiveRoomAnchorToolsViewItemType_AudioConsole,
    LiveRoomAnchorToolsViewItemType_Share,
    LiveRoomAnchorToolsViewItemType_LinkMicSettings,
    LiveRoomAnchorToolsViewItemType_RedPack
};

typedef NS_ENUM(NSInteger,LiveRoomAnchorToolsViewType) {
    LiveRoomAnchorToolsViewType_Send = 0,
    LiveRoomAnchorToolsViewType_Tool
};

@class LiveRoomAnchorToolsView;

@protocol LiveRoomAnchorToolsViewDelegate <NSObject>

- (void)liveRoomAnchorToolsView:(LiveRoomAnchorToolsView *)view didClickedItemWithType:(LiveRoomAnchorToolsViewItemType)type;

@end

@interface LiveRoomAnchorToolsView : LY_AlertView


@property (nonatomic, assign)LiveRoomAnchorToolsViewType type;

@property (nonatomic, weak)id<LiveRoomAnchorToolsViewDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
