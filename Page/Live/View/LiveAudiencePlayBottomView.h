//
//  LiveAudiencePlayBottomView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/9/22.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LYPCustomActionSheetView.h"


NS_ASSUME_NONNULL_BEGIN

@class LiveAudiencePlayBottomView;
@protocol LiveAudiencePlayBottomViewDelegate <NSObject>

@optional
- (void)liveAudiencePlayBottomView:(LiveAudiencePlayBottomView *)view didClickedLuckyNumber:(NSInteger)number;

@end

@interface LiveAudiencePlayBottomView : LYPCustomActionSheetView

@property (nonatomic, assign) id<LiveAudiencePlayBottomViewDelegate> delegate;
@property (nonatomic, copy) NSString * wealthLevel; //

@end

NS_ASSUME_NONNULL_END
