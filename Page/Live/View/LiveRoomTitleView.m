//
//  LiveRoomTitleView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/20.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomTitleView.h"
#import "LiveModel.h"
#import "UIView+LY.h"

@interface LiveRoomTitleView ()

//@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property(nonatomic, strong) LY_PortraitView *portraitView;

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *favorNumLabel;
@property (weak, nonatomic) IBOutlet UIButton *followButton;
@property (nonatomic, strong)CAGradientLayer * gradient;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *onlineNumTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userNameWidth;
@property (weak, nonatomic) IBOutlet UIView *connectingView;
@property (nonatomic, strong) UIImageView * voiceAnimationImageView;
@end

@implementation LiveRoomTitleView

- (UIImageView *)voiceAnimationImageView {
    if (!_voiceAnimationImageView) {
        _voiceAnimationImageView = [UIImageView new];
        NSMutableArray *array = [NSMutableArray array];
        for (NSInteger i = 0; i < 32; i++) {
            [array addObject:[UIImage imageNamed:[NSString stringWithFormat:@"img_zhibojian_yuyinkuosan_000%ld", i]]];
        }
        _voiceAnimationImageView.animationImages = array;
        _voiceAnimationImageView.animationDuration = 1;
        _voiceAnimationImageView.animationRepeatCount = 0;
    }
    return _voiceAnimationImageView;
}

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(portraitTapGestureAction)];
        [_portraitView addGestureRecognizer:tapGesture];
    }
    return _portraitView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.layer insertSublayer:self.gradient atIndex:0];
    self.favorNumLabel.textAlignment = kIsMirroredLayout ? NSTextAlignmentRight:NSTextAlignmentLeft;
    
    [self addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(51);
        make.leading.mas_equalTo(-5);
        make.centerY.mas_equalTo(0);
    }];
    
    [self insertSubview:self.voiceAnimationImageView belowSubview:self.portraitView];
    [self.voiceAnimationImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.centerX.equalTo(self.portraitView.mas_centerX).offset(0);
        make.size.mas_equalTo(54);
    }];
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

- (IBAction)followButtonClicked:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveRoomTitleView:didClickedFollow:)]) {
        [self.delegate liveRoomTitleView:self didClickedFollow:sender];
    }
}

- (void)portraitTapGestureAction {
    if (self.delegate && self.delegate && [self.delegate respondsToSelector:@selector(liveRoomTitleView:didClickedAvatar:)]) {
        [self.delegate liveRoomTitleView:self didClickedAvatar:self.liveModel];
    }
}

- (void)followStatusChangedTo:(BOOL)isFollowing {
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.25 animations:^{
        self.onlineNumTrailing.constant = (isFollowing && !self.isAnchor )? 14 : 46;
        self.userNameWidth.constant = self.isAnchor ?   CGRectGetWidth(self.favorNumLabel.frame) + 46:CGRectGetWidth(self.favorNumLabel.frame);
    } completion:^(BOOL finished) {
        self.followButton.hidden = isFollowing;
    }];
}

- (void)setLiveModel:(LiveModel *)liveModel {
    _liveModel = liveModel;
//    [self.avatarImageView fp_setImageWithURLString:_liveModel.avatar];
    
    self.portraitView.portrait = liveModel.portrait;
    
    if (self.isAnchor) {
        self.followButton.hidden = YES;
        self.backgroundColor = [UIColor clearColor];
        self.userNameLabel.text = [NSString stringWithFormat:SPDLocalizedString(@"直播中 %@"),@"00:00:00"];
        [self followStatusChangedTo:YES];
    }else{
        self.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.1];
        [self.gradient removeFromSuperlayer];
        self.userNameLabel.text = _liveModel.userName;
        [self followStatusChangedTo:_liveModel.isFollow];
    }
}

- (void)setTimerStr:(NSString *)timerStr {
    _timerStr = timerStr;
    self.userNameLabel.text = [NSString stringWithFormat:SPDLocalizedString(@"直播中 %@"),_timerStr];
}

- (void)bindOnlineNum:(NSString *)onlineNum joinNum:(NSString *)joinNum {
    self.favorNumLabel.text = [NSString stringWithFormat:@"%@:%@ %@:%@",@"在线".localized,onlineNum,@"参与".localized,joinNum];
}

- (void)setAnimating:(BOOL)animating {
//    _animating = animating;
    self.voiceAnimationImageView.hidden = !animating;
    if (animating) {
        [self.voiceAnimationImageView startAnimating];
    } else {
        [self.voiceAnimationImageView stopAnimating];
    }
}

- (CAGradientLayer *)gradient {
    if (!_gradient) {
        _gradient = [CAGradientLayer layer];
        _gradient.frame = CGRectMake(0, 0, 160, 40);
        if (kIsMirroredLayout) {
            _gradient.colors = [NSArray arrayWithObjects:
            (id)[UIColor colorWithRed:106/255.0 green:44/255.0 blue:247/255.0 alpha:0].CGColor,
            (id)[UIColor colorWithRed:106/255.0 green:44/255.0 blue:247/255.0 alpha:0.8].CGColor,nil];
        }else{
            _gradient.colors = [NSArray arrayWithObjects:
            (id)[UIColor colorWithRed:106/255.0 green:44/255.0 blue:247/255.0 alpha:0.8].CGColor,
            (id)[UIColor colorWithRed:106/255.0 green:44/255.0 blue:247/255.0 alpha:0].CGColor,nil];
        }
        _gradient.startPoint = CGPointMake(0, 0);
        _gradient.endPoint = CGPointMake(1, 0);
        _gradient.locations  = @[@(0),@(0.9)];
        _gradient.cornerRadius = 20;
        _gradient.masksToBounds = true;
    }
    return _gradient;
}

- (void)setShowConnecting:(BOOL)showConnecting {
    _showConnecting = showConnecting;
    
    self.connectingView.hidden = !_showConnecting;
    self.favorNumLabel.hidden = _showConnecting;
}

@end
