//
//  LiveRoomGiftCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomGiftCell.h"
#import "LiveGiftModel.h"

@interface LiveRoomGiftCell ()
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UILabel *shellLabel;
@property (weak, nonatomic) IBOutlet UILabel *giftNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *tagLabel;
@property (weak, nonatomic) IBOutlet UIImageView *shellImageView;
@property (weak, nonatomic) IBOutlet UILabel *packageEndTimeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tagWidthCons;

@end

@implementation LiveRoomGiftCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setGiftModel:(LiveGiftModel *)giftModel {
    _giftModel = giftModel;
    self.hidden = !_giftModel.giftId.notEmpty;
    self.shellLabel.text =  _giftModel.giftCost;
    [self.coverImageView fp_setImageWithURLString:_giftModel.giftCover];
    self.giftNameLabel.text = _giftModel.giftName;
    if ([_giftModel.type isEqualToString:@"gift"]) {
        if (_giftModel.labelValue.notEmpty && _giftModel.labelColour.notEmpty) {
            self.tagLabel.hidden = NO;
            self.tagLabel.text = _giftModel.labelValue;
            self.tagWidthCons.constant = _giftModel.labelValueWidth;
            self.tagLabel.backgroundColor = SPD_HEXCOlOR(_giftModel.labelColour);
        }else{
            self.tagLabel.hidden = YES;
        }
    }else{
        self.tagLabel.hidden = _giftModel.giftNumber == 1;
        self.tagLabel.text = [NSString stringWithFormat:@"%@",@(_giftModel.giftNumber)];
        self.tagLabel.backgroundColor = SPD_HEXCOlOR(@"6A2CF7");
        if (_giftModel.endTime == -1) {
            self.packageEndTimeLabel.text = @"无期限".localized;
        }else{
            NSDate * date1 = [NSDate dateWithTimeIntervalSinceNow:0];
            NSDate * date2 = [NSDate dateWithTimeInterval:_giftModel.endTime/1000 sinceDate:date1];
            // 3.创建日历
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSCalendarUnit type = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
            // 4.利用日历对象比较两个时间的差值
            NSDateComponents *cmps = [calendar components:type fromDate:date1 toDate:date2 options:0];
            //        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            //        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
            //        NSDate *date1 = [formatter dateFromString:time1];
            //        NSDate *date2 = [formatter dateFromString:time2];

                    // 5.输出结果
            //        NSLog(@"两个时间相差%ld年%ld月%ld日%ld小时%ld分钟%ld秒", cmps.year, cmps.month, cmps.day, cmps.hour, cmps.minute, cmps.second);
            self.packageEndTimeLabel.text = [NSString stringWithFormat:@"%ldD %ldH %ldMin",cmps.day, cmps.hour, cmps.minute];
        }
    }
    
    self.shellLabel.hidden = [_giftModel.type isEqualToString:@"package"];
    self.shellImageView.hidden = [_giftModel.type isEqualToString:@"package"];
    self.packageEndTimeLabel.hidden = ![_giftModel.type isEqualToString:@"package"];

}


@end
