//
//  LiveShareCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/4/8.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveShareCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *lajiImageView;
@property (weak, nonatomic) IBOutlet UIView *layerView;

@end

NS_ASSUME_NONNULL_END
