//
//  LiveShareTipsView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/10.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveShareTipsView.h"
#import "NSString+XXWAddition.h"

@interface LiveShareTipsView ()

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIImageView * iconImageView;
@end

@implementation LiveShareTipsView

- (id)init {
    if (self = [super init]) {
        [self initSubView];
    }
    return self;
}

- (void)initSubView {
    self.userInteractionEnabled = YES;
    self.backgroundColor = [UIColor colorWithRed:0/255.0 green:212/255.0 blue:160/255.0 alpha:0.4];
    self.titleLabel = [UILabel new];
    self.titleLabel.text =SPDStringWithKey(@"好的声音应该被更多人听见，点击分享", nil);
    self.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    self.titleLabel.textColor = [UIColor whiteColor];
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(10);
        make.centerY.mas_equalTo(0);
    }];
    
    self.iconImageView = [[UIImageView alloc]init];
    self.iconImageView.image = [UIImage imageNamed:@"ic_zhibo_yonghufenxiang_xiao"];
    [self addSubview:self.iconImageView];
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.titleLabel.mas_trailing).mas_offset(5);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(11);
    }];
    
    self.width = [NSString sizeWithString:SPDStringWithKey(@"好的声音应该被更多人听见，点击分享", nil) andFont:[UIFont boldSystemFontOfSize:12] andMaxSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)].width + 10 + 11 + 10 + 5;
}

@end
