//
//  LiveOnlineUserNumView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/20.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveUserUpdateMessage.h"

NS_ASSUME_NONNULL_BEGIN

@class LiveOnlineUserNumView;
@protocol LiveOnlineUserNumViewDelegate <NSObject>

@optional
-(void)liveOnlineUserNumViewClickedOnlineUser:(LiveOnlineUserNumView *)onlineUserNumView;

@end

@interface LiveOnlineUserNumView : UIView

@property (nonatomic, strong) LiveUserUpdateMessage * message;
@property (nonatomic, weak)id<LiveOnlineUserNumViewDelegate>delegate;
@end

NS_ASSUME_NONNULL_END
