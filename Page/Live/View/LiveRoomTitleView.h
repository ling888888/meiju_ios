//
//  LiveRoomTitleView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/20.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LiveModel,LiveRoomTitleView;

@protocol LiveRoomTitleViewDelegate <NSObject>

@optional
-(void)liveRoomTitleView:(LiveRoomTitleView *)titleView didClickedFollow:(UIButton *)sender;
-(void)liveRoomTitleView:(LiveRoomTitleView *)titleView didClickedAvatar:(LiveModel *)model;

@end

@interface LiveRoomTitleView : UIView
@property (nonatomic, assign) BOOL isAnchor;
@property (nonatomic, strong) LiveModel * liveModel;
@property (nonatomic, weak)id<LiveRoomTitleViewDelegate> delegate;
@property (nonatomic, copy) NSString * timerStr; // 00:22:22
@property (nonatomic, assign) BOOL showConnecting;
- (void)followStatusChangedTo:(BOOL)isFollowing;

- (void)bindOnlineNum:(NSString *)onlineNum joinNum:(NSString *)joinNum;
- (void)setAnimating:(BOOL)animating;
@end

NS_ASSUME_NONNULL_END
