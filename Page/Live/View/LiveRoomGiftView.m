//
//  LiveRoomGiftView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomGiftView.h"
#import "LiveRoomGiftCell.h"
#import "LiveGiftModel.h"
#import "UIView+LY.h"
#import "LY_LiveRechargeConchView.h"
#import "LiveRoomGiftTitleCell.h"

@interface LiveRoomGiftView ()<UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegateFlowLayout,UIGestureRecognizerDelegate,UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UILabel *shellLabel;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIView *selectNumView;
@property (weak, nonatomic) IBOutlet UILabel *selectNumLabel;
@property (weak, nonatomic) IBOutlet UIView *selectNumBgView;
@property (weak, nonatomic) IBOutlet UITableView *selectNumTableView;
@property (weak, nonatomic) IBOutlet UICollectionView *packageCollectionView;
@property (weak, nonatomic) IBOutlet UIScrollView *contentScrollView;
@property (nonatomic, assign) NSInteger giftSelectedIndex;
@property (nonatomic, strong) NSMutableArray * giftArray;
@property (nonatomic, strong) NSMutableArray * packageArray;
@property (nonatomic, copy) NSArray * numArray;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewConstant;
@property (weak, nonatomic) IBOutlet UICollectionView *titleCollectionView;
@property (nonatomic, copy)NSString * num;
@property (nonatomic, copy) NSArray * titleArray;
@property (nonatomic, assign) NSInteger selectItemIndex; // 选择第几项 背包或者礼物

@end

@implementation LiveRoomGiftView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.titleArray = @[@"背包".localized,@"礼物".localized];
    self.selectItemIndex = 1;
    self.selectNumTableView.delegate = self;
    self.selectNumTableView.dataSource = self;
    self.giftSelectedIndex = 0;
    self.num = @"1";
    self.numArray = [[@[@"1",@"7",@"17",@"77",@"177",@"777"] reverseObjectEnumerator] allObjects];
    [self.selectNumTableView reloadData];
    self.collectionView .delegate = self;
    self.collectionView.dataSource = self;
    self.packageCollectionView.delegate = self;
     self.packageCollectionView.dataSource = self;
    self.titleCollectionView.delegate = self;
    self.titleCollectionView.dataSource = self;
    self.contentScrollView.delegate = self;
    [self.collectionView registerNib:[UINib nibWithNibName:@"LiveRoomGiftCell" bundle:nil] forCellWithReuseIdentifier:@"LiveRoomGiftCell"];
    [self.packageCollectionView registerNib:[UINib nibWithNibName:@"LiveRoomGiftCell" bundle:nil] forCellWithReuseIdentifier:@"LiveRoomGiftCell"];
    [self.titleCollectionView registerNib:[UINib nibWithNibName:@"LiveRoomGiftTitleCell" bundle:nil] forCellWithReuseIdentifier:@"LiveRoomGiftTitleCell"];

    [self requestLiveGift];
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (kIsMirroredLayout) {
        [self.sendButton addRounded: UIRectCornerTopLeft | UIRectCornerBottomLeft withRadius:self.sendButton.bounds.size.height/2];
    }else{
        [self.sendButton addRounded: UIRectCornerTopRight | UIRectCornerBottomRight withRadius:self.sendButton.bounds.size.height/2];
    }

    UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft) cornerRadii:CGSizeMake(10,10)];
       CAShapeLayer *maskLayer1 = [[CAShapeLayer alloc] init];
       maskLayer1.frame = self.bounds;
       maskLayer1.path = maskPath1.CGPath;
    self.bottomView.layer.mask = maskLayer1;
}


- (void)show {
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.25 animations:^{
        self.bottomViewConstant.constant = 0;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)hide {
    [UIView animateWithDuration:0.25 animations:^{
        self.bottomViewConstant.constant = -331;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

// 重置这个整个contentsScrollView状态 显示哪一个 collectionView
- (void)resetContentScrollViewScrollIndex {
    [self.contentScrollView setContentOffset:CGPointMake(kScreenW * (kIsMirroredLayout ? self.titleArray.count - 1- self.selectItemIndex:self.selectItemIndex), 0) animated:NO];
    if (self.selectItemIndex == 1) {
         self.pageControl.numberOfPages = self.giftArray.count/ 8;
         self.pageControl.hidden = (self.giftArray.count<=8);
        [self.collectionView reloadData];
    }else{
        self.pageControl.numberOfPages = self.packageArray.count/ 8;
        self.pageControl.hidden = (self.packageArray.count<=8);
        [self.packageCollectionView reloadData];
    }
}

#pragma mark - Request

- (void)requestLiveGift {
    [RequestUtils GET:URL_NewServer(@"live/live.gift.list") parameters:@{} success:^(id  _Nullable suceess) {
       for (NSDictionary * dic in suceess[@"gifts"]) {
           LiveGiftModel * model = [LiveGiftModel initWithDictionary:dic];
           model.type = @"gift";
           [self.giftArray addObject:model];
       }
       while (self.giftArray.count % 8 != 0) {
            [self.giftArray addObject:[LiveGiftModel new]];
       }
       [self.collectionView reloadData];
        
        //
        for (NSDictionary * dic in suceess[@"package"]) {
            LiveGiftModel * model = [LiveGiftModel initWithDictionary:dic];
            model.type = @"package";
            [self.packageArray addObject:model];
        }
        
        while (self.packageArray.count % 8 != 0) {
             [self.packageArray addObject:[LiveGiftModel new]];
        }
        [self.packageCollectionView reloadData];
        
       self.shellLabel.text = suceess[@"conch"];
        if (self.giftArray.count > 0) {
            [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:self.giftSelectedIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
            [self resetContentScrollViewScrollIndex];
            [self show];
        }
    } failure:^(id  _Nullable failure) {
       [self showTips:failure[@"msg"]];
    }];
}

- (void)requestSendGift {
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    NSMutableDictionary * dict = [NSMutableDictionary new];
    [dict setObject:self.num?:@"" forKey:@"num"];
    LiveGiftModel * model ;
    if (self.selectItemIndex == 0) {
        model = self.packageArray[self.giftSelectedIndex];
    }else{
        model = self.giftArray[self.giftSelectedIndex];
    }
    [dict setObject:model.giftId?:@"" forKey:@"giftId"];
    [dict setObject:self.receiverUserId?:@"" forKey:@"receiverUserId"];
    [dict setObject:self.liveId?:@"" forKey:@"liveId"];
    [RequestUtils POST:URL_NewServer(@"live/live.gift.send") parameters:dict success:^(id  _Nullable suceess) {
        [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
        if (self.selectItemIndex == 0) {
            LiveGiftModel * model = self.packageArray[self.giftSelectedIndex];
            NSInteger count = model.giftNumber;
            if (count != 1) {
                model.giftNumber = count - [self.num integerValue];
            }else{
                [self.packageArray replaceObjectAtIndex:0 withObject:[LiveGiftModel new]];
//                [self.packageArray removeObjectAtIndex:self.giftSelectedIndex];
            }
            [self.packageCollectionView reloadData];
        }
        if (model.needPlay || model.isCombo) {
            [self hide];
            if (model.isCombo && self.delegate && [self.delegate respondsToSelector:@selector(liveRoomGiftViewShowComboButton:prepareComboWithURL:params:)]) {
                [self.delegate liveRoomGiftViewShowComboButton:self prepareComboWithURL:URL_NewServer(@"live/live.gift.send")  params:dict];
            }
        }else{
            [self requestBalance];
        }
        self.sendButton.userInteractionEnabled = YES;
    } failure:^(id  _Nullable failure) {
        self.sendButton.userInteractionEnabled = YES;
        [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
        if ([failure[@"code"] integerValue] == 2005) {
            [self hide];
            [[LY_LiveRechargeConchView new] present];
        }
        [self showTips:failure[@"msg"]];
    }];
}

- (void)requestBalance {
    [RequestUtils GET:URL_NewServer(@"/live/live.gift.list") parameters:@{} success:^(id  _Nullable suceess) {
       self.shellLabel.text = suceess[@"conch"];
    } failure:^(id  _Nullable failure) {
       [self showTips:failure[@"msg"]];
    }];
}

#pragma mark - response event

- (IBAction)rechargeButtonClicked:(UIButton *)sender {
    [self hide];
    [[LY_LiveRechargeConchView new] present];
}

- (IBAction)selectNumButtonClicked:(UIButton *)sender {
    self.selectNumBgView.hidden = NO;
}

- (IBAction)sendButtonClicked:(UIButton *)sender {
    sender.userInteractionEnabled = NO;
    if (self.selectItemIndex == 0) {
        if (self.packageArray.count > 0) {
            LiveGiftModel * model = self.packageArray[self.giftSelectedIndex];
            if (model.giftNumber == 0) {
                return;
            }
        }
    }
    [self requestSendGift];
}

- (IBAction)tapSelectNumBgView:(id)sender {
    self.selectNumBgView.hidden = YES;
}

- (IBAction)tapGiftView:(UITapGestureRecognizer *)sender {
    [self hide];
}

#pragma mark - CollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == self.collectionView) {
        return self.giftArray.count;
    }else if (collectionView == self.packageCollectionView) {
        return self.packageArray.count;
    }else {
        return self.titleArray.count;
    }
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.titleCollectionView) {
        LiveRoomGiftTitleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveRoomGiftTitleCell" forIndexPath:indexPath];
        cell.titleLabel.text = self.titleArray[indexPath.row];
        cell.iconView.hidden = !([[NSUserDefaults standardUserDefaults] boolForKey:@"HaveNewFreeGifts"] && indexPath.row == 0);
        cell.isSelected = (self.selectItemIndex == indexPath.row);
        return cell;
    }else{
        LiveRoomGiftCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveRoomGiftCell" forIndexPath:indexPath];
        cell.giftModel = collectionView == self.collectionView ? self.giftArray[indexPath.row]:self.packageArray[indexPath.row];
        cell.bgImageView.hidden = !(indexPath.row == self.giftSelectedIndex);
        return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.collectionView) {
        self.giftSelectedIndex = indexPath.row;
        [collectionView reloadData];
    }else if (collectionView == self.titleCollectionView){
        if (indexPath.row == self.selectItemIndex) {
            return;
        }
        self.selectItemIndex = indexPath.row;
        self.giftSelectedIndex = 0;
        [self resetContentScrollViewScrollIndex];
        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"HaveNewFreeGifts"]; // 保存一下
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PackageNewNumChanged" object:@(0)];
        [collectionView reloadData];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = ceil(kScreenW - 3 * 5)/4.0f;
    return collectionView == self.titleCollectionView ? CGSizeMake(80,45): CGSizeMake(width, 102.0f);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return collectionView == self.titleCollectionView ? UIEdgeInsetsZero:  UIEdgeInsetsMake(5, 3, 5, 3);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return  collectionView == self.titleCollectionView ? 0:3;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return  collectionView == self.titleCollectionView ? 0:3;
}

#pragma mark - TableView
 
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.numArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"NumCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.backgroundColor = (indexPath.row % 2 == 0) ?[UIColor colorWithRed:20/255.0 green:8/255.0 blue:1/255 alpha:0.05] :[UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.font = [UIFont boldSystemFontOfSize:15];
    cell.textLabel.text = self.numArray[indexPath.row];
    cell.textLabel.textColor = SPD_HEXCOlOR(@"6A2CF7");
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.num = self.numArray[indexPath.row];
    self.selectNumLabel.text = self.num;
    self.selectNumBgView.hidden = YES;
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ![touch.view isDescendantOfView:self.bottomView] && ![touch.view isDescendantOfView:self.selectNumTableView];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView == self.contentScrollView) {
        [self collectionView:self.titleCollectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:kIsMirroredLayout ? (self.titleArray.count -1- scrollView.contentOffset.x/kScreenW):(scrollView.contentOffset.x/kScreenW) inSection:0]];
    }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    if (scrollView == self.collectionView) {
        NSInteger targetPage = ceil(targetContentOffset->x / scrollView.bounds.size.width);
        if (kIsMirroredLayout) {
            self.pageControl.currentPage = self.pageControl.numberOfPages - 1 - targetPage;
        } else {
            self.pageControl.currentPage = targetPage;
        }
    }
}

#pragma mark - Getters

- (NSMutableArray *)giftArray {
    if (!_giftArray) {
        _giftArray = [NSMutableArray new];
    }
    return _giftArray;
}

- (NSMutableArray *)packageArray{
    if (!_packageArray) {
       _packageArray = [NSMutableArray new];
    }
    return _packageArray;
}


@end
