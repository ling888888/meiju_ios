//
//  LiveWalletTtileView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/9/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KKPaddingLabel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveWalletTtileView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet KKPaddingLabel *actionLabel;

@end

NS_ASSUME_NONNULL_END
