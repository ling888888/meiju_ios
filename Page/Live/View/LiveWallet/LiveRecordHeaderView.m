//
//  LiveRecordHeaderView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveRecordHeaderView.h"
@interface LiveRecordHeaderView ()

@property (weak, nonatomic) IBOutlet UILabel *effectiveTotalLabel;//本月直播有效时常
@property (weak, nonatomic) IBOutlet UILabel *effectiveDayLabel;// 本月有效天数
@property (weak, nonatomic) IBOutlet UILabel *dayEndTime;
@property (weak, nonatomic) IBOutlet UILabel *monthEndTime;//本月刷新时间倒计时ms
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (nonatomic, strong) NSTimer * timer;
@property (nonatomic, assign) NSInteger time;

@end

@implementation LiveRecordHeaderView

- (void)dealloc
{
    [self.timer invalidate];
    self.timer = nil;
}

- (void)handleTimer:(NSTimer *)timer {
    [self timeFormatted:_time];
    self.time --;
    if (self.time == 0) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)setDict:(NSDictionary *)dict {
    _dict = dict;
    // 本月直播有效
    NSMutableAttributedString *effectiveTotalStr = [[NSMutableAttributedString alloc]initWithString:_dict[@"effectiveTotal"]];
    [effectiveTotalStr setAttributes:@{NSForegroundColorAttributeName:SPD_HEXCOlOR(@"#FCEA11"),NSFontAttributeName:[UIFont mediumFontOfSize:30]
    } range:NSMakeRange(0, effectiveTotalStr.length)];
    NSAttributedString *effectiveTotalStr1 = [[NSAttributedString alloc]initWithString:@"h" attributes:@{NSForegroundColorAttributeName:SPD_HEXCOlOR(@"#FCEA11"),NSFontAttributeName:[UIFont mediumFontOfSize:12]}];
    [effectiveTotalStr appendAttributedString:effectiveTotalStr1];
    self.effectiveTotalLabel.attributedText = effectiveTotalStr;
    
    // 本月直播有效天数
    NSMutableAttributedString *effectiveDayStr = [[NSMutableAttributedString alloc]initWithString:_dict[@"effectiveDay"]];
    [effectiveDayStr setAttributes:@{NSForegroundColorAttributeName:SPD_HEXCOlOR(@"#FCEA11"),NSFontAttributeName:[UIFont mediumFontOfSize:30]
    } range:NSMakeRange(0, effectiveDayStr.length)];
    NSAttributedString *effectiveDayStr1 = [[NSAttributedString alloc]initWithString:@"day" attributes:@{NSForegroundColorAttributeName:SPD_HEXCOlOR(@"#FCEA11"),NSFontAttributeName:[UIFont mediumFontOfSize:12]}];
    [effectiveDayStr appendAttributedString:effectiveDayStr1];
    self.effectiveDayLabel.attributedText = effectiveDayStr;
    
    //本月剩余时间
    self.time = [_dict[@"monthEndTime"] longLongValue] / 1000;
    if (!self.timer) {
        [self.timer fire];
    }
    
    NSMutableAttributedString *totalLabelStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@%.1f",@"直播总时长".localized,[_dict[@"durationTotal"] doubleValue]]];
    [totalLabelStr setAttributes:@{NSForegroundColorAttributeName:SPD_HEXCOlOR(@"ffffff"),NSFontAttributeName:[UIFont mediumFontOfSize:30]
    } range:NSMakeRange(0, totalLabelStr.length)];
    NSAttributedString *totalLabelStr1 = [[NSAttributedString alloc]initWithString:@"h" attributes:@{NSForegroundColorAttributeName:SPD_HEXCOlOR(@"ffffff"),NSFontAttributeName:[UIFont mediumFontOfSize:15]}];
    [totalLabelStr appendAttributedString:totalLabelStr1];
    self.totalLabel.attributedText = totalLabelStr;
}

- (NSTimer *)timer {
    if (!_timer) {
        _timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(handleTimer:) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
    }
    return _timer;
}

- (void)timeFormatted:(long long)totalSeconds{
    int day = (int)(totalSeconds / (3600 * 24));
    int duration = (int)(totalSeconds % (3600 * 24));
    int seconds = duration % 60;
    int minutes = (duration / 60) % 60;
    int hours = (int)(duration / 3600);
    self.monthEndTime.text = [NSString stringWithFormat:@"%02dday %02d:%02d:%02d",day,hours, minutes, seconds];
    self.dayEndTime.text = [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
}

@end
