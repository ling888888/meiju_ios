//
//  LiveWalletHeaderView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/9/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveWalletHeaderView : UIView

@property (weak, nonatomic) IBOutlet UILabel *todayCrystal;//今日水晶
@property (weak, nonatomic) IBOutlet UILabel *crystalBalance; // yue
@property (weak, nonatomic) IBOutlet UILabel *monthLabel;// 本月水晶
@property (weak, nonatomic) IBOutlet UILabel *allCrystal;// 累计

@end

NS_ASSUME_NONNULL_END
