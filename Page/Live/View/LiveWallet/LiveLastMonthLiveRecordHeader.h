//
//  LiveLastMonthLiveRecordHeader.h
//  SimpleDate
//
//  Created by ling Hou on 2020/9/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveLastMonthLiveRecordHeader : UIView
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationDescLabel;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UILabel *dayDescLabel;

@end

NS_ASSUME_NONNULL_END
