//
//  LY_FansTaskTableViewCell.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_FansTaskTableViewCell.h"

@interface LY_FansTaskTableViewCell ()

@property(nonatomic, strong) UIView *grayBgView;

@property(nonatomic, strong) UIImageView *iconView;

@property(nonatomic, strong) UILabel *titleLabel;

@property(nonatomic, strong) UILabel *descLabel;

@property(nonatomic, strong) UIButton *handleBtn;

@end

@implementation LY_FansTaskTableViewCell

- (UIView *)grayBgView {
    if (!_grayBgView) {
        _grayBgView = [[UIView alloc] init];
        _grayBgView.backgroundColor = [UIColor whiteColor];
//        _grayBgView.backgroundColor = [UIColor colorWithHexString:@"#EBEBEB"];
        _grayBgView.layer.cornerRadius = 10;
    }
    return _grayBgView;
}

- (UIImageView *)iconView {
    if (!_iconView) {
        _iconView = [[UIImageView alloc] init];
    }
    return _iconView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _titleLabel.font = [UIFont mediumFontOfSize:15];
    }
    return _titleLabel;
}

- (UILabel *)descLabel {
    if (!_descLabel) {
        _descLabel = [[UILabel alloc] init];
        _descLabel.textColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.6];
        _descLabel.font = [UIFont mediumFontOfSize:12];
    }
    return _descLabel;
}

- (UIButton *)handleBtn {
    if (!_handleBtn) {
        _handleBtn = [[UIButton alloc] init];
        [_handleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _handleBtn.titleLabel.font = [UIFont mediumFontOfSize:12];
        _handleBtn.layer.cornerRadius = 12.5;
        _handleBtn.layer.masksToBounds = true;
        UIImage *norImg = [UIImage imageWithUIColor:[UIColor colorWithHexString:@"#FF6B93"]];
        UIImage *selImg = [UIImage imageWithUIColor:[[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.2]];
        [_handleBtn setBackgroundImage:norImg forState:UIControlStateNormal];
        [_handleBtn setBackgroundImage:selImg forState:UIControlStateSelected];
        
        [_handleBtn setTitle:@"已完成".localized forState:UIControlStateSelected];
        [_handleBtn addTarget:self action:@selector(handleBtnAction) forControlEvents:UIControlEventTouchUpInside];
        _handleBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    }
    return _handleBtn;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self ) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.contentView addSubview:self.grayBgView];
    [self.grayBgView addSubview:self.iconView];
    [self.grayBgView addSubview:self.handleBtn];
    [self.grayBgView addSubview:self.titleLabel];
    [self.grayBgView addSubview:self.descLabel];
}

- (void)setupUIFrame {
    [self.grayBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(5, 10, 5, 10));
    }];
    [self.iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(40);
        make.leading.mas_equalTo(10);
        make.centerY.mas_equalTo(0);
    }];
    [self.handleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(25);
        make.trailing.mas_equalTo(-10);
        make.centerY.mas_equalTo(0);
        make.width.mas_greaterThanOrEqualTo(75);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.iconView);
        make.leading.mas_equalTo(self.iconView.mas_trailing).offset(10);
        make.trailing.mas_lessThanOrEqualTo(self.handleBtn.mas_leading).offset(-10);
    }];
    [self.descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.iconView);
        make.leading.mas_equalTo(self.iconView.mas_trailing).offset(10);
        make.trailing.mas_lessThanOrEqualTo(self.handleBtn.mas_leading).offset(-10);
    }];
    
//    self.titleLabel.text = @"每日看直播（4/4）";
//    self.descLabel.text = @"每次看5min+200亲密度";
//    [self.handleBtn setSelected:false];
//    [self.handleBtn setTitle:@"去完成" forState:UIControlStateNormal];
}

- (void)setIsValid:(BOOL)isValid {
    _isValid = isValid;
    
    
}

- (void)setTask:(LY_FansTask *)task {
    _task = task;
    
    [self.iconView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:task.cover]] placeholderImage:[UIImage imageNamed:@"img_portrait_placeholder"]];
    
    NSString *title;
    // 判断是否按次数完成任务
    if ([task.type isEqualToString:@"0"]) {
        // 是按次数
        title = [NSString stringWithFormat:@"%@(%@/%@)", task.title, task.finishNum, task.number];
    } else {
        // 按上限
        title = task.title;
    }
    self.titleLabel.text = title;
    
    self.descLabel.text = task.desc;
    
    // 判断任务是否冻结
    if (!self.isValid) {
        [self.handleBtn setTitle:@"" forState:UIControlStateNormal];
        [self.handleBtn setHidden:true];
        [self.handleBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_greaterThanOrEqualTo(0);
        }];
    } else {
        // 判断任务是否完成
        if ([task.status isEqualToString:@"1"]) {
            // 完成
            [self.handleBtn setSelected:true];
            [self.handleBtn setUserInteractionEnabled:false];
        } else {
            // 未完成
            [self.handleBtn setSelected:false];
            [self.handleBtn setUserInteractionEnabled:true];
            [self.handleBtn setTitle:task.buttonText forState:UIControlStateNormal];
        }
        [self.handleBtn setHidden:false];
        [self.handleBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_greaterThanOrEqualTo(75);
        }];
    }
}

- (void)handleBtnAction {
    if ([self.delegate respondsToSelector:@selector(fansTaskTableViewCell:clickToFinishBtn:)]) {
        [self.delegate fansTaskTableViewCell:self clickToFinishBtn:self.handleBtn];
    }
}

@end
