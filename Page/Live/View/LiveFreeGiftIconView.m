//
//  LiveFreeGiftIconView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/22.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveFreeGiftIconView.h"
#import "LiveFreeGiftTimerManager.h"
#import "LiveFreeGiftViewController.h"
#import "LY_HalfNavigationController.h"
#import "LiveFreeGiftReceiveSuccess.h"

@interface LiveFreeGiftIconView ()<CAAnimationDelegate>

@property (nonatomic, strong) UIImageView * coverImageView;
@property (nonatomic, strong) UIImageView * maskImageView;
@property (nonatomic, strong) UILabel * statusLabel;
@property (nonatomic, copy) NSArray * freeGiftList;

@end

@implementation LiveFreeGiftIconView

- (NSArray *)freeGiftList {
    if (!_freeGiftList) {
        _freeGiftList = [NSArray new];
    }
    return _freeGiftList;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapFreeGift:)];
        self.userInteractionEnabled = YES;
        [self addGestureRecognizer:tap];
        [self initSubViews];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleFreeGiftTimer:) name:@"LiveFreeGiftTimerManager" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(achiveFreeGift:) name:@"AchiveFreeGift" object:nil];
        [self requestSysTime];
    }
    return self;
}

- (void)initSubViews
{
    self.maskImageView = [UIImageView new];
    [self addSubview:self.maskImageView];
    [self.maskImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.centerY.mas_equalTo(0);
        make.size.mas_equalTo(self);
    }];
    self.maskImageView.image = [UIImage imageNamed:@"ic_zhibo_dingshisongliwu"];

    self.coverImageView = [UIImageView new];
    [self addSubview:self.coverImageView];
    [self.coverImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.centerY.mas_equalTo(0);
        make.edges.mas_equalTo(UIEdgeInsetsMake(5, 5, 5, 5));
    }];
    
    self.statusLabel = [UILabel new];
    [self addSubview:self.statusLabel];
    [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(16);
        make.leading.mas_equalTo(0);
        make.trailing.mas_equalTo(0);
        make.bottom.equalTo(self.maskImageView.mas_bottom).offset(5);
    }];
    self.statusLabel.font = [UIFont mediumFontOfSize:10];
    self.statusLabel.backgroundColor = SPD_HEXCOlOR(@"#6A2CF7");
    self.statusLabel.textAlignment = NSTextAlignmentCenter;
    self.statusLabel.layer.cornerRadius = 8;
    self.statusLabel.clipsToBounds = YES;
    self.statusLabel.textColor = [UIColor whiteColor];
    self.type = @"2";
        
}

- (void)setType:(NSString *)type {
    _type = type;
    if ([_type isEqualToString:@"1"] ||[_type isEqualToString:@"0"]) {
        self.statusLabel.hidden = NO;
        self.statusLabel.text = [_type isEqualToString:@"1"] ?SPDStringWithKey(@"可领取", nil):@"0";
        if ([_type isEqualToString:@"1"]) {
            [self addAnimation];
        }
    }else{
        self.statusLabel.hidden = YES;
    }
    if (![_type isEqualToString:@"1"]) {
        [self.layer removeAllAnimations];
        [NSObject cancelPreviousPerformRequestsWithTarget:self];
    }
}

- (void)addAnimation{
    if (![_type isEqualToString:@"1"]) {
        return;
    }
    CAKeyframeAnimation *shakeAnim = [CAKeyframeAnimation animation];
    shakeAnim.keyPath = @"transform.translation.x";
    shakeAnim.duration = 0.25;
    shakeAnim.delegate = self;
    shakeAnim.repeatCount = 4;
    CGFloat delta = 3;
    shakeAnim.values = @[@0,@(-delta),@(delta),@0];
    [self.layer addAnimation:shakeAnim forKey:@"shake"];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    if (flag) {
        [self performSelector:@selector(addAnimation) withObject:nil afterDelay:10];
    }
}

- (void)setCurrentImageUrl:(NSString *)currentImageUrl {
    _currentImageUrl = currentImageUrl;
    [self.coverImageView fp_setImageWithURLString:_currentImageUrl];
}

- (void)updateSecond:(NSInteger)second {
    self.statusLabel.text = [NSString stringWithFormat:@"%lds",second];
}


#pragma mark - request

- (void)requestFreeList {
    [RequestUtils GET:URL_NewServer(@"live/live.free.gift.record") parameters:@{} success:^(id  _Nullable suceess) {
        if (!suceess) {
            return ;
        }
        NSInteger receiveNumber = [suceess[@"receiveNumber"] integerValue];
        self.freeGiftList = suceess[@"list"];
        NSDictionary * dict = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"LiveFreeGift:%@",[SPDApiUser currentUser].userId]];
        NSInteger index = [dict[@"index"] integerValue]; // 应该领取在走的状态是第几个
        NSInteger duration = [dict[@"duration"] integerValue];

        // 当和服务器次数不一致 需要
        if (receiveNumber!=0 && receiveNumber != self.freeGiftList.count) {
            NSDictionary * params = @{
                           @"time":dict[@"time"],
                           @"index":@(receiveNumber),
                           @"duration":@([self.freeGiftList[receiveNumber][@"timeLimit"] integerValue])
                       };
            [[NSUserDefaults standardUserDefaults] setObject:params forKey:[NSString stringWithFormat:@"LiveFreeGift:%@",[SPDApiUser currentUser].userId]];
            index = receiveNumber;
        }
        if (receiveNumber == self.freeGiftList.count) {
            NSDictionary * params = @{
                                      @"time":dict[@"time"],
                                      @"index":@(self.freeGiftList.count),
                                      @"duration":@([self.freeGiftList[self.freeGiftList.count -1][@"timeLimit"] integerValue])
                                  };
           [[NSUserDefaults standardUserDefaults] setObject:params forKey:[NSString stringWithFormat:@"LiveFreeGift:%@",[SPDApiUser currentUser].userId]];
            
            NSDictionary * freeGift = self.freeGiftList.lastObject;
            self.currentImageUrl = freeGift[@"giftCover"];
            self.type = @"2";
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReceivedAllFreeGifts" object:nil];
            
        }else{
            NSDictionary * freeGift = self.freeGiftList[index];
            self.type = @"0";
            self.currentImageUrl = freeGift[@"giftCover"];
            [self updateSecond:duration == -1 ? [freeGift[@"timeLimit"] integerValue]:duration];
            [[LiveFreeGiftTimerManager sharedInstance] startWith:duration == -1 ? [freeGift[@"timeLimit"] integerValue]:duration];
        }
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)requestSysTime {
    [RequestUtils GET:URL_NewServer(@"sys/sys.time") parameters:@{} success:^(id  _Nullable suceess) {
        if (!suceess) {
            return ;
        }
        NSString * sysDate = suceess[@"sysDate"];
        NSString * str = [sysDate componentsSeparatedByString:@" "][0];
        NSDictionary * dict = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"LiveFreeGift:%@",[SPDApiUser currentUser].userId]];
        if (!dict || ![dict[@"time"] isEqualToString:str]) {
            NSDictionary * params = @{
                @"time":str,
                @"index":@(0),
                @"duration":@(-1)
            };
            [[NSUserDefaults standardUserDefaults] setObject:params forKey:[NSString stringWithFormat:@"LiveFreeGift:%@",[SPDApiUser currentUser].userId]];
        }
        [self requestFreeList];
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)requestReceiveFreeGift {
    [RequestUtils POST:URL_NewServer(@"live/live.free.gift.receive") parameters:@{} success:^(id  _Nullable suceess) {
        NSDictionary * dict = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"LiveFreeGift:%@",[SPDApiUser currentUser].userId]];
        NSInteger index = [dict[@"index"] integerValue];
        
        LiveFreeGiftReceiveSuccess * receivedView = [[[NSBundle mainBundle]loadNibNamed:@"LiveFreeGiftReceiveSuccess" owner:self options:nil]lastObject];
        [[UIApplication sharedApplication].keyWindow addSubview:receivedView];
        [receivedView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(0);
            make.centerY.mas_equalTo(0);
            make.width.mas_equalTo(kScreenW);
            make.height.mas_equalTo(kScreenH);
        }];
        
        index++;
        NSDictionary * params = @{
            @"time":dict[@"time"],
            @"index":@(index),
            @"duration":index >= self.freeGiftList.count ? @([self.freeGiftList[self.freeGiftList.count -1][@"timeLimit"] integerValue]):@([self.freeGiftList[index][@"timeLimit"] integerValue])
        };
        [[NSUserDefaults standardUserDefaults] setObject:params forKey:[NSString stringWithFormat:@"LiveFreeGift:%@",[SPDApiUser currentUser].userId]];
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"HaveNewFreeGifts"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PackageNewNumChanged" object:@(1)];
        
        [self requestSysTime];
        
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)handleFreeGiftTimer:(NSNotification *)notify {
    NSInteger duration = [notify.object integerValue];
    if (duration != 0) {
     [self updateSecond:duration];
    }else{
        self.type = @"1";
    }
    NSDictionary * dict = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"LiveFreeGift:%@",[SPDApiUser currentUser].userId]];
    NSDictionary * params = @{
                      @"time":dict[@"time"],
                      @"index":dict[@"index"],
                      @"duration":@(duration)
    };
    [[NSUserDefaults standardUserDefaults] setObject:params forKey:[NSString stringWithFormat:@"LiveFreeGift:%@",[SPDApiUser currentUser].userId]];
}

- (void)handleTapFreeGift:(UITapGestureRecognizer *)tap {
    if ([self.type isEqualToString:@"1"]) {
        [self requestReceiveFreeGift];
    }else{
       // 弹出框
        LiveFreeGiftViewController * vc = [LiveFreeGiftViewController new];
        vc.dataArray = [self.freeGiftList mutableCopy];
        LY_HalfNavigationController *nav = [[LY_HalfNavigationController alloc] initWithRootViewController:vc controllerHeight:360];
        [self.VC presentViewController: nav animated:true completion:nil];
    }
}

- (void)achiveFreeGift:(NSNotification *)notify {
    [self requestReceiveFreeGift];
}


@end
