//
//  LiveShareView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/8.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveShareView.h"
#import "LiveShareCell.h"
#import "UIView+LY.h"
#import "SPDUMShareUtils.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "SPDContactsViewController.h"
#import "LiveModel.h"
#import "LDTextView.h"
#import "NSString+XXWAddition.h"

@interface LiveShareView ()<UICollectionViewDelegate,UICollectionViewDataSource,UITextViewDelegate,UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *friendCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *shareCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (nonatomic, strong) NSMutableArray * usersArray;
@property (nonatomic, strong) NSMutableArray * shareArray;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomCons;
@property (weak, nonatomic) IBOutlet UILabel *emptyLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCons;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (nonatomic, copy) NSString * shareUrl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *processWidthCons;
@property (weak, nonatomic) IBOutlet UIView *underView;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;
@property (weak, nonatomic) IBOutlet LDTextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIView *shareView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *shareViewCons;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *shareViewBottomCons;
//@property (nonatomic, strong) NSMutableArray * selectUsersArray;
@property (weak, nonatomic) IBOutlet UIProgressView *process;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewHeightCons;
@property (nonatomic, copy) NSString * shareTitle;
@property (nonatomic, copy) NSString * shareContent;
@property (nonatomic, copy) NSString * shareImage;
@property (weak, nonatomic) IBOutlet UIButton *selectedAllBtn;
@property (nonatomic, strong) NSMutableArray * selectIndexPath;
@end

@implementation LiveShareView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.selectIndexPath = [NSMutableArray new];
    self.textView.delegate = self;
    self.textView.placeholderFont = [UIFont mediumFontOfSize:12];
    self.textView.placeholderColor = [SPD_HEXCOlOR(@"#1A1A1A") colorWithAlphaComponent:0.6];
    self.textView.placeholder = @"对朋友说点什么".localized;
    self.heightCons.constant = 345 + IphoneX_Bottom;
    self.shareViewCons.constant = 200 + IphoneX_Bottom;
    self.shareViewBottomCons.constant = - (200 + IphoneX_Bottom);
    self.shareView.alpha = 0;
    [self.friendCollectionView registerNib:[UINib nibWithNibName:@"LiveShareCell" bundle:nil] forCellWithReuseIdentifier:@"LiveShareCell"];
    self.friendCollectionView.allowsMultipleSelection = YES;
    [self.shareCollectionView registerNib:[UINib nibWithNibName:@"LiveShareCell" bundle:nil] forCellWithReuseIdentifier:@"LiveShareCell"];
    self.friendCollectionView.delegate = self;
    self.shareCollectionView.delegate = self;
    self.friendCollectionView.dataSource = self;
    self.shareCollectionView.dataSource = self;
    [self requestList];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handletap)];
    self.shareView.userInteractionEnabled = YES;
    [self.shareView addGestureRecognizer:tap];
    
    UITapGestureRecognizer * desTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hide)];
    desTap.delegate = self;
    [self addGestureRecognizer:desTap];
    
    [self.selectedAllBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [self.selectedAllBtn setBackgroundImage:[UIImage imageNamed:@"img_zhibo_quanxuan_s"] forState:UIControlStateSelected];
    [self.selectedAllBtn setTitle:@"全选".localized forState:UIControlStateNormal];
    [self.selectedAllBtn setTitle:@"取消全选".localized forState:UIControlStateSelected];
    [self.selectedAllBtn setTitleColor:[UIColor colorWithHexString:@"#1A1A1A"]  forState:UIControlStateNormal];
    [self.selectedAllBtn setBackgroundImage:[UIImage imageNamed:@"img_zhibo_quanxuan"] forState:UIControlStateNormal];

}

- (void)handletap {
    [self requestSend];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.bottomView
       addRounded:UIRectCornerTopLeft|UIRectCornerTopRight withRadius:20];
}

- (void)setLiveInfo:(LiveModel *)liveInfo {
    _liveInfo = liveInfo;
    [self.avatarImageView fp_setImageWithURLString:_liveInfo.liveCover];
    self.titleLabel.text = _liveInfo.liveTitle;
    self.userNameLabel.text = _liveInfo.userName;
}

- (IBAction)selectAllBtnClicked:(UIButton *)sender {
   sender.selected = !sender.selected;
    if (sender.selected) {
      [self.selectIndexPath removeAllObjects];
      for (NSUInteger index = 0; index < self.usersArray.count; ++index) {
          NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
          [self.selectIndexPath addObject:indexPath];
      }
    }else{
        [self.selectIndexPath removeAllObjects];
    }
    [self.friendCollectionView reloadData];
    [self handleArray];
}

- (IBAction)cancelButtonClicked:(id)sender {
    [self hide];
}

- (void)show {
    [[IQKeyboardManager sharedManager] setEnable:YES];

    [self layoutIfNeeded];
    [UIView animateWithDuration:0.25 animations:^{
        self.bottomCons.constant = 0;
        [self layoutIfNeeded];
  
    } completion:^(BOOL finished) {
       
    }];
    [self animateCollection];
}

- (void)hide {
    [[IQKeyboardManager sharedManager] setEnable:NO];
    self.shareView.alpha = 0;
    [UIView animateWithDuration:0.25 animations:^{
        self.bottomCons.constant = -(345+IphoneX_Bottom);
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - request

- (void)requestList {
    [RequestUtils GET:URL_NewServer(@"live/live.share.list") parameters:@{} success:^(id  _Nullable suceess) {
        
        self.usersArray = [suceess[@"list"] mutableCopy];
        self.emptyLabel.hidden = !(self.usersArray.count == 0) ;
        self.shareUrl = suceess[@"shareUrl"];
        self.shareTitle = suceess[@"shareTitle"];
        self.shareImage = suceess[@"thumbNailImage"];
        self.selectedAllBtn.hidden = self.usersArray.count == 0 ;
        [self.friendCollectionView reloadData];
        
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)requestSend {
    NSDictionary *params = @{@"liveId": self.liveInfo.liveId,
                             @"liveUserId": self.liveInfo.userId,
                             @"code": @"liveShare"
    };
    [LY_Network postRequestWithURL:LY_Api.live_fansGroup_finishTask parameters:params success:^(id  _Nullable response) {
    } failture:^(NSError * _Nullable error) {
    }];
    NSMutableArray * userids = [NSMutableArray new];
    for (int i =0; i< self.usersArray.count; i++) {
        NSIndexPath * index = [NSIndexPath indexPathForRow:i inSection:0];
        if ([self.selectIndexPath containsObject:index]) {
            [userids addObject:self.usersArray[i][@"userId"]];
        }
    }
    NSString * useridStr = [userids componentsJoinedByString:@","];
    [RequestUtils POST:URL_NewServer(@"/live/live.share.send") parameters:@{@"liveId":self.liveInfo.liveId,@"userIds":useridStr,@"txtMsg":self.textView.text?:@""} success:^(id  _Nullable suceess) {
        self.resultLabel.text = @"分享成功!".localized;
        [UIView animateWithDuration:0.25 animations:^{
            [self.process setProgress:1 animated:YES];
        } completion:^(BOOL finished) {
            [self performSelector:@selector(hide) withObject:nil afterDelay:0.5];
        }];
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
    if (self.liveInfo.canFinishTask) {
        NSDictionary *params = @{@"liveId": self.liveInfo.liveId,
                                 @"liveUserId": self.liveInfo.userId,
                                 @"code": @"liveShare"
        };
        [LY_Network postRequestWithURL:LY_Api.live_fansGroup_finishTask parameters:params success:^(id  _Nullable response) {
        } failture:^(NSError * _Nullable error) {
        }];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return collectionView == self.friendCollectionView ? self.usersArray.count : self.shareArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LiveShareCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveShareCell" forIndexPath:indexPath];
    NSDictionary * dict = (collectionView == self.friendCollectionView) ? self.usersArray[indexPath.row] :self.shareArray[indexPath.row];
    if (collectionView == self.friendCollectionView) {
        [cell.iconImageView fp_setImageWithURLString:dict[@"avatar"]];
        cell.lajiImageView.hidden = ![self.selectIndexPath containsObject:indexPath];
        cell.iconImageView.layer.cornerRadius = 25;
        cell.iconImageView.clipsToBounds = YES;
        cell.layerView.hidden = NO;
    }else{
        cell.iconImageView.image = [UIImage imageNamed:dict[@"avatar"]];
        cell.layerView.hidden = YES;
    }
    cell.titleLabel.text = dict[@"name"];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.shareCollectionView) {
        NSDictionary *params = @{@"liveId": self.liveInfo.liveId,
                                 @"liveUserId": self.liveInfo.userId,
                                 @"code": @"liveShare"
        };
        [LY_Network postRequestWithURL:LY_Api.live_fansGroup_finishTask parameters:params success:^(id  _Nullable response) {
        } failture:^(NSError * _Nullable error) {
        }];
        NSMutableString *str = [NSMutableString stringWithFormat:@"%@",self.shareUrl ?:@""];
        if (!str) {return;}
        NSString * url ;
        if (indexPath.row == 0) {
            NSString *msg = [NSString stringWithFormat:@"%@&from=whatsapp",self.shareUrl?:@""];
            url = [NSString stringWithFormat:@"whatsapp://send?text=%@", [msg stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
           NSURL *whatsappURL = [NSURL URLWithString: url];
           if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
              [[UIApplication sharedApplication] openURL: whatsappURL];
           } else {
              [SPDCommonTool showWindowToast:SPDStringWithKey(@"未安裝此应用，请安装后再进行分享", nil)];
           }
           //[MobClick event:@"clickShare_WhatsApp"];
        }else if (indexPath.row == 2){
            NSString *msg = [NSString stringWithFormat:@"%@&from=telegram",self.shareUrl ?:@""];
            url = [NSString stringWithFormat:@"https://t.me/share/url?url=%@", msg];
            NSURL *whatsappURL = [NSURL URLWithString: url];
            if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
                 [[UIApplication sharedApplication] openURL: whatsappURL];
            } else {
                 [SPDCommonTool showWindowToast:SPDStringWithKey(@"未安裝此应用，请安装后再进行分享", nil)];
            }
            //[MobClick event:@"clickShare_Telegram"];
        }else if (indexPath.row == 3){
            NSString *msg = [NSString stringWithFormat:@"%@",self.shareUrl ?:@""];
            url = [NSString stringWithFormat:@"https://www.facebook.com/sharer/sharer.php?u=%@",msg];
            NSURL *whatsappURL = [NSURL URLWithString: url];
            if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
                 [[UIApplication sharedApplication] openURL: whatsappURL];
            } else {
                 [SPDCommonTool showWindowToast:SPDStringWithKey(@"未安裝此应用，请安装后再进行分享", nil)];
            }
            //[MobClick event:@"clickShare_FB"];
        }else if (indexPath.row ==1) {
            [self shareToMessager];
        }else if (indexPath.row == 4){
            [self shareto:SPDShareToTwitter];
        }
        else if (indexPath.row == 5){
            [self shareto:SPDShareToWechatSession];
        }else if (indexPath.row == 6){
            [self shareto:SPDShareToWechatTimeline];
        }else if (indexPath.row == 7){
           SPDContactsViewController *vc = [[SPDContactsViewController alloc] init];
           vc.fromShare = YES;
           [[SPDCommonTool getWindowTopViewController].navigationController pushViewController:vc animated:YES];
        }
        
    }else{
        if ([self.selectIndexPath containsObject:indexPath]) {
            [self.selectIndexPath removeObject:indexPath];
            [self.friendCollectionView reloadData];
        }else{
            [self.selectIndexPath addObject:indexPath];
            [self.friendCollectionView reloadData];
        }
        [self handleArray];
        self.selectedAllBtn.selected = (self.usersArray.count == self.selectIndexPath.count);
    }
}

- (void)handleArray {
     if (self.selectIndexPath.count >0 && self.shareViewBottomCons.constant < 0) {
        [self bringSubviewToFront:self.shareView];
        [UIView animateWithDuration:0.25 animations:^{
            self.shareViewBottomCons.constant = 0;
            self.shareView.alpha = 1;
        } completion:^(BOOL finished) {

        }];
    }
    if (self.selectIndexPath.count > 1) {
        self.resultLabel.text = @"分别发送".localized;
    }
    
    if (self.selectIndexPath.count == 1) {
        self.resultLabel.text = @"发送".localized;
    }
    
    if (self.selectIndexPath.count == 0) {
       [UIView animateWithDuration:0.25 animations:^{
           self.shareViewBottomCons.constant = - (200+IphoneX_Bottom);
       } completion:^(BOOL finished) {
           self.shareView.alpha = 0;
       }];
    }
}

- (void)shareto:(SPDUMShareType)type {
    [self hide];
//    NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,self.shareImage]]];
//    UIImage *image = [[UIImage alloc] initWithData:data];
//    [[SPDUMShareUtils shareInstance] shareToPlatform:type WithShareUrl:self.shareUrl andshareTitle:self.shareTitle andshareContent:self.shareTitle andImageData:image andShareSourece:@"my_info" andSPDUMShareSuccess:^(id shareSuccess) {
//
//        [SPDCommonTool showWindowToast:SPDStringWithKey(@"分享成功!", nil)];
//
//    } presentedController:[SPDCommonTool getWindowTopViewController] andSPDUMShareFailure:^(id shareFailure) {
//
//    }];
//    NSDictionary *params = @{@"liveId": self.liveInfo.liveId,
//                             @"liveUserId": self.liveInfo.userId,
//                             @"code": @"liveShare"
//    };
//    [LY_Network postRequestWithURL:LY_Api.live_fansGroup_finishTask parameters:params success:^(id  _Nullable response) {
//    } failture:^(NSError * _Nullable error) {
//    }];
}

- (void)shareToMessager {
//    FBSDKShareMessengerURLActionButton *urlButton = [[FBSDKShareMessengerURLActionButton alloc] init];
//    NSMutableString * str = [NSMutableString stringWithFormat:@"%@",self.shareUrl];
//    if(!str) return;
//    [str appendString:@"&from=messenger"];
//    urlButton.title = str;
//    urlButton.url = [NSURL URLWithString:str];
//
//    FBSDKShareMessengerGenericTemplateElement *element = [[FBSDKShareMessengerGenericTemplateElement alloc] init];
//    element.title = self.shareTitle?:@"welcome to Famy";
//    element.subtitle = self.shareTitle?:@"welcome to Famy";
//    element.imageURL = [NSURL URLWithString:@""];
//    element.button = urlButton;
//
//    FBSDKShareMessengerGenericTemplateContent *content = [[FBSDKShareMessengerGenericTemplateContent alloc] init];
//    content.pageID = FacebookKey;// Your page ID, required for attribution
//    content.element = element;
//
//    FBSDKMessageDialog *messageDialog = [[FBSDKMessageDialog alloc] init];
//    messageDialog.shareContent = content;
//
//    if ([messageDialog canShow]) {
//        [messageDialog show];
//    }
//
    //[MobClick event:@"clickShare_Messenger"];
//    NSDictionary *params = @{@"liveId": self.liveInfo.liveId,
//                             @"liveUserId": self.liveInfo.userId,
//                             @"code": @"liveShare"
//    };
//    [LY_Network postRequestWithURL:LY_Api.live_fansGroup_finishTask parameters:params success:^(id  _Nullable response) {
//    } failture:^(NSError * _Nullable error) {
//    }];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    CGFloat height = [NSString sizeWithString:textView.text andFont:[UIFont systemFontOfSize:12] andMaxSize:CGSizeMake(kScreenW - 110 - 18 -10 - 19, 50)].height;
    [UIView animateWithDuration:0.2 animations:^{
        if (height > 34) {
            self.textViewHeightCons.constant = (height + 10 > 50) ? 50 : height;
        }
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}


#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ![touch.view isDescendantOfView:self.shareView] && ![touch.view isDescendantOfView:self.bottomView];
}

-(void)animateCollection{
    NSArray *cells = _shareCollectionView.visibleCells;
    CGFloat collectionHeight = self.shareCollectionView.bounds.size.height;
    for (UICollectionViewCell *cell in cells.objectEnumerator) {
        cell.alpha = 1.0f;
        cell.transform = CGAffineTransformMakeTranslation(0, collectionHeight);
        NSUInteger index = [cells indexOfObject:cell];
        [UIView animateWithDuration:0.7f delay:0.05*index usingSpringWithDamping:0.8 initialSpringVelocity:0 options:0 animations:^{
            cell.transform =  CGAffineTransformMakeTranslation(0, 0);
        } completion:nil];
    }
}
#pragma mark - getters


- (NSMutableArray *)usersArray {
    if (!_usersArray) {
        _usersArray = [NSMutableArray new];
    }
    return _usersArray;
}

- (NSMutableArray *)shareArray {
    if (!_shareArray) {
        _shareArray = [[NSMutableArray alloc]initWithArray:@[@{@"name":@"WhatsApp",@"avatar":@"ic_whatsAPP"},
        @{@"name":@"Messager",@"avatar":@"ic_messenger"},
        @{@"name":@"Telegram",@"avatar":@"ic_telegram"},
        @{@"name":@"Facebook",@"avatar":@"ic_facebook"},
        @{@"name":@"Twitter",@"avatar":@"ic_twitter"},
        @{@"name":@"Wechat",@"avatar":@"ic_wechat"},
        @{@"name":@"Moment",@"avatar":@"ic_moments"},
        @{@"name":@"AddressBook",@"avatar":@"ic_contacts"}]];
    }
    return _shareArray;
}

@end
