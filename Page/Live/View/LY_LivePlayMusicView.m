//
//  LY_LivePlayMusicView.m
//  SimpleDate
//
//  Created by 杨宝龙 on 2020/5/28.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_LivePlayMusicView.h"

@interface LY_LivePlayMusicView ()

// 当前播放音乐名
@property(nonatomic, strong) UILabel *currentMusicNameLabel;

// 下一首音乐名
@property(nonatomic, strong) UILabel *nextMusicNameLabel;

// 关闭按钮
@property(nonatomic, strong) UIButton *closeBtn;

// 音量条
@property(nonatomic, strong) UISlider *volumeSlider;

// 音乐循环模式按钮
@property(nonatomic, strong) UIButton *playModeBtn;

// 上一首
@property(nonatomic, strong) UIButton *lastBtn;

// 暂停、播放按钮
@property(nonatomic, strong) UIButton *playBtn;

// 下一首
@property(nonatomic, strong) UIButton *nextBtn;

// 音乐列表
@property(nonatomic, strong) UIButton *musicListBtn;

@end

@implementation LY_LivePlayMusicView

- (UILabel *)currentMusicNameLabel {
    if (!_currentMusicNameLabel) {
        _currentMusicNameLabel = [[UILabel alloc] init];
        _currentMusicNameLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _currentMusicNameLabel.font = [UIFont mediumFontOfSize:15];
        _currentMusicNameLabel.textAlignment = NSTextAlignmentCenter;
        
    }
    return _currentMusicNameLabel;
}

- (UILabel *)nextMusicNameLabel {
    if (!_nextMusicNameLabel) {
        _nextMusicNameLabel = [[UILabel alloc] init];
        _nextMusicNameLabel.textColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.6];
        _nextMusicNameLabel.font = [UIFont mediumFontOfSize:12];
        _nextMusicNameLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _nextMusicNameLabel;
}

- (UIButton *)closeBtn {
    if (!_closeBtn) {
        _closeBtn = [[UIButton alloc] init];
        [_closeBtn setImage:[UIImage imageNamed:@"ic_zhibo_zhubo_yinyue_guanbi"] forState:UIControlStateNormal];
        [_closeBtn addTarget:self action:@selector(closeBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeBtn;
}

- (UISlider *)volumeSlider {
    if (!_volumeSlider) {
        _volumeSlider = [[UISlider alloc] init];
        [_volumeSlider setThumbImage:[UIImage imageNamed:@"ic_zhibo_zhubo_yinyue_tiaojieyinliang"] forState:UIControlStateNormal];
        _volumeSlider.tintColor = [[UIColor colorWithHexString:@"#6A2CF7"] colorWithAlphaComponent:0.5];
        _volumeSlider.thumbTintColor = [UIColor colorWithHexString:@"#6A2CF7"];
    }
    return _volumeSlider;
}

- (UIButton *)playModeBtn {
    if (!_playModeBtn) {
        _playModeBtn = [[UIButton alloc] init];
        [_playModeBtn setImage:[UIImage imageNamed:@"ic_zhibo_zhubo_yinyue_danquxunhuan"] forState:UIControlStateNormal];
        [_playModeBtn addTarget:self action:@selector(playModeBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _playBtn;
}

- (UIButton *)musicListBtn {
    if (!_musicListBtn) {
        _musicListBtn = [[UIButton alloc] init];
        [_musicListBtn setImage:[UIImage imageNamed:@"ic_zhibo_zhubo_yinyue_liebiao"] forState:UIControlStateNormal];
        [_musicListBtn addTarget:self action:@selector(musicListBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _musicListBtn;
}

//- (UIButton *)lastBtn {
//    if (!_lastBtn) {
//        _lastBtn = [[UIButton alloc] init];
//        [_lastBtn setImage:[UIImage imageNamed:@""] forState:<#(UIControlState)#>]
//    }
//    return _lastBtn;
//}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
        self.currentMusicNameLabel.text = @"阿u收到回复气味很快进入哈口味啊看收到回复";
        self.nextMusicNameLabel.text = @"asdjyfjeqwasdffasd";
    }
    return self;
}

- (void)willPresent {
    
}

- (void)animatePresent {
    self.containerView.frame = CGRectMake(22.5, [UIScreen mainScreen].bounds.size.height - self.containerHeight - TabBarHeight, [UIScreen mainScreen].bounds.size.width - 22.5 * 2, self.containerHeight);
}

- (void)willDismiss {
    
}

- (void)animateDismiss {
    self.containerView.frame = CGRectMake(22.5, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width - 22.5 * 2, self.containerHeight);
}

- (CGFloat)containerHeight {
    return 200;
}

- (void)setupUI {
    self.containerView.layer.cornerRadius = 5;
    [self addSubview:self.containerView];
    [self.containerView addSubview:self.currentMusicNameLabel];
    [self.containerView addSubview:self.nextMusicNameLabel];
    [self.containerView addSubview:self.closeBtn];
    [self.containerView addSubview:self.volumeSlider];
    [self.containerView addSubview:self.playModeBtn];
    [self.containerView addSubview:self.musicListBtn];
}

- (void)setupUIFrame {
    self.containerView.frame = CGRectMake(22.5, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width - 22.5 * 2, self.containerHeight);
    [self.currentMusicNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(53);
        make.trailing.mas_equalTo(-53);
        make.top.mas_equalTo(35);
    }];
    [self.nextMusicNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.mas_equalTo(self.currentMusicNameLabel);
        make.top.mas_equalTo(self.currentMusicNameLabel.mas_bottom).offset(8);
    }];
    [self.closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(35);
        make.centerY.mas_equalTo(self.currentMusicNameLabel);
        make.leading.mas_equalTo(self.currentMusicNameLabel.mas_trailing).offset(5);
    }];
    [self.volumeSlider mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.mas_equalTo(self.currentMusicNameLabel);
        make.top.mas_equalTo(self.nextMusicNameLabel.mas_bottom).offset(18);
    }];
    [self.playModeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(35);
        make.leading.mas_equalTo(15);
        make.bottom.mas_equalTo(-20);
    }];
    [self.musicListBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(35);
        make.trailing.mas_equalTo(-15);
        make.bottom.mas_equalTo(-20);
    }];
//    self.volumeSlider
}

- (void)closeBtnAction {
    
}

- (void)playModeBtnAction {
    
}

- (void)musicListBtnAction {
    
}

@end
