//
//  LY_LiveFansInviteMessageCell.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/23.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_BaseMessageCell.h"
#import "LY_LiveFansInviteMessage.h"

NS_ASSUME_NONNULL_BEGIN

@class LY_LiveFansInviteMessageCell;

@protocol LY_LiveFansInviteMessageCellDelegate <NSObject>

- (void)liveFansInviteMessageCell:(LY_LiveFansInviteMessageCell *)liveFansInviteMessageCell didClickJoinBtn:(UIButton *)joinBtn;

- (void)liveFansInviteMessageCell:(LY_LiveFansInviteMessageCell *)liveFansInviteMessageCell didClickSeeBtn:(UIButton *)seeBtn;


@end


@interface LY_LiveFansInviteMessageCell : LY_BaseMessageCell

@property(nonatomic, weak) id<LY_LiveFansInviteMessageCellDelegate>delegate;

//@property(nonatomic, strong) LY_LiveFansInviteMessage *message;

@end

NS_ASSUME_NONNULL_END
