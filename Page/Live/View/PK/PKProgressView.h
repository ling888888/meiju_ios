//
//  PKProgressView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/6/28.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PKProgressView : UIView

// 更新别人的分数
- (void)updatePKScore:(NSInteger)mineScore otherScore:(NSInteger)otherScore;

@end

NS_ASSUME_NONNULL_END
