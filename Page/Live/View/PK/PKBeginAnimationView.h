//
//  PKBeginAnimationView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/6/23.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LivePKBeginMessage,PKBeginAnimationView;

@protocol PKBeginAnimationViewDelegate <NSObject>

@optional
- (void)pkBeginAnimationViewdidFinishPKBeginAnimation:(PKBeginAnimationView *)animationView WithMessage:(LivePKBeginMessage *)message;

@end

@interface PKBeginAnimationView : UIView

- (void)startAnimation;

// config data 1
- (void)configMessage:(LivePKBeginMessage *)beginMsg andMineAnchorUserId:(NSString *)anchorId;

@property (nonatomic, weak)id<PKBeginAnimationViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
