//
//  PKReceiveInviteMessageView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/7/3.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PKReceiveInviteMessageView.h"
#import "LivePKInviteMessage.h"
#import "UIButton+HXExtension.h"

@interface PKReceiveInviteMessageView ()

@property (nonatomic, strong) UIImageView * backgroundImageView;
@property (nonatomic, strong) UIImageView * titleImageView;
@property (nonatomic, strong) UILabel * timerLabel;
@property (nonatomic, strong) UILabel * messageLabel;
@property (nonatomic, strong) UIButton * acceptButton;
@property (nonatomic, strong) UIButton * refuseButton;
@property (nonatomic, strong) UIButton * flagButton; // 本场要不要再出现pk邀请
@property (nonatomic, strong) UILabel * flagLabel;
@property (nonatomic, strong) NSTimer * timer;
@property (nonatomic, assign) NSInteger second; // 邀请框显示时间 10s

@end


@implementation PKReceiveInviteMessageView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.second = 10;
        self.shouldDismissOnTouchBackgound = NO;
        self.containerView.backgroundColor = [UIColor clearColor];
        self.containerView.clipsToBounds = NO;
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews {
    [self.containerView addSubview:self.backgroundImageView];
    [self.backgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    [self.containerView addSubview:self.titleImageView];
    [self.titleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(-45);
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(160, 90));
    }];
    [self.containerView addSubview:self.timerLabel];
    [self.timerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(self.titleImageView.mas_bottom).offset(10);
    }];
    [self.containerView addSubview:self.messageLabel];
    [self.messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_offset(5);
        make.trailing.mas_offset(-5);
        make.top.equalTo(self.timerLabel.mas_bottom).offset(10);
        make.centerX.mas_equalTo(0);
    }];
    [self.containerView addSubview:self.acceptButton];
    [self.acceptButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(170, 40));
        make.top.equalTo(self.messageLabel.mas_bottom).offset(25);
    }];
    [self.containerView addSubview:self.refuseButton];
    [self.refuseButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(170, 40));
        make.top.equalTo(self.acceptButton.mas_bottom).offset(25);
    }];
    [self.containerView addSubview:self.flagLabel];
    [self.flagLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(kIsMirroredLayout ? -9 : 9);
        make.top.equalTo(self.refuseButton.mas_bottom).offset(25);
        make.bottom.mas_equalTo(-50);
    }];
    [self.containerView addSubview:self.flagButton];
    [self.flagButton hx_setEnlargeEdgeWithTop:10 right:10 bottom:10 left:10];
    [self.flagButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.flagLabel.mas_centerY);
        make.trailing.equalTo(self.flagLabel.mas_leading).offset(-6);
        make.size.mas_equalTo(12);
    }];
}

- (void)setInviteMsg:(LivePKInviteMessage *)inviteMsg {
    _inviteMsg = inviteMsg;
    self.messageLabel.text = [NSString stringWithFormat:@"%@邀请你进行PK，是否应战？".localized,_inviteMsg.userName];
    [self.timer fire];
}

- (void)handleTimer:(NSTimer* )timer {
    self.timerLabel.text = [NSString stringWithFormat:@"%@(%@s)",@"PK邀请".localized,@(self.second)];
    if (self.second == 0) {
        NSDictionary * dict = @{
            @"pkId": @(_inviteMsg.pkId),
            @"type": @(0),
            @"noDisturb": @(0),
            @"inChat": @(2)
        };
        [self requestInviteDeal:dict];
        [self dismiss];
        [self resetData];
        return;
    }
    self.second --;
}

- (void)resetData {
    [self.timer invalidate];
    self.timer = nil;
}

#pragma mark - responses

- (void)acceptClicked:(UIButton *)sender {
    NSDictionary * dict = @{
          @"pkId": @(_inviteMsg.pkId),
          @"type": @(1),
          @"noDisturb": @(_flagButton.selected)
    };
    [self requestInviteDeal:dict];
    if (self.delegate && [self.delegate respondsToSelector:@selector(receivePKInviteMessageViewDidClickedAgreeBtn:)]) {
        [self.delegate receivePKInviteMessageViewDidClickedAgreeBtn:self];
    }
    [self dismiss];
    [self resetData];
}

- (void)refuseClicked:(UIButton *)sender {
    NSDictionary * dict = @{
        @"pkId": @(_inviteMsg.pkId),
        @"type": @(0),
        @"noDisturb": @(_flagButton.selected)
    };
    [self requestInviteDeal:dict];
    [self dismiss];
    [self resetData];
}

- (void)flagButtonClicked:(UIButton *)sender {
    sender.selected = !sender.selected;
}

#pragma mark - request

- (void)requestInviteDeal:(NSDictionary *)params {
    [RequestUtils POST:URL_NewServer(@"live/pk.invite.deal") parameters:params success:^(id  _Nullable suceess) {
        
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

#pragma mark - getters

- (UIImageView *)backgroundImageView {
    if (!_backgroundImageView) {
        _backgroundImageView = [UIImageView new];
        _backgroundImageView.image = [UIImage imageNamed:@"img_zhibo_pk_yaoqing_bg"];
    }
    return _backgroundImageView;
}

- (UIImageView *)titleImageView {
    if (!_titleImageView) {
        _titleImageView = [UIImageView new];
        _titleImageView.image = [UIImage imageNamed:@"img_zhibo_pk_yaoqing_toutu"];
        _titleImageView.contentMode = UIViewContentModeCenter;
    }
    return _titleImageView;
}

- (UILabel *)timerLabel {
    if (!_timerLabel) {
        _timerLabel = [UILabel new];
        _timerLabel.textColor = [UIColor whiteColor];
        _timerLabel.font = [UIFont boldSystemFontOfSize:18];
        _timerLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _timerLabel;
}

- (UILabel *)messageLabel {
    if (!_messageLabel) {
        _messageLabel = [UILabel new];
        _messageLabel.textColor = [UIColor whiteColor];
        _messageLabel.font = [UIFont boldSystemFontOfSize:15];
        _messageLabel.textAlignment = NSTextAlignmentCenter;
        _messageLabel.numberOfLines = 2;
    }
    return _messageLabel;
}

- (UIButton *)acceptButton {
    if (!_acceptButton) {
        _acceptButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_acceptButton setBackgroundColor:SPD_HEXCOlOR(COMMON_PINK)];
        [_acceptButton  setTitle:@"接受PK邀请".localized forState:UIControlStateNormal];
        _acceptButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
        [_acceptButton setTitleColor:SPD_HEXCOlOR(@"ffffff") forState:UIControlStateNormal];
        _acceptButton.layer.cornerRadius = 20;
        [_acceptButton addTarget:self action:@selector(acceptClicked:) forControlEvents:UIControlEventTouchUpInside];

    }
    return _acceptButton;
}

- (UIButton *)refuseButton {
    if (!_refuseButton) {
        _refuseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_refuseButton setBackgroundColor:[UIColor clearColor]];
        [_refuseButton  setTitle:@"拒绝".localized forState:UIControlStateNormal];
        _refuseButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
        [_refuseButton setTitleColor:SPD_HEXCOlOR(@"ffffff") forState:UIControlStateNormal];
        _refuseButton.layer.borderColor = [UIColor whiteColor].CGColor;
        _refuseButton.layer.borderWidth = 1;
        _refuseButton.layer.cornerRadius = 20;
        [_refuseButton addTarget:self action:@selector(refuseClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _refuseButton;
}

- (UILabel *)flagLabel {
    if (!_flagLabel) {
        _flagLabel = [UILabel new];
        _flagLabel.textColor = [UIColor whiteColor];
        _flagLabel.font = [UIFont mediumFontOfSize:13];
        _flagLabel.textAlignment = NSTextAlignmentCenter;
        _flagLabel.text = @"本场不出现PK邀请".localized;
    }
    return _flagLabel;
}

- (UIButton *)flagButton {
    if (!_flagButton) {
        _flagButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_flagButton setImage:[UIImage imageNamed:@"ic_zhibo_pk_yaoqing"] forState:UIControlStateNormal];
        [_flagButton setImage:[UIImage imageNamed:@"ic_zhibo_pk_yaoqing_s"] forState:UIControlStateSelected];
        _flagButton.selected = NO;
        [_flagButton addTarget:self action:@selector(flagButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _flagButton;
}

- (NSTimer *)timer {
    if (!_timer) {
        _timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(handleTimer:) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
    }
    return _timer;
}

@end
