//
//  PKProcessAnimationView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/6/24.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class PKProcessAnimationView,LivePKBeginMessage,LivePKTimeMessage,LivePKScoreMessage,PKCheckBeheadingMsg,PKResultMessage;

@protocol PKProcessAnimationViewDelegate <NSObject>

// pk 结束
- (void)PKProcessAnimationViewDidEndPK:(PKProcessAnimationView *)processView otherAnchorID:(NSString *)otherAnchorID;
- (void)PKProcessAnimationViewDidTapContributionList:(NSString * )pkId liveId:(NSString *)liveId andType:(NSString *)type;

@end

@interface PKProcessAnimationView : UIView

@property (nonatomic, weak)id<PKProcessAnimationViewDelegate> delegate;

- (void)configMessage:(LivePKBeginMessage *)beginMsg andMineAnchorUserId:(NSString *)anchorId;

- (void)updateTimeWith:(LivePKTimeMessage *)timeMsg;

- (void)updateScore:(LivePKScoreMessage *)scoreMsg;

- (void)updateBehindMsg:(PKCheckBeheadingMsg *)msg;

- (void)updateResultMsg:(PKResultMessage *)msg;

@end

NS_ASSUME_NONNULL_END
