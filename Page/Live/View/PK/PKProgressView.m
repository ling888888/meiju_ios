//
//  PKProgressView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/6/28.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PKProgressView.h"
#import "UIView+RGSize.h"
#import "YYWebImage.h"

@interface PKProgressView ()

@property (nonatomic, strong) UIView * mineView;
@property (nonatomic, strong) UILabel * mineScoreLabel;
@property (nonatomic, strong) UIView * otherView;
@property (nonatomic, strong) UILabel * otherScoreLabel;
@property (nonatomic, strong) YYAnimatedImageView * flagImageView; //
@property (nonatomic, strong) UIImageView * flagBgImageView; //
@end

@implementation PKProgressView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (UIImageView *)flagBgImageView {
    if (!_flagBgImageView) {
        _flagBgImageView = [UIImageView new];
    }
    return _flagBgImageView;
}

- (void)initSubViews {
    self.layer.cornerRadius = self.height/2.0f;
        
    [self addSubview:self.mineView];
    [self.mineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(0);
        make.height.mas_equalTo(self.height);
        make.width.mas_equalTo(self.width/2.0f);
    }];
    self.mineView.layer.cornerRadius = self.height/2.0f;
    
    [self addSubview:self.otherView];
    [self.otherView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(0);
        make.height.mas_equalTo(self.height);
        make.width.mas_equalTo(self.width/2.0f);
    }];
    self.otherView.layer.cornerRadius = self.height/2.0f;

    [self addSubview:self.mineScoreLabel];
    [self.mineScoreLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mineView.mas_centerY);
        make.leading.mas_equalTo(15);
    }];
    
    [self addSubview:self.otherScoreLabel];
    [self.otherScoreLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.otherView.mas_centerY);
        make.trailing.mas_equalTo(-15);
    }];
    
    [self addSubview:self.flagImageView];
    [self.flagImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.trailing.mas_equalTo(self.mineView.mas_trailing).offset(25/2.0f);
        make.size.mas_equalTo(CGSizeMake(25, 39));
    }];
    [self insertSubview:self.flagBgImageView belowSubview:self.flagImageView];
    [self.flagBgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(25);
        make.center.equalTo(self.flagImageView);
    }];
    self.flagBgImageView.image = [UIImage mirroredImageNamed:@"img_zhibo_pkjingdutiao"];
}

- (void)updatePKScore:(NSInteger)mineScore otherScore:(NSInteger)otherScore {
    if (mineScore == 0 && otherScore == 0) {
        return;
    }
    _otherScoreLabel.text = [NSString stringWithFormat:@"%@ 对方".localized,@(otherScore)];
    _mineScoreLabel.text = [NSString stringWithFormat:@"我方 %@".localized,@(mineScore)];

    CGFloat ratio = mineScore * 1.0f/ (mineScore + otherScore);
    CGFloat mineWith = (self.width - 70 *2) * ratio + 70;
    [UIView animateWithDuration:0.3 animations:^{
        [self.mineView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(mineWith);
        }];
        [self.otherView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(self.width - mineWith);
        }];
        [self layoutIfNeeded];
    }];
}

- (YYAnimatedImageView *)flagImageView {
    if (!_flagImageView) {
        _flagImageView = [YYAnimatedImageView new];
        _flagImageView.yy_imageURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:kIsMirroredLayout ? @"pk_fen_ar":@"pk_fen" ofType:@"webp"]];
    }
    return _flagImageView;
}

- (UIView *)mineView {
    if (!_mineView) {
        _mineView = [UIView new];
        _mineView.backgroundColor = SPD_HEXCOlOR(@"FF6B93");
    }
    return _mineView;
}

- (UIView *)otherView {
    if (!_otherView) {
        _otherView = [UIView new];
        _otherView.backgroundColor = SPD_HEXCOlOR(@"5591FF");
    }
    return _otherView;
}

- (UILabel *)otherScoreLabel {
    if (!_otherScoreLabel) {
        _otherScoreLabel = [UILabel new];
        _otherScoreLabel.font = [UIFont boldSystemFontOfSize:12];
        _otherScoreLabel.text = [NSString stringWithFormat:@"%@ 对方".localized,@"0"];
        _otherScoreLabel.textColor = SPD_HEXCOlOR(@"ffffff");
    }
    return _otherScoreLabel;
}

- (UILabel *)mineScoreLabel {
    if (!_mineScoreLabel) {
        _mineScoreLabel = [UILabel new];
        _mineScoreLabel.font = [UIFont boldSystemFontOfSize:12];
        _mineScoreLabel.text = [NSString stringWithFormat:@"我方 %@".localized,@"0"];
        _mineScoreLabel.textColor = SPD_HEXCOlOR(@"ffffff");
    }
    return _mineScoreLabel;
}

@end
