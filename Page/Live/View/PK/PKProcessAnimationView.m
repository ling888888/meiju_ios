//
//  PKProcessAnimationView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/6/24.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PKProcessAnimationView.h"
#import "PKProgressView.h"
#import "UIView+RGSize.h"
#import "LivePKBeginMessage.h"
#import "LY_GradientView.h"
#import "LivePKTimeMessage.h"
#import "UIView+LY.h"
#import "LivePKScoreMessage.h"
#import "PKKOProgressView.h"
#import "YYWebImage.h"
#import "UIView+LY.h"
#import "PKCheckBeheadingMsg.h"
#import "PKResultView.h"
#import "PKResultMessage.h"
#import "KKPaddingLabel.h"
#import "ZegoKitManager.h"
#import "LiveModel.h"
#import "LY_HalfNavigationController.h"
#import "LY_FansGroupRuleViewController.h"
#import "UIButton+HXExtension.h"
@interface PKProcessAnimationView ()

@property (nonatomic, strong) LY_PortraitView * minePortraitView;
@property (nonatomic, copy) NSString * mineAnchorPortraitUrl; // 本方主播的头像url 给结果消息使用
@property (nonatomic, strong) UILabel * minePKStstusLabel;

@property (nonatomic, strong) LY_PortraitView * mFristPortraitView;
@property (nonatomic, strong) LY_PortraitView * mSecondPortraitView;
@property (nonatomic, strong) LY_PortraitView * mThridPortraitView;

@property (nonatomic, strong) NSMutableArray * minePortraitViewArray;
@property (nonatomic, strong) NSMutableArray * otherPortraitViewArray;

@property (nonatomic, strong) LY_PortraitView * otherPortraitView;
@property (nonatomic, strong) UILabel * otherPKStstusLabel;
@property (nonatomic, strong) LY_PortraitView * oFristPortraitView;
@property (nonatomic, strong) LY_PortraitView * oSecondPortraitView;
@property (nonatomic, strong) LY_PortraitView * oThridPortraitView;
@property (nonatomic, strong) PKProgressView * progressView;

@property (nonatomic, strong) UILabel * timeLabel;
@property (nonatomic, strong) UIImageView * timeLabelBgImageView;

@property (nonatomic, strong) UILabel * mineNameLabel;
@property (nonatomic, strong) UILabel * otherNameLabel;
@property (nonatomic, strong) UIButton * ruleButton;
@property (nonatomic, strong) LY_GradientView *gradient;
@property (nonatomic, assign) double timeDifference;
@property (nonatomic, strong) NSTimer * timer;
@property (nonatomic, assign) NSInteger time; //shengyu 时间
@property (nonatomic, assign) NSInteger myTime;

@property (nonatomic, assign) NSInteger round; // [ 1:pk 2:k.o 3:final 4:chengfa]
@property (nonatomic, copy) NSString * pkId;
@property (nonatomic, assign) BOOL isShanShuo; // 已经正在执行闪烁动画
@property (nonatomic, assign) BOOL isKill; // 当前是不是在斩杀阶段

@property (nonatomic, strong) YYAnimatedImageView * KOWinAnimationImageView; //胜利
@property (nonatomic, strong) PKKOProgressView * koProgressView;

@property (nonatomic, strong) YYAnimatedImageView * animationImageView;

@property (nonatomic, strong) UIImageView * topBarImageView;
@property (nonatomic, strong) NSString * mineLiveId;
@property (nonatomic, strong) NSString * mineAnchorId;
@property (nonatomic, strong) NSString * otherAnchorId;

@property (nonatomic, strong) NSString * otherLiveId;
@property (nonatomic, assign) int mineScore; // 我的分数
@property (nonatomic, assign) int otherScore;
@property (nonatomic, strong) NSMutableDictionary * userInfo;
@property (nonatomic, strong) NSArray * mineSupportIds;
@property (nonatomic, strong) NSArray * otherSupportIds;
@property (nonatomic, strong) UIView * breathView;
@property (nonatomic, strong) UILabel * finalTimeLabel; // 最终时间的倒计时
@property (nonatomic, strong) UIImageView * mineStatusImageView;
@property (nonatomic, strong) UIImageView * otherStatusImageView;

@end

@implementation PKProcessAnimationView

- (UILabel *)finalTimeLabel {
    if (!_finalTimeLabel) {
        _finalTimeLabel = [UILabel new];
        _finalTimeLabel.font = [UIFont boldSystemFontOfSize:24];
        _finalTimeLabel.textColor = [UIColor whiteColor];
    }
    return _finalTimeLabel;
}

- (UIView *)breathView {
    if (!_breathView) {
        _breathView = [UIView new];
    }
    return _breathView;
}

- (NSArray *)mineSupportIds {
    if (!_mineSupportIds) {
        _mineSupportIds = [NSArray new];
    }
    return _mineSupportIds;
}

- (UILabel *)minePKStstusLabel {
    if (!_minePKStstusLabel) {
        _minePKStstusLabel = [UILabel new];
        _minePKStstusLabel.font = [UIFont mediumFontOfSize:12];
        _minePKStstusLabel.textColor = SPD_HEXCOlOR(@"ffffff");
    }
    return _minePKStstusLabel;
}

- (UILabel *)otherPKStstusLabel {
    if (!_otherPKStstusLabel) {
        _otherPKStstusLabel = [UILabel new];
        _otherPKStstusLabel.font = [UIFont mediumFontOfSize:12];
        _otherPKStstusLabel.textColor = SPD_HEXCOlOR(@"ffffff");
    }
    return _otherPKStstusLabel;
}

- (NSArray *)otherSupportIds {
    if (!_otherSupportIds) {
        _otherSupportIds = [NSArray new];
    }
    return _otherSupportIds;
}

- (NSMutableDictionary *)userInfo {
    if (!_userInfo) {
        _userInfo = [NSMutableDictionary new];
    }
    return _userInfo;
}

- (UIImageView *)timeLabelBgImageView {
    if (!_timeLabelBgImageView) {
        _timeLabelBgImageView = [UIImageView new];
        _timeLabelBgImageView.image = [UIImage imageNamed:@"ic_zhibo_pk_shijianbeijing"];
    }
    return _timeLabelBgImageView;
}

- (UIImageView *)topBarImageView {
    if (!_topBarImageView) {
        _topBarImageView = [UIImageView new];
        _topBarImageView.image = [UIImage imageNamed:@"img_zhibo_pk_lvtiao"];
    }
    return _topBarImageView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.minePortraitViewArray = [NSMutableArray new];
        self.otherPortraitViewArray = [NSMutableArray new];
        [self initSubViews];
        [self requestTimeDifference];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.koProgressView addRounded:UIRectCornerTopLeft|UIRectCornerTopRight withRadius:5];
    [self.timeLabel addRounded:UIRectCornerBottomRight|UIRectCornerBottomLeft withRadius:12.5];

}

- (YYAnimatedImageView *)animationImageView {
    if (!_animationImageView) {
        _animationImageView = [YYAnimatedImageView new];
        _animationImageView.contentMode = UIViewContentModeCenter;
    }
    return _animationImageView;
}

- (PKKOProgressView *)koProgressView {
    if (!_koProgressView) {
        _koProgressView = [[[NSBundle mainBundle]loadNibNamed:@"PKKOProgressView" owner:self options:nil] lastObject];
    }
    return _koProgressView;
}
 
- (void)initSubViews {
    
    self.backgroundColor = SPD_HEXCOlOR(@"#6A2CF7");
    CGFloat leading = 25.0f/375 *kScreenW;
    
    self.gradient = [[LY_GradientView alloc] initWithFrame:self.bounds];
    self.gradient.colors = @[SPD_HEXCOlOR(@"6A2CF7"),SPD_HEXCOlOR(@"9465FF"),SPD_HEXCOlOR(@"6A2CF7")];
    [self insertSubview:self.gradient atIndex:0];
    
    [self addSubview:self.topBarImageView];
    [self.topBarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.width.equalTo(self);
        make.height.mas_equalTo(3);
    }];
    
    // KO 呼吸动画
    [self addSubview:self.breathView];
    self.breathView.layer.borderWidth = 4;
    self.breathView.backgroundColor = [UIColor clearColor];
    self.breathView.layer.borderColor = [UIColor clearColor].CGColor;
    [self.breathView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self addSubview:self.ruleButton];
    [self.ruleButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(6);
        make.trailing.mas_equalTo(-6);
        make.size.mas_equalTo(11);
    }];
    
    [self addSubview:self.minePortraitView];
    [self.minePortraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(leading);
        make.width.and.height.mas_equalTo(97.5);
        make.top.mas_equalTo(5);
    }];
    
    self.mineStatusImageView  = [UIImageView new];
    [self addSubview:_mineStatusImageView];
    _mineStatusImageView.hidden = YES;
    [_mineStatusImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.minePortraitView.mas_centerX);
        make.top.mas_equalTo(5);
        make.size.mas_equalTo(CGSizeMake(70, 38));
    }];
    [self.mineStatusImageView addSubview:self.minePKStstusLabel];
    [self.minePKStstusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(_mineStatusImageView);
    }];
    
    self.mineNameLabel = [UILabel new];
    self.mineNameLabel.textAlignment = NSTextAlignmentCenter;
    self.mineNameLabel.font = [UIFont mediumFontOfSize:12];
    self.mineNameLabel.textColor = [UIColor whiteColor];
    [self addSubview:self.mineNameLabel];
    [self.mineNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.minePortraitView.mas_centerX).offset(0);
        make.top.equalTo(self.minePortraitView.mas_bottom).offset(-5);
        make.width.mas_lessThanOrEqualTo(100);
    }];
    
    [self addSubview:self.otherPortraitView];
    self.otherPortraitView.userInteractionEnabled = YES;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleJumpToOtherLiveRoom:)];
    [self.otherPortraitView addGestureRecognizer:tap];
    [self.otherPortraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.minePortraitView.mas_top);
        make.trailing.mas_equalTo(-leading);
        make.width.and.height.mas_equalTo(97.5);
    }];
    
    self.otherStatusImageView  = [UIImageView new];
    [self addSubview:_otherStatusImageView];
    _otherStatusImageView.hidden = YES;
    [_otherStatusImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.otherPortraitView.mas_centerX);
        make.top.mas_equalTo(5);
        make.size.mas_equalTo(CGSizeMake(70, 38));
    }];
    [_otherStatusImageView addSubview:self.otherPKStstusLabel];
    [self.otherPKStstusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(_otherStatusImageView);
    }];
    
    self.otherNameLabel = [UILabel new];
    self.otherNameLabel.textAlignment = NSTextAlignmentCenter;
    self.otherNameLabel.font = [UIFont mediumFontOfSize:12];
    self.otherNameLabel.textColor = [UIColor whiteColor];
    [self addSubview:self.otherNameLabel];
    [self.otherNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.otherPortraitView.mas_centerX).offset(0);
        make.top.equalTo(self.otherPortraitView.mas_bottom).offset(-5);
        make.width.mas_lessThanOrEqualTo(100);
    }];
    
    [self addSubview:self.mSecondPortraitView];
    [self.mSecondPortraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.minePortraitView.mas_centerX);
        make.width.and.height.mas_equalTo(45);
        make.top.mas_equalTo(self.minePortraitView.mas_bottom).offset(10);
    }];
    self.mSecondPortraitView.imageView.image = [UIImage imageNamed:@"ic_zhibo_pk_123morentu"];
    self.mSecondPortraitView.userInteractionEnabled = YES;
    self.mSecondPortraitView.tag = 0;
    UITapGestureRecognizer * mSTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapContributionListAvatar:)];
    [self.mSecondPortraitView addGestureRecognizer:mSTap];
    
    [self addSubview:self.mFristPortraitView];
    [self.mFristPortraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(self.mSecondPortraitView);
        make.centerY.equalTo(self.mSecondPortraitView);
        make.leading.equalTo(self.mSecondPortraitView.mas_trailing).offset(1);
    }];
    self.mFristPortraitView.imageView.image = [UIImage imageNamed:@"ic_zhibo_pk_123morentu"];
    self.mFristPortraitView.userInteractionEnabled = YES;
    self.mFristPortraitView.tag = 0;
    UITapGestureRecognizer * mFTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapContributionListAvatar:)];
    [self.mFristPortraitView addGestureRecognizer:mFTap];
    
    [self addSubview:self.mThridPortraitView];
    [self.mThridPortraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(self.mSecondPortraitView);
        make.centerY.equalTo(self.mSecondPortraitView);
        make.trailing.equalTo(self.mSecondPortraitView.mas_leading).offset(-1);
    }];
    self.mThridPortraitView.imageView.image = [UIImage imageNamed:@"ic_zhibo_pk_123morentu"];
    self.mThridPortraitView.userInteractionEnabled = YES;
    self.mThridPortraitView.tag = 0;
    
    [self addSubview:self.oSecondPortraitView];
    [self.oSecondPortraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.otherPortraitView.mas_centerX);
        make.width.and.height.mas_equalTo(45);
        make.top.mas_equalTo(self.otherPortraitView.mas_bottom).offset(10);
    }];
    self.oSecondPortraitView.imageView.image = [UIImage imageNamed:@"ic_zhibo_pk_123morentu"];
    self.oSecondPortraitView.userInteractionEnabled = YES;
    self.oSecondPortraitView.tag = 1;
    UITapGestureRecognizer * oSTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapContributionListAvatar:)];
    [self.oSecondPortraitView addGestureRecognizer:oSTap];


    [self addSubview:self.oFristPortraitView];
    [self.oFristPortraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(self.oSecondPortraitView);
        make.centerY.equalTo(self.oSecondPortraitView);
        make.trailing.equalTo(self.oSecondPortraitView.mas_leading).offset(-1);
    }];
    self.oFristPortraitView.userInteractionEnabled = YES;
    self.oFristPortraitView.tag = 1;
    UITapGestureRecognizer * ofTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapContributionListAvatar:)];
    [self.oFristPortraitView addGestureRecognizer:ofTap];
    self.oFristPortraitView.imageView.image = [UIImage imageNamed:@"ic_zhibo_pk_123morentu"];

    [self addSubview:self.oThridPortraitView];
    [self.oThridPortraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(self.oSecondPortraitView);
        make.centerY.equalTo(self.oSecondPortraitView);
        make.leading.equalTo(self.oSecondPortraitView.mas_trailing).offset(1);
    }];
    self.oThridPortraitView.userInteractionEnabled = YES;
    self.oThridPortraitView.tag = 1;
    UITapGestureRecognizer * oTTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapContributionListAvatar:)];
    self.oThridPortraitView.imageView.image = [UIImage imageNamed:@"ic_zhibo_pk_123morentu"];
    [self.oThridPortraitView addGestureRecognizer:oTTap];

    [self addSubview:self.progressView];
    [self.progressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.trailing.mas_equalTo(-15);
        make.bottom.equalTo(self.mas_bottom).offset(12.5f);
        make.height.mas_equalTo(25);
    }];
    
    [self addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.centerX.equalTo(self);
        make.height.mas_equalTo(32);
    }];
    
    [self insertSubview:self.timeLabelBgImageView belowSubview:self.timeLabel];
    [self.timeLabelBgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.timeLabel.mas_leading).offset(-5);
        make.top.equalTo(self.topBarImageView.mas_bottom);
        make.trailing.equalTo(self.timeLabel.mas_trailing).offset(5);
        make.height.mas_equalTo(32);
    }];
    
    [self addSubview:self.animationImageView];
    [self.animationImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(50);
        make.size.mas_equalTo(85);
    }];
    self.animationImageView.yy_imageURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"pk_zhong@2x" ofType:@"webp"]];
    
    UIImageView * img = [UIImageView new];
    img.image = [UIImage imageNamed:@"img_zhibo_pk_shandianbeijin"];
    [self insertSubview:img belowSubview:self.animationImageView];
    [img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.animationImageView);
        make.size.mas_equalTo(CGSizeMake(49, 79));
    }];
    [self.minePortraitViewArray addObjectsFromArray:@[self.mFristPortraitView,self.mSecondPortraitView,self.mThridPortraitView]];
    [self.otherPortraitViewArray addObjectsFromArray:@[self.oFristPortraitView,self.oSecondPortraitView,self.oThridPortraitView]];

    [img addSubview:self.finalTimeLabel];
    self.finalTimeLabel.hidden = YES;
    self.finalTimeLabel.textAlignment = NSTextAlignmentCenter;
    [self.finalTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(img);
        make.size.mas_equalTo(85);
    }];
    
    // label
    for (int i = 1; i< 4; i++) {
        KKPaddingLabel * mineMVPLabel = [KKPaddingLabel new];
        mineMVPLabel.textColor = SPD_HEXCOlOR(@"ffffff");
        mineMVPLabel.text = (i == 1) ? @"MVP":[NSString stringWithFormat:@"%d",i];
        if (i == 1) {
            mineMVPLabel.padding = UIEdgeInsetsMake(3, 3, 3, 3);
        }
        mineMVPLabel.backgroundColor = SPD_HEXCOlOR(@"#FF6B93");
        [self addSubview:mineMVPLabel];
        mineMVPLabel.layer.cornerRadius = 6;
        mineMVPLabel.textAlignment = NSTextAlignmentCenter;
        mineMVPLabel.clipsToBounds = YES;
        mineMVPLabel.font = [UIFont systemFontOfSize:9];
        UIView * lastV ;
        if (i == 1) {
            lastV = self.mFristPortraitView;
        }else if (i == 2){
            lastV = self.mSecondPortraitView;
        }else{
            lastV = self.mThridPortraitView;
        }
        [mineMVPLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(lastV.mas_centerX);
            make.bottom.equalTo(lastV.mas_bottom).offset(-1);
            make.height.mas_offset(12);
            make.width.mas_greaterThanOrEqualTo(12);
        }];
    }
    
    for (int i = 1; i< 4; i++) {
        KKPaddingLabel * mineMVPLabel = [KKPaddingLabel new];
        mineMVPLabel.textColor = SPD_HEXCOlOR(@"ffffff");
        mineMVPLabel.text = (i == 1) ? @"MVP":[NSString stringWithFormat:@"%d",i];
        if (i == 1) {
            mineMVPLabel.padding = UIEdgeInsetsMake(3, 3, 3, 3);
        }
        mineMVPLabel.backgroundColor = SPD_HEXCOlOR(@"5591FF");
        [self addSubview:mineMVPLabel];
        mineMVPLabel.layer.cornerRadius = 6;
        mineMVPLabel.textAlignment = NSTextAlignmentCenter;
        mineMVPLabel.clipsToBounds = YES;
        mineMVPLabel.font = [UIFont systemFontOfSize:9];
        UIView * lastV ;
        if (i == 1) {
            lastV = self.oFristPortraitView;
        }else if (i == 2){
            lastV = self.oSecondPortraitView;
        }else{
            lastV = self.oThridPortraitView;
        }
        [mineMVPLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(lastV.mas_centerX);
            make.bottom.equalTo(lastV.mas_bottom).offset(-1);
            make.height.mas_offset(12);
            make.width.mas_greaterThanOrEqualTo(12);
        }];
    }

    [self insertSubview:self.koProgressView belowSubview:self.progressView];
    self.koProgressView.alpha = 0;
    [self.koProgressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.leading.mas_equalTo(self.progressView.mas_leading).offset(25);
        make.trailing.mas_equalTo(self.progressView.mas_trailing).offset(-25);
        make.bottom.mas_equalTo(self.progressView.mas_top).offset(0);
        make.height.mas_equalTo(33);
    }];
}

- (void)requestTimeDifference {
    [RequestUtils GET:URL_NewServer(@"sys/sys.time") parameters:@{} success:^(id  _Nullable suceess) {
        self.timeDifference = [suceess[@"sysTime"] doubleValue] - [[NSDate date] timeIntervalSince1970] * 1000.0f;
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)handleRuleButtonClicked:(UIButton *)sender {
    NSString *urlString = [NSString stringWithFormat:@"https://%@share.famy.ly/ar/pkRule.html?language=%@", DEV?@"dev-":@"",[SPDCommonTool getFamyLanguage]];
    LY_FansGroupRuleViewController *webVC = [[LY_FansGroupRuleViewController alloc] initWithURLString:urlString];
    webVC.title = @"PK规则".localized;
    LY_HalfNavigationController * nav = [[LY_HalfNavigationController alloc]initWithRootViewController:webVC controllerHeight:412];
    [self.VC presentViewController:nav animated:YES completion:nil];
}

- (void)handleTapContributionListAvatar:(UITapGestureRecognizer *)tap {
    if (self.delegate && [self.delegate respondsToSelector:@selector(PKProcessAnimationViewDidTapContributionList:liveId:andType:)]) {
        [self.delegate PKProcessAnimationViewDidTapContributionList:self.pkId liveId:(tap.view.tag == 0 ?self.mineLiveId:self.otherLiveId) andType:(tap.view.tag == 0?@"0":@"1")];
    }
}

- (void)handleJumpToOtherLiveRoom:(UITapGestureRecognizer *)tap {
    if ([ZegoManager.liveInfo.userId isEqualToString:[SPDApiUser currentUser].userId]) {
        return;
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"JoinInOtherLiveRoomNotification" object:self userInfo:@{@"liveId":self.otherLiveId}];
}

#pragma mark - Handle Data

- (void)configMessage:(LivePKBeginMessage *)beginMsg andMineAnchorUserId:(NSString *)anchorId {
    self.isKill = NO;
    NSString * launchUserId = beginMsg.launchUserId;
    NSString * acceptUserId = beginMsg.acceptUserId;
    NSString * otherUID = [beginMsg.launchUserId isEqualToString:anchorId] ? acceptUserId :launchUserId;
    self.mineAnchorId = anchorId;
    self.otherAnchorId = otherUID;
    if ([beginMsg.launchUserId isEqualToString:anchorId]) {
        self.mineLiveId = beginMsg.launchLiveId;
        self.otherLiveId = beginMsg.acceptLiveId;
    }else{
        self.mineLiveId = beginMsg.acceptLiveId;
        self.otherLiveId = beginMsg.launchLiveId;
    }

    NSDictionary *params = @{@"user_ids": [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:@[otherUID,anchorId] options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding]};
    [RequestUtils commonGetRequestUtils:[params mutableCopy] bURL:@"user.head" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        NSArray *users = suceess[@"users"];
        for (NSDictionary * dic  in users ) {
            if ([dic[@"_id"] isEqualToString:anchorId]) {
                self.mineNameLabel.text = dic[@"nick_name"];
                LY_Portrait * head = [[LY_Portrait alloc]init];
                head.headwearUrl = dic[@"headwearWebp"];
                head.url = dic[@"avatar"];
                self.minePortraitView.portrait = head;
                self.mineAnchorPortraitUrl = dic[@"avatar"];
            }else{
                self.otherNameLabel.text = dic[@"nick_name"];
                LY_Portrait * head = [[LY_Portrait alloc]init];
                head.headwearUrl = dic[@"headwearWebp"];
                head.url = dic[@"avatar"];
                self.otherPortraitView.portrait = head;
            }
        }
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)updateTimeWith:(LivePKTimeMessage *)timeMsg {
    NSLog(@"timemsg ====%@",@(timeMsg.time));
    if (!_timer) {
        self.round = timeMsg.round;
        self.time = timeMsg.time;
        self.pkId = timeMsg.pkId;
        _timer = [NSTimer timerWithTimeInterval:1.0f target:self selector:@selector(handleTimer:) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
        self.myTime = 20;
    }
    if (!self.timer.valid) {
        [self.timer fire];
    }
}

- (void)handleTimer:(NSTimer *)timer {
    int minute = ceil(self.time / 60);
    int second  = ceil(self.time %60);
    NSString * secondStr = second < 10 ? [NSString stringWithFormat:@"0%d",second]:[NSString stringWithFormat:@"%d",second];
    NSString * miniteStr = minute < 10 ? [NSString stringWithFormat:@"0%d",minute]:[NSString stringWithFormat:@"%d",minute];
    if (!self.isKill) {
        self.timeLabel.text = [NSString stringWithFormat:@"%@ %@:%@",self.round == 4 ?(self.mineScore == self.otherScore ? @"交流".localized : @"惩罚".localized):@"PK",miniteStr ,secondStr];
        self.timeLabel.backgroundColor = [UIColor clearColor];
    }
    
    // 斩杀预热阶段
    if (self.time <= 3 * 60 + 20 && self.round == 1) {
        if (self.frame.size.height < 215) {
            [self changeSelfFrameHigher:YES];
            self.animationImageView.image = [UIImage imageNamed:@"img_zhibo_pk_zhunbei"];
        }
        if (self.time <= 3 * 60 + 20 && self.time > 3* 60) {
            self.koProgressView.titleLabel.text = [NSString stringWithFormat:@"%@s",@(second)];
            self.koProgressView.desLabel.text = @"K.O模式即将开启".localized;
            self.koProgressView.titleLabel.backgroundColor = [SPD_HEXCOlOR(@"FFFFFF") colorWithAlphaComponent:0.1];
        }
    }
    
    // 第一阶段结束 d开启第二阶段
    if (self.time == 3 * 60 && self.round == 1) {
        self.round = 2;
    }
    
    // 斩杀阶段
    if (self.time < 3 * 60 && self.time > 60 && self.round == 2) {
        [self processKO];
    }
    
    // 第 3阶段结束开始 finaltime
    if (self.time == 61 && self.round == 2) {
        self.round = 3;
    }
    
    // FinalTime
    if (self.round == 3) {
        self.topBarImageView.hidden = NO;
        self.timeLabelBgImageView.hidden = NO;
        self.timeLabel.text = @"FINAL TIME";
        self.timeLabel.backgroundColor = [UIColor clearColor];
        self.timeLabel.textColor = SPD_HEXCOlOR(@"08312F");
        self.timeLabel.font = [UIFont boldSystemFontOfSize:14];
        self.breathView.layer.borderColor = [UIColor clearColor].CGColor;
        [NSObject cancelPreviousPerformRequestsWithTarget:self];
        // 2s 展示 斩杀失败
        
        if (self.time <= 58) {
            self.finalTimeLabel.hidden = NO;
            self.animationImageView.hidden = YES;
            self.finalTimeLabel.text = [NSString stringWithFormat:@"%@s",@(self.time)];
            if (self.frame.size.height != 180) {
                [self changeSelfFrameHigher:NO];
            }
        }else{
            self.animationImageView.hidden = NO;
            self.animationImageView.image = [UIImage imageNamed:@"img_zhibo_pk_jieshu"];
            self.koProgressView.type = @"5";
        }
        // 3 阶段结束 开始round 4
        if (self.time == 0) {
            self.round = 4;
            self.time = 2* 60;
        }
    }
    

    if (self.round == 4 && self.time <= 2 * 60) {
        [self.animationImageView.layer removeAllAnimations];
        self.mineStatusImageView.hidden = NO;
        self.otherStatusImageView.hidden = NO;
        self.animationImageView.hidden = NO;
        self.finalTimeLabel.hidden = YES;
        self.timeLabel.textColor = SPD_HEXCOlOR(@"08312F");
        self.timeLabel.font = [UIFont boldSystemFontOfSize:14];
        self.animationImageView.yy_imageURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"pk_zhong@2x" ofType:@"webp"]];
        self.mineStatusImageView.image = self.mineScore > self.otherScore ? [UIImage imageNamed:@"img_zhibo_pk_shengli_xiao"]:[UIImage imageNamed:@"img_zhibo_pk_shibai_xiao"];
        self.otherStatusImageView.image = self.otherScore <= self.mineScore ? [UIImage imageNamed:@"img_zhibo_pk_shibai_xiao"]:[UIImage imageNamed:@"img_zhibo_pk_shengli_xiao"];
        self.minePKStstusLabel.text = self.mineScore > self.otherScore ? @"胜利".localized:(self.mineScore == self.otherScore ? @"平局".localized :@"失败".localized);
        self.minePKStstusLabel.textColor = (self.mineScore > self.otherScore)? SPD_HEXCOlOR(@"ffffff"):SPD_HEXCOlOR(@"1A1A1A");
        self.otherPKStstusLabel.textColor = (self.mineScore >= self.otherScore)? SPD_HEXCOlOR(@"1A1A1A"):SPD_HEXCOlOR(@"ffffff");
        self.otherPKStstusLabel.text = self.mineScore > self.otherScore? @"失败".localized: (self.mineScore == self.otherScore ? @"平局".localized :@"胜利".localized);
    }
    if (self.time <= 0 && self.round == 4) {
        [self didStopPK];
    }
    self.time -=1;
}

- (void)didStopPK {
    if (self.delegate && [self.delegate respondsToSelector:@selector(PKProcessAnimationViewDidEndPK: otherAnchorID:)]) {
        [self.delegate PKProcessAnimationViewDidEndPK:self otherAnchorID:self.otherAnchorId];
    }
    [self.timer invalidate];
    self.timer = nil;
    [self removeFromSuperview];
}

// 改变自身view的高度
- (void)changeSelfFrameHigher:(BOOL) higher {
    [UIView animateWithDuration:0.25 animations:^{
        self.koProgressView.alpha = higher;
        CGRect selfFrame = self.frame;
        selfFrame.size.height = higher ? 215 : 180;
        self.frame = selfFrame;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
      
    }];
}

// 斩杀UI
- (void)processKO {
    if (self.isKill) {
        // 斩杀中
        self.koProgressView.type = (self.mineScore >= self.otherScore) ? @"3":@"4";
        if (self.mineScore >= self.otherScore) {
            self.koProgressView.titleLabel.text = [NSString stringWithFormat:@"已领先 %d".localized,self.mineScore - self.otherScore];
            self.koProgressView.desLabel.text = [NSString stringWithFormat:@"%ds后K.O".localized,self.myTime];
            if (self.mineScore != self.otherScore) {
                self.timeLabel.backgroundColor = SPD_HEXCOlOR(@"FF6B93");
                self.timeLabel.text = @"保持K.O！".localized;
            }
        }else{
            self.koProgressView.titleLabel.text = [NSString stringWithFormat:@"已落后 %d".localized,self.otherScore - self.mineScore];
            self.koProgressView.desLabel.text = [NSString stringWithFormat:@"%ds后被K.O".localized,self.myTime];
            self.timeLabel.backgroundColor = SPD_HEXCOlOR(@"5591FF");
            self.timeLabel.text = @"即将被K.O！".localized;
        }
        self.timeLabel.textColor = [UIColor whiteColor];
        self.timeLabel.font = [UIFont systemFontOfSize:12];
        self.myTime--;
        if (self.myTime == 0) {
            self.time = 2 * 60;
            self.round = 4;
        }
    }else{
        self.koProgressView.type = self.mineScore >= self.otherScore ? @"1":@"2";
        self.koProgressView.titleLabel.text = [NSString stringWithFormat:@"%@ %d",((self.mineScore >= self.otherScore) ? @"已领先".localized:@"已落后".localized),abs(self.mineScore - self.otherScore)];
        self.timeLabel.textColor = SPD_HEXCOlOR(@"08312F");
        self.timeLabel.font = [UIFont boldSystemFontOfSize:14];
    }
    if (self.size.height < 215) {
        [self changeSelfFrameHigher:YES];
    }
    if (!self.isKill ||(self.mineScore == self.otherScore && self.isKill)) {
        self.animationImageView.yy_imageURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"zhansha@2x" ofType:@"webp"]];
    }
    if (self.isKill) {
         if (self.mineScore > self.otherScore){
            self.animationImageView.yy_imageURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:kIsMirroredLayout ? @"otherko_me": @"meko_other" ofType:@"webp"]];
        }else if (self.mineScore < self.otherScore) {
            self.animationImageView.yy_imageURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:kIsMirroredLayout ? @"meko_other": @"otherko_me" ofType:@"webp"]];
        }
    }
    if (!self.isShanShuo && self.isKill) {
        [self configKOBorderColor];
        self.topBarImageView.hidden = YES;
        self.timeLabelBgImageView.hidden = YES;
        self.isShanShuo = YES;
    }
    if (self.isShanShuo && (self.mineScore == self.otherScore || !self.isKill)) { // 正在呼吸 平局 或者是 突然变成不是斩杀
        self.topBarImageView.hidden = NO;
        self.timeLabelBgImageView.hidden = NO;
        self.isShanShuo = NO;
        [NSObject cancelPreviousPerformRequestsWithTarget:self];
        self.breathView.layer.borderColor = [UIColor clearColor].CGColor;
    }
    
}

- (void)updateScore:(LivePKScoreMessage *)scoreMsg {
    if ([scoreMsg.opponentLiveId isEqualToString:self.mineLiveId]) {
        self.mineSupportIds = scoreMsg.opponentSupportIds;
        self.otherSupportIds = scoreMsg.mySupportIds;
        self.mineScore = scoreMsg.opponentScore;
        self.otherScore = scoreMsg.myScore;
    }else{
        self.mineSupportIds = scoreMsg.mySupportIds;
        self.otherSupportIds = scoreMsg.opponentSupportIds;
        self.mineScore = scoreMsg.myScore;
        self.otherScore = scoreMsg.opponentScore;
    }
    [self configSupportIds:self.otherSupportIds isOther:true];
    [self configSupportIds:self.mineSupportIds isOther:NO];
    [self.progressView updatePKScore:self.mineScore otherScore:self.otherScore];
}

- (void)configSupportIds:(NSArray *)supportIds isOther:(BOOL) isOther{
    
    NSMutableArray * array = [NSMutableArray new];
    for (NSString * userIds in supportIds) {
        [array addObject:userIds];
    }
    if (supportIds.count == 0 || array.count == 0) {
        return;
    }
    NSDictionary *params = @{@"user_ids": [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding]};
    [RequestUtils commonGetRequestUtils:[params mutableCopy] bURL:@"user.head" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        NSArray *users = suceess[@"users"];
        for (NSDictionary * dic  in users ) {
            [self.userInfo setValue:dic forKey:dic[@"_id"]];
        }
        for (int i =0 ; i<supportIds.count; i++) {
            NSDictionary * dic = self.userInfo[supportIds[i]];
            LY_PortraitView * head = isOther ? self.otherPortraitViewArray[i] : self.minePortraitViewArray[i];
            LY_Portrait * portrait = [[LY_Portrait alloc]init];
            portrait.headwearUrl = dic[@"headwearWebp"];
            portrait.url = dic[@"avatar"];
            head.portrait = portrait;
        }
    } bFailure:^(id  _Nullable failure) {

    }];
}

- (void)configKOBorderColor{
    if (self.round == 4) { return;}
    [UIView animateWithDuration:2.0 animations:^{
        self.breathView.layer.borderColor = self.mineScore < self.otherScore ? SPD_HEXCOlOR(@"#5591FF").CGColor :SPD_HEXCOlOR(@"FF6B93").CGColor;
    } completion:^(BOOL finished) {
        [self performSelector:@selector(configKOBorderColor2) withObject:nil afterDelay:1];
    }];
}

- (void)configKOBorderColor2{
    if (self.round == 4) { return;}
    [UIView animateWithDuration:2.0 animations:^{
        self.breathView.layer.borderColor = [UIColor clearColor].CGColor;
    } completion:^(BOOL finished) {
        [self performSelector:@selector(configKOBorderColor) withObject:nil afterDelay:1];
    }];
}

- (void)updateBehindMsg:(PKCheckBeheadingMsg *)msg {
    self.isKill = msg.isKill;
    self.koProgressView.koValueLabel.text = [NSString stringWithFormat:@"领先%d K.O".localized,msg.beheadingScore];
    
//    if (msg.isKill) {
//        // 斩杀中
//        self.koProgressView.type = (self.mineScore >= self.otherScore) ? @"3":@"4";
//        if (self.mineScore >= self.otherScore) {
//            self.koProgressView.titleLabel.text = [NSString stringWithFormat:@"%@ %d",@"已领先".localized,self.mineScore - self.otherScore];
//            self.koProgressView.desLabel.text = [NSString stringWithFormat:@"%ds后K.O".localized,msg.time];
//            if (self.mineScore != self.otherScore) {
//                self.timeLabel.backgroundColor = SPD_HEXCOlOR(@"FF6B93");
//                self.timeLabel.text = @"保持K.O！".localized;
//            }
//        }else{
//            self.koProgressView.titleLabel.text = [NSString stringWithFormat:@"%@ %d",@"已落后".localized,self.otherScore - self.mineScore];
//            self.koProgressView.desLabel.text = [NSString stringWithFormat:@"%ds后被K.O".localized,msg.time];
//            self.timeLabel.backgroundColor = SPD_HEXCOlOR(@"5591FF");
//            self.timeLabel.text = @"即将被K.O！".localized;
//        }
//        self.timeLabel.textColor = [UIColor whiteColor];
//        self.timeLabel.font = [UIFont systemFontOfSize:12];
//    }else{
//        self.koProgressView.type = self.mineScore >= self.otherScore ? @"1":@"2";
//        self.koProgressView.titleLabel.text = [NSString stringWithFormat:@"%@ %d",((self.mineScore >= self.otherScore) ? @"已领先".localized:@"已落后".localized),abs(self.mineScore - self.otherScore)];
//        self.timeLabel.textColor = SPD_HEXCOlOR(@"08312F");
//        self.timeLabel.font = [UIFont boldSystemFontOfSize:14];
//    }

}

- (void)updateResultMsg:(PKResultMessage *)msg {
    self.isKill = NO;
    int delay = msg.isKill && [msg.winLiveId isEqualToString:self.mineLiveId] ? 4: 0;
    self.animationImageView.yy_imageURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"pk_zhong@2x" ofType:@"webp"]];
    self.topBarImageView.hidden = NO;
    self.timeLabelBgImageView.hidden = NO;
    [self changeSelfFrameHigher:NO];
    self.breathView.layer.borderColor = [UIColor clearColor].CGColor;
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    YYAnimatedImageView * image = [[YYAnimatedImageView alloc]initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH)];
    if (delay) {
        image.yy_imageURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"ko_2" ofType:@"webp"]];
        [[UIApplication sharedApplication].keyWindow addSubview:image];
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (delay == 4) {
           [image removeFromSuperview];
        }
        PKResultView * resultView = [[PKResultView alloc]initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH)];
        [resultView updateResultMsg:msg liveId:self.mineLiveId mineAnchorUrl:self.mineAnchorPortraitUrl?:@""];
        [[UIApplication sharedApplication].keyWindow addSubview:resultView];
    });
    
}

- (LY_PortraitView *)minePortraitView  {
    if (!_minePortraitView) {
        _minePortraitView = [LY_PortraitView new];
        _minePortraitView.borderColor = [[UIColor whiteColor] colorWithAlphaComponent:0.2];
        _minePortraitView.showBorder = YES;
    }
    return _minePortraitView;
}

- (LY_PortraitView *)mFristPortraitView {
    if (!_mFristPortraitView) {
        _mFristPortraitView = [[LY_PortraitView alloc]init];
    }
    return _mFristPortraitView;
}

- (LY_PortraitView *)mSecondPortraitView {
    if (!_mSecondPortraitView) {
        _mSecondPortraitView = [[LY_PortraitView alloc]init];
    }
    return _mSecondPortraitView;
}

- (LY_PortraitView *)mThridPortraitView {
    if (!_mThridPortraitView) {
        _mThridPortraitView = [[LY_PortraitView alloc]init];
    }
    return _mThridPortraitView;
}

- (LY_PortraitView *)otherPortraitView {
    if (!_otherPortraitView) {
        _otherPortraitView = [LY_PortraitView new];
        _otherPortraitView.borderColor = [[UIColor whiteColor] colorWithAlphaComponent:0.2];
        _otherPortraitView.showBorder = YES;
    }
    return _otherPortraitView;
}

- (LY_PortraitView *)oFristPortraitView {
    if (!_oFristPortraitView) {
        _oFristPortraitView = [LY_PortraitView new];
    }
    return _oFristPortraitView;
}

- (LY_PortraitView *)oSecondPortraitView {
    if (!_oSecondPortraitView) {
        _oSecondPortraitView = [LY_PortraitView new];
    }
    return _oSecondPortraitView;
}

- (LY_PortraitView *)oThridPortraitView {
    if (!_oThridPortraitView) {
        _oThridPortraitView = [LY_PortraitView new];
    }
    return _oThridPortraitView;
}


- (UIButton *)ruleButton {
    if (!_ruleButton) {
        _ruleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_ruleButton setImage:[UIImage imageNamed:@"ic_zhibo_pk_guize"] forState:UIControlStateNormal];
        [_ruleButton addTarget:self action:@selector(handleRuleButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_ruleButton hx_setEnlargeEdgeWithTop:10 right:10 bottom:10 left:10];
    }
    return _ruleButton;
}

- (PKProgressView *)progressView {
    if (!_progressView) {
        _progressView = [[PKProgressView alloc]initWithFrame:CGRectMake(15, self.height - 25/2.0f, self.width - 15*2, 25)];
        _progressView.layer.cornerRadius = 25/2.0f;
        _progressView.layer.borderColor = SPD_HEXCOlOR(@"ffffff").CGColor;
        _progressView.layer.borderWidth = 1.0f;
    }
    return _progressView;
}

- (UILabel *)timeLabel{
    if (!_timeLabel) {
        _timeLabel = [UILabel new];
        _timeLabel.textColor = SPD_HEXCOlOR(@"08312F");
        _timeLabel.textAlignment = NSTextAlignmentCenter;
        _timeLabel.font = [UIFont boldSystemFontOfSize:14];
    }
    return _timeLabel;
}

- (UILabel *)mineNameLabel {
    if (!_mineNameLabel) {
        _mineNameLabel = [[UILabel alloc]init];
        _mineNameLabel.font = [UIFont mediumFontOfSize:12];
        _mineNameLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _mineNameLabel;
}

- (UILabel *)otherNameLabel {
    if (!_otherNameLabel) {
        _otherNameLabel = [[UILabel alloc]init];
        _otherNameLabel.font = [UIFont mediumFontOfSize:12];
        _otherNameLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _otherNameLabel;
}

- (void)dealloc
{
    [self.timer invalidate];
    self.timer = nil;
    NSLog(@"PKProcessAnimationView dealloc");
}


@end
