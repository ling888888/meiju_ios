//
//  PKBeginAnimationView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/6/23.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PKBeginAnimationView.h"
#import "YYWebImage.h"
#import "LivePKBeginMessage.h"

@interface PKBeginAnimationView ()

@property (nonatomic, strong) UIImageView * mineBgView;
@property (nonatomic, strong) UIImageView * otherBgView;
@property (nonatomic, strong) LY_PortraitView * minePortraitView;
@property (nonatomic, strong) LY_PortraitView * otherPortraitView;
@property (nonatomic, strong) UILabel * myNameLabel;
@property (nonatomic, strong) UILabel * otherNameLabel;
@property (nonatomic, strong) YYAnimatedImageView * vsImageView;
@property (nonatomic, assign) CGFloat mineWidth;
@property (nonatomic, assign) CGFloat otherWidth;
@property (nonatomic, strong) LivePKBeginMessage * message;

@end

@implementation PKBeginAnimationView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews {
    
    self.mineWidth = 267.0f/375 * kScreenW;
    self.otherWidth = 252.0f/375 * kScreenW;

    self.mineBgView = [UIImageView new];
    self.mineBgView.image = [UIImage mirroredImageNamed:@"img_zhibo_pk_kaishi_hong"];
    [self addSubview:self.mineBgView];
    [self.mineBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(-_mineWidth);
        make.top.bottom.mas_equalTo(0);
        make.width.mas_equalTo(self.mineWidth);
    }];
    
    self.minePortraitView = [LY_PortraitView new];
    [self.mineBgView addSubview:self.minePortraitView];
    [self.minePortraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(24.5);
        make.top.mas_equalTo(55);
        make.size.mas_equalTo(97.5);
    }];
    
    self.myNameLabel = [UILabel new];
    self.myNameLabel.textColor = [UIColor whiteColor];
    self.myNameLabel.font = [UIFont mediumFontOfSize:12];
    self.myNameLabel.text = @"";
    self.myNameLabel.textAlignment = NSTextAlignmentCenter;
    [self.mineBgView addSubview:self.myNameLabel];
    [self.myNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.minePortraitView.mas_bottom).offset(-5);
        make.centerX.mas_equalTo(self.minePortraitView.mas_centerX).offset(0);
        make.width.mas_equalTo(100);
    }];
    
    self.otherBgView = [UIImageView new];
    [self addSubview:self.otherBgView];
    self.otherBgView.image = [UIImage mirroredImageNamed:@"img_zhibo_pk_kaishi_lan"];
    [self.otherBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(kScreenW);
        make.top.bottom.mas_equalTo(0);
        make.width.mas_equalTo(_otherWidth);
    }];
    
    self.otherPortraitView = [LY_PortraitView new];
    [self.otherBgView addSubview:self.otherPortraitView];
    [self.otherPortraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-24.5);
        make.top.mas_equalTo(35);
        make.size.mas_equalTo(97.5);
    }];
    
    self.otherNameLabel = [[UILabel alloc]init];
    self.otherNameLabel.text = @"";
    self.otherNameLabel.textColor = [UIColor whiteColor];
    self.otherNameLabel.font = [UIFont mediumFontOfSize:12];
    [self.otherBgView addSubview:self.otherNameLabel];
    self.otherNameLabel.textAlignment = NSTextAlignmentCenter;
    [self.otherNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.otherPortraitView.mas_bottom).offset(-5);
        make.centerX.mas_equalTo(self.otherPortraitView.mas_centerX).offset(0);
        make.width.mas_equalTo(100);
    }];
    
    self.vsImageView = [[YYAnimatedImageView alloc]init];
    [self addSubview:self.vsImageView];
    self.vsImageView.alpha = 0;
    self.vsImageView.animationRepeatCount = 1;
    self.vsImageView.contentMode = UIViewContentModeCenter;
    [self.vsImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(154, self.frame.size.height));
    }];
}

- (void)configMessage:(LivePKBeginMessage *)beginMsg andMineAnchorUserId:(NSString *)anchorId {
    self.message = beginMsg;
    NSString * launchUserId = beginMsg.launchUserId;
    NSString * acceptUserId = beginMsg.acceptUserId;
    NSString * otherUID = [beginMsg.launchUserId isEqualToString:anchorId] ? acceptUserId :launchUserId;
    NSDictionary *params = @{@"user_ids": [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:@[otherUID,anchorId] options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding]};
    [RequestUtils commonGetRequestUtils:[params mutableCopy] bURL:@"user.head" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        NSArray *users = suceess[@"users"];
        for (NSDictionary * dic  in users ) {
            NSLog(@"[pk]--dic -- %@",dic);
            if ([dic[@"_id"] isEqualToString:anchorId]) {
                self.myNameLabel.text = dic[@"nick_name"];
                [self.minePortraitView.imageView fp_setImageWithURLString:dic[@"avatar"]];
            }else{
                self.otherNameLabel.text = dic[@"nick_name"];
                [self.otherPortraitView.imageView fp_setImageWithURLString:dic[@"avatar"]];
            }
        }
    } bFailure:^(id  _Nullable failure) {

    }];
}

- (void)startAnimation {
    self.alpha = 1;
    [UIView animateWithDuration:0.23 animations:^{
        [self.mineBgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo(0);
        }];
        [self.otherBgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo(kScreenW-(self.otherWidth));
        }];
        [self layoutIfNeeded];
    }completion:^(BOOL finished) {
        // 播放vs
        self.vsImageView.alpha = 1;
        NSString *path = [[NSBundle mainBundle] pathForResource:@"pk_vs" ofType:@"webp"];
        self.vsImageView.yy_imageURL = [NSURL fileURLWithPath:path];
        [self.vsImageView startAnimating];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self stopAnimation];
        });
    }];
}

- (void)stopAnimation {
    [UIView animateWithDuration:0.35 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(pkBeginAnimationViewdidFinishPKBeginAnimation:WithMessage:)]) {
            [self.delegate pkBeginAnimationViewdidFinishPKBeginAnimation:self WithMessage:self.message];
        }
        [self.mineBgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo(-_mineWidth);
        }];
        [self.otherBgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo(kScreenW);
        }];
        [self layoutIfNeeded];
        self.minePortraitView.imageView.image = [UIImage imageNamed:@""];
        self.minePortraitView.headwearImageView.image = [UIImage imageNamed:@""];
        self.otherPortraitView.imageView.image = [UIImage imageNamed:@""];
        self.otherPortraitView.headwearImageView.image = [UIImage imageNamed:@""];
        //        [self.vsImageView removeObserver:self forKeyPath:@"currentIsPlayingAnimation"];
    }];
}

//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
//{
//    if ([keyPath isEqualToString:@"currentIsPlayingAnimation"]) {
//        BOOL old = [change[NSKeyValueChangeOldKey] boolValue];
//        BOOL new = [change[NSKeyValueChangeNewKey] boolValue];
//        if (old == YES && new == NO) {
//            // 代表播放完成
//            NSLog(@"ggggggggggg");
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                [self stopAnimation];
//            });
//        }
//    }else{
//        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
//    }
//}

- (void)dealloc
{
    NSLog(@"PKBeginAnimationView dealloc");
}

@end
