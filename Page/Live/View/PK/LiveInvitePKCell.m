//
//  LiveInvitePKCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/6/22.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveInvitePKCell.h"

@interface LiveInvitePKCell ()

@property (nonatomic, strong) UIImageView * avatar;
@property (nonatomic, strong) UIButton * livingBtn;
@property (nonatomic, strong) UILabel * nameLabel;


@end

@implementation LiveInvitePKCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews{
    
    [self.contentView addSubview:self.avatar];
    [self.avatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(17);
        make.centerY.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(45, 45));
    }];
    self.avatar.contentMode = UIViewContentModeScaleAspectFit;
    self.avatar.layer.cornerRadius = 22.5;
    self.avatar.clipsToBounds = YES;
    
    [self.contentView addSubview:self.livingBtn];
    [self.livingBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.avatar.mas_leading).offset(5);
        make.width.mas_equalTo(35);
        make.height.mas_equalTo(11);
        make.bottom.equalTo(self.avatar.mas_bottom).offset(0);
    }];
   
    
    [self.contentView addSubview:self.inviteButton];
    [self.inviteButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.trailing.mas_equalTo(-15);
        make.height.mas_equalTo(24);
        make.width.mas_greaterThanOrEqualTo(72);
    }];
    
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.leading.equalTo(self.avatar.mas_trailing).offset(14);
        make.width.mas_offset(100);
    }];
    self.inviteButton.userInteractionEnabled = NO;
    self.inviteButton.layer.cornerRadius = 12;
    
}

- (void)setDict:(NSDictionary *)dict {
    _dict = dict;
    [self.avatar fp_setImageWithURLString:_dict[@"avatar"]];
    self.nameLabel.text = _dict[@"userName"];
    
}

- (UIImageView *)avatar {
    if (!_avatar) {
        _avatar = [UIImageView new];
        _avatar.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _avatar;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.textColor = SPD_HEXCOlOR(@"333333");
        _nameLabel.font = [UIFont systemFontOfSize:14];
    }
    return _nameLabel;
}

- (UIButton *)inviteButton {
    if (!_inviteButton) {
        _inviteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_inviteButton setTitle:@"邀请PK".localized forState:UIControlStateNormal];
        [_inviteButton setBackgroundColor:SPD_HEXCOlOR(@"6229E4")];
        [_inviteButton setTitleColor:SPD_HEXCOlOR(@"ffffff") forState:UIControlStateNormal];
//        [_inviteButton setTitleColor:SPD_HEXCOlOR(@"ffffff") forState:UIControlStateSelected];
        _inviteButton.titleLabel.font = [UIFont mediumFontOfSize:14];
    }
    return _inviteButton;
}

- (UIButton *)livingBtn {
    if (!_livingBtn) {
        _livingBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_livingBtn setTitle:@"直播中".localized forState:UIControlStateNormal];
        [_livingBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _livingBtn.titleLabel.font = [UIFont mediumFontOfSize:7];
        _livingBtn.layer.cornerRadius = 5.5;
        _livingBtn.layer.masksToBounds = true;
        UIImage * bgImg = [[UIImage alloc] initWithGradient:@[[UIColor colorWithHexString:@"#22BBB9"], [UIColor colorWithHexString:@"#45E994"]] size:CGSizeMake(35, 11) direction:UIImageGradientColorsDirectionHorizontal];
        [_livingBtn setBackgroundImage:bgImg forState:UIControlStateNormal];
    }
    return _livingBtn;
}


@end
