//
//  PKKOProgressView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/6/30.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PKKOProgressView : UIView

@property (nonatomic, copy) NSString * type; // [1 2 3 4 5] 1 :领先并没有斩杀 2 落后没有斩杀 3 领先斩杀 4 落后斩杀 5很遗憾pk失败

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *desLabel; // 19s后pk

@property (weak, nonatomic) IBOutlet UILabel *koValueLabel; //

@property (weak, nonatomic) IBOutlet UIImageView *flagLabel; //旗子

@end

NS_ASSUME_NONNULL_END
