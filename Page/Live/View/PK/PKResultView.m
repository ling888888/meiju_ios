//
//  PKResultView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/7/1.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PKResultView.h"
#import "KKPaddingLabel.h"
#import "PKResultMessage.h"
#import "YYWebImage.h"
#import "SVGA.h"
@interface PKResultView ()

@property (nonatomic, strong) YYAnimatedImageView * titleAnimationImageView;
@property (nonatomic, strong) YYAnimatedImageView * lightImageView;

@property (nonatomic, strong) UIImageView * avatar; // 我方主播的头像
@property (nonatomic, strong) UILabel * resultLabel;
@property (nonatomic, strong) UIView * bottomView;
@property (nonatomic, strong) UILabel * scoreLabel;
@property (nonatomic, strong) UILabel * winCountLabel;
@property (nonatomic, strong) UIImageView * mvpView; // mvp 背景图片
@property (nonatomic, strong) KKPaddingLabel * mvpNameLabel;
@property (nonatomic, strong) KKPaddingLabel * mvpScoreLabel;
@property (nonatomic, strong) UIImageView * mvpImage; // mvp头像
@property (nonatomic, strong) UILabel * emptyMVPLabel;
@property (nonatomic, strong) UIImageView * bottomBg;
@property (nonatomic, strong ) SVGAImageView * player;

@end

@implementation PKResultView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.bottomView = [UIView new];
        self.bottomView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.bottomView];
        [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_offset(164/667.0f*kScreenH);
            make.centerX.equalTo(self.mas_centerX);
            make.size.mas_equalTo(CGSizeMake(354, 302));
        }];
        
        self.titleAnimationImageView = [YYAnimatedImageView new];
        [self addSubview:self.titleAnimationImageView];
        [self.titleAnimationImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.bottomView.mas_top).offset(-165);
            make.centerX.mas_equalTo(0);
            make.size.mas_equalTo(CGSizeMake(360, 245));
        }];
        
        self.lightImageView = [YYAnimatedImageView new];
        self.lightImageView.contentMode = UIViewContentModeCenter;
        [self insertSubview:self.lightImageView belowSubview:self.titleAnimationImageView];
        [self.lightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(220);
            make.top.equalTo(self.titleAnimationImageView.mas_top).offset(8);
            make.centerX.equalTo(self);
        }];
        
        self.avatar = [UIImageView new];
        self.avatar.layer.cornerRadius = 27.0f;
        self.avatar.backgroundColor = [UIColor orangeColor];
        self.avatar.clipsToBounds = YES;
        [self.titleAnimationImageView addSubview:self.avatar];
        [self.avatar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(96);
            make.centerX.mas_equalTo(0);
            make.size.mas_equalTo(50);
        }];
        
        self.resultLabel = [UILabel new];
        [self.titleAnimationImageView addSubview:self.resultLabel];
        [self.resultLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.mas_equalTo(self.titleAnimationImageView.mas_top).offset(150);
        }];
        self.resultLabel.font = [UIFont systemFontOfSize:23];
                
        self.bottomBg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 354, 302)];
        self.bottomBg.image = [UIImage imageNamed:@"img_zhibo_pk_jiesuan_shengli"];
        [self.bottomView addSubview:self.bottomBg];
        
        self.scoreLabel = [UILabel new];
        self.scoreLabel.textColor = SPD_HEXCOlOR(@"8A4CFF");
        self.scoreLabel.font = [UIFont boldSystemFontOfSize:18];
        [self.bottomView addSubview:self.scoreLabel];
        [self.scoreLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.bottomView.mas_centerX);
            make.top.mas_equalTo(74);
        }];
        
        self.winCountLabel =[UILabel new];
        self.winCountLabel.textColor = SPD_HEXCOlOR(@"#E248FF");
        self.winCountLabel.font = [UIFont boldSystemFontOfSize:20];
        self.winCountLabel.numberOfLines = 2;
        self.winCountLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.winCountLabel];
        [self.winCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.bottomView.mas_centerX);
            make.top.mas_equalTo(self.scoreLabel.mas_bottom).offset(16);
            make.width.mas_equalTo(200);
        }];
        
        self.mvpView = [UIImageView new];
        self.mvpView.image = [UIImage imageNamed:@"MVP"];
        [self.bottomView addSubview:self.mvpView];
        [self.mvpView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.winCountLabel.mas_bottom).offset(12);
            make.size.mas_equalTo(CGSizeMake(249, 124));
            make.centerX.equalTo(self.bottomView.mas_centerX);
        }];
        
        self.mvpImage = [UIImageView new];
        [self.mvpView addSubview:self.mvpImage];
        [self.mvpImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.mvpView.mas_centerY);
            make.leading.mas_equalTo(32);
            make.size.mas_equalTo(45);
        }];
        self.mvpImage.layer.cornerRadius = 45.0f/2;
        self.mvpImage.clipsToBounds = YES;
        
        self.mvpNameLabel = [KKPaddingLabel new];
        self.mvpNameLabel.textColor = [UIColor whiteColor];
        self.mvpNameLabel.font = [UIFont mediumFontOfSize:16];
        [self.mvpView addSubview:self.mvpNameLabel];
        [self.mvpNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo(91);
            make.top.equalTo(self.mvpImage.mas_top).offset(6);
            make.width.mas_equalTo(100);
        }];
        
        self.mvpScoreLabel = [KKPaddingLabel new];
        self.mvpScoreLabel.textColor = [[UIColor whiteColor]colorWithAlphaComponent:0.6];
        self.mvpScoreLabel.font = [UIFont mediumFontOfSize:12];
        [self.mvpView addSubview:self.mvpScoreLabel];
        [self.mvpScoreLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo(91);
            make.top.equalTo(self.mvpNameLabel.mas_bottom).offset(6);
        }];
        
        self.emptyMVPLabel = [UILabel new];
        self.emptyMVPLabel.font = [UIFont mediumFontOfSize:16];
        self.emptyMVPLabel.textColor = [SPD_HEXCOlOR(@"#FFFFFF") colorWithAlphaComponent:0.6];
        self.emptyMVPLabel.text = @"本场没有人送礼物".localized;
        [self.mvpView addSubview:self.emptyMVPLabel];
        [self.emptyMVPLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.mvpView);
        }];
        self.emptyMVPLabel.hidden = YES;
        
        self.player = [[SVGAImageView alloc]init];
        [self addSubview:self.player];
        [self.player mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.size.mas_equalTo(CGSizeMake(kScreenW, kScreenH));
        }];
    }
    return self;
}

- (void)updateResultMsg:(PKResultMessage *)resultMsg liveId:(NSString *)mineLiveId  mineAnchorUrl:(NSString *) anchorAvatarUrl {
    self.resultMsg = resultMsg;
    [self.avatar fp_setImageWithURLString:anchorAvatarUrl];
    BOOL isMeWin = NO;
    if (!self.resultMsg.isBalance && [resultMsg.winLiveId isEqualToString:mineLiveId]) {
        isMeWin = YES;
    }
    if (isMeWin) {
        self.resultLabel.textColor = SPD_HEXCOlOR(@"#F9E24D");
        self.resultLabel.text = @"WIN";
        self.winCountLabel.textColor = SPD_HEXCOlOR(@"E248FF");
    }else{
        self.resultLabel.textColor = SPD_HEXCOlOR(@"7D8AA1");
        self.resultLabel.text = !resultMsg.isBalance ? @"FAIL":@"DRAWN";
        self.winCountLabel.textColor = SPD_HEXCOlOR(@"2E3676");
        [self.avatar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(100);
            make.centerX.mas_equalTo(0);
            make.size.mas_equalTo(46);
        }];
        self.avatar.layer.cornerRadius = 23;
        self.avatar.clipsToBounds = YES;
    }
    self.titleAnimationImageView.yy_imageURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:isMeWin ?@"win2": @"fail" ofType:@"webp"]];
    self.lightImageView.yy_imageURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:isMeWin ?@"pk_win_light":@"pk_fail_light" ofType:@"webp"]];
    self.bottomBg.image = [UIImage imageNamed:isMeWin?@"img_zhibo_pk_jiesuan_shengli":@"img_pk_fail"];
    BOOL isAccept = [mineLiveId isEqualToString:resultMsg.acceptId];
    NSString * mvpUIds = isAccept ? resultMsg.acceptMvpUserId :resultMsg.launchMvpUserId;
    self.mvpImage.hidden = !mvpUIds.notEmpty;
    self.mvpNameLabel.hidden = !mvpUIds.notEmpty;
    self.mvpScoreLabel.hidden = !mvpUIds.notEmpty;
    self.emptyMVPLabel.hidden = mvpUIds.notEmpty;
    
    [self.mvpImage fp_setImageWithURLString: isAccept ? resultMsg.acceptMvpUserAvatar:resultMsg.launchMvpUserAvatar];
    self.scoreLabel.text = [NSString stringWithFormat:@"主播本局PK积分 %@".localized,isAccept?@(resultMsg.acceptScore):@(resultMsg.launchScore)];
    if (resultMsg.type == 0) {
        self.winCountLabel.text = [NSString stringWithFormat:@"连胜:%@场".localized,isMeWin ? resultMsg.continuityWinTimes:@"0"];
    }else{
        // 邀请不会获得积分
        self.winCountLabel.text = @"此模式不会获得PK积分".localized;
        self.winCountLabel.textColor = SPD_HEXCOlOR(@"#2E3676");
        self.winCountLabel.font = [UIFont boldSystemFontOfSize:15];
    }
    self.mvpNameLabel.text = isAccept ? resultMsg.acceptMvpUserName:resultMsg.launchMvpUserName;
    self.mvpScoreLabel.text =[NSString stringWithFormat:@"贡献:%@".localized.localized,isAccept ? @(resultMsg.acceptMvpScore):@(resultMsg.launchMvpScore)];
    if (isMeWin) {
        self.player.imageName = @"caidai";
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.25 animations:^{
            self.alpha = 0;
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    });
}
@end
