//
//  PKReceiveInviteMessageView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/7/3.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LYPCustomAlertView.h"
NS_ASSUME_NONNULL_BEGIN

@class LivePKInviteMessage,PKReceiveInviteMessageView;

@protocol PKReceiveInviteMessageViewDelegate <NSObject>

@optional
- (void)receivePKInviteMessageViewDidClickedAgreeBtn:(PKReceiveInviteMessageView *) view;

@end

@interface PKReceiveInviteMessageView : LYPCustomAlertView

@property (nonatomic, strong) LivePKInviteMessage * inviteMsg;

@property (nonatomic, weak) id<PKReceiveInviteMessageViewDelegate> delegate;

@end


NS_ASSUME_NONNULL_END
