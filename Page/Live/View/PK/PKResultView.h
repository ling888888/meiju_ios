//
//  PKResultView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/7/1.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class PKResultMessage;

@interface PKResultView : UIView

@property (nonatomic, strong)PKResultMessage * resultMsg;

- (void)updateResultMsg:(PKResultMessage *)resultMsg liveId:(NSString *)mineLiveId  mineAnchorUrl:(NSString *) anchorAvatarUrl;

@end

NS_ASSUME_NONNULL_END
