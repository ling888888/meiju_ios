//
//  PKKOProgressView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/6/30.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PKKOProgressView.h"

@interface PKKOProgressView ()

@property (nonatomic, strong) CAGradientLayer *gradientLayer;

@end

@implementation PKKOProgressView

- (CAGradientLayer *)gradientLayer {
    if (!_gradientLayer) {
        _gradientLayer = [[CAGradientLayer alloc]init];
        _gradientLayer.startPoint = CGPointMake(0, 0); //(0, 0)
        _gradientLayer.endPoint = CGPointMake(1.0, 0.0);
    }
    return _gradientLayer;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.gradientLayer.frame = CGRectMake(0, 0, self.titleLabel.frame.size.width, self.titleLabel.frame.size.height);

}

- (void)setType:(NSString *)type {
    _type = type;
    self.gradientLayer.hidden = !([_type isEqualToString:@"1"] || [_type isEqualToString:@"2"]);
    self.desLabel.hidden = !([_type isEqualToString:@"3"] || [_type isEqualToString:@"4"] || [_type isEqualToString:@"5"]);
    self.flagLabel.hidden = ([_type isEqualToString:@"5"]);
    self.koValueLabel.hidden = ([_type isEqualToString:@"5"]);
    if ([_type isEqualToString:@"1"]) {
        self.titleLabel.textColor = [UIColor whiteColor];
        self.desLabel.textColor = SPD_HEXCOlOR(@"#00D4A0");
        self.titleLabel.backgroundColor = [UIColor clearColor];
        [self.titleLabel.layer addSublayer:self.gradientLayer];
        UIColor * endColor =  [[UIColor colorWithHexString:@"FF6B93"] colorWithAlphaComponent:0];
        UIColor * beginColor =  [[UIColor colorWithHexString:@"FF6B93"] colorWithAlphaComponent:0.51];
        self.gradientLayer.colors = @[(__bridge id)beginColor.CGColor, (__bridge id)endColor.CGColor];
    }else if ([_type isEqualToString:@"2"]){
        self.titleLabel.textColor = [UIColor whiteColor];
        self.desLabel.textColor = SPD_HEXCOlOR(@"#00D4A0");
        self.titleLabel.backgroundColor = [UIColor clearColor];
        UIColor * endColor =  [[UIColor colorWithHexString:@"5591FF"] colorWithAlphaComponent:0];
        UIColor * beginColor =  [[UIColor colorWithHexString:@"5591FF"] colorWithAlphaComponent:0.51];
        self.gradientLayer.colors = @[(__bridge id)beginColor.CGColor, (__bridge id)endColor.CGColor];
    }else if ([_type isEqualToString:@"3"]){
        self.titleLabel.textColor =SPD_HEXCOlOR(@"#321672");
        self.titleLabel.backgroundColor = SPD_HEXCOlOR(@"#FF6B93");
        self.desLabel.textColor = SPD_HEXCOlOR(@"FF6B93");
    }else if ([_type isEqualToString:@"4"]){
        self.titleLabel.textColor = [UIColor whiteColor];
        self.desLabel.textColor = SPD_HEXCOlOR(@"#5591FF");
        self.titleLabel.backgroundColor = SPD_HEXCOlOR(@"#5591FF");
    }else if ([_type isEqualToString:@"5"]){
        self.titleLabel.hidden = YES;
        self.titleLabel.text = @"0";
        self.desLabel.text = @"很遗憾，K.O失败".localized;
        self.desLabel.textColor = SPD_HEXCOlOR(@"#00D4A0");
    }
}

@end
