//
//  AnchorApplyConditionTitleView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/5/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "AnchorApplyConditionTitleView.h"

@interface AnchorApplyConditionTitleView ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation AnchorApplyConditionTitleView


- (void)setTitle:(NSString *)title {
    _title = title;
    self.titleLabel.text = _title;
}


@end
