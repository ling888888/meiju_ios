//
//  LiveRoomBackgroundMusicViewCell.m
//  SimpleDate
//
//  Created by 李楠 on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomBackgroundMusicViewCell.h"
#import "LiveMusicModel.h"

@interface LiveRoomBackgroundMusicViewCell ()

@property (weak, nonatomic) IBOutlet UIView *playingBackgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *singerLabel;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIView *separatorView;

@end

@implementation LiveRoomBackgroundMusicViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (IBAction)clickPlayButton:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickPlayButtonWithModel:playingStyle:playing:)]) {
        [self.delegate didClickPlayButtonWithModel:self.model playingStyle:self.playingStyle playing:self.playing];
    }
}

- (void)setModel:(LiveMusicModel *)model {
    _model = model;
    
    [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL, _model.cover]] placeholderImage:[UIImage imageNamed:@"live_room_music_ph"]];
    self.nameLabel.text = _model.name;
    self.singerLabel.text = _model.singer;
}

- (void)setPlayingStyle:(BOOL)playingStyle {
    _playingStyle = playingStyle;
    
    self.playingBackgroundView.hidden = !_playingStyle;
    self.separatorView.hidden = _playingStyle;
}

- (void)setPlaying:(BOOL)playing {
    _playing = playing;
    
    self.playButton.backgroundColor = [UIColor colorWithHexString:_playing ? @"#F8145B" : @"#6A2CF7"];
    [self.playButton setTitle:SPDLocalizedString(_playing ? @"暂停" : @"播放") forState:UIControlStateNormal];
}

@end
