//
//  LiveRoomOpenMicView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/3/30.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomOpenMicView.h"
#import "ZegoKitManager.h"

@interface LiveRoomOpenMicView ()

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) NSInteger time;

@end

@implementation LiveRoomOpenMicView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [ZegoManager.api enableMic:NO];
    
    self.timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(timing) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
    [self.timer fire];
}

- (void)removeFromSuperview {
    [super removeFromSuperview];
    
    [self.timer invalidate];
    self.timer = nil;
}

- (void)timing {
    self.timeLabel.text = [NSString stringWithFormat:@"%02ld:%02ld:%02ld", self.time / 3600, self.time / 60 % 60, self.time % 60];
    self.time++;
}

- (IBAction)clickOpenButton:(UIButton *)sender {    
    [ZegoManager.api enableMic:YES];
    [self removeFromSuperview];
}

@end
