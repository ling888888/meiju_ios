//
//  LiveRoomAudioEffectView.h
//  SimpleDate
//
//  Created by 李楠 on 2020/4/8.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol LiveRoomAudioEffectViewDelegate <NSObject>

@optional

- (void)audioEffectViewDidHide;

@end

@interface LiveRoomAudioEffectView : UIView

@property (nonatomic, assign) id<LiveRoomAudioEffectViewDelegate> delegate;

- (void)show;

@end

NS_ASSUME_NONNULL_END
