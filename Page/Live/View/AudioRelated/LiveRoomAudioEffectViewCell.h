//
//  LiveRoomAudioEffectViewCell.h
//  SimpleDate
//
//  Created by 李楠 on 2020/4/8.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveRoomAudioEffectViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;

- (void)showPlaybackProcessWithDuration:(CFTimeInterval)duration;

@end

NS_ASSUME_NONNULL_END
