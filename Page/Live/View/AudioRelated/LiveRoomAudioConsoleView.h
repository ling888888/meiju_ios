//
//  LiveRoomAudioConsoleView.h
//  SimpleDate
//
//  Created by 李楠 on 2020/4/9.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveRoomAudioConsoleView : UIView

- (void)show;

@end

NS_ASSUME_NONNULL_END
