//
//  LiveRoomAudioEffectView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/4/8.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomAudioEffectView.h"
#import "LiveRoomAudioEffectViewCell.h"
#import "ZegoKitManager.h"

@interface LiveRoomAudioEffectView ()<UICollectionViewDataSource, UICollectionViewDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, assign) CGFloat contentHeight;
@property (nonatomic, assign) CGFloat bottomHeight;
@property (nonatomic, strong) NSArray<NSString *> *effectArray;
@property (nonatomic, strong) NSArray<NSNumber *> *durationArray;

@end

@implementation LiveRoomAudioEffectView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.contentHeight = 69;
    self.bottomHeight = 100;
    self.contentViewBottom.constant = -self.contentHeight - self.bottomHeight;
    
    self.effectArray = @[@"live_room_audio_effect_laugh",
                         @"live_room_audio_effect_applaud",
                         @"live_room_audio_effect_funny",
                         @"live_room_audio_effect_oops",
                         @"live_room_audio_effect_awkward",
                         @"live_room_audio_effect_victory",
                         @"live_room_audio_effect_fail",
                         @"live_room_audio_effect_boo"];
    self.durationArray = @[@(2.06), @(4.18), @(1.25), @(0.81), @(1.33), @(4.75), @(5.12), @(2.06)];
    [self.collectionView registerNib:[UINib nibWithNibName:@"LiveRoomAudioEffectViewCell" bundle:nil] forCellWithReuseIdentifier:@"LiveRoomAudioEffectViewCell"];
    [self.collectionView reloadData];
    if (kIsMirroredLayout) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
        });
    }
    
    [self layoutIfNeeded];
}

#pragma mark - Public methods

- (void)show {
    [UIView animateWithDuration:0.25 animations:^{
        self.contentViewBottom.constant = -self.bottomHeight + IphoneX_Bottom;
        [self layoutIfNeeded];
    }];
}

#pragma mark - Event responses

- (IBAction)tapBackground:(UITapGestureRecognizer *)sender {
    [self hide];
}

- (IBAction)clickCloseButton:(UIButton *)sender {
    [self hide];
}

#pragma mark - Private methods

- (void)hide {
    [UIView animateWithDuration:0.25 animations:^{
        self.contentViewBottom.constant = -self.contentHeight - self.bottomHeight;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        
        if ([self.delegate respondsToSelector:@selector(audioEffectViewDidHide)]) {
            [self.delegate audioEffectViewDidHide];
        }
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.effectArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LiveRoomAudioEffectViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveRoomAudioEffectViewCell" forIndexPath:indexPath];
    cell.imageView.image = [UIImage imageNamed:self.effectArray[indexPath.row]];
    cell.textLabel.localizedKey = self.effectArray[indexPath.row];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [ZegoManager playAudioEffect:[[NSBundle mainBundle] pathForResource:self.effectArray[indexPath.row] ofType:@"mp3"]];
    LiveRoomAudioEffectViewCell *cell = (LiveRoomAudioEffectViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell showPlaybackProcessWithDuration:self.durationArray[indexPath.row].doubleValue];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ![touch.view isDescendantOfView:self.contentView];
}

@end
