//
//  LiveRoomAudioEffectViewCell.m
//  SimpleDate
//
//  Created by 李楠 on 2020/4/8.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomAudioEffectViewCell.h"

@interface LiveRoomAudioEffectViewCell ()

@property (nonatomic, strong) CAShapeLayer *shapeLayer;

@end

@implementation LiveRoomAudioEffectViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

#pragma mark - Public methods

- (void)showPlaybackProcessWithDuration:(CFTimeInterval)duration {
    CABasicAnimation *shapeLayerAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    shapeLayerAnimation.duration = duration;
    shapeLayerAnimation.toValue = @(1);
    [self.shapeLayer addAnimation:shapeLayerAnimation forKey:@"strokeEnd"];
}

#pragma mark - Setters & Getters

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    if (!selected) {
        [self.shapeLayer removeAnimationForKey:@"strokeEnd"];
    }
}

- (CAShapeLayer *)shapeLayer {
    if (!_shapeLayer) {
        _shapeLayer = [CAShapeLayer new];
        _shapeLayer.strokeColor = [UIColor whiteColor].CGColor;
        _shapeLayer.fillColor = [UIColor clearColor].CGColor;
        _shapeLayer.lineWidth = 2.5;
        _shapeLayer.lineCap = kCALineCapRound;
        _shapeLayer.strokeEnd = 0;
        _shapeLayer.path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(self.imageView.bounds.size.width / 2, self.imageView.bounds.size.height / 2) radius:(self.imageView.bounds.size.width - 2.5) / 2 startAngle:-M_PI_2 endAngle:M_PI + M_PI_2 clockwise:YES].CGPath;
        [self.imageView.layer addSublayer:_shapeLayer];
    }
    return _shapeLayer;
}

@end
