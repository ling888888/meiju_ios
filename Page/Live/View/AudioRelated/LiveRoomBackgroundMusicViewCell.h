//
//  LiveRoomBackgroundMusicViewCell.h
//  SimpleDate
//
//  Created by 李楠 on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LiveMusicModel;

@protocol LiveRoomBackgroundMusicViewCellDelegate <NSObject>

@optional

- (void)didClickPlayButtonWithModel:(LiveMusicModel *)model playingStyle:(BOOL)playingStyle playing:(BOOL)playing;

@end

@interface LiveRoomBackgroundMusicViewCell : UICollectionViewCell

@property (nonatomic, strong) LiveMusicModel *model;
@property (nonatomic, assign) BOOL playingStyle;
@property (nonatomic, assign) BOOL playing;
@property (nonatomic, weak) id<LiveRoomBackgroundMusicViewCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
