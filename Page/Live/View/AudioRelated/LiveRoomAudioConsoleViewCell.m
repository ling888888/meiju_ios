//
//  LiveRoomAudioConsoleViewCell.m
//  SimpleDate
//
//  Created by 李楠 on 2020/4/9.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomAudioConsoleViewCell.h"

@implementation LiveRoomAudioConsoleViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    if (selected) {
        self.imageView.layer.borderWidth = 2;
        self.textLabel.textColor = [UIColor colorWithHexString:@"#6A2CF7"];
    } else {
        self.imageView.layer.borderWidth = 0;
        self.textLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
    }
}

@end
