//
//  LiveRoomEnteringView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/5/20.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveRoomEnteringView.h"
#import "UIImage+GIF.h"

@interface LiveRoomEnteringView ()

@property (weak, nonatomic) IBOutlet UIView *enteringView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *failedView;

@end

@implementation LiveRoomEnteringView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"live_room_entering" ofType:@"gif"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    self.imageView.image = [UIImage sd_animatedGIFWithData:data];
}

- (void)setShowFailed:(BOOL)showFailed {
    _showFailed = showFailed;
    
    self.enteringView.hidden = _showFailed;
    self.failedView.hidden = !_showFailed;
}

@end
