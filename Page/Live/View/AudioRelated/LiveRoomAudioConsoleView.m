//
//  LiveRoomAudioConsoleView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/4/9.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomAudioConsoleView.h"
#import "LiveRoomAudioConsoleViewCell.h"
#import "ZegoKitManager.h"

@interface LiveRoomAudioConsoleView ()<UICollectionViewDataSource, UICollectionViewDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, assign) CGFloat contentHeight;
@property (nonatomic, assign) CGFloat bottomHeight;
@property (nonatomic, strong) NSArray *effectArray;
@property (nonatomic, assign) NSInteger selectedIndex;

@end

@implementation LiveRoomAudioConsoleView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.contentHeight = 240;
    self.bottomHeight = 100;
    self.contentViewBottom.constant = -self.contentHeight - self.bottomHeight;

    self.effectArray = @[@"live_room_audio_console_default",
                         @"live_room_audio_console_hall",
                         @"live_room_audio_console_auditorium",
                         @"live_room_audio_console_room",
                         @"live_room_audio_console_club"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"LiveRoomAudioConsoleViewCell" bundle:nil] forCellWithReuseIdentifier:@"LiveRoomAudioConsoleViewCell"];
    [self.collectionView reloadData];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.collectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
    });
    
    [self layoutIfNeeded];
}

#pragma mark - Public methods

- (void)show {
    self.hidden = NO;
    [UIView animateWithDuration:0.25 animations:^{
        self.contentViewBottom.constant = -self.bottomHeight + IphoneX_Bottom;
        [self layoutIfNeeded];
    }];
}

#pragma mark - Event responses

- (IBAction)tapBackground:(UITapGestureRecognizer *)sender {
    [self hide];
}

- (IBAction)clickCloseButton:(UIButton *)sender {
    [self hide];
}

- (IBAction)clickSwitch:(UISwitch *)sender {
    [ZegoManager enableLoopback:sender.on];
    [self showTips:SPDLocalizedString(sender.on ? @"耳返已开启" : @"耳返已关闭")];
}

#pragma mark - Private methods

- (void)hide {
    [UIView animateWithDuration:0.25 animations:^{
        self.contentViewBottom.constant = -self.contentHeight - self.bottomHeight;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.effectArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LiveRoomAudioConsoleViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveRoomAudioConsoleViewCell" forIndexPath:indexPath];
    cell.imageView.image = [UIImage imageNamed:self.effectArray[indexPath.row]];
    cell.textLabel.localizedKey = self.effectArray[indexPath.row];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
            [ZegoAudioProcessing enableReverb:NO mode:0];
            break;
        case 1:
            [ZegoAudioProcessing setReverbParam:(ZegoAudioReverbParam){0.3, 0.05, 0.3, 1.0}];
            break;
        case 2:
            [ZegoAudioProcessing enableReverb:YES mode:ZEGOAPI_AUDIO_REVERB_MODE_LARGE_AUDITORIUM];
            break;
        case 3:
            [ZegoAudioProcessing enableReverb:YES mode:ZEGOAPI_AUDIO_REVERB_MODE_SOFT_ROOM];
            break;
        case 4:
            [ZegoAudioProcessing enableReverb:YES mode:ZEGOAPI_AUDIO_REVERB_MODE_WARM_CLUB];
            break;
        default:
            break;
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ![touch.view isDescendantOfView:self.contentView];
}

@end
