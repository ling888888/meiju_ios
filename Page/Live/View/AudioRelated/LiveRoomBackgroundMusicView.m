//
//  LiveRoomBackgroundMusicView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomBackgroundMusicView.h"
#import "LiveRoomBackgroundMusicViewCell.h"
#import "LiveMusicModel.h"
#import "ZegoKitManager.h"

@interface LiveRoomBackgroundMusicView ()<UICollectionViewDataSource, UICollectionViewDelegate, UIGestureRecognizerDelegate, LiveRoomBackgroundMusicViewCellDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewTop;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;

@property (nonatomic, assign) CGFloat contentHeight;
@property (nonatomic, assign) CGFloat bottomHeight;
@property (nonatomic, strong) LiveRoomBackgroundMusicViewCell *playingCell;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) LiveMusicModel *playingModel;

@end

@implementation LiveRoomBackgroundMusicView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.contentHeight = 433;
    self.bottomHeight = 100;
    self.contentViewBottom.constant = -self.contentHeight - self.bottomHeight;
    
    self.page = 1;
    [self.collectionView registerNib:[UINib nibWithNibName:@"LiveRoomBackgroundMusicViewCell" bundle:nil] forCellWithReuseIdentifier:@"LiveRoomBackgroundMusicViewCell"];
    __weak typeof(self) weakSelf = self;
    self.collectionView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
        [weakSelf requestLiveMusicList];
    }];
    self.collectionView.mj_footer.hidden = YES;
    self.flowLayout.itemSize = CGSizeMake(kScreenW, 70.5);
    
    [self layoutIfNeeded];
}

#pragma mark - Public methods

- (void)show {
    if (!self.dataArray.count) {
        [self requestLiveMusicList];
    }
    
    self.hidden = NO;
    [UIView animateWithDuration:0.25 animations:^{
        self.contentViewBottom.constant = -self.bottomHeight;
        [self layoutIfNeeded];
    }];
}

#pragma mark - Event responses

- (IBAction)tapBackground:(UITapGestureRecognizer *)sender {
    [self hide];
}

#pragma mark - Private methods

- (void)requestLiveMusicList {
    NSDictionary *params = @{@"page": @(self.page)};
    [RequestUtils GET:URL_NewServer(@"live/live.music.list") parameters:params success:^(id  _Nullable suceess) {
        NSArray *list = suceess[@"list"];
        for (NSDictionary *dic in list) {
            LiveMusicModel *model = [LiveMusicModel initWithDictionary:dic];
            [self.dataArray addObject:model];
        }
        [self.collectionView reloadData];
        self.page++;
        
        self.collectionView.mj_footer.hidden = !list.count;
        [self.collectionView.mj_footer endRefreshing];
    } failure:^(id  _Nullable failure) {
        [self.collectionView.mj_footer endRefreshing];
    }];
}

- (void)hide {
    [UIView animateWithDuration:0.25 animations:^{
        self.contentViewBottom.constant = -self.contentHeight - self.bottomHeight;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LiveRoomBackgroundMusicViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveRoomBackgroundMusicViewCell" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    cell.playingStyle = NO;
    cell.playing = NO;
    cell.delegate = self;
    return cell;
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ![touch.view isDescendantOfView:self.contentView];
}

#pragma mark - LiveRoomBackgroundMusicViewCellDelegate

- (void)didClickPlayButtonWithModel:(LiveMusicModel *)model playingStyle:(BOOL)playingStyle playing:(BOOL)playing {
    if (playingStyle) {
        if (playing) {
            [ZegoManager pauseBackgroundMusic];
        } else {
            [ZegoManager resumeBackgroundMusic];
        }
        self.playingCell.playing = !playing;
    } else {
        if (self.playingModel) {
            [ZegoManager stopBackgroundMusic];
            [self.dataArray addObject:self.playingModel];
        }
        [ZegoManager playBackgroundMusic:model.musicUrl];
        self.playingModel = model;
        [self.dataArray removeObject:model];
        [self.collectionView reloadData];
    }
}

#pragma mark - Setters & Getters

- (void)setPlayingModel:(LiveMusicModel *)playingModel {
    _playingModel = playingModel;
    
    if (_playingModel) {
        self.playingCell.model = _playingModel;
        self.playingCell.playing = YES;
        self.collectionViewTop.constant = 149.5;
    } else {
        self.collectionViewTop.constant = 50;
    }
}

- (LiveRoomBackgroundMusicViewCell *)playingCell {
    if (!_playingCell) {
        _playingCell = [[NSBundle mainBundle] loadNibNamed:@"LiveRoomBackgroundMusicViewCell" owner:self options:nil].firstObject;
        _playingCell.frame = CGRectMake(0, 68.5, kScreenW, 70.5);
        _playingCell.playingStyle = YES;
        _playingCell.delegate = self;
        [self.contentView addSubview:_playingCell];
    }
    return _playingCell;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

@end
