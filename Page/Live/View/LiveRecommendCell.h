//
//  LiveRecommendCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LiveModel;

@interface LiveRecommendCell : UICollectionViewCell

@property (nonatomic, strong) LiveModel * model;
@property (weak, nonatomic) IBOutlet UIImageView *liveFlagImageView;

@end

NS_ASSUME_NONNULL_END
