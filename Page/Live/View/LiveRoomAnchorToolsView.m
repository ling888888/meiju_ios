//
//  LiveRoomAnchorToolView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/26.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomAnchorToolsView.h"
#import "SPDPresentFlowLayout.h"
#import "ChatroomMoreFunctionCell.h"
#import "LiveSendRedPackView.h"
#import "ZegoKitManager.h"
#import "LiveModel.h"

@interface LiveRoomAnchorToolsView ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView * collectionView;

@property (nonatomic, strong) NSMutableArray * dataArray;

@end


@implementation LiveRoomAnchorToolsView

- (instancetype)initWithStyle:(LY_AlertViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews {
    
    _dataArray = [NSMutableArray arrayWithArray:@[@{@"image": @"ic_zhibo_zhubo_fasongtupian",
                                                    @"type": @(LiveRoomAnchorToolsViewItemType_SendImage),
                                                    @"title": @"发送图片"},
                                                  @{@"image": @"ic_zhibo_zhubo_guanbimaike",
                                                    @"type": @(LiveRoomAnchorToolsViewItemType_CloseMic),
                                                    @"title": @"关闭麦克"},
                                                  @{@"image": @"ic_zhibo_zhubo_guanliyuan",
                                                    @"type": @(LiveRoomAnchorToolsViewItemType_LiveManager),
                                                    @"title": @"管理员"},
                                                  @{@"image": @"ic_zhibo_zhubo_tiaoyintai",
                                                    @"type": @(LiveRoomAnchorToolsViewItemType_AudioConsole),
                                                    @"title": @"调音台"},
                                                  @{@"image": @"ic_zhibo_zhubo_fenxiang",
                                                    @"type": @(LiveRoomAnchorToolsViewItemType_Share),@"title":@"分享"}
                                                    ,@{@"image":@"ic_zhibo_zhubo_kaiqilianmai",@"type":@(LiveRoomAnchorToolsViewItemType_LinkMicSettings),
                                                           @"title":@"连麦设置"
                                                    },@{@"image":@"ic_zhibojian_zhubo_hongbao",
                                                        @"type": @(LiveRoomAnchorToolsViewItemType_RedPack),
                                                        @"title":@"红包"
                                                    },@{},@{},@{}]];
    
    SPDPresentFlowLayout * layout = [[SPDPresentFlowLayout alloc]init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.rowCount = 2;
    layout.cellCountPerRow = 5;
    layout.minimumLineSpacing = 5;
    layout.minimumInteritemSpacing = 5;
    layout.itemSize = CGSizeMake((kScreenW - 6* 5)/5.0f, 100);
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.showsHorizontalScrollIndicator = NO;
    [self.collectionView registerNib:[UINib nibWithNibName:@"ChatroomMoreFunctionCell" bundle:nil] forCellWithReuseIdentifier:@"ChatroomMoreFunctionCell"];
    [self.contentView addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(8);
        make.leading.mas_equalTo(0);
        make.trailing.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
}

- (CGFloat)containerHeight {
    return 2 * 100 + 5  +8;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ChatroomMoreFunctionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatroomMoreFunctionCell" forIndexPath:indexPath];
    NSDictionary * dict = self.dataArray[indexPath.row];
    if (dict.count > 0) {
        cell.titleImageView.hidden = NO;
        cell.titleLabel.hidden = NO;
        cell.titleImageView.image = [UIImage imageNamed:dict[@"image"]];
        cell.titleLabel.text = SPDStringWithKey(dict[@"title"], nil);
    }else{
        cell.titleImageView.hidden = YES;
        cell.titleLabel.hidden = YES;
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self dismiss];
    NSDictionary * dict = self.dataArray[indexPath.row];
    NSInteger itemType = [dict[@"type"] integerValue];
    if (itemType == LiveRoomAnchorToolsViewItemType_RedPack) {
        [RequestUtils GET:URL_NewServer(@"live/live.red.package.config") parameters:@{} success:^(id  _Nullable suceess) {
            if ([suceess[@"levelLimit"] integerValue] <= [ZegoManager.liveInfo.wealthLevel integerValue]) {
                LiveSendRedPackView * view = [LiveSendRedPackView new];
                [view show];
            }else{
                [self showTips:[NSString stringWithFormat:@"财富等级达到%@级后可以发送红包".localized,suceess[@"levelLimit"]]];
            }
        } failure:^(id  _Nullable failure) {
            
        }];
    }else{
        if (dict.count > 0 &&self.delegate && [self.delegate respondsToSelector:@selector(liveRoomAnchorToolsView:didClickedItemWithType:)]) {
            [self.delegate liveRoomAnchorToolsView:self didClickedItemWithType:itemType];
        }
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 5, 0, 5);
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

@end
