//
//  LiveEndedAnchorView.h
//  SimpleDate
//
//  Created by 李楠 on 2020/3/27.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol LiveEndedAnchorViewDelegate <NSObject>

@optional

- (void)didClickBackButtonInEndedAnchorView;
- (void)didClickWalletButtonInEndedAnchorView;
- (void)didClickRecordButtonInEndedAnchorView;

@end

@interface LiveEndedAnchorView : UIView

@property (nonatomic, weak) id<LiveEndedAnchorViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
