//
//  LiveEndedAnchorView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/3/27.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveEndedAnchorView.h"
#import "LiveEndedAnchorViewCell.h"
#import "LY_LiveFiringMoreActionSheetView.h"

@interface LiveEndedAnchorView ()<UICollectionViewDataSource, LY_LiveFiringMoreActionSheetViewDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;

@property (nonatomic, copy) NSString *giftNumber;
@property (nonatomic, copy) NSString *listenerNumber;
@property (nonatomic, copy) NSString *followNumber;

@end

@implementation LiveEndedAnchorView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self requestLiveClose];
    
    self.topConstraint.constant = StatusBarHeight + 2.5;
    [self.avatarImageView fp_setImageWithURLString:[SPDApiUser currentUser].avatar];
    [self.collectionView registerNib:[UINib nibWithNibName:@"LiveEndedAnchorViewCell" bundle:nil] forCellWithReuseIdentifier:@"LiveEndedAnchorViewCell"];
    self.flowLayout.itemSize = CGSizeMake((kScreenW - 9 * 2) / 3, 45);
    self.bottomConstraint.constant = 65 + IphoneX_Bottom;
    
    CAGradientLayer *radientLayer = [CAGradientLayer layer];
    radientLayer.frame = CGRectMake(0, 0, kScreenW, kScreenH);
    radientLayer.startPoint = CGPointMake(0, 0);
    radientLayer.endPoint = CGPointMake(1, 1);
    radientLayer.colors = @[(__bridge id)[UIColor colorWithRed:18 / 255.0 green:7 / 255.0 blue:44 / 255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:47 / 255.0 green:19 / 255.0 blue:59 / 255.0 alpha:1.0].CGColor];
    [self.layer insertSublayer:radientLayer atIndex:0];
}

#pragma mark - Private methods

- (void)requestLiveClose {
    [RequestUtils POST:URL_NewServer(@"live/live.close") parameters:@{} success:^(id  _Nullable suceess) {
        self.durationLabel.text = [NSString stringWithFormat:@"%@ %@", SPDLocalizedString(@"直播时长"), suceess[@"duration"]];
        self.giftNumber = suceess[@"giftValue"];
        self.listenerNumber = suceess[@"listenNum"];
        self.followNumber = [NSString stringWithFormat:@"+%@", suceess[@"newFollow"]];
        [self.collectionView reloadData];
    } failure:^(id  _Nullable failure) {

    }];
}

#pragma mark - Event responses

- (IBAction)clickMoreButton:(UIButton *)sender {
    LY_LiveFiringMoreActionSheetView *moreActionSheetView = [LY_LiveFiringMoreActionSheetView new];
    moreActionSheetView.presentOnLiveEndedView = YES;
    moreActionSheetView.delegate = self;
    [moreActionSheetView presentOnView:self];
}

- (IBAction)clickBackButton:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickBackButtonInEndedAnchorView)]) {
        [self.delegate didClickBackButtonInEndedAnchorView];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 3;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LiveEndedAnchorViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveEndedAnchorViewCell" forIndexPath:indexPath];
    switch (indexPath.row) {
        case 0:
            cell.numberLabel.text = self.giftNumber;
            cell.textLabel.text = SPDLocalizedString(@"收到礼物");
            break;
        case 1:
            cell.numberLabel.text = self.listenerNumber;
            cell.textLabel.text = SPDLocalizedString(@"收听人数");
            break;
        case 2:
            cell.numberLabel.text = self.followNumber;
            cell.textLabel.text = SPDLocalizedString(@"新增关注");
            break;
        default:
            break;
    }
    return cell;
}

#pragma mark - LY_LiveFiringMoreActionSheetViewDelegate

- (void)clickWalletBtn {
    if ([self.delegate respondsToSelector:@selector(didClickWalletButtonInEndedAnchorView)]) {
        [self.delegate didClickWalletButtonInEndedAnchorView];
    }
}

- (void)clickRecordBtn {
    if ([self.delegate respondsToSelector:@selector(didClickRecordButtonInEndedAnchorView)]) {
        [self.delegate didClickRecordButtonInEndedAnchorView];
    }
}

@end
