//
//  LY_LiveFansJoinMessageView.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/24.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_LiveFansJoinMessageView.h"

@interface LY_LiveFansJoinMessageView ()

@property(nonatomic, strong) UIImageView *bgImgView;

@property(nonatomic, strong) LY_PortraitView *portraitView;

@property(nonatomic, strong) UILabel *textLabel;

@property(nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation LY_LiveFansJoinMessageView

- (UIImageView *)bgImgView {
    if (!_bgImgView) {
        _bgImgView = [[UIImageView alloc] init];
        _bgImgView.image = [UIImage imageNamed:@"ic_zhibo_fensiyonghu_jinruzhibojian"].adaptiveRtl;
        _bgImgView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _bgImgView;
}

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
    }
    return _portraitView;
}

- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] init];
        _textLabel.textColor = [UIColor whiteColor];
        _textLabel.font = [UIFont mediumFontOfSize:12];
    }
    return _textLabel;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    return self;
}

- (void)setupUI {
    [self setHidden:true];
    [self addSubview:self.bgImgView];
    [self.bgImgView addSubview:self.portraitView];
    [self.bgImgView addSubview:self.textLabel];
}

- (void)setupUIFrame {
    self.bgImgView.frame = CGRectMake(0, 0, 210, 26);
    if (kIsMirroredLayout) {
        self.portraitView.frame = CGRectMake(210 - 213, -11, 36, 36);
        self.textLabel.frame = CGRectMake(0, 0, 180, 26);
    } else {
        self.portraitView.frame = CGRectMake(-11, -11, 36, 36);
        self.textLabel.frame = CGRectMake(30, 0, 180, 26);
    }
    
//    [self.bgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
////        make.edges.mas_equalTo(self);
//        make.top.bottom.mas_equalTo(self);
//        make.width.mas_equalTo(self);
//        make.leading.mas_equalTo(0);
//    }];
//    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.size.mas_equalTo(24);
//        make.leading.mas_equalTo(self.bgImgView).offset(1);
//        make.centerY.mas_equalTo(self.bgImgView);
//    }];
//    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.bottom.mas_equalTo(self.bgImgView);
//        make.leading.mas_equalTo(self.portraitView.mas_trailing).offset(5);
//        make.trailing.mas_equalTo(self.bgImgView);
//    }];
}

- (void)setMessage:(LiveJoinMessage *)message {
    _message = message;
    [self.dataArray addObject:_message];
    if (self.dataArray.count == 1) {
        [self startAnimationViewWithMessage:_message];
    }
}

- (void)startAnimationViewWithMessage:(LiveJoinMessage *)message {
    LY_Portrait * chanpinshacha = [LY_Portrait new];
    chanpinshacha.headwearUrl = @"";
    chanpinshacha.url = message.portrait.url;
    self.portraitView.portrait = chanpinshacha; // ccw 说这里小粉条不➕头饰
//    self.portraitView.portrait = message.portrait;
//    [self.portraitView sd_setImageWithURL:[NSURL URLWithString:message.avatar] placeholderImage:[UIImage imageNamed:@"img_portrait_placeholder"]];
    self.textLabel.text = [NSString stringWithFormat:@"%@：欢迎进入直播间".localized, message.userName];
    
    [self setHidden:false];
//    [self.bgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.leading.mas_equalTo(210);
//    }];
    if (kIsMirroredLayout) {
        self.bgImgView.frame = CGRectMake(-210, 0, 210, 26);
    } else {
        self.bgImgView.frame = CGRectMake(kScreenW, 0, 210, 26);
    }
    
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
//        [self.bgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.leading.mas_equalTo(0);
//        }];
        if (kIsMirroredLayout) {
            self.bgImgView.frame = CGRectMake(0, 0, 210, 26);
        } else {
            self.bgImgView.frame = CGRectMake(0, 0, 210, 26);
        }
//        self.bgImgView.frame = CGRectMake(0, 0, 210, 26);
//        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        if (finished) {
            [self performSelector:@selector(stopAnimation) withObject:nil afterDelay:1 inModes:@[NSRunLoopCommonModes]];
        } else {
            [self resetFrame];
            [self.dataArray removeAllObjects];
        }
    }];
}

//- (void)startAnimationViewWithMessage:(SPDJoinChatRoomMessage *)message {
//    self.bgImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"enter_%@", message.noble]];
//    self.contentLabel.text = [NSString stringWithFormat:SPDStringWithKey(@"%@ 进入房间", nil), message.senderUserInfo.name];
//
//    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
//        CGRect frame = self.frame;
//        frame.origin.x = 10;
//        self.frame = frame;
//    } completion:^(BOOL finished) {
//        if (finished) {
//            [self performSelector:@selector(stopAnimation) withObject:nil afterDelay:1 inModes:@[NSRunLoopCommonModes]];
//        } else {
//            [self resetFrame];
//            [self.dataArray removeAllObjects];
//        }
//    }];
//}

- (void)stopAnimation {
    NSLog(@"stopAnimation");
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        self.alpha = 1;
        [self resetFrame];
        if (finished) {
            if (self.dataArray.count) {
                [self.dataArray removeObjectAtIndex:0];
            }
            if (self.dataArray.count) {
                [self startAnimationViewWithMessage:self.dataArray[0]];
            }
        } else {
            [self.dataArray removeAllObjects];
        }
    }];
}

- (void)enterBackground {
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self resetFrame];
    [self.dataArray removeAllObjects];
}

- (void)resetFrame {
    [self setHidden:true];
//    [self.bgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.leading.mas_equalTo(210);
//    }];
    if (kIsMirroredLayout) {
        self.bgImgView.frame = CGRectMake(-210, 0, 210, 26);
    } else {
        self.bgImgView.frame = CGRectMake(kScreenW, 0, 210, 26);
    }
    
//    self.bgImgView.frame = CGRectMake(kScreenW, 0, 210, 26);
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

@end
