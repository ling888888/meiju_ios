//
//  LiveRoomGiftView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LiveRoomGiftView;

@protocol LiveRoomGiftViewDelegate <NSObject>

- (void)liveRoomGiftViewShowComboButton:(LiveRoomGiftView *)giftView prepareComboWithURL:(NSString *)URL params:(NSMutableDictionary *)params;

@end

@interface LiveRoomGiftView : UIView

@property (nonatomic, copy) NSString * receiverUserId;
@property (nonatomic, copy) NSString * liveId;
@property (nonatomic, weak) id<LiveRoomGiftViewDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
