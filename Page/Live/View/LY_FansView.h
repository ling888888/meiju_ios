//
//  LY_FansView.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/20.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_FansView : UIControl

@property(nonatomic, copy) NSString *text;

@end

NS_ASSUME_NONNULL_END
