//
//  LiveAudiencePlayBottomView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/22.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveAudiencePlayBottomView.h"
#import "ChatroomMoreFunctionCell.h"
#import "LiveSendRedPackView.h"

@interface LiveAudiencePlayBottomView () <UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView * collectionView;
@property (nonatomic, strong) NSArray * dataArray;

@end

@implementation LiveAudiencePlayBottomView

- (CGFloat)containerHeight {
    return 105 + IphoneX_Bottom;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    [self.containerView addRounded:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:20];
}

- (void)initSubViews {
    [self.containerView addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    [self.collectionView reloadData];
}

#pragma mark - collectionView;

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ChatroomMoreFunctionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatroomMoreFunctionCell" forIndexPath:indexPath];
    NSDictionary * dict = self.dataArray[indexPath.row];
    cell.titleImageView.image = [UIImage imageNamed:dict[@"icon"]];
    cell.titleLabel.text = SPDStringWithKey(dict[@"title"], nil);
    cell.titleImageView.contentMode = UIViewContentModeCenter;
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((kScreenW - 20 - 20) / 4, 105);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 5;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 10, 0, 10);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self dismiss];
    // 发送 幸运数字消息
    if (indexPath.row == 0) {
        // 需求非要让在这里限制 tmd
        
        [RequestUtils GET:URL_NewServer(@"live/live.red.package.config") parameters:@{} success:^(id  _Nullable suceess) {
            if ([suceess[@"levelLimit"] integerValue] <= [_wealthLevel integerValue]) {
                LiveSendRedPackView * view = [LiveSendRedPackView new];
                [view show];
            }else{
                [self showTips:[NSString stringWithFormat:@"财富等级达到%@级后可以发送红包".localized,suceess[@"levelLimit"]]];
            }
        } failure:^(id  _Nullable failure) {
            
        }];
       
       
    }else{
        if (self.delegate && [self.delegate respondsToSelector:@selector(liveAudiencePlayBottomView:didClickedLuckyNumber:)]) {
            [self.delegate liveAudiencePlayBottomView:self didClickedLuckyNumber:(arc4random() % 100)];
        }
    }
}

#pragma mark - getters

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = SPD_HEXCOlOR(@"16181E");
        [_collectionView registerNib:[UINib nibWithNibName:@"ChatroomMoreFunctionCell" bundle:nil] forCellWithReuseIdentifier:@"ChatroomMoreFunctionCell"];
    }
    return _collectionView;
}

- (NSArray *)dataArray {
    if (!_dataArray) {
        _dataArray = @[@{@"icon":@"ic_zhibo_hongbao_!",
                         @"title":@"红包"
        },@{@"icon":@"ic_zhibojian_yule_shaizi",@"title":@"幸运数字"}];
    }
    return _dataArray;
}

@end
