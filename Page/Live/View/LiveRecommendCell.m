//
//  LiveRecommendCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRecommendCell.h"
#import "LiveModel.h"
#import "UIView+LY.h"
#import "UIImage+LY.h"
#import "YYWebImage.h"

@interface LiveRecommendCell ()

@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageVIew;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *listenerNum;
@property (weak, nonatomic) IBOutlet UILabel *liveTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *noBoView;
@property (weak, nonatomic) IBOutlet UILabel *weikaiboLabel;
@property (weak, nonatomic) IBOutlet UIView *jianbianView;
@property (weak, nonatomic) IBOutlet UIImageView *jianbianimageview;
@property (weak, nonatomic) IBOutlet YYAnimatedImageView *pkIconImageView;
@property (weak, nonatomic) IBOutlet UIImageView *redPackImageView;

@end

@implementation LiveRecommendCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.liveFlagImageView startAnimating];
    
    UIColor * endColor =  [[UIColor colorWithHexString:@"000000"] colorWithAlphaComponent:0];
    UIColor * beginColor =  [[UIColor colorWithHexString:@"000000"] colorWithAlphaComponent:0.6];
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.colors = @[(__bridge id)endColor.CGColor, (__bridge id)beginColor.CGColor];
    gradientLayer.startPoint = CGPointMake(0, 0);
    gradientLayer.endPoint = CGPointMake(0, 1);
    gradientLayer.frame = CGRectMake(0, 0, (kScreenW - 15*3)/2, 45);
    [self.jianbianimageview.layer addSublayer:gradientLayer];
    
    self.pkIconImageView.yy_imageURL =  [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"pk_icon" ofType:@"webp"]];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (_model.isBroadcast &&!_model.isPk) {
        [self.noBoView setBackgroundColor:[UIColor colorGradientChangeWithSize:self.noBoView.bounds.size direction:LYGradientChangeDirectionHorizontal  startColor:SPD_HEXCOlOR(@"#22BBB9") endColor:SPD_HEXCOlOR(@"#45E994")]];
    }
    [self.noBoView addRounded:kIsMirroredLayout ? UIRectCornerBottomLeft : UIRectCornerBottomRight withRadius:7];   
}

- (void)setModel:(LiveModel *)model {
    _model = model;
    [self.coverImageView fp_setImageWithURLString:model.liveCover];
    [self.avatarImageVIew fp_setImageWithURLString:model.avatar];
    self.redPackImageView.hidden = !_model.hasRedPackage;
    self.userNameLabel.text = _model.userName;
    self.liveTitleLabel.text = _model.liveTitle;
    self.listenerNum.text = [NSString stringWithFormat:@"%@",_model.listenerNum];
    self.weikaiboLabel.textColor = _model.isBroadcast ? SPD_HEXCOlOR(@"#FFFFFF"):SPD_HEXCOlOR(@"#1A1A1A");
    self.weikaiboLabel.text = _model.isBroadcast ? @"直播中".localized : @"未开播".localized;
    self.weikaiboLabel.hidden = NO;
    if (_model.isBroadcast) {
        NSMutableArray *animationImages = [NSMutableArray array];
        for (NSInteger i = 0; i < 8; i++) {
//            [animationImages addObject:[UIImage imageNamed:[NSString stringWithFormat:@"audio_wave_%zd", i]]];
        }
        self.liveFlagImageView.animationImages = animationImages;
        self.liveFlagImageView.animationDuration = animationImages.count * 0.1;
        if (_model.isPk) {
            self.noBoView.backgroundColor = [SPD_HEXCOlOR(@"#613AEE") colorWithAlphaComponent:0.6];
        }
        self.weikaiboLabel.hidden = _model.isPk;
        self.pkIconImageView.hidden = !_model.isPk;
    }else{
        self.pkIconImageView.hidden = YES;
        self.noBoView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
        self.liveFlagImageView.image = [UIImage imageNamed:@"ic_zhibo_renshu"];
    }
    [self.coverImageView addSubview:self.jianbianimageview];
    [self layoutIfNeeded];
}

- (void)setHighlighted:(BOOL)highlighted {
    
}

- (void)setSelected:(BOOL)selected {
    
}

@end
