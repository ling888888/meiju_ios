//
//  LiveRegisterRecommendCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/23.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveRegisterRecommendCell.h"

@interface LiveRegisterRecommendCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *liveLabel;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectedButton;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;

@end

@implementation LiveRegisterRecommendCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UIImage * image = [[UIImage alloc]initWithGradient:@[[UIColor colorWithHexString:@"22BBB9"],[UIColor colorWithHexString:@"45E994"]] size:CGSizeMake(38, 16) direction:UIImageGradientColorsDirectionHorizontal];
    self.bgImageView.image = image;
    self.bgImageView.layer.cornerRadius = 8;
    self.bgImageView.clipsToBounds = YES;
    
}

- (void)setDict:(NSDictionary *)dict {
    _dict = dict;
    [self.avatarImageView fp_setImageWithURLString:dict[@"avatar"]];
    self.nickNameLabel.text = _dict[@"user_name"];
    self.liveLabel.hidden = ! [_dict[@"status"] boolValue];
    self.bgImageView.hidden = ! [_dict[@"status"] boolValue];
}

- (void)setCellSelected:(BOOL)cellSelected {
    _cellSelected = cellSelected;
    self.selectedButton.selected = _cellSelected;
    self.avatarImageView.layer.borderWidth = _cellSelected ? 2:0;
    self.nickNameLabel.textColor = _cellSelected ? SPD_HEXCOlOR(@"#00D4A0"):SPD_HEXCOlOR(@"#1A1A1A");
}
@end
