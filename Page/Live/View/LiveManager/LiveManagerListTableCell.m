//
//  LiveManagerListTableCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/16.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveManagerListTableCell.h"
#import "LiveUserModel.h"

@interface LiveManagerListTableCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatar;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@end

@implementation LiveManagerListTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(LiveUserModel *)model {
    _model = model;
    [self.avatar fp_setImageWithURLString:_model.avatar];
    self.nameLabel.text = _model.name;
}

- (IBAction)cancelBtnClicked:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveManagerListTableCell:didCancelManager:)]) {
        [self.delegate liveManagerListTableCell:self didCancelManager:self.model];
    }
}

@end
