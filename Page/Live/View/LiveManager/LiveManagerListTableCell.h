//
//  LiveManagerListTableCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/9/16.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class LiveUserModel,LiveManagerListTableCell;

@protocol LiveManagerListTableCellDelegate <NSObject>

@optional
- (void)liveManagerListTableCell:(LiveManagerListTableCell *)cell didCancelManager:(LiveUserModel *)model;

@end

@interface LiveManagerListTableCell : UITableViewCell

@property (nonatomic, strong)LiveUserModel * model;

@property (nonatomic,weak)id<LiveManagerListTableCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
