//
//  LY_LiveRechargeConchView.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/8.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_LiveRechargeConchView.h"
#import "LY_RechargeConchCell.h"
#import "MyWalletListModel.h"
#import "IAPManager.h"

@interface LY_LiveRechargeConchView() <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property(nonatomic, strong) UILabel *rechargeTextLabel;

@property(nonatomic, strong) UILabel *balanceKeyLabel;

@property(nonatomic, strong) UIImageView *balanceIconView;

@property(nonatomic, strong) UILabel *balanceValueLabel;

@property(nonatomic, strong) UICollectionView *collectionView;

@property(nonatomic, strong) UIButton *userAgreementBtn;

@property (nonatomic, strong) NSArray<MyWalletListModel *> *conchArray;

@end

@implementation LY_LiveRechargeConchView

- (UILabel *)rechargeTextLabel {
    if (!_rechargeTextLabel) {
        _rechargeTextLabel = [[UILabel alloc] init];
        _rechargeTextLabel.textColor = [UIColor colorWithHexString:@"#333333"];
        _rechargeTextLabel.font = [UIFont mediumFontOfSize:15];
        _rechargeTextLabel.text = @"充值".localized;
    }
    return _rechargeTextLabel;
}

- (UILabel *)balanceValueLabel {
    if (!_balanceValueLabel) {
        _balanceValueLabel = [[UILabel alloc] init];
        _balanceValueLabel.textColor = [[UIColor colorWithHexString:@"#333333"] colorWithAlphaComponent:0.6];
        _balanceValueLabel.font = [UIFont mediumFontOfSize:14];
    }
    return _balanceValueLabel;
}

- (UIImageView *)balanceIconView {
    if (!_balanceIconView) {
        _balanceIconView = [[UIImageView alloc] init];
        _balanceIconView.contentMode = UIViewContentModeScaleAspectFit;
        _balanceIconView.image = [UIImage imageNamed:@"ic_zhibo_liwuhe_beike_xiao"];
    }
    return _balanceIconView;
}

- (UILabel *)balanceKeyLabel {
    if (!_balanceKeyLabel) {
        _balanceKeyLabel = [[UILabel alloc] init];
        _balanceKeyLabel.textColor = [[UIColor colorWithHexString:@"#333333"] colorWithAlphaComponent:0.6];
        _balanceKeyLabel.font = [UIFont mediumFontOfSize:14];
        _balanceKeyLabel.text = @"余额:".localized;
    }
    return _balanceKeyLabel;
}

- (UIButton *)userAgreementBtn {
    if (!_userAgreementBtn) {
        _userAgreementBtn = [[UIButton alloc] init];
        
        NSString *text1 = @"充值即代表同意".localized;
        NSString *text2 = @"用户充值协议".localized;
        NSString *totalText = [NSString stringWithFormat:@"%@%@", text1, text2];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:totalText];
        [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(0, totalText.length)];
        [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#1A1A1A"] range:NSMakeRange(0, totalText.length)];
        [attributedString addAttribute:NSUnderlineStyleAttributeName value: [NSNumber numberWithInt:NSUnderlineStyleSingle] range:NSMakeRange(0, totalText.length)];
        [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#6A2CF7"] range:NSMakeRange(text1.length, text2.length)];
        
        [_userAgreementBtn setAttributedTitle:attributedString forState:UIControlStateNormal];
        [_userAgreementBtn setHidden:true];
    }
    return _userAgreementBtn;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [_collectionView registerClass:[LY_RechargeConchCell class] forCellWithReuseIdentifier:@"RechargeConchIdentifier"];
    }
    return _collectionView;
}

- (CGFloat)containerHeight {
    return 311;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
        _balanceValueLabel.text = @"--";
        [self requestMyBalance];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    [self.containerView addRounded:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:20];
}

- (void)setupUI {
    [self.containerView addSubview:self.rechargeTextLabel];
    [self.containerView addSubview:self.balanceValueLabel];
    [self.containerView addSubview:self.balanceIconView];
    [self.containerView addSubview:self.balanceKeyLabel];
    [self.containerView addSubview:self.userAgreementBtn];
    [self.containerView addSubview:self.collectionView];
}

- (void)setupUIFrame {
    [self.rechargeTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(10);
    }];
    
    [self.balanceValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-15);
        make.centerY.mas_equalTo(self.rechargeTextLabel);
    }];
    
    [self.balanceIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(self.balanceValueLabel.mas_leading).offset(-5);
        make.size.mas_equalTo(CGSizeMake(14, 12));
        make.centerY.mas_equalTo(self.rechargeTextLabel);
    }];
    
    [self.balanceKeyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(self.balanceIconView.mas_leading).offset(-5);
        make.centerY.mas_equalTo(self.rechargeTextLabel);
    }];
    
    [self.userAgreementBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.containerView);
        make.bottom.mas_equalTo(-13);
    }];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.rechargeTextLabel.mas_bottom).offset(10);
        make.leading.trailing.mas_equalTo(self.containerView);
        make.bottom.mas_equalTo(self.userAgreementBtn.mas_top).offset(-10);
    }];
}

- (void)requestMyBalance {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.balance" bAnimated:NO bAnimatedViewController:[UIViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        self.balanceValueLabel.text = [suceess[@"conch"] stringValue];
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.conchArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LY_RechargeConchCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RechargeConchIdentifier" forIndexPath:indexPath];
    cell.model = self.conchArray[indexPath.row];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((kScreenW - 20 - 20) / 3, 105);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 5;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 10, 0, 10);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self dismiss];
    [kIAPManager startPaymentWithProductId:self.conchArray[indexPath.row].productId completion:nil];
}

//- (NSArray *)conchArray {
//    if (!_conchArray) {
//        _conchArray = @[[MyWalletListModel initWithDictionary:@{@"productId": @"com.yakka.42shells",
//                                                                @"number": @"700",
//                                                                @"price": @"$0.99"}],
//                        [MyWalletListModel initWithDictionary:@{@"productId": @"com.yakka.420shells",
//                                                                @"number": @"7000",
//                                                                @"price": @"$9.99"}],
//                        [MyWalletListModel initWithDictionary:@{@"productId": @"com.yakka.1260shells",
//                                                                @"number": @"21000",
//                                                                @"price": @"$29.99"}],
//                        [MyWalletListModel initWithDictionary:@{@"productId": @"com.yakka.4200shells",
//                                                                @"number": @"70000",
//                                                                @"price": @"$99.99"}],
//                        [MyWalletListModel initWithDictionary:@{@"productId": @"com.yakka.8400shells",
//                                                                @"number": @"140000",
//                                                                @"price": @"$199.99"}]];
//    }
//    return _conchArray;
//}

- (NSArray *)conchArray {
    if (!_conchArray) {
        _conchArray = @[[MyWalletListModel initWithDictionary:@{@"productId": @"com.yakka.42shells",
                                                                @"number": @"700",
                                                                @"price": @"$0.99"}],
                        [MyWalletListModel initWithDictionary:@{@"productId": @"com.yakka.420shells",
                                                                @"number": @"7000",
                                                                @"price": @"$9.99"}],
                        [MyWalletListModel initWithDictionary:@{@"productId": @"com.yakka.1260shells",
                                                                @"number": @"21000",
                                                                @"price": @"$29.99"}],
                        [MyWalletListModel initWithDictionary:@{@"productId": @"com.yakka.4200shells",
                                                                @"number": @"70000",
                                                                @"price": @"$99.99"}],
                        [MyWalletListModel initWithDictionary:@{@"productId": @"com.yakka.8400shells",
                                                                @"number": @"140000",
                                                                @"price": @"$199.99"}]];
    }
    return _conchArray;
}

@end
