//
//  LiveFreeGiftCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/22.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveFreeGiftCell.h"
#import "ShakeLabel.h"
@interface LiveFreeGiftCell ()

@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet ShakeLabel *giftNumberLabel;

@end

@implementation LiveFreeGiftCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handle)];
    self.statusLabel.userInteractionEnabled = YES;
    [self.statusLabel addGestureRecognizer:tap];
    self.giftNumberLabel.borderColor = SPD_HEXCOlOR(@"FE85A6");
}

- (void)handle {
    self.statusLabel.userInteractionEnabled = NO;// 防止重复点击
    if ([self.statusLabel.text isEqualToString:[NSString stringWithFormat:@"%@",@"可领取".localized]]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AchiveFreeGift" object:nil];
    }
}

- (void)setDict:(NSDictionary *)dict {
    _dict = dict;
    
    self.giftNumberLabel.text = [NSString stringWithFormat:@"x%@",_dict[@"giftNumber"]];
    [self.coverImageView fp_setImageWithURLString:_dict[@"giftCover"]];
    
    NSDictionary * huancunDict = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"LiveFreeGift:%@",[SPDApiUser currentUser].userId]];
    NSInteger mIndex = [huancunDict[@"index"] integerValue];
    if (_index < mIndex) {
        self.statusLabel.text = @"已领取".localized;
        self.statusLabel.textColor = [SPD_HEXCOlOR(@"#FFFFFF") colorWithAlphaComponent:0.4];
        self.statusLabel.backgroundColor = [UIColor clearColor];
        self.contentView.alpha = 0.6;
    }else{
       NSInteger duration = [huancunDict[@"duration"] integerValue];
        self.statusLabel.backgroundColor = SPD_HEXCOlOR(@"#6A2CF7");
       if (mIndex == _index) {
           self.statusLabel.textColor = SPD_HEXCOlOR(@"FFFFFF");
           self.statusLabel.text = duration == 0 ? @"可领取".localized:[NSString stringWithFormat:@"%@s",@(duration)];
           self.contentView.alpha = 1;
       }else{
           self.statusLabel.text = @"请稍等".localized;
           self.contentView.alpha = 0.6;
       }
    }

}

- (void)updateSecond:(NSInteger)second {
    self.statusLabel.text = [NSString stringWithFormat:@"%@s",@(second)];
}

@end
