//
//  LiveWorldMessageView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/8/24.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LiveWorldMessage;

NS_ASSUME_NONNULL_BEGIN

@interface LiveWorldMessageView : UIView

@property (nonatomic, strong) LiveWorldMessage * liveWorldMessage; // 直播间

@end


NS_ASSUME_NONNULL_END
