//
//  LiveNotifyFansMessageCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/26.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveNotifyFansMessageCell.h"
#import "LiveBeginNotificationMessage.h"
#import "NSString+XXWAddition.h"

@interface LiveNotifyFansMessageCell ()

@property (nonatomic, strong) UIView *bgContentView;
@property (nonatomic, strong) UILabel *sloganLabel;
@property (nonatomic, strong) UILabel *liveTitleLabel;
@property (nonatomic, strong) UILabel *liveUserNameLabel;
@property (nonatomic, strong) UIImageView *liveCoverImageView;
@property (nonatomic, assign) CGFloat cellHeight;

@end

@implementation LiveNotifyFansMessageCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initSubViews];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super initWithCoder:coder]) {
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews {
    
    self.bgContentView = [[UIView alloc]init];
    self.bgContentView.backgroundColor = [UIColor whiteColor];
    self.bgContentView.layer.cornerRadius = 5;
    self.bgContentView.clipsToBounds = YES;
    [self.messageContentView addSubview:self.bgContentView];
    [self.bgContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(0);
        make.top.mas_equalTo(5);
        make.width.mas_equalTo(185);
    }];
    self.bgContentView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapPress =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPress:)];
    [self.bgContentView addGestureRecognizer:tapPress];
    
    self.sloganLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    self.sloganLabel.font = [UIFont systemFontOfSize:15];
    self.sloganLabel.textColor = SPD_HEXCOlOR(@"#1A1A1A");
    self.sloganLabel.textAlignment = NSTextAlignmentLeft;
    self.sloganLabel.numberOfLines = 0;
    [self.bgContentView addSubview:self.sloganLabel];
    [self.sloganLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(11);
        make.top.mas_equalTo(10);
        make.trailing.mas_equalTo(0);
    }];
    
    self.liveCoverImageView =[[UIImageView alloc]initWithFrame:CGRectZero];
    self.liveCoverImageView.layer.cornerRadius = 7;
    self.liveCoverImageView.clipsToBounds = YES;
    self.liveCoverImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.liveCoverImageView.clipsToBounds = YES;
    [self.bgContentView addSubview:self.liveCoverImageView];
    [self.liveCoverImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(10);
        make.top.equalTo(self.sloganLabel.mas_bottom).offset(8);
        make.height.mas_equalTo(165);
        make.trailing.mas_equalTo(-10);
        make.bottom.equalTo(self.bgContentView.mas_bottom).offset(-10);
    }];
    
    self.liveTitleLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    self.liveTitleLabel.font = [UIFont systemFontOfSize:12];
    self.liveTitleLabel.textColor = [UIColor whiteColor];
    self.liveTitleLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgContentView addSubview:self.liveTitleLabel];
    [self.liveTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.liveCoverImageView.mas_bottom).offset(-7);
        make.leading.equalTo(self.liveCoverImageView.mas_leading).offset(10);
        make.trailing.equalTo(self.liveCoverImageView.mas_trailing).offset(-10);
    }];
    
    self.liveUserNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 132, 185-20, 12)];
    self.liveUserNameLabel.font = [UIFont systemFontOfSize:12];
    self.liveUserNameLabel.textColor = [UIColor whiteColor];
    self.liveUserNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgContentView addSubview:self.liveUserNameLabel];
    [self.liveUserNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.liveTitleLabel.mas_top).offset(-4);
        make.leading.equalTo(self.liveTitleLabel.mas_leading).offset(0);
        make.trailing.equalTo(self.liveTitleLabel.mas_trailing).offset(0);
    }];
    
}

- (void)setDataModel:(RCMessageModel *)model {
    
    [super setDataModel:model];
    
    [self setAutoLayout];
}

- (void)setAutoLayout {
    NSArray * array = [[SPDCommonTool getFamyLanguage] componentsSeparatedByString:@"-"];
    LiveBeginNotificationMessage *message = (LiveBeginNotificationMessage *)self.model.content;
    self.sloganLabel.text = message.slogan[array[0]];
    self.sloganLabel.textAlignment = !kIsMirroredLayout ? NSTextAlignmentLeft:NSTextAlignmentRight;
    [_sloganLabel sizeToFit];
    
    [self.liveCoverImageView fp_setImageWithURLString:message.liveCover];
    self.liveTitleLabel.text = message.liveTitle;
    self.liveUserNameLabel.text = message.senderUserInfo.name;
}

//升级SDK之后-
+(CGSize)sizeForMessageModel:(RCMessageModel *)model withCollectionViewWidth:(CGFloat)collectionViewWidth referenceExtraHeight:(CGFloat)extraHeight{
    NSArray * array = [[SPDCommonTool getFamyLanguage] componentsSeparatedByString:@"-"];
    LiveBeginNotificationMessage *message = (LiveBeginNotificationMessage *)model.content;
    CGFloat height = [NSString sizeWithString:message.slogan[array[0]] andFont:[UIFont systemFontOfSize:15] andMaxSize:CGSizeMake(175, CGFLOAT_MAX)].height;
    return CGSizeMake(kScreenW, height  + 30 + 165 + extraHeight);
}

- (void)tapPress:(id)sender {
    UITapGestureRecognizer *press = (UITapGestureRecognizer *)sender;
    if (press.state == UIGestureRecognizerStateEnded) {
        [self.delegate didTapMessageCell:self.model];
        return;
    }
}

@end
