//
//  LiveRoomUserInfoActionView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/5/12.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveRoomUserInfoActionView.h"

@implementation LiveRoomUserInfoActionView

- (void)setIsMeOnMic:(BOOL)isMeOnMic {
    _isMeOnMic = isMeOnMic;
    self.mentionButton.hidden = _isMeOnMic;
    self.chatButton.hidden = _isMeOnMic;
    self.friendButton.hidden = _isMeOnMic;
    self.actionButton.hidden = _isMeOnMic;
    self.giftButton.hidden = _isMeOnMic;
    self.downMicButton.hidden = !_isMeOnMic;
    self.muteButton.hidden = !_isMeOnMic;
    
}

@end
