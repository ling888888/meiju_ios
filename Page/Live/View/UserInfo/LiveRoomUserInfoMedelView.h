//
//  LiveRoomUserInfoMedelView.h
//  SimpleDate
//
//  Created by 李楠 on 2020/3/30.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LiveUserModel;

@interface LiveRoomUserInfoMedelView : UIView

@property (nonatomic, strong) LiveUserModel *userInfo;

@end

NS_ASSUME_NONNULL_END
