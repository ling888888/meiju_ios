//
//  LiveRoomUserInfoMedalView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/18.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveRoomUserInfoMedalView.h"
#import "LY_HPMedalCollectionCell.h"
#import "MedalListController.h"
#import "MedalModel.h"
#import "MedalLevelViewController.h"
#import "UIButton+HXExtension.h"

@interface LiveRoomUserInfoMedalView ()<UICollectionViewDelegate,UICollectionViewDataSource,UIGestureRecognizerDelegate>

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UICollectionView * collectionView;
@property (nonatomic, strong) UIButton * arrowImageView;
@property (nonatomic, strong) UIImageView * lightImageView;
@property (nonatomic, strong) NSMutableArray * dataArray;

@end

@implementation LiveRoomUserInfoMedalView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = SPD_HEXCOlOR(@"F5F5F5");
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
        self.userInteractionEnabled = YES;
        tap.delegate = self;
        [self addGestureRecognizer:tap];
        self.layer.cornerRadius = 5;
        self.clipsToBounds = YES;
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews {
    [self addSubview:self.titleLabel];
    [self addSubview:self.lightImageView];
    [self addSubview:self.arrowImageView];
    [self addSubview:self.collectionView];
    [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-10);
        make.centerY.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(9, 16));
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.leading.mas_equalTo(10);
    }];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(87);
        make.trailing.mas_equalTo(-34);
        make.top.mas_equalTo(12);
        make.bottom.mas_equalTo(-12);
    }];
    [self.lightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.and.trailing.mas_equalTo(0);
        make.width.mas_equalTo(60);
        make.bottom.mas_equalTo(0);
    }];
}

- (void)handleArrowClicked:(UIButton *)sender {
    MedalListController * medalVC = [MedalListController new];
    medalVC.userId = self.userId;
    medalVC.name = self.name;
    [self.VC.navigationController pushViewController:medalVC animated:YES];
}

- (void)setUserId:(NSString *)userId {
    _userId = userId;
    [self requestList];
}

- (void)requestList {
    [RequestUtils GET:URL_Server(@"user.medal") parameters:@{@"user_id":self.userId} success:^(id  _Nullable suceess) {
        NSMutableArray * dataArray = [NSMutableArray new];
        for (NSDictionary * dic in suceess[@"medal"]) {
            MedalModel * model = [MedalModel initWithDictionary:dic];
            [dataArray  addObject:model];
        }
        self.dataArray = [dataArray mutableCopy];
        if (dataArray.count == 0) {
            for (int i = 0; i < 5; i++) {
                MedalModel * model = [MedalModel new];
                model.cover = @"https://famy.17jianyue.cn/ic_xunzhang_moren%402x.png";
                model.name = @"敬请期待".localized;
                [dataArray addObject:model];
            }
        }

        [self.collectionView reloadData];
    } failure:^(id  _Nullable failure) {
        
    }];
}

- (void)setDataArray:(NSMutableArray *)dataArray {
    _dataArray = dataArray;
    [self.collectionView reloadData];
}

- (void)handleTap:(UITapGestureRecognizer *)tap {
    [self handleArrowClicked:self.arrowImageView];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count >= 5 ? 5 : self.dataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LY_HPMedalCollectionCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LY_HPMedalCollectionCell" forIndexPath:indexPath];
    cell.medalModel = self.dataArray[indexPath.row];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    MedalModel * model = self.dataArray[indexPath.row];
    if (model.items.count == 0) {
        return;
    }
    MedalLevelViewController * vc = [MedalLevelViewController new];
    vc.userId = self.userId;
    vc.currentIndex = model.level;
    vc.medalArray = [model.items mutableCopy];
    [self.VC presentViewController:vc animated:YES completion:nil];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    CGPoint point = [touch locationInView:self.collectionView];
    if (CGRectContainsPoint(self.collectionView.bounds, point)) {
        return NO;
    }
    return YES;
}

#pragma mark - getter

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = [UIFont boldSystemFontOfSize:15];
        _titleLabel.text = @"勋章墙".localized;
        _titleLabel.textColor = SPD_HEXCOlOR(@"1A1A1A");
    }
    return _titleLabel;
}

- (UIImageView *)lightImageView {
    if (!_lightImageView) {
        _lightImageView = [UIImageView new];
        _lightImageView.image = kIsMirroredLayout ? [UIImage imageNamed:@"img_gerentanchuang_xunzhangbeijing"].mirroredImage:[UIImage imageNamed:@"img_gerentanchuang_xunzhangbeijing"];
    }
    return _lightImageView;
}

- (UIButton *)arrowImageView {
    if (!_arrowImageView) {
        _arrowImageView = [UIButton buttonWithType:UIButtonTypeCustom];
        _arrowImageView.tintColor = SPD_HEXCOlOR(@"1A1A1A");
        [_arrowImageView setImage:[UIImage imageNamed:@"ic_all_clan_arrow"].adaptiveRtl forState:UIControlStateNormal];
        [_arrowImageView hx_setEnlargeEdgeWithTop:10 right:10 bottom:10 left:10];
        [_arrowImageView addTarget:self action:@selector(handleArrowClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _arrowImageView;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout new];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumLineSpacing = 5;
        layout.minimumInteritemSpacing = 5;
        CGFloat width = (kScreenW - 5*2 - 4*5 - 34- 87)/5.0f;
        layout.itemSize = CGSizeMake(width, width);
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [_collectionView registerClass:[LY_HPMedalCollectionCell class] forCellWithReuseIdentifier:@"LY_HPMedalCollectionCell"];
        _collectionView.backgroundColor = [UIColor clearColor];
    }
    return _collectionView;
}


@end
