//
//  LiveRoomUserInfoTagCell.h
//  SimpleDate
//
//  Created by 李楠 on 2020/5/11.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LiveUserModel;

typedef NS_ENUM(NSUInteger, LiveRoomUserInfoTag) {
    LiveRoomUserInfoTagHeart,
    LiveRoomUserInfoTagFans,
    LiveRoomUserInfoTagWealth
};

@interface LiveRoomUserInfoTagCell : UICollectionViewCell

- (void)showTag:(LiveRoomUserInfoTag)tag ofUser:(LiveUserModel *)userInfo;
- (void)flipFromTop;

@end

NS_ASSUME_NONNULL_END
