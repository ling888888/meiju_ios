//
//  LiveRoomUserInfoManageView.h
//  SimpleDate
//
//  Created by 李楠 on 2020/5/14.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol LiveRoomUserInfoManageViewDelegate <NSObject>

@optional

- (void)didSelectManage:(NSString *)manage;

@end

@interface LiveRoomUserInfoManageView : UIView

@property (nonatomic, weak) id<LiveRoomUserInfoManageViewDelegate> delegate;

- (void)showWithManageArray:(NSMutableArray *)manageArray;

@end

NS_ASSUME_NONNULL_END
