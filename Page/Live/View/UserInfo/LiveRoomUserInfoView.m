//
//  LiveRoomUserInfoView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/3/19.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveRoomUserInfoView.h"
#import "LiveModel.h"
#import "LiveUserModel.h"
#import "ZegoKitManager.h"
#import "LinkMicModel.h"
//#import "LiveRoomUserInfoMedelView.h"
#import "LiveRoomUserInfoProfileView.h"
#import "LiveRoomUserInfoTagCell.h"
#import "LiveRoomUserInfoContributionView.h"
#import "LiveRoomUserInfoActionView.h"
#import "LiveRoomUserInfoManageView.h"
#import "LY_HPTagView.h"
#import "LinkMicNewModel.h"
#import "LiveLinkMicSilenceMsg.h"
#import "LiveManagerListController.h"
#import "LiveRoomUserInfoMedalView.h"

@interface LiveRoomUserInfoView ()<UICollectionViewDataSource, UICollectionViewDelegate, UIGestureRecognizerDelegate, LiveRoomUserInfoManageViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
//@property (weak, nonatomic) IBOutlet UIButton *avatarButton;
@property (nonatomic, strong) LY_PortraitView *portraitView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *genderImageView;
@property (weak, nonatomic) IBOutlet UIView *idView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *idViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *idViewLeading1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *idViewLeading2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *idViewLeading3;
@property (weak, nonatomic) IBOutlet UILabel *followNumLabel;
@property (weak, nonatomic) IBOutlet UIImageView *specialNumImageView;
@property (weak, nonatomic) IBOutlet UILabel *idLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *idLabelLeading;
@property (weak, nonatomic) IBOutlet UIButton *reportButtton;
@property (weak, nonatomic) IBOutlet UIButton *manageButtton;

@property(nonatomic, strong) LY_HPTagView *tagView;
//@property (nonatomic, strong) LiveRoomUserInfoMedelView *medalView;
@property (nonatomic, strong) LiveRoomUserInfoProfileView *profileView;
@property (nonatomic, strong) UICollectionView *tagCollectionView;
@property (nonatomic, strong) LiveRoomUserInfoContributionView *contributionView;
@property (nonatomic, strong) LiveRoomUserInfoActionView *actionView;
@property (nonatomic, strong) LiveRoomUserInfoMedalView *medalView; // 勋章墙

@property (nonatomic, assign) CGFloat bottomHeight;
@property (nonatomic, strong) LiveUserModel *userInfo;
@property (nonatomic, strong) NSMutableArray<NSNumber *> *tagArray;
@property (nonatomic, strong) NSMutableArray *manageArray;

@end

@implementation LiveRoomUserInfoView

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(portraitViewTapGestureAction)];
        [_portraitView addGestureRecognizer:tapGesture];
    }
    return _portraitView;
}

- (LY_HPTagView *)tagView {
    if (!_tagView) {
        _tagView = [[LY_HPTagView alloc] initWithType:LY_HPTagViewTypeAnchor];
    }
    return _tagView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.bottomHeight = 100;
    self.contentViewBottom.constant = -37.5 -self.contentViewHeight.constant;
    
    [self addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(106.5);
        make.centerX.mas_equalTo(0);
        make.centerY.mas_equalTo(self.contentView.mas_top);
    }];
    
    [self.contentView addSubview:self.tagView];
    [self.tagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.genderImageView.mas_bottom).with.offset(10);
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(16);
    }];
    self.idViewTop.constant = 35;
    
        
    [self layoutIfNeeded];
}

#pragma mark - Public methods

- (void)showWithUserId:(NSString *)userId liveInfo:(LiveModel *)liveInfo isAnchor:(BOOL)isAnchor {
    [MBProgressHUD showHUDAddedTo:self animated:YES];
    
    NSDictionary *params = @{@"userId": userId, @"liveId": liveInfo.liveId};
    [RequestUtils GET:URL_NewServer(@"live/live.user.info") parameters:params success:^(id  _Nullable suceess) {
        self.userInfo = [LiveUserModel initWithDictionary:suceess];
        self.portraitView.portrait = self.userInfo.portrait;
        
        self.nickNameLabel.text = self.userInfo.name;
        
        self.genderImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"room_user_info_%@", self.userInfo.gender]];
        
        // 设置权重
        NSInteger userPostionWeight;
        if ([userId isEqualToString:ZegoManager.liveInfo.userId]) {
            userPostionWeight = PositionWeightAnchor;
        }else {
            userPostionWeight = [suceess[@"isAdmin"] boolValue] ? PositionWeightManager : PositionWeightAudience;
        }
        
        self.manageButtton.hidden = !(self.currentPositionWeight > userPostionWeight);
        
        self.tagView.isAnchor = self.userInfo.isAnchor;
        self.tagView.fansTag = self.userInfo.fansTag;
        self.tagView.customTag = self.userInfo.customTag;
        if (self.userInfo.noble.notEmpty) {
            self.tagView.isNoble = true;
            self.tagView.nobleImgUrlStr = self.userInfo.noble;
        } else {
            self.tagView.isNoble = false;
        }
        
        self.tagView.wealthLevel = self.userInfo.wealthLevel;
        self.tagView.isAdmin = [suceess[@"isAdmin"] boolValue];
        [self.tagView refresh];
        
        self.idViewLeading1.priority = UILayoutPriorityDefaultLow;
        self.idViewLeading2.priority = UILayoutPriorityDefaultLow;
        self.idViewLeading3.priority = UILayoutPriorityDefaultLow;
        if (self.userInfo.followNum.integerValue > 0) {
            self.followNumLabel.text = [NSString stringWithFormat:SPDLocalizedString(@"粉丝数:%@"), self.userInfo.followNum];
            self.idViewLeading3.priority = UILayoutPriorityDefaultHigh;
        } else if (self.userInfo.specialNum.notEmpty) {
            self.idViewLeading2.priority = UILayoutPriorityDefaultHigh;
        } else {
            self.idViewLeading1.priority = UILayoutPriorityDefaultHigh;
        }
        if (self.userInfo.specialNum.notEmpty) {
            self.specialNumImageView.hidden = NO;
            self.idLabel.text = self.userInfo.specialNum;
            self.idLabel.textColor = [UIColor colorWithHexString:@"F7D411"];
            self.idLabelLeading.constant = 48;
        } else {
            self.idLabel.text = [NSString stringWithFormat:@"ID:%@", [SPDCommonTool getInvitationCodeWithUserId:self.userInfo.userId]];
        }
        
        if (self.userInfo.personalDesc.notEmpty) {
            self.profileView.profileLabel.text = self.userInfo.personalDesc;
        }
        
        if (self.userInfo.isAnchor) {
            if (self.userInfo.heartRange.integerValue > 0) {
                [self.tagArray addObject:@(LiveRoomUserInfoTagHeart)];
            }
            if (self.userInfo.fansNum.notEmpty) {
                [self.tagArray addObject:@(LiveRoomUserInfoTagFans)];
            }
        }
        [self.tagArray addObject:@(LiveRoomUserInfoTagWealth)];
        [self.tagCollectionView reloadData];
        
        if ((!self.userInfo.isAnchor)) {
//            self.contributionView.numberLabel.text = [SPDCommonTool transformIntegerToBriefStr:self.userInfo.weekContribution.integerValue];
        }
        
        // 勋章墙
        [self.contentView addSubview:self.medalView];
        [self.medalView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo(5);
            make.trailing.mas_equalTo(-5);
            make.top.equalTo(self.tagCollectionView.mas_bottom).offset(8);
            make.height.mas_equalTo(70.0f/365*(kScreenW - 10));
        }];
        
        CGFloat bottomSpace = 20;
        if (![userId isEqualToString:[SPDApiUser currentUser].userId]) {
            self.reportButtton.hidden = NO;
            bottomSpace = 0;
            self.actionView.friendButton.selected = self.userInfo.isFriend;
            self.actionView.isMeOnMic = NO;
            if (self.currentPositionWeight > userPostionWeight) {
                self.manageButtton.hidden = NO;
                if (self.userInfo.canChat) {
                    [self.manageArray addObject:@{@"action": @"forbidden", @"name": @"禁言"}];
                } else {
                    [self.manageArray addObject:@{@"action": @"release", @"name": @"取消禁言"}];
                }
                [self.manageArray addObject:@{@"action": @"kickout", @"name": @"踢出"}];
                if (isAnchor) {
                    if (userPostionWeight < PositionWeightManager) {
                        [self.manageArray addObject:@{@"action": @"setAdmin", @"name": @"设管理员"}];
                    }else{
                        [self.manageArray addObject:@{@"action": @"removeAdmin", @"name": @"取消管理员"}];

                    }
                }
                if (ZegoManager.linkMicSwitch) {
                    [self.actionView.actionButton setImage:ZegoManager.linkMicUserInfoDic[userId]?SPD_Image_BYNAME(@"ic_zhibijian_yonghuziliaoye_xiamai"): SPD_Image_BYNAME(@"ic_zhibojian_gerenziliaoye_yaoqinglianmai") forState:UIControlStateNormal];
                    [self.actionView.actionButton addTarget:self action:@selector(clickLinkMicButton:) forControlEvents:UIControlEventTouchUpInside];
                    self.actionView.friendButtonTraining.constant = (kScreenW - 0.5 * 3) / 5 + 0.5;
                }
            } else if (self.userInfo.isAnchor) {
                [self.actionView.actionButton setImage:[UIImage imageNamed:@"iconz-zhibojian_gerenziliaoye_guanzhu"] forState:UIControlStateNormal];
                [self.actionView.actionButton setImage:[UIImage imageNamed:@"iconz-zhibojian_gerenziliaoye_guanzhu(1)"] forState:UIControlStateSelected];
                [self.actionView.actionButton addTarget:self action:@selector(clickFollowButton:) forControlEvents:UIControlEventTouchUpInside];
                self.actionView.actionButton.selected = self.userInfo.isFollow;
                self.actionView.friendButtonTraining.constant = (kScreenW - 0.5 * 3) / 5 + 0.5;
            }
        }else {
            if (ZegoManager.linkMicUserInfoDic[userId]) { // 我在麦上 需要添加 静音和下麦按钮
                self.actionView.isMeOnMic = YES;
                LinkMicNewModel * model =  ZegoManager.linkMicUserInfoDic[userId];
                self.actionView.muteButton.selected = [model.inSilence isEqualToString:@"1"];
            }
        }
        
        [self layoutIfNeeded];
        self.contentViewHeight.constant = CGRectGetMaxY(self.contentView.subviews.lastObject.frame) + bottomSpace + self.bottomHeight;
        self.contentViewBottom.constant = -37.5 -self.contentViewHeight.constant;
        [self show];
        
        [MBProgressHUD hideHUDForView:self animated:YES];
    } failure:^(id  _Nullable failure) {
        [MBProgressHUD hideHUDForView:self animated:YES];
        [self removeFromSuperview];
    }];
}

#pragma mark - Event responses

- (IBAction)tapBackground:(UITapGestureRecognizer *)sender {
    [self hide];
}

- (void)portraitViewTapGestureAction {
    if ([self.delegate respondsToSelector:@selector(userInfoView:didClickUserAvatar:)]) {
        [self.delegate userInfoView:self didClickUserAvatar:self.userInfo];
    }
    [self hide];
}

- (IBAction)clickCopyButton:(UIButton *)sender {
    UIPasteboard.generalPasteboard.string = [self.idLabel.text hasPrefix:@"ID:"] ? [self.idLabel.text substringFromIndex:3] : self.idLabel.text;
    [self showTips:SPDLocalizedString(@"ID已复制")];
}

- (IBAction)clickReportButton:(UIButton *)sender {
    [self didSelectAction:@"report"];
}

- (IBAction)clickManageButton:(UIButton *)sender {
    LiveRoomUserInfoManageView *view = [[NSBundle mainBundle] loadNibNamed:@"LiveRoomUserInfoManageView" owner:self options:nil].firstObject;
    view.frame = self.bounds;
    view.delegate = self;
    [self addSubview:view];
    [view showWithManageArray:self.manageArray];
}

- (void)clickContributionView:(UIControl *)sender {
    [self didSelectAction:@"contribution"];
}

- (void)clickMentionButton:(UIButton *)sender {
    NSDictionary *userInfo = @{@"user_id": self.userInfo.userId, @"nick_name": self.userInfo.name, @"avatar": self.userInfo.avatar};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LiveLongPressAvatar" object:nil userInfo:userInfo];
    [self hide];
}

- (void)clickChatButton:(UIButton *)sender {
    [self didSelectAction:@"chat"];
}

- (void)clickFriendButton:(UIButton *)sender {
    if (sender.selected) {
        LYPSTextAlertView *view = [LYPSTextAlertView new];
        view.title = SPDLocalizedString(@"提示");
        view.message = SPDLocalizedString(@"是否删除好友？");
        [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"取消") style:LYPSystemAlertActionStyleCancel handler:^(LYPSystemAlertAction * _Nonnull action) {
            
        }]];
        [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"确定") style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
            [self requestFriendWithType:@"delete"];
        }]];
        [view present];
    } else {
        [self requestFriendWithType:@"request"];
    }
}

- (void)clickFollowButton:(UIButton *)sender {
    if (sender.selected) {
        LYPSTextAlertView *view = [LYPSTextAlertView new];
        view.title = SPDLocalizedString(@"提示");
        view.message = SPDLocalizedString(@"是否取消关注此用户？");
        [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"取消") style:LYPSystemAlertActionStyleCancel handler:^(LYPSystemAlertAction * _Nonnull action) {
            
        }]];
        [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"确定") style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
            [self didSelectAction:@"unfollow"];
        }]];
        [view present];
    } else {
        [self didSelectAction:@"follow"];
    }
}

- (void)clickLinkMicButton:(UIButton *)sender {
    if (!ZegoManager.linkMicUserInfoDic[self.userInfo.userId]) {
        if (ZegoManager.linkMicUserInfoDic.count < 8) {
            [ZegoManager sendCustomCommandToUser:self.userInfo.userId content:@"invite"];
            LinkMicModel *model = [LinkMicModel new];
            model._id = self.userInfo.userId;
            model.avatar = self.userInfo.avatar;
            model.portrait = self.userInfo.portrait;
            model.nick_name = self.userInfo.name;
            [ZegoManager.linkMicInvitingDic setValue:model forKey:self.userInfo.userId];
            [self hide];
        } else {
            [self showTips:SPDLocalizedString(@"最多可同时连麦8人")];
        }
    }else{//主播给别人下麦
        LinkMicNewModel * model = ZegoManager.linkMicUserInfoDic[self.userInfo.userId];
        [ZegoManager requestDownMic:@{@"broadcaster":ZegoManager.liveInfo.userId,@"number":model.site ,@"type":@"-1",@"liveId":ZegoManager.liveInfo.liveId,@"siteUserId":self.userInfo.userId}];
        [self hide];
    }
    
}

// 主动下麦
- (void)clickMeDownMicButton:(UIButton *)sender {
    LinkMicNewModel * model = ZegoManager.linkMicUserInfoDic[self.userInfo.userId];
    [ZegoManager requestDownMic:@{@"broadcaster":ZegoManager.liveInfo.userId,@"number":model.site ,@"type":@"1",@"liveId":ZegoManager.liveInfo.liveId,@"siteUserId":self.userInfo.userId}];
    [self hide];
}

// 请求静音接口
- (void)clickMuteButton:(UIButton *)sender {
    sender.selected = !sender.selected;
    [RequestUtils POST:URL_NewServer(@"live/live.voice.save") parameters:@{@"broadcaster":ZegoManager.liveInfo.userId,@"inSilence":sender.selected ? @"1":@"0"}
               success:^(id  _Nullable suceess) {
        [ZegoManager enableMic:!sender.selected];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MeMicStatusDidChangeNotification" object:nil];
        LiveLinkMicSilenceMsg * msg = [[LiveLinkMicSilenceMsg alloc]init];
        [[RCIM sharedRCIM] sendMessage:ConversationType_CHATROOM targetId:ZegoManager.liveInfo.liveId content:msg pushContent:@"" pushData:@"" success:^(long messageId) {
        } error:^(RCErrorCode nErrorCode, long messageId) {
            
        }];
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
    [self hide];
}

#pragma mark - Private methods

- (void)show {
    [self layoutIfNeeded];
    
    [UIView animateWithDuration:0.25 animations:^{
        self.contentViewBottom.constant = -self.bottomHeight + IphoneX_Bottom;
        [self layoutIfNeeded];
    }];
}

- (void)hide {
    [UIView animateWithDuration:0.25 animations:^{
        self.contentViewBottom.constant = -37.5 -self.contentViewHeight.constant;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)didSelectAction:(NSString *)action {
    [self hide];
    if ([self.delegate respondsToSelector:@selector(userInfoView:didSelectAction:toUser:)]) {
        [self.delegate userInfoView:self didSelectAction:action toUser:self.userInfo];
    }
}

- (void)clickGiftButton:(UIButton *)sender {
    [self didSelectAction:@"gift"];
}

- (void)requestFriendWithType:(NSString *)type {
    NSDictionary *params = @{@"type": type, @"to": self.userInfo.userId};
    [RequestUtils commonPostRequestUtils:[params mutableCopy] bURL:@"friend" bAnimated:NO bAnimatedViewController:[UIViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        if ([type isEqualToString:@"request"]) {
            [self showTips:SPDStringWithKey(@"请求发送成功，请等待对方回应", nil)];
        }
    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
    [self hide];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.tagArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LiveRoomUserInfoTagCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveRoomUserInfoTagCell" forIndexPath:indexPath];
    [cell showTag:self.tagArray[indexPath.row].integerValue ofUser:self.userInfo];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    LiveRoomUserInfoTag tag = self.tagArray[indexPath.row].integerValue;
    switch (tag) {
        case LiveRoomUserInfoTagFans:
            [self didSelectAction:@"fans"];
            break;
        case LiveRoomUserInfoTagWealth:
            [(LiveRoomUserInfoTagCell *)[collectionView cellForItemAtIndexPath:indexPath] flipFromTop];
            break;
        default:
            break;
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return touch.view == self;
}

#pragma mark - LiveRoomUserInfoManageViewDelegate

- (void)didSelectManage:(NSString *)manage {
    [self didSelectAction:manage];
}

#pragma mark - Setters & Getters


//- (LiveRoomUserInfoMedelView *)medalView {
//    if (!_medalView) {
//        _medalView = [LiveRoomUserInfoMedelView new];
//        [self.contentView addSubview:_medalView];
//        [_medalView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.mas_equalTo(self.genderImageView.mas_bottom).with.offset(10);
//            make.centerX.mas_equalTo(0);
//            make.height.mas_equalTo(16);
//        }];
//        self.idViewTop.constant = 35;
//    }
//    return _medalView;
//}

- (LiveRoomUserInfoProfileView *)profileView {
    if (!_profileView) {
        _profileView = [[NSBundle mainBundle] loadNibNamed:@"LiveRoomUserInfoProfileView" owner:self options:nil].firstObject;
        [self.contentView addSubview:_profileView];
        [_profileView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.idView.mas_bottom).with.offset(9);
            make.centerX.mas_equalTo(0);
            make.leading.mas_greaterThanOrEqualTo(45);
        }];
    }
    return _profileView;
}

- (UICollectionView *)tagCollectionView {
    if (!_tagCollectionView) {
        UICollectionViewFlowLayout *flowLayout = [UICollectionViewFlowLayout new];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        flowLayout.itemSize = CGSizeMake((kScreenW - 18 * 2 - 13.5 * 2) / 3, 41);
        flowLayout.minimumLineSpacing = 13.5;
        CGFloat inset = (kScreenW - flowLayout.itemSize.width * self.tagArray.count - flowLayout.minimumLineSpacing * (self.tagArray.count - 1)) / 2;
        flowLayout.sectionInset = UIEdgeInsetsMake(0, inset, 0, inset);
        _tagCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _tagCollectionView.backgroundColor = [UIColor whiteColor];
        _tagCollectionView.dataSource = self;
        _tagCollectionView.delegate = self;
        [_tagCollectionView registerNib:[UINib nibWithNibName:@"LiveRoomUserInfoTagCell" bundle:nil] forCellWithReuseIdentifier:@"LiveRoomUserInfoTagCell"];
        [self.contentView addSubview:_tagCollectionView];
        [_tagCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_profileView ? _profileView.mas_bottom : self.idView.mas_bottom).with.offset(15);
            make.leading.and.trailing.mas_equalTo(0);
            make.height.mas_equalTo(41);
        }];
    }
    return _tagCollectionView;
}

- (LiveRoomUserInfoContributionView *)contributionView {
    if (!_contributionView) {
        _contributionView = [[NSBundle mainBundle] loadNibNamed:@"LiveRoomUserInfoContributionView" owner:self options:nil].firstObject;
        [_contributionView addTarget:self action:@selector(clickContributionView:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_contributionView];
        [_contributionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.tagCollectionView.mas_bottom).with.offset(10);
            make.leading.mas_equalTo(18);
            make.trailing.mas_equalTo(-18);
            make.height.mas_equalTo(53);
        }];
    }
    return _contributionView;
}

- (LiveRoomUserInfoActionView *)actionView {
    if (!_actionView) {
        _actionView = [[NSBundle mainBundle] loadNibNamed:@"LiveRoomUserInfoActionView" owner:self options:nil].firstObject;
        [_actionView.mentionButton addTarget:self action:@selector(clickMentionButton:) forControlEvents:UIControlEventTouchUpInside];
        [_actionView.chatButton addTarget:self action:@selector(clickChatButton:) forControlEvents:UIControlEventTouchUpInside];
        [_actionView.friendButton addTarget:self action:@selector(clickFriendButton:) forControlEvents:UIControlEventTouchUpInside];
        [_actionView.downMicButton addTarget:self action:@selector(clickMeDownMicButton:) forControlEvents:UIControlEventTouchUpInside];
        [_actionView.muteButton addTarget:self action:@selector(clickMuteButton:) forControlEvents:UIControlEventTouchUpInside];
        [_actionView.giftButton addTarget:self action:@selector(clickGiftButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_actionView];
        [_actionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.medalView ? self.medalView.mas_bottom : self.tagCollectionView.mas_bottom).with.offset(8);
            make.leading.and.trailing.mas_equalTo(0);
            make.height.mas_equalTo(50.5);
        }];
    }
    return _actionView;
}

- (LiveRoomUserInfoMedalView *)medalView {
    if (!_medalView) {
        _medalView = [[LiveRoomUserInfoMedalView alloc]initWithFrame:CGRectZero];
        _medalView.userId = self.userInfo.userId;
        _medalView.name = self.userInfo.name;
    }
    return _medalView;
}

- (NSMutableArray *)tagArray {
    if (!_tagArray) {
        _tagArray = [NSMutableArray array];
    }
    return _tagArray;
}

- (NSMutableArray *)manageArray {
    if (!_manageArray) {
        _manageArray = [NSMutableArray array];
    }
    return _manageArray;
}

@end
