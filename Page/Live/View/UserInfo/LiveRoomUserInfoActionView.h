//
//  LiveRoomUserInfoActionView.h
//  SimpleDate
//
//  Created by 李楠 on 2020/5/12.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveRoomUserInfoActionView : UIView

@property (weak, nonatomic) IBOutlet UIButton *mentionButton;
@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *friendButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *friendButtonTraining;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (weak, nonatomic) IBOutlet UIButton *giftButton;
@property (weak, nonatomic) IBOutlet UIButton *downMicButton;
@property (weak, nonatomic) IBOutlet UIButton *muteButton;

@property(nonatomic, assign) BOOL isMeOnMic;

@end

NS_ASSUME_NONNULL_END
