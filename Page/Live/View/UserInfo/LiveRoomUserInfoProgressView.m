//
//  LiveRoomUserInfoProgressView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/5/15.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveRoomUserInfoProgressView.h"

@interface LiveRoomUserInfoProgressView ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trailingConstraint;

@end

@implementation LiveRoomUserInfoProgressView

- (void)setProgress:(float)progress animated:(BOOL)animated {
    if (animated) {
        [UIView animateWithDuration:1 delay:0 usingSpringWithDamping:0.7 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.trailingConstraint.constant = -self.bounds.size.width * progress;
            [self layoutIfNeeded];
        } completion:^(BOOL finished) {
            
        }];
    } else {
        self.trailingConstraint.constant = -self.bounds.size.width * progress;
        [self layoutIfNeeded];
    }
}

@end
