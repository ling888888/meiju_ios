//
//  LiveRoomUserInfoMedelView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/3/30.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveRoomUserInfoMedelView.h"
#import "LiveUserModel.h"
#import "LY_FansLevelView.h"

@interface LiveRoomUserInfoMedelView ()

@property (nonatomic, strong) UILabel *anchorLabel;
@property (nonatomic, strong) LY_FansLevelView *fansImageView;
@property (nonatomic, strong) UIImageView *nobleImageView;
@property (nonatomic, strong) UIImageView *vipImageView;

@end

@implementation LiveRoomUserInfoMedelView

- (void)setUserInfo:(LiveUserModel *)userInfo {
    _userInfo = userInfo;
    
    if (_userInfo.isAnchor) {
        [self addSubview:self.anchorLabel];
        [self.anchorLabel sizeToFit];
        CGFloat labelWidth = self.anchorLabel.bounds.size.width + 11;
        [self.anchorLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.leading.and.trailing.mas_equalTo(0);
            make.centerY.mas_equalTo(self.mas_centerY);
            make.width.mas_equalTo(labelWidth);
            make.height.mas_equalTo(16);
        }];
    } else {
        [self addSubview:self.fansImageView];
        [self.fansImageView setLevel:_userInfo.fansLevel.integerValue andName:_userInfo.fansName];
        [self.fansImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.leading.and.trailing.mas_equalTo(0);
            make.centerY.mas_equalTo(self.mas_centerY);
            make.height.mas_equalTo(15);
        }];
    }
}

- (UILabel *)anchorLabel {
    if (!_anchorLabel) {
        _anchorLabel = [UILabel new];
        _anchorLabel.backgroundColor = [UIColor colorWithHexString:@"#6A2CF7"];
        _anchorLabel.layer.cornerRadius = 8;
        _anchorLabel.clipsToBounds = YES;
        _anchorLabel.localizedKey = @"主播";
        _anchorLabel.font = [UIFont systemFontOfSize:10];
        _anchorLabel.textColor = [UIColor whiteColor];
        _anchorLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _anchorLabel;
}

- (LY_FansLevelView *)fansImageView {
    if (!_fansImageView) {
        _fansImageView = [LY_FansLevelView new];
    }
    return _fansImageView;
}

- (UIImageView *)nobleImageView {
    if (!_nobleImageView) {
        _nobleImageView = [UIImageView new];
        _nobleImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_nobleImageView];
    }
    return _nobleImageView;
}

- (UIImageView *)vipImageView {
    if (!_vipImageView) {
        _vipImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_tongyong_biaozhi_vip"]];
        _vipImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_vipImageView];
    }
    return _vipImageView;
}

@end
