//
//  LiveRoomUserInfoTagCell.m
//  SimpleDate
//
//  Created by 李楠 on 2020/5/11.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveRoomUserInfoTagCell.h"
#import "LiveUserModel.h"
#import "LiveRoomUserInfoProgressView.h"

@interface LiveRoomUserInfoTagCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;

@property (nonatomic, strong) CAGradientLayer *gradientLayer;
@property (nonatomic, assign) LiveRoomUserInfoTag tagType;
@property (nonatomic, strong) LiveUserModel *userInfo;
@property (nonatomic, strong) LiveRoomUserInfoProgressView *progressView;

@end

@implementation LiveRoomUserInfoTagCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.gradientLayer = [CAGradientLayer layer];
    self.gradientLayer.cornerRadius = 5.5;
    if (kIsMirroredLayout) {
        self.gradientLayer.startPoint = CGPointMake(1, 0.5);
        self.gradientLayer.endPoint = CGPointMake(0, 0.5);
    } else {
        self.gradientLayer.startPoint = CGPointMake(0, 0.5);
        self.gradientLayer.endPoint = CGPointMake(1, 0.5);
    }
    [self.layer insertSublayer:self.gradientLayer atIndex:0];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.gradientLayer.frame = self.bounds;
}

- (void)showTag:(LiveRoomUserInfoTag)tag ofUser:(LiveUserModel *)userInfo {
    self.tagType = tag;
    self.userInfo = userInfo;
    
    switch (self.tagType) {
        case LiveRoomUserInfoTagHeart:
            self.gradientLayer.colors = @[(__bridge id)[UIColor colorWithHexString:@"#9853FF"].CGColor,
                                          (__bridge id)[UIColor colorWithHexString:@"#DD8FFF"].CGColor];
            self.imageView.image = [UIImage imageNamed:@"room_user_info_tag_heart"];
            self.titleLabel.localizedKey = @"心动值";
            self.numberLabel.text = [SPDCommonTool transformIntegerToBriefStr:self.userInfo.heartRange.integerValue];
            break;
        case LiveRoomUserInfoTagFans:
            self.gradientLayer.colors = @[(__bridge id)[UIColor colorWithHexString:@"#FF6B93"].CGColor,
                                          (__bridge id)[UIColor colorWithHexString:@"#FF9577"].CGColor];
            self.imageView.image = [UIImage imageNamed:@"room_user_info_tag_fans"];
            self.titleLabel.localizedKey = @"粉丝团";
            self.numberLabel.text = [NSString stringWithFormat:SPDLocalizedString(@"%ld人"), self.userInfo.fansNum.integerValue];
            break;
        case LiveRoomUserInfoTagWealth:
            self.titleLabel.localizedKey = @"财富等级";
            self.numberLabel.text = self.userInfo.wealthLevel;
            if (self.userInfo.wealthLevel.integerValue > 10) {
                self.gradientLayer.colors = @[(__bridge id)[UIColor colorWithHexString:@"#F2AE53"].CGColor,
                                              (__bridge id)[UIColor colorWithHexString:@"#FFD767"].CGColor];
                self.imageView.image = [UIImage imageNamed:@"room_user_info_tag_wealth3"];
            } else if (self.userInfo.wealthLevel.integerValue > 5) {
                self.gradientLayer.colors = @[(__bridge id)[UIColor colorWithHexString:@"#6E56FF"].CGColor,
                                              (__bridge id)[UIColor colorWithHexString:@"#5591FF"].CGColor];
                self.imageView.image = [UIImage imageNamed:@"room_user_info_tag_wealth2"];
            } else {
                self.gradientLayer.colors = @[(__bridge id)[UIColor colorWithHexString:@"#22BBB9"].CGColor,
                                              (__bridge id)[UIColor colorWithHexString:@"#45E994"].CGColor];
                self.imageView.image = [UIImage imageNamed:@"room_user_info_tag_wealth1"];
            }
            break;
        default:
            break;
    }
}

- (void)flipFromTop {
    [UIView transitionWithView:self duration:0.5 options:UIViewAnimationOptionTransitionFlipFromTop animations:^{
        self.progressView.hidden = !self.progressView.hidden;
    } completion:^(BOOL finished) {
        if (!self.progressView.hidden) {
            CGFloat total = self.userInfo.nextLevelNum.floatValue;
            CGFloat current = total - self.userInfo.upgradeNum.floatValue;
            [self.progressView setProgress:current / total animated:YES];
        } else {
            [self.progressView setProgress:0 animated:NO];
        }
    }];
}

- (LiveRoomUserInfoProgressView *)progressView {
    if (!_progressView) {
        _progressView = [[NSBundle mainBundle] loadNibNamed:@"LiveRoomUserInfoProgressView" owner:self options:nil].firstObject;
        _progressView.hidden = YES;
        _progressView.numberLabel.text = self.userInfo.upgradeNum;
        if (self.userInfo.wealthLevel.integerValue > 10) {
            _progressView.trackImageView.backgroundColor = [UIColor colorWithHexString:@"#FFB558"];
            _progressView.progressImageView.image = [UIImage imageNamed:@"room_user_info_progress_wealth3"];
        } else if (self.userInfo.wealthLevel.integerValue > 5) {
            _progressView.trackImageView.backgroundColor = [UIColor colorWithHexString:@"#6E56FF"];
            _progressView.progressImageView.image = [UIImage imageNamed:@"room_user_info_progress_wealth2"];
        } else {
            _progressView.trackImageView.backgroundColor = [UIColor colorWithHexString:@"#24BDB7"];
            _progressView.progressImageView.image = [UIImage imageNamed:@"room_user_info_progress_wealth1"];
        }
        [self addSubview:_progressView];
        [_progressView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(0);
        }];
    }
    return _progressView;
}

@end
