//
//  LiveRoomUserInfoManageView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/5/14.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveRoomUserInfoManageView.h"
#import "LiveRoomUserInfoManageViewCell.h"

@interface LiveRoomUserInfoManageView ()<UICollectionViewDataSource, UICollectionViewDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;

@property (nonatomic, assign) CGFloat contentHeight;
@property (nonatomic, assign) CGFloat bottomHeight;
@property (nonatomic, strong) NSMutableArray *manageArray;

@end

@implementation LiveRoomUserInfoManageView

- (void)awakeFromNib {
    [super awakeFromNib];
        
    self.contentHeight = 115.5;
    self.bottomHeight = 100;
    self.contentViewBottom.constant = -self.contentHeight - self.bottomHeight;
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"LiveRoomUserInfoManageViewCell" bundle:nil] forCellWithReuseIdentifier:@"LiveRoomUserInfoManageViewCell"];
    [self layoutIfNeeded];
}

#pragma mark - Public methods

- (void)showWithManageArray:(NSMutableArray *)manageArray {
    self.manageArray = manageArray;
    CGFloat space = (kScreenW - self.manageArray.count * 65)/ (self.manageArray.count + 1)*1.0f;
    self.flowLayout.minimumLineSpacing = space;
    self.flowLayout.sectionInset = UIEdgeInsetsMake(0, space, 0, space);
    self.flowLayout.itemSize = CGSizeMake(65, self.collectionView.bounds.size.height);
    
    [self.collectionView reloadData];
    
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        self.contentViewBottom.constant = -self.bottomHeight + IphoneX_Bottom;
        [self layoutIfNeeded];
    }];
}

#pragma mark - Event responses

- (IBAction)tapBackground:(UITapGestureRecognizer *)sender {
    [self hide];
}

#pragma mark - Private methods

- (void)hide {
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
        self.contentViewBottom.constant = -self.contentHeight - self.bottomHeight;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.manageArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LiveRoomUserInfoManageViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveRoomUserInfoManageViewCell" forIndexPath:indexPath];
    NSDictionary *dic = self.manageArray[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"room_user_info_%@", dic[@"action"]]];
    cell.label.text = SPDLocalizedString(dic[@"name"]);
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(didSelectManage:)]) {
        [self.delegate didSelectManage:self.manageArray[indexPath.row][@"action"]];
    }
    [self hide];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ![touch.view isDescendantOfView:self.contentView];
}

@end
