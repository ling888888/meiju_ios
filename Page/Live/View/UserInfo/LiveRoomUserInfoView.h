//
//  LiveRoomUserInfoView.h
//  SimpleDate
//
//  Created by 李楠 on 2020/3/19.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LiveRoomUserInfoView, LiveModel, LiveUserModel;

@protocol LiveRoomUserInfoViewDelegate <NSObject>

@optional

- (void)userInfoView:(LiveRoomUserInfoView *)view didClickUserAvatar:(LiveUserModel *)userInfo;
- (void)userInfoView:(LiveRoomUserInfoView *)view didSelectAction:(NSString *)action toUser:(LiveUserModel *)userInfo;

@end

@interface LiveRoomUserInfoView : UIView

- (void)showWithUserId:(NSString *)userId liveInfo:(LiveModel *)liveInfo isAnchor:(BOOL)isAnchor;

@property (nonatomic, weak) id<LiveRoomUserInfoViewDelegate> delegate;

@property (nonatomic, assign) NSInteger currentPositionWeight;// 当前使用手机用户的权重 【180 100 20】

@end

NS_ASSUME_NONNULL_END
