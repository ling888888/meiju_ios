//
//  LiveFreeGiftCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/4/22.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveFreeGiftCell : UICollectionViewCell

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, copy) NSDictionary * dict;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;


- (void)updateSecond:(NSInteger)second;

@end

NS_ASSUME_NONNULL_END
