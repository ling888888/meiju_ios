//
//  LY_LiveFollowTableViewCell.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_TableViewCell.h"
#import "LY_LiveChiefAnnouncer.h"
#import "LY_Fans.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_LiveFollowTableViewCell : LY_TableViewCell

@property(nonatomic, strong) LY_LiveChiefAnnouncer *chiefAnnouncer;
@property (nonatomic, strong) LY_Fans *fans;
@end

NS_ASSUME_NONNULL_END
