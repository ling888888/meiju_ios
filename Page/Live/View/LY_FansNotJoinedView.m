//
//  LY_FansNotJoinedView.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/20.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_FansNotJoinedView.h"
#import "KKPaddingLabel.h"


@interface LY_FansNotJoinedView () <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) KKPaddingLabel *topTextLabel;

@property(nonatomic, strong) UIImageView *topLeadingLineView;

@property(nonatomic, strong) UIImageView *topTrailingLineView;

@property(nonatomic, strong) UITableView *tableView;

@property(nonatomic, strong) UIView *bottomBgView;

@property(nonatomic, strong) UILabel *limitedTimeLabel;

@property(nonatomic, strong) UIImageView *conchIconView;

@property(nonatomic, strong) UILabel *conchNumberLabel;

@property(nonatomic, strong) UILabel *joinLabel;

@end

@implementation LY_FansNotJoinedView

- (UIImageView *)topLeadingLineView {
    if (!_topLeadingLineView) {
        _topLeadingLineView = [[UIImageView alloc] init];
        UIImage *leadingLineImg = [[UIImage alloc] initWithGradient:@[[UIColor whiteColor], [UIColor colorWithHexString:@"#FF6B93"]] size:CGSizeMake(120, 1) direction:UIImageGradientColorsDirectionHorizontal];
        _topLeadingLineView.image = leadingLineImg.adaptiveRtl;
    }
    return _topLeadingLineView;
}

- (UIImageView *)topTrailingLineView {
    if (!_topTrailingLineView) {
        _topTrailingLineView = [[UIImageView alloc] init];
        UIImage *leadingLineImg = [[UIImage alloc] initWithGradient:@[[UIColor colorWithHexString:@"#FF6B93"], [UIColor whiteColor]] size:CGSizeMake(120, 1) direction:UIImageGradientColorsDirectionHorizontal];
        _topTrailingLineView.image = leadingLineImg.adaptiveRtl;
    }
    return _topTrailingLineView;
}

- (KKPaddingLabel *)topTextLabel {
    if (!_topTextLabel) {
        _topTextLabel = [[KKPaddingLabel alloc] init];
        _topTextLabel.padding = UIEdgeInsetsMake(0, 10, 0, 10);
        _topTextLabel.textColor = [UIColor colorWithHexString:@"#FF6B93"];
        _topTextLabel.font = [UIFont mediumFontOfSize:18];
        _topTextLabel.text = @"入团特权".localized;
    }
    return _topTextLabel;
}

- (UIView *)bottomBgView {
    if (!_bottomBgView) {
        _bottomBgView = [[UIView alloc] init];
        _bottomBgView.backgroundColor = [UIColor whiteColor];
    }
    return _bottomBgView;
}

- (UIButton *)joinBtn {
    if (!_joinBtn) {
        _joinBtn = [[UIButton alloc] init];
        [_joinBtn setTitle:@"立即加入".localized forState:UIControlStateNormal];
        [_joinBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _joinBtn.titleLabel.font = [UIFont mediumFontOfSize:15];
        _joinBtn.layer.cornerRadius = 15;
        _joinBtn.layer.masksToBounds = true;
        _joinBtn.backgroundColor = [UIColor colorWithHexString:@"#FF6B93"];
        _joinBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 15);
        [_joinBtn addTarget:self action:@selector(joinBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _joinBtn;
}

- (UILabel *)limitedTimeLabel {
    if (!_limitedTimeLabel) {
        _limitedTimeLabel = [[UILabel alloc] init];
        _limitedTimeLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _limitedTimeLabel.font = [UIFont mediumFontOfSize:15];
        _limitedTimeLabel.text = @"限时".localized;
    }
    return _limitedTimeLabel;
}

- (UIImageView *)conchIconView {
    if (!_conchIconView) {
        _conchIconView = [[UIImageView alloc] init];
        _conchIconView.image = [UIImage imageNamed:@"ic_zhibo_fensituan_beike"];
    }
    return _conchIconView;
}

- (UILabel *)conchNumberLabel {
    if (!_conchNumberLabel) {
        _conchNumberLabel = [[UILabel alloc] init];
        _conchNumberLabel.textColor = [UIColor colorWithHexString:@"#FE85A6"];
        _conchNumberLabel.font = [UIFont mediumFontOfSize:15];
    }
    return _conchNumberLabel;
}

- (UILabel *)joinLabel {
    if (!_joinLabel) {
        _joinLabel = [[UILabel alloc] init];
        _joinLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _joinLabel.font = [UIFont mediumFontOfSize:15];
        _joinLabel.text = @"加入".localized;
    }
    return _joinLabel;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] init];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.rowHeight = 75;
        _tableView.scrollEnabled = false;
        [_tableView registerClass:[LY_FansNotJoinedTableViewCell class] forCellReuseIdentifier:@"FansNotJoinedIdentifier"];
    }
    return _tableView;
}

- (NSArray *)bgImageNameArray {
    return @[@"img_zhibo_fensituantequan_zhuanshuxunzhang",
             @"img_zhibo_fensituantequan_fensidanmu",
             @"img_zhibo_fensituantequan_jinchangtexiao"];
}

- (NSArray *)titleArray {
    return @[@"粉丝专属勋章", @"粉丝弹幕", @"粉丝进场特效"];
}

- (NSArray *)descArray {
    return @[@"亲密粉丝唯一标识", @"发言最闪亮", @"刷足存在感"];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.topTextLabel];
    [self addSubview:self.topLeadingLineView];
    [self addSubview:self.topTrailingLineView];
    [self addSubview:self.bottomBgView];
    [self.bottomBgView addSubview:self.joinBtn];
    [self.bottomBgView addSubview:self.limitedTimeLabel];
    [self.bottomBgView addSubview:self.conchIconView];
    [self.bottomBgView addSubview:self.conchNumberLabel];
    [self.bottomBgView addSubview:self.joinLabel];
    
    [self addSubview:self.tableView];
    
    self.conchNumberLabel.text = @"277";
}

- (void)setupUIFrame {
    [self.topTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(25);
        make.width.mas_greaterThanOrEqualTo(80);
    }];
    [self.topLeadingLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(1);
        make.leading.mas_equalTo(13);
        make.trailing.mas_equalTo(self.topTextLabel.mas_leading).offset(-10);
        make.centerY.mas_equalTo(self.topTextLabel);
    }];
    [self.topTrailingLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(1);
        make.trailing.mas_equalTo(-13);
        make.leading.mas_equalTo(self.topTextLabel.mas_trailing).offset(10);
        make.centerY.mas_equalTo(self.topTextLabel);
    }];
    [self.bottomBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.bottom.trailing.mas_equalTo(self);
        make.height.mas_equalTo(50);
    }];
    [self.joinBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(30);
        make.trailing.mas_equalTo(-10);
        make.centerY.mas_equalTo(self.bottomBgView);
    }];
    [self.limitedTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(10);
        make.centerY.mas_equalTo(self.bottomBgView);
    }];
    [self.conchIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(15, 12));
        make.centerY.mas_equalTo(self.bottomBgView);
        make.leading.mas_equalTo(self.limitedTimeLabel.mas_trailing).offset(2);
    }];
    [self.conchNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.bottomBgView);
        make.leading.mas_equalTo(self.conchIconView.mas_trailing).offset(2);
    }];
    [self.joinLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.bottomBgView);
        make.leading.mas_equalTo(self.conchNumberLabel.mas_trailing).offset(2);
    }];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topTextLabel.mas_bottom).offset(5);
        make.leading.trailing.mas_equalTo(self);
        make.bottom.mas_equalTo(self.bottomBgView.mas_top);
    }];
}

- (void)setNeedConchNumber:(NSInteger)needConchNumber {
    _needConchNumber = needConchNumber;
    
//    self.conchNumberLabel.text = [NSString stringWithFormat:@"%ld", needConchNumber];
}

- (void)joinBtnAction {
    if ([self.delegate respondsToSelector:@selector(fansNotJoinedViewClickJoinBtn:)]) {
        [self.delegate fansNotJoinedViewClickJoinBtn:self.joinBtn];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LY_FansNotJoinedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FansNotJoinedIdentifier" forIndexPath:indexPath];
    
    cell.bgImageName = self.bgImageNameArray[indexPath.row];
    cell.title = self.titleArray[indexPath.row];
    cell.desc = self.descArray[indexPath.row];
    
    return cell;
}

@end

@interface LY_FansNotJoinedTableViewCell ()

@property(nonatomic, strong) UIImageView *bgImgView;

@property(nonatomic, strong) UILabel *titleLabel;

@property(nonatomic, strong) UILabel *descLabel;

@end


@implementation LY_FansNotJoinedTableViewCell

- (UIImageView *)bgImgView {
    if (!_bgImgView) {
        _bgImgView = [[UIImageView alloc] init];
        _bgImgView.contentMode = UIViewContentModeScaleAspectFill;
        _bgImgView.layer.cornerRadius = 10;
        _bgImgView.layer.masksToBounds = true;
    }
    return _bgImgView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.font = [UIFont mediumFontOfSize:18];
    }
    return _titleLabel;
}

- (UILabel *)descLabel {
    if (!_descLabel) {
        _descLabel = [[UILabel alloc] init];
        _descLabel.textColor = [UIColor whiteColor];
        _descLabel.font = [UIFont mediumFontOfSize:12];
    }
    return _descLabel;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.bgImgView];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.descLabel];
}

- (void)setupUIFrame {
    [self.bgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(5, 10, 5, 10));
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgImgView).offset(12);
        make.leading.mas_equalTo(self.bgImgView).offset(15);
    }];
    [self.descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.bgImgView).offset(-12);
        make.leading.mas_equalTo(self.bgImgView).offset(15);
    }];
}

- (void)setBgImageName:(NSString *)bgImageName {
    _bgImageName = bgImageName;
    
    self.bgImgView.image = [UIImage imageNamed:bgImageName].adaptiveRtl;
}

- (void)setTitle:(NSString *)title {
    _title = title;
    
    self.titleLabel.text = title.localized;
}

- (void)setDesc:(NSString *)desc {
    _desc = desc;
    
    self.descLabel.text = desc.localized;
}


@end
