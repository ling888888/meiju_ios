//
//  LY_LiveFollowedMessageCell.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/26.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_LiveFollowedMessageCell.h"

@interface LY_LiveFollowedMessageCell ()

@property(nonatomic, strong) UILabel *textLabel;

@end

@implementation LY_LiveFollowedMessageCell

- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] init];
        _textLabel.textColor = [UIColor colorWithHexString:@"#FFCC5F"];
        _textLabel.font = [UIFont mediumFontOfSize:13];
        _textLabel.numberOfLines = 0;
        _textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    }
    return _textLabel;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self.baseContentView addSubview:self.textLabel];
}

- (void)setupUIFrame {
    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(BASECONTENTPADDING, BASECONTENTPADDING, BASECONTENTPADDING, BASECONTENTPADDING));
    }];
}

- (void)setMessageContent:(LY_LiveBaseMessageContent *)messageContent {
    [super setMessageContent:messageContent];
    
    LY_LiveFollowedMessage *msg = (LY_LiveFollowedMessage *)messageContent;
    
    self.textLabel.text = msg.langContent;
}
//- (void)setMessage:(LY_LiveFollowedMessage *)message {
//    _message = message;
//
//    self.messageContent = message;
//
//    [self setPortrait:message.portrait];
//
//    [self setCustomTag:message.customTag];
//
//    [self setShowAnchor:true];
//
//    [self setMedalWithUrl:message.medal];
//
//    [self setNickname:message.senderUserInfo.name];
//
//    self.wealthLevel = _message.wealthLevel;
//
//    self.contentLabel.text = message.langContent;
//
//    [self refresh];
//
//}

@end
