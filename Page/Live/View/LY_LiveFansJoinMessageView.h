//
//  LY_LiveFansJoinMessageView.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/24.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveJoinMessage.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_LiveFansJoinMessageView : UIView

@property (nonatomic, strong) LiveJoinMessage *message;

@end

NS_ASSUME_NONNULL_END
