//
//  LiveFreeGiftTimerManager.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/22.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveFreeGiftTimerManager.h"

@interface LiveFreeGiftTimerManager ()

@property (nonatomic, assign) NSInteger duration; // 倒计时时间
@property (nonatomic, strong) NSTimer * timer;

@end

@implementation LiveFreeGiftTimerManager

+ (instancetype)sharedInstance {
    static LiveFreeGiftTimerManager * manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[LiveFreeGiftTimerManager alloc]init];
    });
    return manager;
}

- (void)startWith:(NSInteger)duration {
    self.duration = duration;
    if (!_timer) {
        _timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(refresh:) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
        [_timer fire];
    }
}

- (void)refresh:(NSTimer *)timer {
    if (_duration == 0) {
        [_timer  invalidate];
        _timer = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LiveFreeGiftTimerManager" object:@(_duration)];  // 发通知 结束的通知
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LiveFreeGiftTimerManager" object:@(_duration)];  //发通知 更新duration 通知
//        NSLog(@"timer 一直在走");
        self.duration--;
    }
}

- (void)stop {
    if (_timer) {
        [_timer  invalidate];
        _timer = nil;
    }
}

@end
