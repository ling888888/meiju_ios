//
//  LY_LiveRecordTableViewCell.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LY_LiveRecord.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_LiveRecordTableViewCell : UITableViewCell

@property(nonatomic, strong) LY_LiveRecord *record;

@end

NS_ASSUME_NONNULL_END
