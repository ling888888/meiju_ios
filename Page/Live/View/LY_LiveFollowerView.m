//
//  LY_LiveFollowerView.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/26.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_LiveFollowerView.h"

@interface LY_LiveFollowerView ()

@property(nonatomic, strong) UIImageView *bgImgView;

@property(nonatomic, strong) UIImageView *imgView;

@property(nonatomic, strong) UILabel *textLabel1;

@property(nonatomic, strong) UILabel *textLabel2;

@property(nonatomic, strong) NSTimer *timer;

@property(nonatomic, strong) NSArray *portraitArray;

@property(nonatomic, assign) int currentIndex;

@end

@implementation LY_LiveFollowerView

- (UIImageView *)bgImgView {
    if (!_bgImgView) {
        _bgImgView = [[UIImageView alloc] init];
//        NSArray *colors = @[[UIColor colorWithHexString:@"#6E56FF"], [UIColor colorWithHexString:@"#5591FF"], [UIColor clearColor]];
//        CGFloat locations[3] = {0.0, 0.2, 1.0};
//        UIImage *bgImg = [[UIImage alloc] initWithGradient:colors locations:locations size:CGSizeMake(107, 40) direction:UIImageGradientColorsDirectionHorizontal];
                    
        UIImage *bgImg = [[UIImage alloc] initWithGradient:@[[UIColor colorWithHexString:@"#6E56FF"], [UIColor colorWithHexString:@"#5591FF"]] size:CGSizeMake(107, 40) direction:UIImageGradientColorsDirectionHorizontal];

        _bgImgView.image = bgImg;
    }
    return _bgImgView;
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = [[UIImageView alloc] init];
        _imgView.contentMode = UIViewContentModeScaleAspectFill;
        _imgView.userInteractionEnabled = true;
        _imgView.layer.cornerRadius = 14.5;
        _imgView.layer.masksToBounds = true;
    }
    return _imgView;
}

- (UILabel *)textLabel1 {
    if (!_textLabel1) {
        _textLabel1 = [[UILabel alloc] init];
        _textLabel1.textColor = [UIColor whiteColor];
//        _textLabel.font = [UIFont systemFontOfSize:15];
        _textLabel1.font = [UIFont systemFontOfSize:12];
    }
    return _textLabel1;
}

- (UILabel *)textLabel2 {
    if (!_textLabel2) {
        _textLabel2 = [[UILabel alloc] init];
        _textLabel2.textColor = [UIColor whiteColor];
        _textLabel2.font = [UIFont systemFontOfSize:12];
        _textLabel2.text = @"正在直播".localized;
    }
    return _textLabel2;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];

        [self setupUIFrame];
        
        self.timer = [NSTimer timerWithTimeInterval:3 target:self selector:@selector(timerAction) userInfo:nil repeats:true];
        [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
//        self.timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(timerAction) userInfo:nil repeats:true];
//        [self.timer invalidate];
        
        self.currentIndex = 0;
    }
    return self;
}

- (void)removeFromSuperview {
    [super removeFromSuperview];
    
    [self.timer invalidate];
    self.timer = nil;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (kIsMirroredLayout) {
        [self.bgImgView addRounded:UIRectCornerTopRight | UIRectCornerBottomRight withRadius:20];
    } else {
        [self.bgImgView addRounded:UIRectCornerTopLeft | UIRectCornerBottomLeft withRadius:20];
    }
}

- (void)setupUI {
    self.layer.masksToBounds = true;
    [self addSubview:self.bgImgView];
    [self addSubview:self.imgView];
    [self addSubview:self.textLabel1];
    [self addSubview:self.textLabel2];
}

- (void)setupUIFrame {
    [self.bgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
        make.width.mas_equalTo(160);
    }];
    
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(29);
        make.centerY.mas_equalTo(self);
        make.centerX.mas_equalTo(self.mas_leading).offset(20);
    }];
    
    [self.textLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(5);
        make.leading.mas_equalTo(39);
    }];
    
    [self.textLabel2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-5);
        make.leading.mas_equalTo(self.textLabel1);
    }];
    
    [self.textLabel2 setHidden:true];
    [self.textLabel1 mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(30);
    }];
    
    self.imgView.image = [UIImage imageNamed:@"ic_zhibo_wodeguanzhu"];

    NSString *str;
    UIFont *strFont;
    str = @"我的关注".localized;
    strFont = [UIFont systemFontOfSize:15];;
    self.textLabel1.text = str;
    self.textLabel1.font = strFont;
    CGSize size = [str sizeWithAttributes:@{NSFontAttributeName: strFont}];
    [self.bgImgView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(size.width + 47);
    }];
}

- (void)loadNetData {
    [LY_Network getRequestWithURL:LY_Api.live_followerLiving parameters:nil success:^(id  _Nullable response) {
        NSArray *dataArray = response[@"list"];
        
        if ([dataArray isEqualToArray:self.portraitArray]) {
            
            return;
        }
        self.portraitArray = dataArray;
        NSString *str;
        UIFont *strFont;
        if (self.portraitArray.count == 0) {
            // 没有直播用户
            [self.textLabel2 setHidden:true];
            
            [self.textLabel1 mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(30);
            }];
            
            self.imgView.image = [UIImage imageNamed:@"ic_zhibo_wodeguanzhu"];
            
            str = @"我的关注".localized;
            strFont = [UIFont systemFontOfSize:15];;
        } else {
            // 有正在直播用户
            [self.textLabel2 setHidden:false];
            
            [self.textLabel1 mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(13);
            }];
            if (!self.timer.isValid) {
                [self.timer fire];
            }
            
//            [self.imgView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:self.portraitArray[self.currentIndex]]]];
            
            str = [NSString stringWithFormat:@"已关注%d人".localized, self.portraitArray.count];
            strFont = [UIFont systemFontOfSize:12];;
        }
        
        self.textLabel1.text = str;
        self.textLabel1.font = strFont;
        CGSize size = [str sizeWithAttributes:@{NSFontAttributeName: strFont}];
        [self.bgImgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(size.width + 47);
        }];
        
//        UIImage *bgImg = [[UIImage alloc] initWithGradient:@[[UIColor colorWithHexString:@"#6E56FF"], [UIColor colorWithHexString:@"#5591FF"]] size:CGSizeMake(size.width + 47, 40) direction:UIImageGradientColorsDirectionHorizontal];
//        self.bgImgView.image = bgImg;
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

- (void)timerAction {
    if (self.portraitArray.count == 0) {
        return;
    }
    if (self.portraitArray.count == 1) {
        [self.imgView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:self.portraitArray[0]]]];
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.5 animations:^{
        weakSelf.imgView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished) {
        self.currentIndex += 1;
        if (self.currentIndex >= self.portraitArray.count) {
            self.currentIndex = 0;
        }
        [self.imgView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:self.portraitArray[self.currentIndex]]]];
        [UIView animateWithDuration:0.5 animations:^{
            weakSelf.imgView.transform = CGAffineTransformMakeScale(1.0, 1.0);
        }];
    }];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    if (CGRectContainsPoint(self.bounds, point)) {
        return self;
    } else {
        return [super hitTest:point withEvent:event];
    }
}
@end
