//
//  LiveShareView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/4/8.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LiveModel;
@interface LiveShareView : UIView

@property (nonatomic, copy) LiveModel *liveInfo;

- (void)show;

- (void)hide;

@end

NS_ASSUME_NONNULL_END
