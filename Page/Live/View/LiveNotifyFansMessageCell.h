//
//  LiveNotifyFansMessageCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/26.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <RongIMKit/RongIMKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveNotifyFansMessageCell : RCMessageCell

@end

NS_ASSUME_NONNULL_END
