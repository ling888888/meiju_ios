//
//  LY_FansNotJoinedView.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/20.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol LY_FansNotJoinedViewDelegate <NSObject>

- (void)fansNotJoinedViewClickJoinBtn:(UIButton *)btn;

@end

@interface LY_FansNotJoinedView : UIView

@property (nonatomic, weak) id<LY_FansNotJoinedViewDelegate> delegate;

@property (nonatomic, assign) NSInteger needConchNumber;

@property(nonatomic, strong) UIButton *joinBtn;

@end

@interface LY_FansNotJoinedTableViewCell : UITableViewCell

@property(nonatomic, copy) NSString *bgImageName;

@property(nonatomic, copy) NSString *title;

@property(nonatomic, copy) NSString *desc;

@end

NS_ASSUME_NONNULL_END
