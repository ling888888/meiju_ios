//
//  LiveRoomGiftTitleCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/4/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveRoomGiftTitleCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *iconView;
@property (nonatomic, assign) BOOL isSelected;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

NS_ASSUME_NONNULL_END
