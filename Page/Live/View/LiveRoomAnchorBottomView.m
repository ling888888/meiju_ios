//
//  LiveRoomAnchorBottomView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/23.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomAnchorBottomView.h"
#import "ZegoKitManager.h"
#import "UIView+RGSize.h"
#import "LiveRoomAnchorBottomViewCell.h"
#import "JXPopoverView.h"
#import "YYWebImage.h"

@interface LiveRoomAnchorBottomView ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView * collectionView;
@property (nonatomic, strong) NSMutableArray * dataArray;
@property (nonatomic, strong) JXPopoverView *popView;
@property (nonatomic, strong) JXPopoverView *pkBubbleView;

@property (nonatomic, strong) UIImageView * arrowImageView;
@property (nonatomic, strong) UIView * tipsView;
@property (nonatomic, strong) UILabel * tipsLabel;
@property (nonatomic, strong) NSTimer * timer;
@property (nonatomic, assign) int sec;

@end

@implementation LiveRoomAnchorBottomView

- (UIImageView *)arrowImageView {
    if (!_arrowImageView) {
        _arrowImageView = [UIImageView new];
        _arrowImageView.image = [UIImage imageNamed:@"img_zhibo_pk_tishijiantou"];
    }
    return _arrowImageView;
}

- (instancetype)init {
    if (self = [super init]) {
        [self initSubViews];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(linkMicUserDidChange:) name:LinkMicUserDidChangeNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [self invalidate];
    NSLog(@"%@ dealloc",@"LiveRoomAnchorBottomView");
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)initSubViews {
    self.clipsToBounds = NO;
    _sec = 0;
    [self addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    [self.collectionView reloadData];
    
    [self addSubview:self.arrowImageView];
    [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(self.mas_top).offset(-10);
        make.size.mas_equalTo(9);
    }];
    self.arrowImageView.hidden = YES;
    
    self.tipsView = [UIView new];
    self.tipsView.layer.cornerRadius = 15;
    self.tipsView.backgroundColor = [SPD_HEXCOlOR(COMMON_PINK) colorWithAlphaComponent:0.6];
    [self addSubview:self.tipsView];
    [self.tipsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(30);
        make.bottom.mas_equalTo(self.arrowImageView.mas_top).offset(0);
    }];
    self.tipsView.hidden = YES;

    self.tipsLabel = [UILabel new];
    [self.tipsView addSubview:self.tipsLabel];
    self.tipsLabel.font = [UIFont mediumFontOfSize:14];
    self.tipsLabel.textColor = [UIColor whiteColor];
    [self.tipsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(16);
        make.centerY.mas_equalTo(0);
        make.trailing.mas_equalTo(-16);
    }];
}

- (void)linkMicUserDidChange:(NSNotification *)notification {
    if (ZegoManager.linkMicUserInfoDic.count > 0) {
        self.pkStatus = LiveRoomAnchorPKInProcessUnable;
    }else{
        [self.dataArray removeAllObjects];
        self.dataArray = nil;
        self.pkStatus = LiveRoomAnchorPKDefalut;
        [self.collectionView reloadData];
    }
//    LiveRoomAnchorBottomViewCell * cell = (LiveRoomAnchorBottomViewCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
//    if (!self.showingAnchorView && ZegoManager.linkMicUnreadDic.count) {
//        cell.dotImageView.hidden = NO;// 有人呼入
//        _popView = nil;
//        JXPopoverAction * action = [JXPopoverAction actionWithTitle:@"有听众申请连麦...".localized handler:^(JXPopoverAction *action) {
//
//        }];
//        [self.popView showToView:cell.itemImageView withActions:@[action]];
//    } else {
//        [ZegoManager.linkMicUnreadDic removeAllObjects];
//        cell.dotImageView.hidden = YES;
//        [self.popView hide];
//    }
//
//    if (ZegoManager.linkMicLinkingDic.count) {
//        [cell bindCover:@"lm_anchor_linking" title:@"连麦中".localized];
//    } else {
//        [cell bindCover:@"lm_anchor_normal" title:@"连麦".localized];
//    }
}

- (void)setShowingAnchorView:(BOOL)showingAnchorView {
    _showingAnchorView = showingAnchorView;
    
    if (_showingAnchorView) {
        [ZegoManager.linkMicUnreadDic removeAllObjects];
        LiveRoomAnchorBottomViewCell * cell = (LiveRoomAnchorBottomViewCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        cell.dotImageView.hidden = YES;
        [self.popView hide];
    }
}

- (void)setPkStatus:(LiveRoomAnchorPKStatus)pkStatus {
    _pkStatus = pkStatus;
    LiveRoomAnchorBottomViewCell * cell = (LiveRoomAnchorBottomViewCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    if (self.timer.valid) {
        [self invalidate];
    }
    switch (_pkStatus) {
        case LiveRoomAnchorPKDefalut:{
            [cell bindCover:@"ic_zhibo_pk_rukou" title:@"PK"];
            cell.itemImageView.hidden = NO;
            self.tipsView.hidden = YES;
            break;
        }
        case LiveRoomAnchorPKMatchingStatus:{
            cell.anniImageView.yy_imageURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"pk_waiting" ofType:@"webp"]];
            cell.anniImageView.hidden = NO;
            cell.itemImageView.hidden = YES;
            self.arrowImageView.hidden = YES;
            self.tipsView.hidden = NO;
            self.tipsLabel.text = @"匹配中".localized;
            self.tipsView.backgroundColor = SPD_HEXCOlOR(COMMON_PINK);
            if (!_timer) {
                _timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(handleTimer:) userInfo:nil repeats:YES];
                [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
                [_timer fire];
            }
            break;
        }
        case LiveRoomAnchorPKInvitingStatus:{
            cell.anniImageView.yy_imageURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"pk_waiting" ofType:@"webp"]];
            cell.anniImageView.hidden = NO;
            cell.itemImageView.hidden = YES;
            self.arrowImageView.hidden = YES;
            self.tipsView.hidden = NO;
            self.tipsLabel.text = @"等待PK".localized;
            self.tipsView.backgroundColor = SPD_HEXCOlOR(COMMON_PINK);
            break;
        }
        case LiveRoomAnchorPKInProcessStatus:{
            self.arrowImageView.hidden = YES;
            self.tipsView.hidden = YES;
            [cell bindCover:@"ic_zhibo_pk_rukou_hui" title:@"PK"];
            break;
        }
        case LiveRoomAnchorPKInProcessUnable: {
           _dataArray =  [@[@{@"cover": @"ic_zhibo_zhubo_faxiaoxi",
              @"title": @"发消息",
              @"type": @(LiveRoomAnchorBottomViewClickedType_SendMsg)
            },
            @{@"cover": @"ic_zhibo_zhubo_yinxiao",
              @"title": @"音效",
              @"type" :@(LiveRoomAnchorBottomViewClickedType_SoundEffect)
            },@{@"cover": @"ic_zhibo_zhubo_yinyue",
              @"title": @"音乐",
                @"type": @(LiveRoomAnchorBottomViewClickedType_Music)
            },@{@"cover": @"ic_zhibo_zhubo_gongju",
              @"title": @"工具",
                @"type": @(LiveRoomAnchorBottomViewClickedType_Tool)
            }] mutableCopy];
            [self.collectionView reloadData];
        }
        default:
            break;
    }
    
}

- (void)handleTimer:(NSTimer *)timer {
    _sec ++;
    self.tipsLabel.text = [NSString stringWithFormat:@"匹配中%dS...".localized,_sec];
    if (_sec == 60) {
        [self invalidate];
        _sec = 0;
        [self showTips:@"匹配时间过长，请稍后重试".localized];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MatchTimeAutoOutCancel" object:nil];
    }
}

- (void)invalidate {
    if (self.timer.valid) {
        [self.timer invalidate];
        self.timer = nil;
        _sec = 0;
    }
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LiveRoomAnchorBottomViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveRoomAnchorBottomViewCell" forIndexPath:indexPath];
    NSDictionary * dict = self.dataArray[indexPath.row];
    [cell bindCover:dict[@"cover"] title:dict[@"title"]];
    cell.isPKItem = (indexPath.row == LiveRoomAnchorBottomViewClickedType_PK);
    if (indexPath.row == LiveRoomAnchorBottomViewClickedType_PK) {
        cell.isPKItem = YES;
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    NSDictionary * dic = self.dataArray[indexPath.row];
    NSInteger type = [dic[@"type"] integerValue];
    
    if (type == LiveRoomAnchorBottomViewClickedType_PK  && self.pkStatus == LiveRoomAnchorPKInProcessStatus) {
        return;
    }
    
    if (type == LiveRoomAnchorBottomViewClickedType_PK && ZegoManager.linkMicLinkingDic.count > 0 && self.pkStatus == LiveRoomAnchorPKDefalut) {
        [self showTips:@"连麦中，请结束后PK".localized];
        return;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveRoomAnchorBottomView:didClickedWithType:)]) {
        [self.delegate liveRoomAnchorBottomView:self didClickedWithType:type];
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    CGFloat width = (kScreenW - (self.dataArray.count)*50)/(self.dataArray.count+1);
    return  UIEdgeInsetsMake(0, width, 0, width);

}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    CGFloat width = (kScreenW - (self.dataArray.count)*50)/(self.dataArray.count+1);
    return width;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout new];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _collectionView = [[UICollectionView alloc]initWithFrame:self.bounds collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerClass:[LiveRoomAnchorBottomViewCell class] forCellWithReuseIdentifier:@"LiveRoomAnchorBottomViewCell"];
        _collectionView.backgroundColor = [UIColor clearColor];
    }
    return _collectionView;
}


- (NSMutableArray *)dataArray {
    if(!_dataArray){
        _dataArray =  [@[@{@"cover": @"ic_zhibo_zhubo_faxiaoxi",
                           @"title": @"发消息",
                           @"type": @(LiveRoomAnchorBottomViewClickedType_SendMsg)
                         },
                         @{@"cover": @"ic_zhibo_zhubo_yinxiao",
                           @"title": @"音效",
                           @"type" :@(LiveRoomAnchorBottomViewClickedType_SoundEffect)
                         },@{@"cover": @"ic_zhibo_pk_rukou",
                           @"title": @"PK",
                             @"type": @(LiveRoomAnchorBottomViewClickedType_PK)
                         },@{@"cover": @"ic_zhibo_zhubo_yinyue",
                           @"title": @"音乐",
                             @"type": @(LiveRoomAnchorBottomViewClickedType_Music)
                         },@{@"cover": @"ic_zhibo_zhubo_gongju",
                           @"title": @"工具",
                             @"type": @(LiveRoomAnchorBottomViewClickedType_Tool)
                         }] mutableCopy];
    }
    return _dataArray;
}

- (JXPopoverView *)popView {
    if (!_popView) {
        _popView = [JXPopoverView popoverView];
        _popView.style = PopoverViewStyleWhiteBgTextGreen;
        _popView.hideAfterTouchOutside = NO;
        _popView.showShade = NO;
    }
    return _popView;
}

- (JXPopoverView *)pkBubbleView {
    if (!_pkBubbleView) {
        _pkBubbleView = [JXPopoverView popoverView];
        _pkBubbleView.textColor = @"ffffff";
        _pkBubbleView.backgroundColor = [SPD_HEXCOlOR(COMMON_PINK)colorWithAlphaComponent:0.6];
        _pkBubbleView.hideAfterTouchOutside = YES;
        _pkBubbleView.showShade = NO;
    }
    return _pkBubbleView;
}

@end
