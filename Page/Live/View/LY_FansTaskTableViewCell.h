//
//  LY_FansTaskTableViewCell.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LY_FansTask.h"

NS_ASSUME_NONNULL_BEGIN

@class LY_FansTaskTableViewCell;

@protocol LY_FansTaskTableViewCellDelegate <NSObject>

- (void)fansTaskTableViewCell:(LY_FansTaskTableViewCell *)fansTaskTableViewCell clickToFinishBtn:(UIButton *)toFinishBtn;

@end

@interface LY_FansTaskTableViewCell : UITableViewCell

@property(nonatomic, weak) id<LY_FansTaskTableViewCellDelegate> delegate;

@property(nonatomic, assign) BOOL isValid;

@property(nonatomic, strong) LY_FansTask *task;

@end

NS_ASSUME_NONNULL_END
