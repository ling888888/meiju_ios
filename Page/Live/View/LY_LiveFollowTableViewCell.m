//
//  LY_LiveFollowTableViewCell.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_LiveFollowTableViewCell.h"
#import "LY_Button.h"

@interface LY_LiveFollowTableViewCell ()

@property(nonatomic, strong) LY_PortraitView *portraitView;

@property(nonatomic, strong) UILabel *nicknameLabel;

@property(nonatomic, strong) LY_Button *livingBtn;

@end

@implementation LY_LiveFollowTableViewCell

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
    }
    return _portraitView;
}

- (UILabel *)nicknameLabel {
    if (!_nicknameLabel) {
        _nicknameLabel = [[UILabel alloc] init];
        _nicknameLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _nicknameLabel.font = [UIFont mediumFontOfSize:14];
    }
    return _nicknameLabel;
}

- (LY_Button *)livingBtn {
    if (!_livingBtn) {
        _livingBtn = [[LY_Button alloc] initWithImageTitleSpace:7];
        _livingBtn.titleColor = [UIColor whiteColor];
        _livingBtn.titleFont = [UIFont mediumFontOfSize:14];
        _livingBtn.title = @"正在直播".localized;
        _livingBtn.image = [UIImage imageNamed:@"ic_zhibo_zhengzaizhibo"].adaptiveRtl;
        _livingBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
        _livingBtn.backgroundColor = [UIColor colorWithHexString:@"#00D4A0"];
        _livingBtn.layer.cornerRadius = 15;
        _livingBtn.layer.masksToBounds = true;
        [_livingBtn setUserInteractionEnabled:false];
    }
    return _livingBtn;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.portraitView];
    [self.contentView addSubview:self.nicknameLabel];
    [self.contentView addSubview:self.livingBtn];
    
//    self.portraitView.backgroundColor = [UIColor redColor];
    self.nicknameLabel.text = @"ytreswa";
}

- (void)setupUIFrame {
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(66);
        make.centerY.mas_equalTo(self.contentView);
        make.leading.mas_equalTo(5);
    }];
    
    [self.nicknameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.leading.mas_equalTo(self.portraitView.mas_trailing);
        make.trailing.mas_equalTo(self.livingBtn.mas_leading).offset(-15);
    }];
    
    [self.livingBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(self.contentView).offset(-15);
        make.centerY.mas_equalTo(self.contentView);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(95);
    }];
}

- (void)setChiefAnnouncer:(LY_LiveChiefAnnouncer *)chiefAnnouncer {
    _chiefAnnouncer = chiefAnnouncer;
    
    self.portraitView.portrait = chiefAnnouncer.portrait;
//    [self.portraitView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:chiefAnnouncer.portraitUrl]] placeholderImage:[UIImage imageNamed:@""]];
    
    self.nicknameLabel.text = chiefAnnouncer.nickname;
    
    // 判断是否正在直播
    if (chiefAnnouncer.living) {
        // 直播中
        [self.livingBtn setHidden:false];
    } else {
        [self.livingBtn setHidden:true];
    }
}

- (void)setFans:(LY_Fans *)fans {
    _fans = fans;
//    [self.portraitView fp_setImageWithURLString:_fans.avatar];
    self.portraitView.portrait = fans.portrait;
    self.nicknameLabel.text = _fans.nick_name;
    self.livingBtn.hidden = YES;
}

@end
