//
//  LY_LiveRankTableViewCell.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/2.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_LiveRankTableViewCell.h"
#import <SVGAPlayer/SVGA.h>
#import "LY_HPTagView.h"

@interface LY_LiveRankTableViewCell ()

// 排名
@property(nonatomic, strong) UIButton *rankingBtn;

// 头像
@property(nonatomic, strong) LY_PortraitView *portraitView;

// 昵称
@property(nonatomic, strong) UILabel *nicknameLabel;

// 标签视图
@property(nonatomic, strong) LY_HPTagView *tagView;
// 贵族
//@property(nonatomic, strong) UIImageView *nobleImgView;
//
//// VIP
//@property(nonatomic, strong) SVGAImageView *vipImgView;
//
//// 靓号
//@property(nonatomic, strong) UIImageView *specialNumImgView;

// 贝壳图标
@property(nonatomic, strong) UIImageView *conchIconView;

// 贝壳数
@property(nonatomic, strong) UILabel *conchNumberLabel;

@end

@implementation LY_LiveRankTableViewCell

- (UIButton *)rankingBtn {
    if (!_rankingBtn) {
        _rankingBtn = [[UIButton alloc] init];
        [_rankingBtn setTitleColor:[UIColor colorWithHexString:@"#1A1A1A"] forState:UIControlStateNormal];
        _rankingBtn.titleLabel.font = [UIFont mediumFontOfSize:15];
        
    }
    return _rankingBtn;
}

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
//        _portraitView.contentMode = UIViewContentModeScaleAspectFill;
//        _portraitView.layer.cornerRadius = 24;
//        _portraitView.layer.masksToBounds = true;
    }
    return _portraitView;
}

- (UILabel *)nicknameLabel {
    if (!_nicknameLabel) {
        _nicknameLabel = [[UILabel alloc] init];
        _nicknameLabel.textColor = [UIColor colorWithHexString:@"#333333"];
        _nicknameLabel.font = [UIFont mediumFontOfSize:15];
    }
    return _nicknameLabel;
}

- (LY_HPTagView *)tagView {
    if (!_tagView) {
        _tagView = [[LY_HPTagView alloc] initWithType:LY_HPTagViewTypeAudience];
    }
    return _tagView;
}


- (UIImageView *)conchIconView {
    if (!_conchIconView) {
        _conchIconView = [[UIImageView alloc] init];
        _conchIconView.contentMode = UIViewContentModeScaleAspectFit;
        _conchIconView.image = [UIImage imageNamed:@"ic_zhibo_liwuhe_beike_xiao"];
    }
    return _conchIconView;
}

- (UILabel *)conchNumberLabel {
    if (!_conchNumberLabel) {
        _conchNumberLabel = [[UILabel alloc] init];
        _conchNumberLabel.textColor = [UIColor colorWithHexString:@"#FF6B93"];
        _conchNumberLabel.font = [UIFont mediumFontOfSize:15];
        _conchNumberLabel.textAlignment = NSTextAlignmentRight;
    }
    return _conchNumberLabel;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.rankingBtn];
    [self.contentView addSubview:self.portraitView];
    [self.contentView addSubview:self.nicknameLabel];
    [self.contentView addSubview:self.tagView];
    [self.contentView addSubview:self.conchIconView];
    [self.contentView addSubview:self.conchNumberLabel];
}

- (void)setupUIFrame {
    [self.rankingBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(38, 42));
        make.leading.mas_equalTo(3);
    }];
    
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.leading.mas_equalTo(self.rankingBtn.mas_trailing);
        make.size.mas_equalTo(50);
    }];
    
    [self.nicknameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.portraitView);
        make.leading.mas_equalTo(self.portraitView.mas_trailing).offset(10);
    }];
    
    [self.tagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.portraitView);
        make.leading.mas_equalTo(self.portraitView.mas_trailing).offset(10);
        make.height.mas_equalTo(18);
    }];
    
    [self.conchIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.size.mas_equalTo(15);
        make.trailing.mas_equalTo(-15);
    }];
    
    [self.conchNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.trailing.mas_equalTo(self.conchIconView.mas_leading).offset(-5);
    }];
}

- (void)setLiveAudience:(LY_LiveAudience *)liveAudience {
    _liveAudience = liveAudience;
    
    // 设置头像
    self.portraitView.portrait = liveAudience.portrait;
    
    // 设置昵称
    if (liveAudience.nickname.notEmpty) {
        self.nicknameLabel.text = liveAudience.nickname;
    }
    if (liveAudience.userName.notEmpty) {
        self.nicknameLabel.text = liveAudience.userName;
    }
    
    // 自定义标签
    self.tagView.customTag = liveAudience.customTag;
    
    // 粉丝团标签
    
    
    // 判断是否是贵族
    if (liveAudience.noble && ![liveAudience.noble isEqualToString:@""]) {
        // 是贵族
        self.tagView.isNoble = true;
        self.tagView.nobleImgUrlStr = liveAudience.noble;
    } else {
        // 不是贵族
        self.tagView.isNoble = false;
    }
    
    self.tagView.wealthLevel = liveAudience.wealthLevel;

    [self.tagView refresh];
    
//    // 判断是否是贵族
//    if (liveAudience.noble && ![liveAudience.noble isEqualToString:@""]) {
//        // 是贵族
//        [self.nobleImgView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:liveAudience.noble]] placeholderImage:[UIImage imageNamed:@""]];
//        [self.nobleImgView setHidden:false];
//        [self.nobleImgView mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.leading.mas_equalTo(self.nicknameLabel.mas_trailing).offset(10);
//        }];
//    } else {
//        // 不是贵族
//        [self.nobleImgView setHidden:true];
//        [self.nobleImgView mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.leading.mas_equalTo(self.nicknameLabel.mas_trailing).offset(-15);
//        }];
//    }
//
//    // 判断是否是VIP
//    if (liveAudience.isVip) {
//        // 是VIP
//        [self.vipImgView setHidden:false];
//        [self.vipImgView mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.leading.mas_equalTo(self.nobleImgView.mas_trailing).offset(10);
//        }];
//    } else {
//        // 不是VIP
//        [self.vipImgView setHidden:true];
//        [self.vipImgView mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.leading.mas_equalTo(self.nobleImgView.mas_trailing).offset(-15);
//        }];
//    }
//
//    // 判断是否是靓号
//    if (liveAudience.specialNum && ![liveAudience.specialNum isEqualToString:@""]) {
//        // 是靓号
//        [self.specialNumImgView setHidden:false];
//        [self.specialNumImgView mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.leading.mas_equalTo(self.vipImgView.mas_trailing).offset(10);
//        }];
//    } else {
//        // 不是靓号
//        [self.specialNumImgView setHidden:true];
//        [self.specialNumImgView mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.leading.mas_equalTo(self.vipImgView.mas_trailing).offset(-15);
//        }];
//    }
    
    self.conchNumberLabel.text = [SPDCommonTool transformIntegerToBriefStr:liveAudience.conchNum];
    if (liveAudience.score != 0) {
        self.conchNumberLabel.text = [SPDCommonTool transformIntegerToBriefStr:liveAudience.score];
    }
    switch (liveAudience.ranking) {
        case 1: {
            [self.rankingBtn setTitle:nil forState:UIControlStateNormal];
            [self.rankingBtn setImage:[UIImage imageNamed:@"ic_zhibo_yonghu_guanjun"] forState:UIControlStateNormal];
            break;
        }
        case 2: {
            [self.rankingBtn setTitle:nil forState:UIControlStateNormal];
            [self.rankingBtn setImage:[UIImage imageNamed:@"ic_zhibo_yonghu_yajun"] forState:UIControlStateNormal];
            break;
        }
        case 3: {
            [self.rankingBtn setTitle:nil forState:UIControlStateNormal];
            [self.rankingBtn setImage:[UIImage imageNamed:@"ic_zhibo_yonghu_jijun"] forState:UIControlStateNormal];
            break;
        }
        default:
            [self.rankingBtn setTitle:[NSString stringWithFormat:@"%d", liveAudience.ranking] forState:UIControlStateNormal];
            [self.rankingBtn setImage:nil forState:UIControlStateNormal];
            break;
    }
}

@end
