//
//  LiveRoomAudienceBottomView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/23.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, LiveRoomAudienceBottomViewType){
    LiveRoomAudienceBottomViewType_Input = 0,
    LiveRoomAudienceBottomViewType_QB,
    LiveRoomAudienceBottomViewType_Share,
    LiveRoomAudienceBottomViewType_Gift,
    LiveRoomAudienceBottomViewType_LinkMic,
    LiveRoomAudienceBottomViewType_Play

};

@class LiveRoomAudienceBottomView;
@protocol LiveRoomAudienceBottomViewDelegate <NSObject>

@optional
- (void)liveRoomAudienceBottomView:(LiveRoomAudienceBottomView *)view didTapViewWithTag:(LiveRoomAudienceBottomViewType)type;

@end

@interface LiveRoomAudienceBottomView : UIView

@property (nonatomic, weak)id<LiveRoomAudienceBottomViewDelegate> delegate;

- (void)updateStatusIcon:(BOOL) hidden;

@end

NS_ASSUME_NONNULL_END

