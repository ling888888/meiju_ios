//
//  LY_LiveFiringMoreActionSheetView.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_LiveFiringMoreActionSheetView.h"
#import "SPButton.h"
#import "LY_Button.h"

@interface LY_LiveFiringMoreActionSheetView ()

@property(nonatomic, strong) LY_Button *walletBtn;

@property(nonatomic, strong) SPButton *recordBtn;

@end


@implementation LY_LiveFiringMoreActionSheetView

- (LY_Button *)walletBtn {
    if (!_walletBtn) {
        _walletBtn = [[LY_Button alloc] initWithImagePosition:LY_ButtonImagePositionTop imageTitleSpace:10];
        _walletBtn.titleColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _walletBtn.titleFont = [UIFont mediumFontOfSize:13];
        _walletBtn.title = @"直播钱包".localized;
        _walletBtn.image = [UIImage imageNamed:@"ic_zhibo_kaishizhibo_qianbao"];
        [_walletBtn addTarget:self action:@selector(walletBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _walletBtn;
}

- (SPButton *)recordBtn {
    if (!_recordBtn) {
        _recordBtn = [[SPButton alloc] init];
        _recordBtn.imagePosition = SPButtonImagePositionTop;
        _recordBtn.imageTitleSpace = 10;
        [_recordBtn setImage:[UIImage imageNamed:@"ic_zhibo_kaishizhibo_jilu"] forState:UIControlStateNormal];
        [_recordBtn setTitle:@"直播记录".localized forState:UIControlStateNormal];
        [_recordBtn setTitleColor:[UIColor colorWithHexString:@"#1A1A1A"] forState:UIControlStateNormal];
        _recordBtn.titleLabel.font = [UIFont mediumFontOfSize:13];
        [_recordBtn addTarget:self action:@selector(recordBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _recordBtn;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.containerView addSubview:self.walletBtn];
        [self.containerView addSubview:self.recordBtn];
        
        [self.walletBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(self.containerView);
            make.width.mas_equalTo(self.containerView).dividedBy(4);
            make.centerY.mas_equalTo(self.containerView);
            make.leading.mas_equalTo(self.containerView);
        }];
        
        [self.recordBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(self.containerView);
            make.width.mas_equalTo(self.containerView).dividedBy(4);
            make.centerY.mas_equalTo(self.containerView);
            make.leading.mas_equalTo(self.walletBtn.mas_trailing);
        }];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    if (self.presentOnLiveEndedView) {
        self.containerView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.9];
        self.containerView.layer.cornerRadius = 10;
    } else {
        [self.containerView addRounded:UIRectCornerBottomLeft | UIRectCornerBottomRight withRadius:10];
    }
}

- (CGFloat)containerHeight {
    return 100;
    
}

- (void)willPresent {
    if (self.presentOnLiveEndedView) {
        self.containerView.alpha = 0;
        self.containerView.frame = CGRectMake(15, StatusBarHeight + 42.5 - 60, kScreenW - 15 * 2, self.containerHeight);
    } else {
        self.containerView.frame = CGRectMake(0, naviBarHeight - self.containerHeight, [UIScreen mainScreen].bounds.size.width, self.containerHeight);
    }
}

- (void)animatePresent {
    if (self.presentOnLiveEndedView) {
        self.containerView.alpha = 1;
        self.containerView.frame = CGRectMake(15, StatusBarHeight + 42.5, kScreenW - 15 * 2, self.containerHeight);
    } else {
        self.containerView.frame = CGRectMake(0, naviBarHeight, [UIScreen mainScreen].bounds.size.width, self.containerHeight);
    }
}

- (void)willDismiss {
    
}

- (void)animateDismiss {
    if (self.presentOnLiveEndedView) {
        self.containerView.alpha = 0;
        self.containerView.frame = CGRectMake(15, StatusBarHeight + 42.5 - 60, kScreenW - 15 * 2, self.containerHeight);
    } else {
        self.containerView.frame = CGRectMake(0, naviBarHeight - self.containerHeight, [UIScreen mainScreen].bounds.size.width, self.containerHeight);
    }
}

- (void)walletBtnAction {
    [self dismiss];
    if ([self.delegate respondsToSelector:@selector(clickWalletBtn)]) {
        [self.delegate clickWalletBtn];
    }
}

- (void)recordBtnAction {
    [self dismiss];
    if ([self.delegate respondsToSelector:@selector(clickRecordBtn)]) {
        [self.delegate clickRecordBtn];
    }
}

@end
