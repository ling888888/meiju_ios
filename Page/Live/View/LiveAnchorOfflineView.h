//
//  LiveAnchorOfflineView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/23.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LiveModel;

@interface LiveAnchorOfflineView : UIView

@property (nonatomic, strong) LiveModel *liveInfo;

@end

NS_ASSUME_NONNULL_END
