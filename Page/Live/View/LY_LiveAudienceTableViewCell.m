//
//  LY_LiveAudienceTableViewCell.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_LiveAudienceTableViewCell.h"
#import <SVGAPlayer/SVGA.h>
#import "LY_HPTagView.h"
#import "ZegoKitManager.h"

@interface LY_LiveAudienceTableViewCell ()

// 头像
@property(nonatomic, strong) LY_PortraitView *portraitView;

// 昵称
@property(nonatomic, strong) UILabel *nicknameLabel;

// 昵称
@property(nonatomic, strong) LY_HPTagView *tagView;



@end

@implementation LY_LiveAudienceTableViewCell

- (UIButton *)inviteLinkMicButton {
    if (!_inviteLinkMicButton) {
        _inviteLinkMicButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_inviteLinkMicButton setTitle:@"邀请连麦".localized forState:UIControlStateNormal];
        [_inviteLinkMicButton setTitle:@"连麦中".localized forState:UIControlStateSelected];
        _inviteLinkMicButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_inviteLinkMicButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_inviteLinkMicButton setTitleColor:SPD_HEXCOlOR(@"6A2CF7") forState:UIControlStateSelected];
        [_inviteLinkMicButton addTarget:self action:@selector(handleInviteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _inviteLinkMicButton;
}

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
    }
    return _portraitView;
}

- (UILabel *)nicknameLabel {
    if (!_nicknameLabel) {
        _nicknameLabel = [[UILabel alloc] init];
        _nicknameLabel.textColor = [UIColor colorWithHexString:@"#333333"];
        _nicknameLabel.font = [UIFont mediumFontOfSize:15];
    }
    return _nicknameLabel;
}

- (LY_HPTagView *)tagView {
    if (!_tagView) {
        _tagView = [[LY_HPTagView alloc] initWithType:LY_HPTagViewTypeAudience];
    }
    return _tagView;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.portraitView];
    [self.contentView addSubview:self.nicknameLabel];
    [self.contentView addSubview:self.tagView];
    [self.contentView addSubview:self.inviteLinkMicButton];
}

- (void)setupUIFrame {
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.leading.mas_equalTo(5);
        make.size.mas_equalTo(50);
    }];
    
    [self.nicknameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.portraitView);
        make.leading.mas_equalTo(self.portraitView.mas_trailing).offset(10);
    }];
    
    [self.tagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.portraitView);
        make.leading.mas_equalTo(self.portraitView.mas_trailing).offset(10);
        make.height.mas_equalTo(16);
    }];
    
    [self.inviteLinkMicButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.trailing.mas_equalTo(-15);
        make.height.mas_equalTo(30);
        make.width.mas_greaterThanOrEqualTo(85);
    }];
    self.inviteLinkMicButton.layer.cornerRadius = 15;
    self.inviteLinkMicButton.clipsToBounds = YES;
    self.inviteLinkMicButton.selected = NO;
}

- (void)setLiveAudience:(LY_LiveAudience *)liveAudience {
    _liveAudience = liveAudience;
    
    // 设置头像
    self.portraitView.portrait = liveAudience.portrait;
    
    // 设置昵称
    self.nicknameLabel.text = liveAudience.nickname;
    
    // 自定义标签
    self.tagView.customTag = liveAudience.customTag;
    
    // 粉丝团标签
    
    
    // 判断是否是贵族
    if (liveAudience.noble.notEmpty) {
        // 是贵族
        self.tagView.isNoble = true;
        self.tagView.nobleImgUrlStr = liveAudience.noble;
    } else {
        // 不是贵族
        self.tagView.isNoble = false;
    }
    
    self.tagView.wealthLevel = liveAudience.wealthLevel;

    [self.tagView refresh];
    
    // 连麦状态
    
    [_inviteLinkMicButton setBackgroundColor:ZegoManager.linkMicUserInfoDic[_liveAudience.userId]? [SPD_HEXCOlOR(@"1A1A1A") colorWithAlphaComponent:0.1] :SPD_HEXCOlOR(@"#6A2CF7")];
    _inviteLinkMicButton.selected = ZegoManager.linkMicUserInfoDic[_liveAudience.userId];

}

- (void)handleInviteButtonClicked:(UIButton *)sender {
    if (!ZegoManager.linkMicUserInfoDic[_liveAudience.userId]) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(LiveAudienceTableViewCell:didClickedInvite:)]) {
            [self.delegate LiveAudienceTableViewCell:self didClickedInvite:_liveAudience];
        }
    }
}

@end
