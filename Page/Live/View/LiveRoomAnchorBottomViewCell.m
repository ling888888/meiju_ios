//
//  LiveRoomAnchorBottomViewCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/6/20.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveRoomAnchorBottomViewCell.h"

@interface LiveRoomAnchorBottomViewCell ()
@end

@implementation LiveRoomAnchorBottomViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubViews];
    }
    return self;
}


- (void)initSubViews {
    self.titleLabel = [UILabel new];
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.font = [UIFont mediumFontOfSize:12];
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.centerY.mas_equalTo(26);
    }];
    
    self.itemImageView = [[UIImageView alloc]init];
    [self.contentView addSubview:self.itemImageView];
    [self.itemImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(44);
        make.bottom.equalTo(self.titleLabel.mas_top).mas_equalTo(-8);
    }];
    
    self.anniImageView = [YYAnimatedImageView new];
    [self.contentView addSubview:self.anniImageView];
    [self.anniImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(44);
        make.bottom.equalTo(self.titleLabel.mas_top).mas_equalTo(-8);
    }];
    self.anniImageView.hidden = YES;
    
    self.dotImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lm_anchor_dot"]];
    self.dotImageView.hidden = YES;
    [self.contentView addSubview:self.dotImageView];
    [self.dotImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.itemImageView.mas_top).with.offset(0.5);
        make.trailing.mas_equalTo(self.itemImageView.mas_trailing).with.offset(-0.5);
        make.width.and.height.mas_equalTo(11);
    }];
}

- (void)bindCover:(NSString * )cover title:(NSString *)titleName {
    self.itemImageView.image = [UIImage imageNamed:cover];
    self.titleLabel.text = titleName.localized;
    self.anniImageView.hidden = YES;
}

- (void)setIsPKItem:(BOOL)isPKItem {
    if (_isPKItem || !isPKItem) {
        return;
    }
    _isPKItem = isPKItem;
    [self.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(29);
    }];
    [self.itemImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(50);
    }];
    
}

@end
