//
//  LiveRoomGiftCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LiveGiftModel;

@interface LiveRoomGiftCell : UICollectionViewCell

@property (nonatomic, strong) LiveGiftModel * giftModel;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;

@end

NS_ASSUME_NONNULL_END
