//
//  LiveAnchorOfflineView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/23.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveAnchorOfflineView.h"
#import "LiveModel.h"

@interface LiveAnchorOfflineView ()
@property (weak, nonatomic) IBOutlet UILabel *lastLiveTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *liveTimeLabel;
@property (nonatomic, strong)CAGradientLayer * gradient;

@end

@implementation LiveAnchorOfflineView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.layer.cornerRadius = 5;
    self.clipsToBounds = YES;
    [self.layer insertSublayer:self.gradient atIndex:0];
}

- (void)setLiveInfo:(LiveModel *)liveInfo {
    _liveInfo = liveInfo;
    
    self.lastLiveTimeLabel.text = liveInfo.lastTime;
    self.liveTimeLabel.text = liveInfo.duration;
}

- (CAGradientLayer *)gradient {
    if (!_gradient) {
        _gradient = [CAGradientLayer layer];
        _gradient.frame = CGRectMake(0, 0, kScreenW, 126);
        _gradient.colors = [NSArray arrayWithObjects:
                              (id)[UIColor colorWithRed:0/255.0 green:212/255.0 blue:160/255.0 alpha:0.5].CGColor,
                              (id)[UIColor colorWithRed:69/255.0 green:233/255.0 blue:148/255.0 alpha:0.5].CGColor,nil];
        _gradient.startPoint = CGPointMake(0, 0);
        _gradient.endPoint = CGPointMake(1, 0);
        _gradient.locations  = @[@(0),@(0.7)];
    }
    return _gradient;
}

@end
