//
//  LY_FansSuccessJoinAlertView.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/22.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_FansSuccessJoinAlertView.h"

@interface LY_FansSuccessJoinAlertView ()

@property(nonatomic, strong) UIImageView *iconView;

@property(nonatomic, strong) UILabel *titleLabel;

@property(nonatomic, strong) UILabel *descLabel;

@end

@implementation LY_FansSuccessJoinAlertView

- (UIImageView *)iconView {
    if (!_iconView) {
        _iconView = [[UIImageView alloc] init];
        _iconView.image = [UIImage imageNamed:@"img_fensituan_jiaruchenggong"];
    }
    return _iconView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _titleLabel.font = [UIFont mediumFontOfSize:18];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.text = @"加入成功".localized;
    }
    return _titleLabel;
}

- (UILabel *)descLabel {
    if (!_descLabel) {
        _descLabel = [[UILabel alloc] init];
        _descLabel.textColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.8];
        _descLabel.font = [UIFont mediumFontOfSize:15];
        _descLabel.text = @"恭喜您成为主播粉丝，可以享受特权了".localized;
        _descLabel.textAlignment = NSTextAlignmentCenter;
        _descLabel.numberOfLines = 0;
        _descLabel.lineBreakMode = NSLineBreakByWordWrapping;
    }
    return _descLabel;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self.containerView addSubview:self.iconView];
    [self.containerView addSubview:self.titleLabel];
    [self.containerView addSubview:self.descLabel];
}

- (void)setupUIFrame {
    [self.iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(60, 65));
        make.centerX.mas_equalTo(self.containerView);
        make.top.mas_equalTo(15);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.trailing.mas_equalTo(-15);
        make.top.mas_equalTo(self.iconView.mas_bottom).offset(18);
    }];
    [self.descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(47);
        make.trailing.mas_equalTo(-47);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(18);
        make.bottom.mas_equalTo(-38);
    }];
}

@end
