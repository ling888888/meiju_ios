//
//  LiveFreeGiftTimerManager.h
//  SimpleDate
//
//  Created by ling Hou on 2020/4/22.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveFreeGiftTimerManager : NSObject

+(instancetype)sharedInstance;
 
- (void)startWith:(NSInteger)duration; // 开始计时

-(void)stop;

@end

NS_ASSUME_NONNULL_END
