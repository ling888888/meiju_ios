//
//  LY_LiveAudienceTableViewCell.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_TableViewCell.h"
#import "LY_LiveAudience.h"

NS_ASSUME_NONNULL_BEGIN

@class LY_LiveAudienceTableViewCell;

@protocol LY_LiveAudienceTableViewCellDelegate <NSObject>

- (void)LiveAudienceTableViewCell:(LY_LiveAudienceTableViewCell *)cell didClickedInvite:(LY_LiveAudience *)liveAudience;

@end

@interface LY_LiveAudienceTableViewCell : LY_TableViewCell

@property(nonatomic, strong) LY_LiveAudience *liveAudience;

@property (nonatomic, weak)id<LY_LiveAudienceTableViewCellDelegate> delegate;

@property(nonatomic, strong) UIButton *inviteLinkMicButton; // 邀请连麦按钮


@end

NS_ASSUME_NONNULL_END
