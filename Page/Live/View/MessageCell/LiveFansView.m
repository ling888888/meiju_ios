//
//  LiveFansView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/29.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveFansView.h"

@interface LiveFansView ()

@property (nonatomic, strong) UIImageView * levelIcon;
@property (nonatomic, strong) UIImageView * bg;
@property (nonatomic, strong) UILabel * level;
@property (nonatomic, strong) UILabel * fansName;

@end

@implementation LiveFansView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubView];
    }
    return self;
}

- (void)initSubView {
    
    self.bg = [UIImageView new];
    [self addSubview:self.bg];
    [self.bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    self.levelIcon = [UIImageView new];
    [self addSubview:self.levelIcon];
    [self.levelIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(0);
        make.width.mas_equalTo(14);
        make.height.mas_equalTo(12);
        make.centerY.mas_equalTo(0);
    }];
    
    self.level = [UILabel new];
    [self addSubview:self.level];
    self.level.font = [UIFont boldSystemFontOfSize:7];
    self.level.textColor = SPD_HEXCOlOR(@"#CC72ED");
    [self.level mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.levelIcon.mas_centerX).offset(0);
        make.centerY.mas_equalTo(0);
    }];
    
    self.fansName = [[UILabel alloc]init];
    [self addSubview:self.fansName];
    self.fansName.textColor = [UIColor whiteColor];
    self.fansName.font = [UIFont mediumFontOfSize:10];
    [self.fansName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.levelIcon.mas_trailing).offset(2);
        make.centerY.mas_equalTo(0);
        make.trailing.mas_equalTo(-6);
    }];
    
}

- (void)configData:(NSString *)level fansName:(NSString *)name {
    self.levelIcon.image = [UIImage imageNamed:[self getFansIcon:level]];
    self.bg.image = [UIImage imageNamed:[self getFansbg:level]];
    self.fansName.text = name;
    self.level.text = level;
}

- (NSString *)getFansIcon:(NSString *) level {
    NSInteger fansLevel = [level integerValue];
    if (fansLevel >=1 && fansLevel <= 4) {
        return @"ic_zhibojian_fensixunzhang1-4";
    }else if (fansLevel>= 5 && fansLevel <=9){
        return @"ic_zhibojian_fensixunzhang5-9";
    }else{
        return @"ic_zhibojian_fensixunzhang";
    }
}

- (NSString *)getFansbg:(NSString *) level {
    NSInteger fansLevel = [level integerValue];
    if (fansLevel >=1 && fansLevel <= 4) {
        return @"bg_fensixunzhang1_10";
    }else if (fansLevel>= 5 && fansLevel <=9){
        return @"bg_fensixunzhang_11_20";
    }else{
        return @"bg_fensixunzhang_21_30";
    }
}


@end
