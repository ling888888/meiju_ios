//
//  LiveGiftMessageCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//  Created by ling Hou on 2020/5/9.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_BaseMessageCell.h"

NS_ASSUME_NONNULL_BEGIN

@class LiveGiftMessage;
@interface LiveGiftMessageCell : LY_BaseMessageCell

//@property (nonatomic,strong) LiveGiftMessage * giftMessage;

@end

NS_ASSUME_NONNULL_END
