//
//  LiveWealthLevelMessageCell.m
//  SimpleDate
//
//  Created by 杨宝龙 on 2020/5/22.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveWealthLevelMessageCell.h"

@interface LiveWealthLevelMessageCell ()

@property(nonatomic, strong) UILabel *textLabel;

@end

@implementation LiveWealthLevelMessageCell

- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] init];
        _textLabel.textColor = [UIColor colorWithHexString:@"#FFCC5F"];
        _textLabel.font = [UIFont mediumFontOfSize:13];
        _textLabel.numberOfLines = 0;
        _textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    }
    return _textLabel;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self.baseContentView addSubview:self.textLabel];
}

- (void)setupUIFrame {
    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(BASECONTENTPADDING, BASECONTENTPADDING, BASECONTENTPADDING, BASECONTENTPADDING));
    }];
}

- (void)setMessageContent:(LY_LiveBaseMessageContent *)messageContent {
    [super setMessageContent:messageContent];
    
    LiveWealthLevelMessage *msg = (LiveWealthLevelMessage *)messageContent;
    
    self.textLabel.text = msg.content;
    
//    if ([msg.ide)
}

//- (void)setMessage:(LiveWealthLevelMessage *)message {
//    _message = message;
//
//    self.messageContent = message;
//
////    if ([message.senderUserInfo.portraitUri containsString:@"http"]) {
////        [self setPortraitWithUrl:message.senderUserInfo.portraitUri];
////    } else {
////        [self setPortraitWithUrl:[LY_Api getCompleteResourceUrl:message.senderUserInfo.portraitUri]];
////    }
//    [self setPortrait:message.portrait];
//
//    [self setCustomTag:message.customTag];
//
////    if ([message.identity isEqualToString:@"system"]) {
////        [self setShowAnchor:false];
////    } else {
//        [self setShowAnchor:true];
////    }
//
//    [self setMedalWithUrl:message.medal];
//
//    self.wealthLevel = message.wealthLevel;
//
//    [self setNickname:message.senderUserInfo.name];
//
//    self.contentLabel.text = message.content;
//
//    [self refresh];
//}

@end
