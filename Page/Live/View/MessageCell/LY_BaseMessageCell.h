//
//  LY_BaseMessageCell.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/23.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LY_PortraitView.h"
#import "LY_HPTagView.h"
#import "LY_LiveBaseMessageContent.h"

NS_ASSUME_NONNULL_BEGIN

#define BASECONTENTPADDING 10

@interface LY_BaseMessageCell : UICollectionViewCell

// 头像
@property(nonatomic, strong) LY_PortraitView *portraitView;

// 标签
@property(nonatomic, strong) LY_HPTagView *tagView;

// 昵称
@property(nonatomic, strong) UILabel *nicknameLabel;

// 内容容器
@property(nonatomic, strong) UIView *baseContentView;

// 消息对象
@property(nonatomic, strong) LY_LiveBaseMessageContent *messageContent;



//// 更新UI
//- (void)refresh;



@end

NS_ASSUME_NONNULL_END
