//
//  LiveGiftMessageCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/5/9.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveGiftMessageCell.h"
#import "LiveRoomWealthLevelView.h"
#import "LiveFansView.h"
#import "LiveGiftMessage.h"

@interface LiveGiftMessageCell ()

// 送出
@property (nonatomic, strong) UILabel *sendOutLabel;

// 礼物名
@property (nonatomic, strong) UILabel *giftNameLabel;

// 礼物图片
@property (nonatomic, strong) UIImageView *giftImgView;

// 礼物数量
@property (nonatomic, strong) UILabel *giftNumberLabel;

@end

@implementation LiveGiftMessageCell

- (UILabel *)sendOutLabel {
    if (!_sendOutLabel) {
        _sendOutLabel = [[UILabel alloc] init];
        _sendOutLabel.textColor = [UIColor colorWithHexString:COMMON_PINK];
        _sendOutLabel.font = [UIFont boldSystemFontOfSize:13];
    }
    return _sendOutLabel;
}

- (UILabel *)giftNameLabel {
    if (!_giftNameLabel) {
        _giftNameLabel = [[UILabel alloc] init];
        _giftNameLabel.textColor = [UIColor colorWithHexString:COMMON_PINK];
        _giftNameLabel.font = [UIFont boldSystemFontOfSize:13];
    }
    return _giftNameLabel;
}

- (UIImageView *)giftImgView {
    if (!_giftImgView) {
        _giftImgView = [[UIImageView alloc] init];
        _giftImgView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _giftImgView;
}

- (UILabel *)giftNumberLabel {
    if (!_giftNumberLabel) {
        _giftNumberLabel = [[UILabel alloc] init];
        _giftNumberLabel.textColor = [UIColor colorWithHexString:COMMON_PINK];
        _giftNumberLabel.font = [UIFont boldSystemFontOfSize:13];
    }
    return _giftNumberLabel;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        [self initViews];
        
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self.baseContentView addSubview:self.sendOutLabel];
    [self.baseContentView addSubview:self.giftNameLabel];
    [self.baseContentView addSubview:self.giftImgView];
    [self.baseContentView addSubview:self.giftNumberLabel];
}

- (void)setupUIFrame {
    [self.sendOutLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(BASECONTENTPADDING);
        make.leading.mas_equalTo(BASECONTENTPADDING);
        make.bottom.mas_equalTo(-BASECONTENTPADDING);
    }];
    [self.giftNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(BASECONTENTPADDING);
        make.leading.mas_equalTo(self.sendOutLabel.mas_trailing);
        make.bottom.mas_equalTo(-BASECONTENTPADDING);
    }];
    [self.giftImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.giftNameLabel.mas_trailing);
        make.centerY.mas_equalTo(self.baseContentView);
        make.size.mas_equalTo(25);
    }];
    [self.giftNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(BASECONTENTPADDING);
        make.leading.mas_equalTo(self.giftImgView.mas_trailing);
        make.bottom.mas_equalTo(-BASECONTENTPADDING);
        make.trailing.mas_equalTo(-BASECONTENTPADDING);
    }];
}



- (void)setMessageContent:(LY_LiveBaseMessageContent *)messageContent {
    [super setMessageContent:messageContent];
    
    LiveGiftMessage *msg = (LiveGiftMessage *)messageContent;
    
    NSArray * array = [[SPDCommonTool getFamyLanguage] componentsSeparatedByString:@"-"];
    self.sendOutLabel.text = [NSString stringWithFormat:SPDLocalizedString(@"送出%@"),msg.giftName[array[0]]];
    self.giftNumberLabel.text = [NSString stringWithFormat:@"x%@",msg.num?:@""];
    [self.giftImgView sd_setImageWithURL:[NSURL URLWithString:msg.giftCover]];
}


@end
