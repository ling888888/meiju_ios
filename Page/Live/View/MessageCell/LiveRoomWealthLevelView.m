//
//  LiveRoomWealthLevelView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/29.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveRoomWealthLevelView.h"
#import "LY_GradientView.h"

@interface LiveRoomWealthLevelView ()
@property (nonatomic, strong) UIImageView * iconImage;
@property (nonatomic, strong) UILabel * levelLabel;
@property (nonatomic, strong) UIImageView * bgImageView;
@property (nonatomic, strong) LY_GradientView * gradient;
@end

@implementation LiveRoomWealthLevelView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.gradient.layer.cornerRadius = self.bounds.size.height/2.0f;
    self.gradient.clipsToBounds = YES;
}

- (void)initSubView {
    [self addSubview:self.iconImage];
    [self.iconImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.leading.mas_equalTo(2);
        make.top.mas_equalTo(1);
        make.bottom.mas_equalTo(-1);
        make.width.equalTo(self.iconImage.mas_height).multipliedBy(1.0f);
    }];
    
    [self addSubview:self.levelLabel];
    [self.levelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.iconImage.mas_trailing).mas_equalTo(4);
        make.centerY.mas_equalTo(0);
        make.trailing.mas_equalTo(-6);
    }];
    self.gradient = [[LY_GradientView alloc] initWithFrame:CGRectZero];
    [self insertSubview:self.gradient atIndex:0];
    [self.gradient mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(0);
        make.width.mas_equalTo(2);
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
}

- (void)setWealthLevel:(NSString *)wealthLevel {
    _wealthLevel = wealthLevel;
    self.levelLabel.text = _wealthLevel;
    self.iconImage.image = [self configIcon];
    [self.levelLabel sizeToFit];
    [self.gradient mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(self.levelLabel.frame.size.width + 14+ 4 + 6);
    }];
}

- (UIImage *)configIcon{
    if ([self.wealthLevel integerValue] <= 5) {
        self.gradient.colors = @[SPD_HEXCOlOR(@"#22BBB9"),SPD_HEXCOlOR(@"#45E994")];
        return [UIImage imageNamed:@"ic_caifudengji_diji"];
    }else if ([self.wealthLevel integerValue] >= 6 && [self.wealthLevel integerValue] <= 10){
        self.gradient.colors = @[SPD_HEXCOlOR(@"#6E56FF"),SPD_HEXCOlOR(@"#5591FF")];
        return [UIImage imageNamed:@"ic_caifudengji_zhongji"];
    }else{
        self.gradient.colors = @[SPD_HEXCOlOR(@"F2AE53"),SPD_HEXCOlOR(@"#FFB557"),SPD_HEXCOlOR(@"#FFD767")];
        return [UIImage imageNamed:@"ic_caifudengji_zuizhong"];
    }
}

- (UIImageView *)iconImage {
    if (!_iconImage) {
        _iconImage = [[UIImageView alloc]init];
    }
    return _iconImage;
}

- (UILabel *)levelLabel {
    if (!_levelLabel) {
        _levelLabel = [[UILabel alloc]init];
        _levelLabel.font = [UIFont boldSystemFontOfSize:11];
        _levelLabel.textColor = SPD_HEXCOlOR(@"ffffff");
        _levelLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _levelLabel;
}

- (UIImageView *)bgImageView {
    if (!_bgImageView) {
        _bgImageView = [[UIImageView alloc]init];
    }
    return _bgImageView;
}

@end
