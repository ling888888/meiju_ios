//
//  LiveRoomTextMessageCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/4/29.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LY_BaseMessageCell.h"

NS_ASSUME_NONNULL_BEGIN

@class LiveRoomTextMessage;

@interface LiveRoomTextMessageCell : LY_BaseMessageCell

//@property (nonatomic, strong) LiveRoomTextMessage * message;

@end

NS_ASSUME_NONNULL_END
