//
//  LiveUnreadMessageView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/5/8.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveUnreadMessageView.h"

@interface LiveUnreadMessageView ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation LiveUnreadMessageView

- (void)setUnreadCount:(NSInteger)unreadCount {
    _unreadCount = unreadCount;
    self.titleLabel.text = [NSString stringWithFormat:@"%@条新消息".localized,_unreadCount>99?@"99+":@(_unreadCount)];
}


@end
