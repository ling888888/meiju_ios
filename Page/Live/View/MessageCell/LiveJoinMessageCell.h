//
//  LiveJoinMessageCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/24.
//  Copyright © 2020 WYB. All rights reserved.
//  Created by ling Hou on 2020/4/29.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LiveJoinMessage;

@interface LiveJoinMessageCell : UICollectionViewCell

@property (nonatomic, strong) LiveJoinMessage * message;

@end

NS_ASSUME_NONNULL_END
