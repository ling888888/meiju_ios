//
//  LiveFansView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/4/29.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveFansView : UIView

- (void)configData:(NSString *)level fansName:(NSString *)name;

@end

NS_ASSUME_NONNULL_END
