//
//  LY_BaseMessageCell.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/23.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_BaseMessageCell.h"
#import "LiveRoomWealthLevelView.h"
#import "LY_LiveFansInviteMessage.h"

@interface LY_BaseMessageCell ()



@end

@implementation LY_BaseMessageCell

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
//        [_portraitView addTarget:self action:@selector(portraitViewAction) forControlEvents:UIControlEventTouchUpInside];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(portraitViewAction)];
        UILongPressGestureRecognizer * longPressed = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(handleLongPressed:)];
        [_portraitView addGestureRecognizer:tapRecognizer];
        [_portraitView addGestureRecognizer:longPressed];
    }
    return _portraitView;
}

- (LY_HPTagView *)tagView {
    if (!_tagView) {
        _tagView = [[LY_HPTagView alloc] initWithType:LY_HPTagViewTypeAnchor];
        
    }
    return _tagView;
}

- (UILabel *)nicknameLabel {
    if (!_nicknameLabel) {
        _nicknameLabel = [[UILabel alloc] init];
        _nicknameLabel.textColor = [UIColor whiteColor];
        _nicknameLabel.font = [UIFont mediumFontOfSize:12];
    }
    return _nicknameLabel;
}

- (UIView *)baseContentView {
    if (!_baseContentView) {
        _baseContentView = [[UIView alloc] init];
        _baseContentView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.1];
        _baseContentView.layer.cornerRadius = 10;
        _baseContentView.layer.masksToBounds = true;
    }
    return _baseContentView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self __setupUI];
        
        [self __setupUIFrame];
    }
    return self;
}

- (void)__setupUI {
    [self addSubview:self.portraitView];
    [self addSubview:self.tagView];
    [self addSubview:self.nicknameLabel];
    [self addSubview:self.baseContentView];
}

- (void)__setupUIFrame {
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(55.5);
        make.top.mas_equalTo(0);
        make.leading.mas_equalTo(5);
    }];
    
    [self.tagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.leading.mas_equalTo(self.portraitView.mas_trailing).offset(5);
        make.height.mas_equalTo(12);
    }];
    
    [self.nicknameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.tagView.mas_trailing).offset(5);
//        make.trailing.mas_lessThanOrEqualTo(-15);
        make.centerY.mas_equalTo(self.tagView);
    }];
//    [self.nicknameLabel setContentHuggingPriority:UILayoutPriorityDefaultLow
//                              forAxis:UILayoutConstraintAxisHorizontal];
//    [self.tagView setContentHuggingPriority:UILayoutPriorityRequired
//                              forAxis:UILayoutConstraintAxisHorizontal];
    [self.baseContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.tagView.mas_bottom).offset(10);
        make.leading.mas_equalTo(self.portraitView.mas_trailing).offset(5);
        make.width.mas_lessThanOrEqualTo(BASECONTENTWIDTH + 2 * BASECONTENTPADDING);
//        make.bottom.mas_equalTo(-10);
    }];
}

- (void)setMessageContent:(LY_LiveBaseMessageContent *)messageContent {
    _messageContent = messageContent;
    
    self.portraitView.portrait = messageContent.portrait;
    self.nicknameLabel.text = messageContent.senderUserInfo.name;
    self.tagView.customTag = messageContent.customTag;
    self.tagView.wealthLevel = messageContent.wealthLevel;
    
    // 判断消息发送者
    if (messageContent.userIdentity == LY_MessageSenderUserIdentityAnchor) {
        self.tagView.isAnchor = true;
        self.tagView.type = LY_HPTagViewTypeAnchor;
    } else {
        self.tagView.fansTag = messageContent.fansTag;
        self.tagView.type = LY_HPTagViewTypeAudience;
    }
    
    NSString *urlString = @"";
    if (messageContent.medalUrl.notEmpty) {
        if ([messageContent.medalUrl containsString:@"http"]) {
            urlString = messageContent.medalUrl;
        } else {
            urlString = [LY_Api getCompleteResourceUrl:messageContent.medalUrl];
        }
    }
    self.tagView.nobleImgUrlStr = urlString;
    
    // 判断消息发送人
    if (messageContent.userIdentity == LY_MessageSenderUserIdentitySystem) {
        self.tagView.isAnchor = false;
        self.tagView.fansTag = nil;
        self.tagView.type = LY_HPTagViewTypeAudience;
    } else if (messageContent.userIdentity == LY_MessageSenderUserIdentityAnchor) {
        self.tagView.type = LY_HPTagViewTypeAudience;
        self.tagView.isAnchor = true;
        self.tagView.fansTag = nil;
        self.tagView.type = LY_HPTagViewTypeAnchor;
    } else {
        self.tagView.type = LY_MessageSenderUserIdentityAudience;
        self.tagView.isAnchor = false;
        self.tagView.fansTag = messageContent.fansTag;
        self.tagView.type = LY_HPTagViewTypeAudience;
    }
    
//    if (messageContent) {
//        self.tagView.isAdmin = [[messageContent valueForKey:@"isAdmin"] boolValue];
//    }
    self.tagView.isAdmin = messageContent.isAdmin;
//
    [self.tagView refresh];
    
    if (messageContent.noble.notEmpty) {
        self.nicknameLabel.textColor = SPD_HEXCOlOR([SPDCommonTool getNobleNameColor:messageContent.noble]);
    } else {
        self.nicknameLabel.textColor = [UIColor whiteColor];
    }
}

- (void)portraitViewAction {
    if (self.messageContent.senderUserInfo.userId.notEmpty) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LiveRoomTapAvatar" object:nil userInfo:@{@"userId":self.messageContent.senderUserInfo.userId}];
    }
}

- (void)handleLongPressed:(UILongPressGestureRecognizer *)longPressed {
    if (longPressed.state == UIGestureRecognizerStateBegan) {
        NSDictionary *userInfo = @{@"user_id": _messageContent.senderUserInfo.userId ?:@"13", @"nick_name": _messageContent.senderUserInfo.name?:@"13", @"avatar": _messageContent.senderUserInfo.portraitUri?:@"13"};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LiveLongPressAvatar" object:nil userInfo:userInfo];
    }
}

@end
