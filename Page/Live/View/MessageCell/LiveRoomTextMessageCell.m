//
//  LiveRoomTextMessageCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/29.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveRoomTextMessageCell.h"
#import "LiveRoomWealthLevelView.h"
#import "LiveFansView.h"
#import "LiveRoomTextMessage.h"
#import "LiveLuckyNumberMessage.h"

@interface LiveRoomTextMessageCell ()

//@property (nonatomic, strong) UILabel * content;
//@property (nonatomic, strong) UIView * contentBgView;
//@property (nonatomic, strong) UIImageView * bubbleImageView; // 气泡的背景

// 气泡背景图
@property(nonatomic, strong) UIImageView *bubbleImgView;

// 文本
@property(nonatomic, strong) UILabel *textLabel;

@end

@implementation LiveRoomTextMessageCell

- (UIImageView *)bubbleImgView {
    if (!_bubbleImgView) {
        _bubbleImgView = [[UIImageView alloc] init];
    }
    return _bubbleImgView;
}

- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] init];
        _textLabel.textColor = [UIColor whiteColor];
        _textLabel.font = [UIFont mediumFontOfSize:13];
        _textLabel.numberOfLines = 0;
    }
    return _textLabel;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self.baseContentView addSubview:self.bubbleImgView];
    [self.baseContentView addSubview:self.textLabel];
}

- (void)setupUIFrame {
    [self.bubbleImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.baseContentView);
    }];
    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(28);
//        make.leading.mas_equalTo(75);
//        make.bottom.mas_equalTo(-9);
//        make.trailing.mas_equalTo(-15);
        make.edges.mas_equalTo(UIEdgeInsetsMake(BASECONTENTPADDING, BASECONTENTPADDING, BASECONTENTPADDING, BASECONTENTPADDING));
    }];
}

//- (void)initViews {
//    self.contentBgView = [UIView new];
//    [self addSubview:self.contentBgView];
//    [self.contentBgView setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.1]];
//    [self.contentBgView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(19);
//        make.leading.mas_equalTo(65);
//    }];
//
//    self.content = [UILabel new];
//    self.content.font = [UIFont mediumFontOfSize:13];
//    self.content.numberOfLines = 0;
//    self.content.textColor = [UIColor whiteColor];
//    [self addSubview:self.content];
//    [self.content mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.leading.equalTo(self.contentBgView.mas_leading).offset(10);
//        make.top.equalTo(self.contentBgView.mas_top).offset(9);
//        make.bottom.equalTo(self.contentBgView.mas_bottom).offset(-9);
//        make.trailing.equalTo(self.contentBgView.mas_trailing).offset(-15);
//        make.width.mas_lessThanOrEqualTo(kScreenW  - 18 - 38 - 10 - 9 - 9 - 91);
//    }];
//
//    self.bubbleImageView = [[UIImageView alloc]init];
//    [self.contentBgView addSubview:self.bubbleImageView];
//    [self.bubbleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
//    }];
//
//}

- (void)setMessageContent:(LY_LiveBaseMessageContent *)messageContent {
    [super setMessageContent:messageContent];
    
    //
    if ([messageContent isKindOfClass:[LiveLuckyNumberMessage class]]) {
        [self configLuckyMessage:(LiveLuckyNumberMessage *)messageContent];
        return;
    }
    
    LiveRoomTextMessage *textMsg = (LiveRoomTextMessage *)messageContent;
    self.textLabel.text = textMsg.content;
    
    self.textLabel.textColor = SPD_HEXCOlOR(@"ffffff");
    
    //@
    if (textMsg.mentionedType && [textMsg.mentionedType isEqualToString:@"2"] && textMsg.mentionedList) {
        for (NSDictionary *dic in textMsg.mentionedList) {
            if ([dic[@"user_id"] isEqualToString:[SPDApiUser currentUser].userId]) {
                self.textLabel.textColor = SPD_HEXCOlOR(@"FFD165");
            }
        }
        if ([textMsg.senderUserInfo.userId isEqualToString:[SPDApiUser currentUser].userId]) {
            self.textLabel.textColor = SPD_HEXCOlOR(@"FFD165");
        }
    }

    // 气泡
    if (textMsg.bubble.notEmpty) {
        self.textLabel.textColor = SPD_HEXCOlOR(textMsg.bubble[@"text_color"]);
        NSLog(@"%@", textMsg.bubble[kIsMirroredLayout ? @"usecover_ios_ar":@"usecover_ios"]);
        [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:textMsg.bubble[kIsMirroredLayout ? @"usecover_ios_ar":@"usecover_ios"]] options:SDWebImageDownloaderUseNSURLCache progress:^(NSInteger receivedSize, NSInteger expectedSize) {

        } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIImage *img = [UIImage imageWithData:data scale:2.0f];
                self.bubbleImgView.image = [self resizableImage:img];
            });
        }];
        self.baseContentView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.0];
    } else {
        self.bubbleImgView.image = nil;
        self.baseContentView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.1];
    }
    
    if (textMsg.noble.notEmpty) {
        self.nicknameLabel.textColor = SPD_HEXCOlOR([SPDCommonTool getNobleNameColor:textMsg.noble]);
    }
}

- (UIImage *)resizableImage:(UIImage *)originImage {
    CGFloat height = originImage.size.height / 2.0;
    CGFloat width = originImage.size.width / 2.0;
    UIImage *newImage = [originImage resizableImageWithCapInsets:UIEdgeInsetsMake(height-1,width-1,height,width) resizingMode:UIImageResizingModeStretch];
    return newImage;
}

- (void)configLuckyMessage:(LiveLuckyNumberMessage *)message {
    NSMutableAttributedString * str = [[NSMutableAttributedString alloc]initWithString:@"幸运数字".localized];
    [str addAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} range:NSMakeRange(0, str.length)];
    NSAttributedString * numStr = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@",message.lucky_number] attributes:@{NSForegroundColorAttributeName:SPD_HEXCOlOR(@"FFDA01"),
                                                                                NSFontAttributeName:[UIFont boldSystemFontOfSize:30],
                                                                               NSStrokeColorAttributeName:SPD_HEXCOlOR(@"865015")
    }];
    [str appendAttributedString:numStr];
    self.textLabel.attributedText = str;
    
}

@end
