//
//  LiveJoinMessageCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/29.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveJoinMessageCell.h"
#import "LiveRoomWealthLevelView.h"
#import "LiveJoinMessage.h"
#import "NSString+XXWAddition.h"

@interface LiveJoinMessageCell ()
@property (nonatomic, strong) UIView * bgView;
@property (nonatomic, strong) UILabel * wecomeLabel;
@property (nonatomic, strong) UIImageView * noble;
@property (nonatomic, strong) LiveRoomWealthLevelView * wealthLevel;
@property (nonatomic, strong) UILabel * userNameLabel;
@property (nonatomic, strong) UILabel * trailLabel;
@property (nonatomic, assign) CGFloat trailWidth;
@end

@implementation LiveJoinMessageCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubView];
    }
    return self;
}

- (void)initSubView {
    
    self.bgView = [[UIView alloc]init];
    [self addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.bottom.mas_equalTo(0);
        make.top.mas_equalTo(0);
    }];
    self.bgView.layer.cornerRadius = 12.5;
    self.bgView.clipsToBounds = YES;
    self.bgView.backgroundColor = [SPD_HEXCOlOR(@"01DA9D") colorWithAlphaComponent:0.2];
    
    self.wecomeLabel = [UILabel new];
    self.wecomeLabel.font = [UIFont systemFontOfSize:12];
    self.wecomeLabel.textColor = SPD_HEXCOlOR(@"ffffff");
    self.wecomeLabel.text = @"欢迎".localized;
    [self.bgView addSubview:self.wecomeLabel];
    [self.wecomeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(14);
        make.centerY.mas_equalTo(0);
    }];
    
    self.noble = [UIImageView new];
    [self.bgView addSubview:self.noble];
    self.noble.contentMode = UIViewContentModeScaleAspectFill;
    [self.noble mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.wecomeLabel.mas_trailing).offset(5);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(16);
        make.height.mas_equalTo(14);
    }];
    
    self.wealthLevel = [[LiveRoomWealthLevelView alloc]init];
    [self.bgView addSubview:self.wealthLevel];
    [self.wealthLevel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.wecomeLabel.mas_trailing).offset(5);
        make.centerY.mas_equalTo(0);
//        make.width.mas_equalTo(34);
        make.height.mas_equalTo(12);
    }];
    self.wealthLevel.layer.cornerRadius = 6;
    self.wealthLevel.clipsToBounds = NO;
    
   self.userNameLabel = [UILabel new];
   self.userNameLabel.font = [UIFont systemFontOfSize:12];
   self.userNameLabel.textColor = SPD_HEXCOlOR(@"ffffff");
   [self.bgView addSubview:self.userNameLabel];
   [self.userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       make.leading.equalTo(self.wecomeLabel.mas_trailing).offset(5);
       make.centerY.mas_equalTo(0);
   }];
    
    self.trailLabel = [UILabel new];
    self.trailLabel.font = [UIFont systemFontOfSize:12];
    self.trailLabel.text = @"进入直播间".localized;
    self.trailLabel.textColor = SPD_HEXCOlOR(@"ffffff");
    [self.bgView addSubview:self.trailLabel];
    [self.trailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.userNameLabel.mas_trailing).offset(5);
        make.centerY.mas_equalTo(0);
        make.trailing.mas_equalTo(-14);
    }];
    [self.trailLabel sizeToFit];
    
}

- (void)setMessage:(LiveJoinMessage *)message {
    _message = message;
    
    [self.wecomeLabel sizeToFit];
    [self.trailLabel sizeToFit];
    
    CGFloat leading = 5;
    self.noble.hidden = !_message.medal.notEmpty;
    [self.noble fp_setImageWithURLString:_message.medal];
    if (_message.medal.notEmpty) {
        [self.noble mas_updateConstraints:^(MASConstraintMaker *make) {
          make.leading.equalTo(self.wecomeLabel.mas_trailing).offset(leading);
        }];
        leading += 16+ 5;
    }
    
    if (![_message.wealthLevel isEqualToString:@"0"] && _message.wealthLevel.notEmpty) {
        self.wealthLevel.wealthLevel =_message.wealthLevel;
        [self.wealthLevel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(self.wecomeLabel.mas_trailing).offset(leading);
        }];
        [self.wealthLevel layoutIfNeeded];
        leading += CGRectGetWidth(self.wealthLevel.frame) + 5;
        self.wealthLevel.hidden = NO;
    }else{
        self.wealthLevel.hidden = YES;
    }
    
    CGFloat width = [NSString sizeWithString:@"进入直播间".localized andFont:[UIFont systemFontOfSize:12] andMaxSize:CGSizeMake(CGFLOAT_MAX, 12)].width;
    CGFloat welcomWidth = [NSString sizeWithString:@"欢迎".localized andFont:[UIFont systemFontOfSize:12] andMaxSize:CGSizeMake(CGFLOAT_MAX, 12)].width;
    
    self.userNameLabel.text = _message.userName;
    CGFloat maxW = kScreenW  - 30 - leading - width - 14 - 5 - welcomWidth - 14;
    [self.userNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.wecomeLabel.mas_trailing).offset(leading);
        make.width.mas_lessThanOrEqualTo(maxW);
        make.centerY.mas_equalTo(0);
    }];


}

@end
