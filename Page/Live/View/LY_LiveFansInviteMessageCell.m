//
//  LY_LiveFansInviteMessageCell.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/23.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_LiveFansInviteMessageCell.h"
#import "LY_LiveFansInviteMessage.h"

@interface LY_LiveFansInviteMessageCell ()

@property(nonatomic, strong) UILabel *textLabel;

@end

@implementation LY_LiveFansInviteMessageCell

- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] init];
        _textLabel.textColor = [UIColor colorWithHexString:@"#FF6B93"];
        _textLabel.font = [UIFont mediumFontOfSize:13];
        _textLabel.numberOfLines = 0;
        _textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    }
    return _textLabel;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self.baseContentView addSubview:self.textLabel];
}

- (void)setupUIFrame {
    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(BASECONTENTPADDING, BASECONTENTPADDING, BASECONTENTPADDING, BASECONTENTPADDING));
    }];
}

- (void)setMessageContent:(LY_LiveBaseMessageContent *)messageContent {
    [super setMessageContent:messageContent];
    
    LY_LiveFansInviteMessage *msg = (LY_LiveFansInviteMessage *)messageContent;
    
    if (msg.userName.notEmpty) {
        NSRange rang = [msg.content rangeOfString:msg.userName];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:msg.content];

        [attr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#FF6B93"] range:NSMakeRange(0, msg.content.length)];
        [attr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#FFCC5F"] range:rang];
        self.textLabel.attributedText = attr;
    } else {
        self.textLabel.text = msg.content;
    }
}

//- (void)setMessage:(LY_LiveFansInviteMessage *)message {
//    _message = message;
//
//    self.messageContent = message;
//
//    [self setPortrait:message.portrait];
//
//    [self setCustomTag:message.customTag];
//
//    if ([message.identity isEqualToString:@"system"]) {
//        [self setShowAnchor:false];
//    } else {
//        [self setShowAnchor:true];
//    }
//
//    [self setMedalWithUrl:message.medal];
//
//    self.wealthLevel = message.wealthLevel;
//
//    [self setNickname:message.senderUserInfo.name];
//
//    if (message.userName.notEmpty) {
//        NSRange rang = [message.content rangeOfString:message.userName];
//        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:message.content];
//
//        [attr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#FF6B93"] range:NSMakeRange(0, message.content.length)];
//        [attr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#FFCC5F"] range:rang];
//        self.contentLabel.attributedText = attr;
//    } else {
//        self.contentLabel.text = message.content;
//    }
//
//    [self refresh];
//}

@end
