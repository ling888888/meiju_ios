//
//  LiveFreeGiftIconView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/4/22.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveFreeGiftIconView : UIView

@property (nonatomic, copy) NSString * type; // 0 倒计时 1 可领取 2 全部领取成功
@property (nonatomic, copy) NSString * currentImageUrl;

- (void)updateSecond:(NSInteger) second;

@end

NS_ASSUME_NONNULL_END
