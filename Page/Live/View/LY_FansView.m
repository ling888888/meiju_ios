//
//  LY_FansView.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/20.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_FansView.h"

@interface LY_FansView ()

@property(nonatomic, strong) UIImageView *iconView;

@property(nonatomic, strong) UILabel *fansLabel;

@property(nonatomic, strong) UIImageView *moreIconView;



@end

@implementation LY_FansView


- (UIImageView *)iconView {
    if (!_iconView) {
        _iconView = [[UIImageView alloc] init];
        _iconView.contentMode = UIViewContentModeScaleAspectFit;
        _iconView.image = [UIImage imageNamed:@"ic_zhibo_yonghu_fensituan"];
    }
    return _iconView;
}

- (UILabel *)fansLabel {
    if (!_fansLabel) {
        _fansLabel = [[UILabel alloc] init];
        _fansLabel.textColor = [UIColor whiteColor];
        _fansLabel.font = [UIFont mediumFontOfSize:11];
        _fansLabel.text = [NSString stringWithFormat:@"粉丝团 %ld人".localized, 0];
    }
    return _fansLabel;
}

- (UIImageView *)moreIconView {
    if (!_moreIconView) {
        _moreIconView = [[UIImageView alloc] init];
        _moreIconView.contentMode = UIViewContentModeScaleAspectFit;
        _moreIconView.image = [UIImage imageNamed:@"ic_zhibo_zhubo_jiantou"].adaptiveRtl;
    }
    return _moreIconView;
}




- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.iconView];
    [self addSubview:self.fansLabel];
    [self addSubview:self.moreIconView];
}

- (void)setupUIFrame {
    [self.iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(22, 13.5));
        make.leading.mas_equalTo(6);
        make.centerY.mas_equalTo(self);
    }];
    
    [self.fansLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.iconView.mas_trailing).offset(5);
        make.centerY.mas_equalTo(self);
    }];
    
    [self.moreIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(6, 9));
        make.leading.mas_equalTo(self.fansLabel.mas_trailing).offset(7);
        make.trailing.mas_equalTo(-6);
        make.centerY.mas_equalTo(self);
    }];
}

- (void)setText:(NSString *)text {
    _text = text;
    
    self.fansLabel.text = text;
}

@end
