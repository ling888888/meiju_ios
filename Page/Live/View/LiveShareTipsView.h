//
//  LiveShareTipsView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/4/10.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveShareTipsView : UIView

@property (nonatomic, assign) CGFloat width;

@end

NS_ASSUME_NONNULL_END
