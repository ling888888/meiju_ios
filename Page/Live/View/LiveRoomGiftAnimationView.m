//
//  LiveRoomGiftAnimationView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/30.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomGiftAnimationView.h"
#import "LiveGiftMessage.h"
#import "SVGA.h"
#import "UIImage+LY.h"

@interface LiveRoomGiftAnimationView ()<SVGAPlayerDelegate>

@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *giftNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *giftImageView;
@property (weak, nonatomic) IBOutlet UILabel *numLabel;
@property (nonatomic,strong) NSMutableArray * effectDataArray;
@property (nonatomic,assign) BOOL isAnimating;
@property (weak, nonatomic) IBOutlet SVGAPlayer *svgaAnimationPlayer;
@property (weak, nonatomic) IBOutlet UIImageView *coverChangeImageView;
@property (nonatomic, strong) SVGAParser *parser;

@property(nonatomic, strong) LY_PortraitView *portraitView;
@end

@implementation LiveRoomGiftAnimationView

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
        _portraitView.headwearHidden = true;
    }
    return _portraitView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.svgaAnimationPlayer.loops = 1;
    self.svgaAnimationPlayer.clearsAfterStop = YES;
    self.svgaAnimationPlayer.delegate = self;
    self.svgaAnimationPlayer.contentMode = UIViewContentModeScaleAspectFit;
    self.parser = [[SVGAParser alloc] init];
    
    [self.avatarImageView addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(self.avatarImageView).multipliedBy(1.5);
        make.height.mas_equalTo(self.avatarImageView).multipliedBy(1.5);
        make.center.mas_equalTo(self.avatarImageView);
    }];
    
}

- (void)setMessage:(RCMessageContent *)message {
    _message = message;
    dispatch_async(dispatch_get_main_queue(), ^{
       [self.effectDataArray addObject:_message];
       if (!self.isAnimating) {
        [self showGiftImagesEffects:_message];
       }
    });
}


- (void)showGiftImagesEffects:(RCMessageContent *)message {
    if (!message.senderUserInfo) {
        return;
    }
    self.isAnimating = YES;
    self.infoView.hidden = NO;
    self.numLabel.hidden = NO;
    LiveGiftMessage * giftMessage = (LiveGiftMessage *)message;
//    [self.avatarImageView fp_setImageWithURLString:giftMessage.senderUserAvatar];
    
    self.portraitView.portrait = giftMessage.portrait;
    
    self.nickNameLabel.text = giftMessage.senderUserName;
    NSArray * array = [[SPDCommonTool getFamyLanguage] componentsSeparatedByString:@"-"];
    self.giftNameLabel.text = giftMessage.giftName[array[0]];
    [self.giftImageView fp_setImageWithURLString:giftMessage.giftCover];
    self.numLabel.text = [NSString stringWithFormat:@"x%@",giftMessage.num];
    if (giftMessage.giftAnimationUrl.notEmpty) {
        [self.parser parseWithURL:[NSURL URLWithString:giftMessage.giftAnimationUrl]
        completionBlock:^(SVGAVideoEntity * _Nullable videoItem) {
            if (videoItem != nil) {
                self.svgaAnimationPlayer.videoItem = videoItem;
                [self.svgaAnimationPlayer startAnimation];
            } else {
                [self finishedAnimation];
            }
        } failureBlock:^(NSError * _Nullable error) {
            [self finishedAnimation];
        }];
    }
}

- (void)finishedAnimation {
    self.infoView.hidden = YES;
    self.numLabel.hidden = YES;
    if (self.effectDataArray.count > 0) {
        [self.effectDataArray removeObjectAtIndex:0];
        if (self.effectDataArray.count > 0) {
            [self showGiftImagesEffects:self.effectDataArray.firstObject];
        } else {
            self.isAnimating = NO;
        }
    } else {
        self.isAnimating = NO;
    }
}

#pragma mark - SVGAPlayer

- (void)svgaPlayerDidFinishedAnimation:(SVGAPlayer *)player {
    [self finishedAnimation];
}

- (NSMutableArray *)effectDataArray {
    if (!_effectDataArray) {
        _effectDataArray = [NSMutableArray new];
    }
    return _effectDataArray;
}

@end
