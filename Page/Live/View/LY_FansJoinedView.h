//
//  LY_FansJoinedView.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/20.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LY_FansJoinedView, LY_FansJoinedViewHeaderView;

@protocol LY_FansJoinedViewDelegate <NSObject>

- (void)fansJoinedView:(LY_FansJoinedView *)fansJoinedView clickHearLiveBtn:(UIButton *)btn;

- (void)fansJoinedView:(LY_FansJoinedView *)fansJoinedView clickSendGiftBtn:(UIButton *)btn;

- (void)fansJoinedView:(LY_FansJoinedView *)fansJoinedView clickSendMessageeBtn:(UIButton *)btn;

- (void)fansJoinedView:(LY_FansJoinedView *)fansJoinedView clickShareLiveBtn:(UIButton *)btn;

@end

@interface LY_FansJoinedView : UIView

@property(nonatomic, weak) id<LY_FansJoinedViewDelegate> delegate;

// 直播间id
@property(nonatomic, copy) NSString *liveId;

// 粉丝团是否有效
@property(nonatomic, assign) BOOL isValid;

// 文本描述
@property(nonatomic, copy) NSString *text;

- (instancetype)initWithLiveId:(NSString *)liveId;

- (void)setLevel:(NSInteger)level andName:(NSString *)name;

- (void)setLevel:(NSInteger)level nextLevel:(NSInteger)nextLevel intimacy:(NSInteger)intimacy nextIntimacy:(NSInteger)nextIntimacy progress:(CGFloat)progress;

@end

@protocol LY_FansJoinedViewHeaderViewDelegate <NSObject>

- (void)joinedViewHeaderView:(LY_FansJoinedViewHeaderView *)joinedViewHeaderView clickSendGiftBtn:(UIButton *)btn;

@end

@interface LY_FansJoinedViewHeaderView : UIView

@property(nonatomic, weak) id<LY_FansJoinedViewHeaderViewDelegate> delegate;

// 粉丝团是否有效
@property(nonatomic, assign) BOOL isValid;

// 文本描述
@property(nonatomic, copy) NSString *text;


- (void)setLevel:(NSInteger)level andName:(NSString *)name;


- (void)setLevel:(NSInteger)level nextLevel:(NSInteger)nextLevel intimacy:(NSInteger)intimacy nextIntimacy:(NSInteger)nextIntimacy progress:(CGFloat)progress;


@end

NS_ASSUME_NONNULL_END
