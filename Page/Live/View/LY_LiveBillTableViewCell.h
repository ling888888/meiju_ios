//
//  LY_LiveBillTableViewCell.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LY_LiveBill.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_LiveBillTableViewCell : UITableViewCell

@property(nonatomic, strong) LY_LiveBill *bill;

@end

NS_ASSUME_NONNULL_END
