//
//  LiveFreeGiftReceiveSuccess.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/24.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveFreeGiftReceiveSuccess.h"
#import "YYWebImage.h"

@interface LiveFreeGiftReceiveSuccess ()<CAAnimationDelegate>

@property (weak, nonatomic) IBOutlet YYAnimatedImageView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIImageView *layerImageView;

@end

@implementation LiveFreeGiftReceiveSuccess

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hide)];
    [self addGestureRecognizer:tap];
    self.bgView.yy_imageURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"free_gift_success" ofType:@"webp"]];
    self.bgView.animationRepeatCount = 1;
    [self performSelector:@selector(beginAnimation) withObject:nil afterDelay:2];

}

#pragma mark - CAAnimationDelegate

- (void)animationDidStart:(CAAnimation *)anim{
    if (anim == [self.iconImageView.layer animationForKey:@"kAddToCacheAnimationKey"]) {
        self.iconImageView.hidden = NO;
        self.bgView.hidden = YES;
        self.titleLabel.hidden = YES;
        self.descLabel.hidden = YES;
        self.layerImageView.hidden = YES;
    }
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    if (anim == [self.iconImageView.layer animationForKey:@"kAddToCacheAnimationKey"]) {
        self.iconImageView.hidden = YES;
        [self.iconImageView.layer removeAnimationForKey:@"kAddToCacheAnimationKey"];
        [self.iconImageView removeFromSuperview];
        [self hide];
     }
}

- (void)beginAnimation{
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:self.iconImageView.center];
    // 这个是计算的礼物盒的坐标
    [bezierPath addLineToPoint:CGPointMake(kIsMirroredLayout ? 15 + 19: kScreenW - 15 - 19, kScreenH - 19 - 10)];
    
    CAKeyframeAnimation *anim = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    anim.duration = 1;
    anim.values = @[@(1),@(1.3),@(0.3)];
    anim.keyTimes = @[@(0),@(0.3),@(1)];
    
    //创建位移的动画。
    CAKeyframeAnimation *movingAnim  = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    movingAnim.path = bezierPath.CGPath;
    movingAnim.duration = anim.duration;
    
    //创建透明度动画
    CABasicAnimation *alphaAnim = [CABasicAnimation animationWithKeyPath:@"opacity"];
    alphaAnim.fromValue = @(1);
    alphaAnim.toValue = @(0.3);
    alphaAnim.duration = anim.duration;
    
    //创建动画组
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.animations = @[anim,movingAnim,alphaAnim];
    group.duration = anim.duration;
    group.removedOnCompletion = NO;
    group.fillMode = kCAFillModeForwards;
    group.delegate = self;
    
    [self.iconImageView.layer addAnimation:group forKey:@"kAddToCacheAnimationKey"];
}

- (void)hide {
    [self removeFromSuperview];
}

- (void)dealloc
{
    NSLog(@"%@ dealloc",NSStringFromClass([self class]));
}

@end
