//
//  LY_LiveFiringMoreActionSheetView.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LYPCustomActionSheetView.h"

NS_ASSUME_NONNULL_BEGIN


@protocol LY_LiveFiringMoreActionSheetViewDelegate <NSObject>

- (void)clickWalletBtn;

- (void)clickRecordBtn;

@end


@interface LY_LiveFiringMoreActionSheetView : LYPCustomActionSheetView

@property (nonatomic, assign) BOOL presentOnLiveEndedView;
@property (nonatomic, weak) id<LY_LiveFiringMoreActionSheetViewDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
