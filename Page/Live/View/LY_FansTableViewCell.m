//
//  LY_FansTableViewCell.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_FansTableViewCell.h"
#import "LY_FansLevelView.h"

@interface LY_FansTableViewCell ()

@property(nonatomic, strong) UILabel *rankLabel;

@property(nonatomic, strong) UIImageView *portraitView;

@property(nonatomic, strong) UILabel *nameLabel;

@property(nonatomic, strong) LY_FansLevelView *leveView;

@property(nonatomic, strong) UILabel *intimacyLabel;

@property(nonatomic, strong) UIImageView *heartImgView;

@end

@implementation LY_FansTableViewCell

- (UILabel *)rankLabel {
    if (!_rankLabel) {
        _rankLabel = [[UILabel alloc] init];
        _rankLabel.textAlignment = NSTextAlignmentCenter;
        //设置中文倾斜
        //设置反射。倾斜角度。
        CGAffineTransform matrix =CGAffineTransformMake(1, 0, tanf(5 * (CGFloat)M_PI / 180), 1, 0, 0);
        //取得系统字符并设置反射。
        UIFontDescriptor *desc = [ UIFontDescriptor fontDescriptorWithName :[UIFont mediumFontOfSize:15].fontName matrix :matrix];
        _rankLabel.font = [UIFont fontWithDescriptor:desc size :15];
    }
    return _rankLabel;
}

- (UIImageView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[UIImageView alloc] init];
        _portraitView.layer.cornerRadius = 24;
        _portraitView.layer.masksToBounds = true;
        _portraitView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _portraitView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = [UIColor colorWithHexString:@"#333333"];
        _nameLabel.font = [UIFont mediumFontOfSize:15];
    }
    return _nameLabel;
}

- (LY_FansLevelView *)leveView {
    if (!_leveView) {
        _leveView = [[LY_FansLevelView alloc] init];
    }
    return _leveView;
}

- (UILabel *)intimacyLabel {
    if (!_intimacyLabel) {
        _intimacyLabel = [[UILabel alloc] init];
        _intimacyLabel.textColor = [UIColor colorWithHexString:@"#6A2CF7"];
        _intimacyLabel.font = [UIFont mediumFontOfSize:14];
    }
    return _intimacyLabel;
}

- (UIImageView *)heartImgView {
    if (!_heartImgView) {
        _heartImgView = [[UIImageView alloc] init];
        _heartImgView.image = [UIImage imageNamed:@"ic_zhibo_fensituan_qinmidu"];
    }
    return _heartImgView;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
        
        self.rankLabel.textColor = [UIColor colorWithHexString:@"#FFD249"];
        self.portraitView.backgroundColor = [UIColor redColor];
        self.nameLabel.text = @"嘻嘻呼44呼";
        self.intimacyLabel.text = @"36";
    }
    return self;
}

- (void)setupUI {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self addSubview:self.rankLabel];
    [self addSubview:self.portraitView];
    [self addSubview:self.nameLabel];
    [self addSubview:self.leveView];
    [self addSubview:self.intimacyLabel];
    [self addSubview:self.heartImgView];
}

- (void)setupUIFrame {
    [self.rankLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(5);
        make.width.mas_equalTo(35);
        make.centerY.mas_equalTo(0);
    }];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.rankLabel.mas_trailing);
        make.size.mas_equalTo(48);
        make.centerY.mas_equalTo(0);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.portraitView);
        make.leading.mas_equalTo(self.portraitView.mas_trailing).offset(8);
        make.trailing.mas_lessThanOrEqualTo(self.intimacyLabel.mas_leading).offset(-10);
    }];
    [self.leveView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.portraitView).offset(-3);
        make.leading.mas_equalTo(self.portraitView.mas_trailing).offset(8);
        make.height.mas_equalTo(15);
    }];
    [self.heartImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(15, 12.5));
        make.centerY.mas_equalTo(0);
        make.trailing.mas_equalTo(-15);
    }];
    [self.intimacyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.trailing.mas_equalTo(self.heartImgView.mas_leading).offset(-10);
    }];
    
    [self.nameLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];
    [self.intimacyLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
}

- (void)setFans:(LY_Fans *)fans {
    _fans = fans;
    
    self.nameLabel.text = fans.nick_name;
    
    [self.leveView setLevel:fans.fan_level andName:fans.fan_male];
    
    self.intimacyLabel.text = fans.intimacy;
    
    [self.portraitView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:fans.avatar]] placeholderImage:[UIImage imageNamed:@"img_portrait_placeholder"]];
}

- (void)setRank:(NSInteger)rank {
    _rank = rank;
    
    if (rank == 1) {
        self.rankLabel.textColor = [UIColor colorWithHexString:@"#FFD249"];
    } else if (rank == 2) {
        self.rankLabel.textColor = [UIColor colorWithHexString:@"#C6C6C6"];
    } else if (rank == 3) {
        self.rankLabel.textColor = [UIColor colorWithHexString:@"#E1A571"];
    } else {
        self.rankLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
    }
    
    if (rank > 50) {
        self.rankLabel.text = @"50+";
    } else {
        self.rankLabel.text = [NSString stringWithFormat:@"%ld", rank];
    }
}

@end
