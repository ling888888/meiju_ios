//
//  LinkMicAnchorView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/4/23.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LinkMicAnchorView.h"
#import "LinkMicAnchorViewCell.h"
#import "ZegoKitManager.h"
#import "LinkMicModel.h"
#import "LiveModel.h"

@interface LinkMicAnchorView ()<UICollectionViewDataSource, UICollectionViewDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet UISwitch *linkMicSwitch;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;
@property (weak, nonatomic) IBOutlet UIImageView *placeholderImageView;
@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;

@property (nonatomic, assign) CGFloat contentHeight;
@property (nonatomic, assign) CGFloat bottomHeight;

@end

@implementation LinkMicAnchorView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(linkMicUserDidChange:) name:LinkMicUserDidChangeNotification object:nil];
    
    self.contentHeight = 500;
    self.bottomHeight = 100;
    self.contentViewBottom.constant = -self.contentHeight - self.bottomHeight;
    
    self.linkMicSwitch.transform = CGAffineTransformMakeScale(36.0f / 51.0f, 36.0f / 51.0f);
    [self.collectionView registerNib:[UINib nibWithNibName:@"LinkMicAnchorViewCell" bundle:nil] forCellWithReuseIdentifier:@"LinkMicAnchorViewCell"];
    self.flowLayout.itemSize = CGSizeMake(kScreenW, 70);
    self.placeholderLabel.localizedKey = @"等待用户呼入\n或者点击用户头像邀请";
    [self reloadData];
    
    [self layoutIfNeeded];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Public methods

- (void)show {    
    [UIView animateWithDuration:0.25 animations:^{
        self.contentViewBottom.constant = -self.bottomHeight;
        [self layoutIfNeeded];
    }];
}

#pragma mark - Event responses

- (IBAction)tapBackground:(UITapGestureRecognizer *)sender {
    [self hide];
}

- (IBAction)clicLinkMickSwitch:(UISwitch *)sender {
    sender.userInteractionEnabled = NO;
    if (!sender.on && ZegoManager.linkMicLinkingDic.count) {
        LYPSTextAlertView *view = [[LYPSTextAlertView alloc] init];
        view.title = SPDLocalizedString(@"提示");
        view.message = [NSString stringWithFormat:SPDLocalizedString(@"还有%ld名用户正在连麦中，确定要关闭连麦吗？"), ZegoManager.linkMicLinkingDic.count];
        [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"再等等") style:LYPSystemAlertActionStyleCancel handler:^(LYPSystemAlertAction * _Nonnull action) {
            sender.on = !sender.on;
            sender.userInteractionEnabled = YES;
        }]];
        [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"关闭连麦") style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
            [self requestLiveOpenChat];
        }]];
        [view present];
    } else {
        [self requestLiveOpenChat];
    }
}

- (void)linkMicUserDidChange:(NSNotification *)notification {
    [self reloadData];
}

#pragma mark - Private methods

- (void)hide {
    [UIView animateWithDuration:0.25 animations:^{
        self.contentViewBottom.constant = -self.contentHeight - self.bottomHeight;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        if ([self.delegate respondsToSelector:@selector(linkMicAnchorViewDidHide)]) {
            [self.delegate linkMicAnchorViewDidHide];
        }
    }];
}

- (void)reloadData {
    self.linkMicSwitch.on = ZegoManager.linkMicSwitch;
    if (ZegoManager.linkMicSwitch) {
        self.stateLabel.localizedKey = @"连麦已开启";
        self.collectionView.hidden = NO;
        [self.collectionView reloadData];
        if (ZegoManager.linkMicLinkingDic.count || ZegoManager.linkMicApplyDic.count) {
            self.placeholderImageView.hidden = YES;
            self.placeholderLabel.hidden = YES;
        } else {
            self.placeholderImageView.hidden = NO;
            self.placeholderLabel.hidden = NO;
        }
    } else {
        self.stateLabel.localizedKey = @"连麦未开启";
        self.collectionView.hidden = YES;
        self.placeholderImageView.hidden = YES;
        self.placeholderLabel.hidden = YES;
    }
}

- (void)requestLiveOpenChat {
    NSDictionary *params = @{@"liveId": ZegoManager.liveInfo.liveId, @"openChat": @(self.linkMicSwitch.on ? 1 : 0)};
    [RequestUtils POST:URL_NewServer(@"live/live.open.chat") parameters:params success:^(id  _Nullable suceess) {
        ZegoManager.linkMicSwitch = self.linkMicSwitch.on;
        [ZegoManager sendBigRoomMessage:[NSString stringWithFormat:@"isOpenConnMic:%@", ZegoManager.linkMicSwitch ? @"true" : @"false"]];
        if (!ZegoManager.linkMicSwitch) {
            for (LinkMicModel *model in ZegoManager.linkMicLinkingDic.allValues) {
                [ZegoManager sendCustomCommandToUser:model._id content:@"handUp"];
            }
            [ZegoManager.linkMicLinkingDic removeAllObjects];
            for (LinkMicModel *model in ZegoManager.linkMicApplyDic.allValues) {
                [ZegoManager sendCustomCommandToUser:model._id content:@"refuse"];
            }
            [ZegoManager.linkMicApplyDic removeAllObjects];
            [[NSNotificationCenter defaultCenter] postNotificationName:LinkMicUserDidChangeNotification object:nil];
        }
        [self reloadData];
        self.linkMicSwitch.userInteractionEnabled = YES;

        if ([self.delegate respondsToSelector:@selector(anchorDidChangeLinkMicSwitch)]) {
            [self.delegate anchorDidChangeLinkMicSwitch];
        }
    } failure:^(id  _Nullable failure) {
        self.linkMicSwitch.on = !self.linkMicSwitch.on;
        self.linkMicSwitch.userInteractionEnabled = YES;
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return ZegoManager.linkMicLinkingDic.count;
        default:
            return ZegoManager.linkMicApplyDic.count;
    }
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LinkMicAnchorViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LinkMicAnchorViewCell" forIndexPath:indexPath];
    switch (indexPath.section) {
        case 0:
            cell.nickNameLabelTop.constant = 4;
            cell.durationLabel.hidden = NO;
            cell.hangupButton.hidden = NO;
            cell.allowButton.hidden = YES;
            cell.model = ZegoManager.linkMicLinkingDic.allValues[indexPath.row];
            break;
        default:
            cell.nickNameLabelTop.constant = 13.5;
            cell.durationLabel.hidden = YES;
            cell.hangupButton.hidden = YES;
            cell.allowButton.hidden = NO;
            cell.model = ZegoManager.linkMicApplyDic.allValues[indexPath.row];
            break;
    }
    return cell;
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ![touch.view isDescendantOfView:self.contentView];
}

@end
