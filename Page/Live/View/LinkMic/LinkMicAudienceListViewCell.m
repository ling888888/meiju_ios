//
//  LinkMicAudienceListViewCell.m
//  SimpleDate
//
//  Created by 李楠 on 2020/4/26.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LinkMicAudienceListViewCell.h"

@interface LinkMicAudienceListViewCell ()

@property(nonatomic, strong) LY_PortraitView *portraitView;

@end

@implementation LinkMicAudienceListViewCell

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
    }
    return _portraitView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
}

- (void)setPortrait:(LY_Portrait *)portrait {
    _portrait = portrait;
    
    self.portraitView.portrait = portrait;
}

@end
