//
//  LinkMicAudienceListView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/4/26.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LinkMicAudienceListView.h"
#import "LinkMicAudienceListViewCell.h"
#import "ZegoKitManager.h"
#import "LinkMicModel.h"
#import "LY_HomePageViewController.h"

@interface LinkMicAudienceListView ()<UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation LinkMicAudienceListView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(linkMicUserDidChange:) name:LinkMicUserDidChangeNotification object:nil];
    
    self.contentView.alpha = 0;
    self.contentViewBottom.constant = 164.5 + IphoneX_Bottom;
    [self.collectionView registerNib:[UINib nibWithNibName:@"LinkMicAudienceListViewCell" bundle:nil] forCellWithReuseIdentifier:@"LinkMicAudienceListViewCell"];
    [self reloadData];
    
    [self layoutIfNeeded];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *view = [super hitTest:point withEvent:event];
    if (view == self) {
        [self hide];
        return nil;
    } else {
        return view;
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Public methods

- (void)show {
    [UIView animateWithDuration:0.25 animations:^{
        self.contentView.alpha = 1;
    }];
}

#pragma mark - Event responses

- (void)linkMicUserDidChange:(NSNotification *)notification {
    [self reloadData];
}

#pragma mark - Private methods

- (void)hide {
    if ([self.delegate respondsToSelector:@selector(linkMicAudienceListViewWillHide)]) {
        [self.delegate linkMicAudienceListViewWillHide];
    }
    
    [UIView animateWithDuration:0.25 animations:^{
        self.contentView.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)reloadData {
    if (ZegoManager.linkMicLinkingDic.count) {
        self.numberLabel.text = [NSString stringWithFormat:SPDLocalizedString(@"%ld个人正在与主播聊天"), ZegoManager.linkMicLinkingDic.count];
        [self.collectionView reloadData];
    } else {
        [self hide];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return ZegoManager.linkMicLinkingDic.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LinkMicAudienceListViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LinkMicAudienceListViewCell" forIndexPath:indexPath];
    LinkMicModel *model = ZegoManager.linkMicLinkingDic.allValues[indexPath.row];
//    [cell.avatarImageView fp_setImageWithURLString:model.avatar];
    cell.portrait = model.portrait;
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    LinkMicModel *model = ZegoManager.linkMicLinkingDic.allValues[indexPath.row];
    LY_HomePageViewController *homePageVC = [[LY_HomePageViewController alloc] initWithUserId:model._id];
    [self.VC.navigationController pushViewController:homePageVC animated:YES];
}

@end
