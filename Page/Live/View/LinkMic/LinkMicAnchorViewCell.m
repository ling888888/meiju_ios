//
//  LinkMicAnchorViewCell.m
//  SimpleDate
//
//  Created by 李楠 on 2020/4/24.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LinkMicAnchorViewCell.h"
#import "LinkMicModel.h"
#import "ZegoKitManager.h"

@interface LinkMicAnchorViewCell ()

@property (nonatomic, strong) LY_PortraitView *portraitView;

@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;

@property (nonatomic, strong) NSTimer *durationTimer;

@end

@implementation LinkMicAnchorViewCell

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
    }
    return _portraitView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.contentView addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(70);
        make.leading.mas_equalTo(0);
        make.centerY.mas_equalTo(0);
    }];
}

- (void)removeFromSuperview {
    [super removeFromSuperview];
    
    [self releaseTimer];
}

- (IBAction)clickHangupButton:(UIButton *)sender {
    [ZegoManager sendCustomCommandToUser:self.model._id content:@"handUp"];
    [ZegoManager.linkMicLinkingDic removeObjectForKey:self.model._id];
    [[NSNotificationCenter defaultCenter] postNotificationName:LinkMicUserDidChangeNotification object:nil];
}

- (IBAction)clickAllowButton:(UIButton *)sender {
    if (ZegoManager.linkMicLinkingDic.count < 5) {
        [ZegoManager sendCustomCommandToUser:self.model._id content:@"agree"];
        self.model.startTimeStamp = [[NSDate date] timeIntervalSince1970];
        [ZegoManager.linkMicLinkingDic setValue:self.model forKey:self.model._id];
        [ZegoManager.linkMicApplyDic removeObjectForKey:self.model._id];
        [[NSNotificationCenter defaultCenter] postNotificationName:LinkMicUserDidChangeNotification object:nil];
    } else {
        [self showTips:SPDLocalizedString(@"最多可同时连麦5人")];
    }
}

- (void)setModel:(LinkMicModel *)model {
    _model = model;
    
    self.portraitView.portrait = model.portrait;
//    [self.avatarImageView fp_setImageWithURLString:_model.avatar];
    self.nickNameLabel.text = _model.nick_name;
    if (!self.durationLabel.hidden) {
        if (self.durationTimer) {
            [self releaseTimer];
        }
        self.durationTimer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(updateDuration) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:self.durationTimer forMode:NSRunLoopCommonModes];
        [self.durationTimer fire];
    } else {
        [self releaseTimer];
    }
}

- (void)updateDuration {
    NSInteger duration = [[NSDate date] timeIntervalSince1970] - self.model.startTimeStamp;
    self.durationLabel.text = [NSString stringWithFormat:@"%02d:%02d", duration / 60, duration % 60];
}

- (void)releaseTimer {
    if (self.durationTimer) {
        [self.durationTimer invalidate];
        self.durationTimer = nil;
    }
}

@end
