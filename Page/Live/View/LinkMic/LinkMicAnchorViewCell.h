//
//  LinkMicAnchorViewCell.h
//  SimpleDate
//
//  Created by 李楠 on 2020/4/24.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LinkMicModel;

@interface LinkMicAnchorViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nickNameLabelTop;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UIButton *hangupButton;
@property (weak, nonatomic) IBOutlet UIButton *allowButton;

@property (nonatomic, strong) LinkMicModel *model;

@end

NS_ASSUME_NONNULL_END
