//
//  LinkMicSettingsView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/8/31.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_AlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LinkMicSettingsView : LY_AlertView

@property (nonatomic, copy) NSString * liveId;

@end

NS_ASSUME_NONNULL_END
