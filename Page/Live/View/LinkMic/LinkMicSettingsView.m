//
//  LinkMicSettingsView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/8/31.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LinkMicSettingsView.h"
#import "ZegoKitManager.h"
#import "LinkMicModel.h"
#import "LiveModel.h"

@interface LinkMicSettingsView ()

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * linkMicTitleLabel;
@property (nonatomic, strong) UILabel * autoUpMicTitleLabel;
@property (nonatomic, strong) UISwitch * autoUpMicSwitch; // 自由上麦的设置
@property (nonatomic, strong) UISwitch * linkMicSwitch; // 连麦设置
@property (nonatomic, strong) UILabel * statusLabel;

@end

@implementation LinkMicSettingsView

- (instancetype)initWithStyle:(LY_AlertViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews {
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(24);
        make.centerX.mas_equalTo(0);
    }];
    [self.contentView addSubview:self.linkMicTitleLabel];
    [self.linkMicTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(14);
        make.top.mas_equalTo(84);
    }];
    [self.contentView addSubview:self.autoUpMicTitleLabel];
    [self.autoUpMicTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.linkMicTitleLabel.mas_bottom).offset(31);
        make.leading.mas_equalTo(14);
    }];
    [self.contentView addSubview:self.linkMicSwitch];
    [self.linkMicSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-16);
        make.centerY.equalTo(self.linkMicTitleLabel.mas_centerY).offset(0);
    }];
    
    [self.contentView addSubview:self.autoUpMicSwitch];
    [self.autoUpMicSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-16);
        make.centerY.equalTo(self.autoUpMicTitleLabel.mas_centerY).offset(0);
    }];
    
    [self.contentView addSubview:self.statusLabel];
    [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView.mas_centerX);
        make.top.equalTo(self.linkMicTitleLabel.mas_bottom).offset(5);
    }];
    
    self.linkMicSwitch.on = ZegoManager.linkMicSwitch;
    self.autoUpMicSwitch.on = ZegoManager.linkMicFreeUpMicSwitch;
    self.autoUpMicSwitch.hidden = !ZegoManager.linkMicSwitch;
    self.autoUpMicTitleLabel.hidden = !ZegoManager.linkMicSwitch;
    self.statusLabel.hidden = ZegoManager.linkMicSwitch;
}

- (void)handleLinkMicSwitch:(UISwitch *)sender {
    sender.userInteractionEnabled = NO;
    self.autoUpMicSwitch.hidden = !sender.on;
    self.autoUpMicTitleLabel.hidden = !sender.on;
    self.statusLabel.hidden = sender.on;
    if (!sender.on && ZegoManager.linkMicLinkingDic.count >0) {
        LYPSTextAlertView *view = [[LYPSTextAlertView alloc] init];
        view.title = SPDLocalizedString(@"提示");
        view.message = [NSString stringWithFormat:SPDLocalizedString(@"还有%ld名用户正在连麦中，确定要关闭连麦吗？"), ZegoManager.linkMicLinkingDic.count];
        [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"再等等") style:LYPSystemAlertActionStyleCancel handler:^(LYPSystemAlertAction * _Nonnull action) {
            sender.on = !sender.on;
            sender.userInteractionEnabled = YES;
        }]];
        [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"关闭连麦") style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
            [self requestLiveOpenChat];
        }]];
        [view present];
    }else{
        [self requestLiveOpenChat];
    }
}

- (void)requestLiveOpenChat {
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    [RequestUtils POST:URL_NewServer(@"live/live.open.chat") parameters:@{@"liveId":self.liveId,@"openChat":@(self.linkMicSwitch.on)} success:^(id  _Nullable suceess) {
        [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
        ZegoManager.linkMicSwitch = self.linkMicSwitch.on;
        [ZegoManager sendBigRoomMessage:[NSString stringWithFormat:@"isOpenConnMic:%@", ZegoManager.linkMicSwitch ? @"true" : @"false"]];
        if (!ZegoManager.linkMicSwitch) {
            for (LinkMicModel *model in ZegoManager.linkMicLinkingDic.allValues) {
                [ZegoManager sendCustomCommandToUser:model._id content:@"handUp"];
            }
            [ZegoManager.linkMicLinkingDic removeAllObjects];
            for (LinkMicModel *model in ZegoManager.linkMicApplyDic.allValues) {
                [ZegoManager sendCustomCommandToUser:model._id content:@"refuse"];
            }
            [ZegoManager.linkMicApplyDic removeAllObjects];
            [[NSNotificationCenter defaultCenter] postNotificationName:LinkMicUserDidChangeNotification object:nil];
        }
        self.linkMicSwitch.userInteractionEnabled = YES;
        self.statusLabel.hidden = ZegoManager.linkMicSwitch;
        
    } failure:^(id  _Nullable failure) {
        [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
        [self showTips:failure[@"msg"]];
    }];
}

- (void)handleAutoUpMicSwitch:(UISwitch *)sender {
    NSString * status = sender.on ? @"on":@"off";
    [RequestUtils POST:URL_Server(@"live.wheat.position.update") parameters:@{@"status":status} success:^(id  _Nullable suceess) {
        ZegoManager.linkMicFreeUpMicSwitch = sender.on;
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (CGFloat)containerHeight {
    return 180.0f + IphoneX_Bottom;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = [UIFont mediumFontOfSize:16];
        _titleLabel.textColor = SPD_HEXCOlOR(@"1A1A1A");
        _titleLabel.text = @"连麦设置".localized;
    }
    return _titleLabel;
}

- (UILabel *)linkMicTitleLabel {
    if (!_linkMicTitleLabel) {
        _linkMicTitleLabel = [UILabel new];
        _linkMicTitleLabel.font = [UIFont mediumFontOfSize:15];
        _linkMicTitleLabel.textColor = SPD_HEXCOlOR(@"1A1A1A");
        _linkMicTitleLabel.text = @"开启连麦".localized;
    }
    return _linkMicTitleLabel;
}

- (UILabel *)autoUpMicTitleLabel {
    if (!_autoUpMicTitleLabel) {
        _autoUpMicTitleLabel = [UILabel new];
        _autoUpMicTitleLabel.font = [UIFont mediumFontOfSize:15];
        _autoUpMicTitleLabel.textColor = SPD_HEXCOlOR(@"1A1A1A");
        _autoUpMicTitleLabel.text = @"自动通过连麦申请".localized;
    }
    return _autoUpMicTitleLabel;
}

- (UISwitch *)linkMicSwitch {
    if (!_linkMicSwitch) {
        _linkMicSwitch = [[UISwitch alloc]init];
        _linkMicSwitch.onTintColor = SPD_HEXCOlOR(@"6A2CF7");
        [_linkMicSwitch addTarget:self action:@selector(handleLinkMicSwitch:) forControlEvents:UIControlEventValueChanged];
    }
    return _linkMicSwitch;
}

- (UISwitch *)autoUpMicSwitch {
    if (!_autoUpMicSwitch) {
        _autoUpMicSwitch = [[UISwitch alloc]init];
        _autoUpMicSwitch.onTintColor = SPD_HEXCOlOR(@"6A2CF7");
        [_autoUpMicSwitch addTarget:self action:@selector(handleAutoUpMicSwitch:) forControlEvents:UIControlEventValueChanged];
    }
    return _autoUpMicSwitch;
}

- (UILabel *)statusLabel {
    if (!_statusLabel) {
        _statusLabel = [UILabel new];
        _statusLabel.font = [UIFont mediumFontOfSize:14];
        _statusLabel.textColor = SPD_HEXCOlOR(@"1A1A1A");
        _statusLabel.hidden = YES;
        _statusLabel.text = @"暂未开启连麦".localized;
    }
    return _statusLabel;
}

@end
