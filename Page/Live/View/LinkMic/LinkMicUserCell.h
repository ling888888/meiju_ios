//
//  LinkMicUserCell.h
//  SimpleDate
//
//  Created by ling Hou on 2020/8/26.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LinkMicNewModel;

@interface LinkMicUserCell : UICollectionViewCell

@property (nonatomic, assign) BOOL isAnchor;

@property (nonatomic, assign) NSString * anchorStatus;// ["0","1"]

@property (nonatomic, strong) LinkMicNewModel * model;

@property (nonatomic, assign) BOOL animating;

@end

NS_ASSUME_NONNULL_END
