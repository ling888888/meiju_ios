//
//  LinkMicAudienceListButton.m
//  SimpleDate
//
//  Created by 李楠 on 2020/4/26.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LinkMicAudienceListButton.h"
#import "LinkMicAudienceListView.h"
#import "ZegoKitManager.h"
#import "LinkMicModel.h"

@interface LinkMicAudienceListButton ()<LinkMicAudienceListViewDelegate>

@property(nonatomic, strong) LY_PortraitView *portraitView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;

@property (nonatomic, assign) BOOL showingListView;

@end

@implementation LinkMicAudienceListButton

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
        _portraitView.headwearHidden = true;
    }
    return _portraitView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(linkMicUserDidChange:) name:LinkMicUserDidChangeNotification object:nil];

    self.alpha = 0;
    self.userInteractionEnabled = NO;
    [self addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    [self reloadData];
    
    [self.avatarImageView addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.avatarImageView);
    }];
}

#pragma mark - Event responses

- (void)click {
    self.showingListView = YES;
    [self hide];
    
    LinkMicAudienceListView *view = [[NSBundle mainBundle] loadNibNamed:@"LinkMicAudienceListView" owner:self options:nil].firstObject;
    view.frame = self.superview.bounds;
    view.delegate = self;
    [self.superview addSubview:view];
    [view show];
}

- (void)linkMicUserDidChange:(NSNotification *)notification {
    [self reloadData];
}

#pragma mark - Private methods

- (void)show {
    [UIView animateWithDuration:0.25 animations:^{
        self.alpha = 1;
    } completion:^(BOOL finished) {
        self.userInteractionEnabled = YES;
    }];
}

- (void)hide {
    self.userInteractionEnabled = NO;
    [UIView animateWithDuration:0.25 animations:^{
        self.alpha = 0;
    }];
}

- (void)reloadData {
    if (ZegoManager.linkMicLinkingDic.count) {
        if (!self.showingListView) {
            [self show];
        }
        
        LinkMicModel *model = ZegoManager.linkMicLinkingDic[[SPDApiUser currentUser].userId];
        if (!model) {
            for (LinkMicModel *nextModel in ZegoManager.linkMicLinkingDic.allValues) {
                if (model.startTimeStamp < nextModel.startTimeStamp) {
                    model = nextModel;
                }
            }
        }
        self.portraitView.portrait = model.portrait;
//        [self.avatarImageView fp_setImageWithURLString:model.avatar];
        if (ZegoManager.linkMicLinkingDic.count > 1) {
            self.numberLabel.superview.hidden = NO;
                self.numberLabel.text = [NSString stringWithFormat:SPDLocalizedString(@"%ld人"), ZegoManager.linkMicLinkingDic.count];
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(101);
            }];
        } else {
            self.numberLabel.superview.hidden = YES;
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(72);
            }];
        }
        [self layoutIfNeeded];
    } else {
        [self hide];
    }
}

#pragma mark - LinkMicAudienceListViewDelegate

- (void)linkMicAudienceListViewWillHide {
    self.showingListView = NO;
    if (ZegoManager.linkMicLinkingDic.count) {
        [self show];
    }
}

@end
