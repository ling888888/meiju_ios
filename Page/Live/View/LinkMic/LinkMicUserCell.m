//
//  LinkMicUserCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/8/26.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LinkMicUserCell.h"
#import "KKPaddingLabel.h"
#import "LY_PortraitView.h"
#import "UIView+RGSize.h"
#import "LinkMicNewModel.h"

@interface LinkMicUserCell ()

@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) KKPaddingLabel * statusLabel; // 已下播label
@property (nonatomic, strong) LY_PortraitView * portraitView;
@property (nonatomic, strong) UIImageView * muteImageView;
@property (nonatomic, strong) UIImageView * soundImg;
@end

@implementation LinkMicUserCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews {
    CGFloat awidth = self.width - 9 *2;
    [self.contentView addSubview:self.soundImg];
    [self.soundImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(awidth);
    }];
    NSMutableArray *array = [NSMutableArray array];
    for (NSInteger i = 0; i < 32; i++) {
        [array addObject:[UIImage imageNamed:[NSString stringWithFormat:@"img_zhibojian_yuyinkuosan_000%ld", i]]];
    }
    self.soundImg.animationImages = array;
    self.soundImg.animationDuration = 1;
    self.soundImg.animationRepeatCount = 0;
    
    [self.contentView addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(awidth);
    }];
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(self.portraitView.mas_bottom).offset(-5);
        make.leading.mas_greaterThanOrEqualTo(3);
        make.trailing.mas_lessThanOrEqualTo(-3);

    }];
    [self.contentView addSubview:self.statusLabel];
    CGFloat space = (awidth - awidth/1.5)/2.0f - 7.5;
    [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(15);
        make.bottom.equalTo(self.portraitView.mas_bottom).offset(-space - 5);
    }];
    self.statusLabel.text = @"已下播".localized;
    self.statusLabel.hidden = YES;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
}

- (void)setAnchorStatus:(NSString *)anchorStatus {
    _anchorStatus = anchorStatus;
    self.statusLabel.text = ([_anchorStatus isEqualToString:@"1"]) ?@"主播".localized:@"已下播".localized;
    self.statusLabel.backgroundColor = ([_anchorStatus isEqualToString:@"1"]) ?SPD_HEXCOlOR(@"6A2CF7"):SPD_HEXCOlOR(@"#999999");
}


- (void)setIsAnchor:(BOOL)isAnchor {
    _isAnchor = isAnchor;
    self.statusLabel.hidden = !_isAnchor;
    CGFloat awidth = self.width - 9 *2;
    [self.portraitView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(awidth);
    }];
    [self.soundImg mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.centerX.mas_equalTo(0);
        make.size.mas_equalTo(awidth);
    }];
}

- (void)setAnimating:(BOOL)animating {
    _animating = animating;
    self.soundImg.hidden = !_animating;
    if (_animating) {
        [self.soundImg startAnimating];
    } else {
        [self.soundImg stopAnimating];
    }
}

- (void)setHighlighted:(BOOL)highlighted {
    
}

- (void)setSelected:(BOOL)selected {
    
}

- (void)setModel:(LinkMicNewModel *)model {
    _model = model;
    [self.portraitView.imageView sd_cancelCurrentImageLoad];
    if (_model.userId.notEmpty) {
        self.portraitView.headwearHidden = NO;
        self.portraitView.portrait = _model.portrait;
        self.nameLabel.text = _model.userName;
    }else{
        self.portraitView.headwearHidden = YES;
        self.portraitView.imageView.image = [UIImage imageNamed:@"ic_zhibo_zhuboyixiabo"];
        self.nameLabel.text = @"";
    }
    self.muteImageView.hidden = ![model.inSilence isEqualToString:@"1"];
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.textColor = [UIColor whiteColor];
        _nameLabel.font = [UIFont systemFontOfSize:13];
    }
    return _nameLabel;
}

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [LY_PortraitView new];
        _portraitView.imageView.image = [UIImage imageNamed:@"ic_zhibo_zhuboyixiabo"];
    }
    return _portraitView;
}

- (KKPaddingLabel *)statusLabel {
    if (!_statusLabel) {
        _statusLabel = [KKPaddingLabel new];
        _statusLabel.textColor = [UIColor whiteColor];
        _statusLabel.padding = UIEdgeInsetsMake(0, 8, 0, 8);
        _statusLabel.font = [UIFont systemFontOfSize:10];
        _statusLabel.layer.cornerRadius = 7.5;
        _statusLabel.clipsToBounds = YES;
    }
    return _statusLabel;
}

- (UIImageView *)muteImageView {
    if (!_muteImageView) {
        _muteImageView = [UIImageView new];
        _muteImageView.image = [UIImage imageNamed:@"ic_jinmai"];
        _muteImageView.hidden = YES;
        [self.portraitView addSubview:_muteImageView];
        [_muteImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.portraitView.imageView.mas_bottom).offset(0);
            make.trailing.equalTo(self.portraitView.imageView.mas_trailing).offset(0);
            make.size.mas_equalTo(15);
        }];
    }
    return _muteImageView;
}

- (UIImageView *)soundImg {
    if (!_soundImg) {
        _soundImg = [UIImageView new];
    }
    return _soundImg;
}

@end
