//
//  LinkMicAudienceIncomingView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/4/24.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LinkMicAudienceIncomingView.h"
#import "ZegoKitManager.h"
#import "LiveModel.h"

@interface LinkMicAudienceIncomingView ()

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewTop;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;

@property(nonatomic, strong) LY_PortraitView *portraitView;

@end

@implementation LinkMicAudienceIncomingView

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
    }
    return _portraitView;
}

- (void)awakeFromNib {
    [super awakeFromNib];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(linkMicStateDidChange:) name:LinkMicStateDidChangeNotification object:nil];

    self.contentView.alpha = 0;
    self.contentViewTop.constant = StatusBarHeight + 17 - 60;
//    [self.avatarImageView fp_setImageWithURLString:ZegoManager.liveInfo.avatar];
    
    [self.avatarImageView addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.avatarImageView);
    }];
    
    self.portraitView.portrait = ZegoManager.liveInfo.portrait;
    
    [self layoutIfNeeded];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *view = [super hitTest:point withEvent:event];
    if (view == self) {
        return nil;
    } else {
        return view;
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Public methods

- (void)show {
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.contentView.alpha = 1;
        self.contentViewTop.constant = StatusBarHeight + 17;
        [self layoutIfNeeded];
    }];
}

#pragma mark - Event responses

- (IBAction)clickHangupButton:(UIButton *)sender {
    [self hide];
    [ZegoManager sendCustomCommandToUser:ZegoManager.liveInfo.userId content:@"refuse"];
    ZegoManager.linkMicState = LinkMicStateNormal;
}

- (IBAction)clickAnswerButton:(UIButton *)sender {
    [self hide];
    [ZegoManager sendCustomCommandToUser:ZegoManager.liveInfo.userId content:@"agree"];
    [ZegoManager startLinkMic];
}

- (void)linkMicStateDidChange:(NSNotification *)notification {
    switch (ZegoManager.linkMicState) {
        case LinkMicStateNormal:
            [self hide];
            break;
        default:
            break;
    }
}

#pragma mark - Private methods

- (void)hide {
    [UIView animateWithDuration:0.2 animations:^{
        self.contentView.alpha = 0;
        self.contentViewTop.constant = StatusBarHeight + 17 - 60;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end
