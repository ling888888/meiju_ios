//
//  LinkMicAudienceIncomingView.h
//  SimpleDate
//
//  Created by 李楠 on 2020/4/24.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LinkMicAudienceIncomingView : UIView

- (void)show;

@end

NS_ASSUME_NONNULL_END
