//
//  LinkMicReceiveApplyMsgView.h
//  SimpleDate
//
//  Created by ling Hou on 2020/8/27.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LYPSystemAlertView.h"

@class LiveApplyMicMessage;

NS_ASSUME_NONNULL_BEGIN

@interface LinkMicReceiveApplyMsgView : LYPSystemAlertView

@property (nonatomic, strong) LiveApplyMicMessage * message;

@property (nonatomic, copy) NSString * name;

@property (nonatomic, copy) NSString * avatarUrl;

@end

NS_ASSUME_NONNULL_END
