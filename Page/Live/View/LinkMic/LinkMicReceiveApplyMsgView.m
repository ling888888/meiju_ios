//
//  LinkMicReceiveApplyMsgView.m
//  SimpleDate
//
//  Created by ling Hou on 2020/8/27.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LinkMicReceiveApplyMsgView.h"
#import "LiveApplyMicMessage.h"

@interface LinkMicReceiveApplyMsgView ()

@property (nonatomic, strong) UIImageView * avatar;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * txtLabel;
@property (nonatomic, strong) NSTimer * timer;

@property (nonatomic, assign) NSInteger  sec;

@end

@implementation LinkMicReceiveApplyMsgView

- (UIImageView *)avatar {
    if (!_avatar) {
        _avatar = [UIImageView new];
    }
    return _avatar;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.font = [UIFont mediumFontOfSize:15];
        _nameLabel.textColor = SPD_HEXCOlOR(@"#6A2CF7");
    }
    return _nameLabel;
}

- (UILabel *)txtLabel {
    if (!_txtLabel) {
        _txtLabel = [UILabel new];
        _txtLabel.font = [UIFont mediumFontOfSize:15];
        _txtLabel.textColor = SPD_HEXCOlOR(@"1a1a1a");
        _txtLabel.text = @"想和你进行连麦".localized;
    }
    return _txtLabel;
}

- (void)setAvatarUrl:(NSString *)avatarUrl {
    _avatarUrl = avatarUrl;
    [self.avatar fp_setImageWithURLString:_avatarUrl];
}

- (void)setName:(NSString *)name {
    _name = name;
    self.nameLabel.text = _name;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.sec = 10;
        if (!_timer) {
            _timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(handleSec:) userInfo:nil repeats:YES];
            [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
            [_timer fire];
        }
        [self initSubsViews];
    }
    return self;
}

- (void)handleSec:(NSTimer *)timer {
    if (_sec == 0) {
        [self dismiss];
        return;
    }
    self.title = [NSString stringWithFormat:@"连麦申请(%@)S".localized,@(_sec)];
    _sec = _sec -1;
}

- (void)initSubsViews {
    [self.contentView addSubview:self.avatar];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.txtLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(-10);
        make.centerX.mas_equalTo(9);
    }];
    [self.avatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(self.nameLabel.mas_leading).offset(-6);
        make.size.mas_equalTo(24);
        make.centerY.equalTo(self.nameLabel.mas_centerY);
    }];
    self.avatar.layer.cornerRadius = 12;
    self.avatar.clipsToBounds = YES;
    [self.txtLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(self.nameLabel.mas_bottom).offset(5);
    }];
}

- (void)setMessage:(LiveApplyMicMessage *)message {
    _message = message;
    [self.avatar fp_setImageWithURLString:message.headImg];
    self.nameLabel.text = _message.userName;
}

- (void)dismiss {
    [super dismiss];
    [self invalidateTimer];
}

- (void)dealloc
{
    [self invalidateTimer];
}

- (void)invalidateTimer {
    [self.timer invalidate];
    self.timer = nil;
    _sec = 0;
}

@end
