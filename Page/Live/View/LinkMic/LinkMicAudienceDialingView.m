//
//  LinkMicAudienceDialingView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/4/24.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LinkMicAudienceDialingView.h"
#import "ZegoKitManager.h"
#import "LiveModel.h"
#import "SVGA.h"

@interface LinkMicAudienceDialingView ()<UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;

@property (nonatomic, strong) LY_PortraitView *portraitView;
@property (weak, nonatomic) IBOutlet SVGAImageView *svgaImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UIButton *micButton;
@property (weak, nonatomic) IBOutlet UIButton *speekerButton;

@property (nonatomic, strong) LiveModel *liveInfo;
@property (nonatomic, strong) NSTimer *durationTimer;

@end

@implementation LinkMicAudienceDialingView

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
    }
    return _portraitView;
}

- (void)awakeFromNib {
    [super awakeFromNib];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(linkMicStateDidChange:) name:LinkMicStateDidChangeNotification object:nil];
    
    self.contentView.alpha = 0;
    self.contentViewBottom.constant = 10 + IphoneX_Bottom - 60;
    [self updateState];
    
    [self.contentView addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(106.5);
        make.top.mas_equalTo(10);
        make.centerX.mas_equalTo(0);
    }];
    
    [self layoutIfNeeded];
}

- (void)removeFromSuperview {
    [super removeFromSuperview];
    
    if (self.durationTimer) {
        [self.durationTimer invalidate];
        self.durationTimer = nil;
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Public methods

- (void)showWithLiveInfo:(LiveModel *)liveInfo {
    self.liveInfo = liveInfo;
//    [self.avatarImageView fp_setImageWithURLString:self.liveInfo.avatar];
    
    
    self.portraitView.portrait = liveInfo.portrait;
    
    self.nickNameLabel.text = self.liveInfo.userName;
    
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        self.contentView.alpha = 1;
        self.contentViewBottom.constant = 10 + IphoneX_Bottom;
        [self layoutIfNeeded];
    }];
}

#pragma mark - Event responses

- (IBAction)tapBackground:(UITapGestureRecognizer *)sender {
    [self hide];
}

- (IBAction)clickHideButton:(UIButton *)sender {
    [self hide];
}

- (IBAction)clickMicButton:(UIButton *)sender {
    sender.selected = !sender.selected;
    [ZegoManager enableMic:!sender.selected];
}

- (IBAction)clickSpeekerButton:(UIButton *)sender {
    sender.selected = !sender.selected;
    [ZegoManager enableSpeaker:!sender.selected];
}

- (IBAction)clickHangupButton:(UIButton *)sender {
    switch (ZegoManager.linkMicState) {
        case LinkMicStateLinking:
            [ZegoManager sendCustomCommandToUser:self.liveInfo.userId content:@"handUp"];
            [ZegoManager stopLinkMic];
            break;
        default:
            [ZegoManager sendCustomCommandToUser:self.liveInfo.userId content:@"cancel"];
            ZegoManager.linkMicState = LinkMicStateNormal;
            break;
    }
}

- (void)linkMicStateDidChange:(NSNotification *)notification {
    [self updateState];
}

#pragma mark - Private methods

- (void)hide {
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
        self.contentView.alpha = 0;
        self.contentViewBottom.constant = 10 + IphoneX_Bottom - 60;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)updateState {
    switch (ZegoManager.linkMicState) {
        case LinkMicStateNormal:
            [self hide];
            break;
        case LinkMicStateDialing:
            self.svgaImageView.hidden = NO;
            self.durationLabel.text = [NSString stringWithFormat:@"%@...", SPDLocalizedString(@"等待连接")];
            self.durationLabel.textColor = [UIColor colorWithRed:26 / 255.0 green:26 / 255.0 blue:26 / 255.0 alpha:0.6];
            self.micButton.userInteractionEnabled = NO;
            self.speekerButton.userInteractionEnabled = NO;
            self.micButton.selected = NO;
            self.speekerButton.selected = NO;
            break;
        case LinkMicStateLinking:
            self.svgaImageView.hidden = YES;
            self.durationLabel.text = @"00:00";
            self.durationLabel.textColor = [UIColor colorWithHexString:COMMON_PINK];
            self.micButton.userInteractionEnabled = YES;
            self.speekerButton.userInteractionEnabled = YES;
            self.micButton.selected = !ZegoManager.micEnabled;
            self.speekerButton.selected = !ZegoManager.speakerEnabled;
            
            if (!self.durationTimer) {
                self.durationTimer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(updateDuration) userInfo:nil repeats:YES];
                [[NSRunLoop currentRunLoop] addTimer:self.durationTimer forMode:NSRunLoopCommonModes];
                [self.durationTimer fire];
            }
            break;
        default:
            break;
    }
}

- (void)updateDuration {
    NSInteger duration = [[NSDate date] timeIntervalSince1970] - ZegoManager.linkMicStartTimeStamp;
    self.durationLabel.text = [NSString stringWithFormat:@"%02d:%02d", duration / 60, duration % 60];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ![touch.view isDescendantOfView:self.contentView];
}

@end
