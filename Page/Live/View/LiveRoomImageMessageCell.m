//
//  LiveRoomImageMessageCell.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/27.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomImageMessageCell.h"
#import "LiveImageMessage.h"
#import "SVGA.h"

@interface LiveRoomImageMessageCell ()

@property (strong, nonatomic) UIImageView *coverImgView;
@end

@implementation LiveRoomImageMessageCell

- (UIImageView *)coverImgView {
    if (!_coverImgView) {
        _coverImgView = [[UIImageView alloc] init];
        _coverImgView.contentMode = UIViewContentModeScaleAspectFill;
        _coverImgView.userInteractionEnabled = true;
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickedImage)];
        [_coverImgView addGestureRecognizer:tap];
    }
    return _coverImgView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //
//        self.cover = [[UIImageView alloc]init];
//        [self.baseContentView addSubview:self.cover];
//        self.cover.userInteractionEnabled = YES;
//
//
//        [self.cover mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.leading.mas_equalTo(0);
//            make.top.mas_equalTo(0);
//            make.bottom.mas_equalTo(0);
//            make.width.mas_equalTo(2);
//        }];
//        self.cover.layer.cornerRadius = 5;
//        self.cover.clipsToBounds = YES;
        
        [self setupUI];
        
        [self setupUIFrame];
    }
    return self;
}

- (void)setupUI {
    [self.baseContentView addSubview:self.coverImgView];
}

- (void)setupUIFrame {
    [self.coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        make.height.mas_equalTo(150);
    }];
}

- (void)setMessageContent:(LY_LiveBaseMessageContent *)messageContent {
    [super setMessageContent:messageContent];
    
    LiveImageMessage *msg = (LiveImageMessage *)messageContent;
    CGSize size = msg.imageSize.CGSizeValue;
    CGFloat ratio  = size.height/size.width;
    CGFloat width = ceil(150/ratio);
    if (size.height != 0) {
        width = width?:150;
    }else{
        width = 150;
    }
    [self.baseContentView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(width);
    }];
    [self.coverImgView fp_setImageWithURLString:msg.imageUrl];
}

- (void)clickedImage {
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveRoomImageMessageCell:)]) {
        LiveImageMessage *msg = (LiveImageMessage *)self.messageContent;
        [self.delegate liveRoomImageMessageCell:msg];
    }
}

//- (void)handleTapAvatar:(UITapGestureRecognizer *)tap{
//    if (_message.senderUserInfo.userId.notEmpty) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"LiveRoomTapAvatar" object:nil userInfo:@{@"userId":_message.senderUserInfo.userId}];
//    }
//}
//
//- (void)longPressAvatarImageView:(UILongPressGestureRecognizer *)longPressed {
//   if (_message.senderUserInfo && longPressed.state == UIGestureRecognizerStateBegan) {
//        NSDictionary *userInfo = @{@"user_id":_message.senderUserInfo.userId ?:@"13", @"nick_name": _message.senderUserInfo.name?:@"13", @"avatar": _message.senderUserInfo.portraitUri?:@"13"};
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"LiveLongPressAvatar" object:nil userInfo:userInfo];
//    }
//}

//- (void)setMessage:(LiveImageMessage *)message {
//    _message = message;
//
//    self.portrait = message.portrait;
////    self.avatar = _message.senderUserInfo.portraitUri;
//    self.medal =_message.medal;
//    self.fansName = _message.fansName;
//    self.fansLevel = _message.fansLevel;
//    self.wealthLevel =_message.wealthLevel;
//    self.name = _message.senderUserInfo.name;
//    self.isAnchor = YES;
//    CGSize size = _message.imageSize.CGSizeValue;
//    CGFloat ratio  = size.height/size.width;
//    CGFloat width = ceil(150/ratio);
//    if (size.height != 0) {
//        width = width?:150;
//    }else{
//        width = 150;
//    }
//    [self.cover mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(width);
//    }];
//    [self.cover fp_setImageWithURLString:_message.imageUrl];
//    [self refresh];
//}

@end
