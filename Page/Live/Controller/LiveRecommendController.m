//
//  LiveRecommedController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRecommendController.h"
#import "LiveRecommendCell.h"
#import "LiveModel.h"
#import "LiveRoomViewController.h"
#import "ZegoKitManager.h"
#import "LY_LiveFollowerView.h"
#import "LY_LiveFollowViewController.h"
#import "LY_BannerView.h"

#define Space  15
#define Width ((kScreenW - Space*3)/2)

@interface LiveRecommendController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) LY_BannerView *bannerView;

@property (nonatomic, strong) LY_LiveFollowerView *liveFollowerView;

//@property (nonatomic, strong) NSMutableArray *bannerArray;

@property (nonatomic, strong) UICollectionView * collectionView;
@property (nonatomic, strong) NSMutableArray * dataArray;
@property (nonatomic, assign) NSInteger page;

@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);
@end

@implementation LiveRecommendController

- (LY_BannerView *)bannerView {
    if (!_bannerView) {
        _bannerView = [[LY_BannerView alloc] init];
    }
    return _bannerView;
}

- (LY_LiveFollowerView *)liveFollowerView {
    if (!_liveFollowerView) {
        _liveFollowerView = [[LY_LiveFollowerView alloc] init];
        [_liveFollowerView addTarget:self action:@selector(liveFollowerViewAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _liveFollowerView;
}

//- (NSMutableArray *)bannerArray {
//    if (!_bannerArray) {
//        _bannerArray = [NSMutableArray array];
//    }
//    return _bannerArray;
//}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.page = 1;
    
    [self setupUI];
    
    [self setupUIFrame];
    
    [self requestData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.liveFollowerView loadNetData];
}

- (void)setupUI {
    [self.view addSubview:self.collectionView];
    [self.view addSubview:self.liveFollowerView];
}

- (void)setupUIFrame {
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.leading.trailing.bottom.mas_equalTo(self.view);
    }];
    
    [self.liveFollowerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(40);
        make.trailing.mas_equalTo(self.view);
        make.bottom.mas_equalTo(-TabBarHeight-30);
    }];
}

- (void)requestData {
    [RequestUtils GET:URL_NewServer(@"live/live.recommend") parameters:@{@"page":@(self.page)} success:^(id  _Nullable suceess) {
        if (self.page == 1) {
            [self.dataArray removeAllObjects];
        }
        NSArray * list = suceess[@"list"];
        for (NSDictionary * dict in list ) {
            LiveModel * model = [LiveModel initWithDictionary:dict];
            [self.dataArray addObject:model];
        }
        self.page++;
        [self.collectionView reloadData];
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
        self.collectionView.mj_footer.hidden = list.count < 15;
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
    }];
    
    // 获取Banner
    NSDictionary *param = @{@"name": @"iOS",
                            @"type":@"live_list"
    };
    [LY_Network getRequestWithURL:LY_Api.clan_banner parameters:param success:^(id  _Nullable response) {
        NSArray *bannerArray = response[@"banner"];
        NSMutableArray *models = [NSMutableArray array];
        for (NSDictionary *dict in bannerArray) {
            BannerModel *model = [BannerModel initWithDictionary:dict];
            double beginTime = [model.begin_time doubleValue] / 1000;
            double endTime = [model.end_time doubleValue] / 1000;
            NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
            if (currentTime >= beginTime && currentTime <= endTime) {
                [models addObject:model];
            }
        }
        self.bannerView.bannerList = models;
    } failture:^(NSError * _Nullable error) {
        
    }];
}

// 关注浮窗点击事件
- (void)liveFollowerViewAction:(LY_LiveFollowerView *)view {
    LY_LiveFollowViewController *vc = [[LY_LiveFollowViewController alloc] init];
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LiveRecommendCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveRecommendCell" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    if (cell.model.isBroadcast) {
        [cell.liveFlagImageView startAnimating];
    }
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"sectionHeaderIdentifier" forIndexPath:indexPath];
    
    if (view.subviews.count == 0) {
        [view addSubview:self.bannerView];
        [self.bannerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.trailing.mas_equalTo(view);
            make.height.mas_equalTo(LYBannerViewHeight);
        }];
    }
    return view;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [ZegoManager joinLiveRoomWithliveInfo:self.dataArray[indexPath.row] isAnchor:NO];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(Space, Space, Space, Space);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(kScreenW, LYBannerViewHeight + 20);
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback ?: self.scrollCallback(scrollView);
}

#pragma mark - JXPagerViewListViewDelegate

- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return self.collectionView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *scrollView))callback {
    self.scrollCallback = callback;
}

#pragma mark - getter

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout new];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.minimumInteritemSpacing = Space;
        layout.minimumLineSpacing = Space;
        layout.itemSize = CGSizeMake(Width, Width + 30);
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerNib:[UINib nibWithNibName:@"LiveRecommendCell" bundle:nil] forCellWithReuseIdentifier:@"LiveRecommendCell"];
        _collectionView.backgroundColor = [UIColor whiteColor];
        __weak typeof(self) weakSelf = self;
        _collectionView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            weakSelf.page = 1;
            [weakSelf requestData];
        }];
        _collectionView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            [weakSelf requestData];
        }];
        
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"sectionHeaderIdentifier"];
    }
    return _collectionView;
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

@end
