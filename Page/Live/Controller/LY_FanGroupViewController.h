//
//  LY_FanGroupViewController.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/20.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, LY_FansGroupStatus) {
    LY_FansGroupStatusNotJoined = 0,
    LY_FansGroupStatusJoined,
    LY_FansGroupStatusInvalid,
};

@class LY_FanGroupViewController;

@protocol LY_FanGroupViewControllerDelegate <NSObject>

- (void)fanGroupViewControllerDidClickJoinFansGroupWith:(LY_FanGroupViewController *)fanGroupViewController withFansGroupName:(NSString *)fansGroupName;

- (void)fanGroupViewControllerDidClickHearLiveWith:(LY_FanGroupViewController *)fanGroupViewController;

- (void)fanGroupViewControllerDidClickSendGiftWith:(LY_FanGroupViewController *)fanGroupViewController;

- (void)fanGroupViewControllerDidClickSendMessageWith:(LY_FanGroupViewController *)fanGroupViewController;

- (void)fanGroupViewControllerDidClickShareLiveWith:(LY_FanGroupViewController *)fanGroupViewController;

@end

@interface LY_FanGroupViewController : LY_BaseViewController

@property(nonatomic, weak) id<LY_FanGroupViewControllerDelegate>delegate;

@property(nonatomic, assign) LY_FansGroupStatus status;

// 直播间id
@property(nonatomic, copy) NSString *liveId;

@property(nonatomic, copy) NSString *anchorUserId;

- (instancetype)initWithLiveId:(NSString *)liveId anchorUserId:(NSString *)anchorUserId;

@end

NS_ASSUME_NONNULL_END
