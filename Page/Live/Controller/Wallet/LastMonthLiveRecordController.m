//
//  LastMonthLiveRecordController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LastMonthLiveRecordController.h"
#import "LiveLastMonthLiveRecordHeader.h"
#import "LY_LiveRecordTableViewCell.h"
#import "LiveWalletTtileView.h"
#import "LY_LiveRecord.h"

@interface LastMonthLiveRecordController ()

@property (nonatomic, strong) LiveWalletTtileView * titleView;
@property (nonatomic, strong) LiveLastMonthLiveRecordHeader * headerView;

@end

@implementation LastMonthLiveRecordController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavigation];
    
    [self setupUI];
    
    [self setupUIFrame];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self loadNetDataWith:RequestNetDataTypeRefresh];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:0.0];
    self.navigationController.navigationBar.titleTextAttributes = @{
        NSFontAttributeName:[UIFont mediumFontOfSize:18],
        NSForegroundColorAttributeName:[UIColor colorWithHexString:@"ffffff"]
    };
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
}

- (void)setupNavigation {
    self.title = @"上月直播记录".localized;
}

- (void)setupUI {
    // 取消自动偏移
    self.extendedLayoutIncludesOpaqueBars = true;
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    self.tableView.allowsSelection = false;
    self.tableView.rowHeight = 130;
    [self.tableView registerClass:[LY_LiveRecordTableViewCell class] forCellReuseIdentifier:@"LiveBillIdentifier"];
    
    _headerView = [[[NSBundle mainBundle]loadNibNamed:@"LiveLastMonthLiveRecordHeader" owner:self options:nil]lastObject];
    [self.view addSubview:_headerView];
    [_headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.trailing.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(220.0f/375 *kScreenW);
    }];

    _titleView = [[[NSBundle mainBundle]loadNibNamed:@"LiveWalletTtileView" owner:self options:nil]lastObject];
    _titleView.iconImageView.image = [UIImage imageNamed:@"ic_zhibo_zhiboqianbao_shouzhijilu"];
    _titleView.actionLabel.hidden = YES;
    [self.view addSubview:_titleView];
    [_titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.trailing.mas_equalTo(0);
        make.top.equalTo(self.headerView.mas_bottom).offset(0);
        make.height.mas_equalTo(60);
    }];
    
}

- (void)setupUIFrame {
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.trailing.mas_equalTo(0);
        make.bottom.mas_equalTo(-IphoneX_Bottom);
        make.top.equalTo(self.titleView.mas_bottom).offset(0);
    }];
}

- (void)loadNetDataWith:(RequestNetDataType)type {
    NSDictionary *params = @{
        @"page": @(self.pageNumber),
        @"isOld": @(true)
    };
    [LY_Network getRequestWithURL:LY_Api.live_list parameters:params success:^(id  _Nullable response) {
        
        NSArray *dataArray = response[@"list"];
        NSMutableArray *models = [NSMutableArray array];
        for (NSDictionary *data in dataArray) {
            LY_LiveRecord *bill = [LY_LiveRecord yy_modelWithDictionary:data];
            [models addObject:bill];
        }
        if (type == RequestNetDataTypeRefresh) {
            
            self.headerView.durationDescLabel.text = [NSString stringWithFormat:@"%@月直播有效时长".localized,response[@"month"]];
            self.headerView.dayDescLabel.text = [NSString stringWithFormat:@"%@月直播有效天数".localized,response[@"month"]];
            self.titleView.titleLabel.text = [NSString stringWithFormat:@"%@月时长记录".localized,response[@"month"]];
            
            NSMutableAttributedString *effectiveTotalStr = [[NSMutableAttributedString alloc]initWithString:response[@"effectiveTotal"]];
            [effectiveTotalStr setAttributes:@{NSForegroundColorAttributeName:SPD_HEXCOlOR(@"#FCEA11"),NSFontAttributeName:[UIFont mediumFontOfSize:30]
            } range:NSMakeRange(0, effectiveTotalStr.length)];
            NSAttributedString *effectiveTotalStr1 = [[NSAttributedString alloc]initWithString:@"h" attributes:@{NSForegroundColorAttributeName:SPD_HEXCOlOR(@"#FCEA11"),NSFontAttributeName:[UIFont mediumFontOfSize:12]}];
            [effectiveTotalStr appendAttributedString:effectiveTotalStr1];
            self.headerView.durationLabel.attributedText = effectiveTotalStr;
            
            // 本月直播有效天数
            NSMutableAttributedString *effectiveDayStr = [[NSMutableAttributedString alloc]initWithString:response[@"effectiveDay"]];
            [effectiveDayStr setAttributes:@{NSForegroundColorAttributeName:SPD_HEXCOlOR(@"#FCEA11"),NSFontAttributeName:[UIFont mediumFontOfSize:30]
            } range:NSMakeRange(0, effectiveDayStr.length)];
            NSAttributedString *effectiveDayStr1 = [[NSAttributedString alloc]initWithString:@"day" attributes:@{NSForegroundColorAttributeName:SPD_HEXCOlOR(@"#FCEA11"),NSFontAttributeName:[UIFont mediumFontOfSize:12]}];
            [effectiveDayStr appendAttributedString:effectiveDayStr1];
            self.headerView.dayLabel.attributedText = effectiveDayStr;

            self.dataList = models;
        } else {
            if (models.count == 0) {
                // 没有更多数据了
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                [self.dataList addObjectsFromArray:models];
            }
        }
        [self reloadData];
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LY_LiveRecordTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LiveBillIdentifier" forIndexPath:indexPath];
    
    cell.record = self.dataList[indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return (220.0f/ kScreenW + 60);
}


@end
