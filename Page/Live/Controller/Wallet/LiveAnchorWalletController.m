//
//  LiveAnchorWalletController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveAnchorWalletController.h"
#import "LY_LiveWalletViewController.h"
#import "LY_LiveRecordViewController.h"
#import "LYPBaseAlertView.h"
#import <WebKit/WebKit.h>

@interface LiveAnchorWalletController ()

@property (nonatomic, copy) NSArray * titleArray;
@property (nonatomic, strong) LY_LiveWalletViewController * walletVC;
@property (nonatomic, strong) LY_LiveRecordViewController * recordVC;

@end

@implementation LiveAnchorWalletController

- (void)viewDidLoad {
    
    self.titleColorNormal = [[UIColor colorWithHexString:@"FFFFFF"] colorWithAlphaComponent:0.5];
    self.titleColorSelected = [UIColor colorWithHexString:@"FFFFFF"];
    self.titleSizeNormal = 15;
    self.titleSizeSelected = 18;
    self.menuView.fontName = @"PingFangSC-Medium";;
    self.menuView.style = WMMenuViewStyleDefault;
    self.showOnNavigationBar = YES;
    self.menuItemWidth = kScreenW/2 -80;
    self.menuViewLayoutMode = WMMenuViewLayoutModeCenter;
    
    [super viewDidLoad];
    
    self.menuView.progressViewBottomSpace = 10;
    self.progressWidth = 85;
    self.menuView.progressHeight = 2;
    self.menuView.lineColor = [[UIColor colorWithHexString:@"#00D4A0"] colorWithAlphaComponent:0.2];
    
    [self reloadData];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ic_tongyong_tips"] style:UIBarButtonItemStylePlain target:self action:@selector(ruleButtonClicked:)];
    
}

- (void)ruleButtonClicked:(UIBarButtonItem *)sender {
    LYPBaseAlertView * view = [[LYPBaseAlertView alloc]init];
    WKWebView * webView = [[WKWebView alloc]init];
    [view.containerView addSubview:webView];
    CGFloat width = 275.0f/375*kScreenW;
    CGFloat height = 270.0f/275 * width;
    [webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.trailing.mas_equalTo(0);
        make.top.and.bottom.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(width, height));
    }];
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://share.famy.ly/ar/liveHelp.html?lang=%@",[SPDCommonTool getFamyLanguage]]]]];
    [view present];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:0.0];
    [self.navigationController.navigationBar vhl_setBackgroundColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.titleTextAttributes = @{
        NSFontAttributeName:[UIFont mediumFontOfSize:18],
        NSForegroundColorAttributeName:[UIColor colorWithHexString:@"ffffff"]
    };
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
}

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.titleArray.count;
}

- (__kindof UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    return index == 0 ? self.walletVC : self.recordVC;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    NSString *title = self.titleArray[index];
    return title.localized;
}


- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    return CGRectMake(0,0, kScreenW, kScreenH);
}


#pragma mark - getters

- (NSArray *)titleArray {
    if (!_titleArray) {
        _titleArray = @[@"直播钱包",@"直播记录"];
    }
    return _titleArray;
}

- (LY_LiveWalletViewController *)walletVC {
    if (!_walletVC) {
        _walletVC = [LY_LiveWalletViewController new];
    }
    return _walletVC;
}

- (LY_LiveRecordViewController *)recordVC {
    if (!_recordVC) {
        _recordVC = [LY_LiveRecordViewController new];
    }
    return _recordVC;
}

@end
