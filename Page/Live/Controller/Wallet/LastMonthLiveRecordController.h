//
//  LastMonthLiveRecordController.h
//  SimpleDate
//
//  Created by ling Hou on 2020/9/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_BaseTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LastMonthLiveRecordController : LY_BaseTableViewController

@end

NS_ASSUME_NONNULL_END
