//
//  LastMonthCrystalController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LastMonthCrystalController.h"
#import "LY_LiveBillTableViewCell.h"
#import "LiveWalletTtileView.h"

@interface LastMonthCrystalController ()

@property(nonatomic, strong) UIImageView *headerImgView;

@property(nonatomic, strong) UILabel *totalNumberLabel;

@property(nonatomic, strong) UILabel *myCoinTextLabel;

@property (nonatomic, strong) LiveWalletTtileView * titleView;

@end

@implementation LastMonthCrystalController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavigation];
    
    [self setupUI];
    
    [self setupUIFrame];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self loadNetDataWith:RequestNetDataTypeRefresh];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:0.0];
    self.navigationController.navigationBar.titleTextAttributes = @{
        NSFontAttributeName:[UIFont mediumFontOfSize:18],
        NSForegroundColorAttributeName:[UIColor colorWithHexString:@"ffffff"]
    };
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
}

- (void)setupNavigation {
    self.title = @"上月水晶记录".localized;
}

- (void)setupUI {
    // 取消自动偏移
    self.extendedLayoutIncludesOpaqueBars = true;
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    self.tableView.allowsSelection = false;
    self.tableView.rowHeight = 60;
    [self.tableView registerClass:[LY_LiveBillTableViewCell class] forCellReuseIdentifier:@"LiveBillIdentifier"];
    
    UIView * headerView = [UIView new];
    headerView.frame = CGRectMake(0, 0, kScreenW, 156.0f/ 667 * kScreenH+ 60 + naviBarHeight);
    [headerView addSubview:self.headerImgView];
    [self.headerImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.trailing.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(156.0f/ 375 * kScreenW + naviBarHeight);
    }];
    [self.headerImgView addSubview:self.totalNumberLabel];
    [self.headerImgView addSubview:self.myCoinTextLabel];
    
    _titleView = [[[NSBundle mainBundle]loadNibNamed:@"LiveWalletTtileView" owner:self options:nil]lastObject];
    _titleView.iconImageView.image = [UIImage imageNamed:@"ic_zhibo_zhiboqianbao_shouzhijilu"];
    _titleView.actionLabel.hidden = YES;
    [headerView addSubview:_titleView];
    [_titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.trailing.mas_equalTo(0);
        make.top.equalTo(self.headerImgView.mas_bottom).offset(0);
        make.height.mas_equalTo(60);
    }];
    [self.view addSubview:headerView];
    
}

- (void)setupUIFrame {
    [self.totalNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.headerImgView);
        make.centerY.mas_equalTo(self.headerImgView).offset(10);
    }];

    [self.myCoinTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.headerImgView);
        make.top.mas_equalTo(self.totalNumberLabel.mas_bottom).offset(10);
    }];
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.trailing.mas_equalTo(0);
        make.bottom.mas_equalTo(-IphoneX_Bottom);
        make.top.mas_equalTo(60+ 156.0f/375 * kScreenW + naviBarHeight);
    }];
}

- (void)loadNetDataWith:(RequestNetDataType)type {
    NSDictionary *params = @{
        @"page": @(self.pageNumber),
        @"isOld": @(true)
    };
    [LY_Network getRequestWithURL:LY_Api.live_wallet_billList parameters:params success:^(id  _Nullable response) {
        NSArray *dataArray = response[@"list"];
        NSMutableArray *models = [NSMutableArray array];
        for (NSDictionary *data in dataArray) {
            LY_LiveBill *bill = [LY_LiveBill yy_modelWithDictionary:data];
            [models addObject:bill];
        }
        if (type == RequestNetDataTypeRefresh) {
            _titleView.titleLabel.text = [NSString stringWithFormat:@"%@月收入水晶".localized,response[@"month"]];
            _myCoinTextLabel.text = [NSString stringWithFormat:@"%@月收入水晶".localized,response[@"month"]];
            
            NSString *walletNum = response[@"walletMonthNum"];
            if (walletNum.notEmpty) {
                self.totalNumberLabel.text = walletNum;
            } else {
                self.totalNumberLabel.text = @"0.0";
            }
            self.dataList = models;
            
        } else {
            if (models.count == 0) {
                // 没有更多数据了
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                [self.dataList addObjectsFromArray:models];
            }
        }
        [self reloadData];
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LY_LiveBillTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LiveBillIdentifier" forIndexPath:indexPath];
    cell.bill = self.dataList[indexPath.row];
    return cell;
}

#pragma mark - getters

- (UIImageView *)headerImgView {
    if (!_headerImgView) {
        _headerImgView = [[UIImageView alloc] init];
        _headerImgView.image = [UIImage imageNamed:@"img_zhibo_kaishizhibo_qianbao"];
    }
    return _headerImgView;
}

- (UILabel *)totalNumberLabel {
    if (!_totalNumberLabel) {
        _totalNumberLabel = [[UILabel alloc] init];
        _totalNumberLabel.textColor = [UIColor whiteColor];
        _totalNumberLabel.font = [UIFont mediumFontOfSize:50];
        _totalNumberLabel.text = @"0.0";
    }
    return _totalNumberLabel;
}

- (UILabel *)myCoinTextLabel {
    if (!_myCoinTextLabel) {
        _myCoinTextLabel = [[UILabel alloc] init];
        _myCoinTextLabel.textColor = [UIColor whiteColor];
        _myCoinTextLabel.font = [UIFont mediumFontOfSize:14];
    }
    return _myCoinTextLabel;
}


@end
