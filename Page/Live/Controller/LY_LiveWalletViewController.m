//
//  LY_LiveWalletViewController.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_LiveWalletViewController.h"
#import "LY_LiveBillTableViewCell.h"
#import "LY_LiveBill.h"
#import "LiveWalletHeaderView.h"
#import "LiveWalletTtileView.h"
#import "LastMonthCrystalController.h"

@interface LY_LiveWalletViewController ()

@property(nonatomic, strong) UIImageView *headerImgView;

@property(nonatomic, strong) UILabel *totalNumberLabel;

@property(nonatomic, strong) UILabel *myCoinTextLabel;

@property (nonatomic, strong) LiveWalletHeaderView * headerView;


@end

@implementation LY_LiveWalletViewController

- (UIImageView *)headerImgView {
    if (!_headerImgView) {
        _headerImgView = [[UIImageView alloc] init];
        _headerImgView.image = [UIImage imageNamed:@"img_zhibo_kaishizhibo_qianbao"];
    }
    return _headerImgView;
}

- (UILabel *)totalNumberLabel {
    if (!_totalNumberLabel) {
        _totalNumberLabel = [[UILabel alloc] init];
        _totalNumberLabel.textColor = [UIColor whiteColor];
        _totalNumberLabel.font = [UIFont mediumFontOfSize:50];
        _totalNumberLabel.text = @"0.0";
    }
    return _totalNumberLabel;
}

- (UILabel *)myCoinTextLabel {
    if (!_myCoinTextLabel) {
        _myCoinTextLabel = [[UILabel alloc] init];
        _myCoinTextLabel.textColor = [UIColor whiteColor];
        _myCoinTextLabel.font = [UIFont mediumFontOfSize:14];
        _myCoinTextLabel.text = @"我的水晶".localized;
    }
    return _myCoinTextLabel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavigation];
    
    [self setupUI];
    
    [self setupUIFrame];
    
    [self loadNetDataWith:RequestNetDataTypeRefresh];
}

- (void)setupNavigation {
    self.title = @"直播钱包".localized;
}

- (void)setupUI {
    // 取消自动偏移
    self.extendedLayoutIncludesOpaqueBars = true;
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    self.tableView.allowsSelection = false;
    self.tableView.rowHeight = 60;
    [self.tableView registerClass:[LY_LiveBillTableViewCell class] forCellReuseIdentifier:@"LiveBillIdentifier"];
    
    [self.view addSubview:self.headerView];

}

- (void)setupUIFrame {

    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
         make.leading.and.trailing.mas_equalTo(0);
         make.top.mas_equalTo(0);
         make.height.mas_equalTo(naviBarHeight + 181.0f/ kScreenH * kScreenH );
     }];
    
    LiveWalletTtileView * titleView = [[[NSBundle mainBundle]loadNibNamed:@"LiveWalletTtileView" owner:self options:nil]lastObject];
    titleView.iconImageView.image = [UIImage imageNamed:@"ic_zhibo_zhiboqianbao_shouzhijilu"];
    titleView.titleLabel.text = @"收支记录".localized;
    titleView.actionLabel.text = @"上月水晶记录".localized;
    titleView.actionLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hanldeTap:)];
    [titleView.actionLabel addGestureRecognizer:tap];
    
    [self.view addSubview:titleView];
    [titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.trailing.mas_equalTo(0);
        make.top.equalTo(self.headerView.mas_bottom).offset(0);
        make.height.mas_equalTo(60);
    }];
    
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.trailing.mas_equalTo(0);
        make.top.equalTo(titleView.mas_bottom).offset(0);
        make.bottom.mas_equalTo(-IphoneX_Bottom);
    }];
    
}

- (void)loadNetDataWith:(RequestNetDataType)type {
    NSDictionary *params = @{
        @"page": @(self.pageNumber)
    };
    [LY_Network getRequestWithURL:LY_Api.live_wallet_billList parameters:params success:^(id  _Nullable response) {
        
        self.headerView.todayCrystal.text = [NSString stringWithFormat:@"%@",response[@"walletDayNum"]];
        self.headerView.monthLabel.text = [NSString stringWithFormat:@"%@",response[@"walletMonthNum"]];
        self.headerView.allCrystal.text = [NSString stringWithFormat:@"%@",response[@"walletTotalNum"]];
        self.headerView.crystalBalance.text = [NSString stringWithFormat:@"%@",response[@"walletNum"]];

        NSArray *dataArray = response[@"list"];
        NSMutableArray *models = [NSMutableArray array];
        for (NSDictionary *data in dataArray) {
            LY_LiveBill *bill = [LY_LiveBill yy_modelWithDictionary:data];
            [models addObject:bill];
        }
        if (type == RequestNetDataTypeRefresh) {
            NSString *walletNum = response[@"walletNum"];
            if (walletNum.notEmpty) {
                self.totalNumberLabel.text = walletNum;
            } else {
                self.totalNumberLabel.text = @"0.0";
            }
            self.dataList = models;
        } else {
            if (models.count == 0) {
                // 没有更多数据了
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                [self.dataList addObjectsFromArray:models];
            }
        }
        [self reloadData];
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LY_LiveBillTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LiveBillIdentifier" forIndexPath:indexPath];
    
    cell.bill = self.dataList[indexPath.row];
    
    return cell;
}

- (LiveWalletHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[[NSBundle mainBundle]loadNibNamed:@"LiveWalletHeaderView" owner:self options:nil]lastObject];
    }
    return _headerView;
}

- (void)hanldeTap:(UITapGestureRecognizer *)tap {
    [self.navigationController pushViewController:[LastMonthCrystalController new] animated:YES];
}

@end
