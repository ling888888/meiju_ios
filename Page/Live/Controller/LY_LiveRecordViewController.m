//
//  LY_LiveRecordViewController.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/25.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_LiveRecordViewController.h"
#import "LY_LiveRecordTableViewCell.h"
#import "LY_LiveRecord.h"
#import "LiveRecordHeaderView.h"
#import "LiveWalletTtileView.h"
#import "LastMonthLiveRecordController.h"

@interface LY_LiveRecordViewController ()

@property (nonatomic, strong) LiveRecordHeaderView * headerView;
@property (nonatomic, strong) LiveWalletTtileView * titleView;

@end

@implementation LY_LiveRecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavigation];

    [self setupUI];
    
    [self loadNetDataWith:RequestNetDataTypeRefresh];
}

- (void)setupNavigation {
    self.title = @"直播记录".localized;
}

- (void)setupUI {
    
    // 取消自动偏移
    self.extendedLayoutIncludesOpaqueBars = true;
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    self.tableView.allowsSelection = false;
    self.tableView.rowHeight = 130;
    [self.tableView registerClass:[LY_LiveRecordTableViewCell class] forCellReuseIdentifier:@"LiveRecordIdentifier"];
    
    self.headerView = [[[NSBundle mainBundle]loadNibNamed:@"LiveRecordHeaderView" owner:self options:nil]lastObject];
    [self.view addSubview:self.headerView];
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.trailing.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(266.0f/375*kScreenW + naviBarHeight);
    }];
    
    _titleView = [[[NSBundle mainBundle]loadNibNamed:@"LiveWalletTtileView" owner:self options:nil]lastObject];
    _titleView.iconImageView.image = [UIImage imageNamed:@"ic_zhibo_zhiboqianbao_shichangjilu"];
    _titleView.titleLabel.text = @"时长记录".localized;
    _titleView.actionLabel.text = @"上月直播记录".localized;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    [_titleView addGestureRecognizer:tap];
    [self.view addSubview:_titleView];
    [_titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.trailing.mas_equalTo(0);
        make.top.equalTo(self.headerView.mas_bottom).offset(0);
        make.height.mas_equalTo(60);
    }];
    
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.trailing.mas_equalTo(0);
        make.top.equalTo(_titleView.mas_bottom).offset(0);
        make.bottom.mas_equalTo(-IphoneX_Bottom);
    }];
}


- (void)loadNetDataWith:(RequestNetDataType)type {
    NSDictionary *params = @{
        @"page": @(self.pageNumber)
    };
    [LY_Network getRequestWithURL:LY_Api.live_list parameters:params success:^(id  _Nullable response) {
        
        self.headerView.dict = response;
        
        NSArray *dataArray = response[@"list"];
        NSMutableArray *models = [NSMutableArray array];
        for (NSDictionary *data in dataArray) {
            LY_LiveRecord *record = [LY_LiveRecord yy_modelWithDictionary:data];
            [models addObject:record];
        }
        if (type == RequestNetDataTypeRefresh) {
            self.dataList = models;
        } else {
            if (models.count == 0) {
                // 没有更多数据了
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                [self.dataList addObjectsFromArray:models];
            }
        }
        
        [self reloadData];
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LY_LiveRecordTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LiveRecordIdentifier" forIndexPath:indexPath];
    cell.record = self.dataList[indexPath.row];
    return cell;
}

- (void)handleTap:(UITapGestureRecognizer *)tap {
    [self.navigationController pushViewController:[LastMonthLiveRecordController new] animated:YES];
}



@end
