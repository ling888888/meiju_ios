//
//  LiveFreeGiftViewController.h
//  SimpleDate
//
//  Created by ling Hou on 2020/4/22.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveFreeGiftViewController : BaseViewController

@property (nonatomic, strong) NSMutableArray * dataArray;


@end

NS_ASSUME_NONNULL_END
