//
//  LY_LiveFollowViewController.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_LiveFollowViewController.h"
#import "LY_LiveFollowTableViewCell.h"
#import "LY_HomePageViewController.h"
#import "LiveRoomViewController.h"
#import "LiveModel.h"
#import "ZegoKitManager.h"

@interface LY_LiveFollowViewController ()

//@property(nonatomic, strong) NSArray *dataList;

@end

@implementation LY_LiveFollowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavigation];
    
    [self setupUI];
    
    [self setupUIFrame];
    
    [self loadNetDataWith:RequestNetDataTypeRefresh];
    
    //[MobClick event:@"FM1_10_1"];
}

- (void)setupNavigation {
    self.title = @"我的关注".localized;
    [self.navigationController.navigationBar vhl_setBackgroundColor:[UIColor whiteColor]];
}

- (void)setupUI {
    self.tableView.tipText = @"暂时还没有关注的主播".localized;
    self.tableView.rowHeight = 75;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[LY_LiveFollowTableViewCell class] forCellReuseIdentifier:@"LiveFollowIdentifier"];
}

- (void)setupUIFrame {
    
}

- (void)loadNetDataWith:(RequestNetDataType)type {
    [super loadNetDataWith:type];
    NSDictionary * params = self.userId.notEmpty ? @{@"page": @(self.pageNumber),@"userId":self.userId}:@{@"page": @(self.pageNumber)};
    [LY_Network getRequestWithURL:self.userId.notEmpty ? LY_Api.followOtherList:LY_Api.followList parameters:params success:^(id  _Nullable response) {
        NSArray *dataArray = response[@"list"];
        NSMutableArray *models = [NSMutableArray array];
        for (NSDictionary *data in dataArray) {
            LY_LiveChiefAnnouncer *chiefAnnouncer = [LY_LiveChiefAnnouncer yy_modelWithDictionary:data];
            [models addObject:chiefAnnouncer];
        }
        if (type == RequestNetDataTypeRefresh) {
            self.dataList = models;
        } else {
            if (models.count == 0) {
                // 没有更多数据了
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                [self.dataList addObjectsFromArray:models];
            }
        }
        
        self.tableView.showEmptyTipView = self.dataList.count == 0 ? true : false;
        
        [self reloadData];
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LY_LiveFollowTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LiveFollowIdentifier" forIndexPath:indexPath];
    
    cell.chiefAnnouncer = self.dataList[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    //[MobClick event:@"FM1_10_1_1"];
    LY_LiveChiefAnnouncer *chiefAnnouncer = self.dataList[indexPath.row];
    
    // 判断是否正在直播
    if (chiefAnnouncer.living) {
        LiveModel * model = [LiveModel new];
        model.userId = chiefAnnouncer.userId;
        model.liveId = chiefAnnouncer.liveId;
        model.portrait = chiefAnnouncer.portrait;
        [ZegoManager joinLiveRoomWithliveInfo:model isAnchor:NO];
    } else {
        // 未直播
        [SPDCommonTool pushToDetailTableViewController:self userId:chiefAnnouncer.userId];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.userId.notEmpty && ![[SPDApiUser currentUser].userId isEqualToString:self.userId]) {
        return NO;
    }else{
        return YES;
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewCellEditingStyleDelete;
}

- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    __weak typeof(self) weakSelf = self;
    UITableViewRowAction *action = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"取消关注".localized handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        //[MobClick event:@"FM1_10_1_2"];
        LY_LiveChiefAnnouncer *chiefAnnouncer = weakSelf.dataList[indexPath.row];
        [weakSelf.dataList removeObject:chiefAnnouncer];
        [weakSelf.tableView reloadData];
        NSDictionary *parasm = @{@"userId": chiefAnnouncer.userId};
        [LY_Network postRequestWithURL:LY_Api.unfollow parameters:parasm success:^(id  _Nullable response) {
        } failture:^(NSError * _Nullable error) {
            
        }];
    }];
    
    return ((self.userId.notEmpty &&[self.userId isEqualToString:[SPDApiUser currentUser].userId]) || !self.userId)? @[action]:@[];
}

@end
