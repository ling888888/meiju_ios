//
//  PKScoreViewController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/8/20.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PKScoreViewController.h"
#import "LY_FansGroupRuleViewController.h"
#import "LY_HalfNavigationController.h"

@interface PKScoreViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lianshengLabel;
@property (weak, nonatomic) IBOutlet UILabel *changciLabel;
// 场次label
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel; // 总得分

@end

@implementation PKScoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"PK积分".localized;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ic_zhibojian_PK_guize"] style:UIBarButtonItemStylePlain target:self action:@selector(hanldeRule:)];
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithHexString:@"#ffffff"];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"ffffff"],
      NSFontAttributeName: [UIFont mediumFontOfSize:18]
    };
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:0.0];
    [self.navigationController.navigationBar vhl_setShadowImageHidden:true];
    [self.navigationController.navigationBar setTranslucent:true];
    [self requestData];
    
}


- (void)hanldeRule:(UIBarButtonItem *)item {
    NSString *urlString = [NSString stringWithFormat:@"https://share.famy.ly/ar/pkRule.html?lang=%@&type=live", [SPDCommonTool getFamyLanguage]];
    LY_FansGroupRuleViewController *webVC = [[LY_FansGroupRuleViewController alloc] initWithURLString:urlString];
    webVC.title = @"PK规则".localized;
    LY_HalfNavigationController * nav = [[LY_HalfNavigationController alloc]initWithRootViewController:webVC controllerHeight:275+IphoneX_Bottom];
    [self presentViewController:nav animated:YES completion:nil];
}

- (void)requestData {
    [RequestUtils GET:URL_NewServer(@"live/pk.score") parameters:@{} success:^(id  _Nullable suceess) {
        
        self.lianshengLabel.text = [NSString stringWithFormat:@"连胜:%@".localized,[suceess[@"pkWinCount"] stringValue]];
        self.changciLabel.text = [NSString stringWithFormat:@"场次:%@".localized,[suceess[@"pkCount"] stringValue]];

        self.scoreLabel.text = [suceess[@"finScore"] stringValue];
        
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

@end
