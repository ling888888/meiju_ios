//
//  PKContributionListController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/8/20.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PKContributionListController.h"
#import "LY_LiveRankTableViewCell.h"
#import "LY_LiveAudience.h"
#import "LY_LiveRankTableViewCell.h"
#import "LY_HomePageViewController.h"


@interface PKContributionListController ()


@end

@implementation PKContributionListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    
    [self setupUIFrame];
    
//    [self loadNetDataWith:RequestNetDataTypeRefresh];
    [self.tableView.mj_header beginRefreshing];
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithHexString:@"1a1a1a"];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"1a1a1a"],
      NSFontAttributeName: [UIFont mediumFontOfSize:18]
    };
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:1.0];
    [self.navigationController.navigationBar vhl_setShadowImageHidden:true];
    [self.navigationController.navigationBar setTranslucent:false];

}

- (void)setType:(NSString *)type {
    _type = type;
    self.navigationItem.title = [_type isEqualToString:@"0"] ? @"我方PK贡献榜".localized : @"对方PK贡献榜".localized;
    if ([_type isEqualToString:@"0"]) {
        self.tableView.tipButtonTitle = @"立即送礼".localized;
        self.tableView.tipImageName = @"img_kongyemian_songliwu";
        self.tableView.tipButton.backgroundColor = [UIColor colorWithHexString:@"#6A2CF7"];
        self.tableView.tipButton.titleLabel.font = [UIFont mediumFontOfSize:15];
        [self.tableView.tipButton addTarget:self action:@selector(handleSendGift) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)handleSendGift {
    [self dismissViewControllerAnimated:YES completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LiveRoomShowGiftViewNotification" object:nil];
}

- (void)setupUI {
    
    self.tableView.tipText = @"送礼助主播获胜，还能争夺MVP".localized;
    self.tableView.rowHeight = 70;
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 75, 0, 0);
    self.tableView.separatorColor = [UIColor colorWithHexString:@"#EFEFEF"];
    [self.tableView registerClass:[LY_LiveRankTableViewCell class] forCellReuseIdentifier:@"RankIdentifier"];
    self.tableView.mj_footer = nil;

}

- (void)setupUIFrame {

}

- (void)loadNetDataWith:(RequestNetDataType)type {
    if (!self.pkId || !self.liveId) {
        return;
    }
    NSDictionary *params =@{@"pkId": self.pkId,
                            @"liveId": self.liveId,
    };
    [LY_Network getRequestWithURL:LY_Api.live_pk_score_list parameters:params success:^(id  _Nullable response) {
        NSArray *dataArray = response[@"list"];
        NSMutableArray *models = [NSMutableArray array];
        for (NSDictionary *data in dataArray) {
            LY_LiveAudience *audience = [LY_LiveAudience yy_modelWithDictionary:data];
            [models addObject:audience];
        }
        self.dataList = models;

        if (self.dataList.count == 0) {
//            if (![self.entrance isEqualToString:@"mine"]) {
//                self.tableVi                    ew.tipButton = self.emptyTipGiftingBtn;
//            }
            self.tableView.showEmptyTipView = true;
        } else {
            self.tableView.showEmptyTipView = false;
        }
        [self reloadData];
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LY_LiveRankTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RankIdentifier" forIndexPath:indexPath];
    LY_LiveAudience *liveAudience = self.dataList[indexPath.row];
    liveAudience.ranking = (int)indexPath.row + 1;
    cell.liveAudience = liveAudience;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    [self dismissViewControllerAnimated:true completion:nil];
    LY_LiveAudience *liveAudience = self.dataList[indexPath.row];
    LY_HomePageViewController * detailTableViewController = [[LY_HomePageViewController alloc] initWithUserId:liveAudience.userId];
    [[LY_Macro currentNavigationController] pushViewController:detailTableViewController animated:true];
}

- (void)emptyTipGiftingBtnAction {
//    if ([self.delegate respondsToSelector:@selector(liveRankListViewControllerDidClick:)]) {
//        [self.delegate liveRankListViewControllerDidClick:self.emptyTipGiftingBtn];
//    }
}


@end
