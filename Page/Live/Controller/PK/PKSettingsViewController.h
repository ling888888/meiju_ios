//
//  PKSettingsViewController.h
//  SimpleDate
//
//  Created by ling Hou on 2020/9/9.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PKSettingsViewController : UIViewController

@property (nonatomic, assign) BOOL on;

@end

NS_ASSUME_NONNULL_END
