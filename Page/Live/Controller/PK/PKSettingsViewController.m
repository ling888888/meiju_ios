//
//  PKSettingsViewController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/9.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PKSettingsViewController.h"

@interface PKSettingsViewController ()

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UISwitch * autoSwitch;

@end

@implementation PKSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"PK设置".localized;
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.titleLabel];
    [self.view addSubview:self.autoSwitch];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(18);
        make.top.mas_equalTo(10+ naviBarHeight);
    }];
    [self.autoSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10+ naviBarHeight);
        make.trailing.mas_equalTo(-18);
    }];
    [_autoSwitch addTarget:self action:@selector(handleLinkMicSwitch:) forControlEvents:UIControlEventValueChanged];
    _autoSwitch.on = _on;
}

- (void)handleLinkMicSwitch:(UISwitch *)sender {
    // 0 接受 1 不接受
    [RequestUtils POST:URL_NewServer(@"live/pk.accept") parameters:@{@"status":@(!sender.on)} success:^(id  _Nullable suceess) {
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = [UIFont mediumFontOfSize:16];
        _titleLabel.textColor = SPD_HEXCOlOR(@"1A1A1A");
        _titleLabel.text = @"接受PK邀请".localized;
    }
    return _titleLabel;
}

- (UISwitch *)autoSwitch {
    if (!_autoSwitch) {
        _autoSwitch = [[UISwitch alloc]init];
        _autoSwitch.onTintColor = SPD_HEXCOlOR(@"6A2CF7");
    }
    return _autoSwitch;
}



@end
