//
//  PKContributionListController.h
//  SimpleDate
//
//  Created by ling Hou on 2020/8/20.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_BaseTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PKContributionListController : LY_BaseTableViewController

@property (nonatomic, strong) NSString * type; // 0 我方 1 对方

@property (nonatomic, strong) NSString * liveId;

@property (nonatomic, strong) NSString * pkId;

@end

NS_ASSUME_NONNULL_END
