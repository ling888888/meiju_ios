//
//  PKInviteViewController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/6/22.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "PKInviteViewController.h"
#import "LiveInvitePKCell.h"
#import "LY_FansGroupRuleViewController.h"
#import "ZegoKitManager.h"
#import "PKSettingsViewController.h"

@interface PKInviteViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView * collectionView;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) UIImageView * matchView; // 快速
@property (nonatomic, strong) UIButton * beginButton;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) NSMutableArray * dataArray;

@property (nonatomic, assign) BOOL acceptPk;

@end

@implementation PKInviteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    // nav
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ic_zhibojian_PK_guize"] style:UIBarButtonItemStylePlain target:self action:@selector(handleRule)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ic_zhibojian_PK_shezhi"] style:UIBarButtonItemStylePlain target:self action:@selector(handleSetting)];

    
    self.page = 1;
    
    self.navigationItem.title = @"发起PK".localized;
    self.matchView = [UIImageView new];
    self.matchView.image = [UIImage imageNamed:[SPDCommonTool getArImageNameWithImage:@"img_zhibo_pk_faqipk"]];
    self.matchView.userInteractionEnabled = YES;
    [self.view addSubview:self.matchView];
    [self.matchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(61);
        make.height.mas_equalTo(107);
        make.width.mas_equalTo(kScreenW);
        make.centerX.mas_equalTo(0);
    }];
    
    self.beginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.beginButton addTarget:self action:@selector(handleMatch:) forControlEvents:UIControlEventTouchUpInside];
    [self.beginButton setTitle:@"开始".localized forState:UIControlStateNormal];
    [self.beginButton setBackgroundImage:[UIImage imageNamed:@"btn_zhibo_pk_faqipk"] forState:UIControlStateNormal];
    self.beginButton.titleLabel.font = [UIFont mediumFontOfSize:18];
    self.beginButton.layer.cornerRadius = 15;
    [self.matchView addSubview:self.beginButton];
    [self.beginButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 68));
        make.centerY.equalTo(self.matchView.mas_centerY).offset(0);
        make.trailing.mas_offset(-5);
    }];
    
    
    self.titleLabel = [UILabel new];
    self.titleLabel.text = @"邀请PK".localized;
    self.titleLabel.font = [UIFont mediumFontOfSize:15];
    [self.view addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.equalTo(self.matchView.mas_bottom).offset(19);
    }];
    
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(15);
        make.leading.and.trailing.mas_equalTo(0);
        make.bottom.mas_equalTo(-IphoneX_Bottom);
    }];
    [self requestPKList];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self requestData];
}

- (void)requestData {
    [RequestUtils GET:URL_NewServer(@"live/pk.accept.status") parameters:@{} success:^(id  _Nullable suceess) {
        _acceptPk = [suceess[@"acceptPk"] boolValue];
    } failure:^(id  _Nullable failure) {
        
    }];
}

- (void)handleSetting {
    PKSettingsViewController * vc = [PKSettingsViewController new];
    vc.on = !_acceptPk;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)handleMatch:(UIButton *)sender {
    if (ZegoManager.linkMicLinkingDic.count > 0) {
        [self showTips:@"连麦中，请结束后PK".localized];
        return;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    if (self.delegate && [self.delegate respondsToSelector:@selector(didClickedPkMatch)]) {
        [self.delegate didClickedPkMatch];
    }
}

- (void)requestPKList {
    [RequestUtils GET:URL_NewServer(@"live/pk.list") parameters:@{@"page":@(self.page)} success:^(id  _Nullable suceess) {
        self.dataArray = [suceess[@"list"] mutableCopy];
        [self.collectionView reloadData];
        if (self.page == 1) {
            [self.collectionView.mj_header endRefreshing];
        }else{
            [self.collectionView.mj_footer endRefreshing];
        }
  
        self.collectionView.mj_footer.hidden = (self.dataArray.count < 30);

    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)handleRule {
    NSString *urlString = [NSString stringWithFormat:@"https://share.famy.ly/ar/pkRule.html?lang=%@&type=live", [SPDCommonTool getFamyLanguage]];
    LY_FansGroupRuleViewController *webVC = [[LY_FansGroupRuleViewController alloc] initWithURLString:urlString];
    webVC.title = @"PK规则".localized;
    [self.navigationController pushViewController:webVC animated:true];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    LiveInvitePKCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveInvitePKCell" forIndexPath:indexPath];
    cell.dict = self.dataArray[indexPath.row];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (ZegoManager.linkMicLinkingDic.count > 0) {
        [self showTips:@"连麦中，请结束后PK".localized];
        return;
    }
//    LiveInvitePKCell * cell = (LiveInvitePKCell *)[collectionView cellForItemAtIndexPath:indexPath];
//    cell.inviteButton.selected = YES;
//    [cell.inviteButton setBackgroundColor:SPD_HEXCOlOR(@"A17FEF")];
    [self dismissViewControllerAnimated:YES completion:nil];
    if (self.delegate && [self.delegate respondsToSelector:@selector(didClickedPkInviteWithUserId:)]) {
        [self.delegate didClickedPkInviteWithUserId:self.dataArray[indexPath.row][@"userId"]];
    }
}

#pragma mark - getters

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout new];
        layout.scrollDirection =  UICollectionViewScrollDirectionVertical;
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        layout.itemSize = CGSizeMake(self.view.bounds.size.width, 70);
        _collectionView = [[UICollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerClass:[LiveInvitePKCell class] forCellWithReuseIdentifier:@"LiveInvitePKCell"];
        _collectionView.backgroundColor = [UIColor whiteColor];
        __weak typeof(self) weakSelf = self;
        _collectionView.mj_header = [HL_GIFRefreshHeader headerWithRefreshingBlock:^{
            weakSelf.page = 1;
            [weakSelf requestPKList];
        }];
        _collectionView.mj_footer = [HL_GIFRefreshFooter footerWithRefreshingBlock:^{
            weakSelf.page ++;
            [weakSelf requestPKList];
        }];
    }
    return _collectionView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

@end
