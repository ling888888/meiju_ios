//
//  PKInviteViewController.h
//  SimpleDate
//
//  Created by ling Hou on 2020/6/22.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol PKInviteViewControllerDelegate <NSObject>

@optional
- (void)didClickedPkMatch;
- (void)didClickedPkInviteWithUserId:(NSString * )userId;


@end

@interface PKInviteViewController : UIViewController

@property (nonatomic, weak)id<PKInviteViewControllerDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
