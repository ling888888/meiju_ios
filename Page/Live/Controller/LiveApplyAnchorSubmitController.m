//
//  LiveApplyAnchorSubmitController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/5/18.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveApplyAnchorSubmitController.h"
#import <AVFoundation/AVFoundation.h>
#import "LiveApplyAnchorSubmitInfoController.h"
#import <AVFoundation/AVFoundation.h>

@interface LiveApplyAnchorSubmitController ()<AVAudioRecorderDelegate,AVAudioPlayerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *arButton;
@property (weak, nonatomic) IBOutlet UIButton *enButton;
@property (weak, nonatomic) IBOutlet UIImageView *bg;
@property (weak, nonatomic) IBOutlet UILabel *beginLabel;
@property (weak, nonatomic) IBOutlet UIButton *beginButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topCons;
@property (nonatomic, copy) NSArray * dataArray;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, copy) NSString * selectedLang;
@property (weak, nonatomic) IBOutlet UIView *recordView;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property (nonatomic, strong) AVAudioRecorder * recorder;
@property (nonatomic, strong) NSTimer * timer;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, assign) NSInteger sec;
@property (nonatomic, strong) NSURL * recorderFileStr;
@property (nonatomic, strong) AVAudioPlayer * audioPlayer;
@property (weak, nonatomic) IBOutlet UIButton *againBtn;
@property (weak, nonatomic) IBOutlet UILabel *agaginLabel;
@property (weak, nonatomic) IBOutlet UIButton *uploadButton;
@property (weak, nonatomic) IBOutlet UILabel *uploadLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomCons;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (nonatomic, assign) BOOL isDelete;
@property (weak, nonatomic) IBOutlet UILabel *descLabel1;
@property (nonatomic, assign) NSInteger playSec;

@end

@implementation LiveApplyAnchorSubmitController

// 此时此刻我的内心非常烦躁

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"提交申请材料".localized;
    self.topCons.constant = naviBarHeight + 10;
    self.bottomCons.constant = IphoneX_Bottom + 20;
    
    self.descLabel.textAlignment = kIsMirroredLayout ? NSTextAlignmentRight: NSTextAlignmentLeft;
    self.descLabel1.textAlignment = kIsMirroredLayout ? NSTextAlignmentRight: NSTextAlignmentLeft;

    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryRecord error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    NSMutableDictionary *recordSettings = [[NSMutableDictionary alloc] init];
    [recordSettings setValue :[NSNumber numberWithInt:kAudioFormatLinearPCM] forKey: AVFormatIDKey];
    [recordSettings setValue:[NSNumber numberWithInt:AVAudioQualityMin] forKey:AVEncoderAudioQualityKey];
    [recordSettings setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    self.recorderFileStr = [NSURL URLWithString:[ [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"anchor.caf"]];
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:self.recorderFileStr error:nil];
    _audioPlayer.delegate = self;
    
    _recorder = [[AVAudioRecorder alloc]initWithURL:_recorderFileStr settings:recordSettings error:nil];
    _recorder.delegate = self;
    
    self.recordView.hidden = YES;
    self.index = 0;
    [self requestData];
    
    self.iconImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handle)];
    [self.iconImageView addGestureRecognizer:tap];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.audioPlayer.isPlaying) {
        [self.audioPlayer pause];
        [self configPlayStatus:NO];
    }
    if (self.recorder.isRecording) {
        [self.recorder stop];
        self.recordView.backgroundColor = SPD_HEXCOlOR(@"#6A2CF7");
        self.iconImageView.image = [UIImage imageNamed:@"ic_zhaomuzhubo_shiting"];
        self.titleLabel.text = @"试听".localized;
        self.titleLabel.textColor = [UIColor whiteColor];
        self.timerLabel.textColor = [UIColor whiteColor];
        self.beginButton.hidden = YES;
        self.beginButton.selected = NO;
        self.beginLabel.hidden = YES;
        self.againBtn.hidden = NO;
        self.agaginLabel.hidden = NO;
        self.uploadButton.hidden = NO;
        self.uploadLabel.hidden = NO;
    }
}

- (void)dealloc {
    NSLog(@"LiveApplyAnchorSubmitController dealloc");
}

- (void)requestData {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"live.apply.read.list" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.dataArray = suceess[@"list"];
        self.selectedLang = @"ar";
    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)setSelectedLang:(NSString *)selectedLang {
    _selectedLang = selectedLang;
    self.textView.text = self.dataArray[_index][_selectedLang];
}

- (IBAction)arButoonClicked:(UIButton *)sender {
    if ([self.selectedLang isEqualToString:@"ar"]) {
        return;
    }
    self.selectedLang = @"ar";
    [sender setTitleColor:SPD_HEXCOlOR(@"ffffff") forState:UIControlStateNormal];
    [self.enButton setTitleColor:SPD_HEXCOlOR(@"1a1a1a") forState:UIControlStateNormal];
    UIImage * bg = [UIImage imageNamed:@"ic_zhaomuzhubo_qiehuan"];
    self.bg.image = kIsMirroredLayout ?  bg.mirroredImage : bg;
}

- (IBAction)enButtonClicked:(UIButton *)sender {
    self.selectedLang = @"en";
    [sender setTitleColor:SPD_HEXCOlOR(@"ffffff") forState:UIControlStateNormal];
    [self.arButton setTitleColor:SPD_HEXCOlOR(@"1a1a1a") forState:UIControlStateNormal];
    UIImage * bg = [UIImage imageNamed:@"ic_zhaomuzhubo_qiehuan_2"];
    self.bg.image = kIsMirroredLayout ?  bg.mirroredImage : bg;
}

- (IBAction)changeBtnClicked:(UIButton *)sender {
    NSInteger index = arc4random() % (self.dataArray.count);
    self.index = index;
     self.textView.text = self.dataArray[_index][_selectedLang];
}

// 开始录制
- (IBAction)beginButtonClicked:(UIButton *)sender {
    AVAuthorizationStatus microPhoneStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
    if (microPhoneStatus != AVAuthorizationStatusAuthorized) {
        // alert
        if (microPhoneStatus == AVAuthorizationStatusNotDetermined) {
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted) {

            }];
        }else if (microPhoneStatus ==AVAuthorizationStatusDenied){
            [self presentAlertWithTitle:@"提示".localized message:@"启用麦克风访问权限".localized cancelTitle:@"取消".localized cancelHandler:^(UIAlertAction * _Nonnull action) {
                
            } actionTitle:@"去设置".localized actionHandler:^(UIAlertAction * _Nonnull action) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [UIApplication.sharedApplication openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:nil completionHandler:nil];
                });
            }];
        }
        return;
    }
    if (!sender.selected) {
        self.recordView.hidden = NO;
        [self.recorder prepareToRecord];
        [self.recorder record];
        [self stopTimer];
        if (_timer == nil) {
            _timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(handleTimer:) userInfo:nil repeats:YES];
            [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
        }
        [self.timer fire];
    }else{
        [self.recorder stop];
    }
    self.beginLabel.text = !sender.selected ? @"录制中".localized:@"点击录制".localized;
    sender.selected = !sender.selected;
}
- (IBAction)recordAgain:(id)sender {
    __weak typeof(self) WeakSelf = self;
    LYPSTextAlertView * view = [LYPSTextAlertView new];
    view.title = @"提示".localized;
    view.message = @"点击确定后将删除本次的录音".localized;
    [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"取消") style:LYPSystemAlertActionStyleCancel handler:^(LYPSystemAlertAction * _Nonnull action) {
        
    }]];
    [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"确定") style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
        if (self.audioPlayer.isPlaying) {
             [self.audioPlayer stop];
        }
        [[NSFileManager defaultManager] removeItemAtPath:[ [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"anchor.caf"] error:nil];
        WeakSelf.isDelete = YES;
        WeakSelf.sec = 0;
        WeakSelf.beginButton.hidden = NO;
        WeakSelf.titleLabel.textColor = SPD_HEXCOlOR(@"1a1a1a");
        WeakSelf.timerLabel.textColor = SPD_HEXCOlOR(@"1a1a1a");
        WeakSelf.beginLabel.hidden = NO;
        WeakSelf.againBtn.hidden = YES;
        WeakSelf.agaginLabel.hidden = YES;
        WeakSelf.uploadButton.hidden = YES;
        WeakSelf.uploadLabel.hidden = YES;
        WeakSelf.recordView.hidden = YES;
        WeakSelf.beginButton.selected = NO;
        WeakSelf.recordView.backgroundColor = [SPD_HEXCOlOR(@"#F8145B") colorWithAlphaComponent:0.1];
        WeakSelf.iconImageView.image = [UIImage imageNamed:@"ic_zhaomuzhubo_hongdian"];
        WeakSelf.titleLabel.text = @"REC";
        //需要设置为录音状态
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryRecord error:nil];
        
    }]];
    [view present];
}

- (IBAction)uploadVoice:(id)sender {
    [self.navigationController pushViewController:[LiveApplyAnchorSubmitInfoController new] animated:YES];
}

#pragma mark - AVAudioRecorderDelegate

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag {
    [self stopTimer];
    self.recordView.backgroundColor = SPD_HEXCOlOR(@"#6A2CF7");
    self.iconImageView.image = [UIImage imageNamed:@"ic_zhaomuzhubo_shiting"];
    self.titleLabel.text = @"试听".localized;
    self.titleLabel.textColor = [UIColor whiteColor];
    self.timerLabel.textColor = [UIColor whiteColor];
    self.beginButton.hidden = YES;
    self.beginLabel.hidden = YES;
    self.againBtn.hidden = NO;
    self.agaginLabel.hidden = NO;
    self.uploadButton.hidden = NO;
    self.uploadLabel.hidden = NO;
}

- (void)audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder error:(NSError * __nullable)error {
    NSLog(@"AVAudioRecorder");
}

#pragma mark - AVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
   
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError * __nullable)error {
}

- (void)handle {
    if (self.recorder.isRecording) {
        return;
    }
    if (self.audioPlayer.isPlaying) {
        [self.audioPlayer pause];
        [self configPlayStatus:NO];
    }else{
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategorySoloAmbient error:nil];
        if (self.isDelete) {
            self.isDelete = NO;
            self.audioPlayer = nil;
            _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:self.recorderFileStr error:nil];
            _audioPlayer.delegate = self;
        }
        [self.audioPlayer prepareToPlay];
        [self.audioPlayer play];
        [self configPlayStatus:YES];
        [self stopTimer];
        if (_timer == nil) {
            _timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(handleListenTimer:) userInfo:nil repeats:YES];
            [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
        }
        [self.timer fire];

    }
}

- (void)configPlayStatus:(BOOL)isPlaying {
    self.titleLabel.text = isPlaying ? @"试听中".localized : @"试听".localized;
    self.iconImageView.image = [UIImage imageNamed:isPlaying? @"ic_zhaomuzhubo_shitingzhong": @"ic_zhaomuzhubo_shiting"];
    if (!isPlaying) {
        [self stopTimer];
    }
}

- (void)handleTimer:(NSTimer *)timer {
    _sec ++;
    _playSec =_sec;
    self.timerLabel.text = [NSString stringWithFormat:@"%02d:%02d",_sec/ 60,_sec % 60];
}

- (void)handleListenTimer:(NSTimer *)timer {
    if (_playSec == 0) {
        [self.audioPlayer stop];
        [self configPlayStatus:NO];
        self.timerLabel.text = [NSString stringWithFormat:@"%02d:%02d",_sec/ 60,_sec % 60];
        _playSec = _sec;
        return;
    }
    self.timerLabel.text = [NSString stringWithFormat:@"%02d:%02d",_playSec/ 60,_playSec % 60];
    _playSec --;
    
}

- (NSArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSArray array];
    }
    return _dataArray;
}


- (void)stopTimer {
    [self.timer invalidate];
    self.timer = nil;
}

@end
