//
//  LY_LiveAudienceHalfVC.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/17.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "WMPageController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol LY_LiveAudienceHalfVCDelegate <NSObject>

- (void)liveAudienceHalfVCDidClick:(UIButton *)giftingBtn;

@end

@interface LY_LiveAudienceHalfVC : WMPageController

@property (nonatomic, weak) id<LY_LiveAudienceHalfVCDelegate> giftingBtnDelegate;

@property(nonatomic, copy) NSString *type;

@property(nonatomic, copy) NSString *liveId;

- (instancetype)initWithControllerHeight:(CGFloat)controllerHeight;

@property (nonatomic, assign) BOOL showInviteOnlineLinkMic; // 显示邀请

@end

NS_ASSUME_NONNULL_END
