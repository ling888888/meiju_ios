//
//  LY_FunsGroupCreatViewController.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@class LY_FunsGroupCreatViewController;

@protocol LY_FunsGroupCreatViewControllerDelegate <NSObject>

- (void)funsGroupCreatViewController:(LY_FunsGroupCreatViewController *)funsGroupCreatViewController checkStatus:(NSString *)checkStatus;

@end

@interface LY_FunsGroupCreatViewController : LY_BaseViewController

@property(nonatomic, strong) id<LY_FunsGroupCreatViewControllerDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
