//
//  LY_LiveAudienceListViewController.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_BaseTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_LiveAudienceListViewController : LY_BaseTableViewController

@property(nonatomic, copy)NSString *liveId;

@property (nonatomic, assign) BOOL showInviteOnlineLinkMic; // 显示邀请

@end

NS_ASSUME_NONNULL_END
