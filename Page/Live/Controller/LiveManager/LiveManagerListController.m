//
//  LiveManagerListController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/16.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveManagerListController.h"
#import "LiveManagerListTableCell.h"
#import "LiveUserModel.h"
#import "LiveChangeManagerMessage.h"
#import "ZegoKitManager.h"
#import "LiveModel.h"

NSInteger const PositionWeightAnchor = 180;
NSInteger const PositionWeightManager = 100;
NSInteger const PositionWeightAudience = 20;

@interface LiveManagerListController ()<UITableViewDelegate,UITableViewDataSource,LiveManagerListTableCellDelegate>

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * dataList;

@end

@implementation LiveManagerListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"管理员".localized;
    
    [self.view addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(64);
        make.leading.mas_equalTo(10);
        make.trailing.mas_equalTo(-10);
    }];
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.trailing.mas_equalTo(0);
        make.top.equalTo(self.titleLabel.mas_bottom).mas_equalTo(10);
        make.bottom.mas_equalTo(-IphoneX_Bottom);
    }];
    
    [self requestManagerList];

}

#pragma mark - request

- (void)requestManagerList{
    [RequestUtils GET:URL_NewServer(@"live/admin.list") parameters:@{} success:^(id  _Nullable suceess) {
        [self.dataList removeAllObjects];
        for (NSDictionary * dic in suceess) {
            LiveUserModel * model = [LiveUserModel initWithDictionary:dic];
            [self.dataList addObject:model];
        }
        self.tableView.showEmptyTipView = (self.dataList.count == 0);

        [self.tableView reloadData];
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)requestRemoveManager:(LiveUserModel *)model {
    [RequestUtils POST:URL_NewServer(@"live/remove.admin") parameters:@{@"lUserId":model.userId} success:^(id  _Nullable suceess) {
        [self showTips:@"操作成功".localized];
        [self.dataList removeObject:model];
        [self.tableView reloadData];
        self.tableView.showEmptyTipView = (self.dataList.count == 0);
        
        LiveChangeManagerMessage * msg = [LiveChangeManagerMessage new];
        msg.type = @"remove";
        msg.userId = model.userId;
        [[RCIMClient sharedRCIMClient] sendMessage:ConversationType_CHATROOM targetId:ZegoManager.liveInfo.liveId content:msg pushContent:@"" pushData:@"" success:^(long messageId) {
            
        } error:^(RCErrorCode nErrorCode, long messageId) {
            
        }];
        
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LiveManagerListTableCell * cell = [tableView dequeueReusableCellWithIdentifier:@"LiveManagerListTableCell"];
    cell.model = self.dataList[indexPath.row];
    cell.delegate = self;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  70.0f;
}

- (void)liveManagerListTableCell:(LiveManagerListTableCell *)cell didCancelManager:(LiveUserModel *)model {
    [self requestRemoveManager:model];
}

#pragma mark - getters

- (NSMutableArray *)dataList {
    if (!_dataList) {
        _dataList = [NSMutableArray new];
    }
    return _dataList;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = [UIFont systemFontOfSize:13];
        _titleLabel.textColor = SPD_HEXCOlOR(@"1a1a1a");
        _titleLabel.numberOfLines = 0;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.text = @"管理员拥有直播间禁言、踢出、邀请上麦权限".localized;
    }
    return _titleLabel;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]init];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.showEmptyTipView = YES;
        _tableView.tipText = @"暂未设置，点击用户资料“管理”即可设置".localized;
        _tableView.tipImageName = @"img_zhibo_fensiquan_kongyemian";
        _tableView.separatorColor = SPD_HEXCOlOR(@"e7e7e7");
        _tableView.separatorInset = UIEdgeInsetsMake(0, 20, 0, 0);
        [_tableView registerNib:[UINib nibWithNibName:@"LiveManagerListTableCell" bundle:nil] forCellReuseIdentifier:@"LiveManagerListTableCell"];
    }
    return _tableView;
}

@end
