//
//  LiveManagerListController.h
//  SimpleDate
//
//  Created by ling Hou on 2020/9/16.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

UIKIT_EXTERN NSInteger const PositionWeightAnchor;
UIKIT_EXTERN NSInteger const PositionWeightManager;
UIKIT_EXTERN NSInteger const PositionWeightAudience;


@interface LiveManagerListController : UIViewController

@end

NS_ASSUME_NONNULL_END
