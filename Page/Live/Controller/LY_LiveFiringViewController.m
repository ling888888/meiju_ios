//
//  LY_LiveFiringViewController.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/24.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_LiveFiringViewController.h"
#import "LY_TextView.h"
#import "UITextView+PlaceHolder.h"
#import "LYPSActionSheetView.h"
#import "LY_LiveFiringMoreActionSheetView.h"
#import "LY_LiveWalletViewController.h"
#import "LY_LiveRecordViewController.h"
#import "ZegoKitManager.h"
#import "LiveModel.h"
#import "LiveAnchorWalletController.h"

@interface LY_LiveFiringViewController () <LY_LiveFiringMoreActionSheetViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate, UITextViewDelegate>

@property(nonatomic, strong) UIButton *coverImgBtn;

@property(nonatomic, strong) UILabel *addCoverImgLabel;

@property(nonatomic, strong) LY_TextView *titleTextView;

@property(nonatomic, strong) UIImageView *fansIconView;

@property(nonatomic, strong) UIImageView *linkIconView; // 开启连麦

@property(nonatomic, strong) UILabel *linkMicLabel;

@property(nonatomic, strong) UISwitch *linkMicSwitch;

@property(nonatomic, strong) UILabel *noticeFansLabel;

@property(nonatomic, strong) UISwitch *noticeFansSwitch;

@property(nonatomic, strong) UIButton *startupBtn;

@property(nonatomic, strong) UIButton *moreBtn;

@property(nonatomic, strong) UIImagePickerController *imagePickerController;

@property(nonatomic, copy) NSString *liveTitle;

@property(nonatomic, copy) NSString *liveIsNotiFans;
@property(nonatomic, copy) NSString *liveId;
@property(nonatomic, strong) UIImage *liveCover;

@end

@implementation LY_LiveFiringViewController

- (UIButton *)coverImgBtn {
    if (!_coverImgBtn) {
        _coverImgBtn = [[UIButton alloc] init];
        [_coverImgBtn setImage:[UIImage imageNamed:@"ic_zhibo_kaishizhibo_morentu"] forState:UIControlStateNormal];
        _coverImgBtn.layer.cornerRadius = 7;
        _coverImgBtn.layer.masksToBounds = true;
        [_coverImgBtn addTarget:self action:@selector(coverImgBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _coverImgBtn;
}

- (UILabel *)addCoverImgLabel {
    if (!_addCoverImgLabel) {
        _addCoverImgLabel = [[UILabel alloc] init];
        _addCoverImgLabel.text = @"添加封面".localized;
        _addCoverImgLabel.textColor = [UIColor whiteColor];
        _addCoverImgLabel.textAlignment = NSTextAlignmentCenter;
        _addCoverImgLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
        _addCoverImgLabel.font = [UIFont mediumFontOfSize:15];
    }
    return _addCoverImgLabel;
}

- (LY_TextView *)titleTextView {
    if (!_titleTextView) {
        _titleTextView = [[LY_TextView alloc] init];
        _titleTextView.delegate = self;
        _titleTextView.font = [UIFont mediumFontOfSize:15];
        _titleTextView.textColor = [[UIColor colorWithHexString:@"#333333"] colorWithAlphaComponent:0.7];
        _titleTextView.textContainerInset = UIEdgeInsetsMake(5, 5, 5, 5);
        _titleTextView.backgroundColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.1];
        _titleTextView.layer.cornerRadius = 5;
        _titleTextView.layer.masksToBounds = true;
        _titleTextView.returnKeyType = UIReturnKeyDone;
    }
    return _titleTextView;
}

- (UIImageView *)fansIconView {
    if (!_fansIconView) {
        _fansIconView = [[UIImageView alloc] init];
        _fansIconView.contentMode = UIViewContentModeScaleAspectFit;
        _fansIconView.image = [UIImage imageNamed:@"ic_zhibo_kaishizhibo_tongzhifensi"];
    }
    return _fansIconView;
}

- (UILabel *)noticeFansLabel {
    if (!_noticeFansLabel) {
        _noticeFansLabel = [[UILabel alloc] init];
        _noticeFansLabel.text = @"开播时通知粉丝".localized;
        _noticeFansLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _noticeFansLabel.font = [UIFont mediumFontOfSize:15];
    }
    return _noticeFansLabel;
}

- (UISwitch *)noticeFansSwitch {
    if (!_noticeFansSwitch) {
        _noticeFansSwitch = [[UISwitch alloc] init];
        _noticeFansSwitch.onTintColor = [UIColor colorWithHexString:@"#6A2CF7"];
        [_noticeFansSwitch addTarget:self action:@selector(noticeFansSwitchAction) forControlEvents:UIControlEventValueChanged];
    }
    return _noticeFansSwitch;
}

- (UISwitch *)linkMicSwitch {
    if (!_linkMicSwitch) {
        _linkMicSwitch = [[UISwitch alloc] init];
        _linkMicSwitch.onTintColor = [UIColor colorWithHexString:@"#6A2CF7"];
        [_linkMicSwitch addTarget:self action:@selector(linkMicSwitchAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _linkMicSwitch;
}

- (UIButton *)startupBtn {
    if (!_startupBtn) {
        _startupBtn = [[UIButton alloc] init];
        _startupBtn.backgroundColor = [UIColor colorWithHexString:@"#6A2CF7"];
        [_startupBtn setTitle:@"开始直播".localized forState:UIControlStateNormal];
        _startupBtn.titleLabel.font = [UIFont mediumFontOfSize:15];
        _startupBtn.layer.cornerRadius = 20;
        _startupBtn.layer.masksToBounds = true;
        [_startupBtn addTarget:self action:@selector(startupBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _startupBtn;
}

- (UIButton *)moreBtn {
    if (!_moreBtn) {
        _moreBtn = [[UIButton alloc] init];
        [_moreBtn setImage:[UIImage imageNamed:@"ic_gerenzhuye_gengduo_hei"] forState:UIControlStateNormal];
        [_moreBtn addTarget:self action:@selector(moreBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _moreBtn;
}

- (UIImageView *)linkIconView {
    if (!_linkIconView) {
        _linkIconView = [UIImageView new];
        _linkIconView.image = [UIImage imageNamed:@"ic_kaishizhibo_kaiqilianmai"];
    }
    return _linkIconView;
}

- (UILabel *)linkMicLabel {
    if (!_linkMicLabel) {
        _linkMicLabel = [UILabel new];
        _linkMicLabel.font = [UIFont mediumFontOfSize:15];
        _linkMicLabel.text = @"开启连麦".localized;
        _linkMicLabel.textColor = SPD_HEXCOlOR(@"#1A1A1A");
    }
    return _linkMicLabel;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavigation];
    
    [self setupUI];
    
    [self setupUIFrame];
    
    [self setupData];
    
    //[MobClick event:@"FM12_1"];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.titleTextView.placeholderColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.5];
    self.titleTextView.placeholder = @"输入一个今日直播的标题，将在首页中展示".localized;
    [self.titleTextView updatePlaceholderLabel];
}

- (void)setupNavigation {
    self.title = @"开始直播".localized;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.moreBtn];
}

- (void)setupUI {
    [self.view addSubview:self.coverImgBtn];
    [self.coverImgBtn addSubview:self.addCoverImgLabel];
    [self.view addSubview:self.titleTextView];
    [self.view addSubview:self.fansIconView];
    [self.view addSubview:self.noticeFansLabel];
    [self.view addSubview:self.noticeFansSwitch];
    [self.view addSubview:self.startupBtn];
    [self.view addSubview:self.linkIconView];
    [self.view addSubview:self.linkMicLabel];
    [self.view addSubview:self.linkMicSwitch];
}

- (void)setupUIFrame {
    [self.coverImgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(165, 165));
        if ([UIDevice currentDevice].is_iPhoneX) {
            make.top.mas_equalTo(88 + 25);
        } else {
            make.top.mas_equalTo(64 + 25);
        }
        make.centerX.mas_equalTo(self.view);
    }];
    
    [self.addCoverImgLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.bottom.trailing.mas_equalTo(self.coverImgBtn);
        make.height.mas_equalTo(35);
    }];

    
    [self.titleTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(self.coverImgBtn.mas_bottom).offset(25);
        make.height.mas_equalTo(42);
        make.trailing.mas_equalTo(-15);
    }];
    
    [self.fansIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(13, 15));
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(self.titleTextView.mas_bottom).offset(33);
    }];
    
    
    [self.noticeFansLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.fansIconView.mas_trailing).offset(7);
        make.centerY.mas_equalTo(self.fansIconView);
    }];
    
    [self.noticeFansSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-15);
        make.centerY.mas_equalTo(self.fansIconView);
    }];
    
    [self.linkIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(13, 15));
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(self.fansIconView.mas_bottom).offset(25);
    }];
    
    [self.linkMicLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.linkIconView.mas_centerY).offset(0);
        make.leading.equalTo(self.linkIconView.mas_trailing).offset(6);
    }];
    
    [self.linkMicSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-15);
        make.centerY.mas_equalTo(self.linkIconView);
    }];
    
    [self.startupBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(135, 44));
        make.centerX.mas_equalTo(self.view);
        make.bottom.mas_equalTo(-IphoneX_Bottom-30);
//        if (@available(iOS 11.0, *)) {
//            make.bottom.mas_equalTo(self.view).offset(-30);
//        } else {
//            make.bottom.mas_equalTo(self.view).offset(-30);
//        }
    }];
    
    
    __weak typeof(self) weakSelf = self;
    self.titleTextView.changeHeightBlock = ^(CGFloat height) {
        if (height < 42) {
            height = 42;
        }
        [weakSelf.titleTextView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(height);
        }];
        NSLog(@"%f", height);
    };
}

- (void)setupData {
    // 设置默认值
    [self.noticeFansSwitch setOn:true];
    self.liveIsNotiFans = @"true";
    // 请求上次直播资料
    [LY_Network getRequestWithURL:LY_Api.live_start parameters:nil success:^(id  _Nullable response) {
        NSString *title = response[@"title"];
        NSString *inform = response[@"inform"];
        NSString *conver = response[@"cover"];
        self.titleTextView.text = title;
        [self.titleTextView updatePlaceholderLabel];
        self.liveTitle = title;
        self.liveId = response[@"liveId"];
        
        if (conver.notEmpty) {
            [self.coverImgBtn sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:conver]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"ic_zhibo_kaishizhibo_morentu"]];
            self.addCoverImgLabel.text = @"点击更换封面".localized;
        }

        self.linkMicSwitch.on = [response[@"openChat"] boolValue];
        
        if ([inform isEqualToString:@"false"]) {
            [self.noticeFansSwitch setOn:false];
        } else {
            [self.noticeFansSwitch setOn:true];
        }
        
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

- (UIImagePickerController *)imagePickerController {
    if (!_imagePickerController) {
        _imagePickerController = [[UIImagePickerController alloc] init];
        _imagePickerController.delegate = self;
        _imagePickerController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        // 设置是否可以管理已经存在的图片或者视频
        _imagePickerController.allowsEditing = true;
    }
    return _imagePickerController;
}

/// 开启相机相册
- (void)getImageFromPhotoLib:(UIImagePickerControllerSourceType)type {
    self.imagePickerController.sourceType = type;
    // 判断是否支持相机
    if ([UIImagePickerController isSourceTypeAvailable:type]) {
        [self presentViewController:self.imagePickerController animated:true completion:^{
            self.imagePickerController.delegate = self;
        }];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info {
    
    [picker dismissViewControllerAnimated:true completion:nil];
    
    UIImage *img = info[@"UIImagePickerControllerEditedImage"];
    self.liveCover = img;
    self.addCoverImgLabel.text = @"点击更换封面".localized;
    [self.coverImgBtn setImage:img forState:UIControlStateNormal];
}


- (void)coverImgBtnAction {
    LYPSActionSheetView *actionSheetView = [[LYPSActionSheetView alloc] init];
    [actionSheetView addAction:[LYPSystemAlertAction actionWithTitle:@"从相册中选择".localized style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
        [self getImageFromPhotoLib:UIImagePickerControllerSourceTypePhotoLibrary];
    }]];
    [actionSheetView addAction:[LYPSystemAlertAction actionWithTitle:@"拍照".localized style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
        [self getImageFromPhotoLib:UIImagePickerControllerSourceTypeCamera];
    }]];
    [actionSheetView present];
}

- (void)moreBtnAction {
    //[MobClick event:@"FM12_1_2"];
    [self.moreBtn setEnabled:false];
    LY_LiveFiringMoreActionSheetView *moreActionSheetView = [[LY_LiveFiringMoreActionSheetView alloc] init];
    moreActionSheetView.delegate = self;
    [moreActionSheetView presentOnView:self.view];
    __weak typeof(self) weakSelf = self;
    [moreActionSheetView dismiss:^{
        [weakSelf.moreBtn setEnabled:true];
    }];
}

- (void)noticeFansSwitchAction {
    if (self.noticeFansSwitch.isOn) {
        [LY_HUD showToastTips:@"已开启“开播时通知粉丝”".localized];
        self.liveIsNotiFans = @"true";
    } else {
        [LY_HUD showToastTips:@"已关闭“开播时通知粉丝”".localized];
        self.liveIsNotiFans = @"false";
    }
    
    //[MobClick event:@"FM12_1_1"];
}

// 开启关闭连麦
- (void)linkMicSwitchAction:(UISwitch *)sender {
    if (!self.liveId.notEmpty) {
        return;
    }
    // 0 关闭 1 开启
    [RequestUtils POST:URL_NewServer(@"live/live.open.chat") parameters:@{@"liveId":self.liveId,@"openChat":@(sender.on)} success:^(id  _Nullable suceess) {
        
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
    
}

- (void)startupBtnAction {
    // 判断封面
//    if (self.liveCover == nil) {
//        [LY_HUD showToastTips:@"请选择封面"];
//        return;
//    }
    //[MobClick event:@"FM12_1_3"];
    if (self.liveTitle == nil || self.liveTitle.length == 0) {
        [LY_HUD showToastTips:@"请输入直播标题".localized];
        return;
    }
    
    // 判断标题
    
    NSDictionary *params = @{
        @"title": self.liveTitle,
        @"inform": self.liveIsNotiFans,
    };
    
    NSArray *images;
    if (self.liveCover) {
        images = @[self.liveCover];
    }
    [LY_HUD show];
    [LY_Network postImageRequestWithURL:LY_Api.live_updateInfo parameters:params imageParametersName:@"cover" images:images success:^(id  _Nullable response) {
        
        [LY_Network getRequestWithURL:LY_Api.live_start parameters:nil success:^(id  _Nullable response) {
            NSString *title = response[@"title"];
            NSString *inform = response[@"inform"];
            NSString *conver = response[@"cover"];
            NSString *liveId = response[@"liveId"];

            // 成功
            LiveModel *model = [[LiveModel alloc] init];
            
            model.userId = [SPDApiUser currentUser].userId;
            model.userName = [SPDApiUser currentUser].nickName;
            model.avatar = [SPDApiUser currentUser].avatar;
            model.liveId = liveId;
            model.liveTitle = title;
            model.liveCover = conver;
            
            [self.navigationController popViewControllerAnimated:false];
            
            [ZegoManager joinLiveRoomWithliveInfo:model isAnchor:YES];
            [LY_HUD dismiss];
        } failture:^(NSError * _Nullable error) {
            [LY_HUD showToastTips:error.localizedDescription];
        }];
        
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

- (void)clickWalletBtn {
    LiveAnchorWalletController *walletVC = [[LiveAnchorWalletController alloc] init];
    walletVC.selectIndex = 0;
    [self.navigationController pushViewController:walletVC animated:true];
}

- (void)clickRecordBtn {
    LiveAnchorWalletController *walletVC = [[LiveAnchorWalletController alloc] init];
    walletVC.selectIndex = 1;
    [self.navigationController pushViewController:walletVC animated:true];
}

- (void)textViewDidChange:(UITextView *)textView {
    //该判断用于联想输入
    if (textView.text.length > 50) {
        textView.text = [textView.text substringToIndex:50];
    }
    
    self.liveTitle = textView.text;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@""] && range.length > 0) {
        //删除字符肯定是安全的
        return YES;
    }else if ([text isEqualToString:@"\n"]){
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
//    if ([text isEqualToString:@"\n"]){ //判断输入的字是否是回车，即按下return
//        [textView resignFirstResponder];
//        return false;
//    }
//    return true;
}

@end
