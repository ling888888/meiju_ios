//
//  LY_FansListViewController.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_BaseViewController.h"
#import "LY_Fans.h"

NS_ASSUME_NONNULL_BEGIN

@class LY_FansListViewController;

@protocol LY_FansListViewControllerDelegate <NSObject>

- (void)fansListViewControllerDidJoinFansGroup:(LY_FansListViewController *)fansListViewController;

@end

@interface LY_FansListViewController : LY_BaseViewController

@property(nonatomic, weak) id<LY_FansListViewControllerDelegate>delegate;

/// 是否是主播样式
@property(nonatomic, assign) BOOL isAnchorStyle;

/// 主播的用户id
@property(nonatomic, copy) NSString *anchorUserId;

/// 主播的头像
@property(nonatomic, copy) NSString *portraitUrl;

/// 主播的昵称
@property(nonatomic, copy) NSString *nickname;

@end

@interface BottomSelfView : UIView

@property(nonatomic, strong) LY_Fans *fans;

@property(nonatomic, assign) NSInteger rank;

@end

NS_ASSUME_NONNULL_END
