//
//  LY_FanGroupViewController.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/20.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_FanGroupViewController.h"
#import "LY_Button.h"
#import "LY_FansNotJoinedView.h"
#import "LY_FansJoinedView.h"
#import "LY_FansListViewController.h"
#import "LY_FansSuccessJoinAlertView.h"
#import "LY_FansGroupRuleViewController.h"

@interface LY_FanGroupViewController () <LY_FansNotJoinedViewDelegate, LY_FansJoinedViewDelegate, LY_FansListViewControllerDelegate>

//@property(nonatomic, strong) UIImageView *bgImgView;

//@property(nonatomic, strong) UIView *whiteBgView;

@property(nonatomic, strong) UIImageView *portraitView;

@property(nonatomic, strong) UILabel *nameLabel;

@property(nonatomic, strong) LY_Button *fansNumberBtn;

@property(nonatomic, strong) UIButton *whyBtn;

@property(nonatomic, strong) LY_FansNotJoinedView *notJoinedView;

@property(nonatomic, strong) LY_FansJoinedView *joinedView;

@property(nonatomic, copy) NSString *anchorNickname;

@property(nonatomic, copy) NSString *fansGroupName;

@end

@implementation LY_FanGroupViewController

//- (UIImageView *)bgImgView {
//    if (!_bgImgView) {
//        _bgImgView = [[UIImageView alloc] init];
//        _bgImgView.backgroundColor = [UIColor colorWithHexString:@"#FF6B93"];
//
//    }
//    return _bgImgView;
//}

//- (UIView *)whiteBgView {
//    if (!_whiteBgView) {
//        _whiteBgView = [[UIView alloc] init];
//        _whiteBgView.backgroundColor = [UIColor whiteColor];
//    }
//    return _whiteBgView;
//}

- (UIImageView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[UIImageView alloc] init];
        _portraitView.contentMode = UIViewContentModeScaleAspectFill;
//        _portraitView.layer.borderWidth = 4;
//        _portraitView.layer.borderColor = [UIColor colorWithHexString:@"#FF6B93"].CGColor;
        _portraitView.layer.cornerRadius = 22.5;
        _portraitView.layer.masksToBounds = true;
        _portraitView.backgroundColor = [UIColor whiteColor];
    }
    return _portraitView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _nameLabel.font = [UIFont boldSystemFontOfSize:17];
        _nameLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    }
    return _nameLabel;
}

- (LY_Button *)fansNumberBtn {
    if (!_fansNumberBtn) {
        _fansNumberBtn = [[LY_Button alloc] initWithImageTitleSpace:7];
        _fansNumberBtn.titleColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _fansNumberBtn.titleFont = [UIFont mediumFontOfSize:12];
        _fansNumberBtn.title = [NSString stringWithFormat:@"粉丝:%@".localized, @"0"];
        _fansNumberBtn.image = [UIImage imageNamed:@"ic_zhibo_fensituan_gengduo"].adaptiveRtl;
        _fansNumberBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 5);
        [_fansNumberBtn addTarget:self action:@selector(fansNumberBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _fansNumberBtn;
}

- (UIButton *)whyBtn {
    if (!_whyBtn) {
        _whyBtn = [[UIButton alloc] init];
        [_whyBtn setImage:[UIImage imageNamed:@"ic_zhubo_guize_hei"] forState:UIControlStateNormal];
        [_whyBtn addTarget:self action:@selector(whyBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _whyBtn;
}

- (LY_FansNotJoinedView *)notJoinedView {
    if (!_notJoinedView) {
        _notJoinedView = [[LY_FansNotJoinedView alloc] init];
        _notJoinedView.delegate = self;
    }
    return _notJoinedView;
}

- (LY_FansJoinedView *)joinedView {
    if (!_joinedView) {
        _joinedView = [[LY_FansJoinedView alloc] initWithLiveId:self.liveId];
        _joinedView.delegate = self;
    }
    return _joinedView;
}

- (instancetype)initWithLiveId:(NSString *)liveId anchorUserId:(NSString *)anchorUserId {
    self = [super init];
    if (self) {
        self.liveId = liveId;
        self.anchorUserId = anchorUserId;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.whyBtn];
    
    [self setupUI];
    
    [self setupUIFrame];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
//    self.nameLabel.text = @"ANGLE的粉丝团";
//    self.status = LY_FansGroupStatusNotJoined;
}

- (void)dealloc
{
    NSLog(@"LY_FansGroupStatusNotJoined");
}
//- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//
//    [self.navigationController setNavigationBarHidden:true];
//}
//
//- (void)viewWillDisappear:(BOOL)animated {
//    [super viewWillDisappear:animated];
//
//    [self.navigationController setNavigationBarHidden:false];
//}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
//    [self.bgImgView addRounded:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:10];
//    [self.whiteBgView addRounded:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:10];
    
//    UIImage *bgImg = [[UIImage alloc] initWithGradient:@[[UIColor colorWithHexString:@"#FF6B93"], [UIColor whiteColor]] size:self.bgImgView.bounds.size direction:UIImageGradientColorsDirectionVertical];
//    self.bgImgView.image = bgImg;
}

- (void)setupUI {
//    [self.view addSubview:self.bgImgView];
//    [self.view addSubview:self.whiteBgView];
    [self.view addSubview:self.whyBtn];
    [self.view addSubview:self.portraitView];
    [self.view addSubview:self.nameLabel];
    [self.view addSubview:self.fansNumberBtn];
}

- (void)setupUIFrame {
//    [self.bgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(UIEdgeInsetsMake(42, 0, 0, 0));
//    }];
//    [self.whiteBgView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
//    }];
//    [self.whyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.size.mas_equalTo(35);
//        make.top.mas_equalTo(self.whiteBgView).offset(5);
//        make.trailing.mas_equalTo(self.whiteBgView).offset(-5);
//    }];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(45);
//        make.centerY.mas_equalTo(self.bgImgView.mas_top);
//        make.centerX.mas_equalTo(self.view);
        make.top.mas_equalTo(15);
        make.leading.mas_equalTo(15);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.portraitView);
        make.leading.mas_equalTo(self.portraitView.mas_trailing).offset(10);
        make.trailing.mas_equalTo(-70);
    }];
    [self.fansNumberBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.portraitView);
        make.leading.mas_equalTo(self.portraitView.mas_trailing).offset(10);
    }];
}

- (void)setStatus:(LY_FansGroupStatus)status {
//    if (_status == status) {
//        return;
//    }
    _status = status;
    switch (status) {
        case LY_FansGroupStatusNotJoined: {
            // 未加入
            if (_joinedView) {
                [_joinedView removeFromSuperview];
            }
            if ([self.anchorUserId isEqualToString:[SPDApiUser currentUser].userId]) {
                [self.notJoinedView.joinBtn setHidden:true];
            }
            [self.view addSubview:self.notJoinedView];
            [self.notJoinedView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.fansNumberBtn.mas_bottom);
                make.leading.mas_equalTo(4);
                make.trailing.mas_equalTo(-4);
                make.bottom.mas_equalTo(self.view);
            }];
            
            break;
        }
        case LY_FansGroupStatusJoined: {
            // 已加入
            if (_notJoinedView) {
                [_notJoinedView removeFromSuperview];
            }
            self.joinedView.isValid = true;
            self.liveId = self.liveId;
            [self.view addSubview:self.joinedView];
            [self.joinedView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.fansNumberBtn.mas_bottom);
                make.leading.mas_equalTo(4);
                make.trailing.mas_equalTo(-4);
                make.bottom.mas_equalTo(self.view);
            }];
            break;
        }
        case LY_FansGroupStatusInvalid: {
            // 已失效
            if (_notJoinedView) {
                [_notJoinedView removeFromSuperview];
            }
            self.joinedView.isValid = false;
            self.liveId = self.liveId;
            [self.view addSubview:self.joinedView];
            [self.joinedView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.fansNumberBtn.mas_bottom);
                make.leading.mas_equalTo(4);
                make.trailing.mas_equalTo(-4);
                make.bottom.mas_equalTo(self.view);
            }];
            break;
        }
        default:
            break;
    }
    // 重新请求网络粉丝团数据
    [self requestFansGroupInfo];
}

- (void)setLiveId:(NSString *)liveId {
    _liveId = liveId;
    
    if (_joinedView) {
        _joinedView.liveId = liveId;
    }
}

- (void)requestFansGroupInfo {
    NSDictionary *params = @{@"anchor_id": self.anchorUserId};;
    [LY_Network getRequestWithURL:LY_Api.live_fansGroup_info parameters:params success:^(id  _Nullable response) {
        // 设置封面头像
        [self.portraitView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:response[@"avatar"]]] placeholderImage:[UIImage imageNamed:@"img_portrait_placeholder"]];
        
        // 设置粉丝团名字
        self.nameLabel.text = [NSString stringWithFormat:@"%@的粉丝团".localized, response[@"nick_name"]];
        self.fansGroupName = response[@"fan_male"];
        self.anchorNickname = response[@"nick_name"];
        
        self.fansNumberBtn.title = [NSString stringWithFormat:@"粉丝:%@".localized, response[@"count"]];
        
        //isFan: "1" //加入状态 0:未加入 1加入 2冻结
        if (![response[@"isFan"] isEqualToString:[NSString stringWithFormat:@"%ld", (long)self.status]]) {
            self.status = [response[@"isFan"] intValue];
        }
        
        // 判断状态
        if (self.status == LY_FansGroupStatusNotJoined) {
            // 未加入
            self.notJoinedView.needConchNumber = [response[@"count"] integerValue];
        } else {
            NSInteger level = [response[@"level"] integerValue];
            NSInteger intimacy = [response[@"intimacy"] integerValue];
            NSInteger nextLevel = 0;
            if (response[@"next_level"]) {
                nextLevel = [response[@"next_level"] integerValue];
            } else {
                nextLevel= level;
            }
            NSInteger nexIntimacy = 0;
            if (response[@"nex_intimacy"]) {
                nexIntimacy = [response[@"nex_intimacy"] integerValue];
            } else {
                nexIntimacy = intimacy;
            }
             
            
            CGFloat progress = 1.0;
            if (response[@"percentage"]) {
                progress = [response[@"percentage"] floatValue];
            } else {
                progress = 1.0;
            }
            
            [self.joinedView setLevel:level andName:response[@"fan_male"]];
            [self.joinedView setLevel:level nextLevel:nextLevel intimacy:intimacy nextIntimacy:nexIntimacy progress:progress];
            if (self.status == LY_FansGroupStatusJoined) {
                // 已加入
                
            } else {
                // 已失效
                self.joinedView.text = response[@"desc"];
            }
        }
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

- (void)fansNumberBtnAction {
    LY_FansListViewController *fansListVC = [[LY_FansListViewController alloc] init];
    fansListVC.anchorUserId = self.anchorUserId;
    fansListVC.nickname = self.anchorNickname;
    fansListVC.delegate = self;
    [self.navigationController pushViewController:fansListVC animated:true];
//    self.status = LY_FansGroupStatusJoined;
}

- (void)whyBtnAction {
    NSString *urlString = [NSString stringWithFormat:@"https://share.famy.ly/ar/fansRule.html?lang=%@", [SPDCommonTool getFamyLanguage]];
    LY_FansGroupRuleViewController *webVC = [[LY_FansGroupRuleViewController alloc] initWithURLString:urlString];
    webVC.title = @"粉丝团规则".localized;
    [self.navigationController pushViewController:webVC animated:true];
//    self.status = LY_FansGroupStatusInvalid;
}

- (void)fansNotJoinedViewClickJoinBtn:(UIButton *)btn {
    NSDictionary *params = @{@"anchor_id":self.anchorUserId};
    [LY_Network postRequestWithURL:LY_Api.live_fansGroup_join parameters:params success:^(id  _Nullable response) {
        LY_FansSuccessJoinAlertView * successJoinAV = [[LY_FansSuccessJoinAlertView alloc] init];
        [successJoinAV present];
        __weak typeof(self) weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [successJoinAV dismiss];
            weakSelf.status = LY_FansGroupStatusJoined;
        });
        if ([self.delegate respondsToSelector:@selector(fanGroupViewControllerDidClickJoinFansGroupWith:withFansGroupName:)]) {
            [self.delegate fanGroupViewControllerDidClickJoinFansGroupWith:self withFansGroupName:self.fansGroupName];
        }
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

- (void)fansJoinedView:(nonnull LY_FansJoinedView *)fansJoinedView clickHearLiveBtn:(nonnull UIButton *)btn {
    if ([self.delegate respondsToSelector:@selector(fanGroupViewControllerDidClickHearLiveWith:)]) {
        [self.delegate fanGroupViewControllerDidClickHearLiveWith:self];
        [self.navigationController dismissViewControllerAnimated:true completion:nil];
    }
}

- (void)fansJoinedView:(nonnull LY_FansJoinedView *)fansJoinedView clickSendGiftBtn:(nonnull UIButton *)btn {
    if ([self.delegate respondsToSelector:@selector(fanGroupViewControllerDidClickSendGiftWith:)]) {
        [self.delegate fanGroupViewControllerDidClickSendGiftWith:self];
        [self.navigationController dismissViewControllerAnimated:true completion:nil];
    }
}

- (void)fansJoinedView:(nonnull LY_FansJoinedView *)fansJoinedView clickSendMessageeBtn:(nonnull UIButton *)btn {
    if ([self.delegate respondsToSelector:@selector(fanGroupViewControllerDidClickSendMessageWith:)]) {
        [self.delegate fanGroupViewControllerDidClickSendMessageWith:self];
        [self.navigationController dismissViewControllerAnimated:true completion:nil];
    }
}

- (void)fansJoinedView:(nonnull LY_FansJoinedView *)fansJoinedView clickShareLiveBtn:(nonnull UIButton *)btn {
    if ([self.delegate respondsToSelector:@selector(fanGroupViewControllerDidClickShareLiveWith:)]) {
        [self.delegate fanGroupViewControllerDidClickShareLiveWith:self];
        [self.navigationController dismissViewControllerAnimated:true completion:nil];
    }
}

// MARK: - LY_FansListViewControllerDelegate

- (void)fansListViewControllerDidJoinFansGroup:(LY_FansListViewController *)fansListViewController {
    self.status = LY_FansGroupStatusJoined;
    if ([self.delegate respondsToSelector:@selector(fanGroupViewControllerDidClickJoinFansGroupWith:withFansGroupName:)]) {
        [self.delegate fanGroupViewControllerDidClickJoinFansGroupWith:self withFansGroupName:self.fansGroupName];
    }
}

@end
