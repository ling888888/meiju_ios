//
//  LY_FunsGroupEditViewController.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/26.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_FunsGroupEditViewController.h"
#import "NSString+XXWAddition.h"

@interface LY_FunsGroupEditViewController () <UITextFieldDelegate>

@property(nonatomic, strong) UIImageView *topBgImgView;

@property(nonatomic, strong) UIImageView *topLeadingImgView;

@property(nonatomic, strong) UIImageView *topTrailingImgView;

@property(nonatomic, strong) UILabel *nameLabel;

@property(nonatomic, strong) UILabel *editTitleLabel;

@property(nonatomic, strong) UITextField *textField;

@property(nonatomic, strong) UILabel *descLabel;

@property(nonatomic, strong) UILabel *bottomTextLabel;

@property(nonatomic, strong) UIButton *finishBtn;

@end

@implementation LY_FunsGroupEditViewController

- (UIImageView *)topBgImgView {
    if (!_topBgImgView) {
        _topBgImgView = [[UIImageView alloc] init];
        _topBgImgView.image = [UIImage imageNamed:@"ic_zhibo_zhubo_bianjifensituanmingcheng"];
        _topBgImgView.layer.cornerRadius = 5;
        _topBgImgView.layer.masksToBounds = true;
    }
    return _topBgImgView;
}

- (UIImageView *)topLeadingImgView {
    if (!_topLeadingImgView) {
        _topLeadingImgView = [[UIImageView alloc] init];
        _topLeadingImgView.image = [UIImage imageNamed:@"ic_zhibo_yonghu"];
    }
    return _topLeadingImgView;
}

- (UIImageView *)topTrailingImgView {
    if (!_topTrailingImgView) {
        _topTrailingImgView = [[UIImageView alloc] init];
        _topTrailingImgView.image = [UIImage imageNamed:@"ic_zhibo_yonghu_fenxituanmingcheng"];
    }
    return _topTrailingImgView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = [UIColor whiteColor];
        _nameLabel.font = [UIFont mediumFontOfSize:12];
    }
    return _nameLabel;
}

- (UILabel *)editTitleLabel {
    if (!_editTitleLabel) {
        _editTitleLabel = [[UILabel alloc] init];
        _editTitleLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _editTitleLabel.font = [UIFont mediumFontOfSize:15];
        _editTitleLabel.text = @"编辑粉丝勋章名称".localized;
    }
    return _editTitleLabel;
}

- (UITextField *)textField {
    if (!_textField) {
        _textField = [[UITextField alloc] init];
        _textField.placeholder = @"请输入名称".localized;
        _textField.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.05];
        _textField.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _textField.font = [UIFont mediumFontOfSize:15];
        _textField.layer.cornerRadius = 5;
        _textField.layer.masksToBounds = true;
        _textField.delegate = self;
        _textField.returnKeyType = UIReturnKeyDone;
    }
    return _textField;
}

- (UILabel *)descLabel {
    if (!_descLabel) {
        _descLabel = [[UILabel alloc] init];
        _descLabel.textColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.6];
        _descLabel.font = [UIFont systemFontOfSize:12];
        _descLabel.numberOfLines = 0;
    }
    return _descLabel;
}

- (UILabel *)bottomTextLabel {
    if (!_bottomTextLabel) {
        _bottomTextLabel = [[UILabel alloc] init];
        _bottomTextLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _bottomTextLabel.font = [UIFont mediumFontOfSize:14];
        _bottomTextLabel.numberOfLines = 0;
    }
    return _bottomTextLabel;
}

- (UIButton *)finishBtn {
    if (!_finishBtn) {
        _finishBtn = [[UIButton alloc] init];
        [_finishBtn setTitle:@"提交修改".localized forState:UIControlStateNormal];
        [_finishBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _finishBtn.titleLabel.font = [UIFont mediumFontOfSize:15];
        _finishBtn.layer.cornerRadius = 20;
        _finishBtn.layer.masksToBounds = true;
        UIImage *norImg = [UIImage imageWithUIColor:[UIColor colorWithHexString:@"#CCCCCC"]];
        UIImage *selImg = [UIImage imageWithUIColor:[UIColor colorWithHexString:@"#6A2CF7"]];
        [_finishBtn setBackgroundImage:norImg forState:UIControlStateNormal];
        [_finishBtn setBackgroundImage:selImg forState:UIControlStateSelected];
        [_finishBtn addTarget:self action:@selector(finishBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _finishBtn;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"编辑粉丝勋章名称".localized;
    
    [self setupUI];
    
    [self setupUIFrame];
    
    [self setupData];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar vhl_setBackgroundColor:[UIColor whiteColor]];
    
    //TODO: 页面Disappear 启用
    // 使用智能键盘
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    // 控制整个功能是否启用
    manager.enable = false;
    // 控制是否显示键盘上的自动工具条,当需要支持内联编辑(Inline Editing), 这就需要隐藏键盘上的工具条(默认打开)
    manager.enableAutoToolbar = false;
    // 启用手势触摸:控制点击背景是否收起键盘。
    manager.shouldResignOnTouchOutside = true;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar vhl_setBackgroundColor:[UIColor clearColor]];
}

- (void)setupUI {
    [self.view addSubview:self.topBgImgView];
    [self.topBgImgView addSubview:self.topLeadingImgView];
    [self.topBgImgView addSubview:self.topTrailingImgView];
    [self.topBgImgView addSubview:self.nameLabel];
    [self.view addSubview:self.textField];
    [self.view addSubview:self.descLabel];
    [self.view addSubview:self.bottomTextLabel];
    [self.view addSubview:self.finishBtn];
}

- (void)setupUIFrame {
    [self.topBgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(63);
        make.leading.mas_equalTo(15);
        make.trailing.mas_equalTo(-15);
        make.height.mas_equalTo(60);
    }];
    [self.topLeadingImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(173, 43.5));
        make.centerY.mas_equalTo(self.topBgImgView);
        make.leading.mas_equalTo(5);
    }];
    [self.topTrailingImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(113, 24.5));
        make.centerY.mas_equalTo(self.topBgImgView);
        make.leading.mas_equalTo(self.topLeadingImgView.mas_trailing).offset(5.5);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.trailing.bottom.mas_equalTo(self.topTrailingImgView);
        make.leading.mas_equalTo(self.topTrailingImgView).offset(32.5);
    }];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topBgImgView.mas_bottom).offset(10);
        make.leading.mas_equalTo(15);
        make.trailing.mas_equalTo(-15);
        make.height.mas_equalTo(45);
    }];
    [self.descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.textField.mas_bottom).offset(15);
        make.leading.mas_equalTo(15);
        make.trailing.mas_equalTo(-15);
    }];
    [self.bottomTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.descLabel.mas_bottom).offset(18);
        make.leading.mas_equalTo(15);
        make.trailing.mas_equalTo(-15);
    }];
    [self.finishBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(165, 40));
        make.bottom.mas_equalTo(-35);
        make.centerX.mas_equalTo(0);
    }];
    
    self.descLabel.text = @"1.入团用户将佩戴并显示此勋章名称\n2.名称不可涉黄、涉暴等内容或词汇，提交审核后生效".localized;
    self.bottomTextLabel.text = @"粉丝团可以帮助主播收获更多忠实观众，容易获得更多礼物".localized;
    [self.finishBtn setSelected:true];
}

- (void)setupData {
    [LY_Network getRequestWithURL:LY_Api.live_fansGroup_creatStatus parameters:nil success:^(id  _Nullable response) {
        [LY_HUD dismiss];
        // 审核状态，0待审核，1审核通过，2审核拒绝
        if (!response) {
            return;
        }
//        if ([self.delegate respondsToSelector:@selector(funsGroupCreatViewController:checkStatus:)]) {
//            [self.delegate funsGroupCreatViewController:self checkStatus:[response[@"checkStatus"] stringValue]];
//        }
        if ([[response[@"checkStatus"] stringValue] isEqualToString:@"0"]) {
            // 审核中
            self.textField.textColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.6];
            [self.textField setUserInteractionEnabled:false];
            [self.finishBtn setTitle:@"审核中".localized forState:UIControlStateNormal];
            [self.finishBtn setTitleColor:[UIColor colorWithHexString:@"#6A2CF7"] forState:UIControlStateNormal];
            [self.finishBtn setSelected:false];
            [self.finishBtn setUserInteractionEnabled:false];
            // 粉丝牌名称
            self.textField.text = response[@"fansName"];
            self.nameLabel.text = response[@"fansName"];
        } else {
            
        }
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

- (void)KeyboardWillShow:(NSNotification *)notification {
    CGRect keyboardBounds = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    UIViewAnimationCurve curve = [[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
//    CGRect frame = self.tableView.frame;
    [self.topBgImgView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(63);
    }];
//    frame.size.height = self.view.frame.size.height - keyboardBounds.size.height;
    CGFloat animationDuration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^{
//        [UIView setAnimationCurve:curve];
        [self.topBgImgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(-16);
        }];
        [self.view setNeedsLayout];
//        [self.tableView setFrame:frame];
//        [UIView commitAnimations];
    }];
//    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

- (void)KeyboardWillHide:(NSNotification *)notification {
    UIViewAnimationCurve curve = [[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    CGFloat animationDuration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^{
//        [UIView setAnimationCurve:curve];
//        [self.tableView setFrame:self.view.bounds];
        [self.topBgImgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(63);
        }];
        [self.view setNeedsLayout];
//        [UIView commitAnimations];
    }];
}

- (void)finishBtnAction {
    [LY_HUD show];
    NSDictionary *params = @{@"fansName": self.textField.text};
    [LY_Network postRequestWithURL:LY_Api.live_fansGroup_update parameters:params success:^(id  _Nullable response) {
//        [LY_HUD showToastTips:@"创建成功longge"];
//        [self.navigationController dismissViewControllerAnimated:true completion:nil];;
        [self setupData];
        
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // 判断是否为表情键盘
    if ([[[textField textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textField textInputMode] primaryLanguage]) {
        return false;
    }
    // 判断是否点击完成
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return false;
    }
    NSString *text = [textField changeCharactersInRange:range replacementString:string];
    
    if (text.length > 12) {
        return false;
    }
    return true;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    // 过滤表情
    NSString *text = textField.text.filterEmojiString;
    textField.text = text;
    self.textField.text = text;
    if (text != nil && ![text isEqualToString:@""]) {
        [self.finishBtn setSelected:true];
        [self.finishBtn setEnabled:true];
        self.nameLabel.text = textField.text;
    } else {
        [self.finishBtn setSelected:false];
        [self.finishBtn setEnabled:false];
        self.nameLabel.text = @"MIKEEI";
    }
}

@end
