//
//  LiveRecommendAnchorController.h
//  SimpleDate
//
//  Created by ling Hou on 2020/4/23.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LiveRecommendAnchorController,LiveModel;
@protocol LiveRecommendAnchorControllerDelegate <NSObject>

@optional
- (void)liveRecommendAnchorController:(LiveRecommendAnchorController *)controller didClickedFollowButton:(UIButton *)sender;

- (void)jump:(LiveModel *)model;
@end

@interface LiveRecommendAnchorController : UIViewController

@property (nonatomic, strong) LiveModel * liveInfo;
@property (nonatomic, strong) NSMutableArray * dataArray;
@property (nonatomic, weak) id<LiveRecommendAnchorControllerDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
