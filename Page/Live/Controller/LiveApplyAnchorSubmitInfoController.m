//
//  LiveApplyAnchorSubmitInfoController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/5/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveApplyAnchorSubmitInfoController.h"
#import "lame.h"
#import "LiveApplyAnchorConditionsViewController.h"

@interface LiveApplyAnchorSubmitInfoController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topCons;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomCons;
@property (weak, nonatomic) IBOutlet UITextField *textFeild;
@property (weak, nonatomic) IBOutlet UIButton *completeButton;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (nonatomic, assign) BOOL isRequest;
@end

@implementation LiveApplyAnchorSubmitInfoController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"提交申请材料".localized;
    self.topCons.constant = 36;
    self.bottomCons.constant = IphoneX_Bottom + 35;
    self.textFeild.placeholder = @"点击输入".localized;
    self.completeButton.userInteractionEnabled = YES;
    
    NSString * str = @"请告诉我你的WhatsApp账号，方便以后联系你".localized;
    NSMutableAttributedString *messageStr = [[NSMutableAttributedString alloc] initWithString:str];
    NSRange range = [str rangeOfString:@"WhatsApp"];
    [messageStr addAttribute:NSForegroundColorAttributeName value:SPD_HEXCOlOR(@"1a1a1a") range:NSMakeRange(0, str.length)];
    [messageStr addAttribute:NSForegroundColorAttributeName value:SPD_HEXCOlOR(@"6A2CF7") range:NSMakeRange(range.location, 8)];
    [messageStr addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:15] range:NSMakeRange(0, str.length)];
    self.contentLabel.attributedText = messageStr;
    [self.textFeild addTarget:self action:@selector(textFieldDidChange:) forControlEvents:(UIControlEventEditingChanged)];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[IQKeyboardManager sharedManager] setEnable:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[IQKeyboardManager sharedManager] setEnable:NO];
}

- (void)backBtnAction {
    if (self.isRequest) {
        return;
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)textFieldDidChange:(UITextField *)textFeild {
    self.completeButton.userInteractionEnabled = textFeild.text.length;
    self.completeButton.backgroundColor = (textFeild.text.length > 0) ? SPD_HEXCOlOR(@"#6A2CF7"):SPD_HEXCOlOR(@"#E8E8E8");
    if (textFeild.text.length > 20) {
        textFeild.text = [textFeild.text substringToIndex:20];
    }
}

- (IBAction)completeButtonClicked:(UIButton *)sender {
    sender.userInteractionEnabled = NO;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
   NSString *string = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"anchor.caf"];
    NSString * outPutString = [self audioToMP3:string isDeleteSourchFile:YES];
    NSData * data =[[NSFileManager defaultManager] contentsAtPath:outPutString];
    self.isRequest = YES;
    [RequestUtils commonPostMultipartFormDataUtils:[@{@"whatsappId":self.textFeild.text?:@"1"} mutableCopy] bURL:@"live.apply.submit" bAnimatedViewController:self constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:data name:@"audio" fileName:[NSString stringWithFormat:@"%@.mp3", [SPDApiUser currentUser].userId] mimeType:@"audio/mpeg"];

    } success:^(id  _Nullable suceess) {
        sender.userInteractionEnabled = YES;
        self.isRequest = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.navigationController pushViewController:[LiveApplyAnchorConditionsViewController new] animated:YES];
    } failure:^(id  _Nullable failure) {
        self.isRequest = NO;
        sender.userInteractionEnabled = YES;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showTips:failure[@"msg"]];
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(NSString *)audioToMP3: (NSString *)sourcePath isDeleteSourchFile:(BOOL)isDelete {
    NSString *inPath = sourcePath;
    NSFileManager *fm = [NSFileManager defaultManager];
    if (![fm fileExistsAtPath:sourcePath]) {
        NSLog(@"文件不存在");
        return inPath;
    }
    // 输出路径
    NSString *outPath = [[sourcePath stringByDeletingPathExtension] stringByAppendingString:@".mp3"];
    @try {
        int read, write;
        
        FILE *pcm = fopen([inPath cStringUsingEncoding:1], "rb");
        fseek(pcm, 4*1024, SEEK_CUR);
        FILE *mp3 = fopen([outPath cStringUsingEncoding:1], "wb");
        
        const int PCM_SIZE = 8192;
        const int MP3_SIZE = 8192;
        short int pcm_buffer[PCM_SIZE*2];
        unsigned char mp3_buffer[MP3_SIZE];

        lame_t lame = lame_init();
        lame_set_VBR(lame, vbr_default);
        lame_set_quality(lame,5); /* 2=high 5 = medium 7=low 音质*/
        lame_init_params(lame);
        
        do {
            read = (int)fread(pcm_buffer, 2*sizeof(short int), PCM_SIZE, pcm);
            if (read == 0)
                write = lame_encode_flush(lame, mp3_buffer, MP3_SIZE);
            else
                write = lame_encode_buffer_interleaved(lame, pcm_buffer, read, mp3_buffer, MP3_SIZE);
            
            fwrite(mp3_buffer, write, 1, mp3);
            
        } while (read != 0);
        
        lame_close(lame);
        fclose(mp3);
        fclose(pcm);
    }
    @catch (NSException *exception) {
        NSLog(@"%@",[exception description]);
    }
    @finally {
        NSLog(@"MP3转换成功: %@",outPath);
        return outPath;
    }

}



@end
