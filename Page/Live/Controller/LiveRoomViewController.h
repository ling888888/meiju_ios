//
//  LiveRoomViewController.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/20.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@class LiveModel;

@interface LiveRoomViewController : BaseViewController

@property (nonatomic, strong) LiveModel *liveInfo;

@property (nonatomic, assign) BOOL isAnchor;

@end

NS_ASSUME_NONNULL_END
