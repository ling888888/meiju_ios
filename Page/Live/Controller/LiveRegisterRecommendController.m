//
//  LiveRegisterRecommendController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/23.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveRegisterRecommendController.h"
#import "LiveRegisterRecommendCell.h"
#import "UIImage+LY.h"
@interface LiveRegisterRecommendController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UIButton *ensureButton;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *jumpButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topCons;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomCons;
@property (nonatomic, strong) NSMutableArray * selectedIndexPaths;
@property (nonatomic, strong) UIImage * gradientImage;
@end

@implementation LiveRegisterRecommendController

- (IBAction)ensureBtnClicked:(UIButton *)sender {
    sender.userInteractionEnabled = NO;
    NSMutableArray * array = [NSMutableArray new];
    for (NSIndexPath * path in self.selectedIndexPaths) {
        [array addObject:self.dataArray[path.row][@"user_id"]];
    }
    NSString * str = [array componentsJoinedByString:@","];
    NSLog(@"选择关注的人--- %@",str);
    [self request:str];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.topCons.constant = Is_iPhoneX ? naviBarHeight + 10: 40;
    self.bottomCons.constant = 25 + IphoneX_Bottom;
    self.view.backgroundColor = [UIColor whiteColor];
    self.gradientImage = [[UIImage alloc]initWithGradient:@[[UIColor colorWithHexString:@"#45E994"],[UIColor colorWithHexString:@"#22BBB9"]] size:CGSizeMake(180, 40) direction:UIImageGradientColorsDirectionHorizontal];
    [self.ensureButton setBackgroundImage:self.gradientImage forState:UIControlStateNormal];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"LiveRegisterRecommendCell" bundle:nil] forCellWithReuseIdentifier:@"LiveRegisterRecommendCell"];
    
}

- (void)setDataArray:(NSMutableArray *)dataArray {
    _dataArray = dataArray;
    for (int i =0; i< _dataArray.count; i++) {
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        [self.selectedIndexPaths addObject:indexPath];
    }
    [self.collectionView reloadData];
}

- (void)request:(NSString *)user_ids {
    [RequestUtils commonPostRequestUtils:[@{@"users":user_ids} mutableCopy] bURL:@"live.recommended.follow" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.ensureButton.userInteractionEnabled = YES;
        [self showTips:@"成功关注主播".localized];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self dismissViewControllerAnimated:YES completion:nil];
        });
    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
        self.ensureButton.userInteractionEnabled = YES;
    }];
}

- (IBAction)jumBtnClicked:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LiveRegisterRecommendCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveRegisterRecommendCell" forIndexPath:indexPath];
    cell.dict = self.dataArray[indexPath.row];
    cell.cellSelected = [_selectedIndexPaths containsObject:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([_selectedIndexPaths containsObject:indexPath]) {
        [_selectedIndexPaths removeObject:indexPath];
    }else{
        [_selectedIndexPaths addObject:indexPath];
    }
    self.ensureButton.userInteractionEnabled = !(_selectedIndexPaths.count == 0);
    if (_selectedIndexPaths.count == 0) {
        [self.ensureButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [self.ensureButton setBackgroundColor:SPD_HEXCOlOR(@"#C7C7C7")];
    }else{
        [self.ensureButton setBackgroundImage:self.gradientImage forState:UIControlStateNormal];
    }
    [collectionView reloadItemsAtIndexPaths:@[indexPath]];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(floorf(kScreenW/2.0f), CGRectGetHeight(self.collectionView.frame)/4.0f);
}

- (NSMutableArray *)selectedIndexPaths {
    if (!_selectedIndexPaths) {
        _selectedIndexPaths = [NSMutableArray new];
    }
    return _selectedIndexPaths;
}

@end
