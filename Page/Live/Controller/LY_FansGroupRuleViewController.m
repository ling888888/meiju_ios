//
//  LY_FansGroupRuleViewController.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_FansGroupRuleViewController.h"

@interface LY_FansGroupRuleViewController ()

@end

@implementation LY_FansGroupRuleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.title = @"粉丝团规则".localized;
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.webView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(44, 0, 0, 0));
    }];
}

@end
