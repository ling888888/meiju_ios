//
//  LiveRecommendAnchorController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/23.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveRecommendAnchorController.h"
#import "UIView+LY.h"
#import "LiveRecommendCell.h"
#import "LiveModel.h"
#import "ZegoKitManager.h"

#define Space  15
#define Width ((kScreenW - Space*3)/2)

@interface LiveRecommendAnchorController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate>

//@property (nonatomic, strong) UIImageView * avatarImageView;
@property(nonatomic, strong) LY_PortraitView *portraitView;
@property (nonatomic, strong) UIView * bottomView;
@property (nonatomic, strong) UIButton * backButton;
@property (nonatomic, strong) UIButton * followButton;
@property (nonatomic, strong) UIButton * statusButton;
@property (nonatomic, strong) UILabel * anchorNameLabel;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UICollectionView * collectionView;

@end

@implementation LiveRecommendAnchorController

- (LY_PortraitView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[LY_PortraitView alloc] init];
    }
    return _portraitView;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    
    [self initSubView];

    [self.collectionView reloadData];
    [self.collectionView setContentInset:UIEdgeInsetsMake(CGRectGetMaxY(self.titleLabel.frame) +20 - 45, 0, 0, 0)];

    if (self.view.bounds.size.height < 520) {
        self.collectionView.hidden = YES;
        self.titleLabel.hidden = YES;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)dealloc
{
    NSLog(@"LiveRecommendAnchorController");
}

- (void)initSubView {
    
    self.bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, 57, self.view.bounds.size.width, self.view.bounds.size.height)];
    self.bottomView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.bottomView];
    self.bottomView.layer.cornerRadius = 20;
    self.bottomView.clipsToBounds = YES;
//    [self.bottomView addRounded:UIRectCornerTopLeft|UIRectCornerTopRight withRadius:20];

    
//    self.avatarImageView = [[UIImageView alloc]initWithFrame:CGRectMake((kScreenW - 70)/2, 0, 70, 70)];
    [self.view addSubview:self.portraitView];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(105);
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(0);
        //        make.centerY.mas_equalTo(self.bottomView.mas_top);
    }];
    self.portraitView.portrait = self.liveInfo.portrait;
//    self.avatarImageView.contentMode = UIViewContentModeScaleAspectFill;
//    self.avatarImageView.clipsToBounds = YES;
//    self.avatarImageView.layer.cornerRadius = 35;
//    self.avatarImageView.clipsToBounds = YES;
//    [self.avatarImageView fp_setImageWithURLString:self.liveInfo.avatar];
    
    self.backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.bottomView addSubview:self.backButton];
    self.backButton.frame = CGRectMake(0, 0, 50, 50);
    [self.backButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];
    [self.backButton setImage:[UIImage imageNamed:@"ic_zhibo_yonghu_shanchu"] forState:UIControlStateNormal];
    
    self.statusButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.bottomView addSubview:self.statusButton];
    self.statusButton.userInteractionEnabled = NO;
    [self.statusButton setTitleColor:SPD_HEXCOlOR(@"F04747") forState:UIControlStateNormal];
    self.statusButton.titleLabel.font = [UIFont mediumFontOfSize:12];
    [self.statusButton setTitle:@"直播结束".localized forState:UIControlStateNormal];
    self.statusButton.layer.borderColor = SPD_HEXCOlOR(@"#F04747").CGColor;
    self.statusButton.layer.borderWidth = 1;
    self.statusButton.frame = CGRectMake(kScreenW - 80 - 19, 15, 80, 24);
    self.statusButton.layer.cornerRadius = 12;
    self.statusButton.clipsToBounds = YES;
    
    self.anchorNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 50, kScreenW - 30, 20)];
    [self.bottomView addSubview:self.anchorNameLabel];
    self.anchorNameLabel.font = [UIFont mediumFontOfSize:18];
    self.anchorNameLabel.textColor = SPD_HEXCOlOR(@"#1A1A1A");
    self.anchorNameLabel.text = self.liveInfo.userName;
    self.anchorNameLabel.textAlignment = NSTextAlignmentCenter;
    
    self.followButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.bottomView addSubview:self.followButton];
    self.followButton.frame = CGRectMake((kScreenW - 100)/2, CGRectGetMaxY(self.anchorNameLabel.frame)+14, 100, 28);
    self.followButton.layer.cornerRadius = 12;
    self.followButton.clipsToBounds = YES;
    [self.followButton setBackgroundColor:SPD_HEXCOlOR(@"5591FF")];
    [self.followButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.followButton setTitle:@"关注".localized forState:UIControlStateNormal];
    [self.followButton setTitle:@"已关注".localized forState:UIControlStateSelected];
    self.followButton.titleLabel.font = [UIFont mediumFontOfSize:13];
    [self.followButton addTarget:self action:@selector(handleFollow:) forControlEvents:UIControlEventTouchUpInside];
    [self.followButton setBackgroundColor:SPD_HEXCOlOR(self.liveInfo.isFollow ?@"C4C4C4":@"5591FF")];
    self.followButton.selected = self.liveInfo.isFollow;
    
    self.titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(self.followButton.frame)+45, kScreenW - 30, 20)];
    [self.bottomView addSubview:self.titleLabel];
    self.titleLabel.font = [UIFont mediumFontOfSize:16];
    self.titleLabel.textColor = SPD_HEXCOlOR(@"#1A1A1A");//
    self.titleLabel.text = [NSString stringWithFormat:@"- %@ -",@"为你推荐更多精彩直播".localized];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.bottomView addSubview:self.collectionView];
    self.collectionView.backgroundColor = [UIColor clearColor];
    [self.bottomView bringSubviewToFront:self.followButton];
}

- (void)handleBack:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)handleFollow:(UIButton *)sender {
    if (sender.selected) {
        return;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveRecommendAnchorController:didClickedFollowButton:)]) {
        [self.delegate liveRecommendAnchorController:self didClickedFollowButton:sender];
    }
    sender.selected = !sender.selected;
    [self.followButton setBackgroundColor:SPD_HEXCOlOR(sender.selected ?@"C4C4C4":@"5591FF")];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LiveRecommendCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveRecommendCell" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    if (cell.model.isBroadcast) {
        [cell.liveFlagImageView startAnimating];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self dismissViewControllerAnimated:YES completion:^{
         [ZegoManager joinLiveRoomWithliveInfo:self.dataArray[indexPath.row] isAnchor:NO];
    }];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(Space, Space, Space, Space);
}

CGFloat lastContentOffsetY;


-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {

    lastContentOffsetY = scrollView.contentOffset.y;

}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint point = self.collectionView.contentOffset;
    CGFloat alpha = -point.y/152.0f;
    self.portraitView.alpha = alpha;
    self.anchorNameLabel.alpha = alpha;
    self.followButton.alpha = alpha;
    self.statusButton.alpha = alpha;
    if (scrollView.contentOffset.y > lastContentOffsetY) {
        CGRect frame = self.titleLabel.frame;
         frame.origin.y = frame.origin.y <= 17 ? 17: -self.collectionView.contentOffset.y - 20;
         [UIView animateWithDuration:0.01 animations:^{
            self.titleLabel.frame = frame;
         }];

    }else if (scrollView.contentOffset.y < lastContentOffsetY) {
        if (scrollView.contentOffset.y < 0) { // 向下g && 超过 0 的位置 才开始改变 titleLabel
            CGRect frame = self.titleLabel.frame;
            frame.origin.y =  -self.collectionView.contentOffset.y +20 >= 152 ? 152: -self.collectionView.contentOffset.y +20;
            [UIView animateWithDuration:0.01 animations:^{
                self.titleLabel.frame = frame;
            }];
        }
    }

}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout new];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.minimumInteritemSpacing = Space;
        layout.minimumLineSpacing = Space;
        layout.itemSize = CGSizeMake(Width, Width + 30);
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 45, self.view.bounds.size.width, self.bottomView.bounds.size.height - 45) collectionViewLayout:layout];
        [_collectionView registerNib:[UINib nibWithNibName:@"LiveRecommendCell" bundle:nil] forCellWithReuseIdentifier:@"LiveRecommendCell"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = NO;
//        _collectionView.alwaysBounceVertical = YES;
    }
    return _collectionView;
}

@end
