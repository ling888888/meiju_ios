//
//  LY_LiveAudienceHalfVC.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/17.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_LiveAudienceHalfVC.h"
#import "LY_Animator.h"

#import "LY_LiveAudienceListViewController.h"
#import "LY_LiveRankListViewController.h"


@interface LY_LiveAudienceHalfVC () <UIViewControllerTransitioningDelegate, LY_LiveRankListViewControllerDelegate>

@property(nonatomic, strong) NSArray *VCArray;

@property(nonatomic, strong) NSArray *titleArray;

@end

@interface LY_LiveAudienceHalfVC () {
    LY_Animator * _animator;
}

@end

@implementation LY_LiveAudienceHalfVC

- (NSArray *)VCArray {
    if(!_VCArray) {
        LY_LiveRankListViewController *rankVC1 = [[LY_LiveRankListViewController alloc] init];
        rankVC1.entrance = self.type;
        rankVC1.type = @"2";
        rankVC1.liveId = self.liveId;
        rankVC1.delegate = self;
        LY_LiveRankListViewController *rankVC2 = [[LY_LiveRankListViewController alloc] init];
        rankVC2.entrance = self.type;
        rankVC2.type = @"1";
        rankVC2.liveId = self.liveId;
        rankVC2.delegate = self;
        LY_LiveRankListViewController *rankVC3 = [[LY_LiveRankListViewController alloc] init];
        rankVC3.entrance = self.type;
        rankVC3.type = @"4"; // 月榜 4
        rankVC3.liveId = self.liveId;
        rankVC3.delegate = self;
        LY_LiveAudienceListViewController *audienceListVC = [[LY_LiveAudienceListViewController alloc] init];
        audienceListVC.liveId = self.liveId;
        audienceListVC.showInviteOnlineLinkMic = _showInviteOnlineLinkMic;
        if ([self.type isEqualToString:@"mine"]) {
            _VCArray = @[rankVC2, rankVC3];
        } else {
            _VCArray = @[rankVC1, rankVC2, rankVC3, audienceListVC];
        }
    }
    return _VCArray;
}

- (NSArray *)titleArray {
    if (!_titleArray) {
        if ([self.type isEqualToString:@"mine"]) {
            _titleArray = @[@"周榜", @"总榜"];
        } else {
            _titleArray = @[@"本场榜", @"周榜", @"月榜", @"在线"];
        }
    }
    return _titleArray;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.modalPresentationStyle = UIModalPresentationCustom;
        
        _animator = [[LY_Animator alloc] initWithControllerHeight:0];
        
        self.transitioningDelegate = _animator;
    }
    return self;
}

- (instancetype)initWithControllerHeight:(CGFloat)controllerHeight {
    self = [super init];
    if (self) {
        self.modalPresentationStyle = UIModalPresentationCustom;
        
        _animator = [[LY_Animator alloc] initWithControllerHeight:controllerHeight];
        
        self.transitioningDelegate = _animator;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleColorNormal = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.6];
    self.titleColorSelected = [UIColor colorWithHexString:@"#1A1A1A"];
    self.titleSizeNormal = 16;
    self.titleSizeSelected = 16;
    self.menuView.fontName = @"PingFangSC-Semibold";;
    self.menuView.style = WMMenuViewStyleLine;
    self.menuView.progressHeight = 2;
    self.menuView.lineColor = [UIColor colorWithHexString:@"#00D4A0"];
    self.menuView.progressViewBottomSpace = 10;
    self.progressWidth = 30;
    self.menuItemWidth = kScreenW / 4;
    [self reloadData];
    
    //[MobClick event:@"FM12_3_4"];
}

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.titleArray.count;
}

- (__kindof UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    return self.VCArray[index];
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    NSString *title = self.titleArray[index];
    return title.localized;
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    return CGRectMake(0, 0, kScreenW, 60);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    return CGRectMake(0, 60, kScreenW, 440);
}

- (void)liveRankListViewControllerDidClick:(UIButton *)giftingBtn {
    if ([self.giftingBtnDelegate respondsToSelector:@selector(liveAudienceHalfVCDidClick:)]) {
        [self dismissViewControllerAnimated:true completion:nil];
        [self.giftingBtnDelegate liveAudienceHalfVCDidClick:giftingBtn];
    }
}

@end

