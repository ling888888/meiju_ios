//
//  LiveApplyAnchorConditionsViewController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/5/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveApplyAnchorConditionsViewController.h"
#import "AnchorApplyConditionTitleView.h"
#import "LiveApplyAnchorSubmitController.h"
#import "UIScrollView+LY.h"

@interface LiveApplyAnchorConditionsViewController ()

@property (nonatomic, strong) UIScrollView * scrollView;
@property (nonatomic, strong) UIImageView * headerImageView;

@property (nonatomic, strong) LiveApplyAnchorSubmitController* applyVC;

@end

@implementation LiveApplyAnchorConditionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (@available(iOS 11.0, *)) {
        self.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    self.scrollView.tipText = @"审核结果将通过famy系统消息和你填写的联系方式进行通知".localized;
    self.scrollView.tipImageName = @"img_kongyemian_zhiboshenqing_shenhezhong";
    self.scrollView.backgroundColor = SPD_HEXCOlOR(@"ffffff");
    [self.view addSubview:self.scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.leading.mas_equalTo(kIsMirroredLayout ? 0 : 0);
        make.bottom.mas_equalTo(0);
        make.trailing.mas_equalTo(0);
    }];
    [self requestData];
}

- (void)backBtnAction {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:0];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar vhl_setBackgroundAlpha:1];
}



- (void)requestData {
    [RequestUtils commonGetRequestUtils:[@{@"text":@"123"} mutableCopy] bURL:@"live.apply.info" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
       
        NSNumber * type = suceess[@"type"];
        self.scrollView.showEmptyTipView = ([type integerValue] == 1);
        if ([type integerValue] == 1) {
            // 审核中
            return ;
        }
        [self.scrollView setContentSize:CGSizeMake(kScreenW, 200+1200 + IphoneX_Bottom + 50 + (kIsMirroredLayout ? 50 : 0))];
        self.scrollView.backgroundColor = SPD_HEXCOlOR(COMMON_PINK);
        [self.scrollView addSubview:self.headerImageView];
        self.headerImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"img_zhaomuzhubo_biaoti_%@",[SPDCommonTool getFamyLanguage]]];
        [self.headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.leading.equalTo(self.view.mas_leading).offset(0);
            make.width.mas_equalTo(kScreenW);
            make.height.mas_equalTo(330);
        }];
        
        UILabel * titlelabel = [UILabel new];
        titlelabel.textColor =SPD_HEXCOlOR(@"ffffff");
        titlelabel.font = [UIFont boldSystemFontOfSize:15];
        titlelabel.numberOfLines = 0;
        titlelabel.text = @"新的一年famy全新升级，为了感谢大家的支持，我们将提供大家一个展示自己的机会。 我们现在向全平台征集声音甜美，多才多艺，喜欢和大家一起聊天交朋友的用户成为首批famy声音主播。".localized;
        [self.scrollView addSubview:titlelabel];
        [titlelabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(self.view.mas_leading).offset(20);
            make.top.mas_equalTo(330);
            make.width.mas_equalTo(kScreenW - 40);
        }];
        
        UIImageView * bg = [UIImageView new];
        bg.image = [UIImage imageNamed:@"bg_zhaomuzhubo_jianjie"];
        [self.scrollView insertSubview:bg belowSubview:titlelabel];
        [bg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(titlelabel.mas_leading).offset(-10);
            make.bottom.equalTo(titlelabel.mas_bottom).offset(28);
            make.top.mas_equalTo(330);
            make.trailing.equalTo(titlelabel.mas_trailing).offset(10);
        }];
        
        AnchorApplyConditionTitleView * titleView = [[[NSBundle mainBundle] loadNibNamed:@"AnchorApplyConditionTitleView" owner:self options:nil]lastObject];
        titleView.title = @"需满足条件".localized;
        [self.scrollView addSubview:titleView];
        [titleView mas_makeConstraints:^(MASConstraintMaker *make) {
            //            make.leading.mas_equalTo(0);
            make.leading.equalTo(self.view.mas_leading).offset(0);
            
            make.width.mas_equalTo(kScreenW);
            make.height.mas_equalTo(50);
            make.top.equalTo(bg.mas_bottom).offset(30);
        }];
        
        CGFloat leading = 30;
        CGFloat qualified= true;
        NSMutableArray * array = [suceess[@"condition"] mutableCopy];
        [array addObject:@{@"qualified":@(1),@"content":@" 能唱歌或者朗诵诗歌给进入房间的朋友".localized}];
        for (int i = 0; i < array.count; i++) {
            UIImageView * img = [UIImageView new];
            [self.scrollView addSubview:img];
            BOOL b = [array[i][@"qualified"] boolValue];
            if (!b) {
                qualified = false;
            }
            if (i == array.count -1) {
                img.image = [UIImage imageNamed:@"ic_zhaomuzhubo_dian"];
            }else{
                img.image = [UIImage imageNamed:b ? @"ic_zhaomuzhubo_dui":@"ic_zhaomuzhubo_cuo"];
            }
            [img mas_makeConstraints:^(MASConstraintMaker *make) {
                //                make.leading.mas_equalTo(leading);
                make.leading.equalTo(self.view.mas_leading).offset(leading);
                make.width.and.height.mas_equalTo(15);
                if (i != 0) {
                    make.top.equalTo(self.scrollView.subviews[self.scrollView.subviews.count - 2].mas_bottom).offset(15);
                }else{
                    make.top.equalTo(titleView.mas_bottom).offset(55);
                }
            }];
            
            UILabel * label = [UILabel new];
            label.textColor = SPD_HEXCOlOR(@"ffffff");
            label.font = [UIFont boldSystemFontOfSize:15];
            label.text = array[i][@"content"];
            label.numberOfLines = 0;
            [self.scrollView addSubview:label];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.leading.equalTo(img.mas_trailing).offset(10);
                make.centerY.equalTo(img.mas_centerY).offset(0);
                make.width.mas_lessThanOrEqualTo(kScreenW - 65);
            }];
        }
        
        AnchorApplyConditionTitleView * titleView1 = [[[NSBundle mainBundle] loadNibNamed:@"AnchorApplyConditionTitleView" owner:self options:nil]lastObject];
        titleView1.title = @"主播必须做到：".localized;
        [self.scrollView addSubview:titleView1];
        [titleView1 mas_makeConstraints:^(MASConstraintMaker *make) {
            //            make.leading.mas_equalTo(0);
            make.leading.equalTo(self.view.mas_leading).offset(0);
            
            make.width.mas_equalTo(kScreenW);
            make.height.mas_equalTo(50);
            make.top.equalTo(self.scrollView.subviews[self.scrollView.subviews.count - 2].mas_bottom).offset(50);
        }];
        
        UILabel * secondLabel = [UILabel new];
        [self.scrollView addSubview:secondLabel];
        secondLabel.numberOfLines = 0;
        [secondLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            //            make.leading.mas_equalTo(30);
            make.leading.equalTo(self.view.mas_leading).offset(30);
            make.width.mas_equalTo(kScreenW - 60);
            make.top.equalTo(titleView1.mas_bottom).offset(29);
        }];
        secondLabel.font = [UIFont systemFontOfSize:15];
        secondLabel.textColor = SPD_HEXCOlOR(@"1a1a1a");
        secondLabel.text = suceess[@"live_rule"];
        
        AnchorApplyConditionTitleView * titleView2 = [[[NSBundle mainBundle] loadNibNamed:@"AnchorApplyConditionTitleView" owner:self options:nil]lastObject];
        titleView2.title = @"申请材料".localized;
        [self.scrollView addSubview:titleView2];
        [titleView2 mas_makeConstraints:^(MASConstraintMaker *make) {
            //            make.leading.mas_equalTo(0);
            make.leading.equalTo(self.view.mas_leading).offset(0);
            make.width.mas_equalTo(kScreenW);
            make.height.mas_equalTo(50);
            make.top.equalTo(secondLabel.mas_bottom).offset(50);
        }];
        
        UILabel * thirdLabel = [UILabel new];
        [self.scrollView addSubview:thirdLabel];
        thirdLabel.numberOfLines = 0;
        [thirdLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            //            make.leading.mas_equalTo(30);
            make.leading.equalTo(self.view.mas_leading).offset(30);
            
            make.width.mas_equalTo(kScreenW - 60);
            make.top.equalTo(titleView2.mas_bottom).offset(17);
        }];
        thirdLabel.font = [UIFont systemFontOfSize:15];
        thirdLabel.textColor = SPD_HEXCOlOR(@"1a1a1a");
        thirdLabel.text = @" 您需要提供一段不超过90s的录音，内容需积极向上，可以唱歌，可以朗诵，让我们了解你们的声音".localized;
        
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:!qualified ? @"条件不足".localized:@"立即申请".localized forState:UIControlStateNormal];
        btn.userInteractionEnabled = qualified;
        [btn addTarget:self action:@selector(apply:) forControlEvents:UIControlEventTouchUpInside];
        [btn setBackgroundColor:qualified? SPD_HEXCOlOR(@"#6A2CF7"):SPD_HEXCOlOR(@"#34B67A")];
        [self.view addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(-IphoneX_Bottom -19);
            make.centerX.mas_equalTo(0);
            make.width.mas_equalTo(170);
            make.height.mas_equalTo(50);
        }];
        btn.layer.cornerRadius = 25;
        btn.clipsToBounds = YES;
        
    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)apply:(UIButton *)sender {
    [self.navigationController pushViewController:self.applyVC animated:YES];
}


#pragma mark - getters

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectZero];
        _scrollView.backgroundColor = SPD_HEXCOlOR(COMMON_PINK);
    }
    return _scrollView;
}

- (UIImageView *)headerImageView {
    if (!_headerImageView) {
        _headerImageView = [UIImageView new];
    }
    return _headerImageView;
}

- (LiveApplyAnchorSubmitController *)applyVC {
    if (!_applyVC) {
        _applyVC = [LiveApplyAnchorSubmitController new];
    }
    return _applyVC;
}

@end
