//
//  RocketWebViewController.h
//  SimpleDate
//
//  Created by ling Hou on 2020/9/3.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_WebViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RocketWebViewController : BaseViewController

@property (nonatomic, copy) NSString * urlString;

@property (nonatomic, copy) NSString * type; // live chat

@end

NS_ASSUME_NONNULL_END
