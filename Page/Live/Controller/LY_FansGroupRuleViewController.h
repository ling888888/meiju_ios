//
//  LY_FansGroupRuleViewController.h
//  SimpleDate
//
//  Created by jianyue on 2020/4/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_WebViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_FansGroupRuleViewController : LY_WebViewController

@end

NS_ASSUME_NONNULL_END
