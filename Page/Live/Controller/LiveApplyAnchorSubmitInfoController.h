//
//  LiveApplyAnchorSubmitInfoController.h
//  SimpleDate
//
//  Created by ling Hou on 2020/5/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "BaseViewController.h"
#import "LY_BaseViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface LiveApplyAnchorSubmitInfoController : LY_BaseViewController

@end

NS_ASSUME_NONNULL_END
