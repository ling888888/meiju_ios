//
//  LiveFreeGiftViewController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/22.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LiveFreeGiftViewController.h"
#import "SPDPresentFlowLayout.h"
#import "LiveFreeGiftCell.h"
#import "LY_FansGroupRuleViewController.h"
@interface LiveFreeGiftViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView * collectionView;
@property (nonatomic, strong) UILabel * tipsLabel;

@end

@implementation LiveFreeGiftViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"收听奖励".localized;
    self.navigationController.navigationBar.tintColor = [UIColor colorWithHexString:@"#ffffff"];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"ffffff"],
                                                                    NSFontAttributeName: [UIFont mediumFontOfSize:18]};
    
    self.view.backgroundColor = SPD_HEXCOlOR(@"#4B3390");
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ic_zhubo_guize_bai"] style:UIBarButtonItemStylePlain target:self action:@selector(handle)];
    
    [self.view addSubview:self.tipsLabel];
    [self.tipsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.bottom.mas_equalTo(-13);
        make.height.mas_equalTo(20);
    }];
    
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(0);
        make.top.mas_equalTo(55);
        make.width.mas_equalTo(kScreenW);
        make.height.mas_equalTo(127*2 +9);
    }];
    [self.collectionView reloadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleFreeGiftTimer:) name:@"LiveFreeGiftTimerManager" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleReceivedAllFreeGiftTimer:) name:@"ReceivedAllFreeGifts" object:nil];

}

- (void)handle {
    LY_FansGroupRuleViewController * vc = [[LY_FansGroupRuleViewController alloc]init];
    vc.navigationItem.title = @"规则".localized;
    vc.view.backgroundColor = SPD_HEXCOlOR(@"4B3390");
    NSArray * array = [[SPDCommonTool getFamyLanguage] componentsSeparatedByString:@"-"];
    vc.URLString = [NSString stringWithFormat:@"https://share.famy.ly/ar/anchorRule.html?lang=%@",array[0]];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)handleReceivedAllFreeGiftTimer:(NSNotification *)notify {
    
    [self.collectionView reloadData];
}

- (void)handleFreeGiftTimer:(NSNotification *)notify {
    NSInteger duration = [notify.object integerValue];
    NSDictionary * dict = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"LiveFreeGift:%@",[SPDApiUser currentUser].userId]];
    NSInteger index = [dict[@"index"] integerValue];
    if (duration != 0) {// 更新描述
        LiveFreeGiftCell * cell =(LiveFreeGiftCell *) [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        NSDictionary * dic = self.dataArray[index];
        [cell updateSecond:duration];
        if ([dic[@"timeLimit"] integerValue] == duration ) {
            [self.collectionView reloadData];
        }
    }else{
       LiveFreeGiftCell * cell =(LiveFreeGiftCell *) [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        cell.statusLabel.text = [NSString stringWithFormat:@"%@",@"可领取".localized];
    }
}

#pragma mark - collection

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LiveFreeGiftCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveFreeGiftCell" forIndexPath:indexPath];
    cell.index = indexPath.row;
    cell.dict = self.dataArray[indexPath.row];
    return cell;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
        return self.dataArray.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((ceil(kScreenW - 15*2-9*2)/3.0f), floor((127*2 + 9 - 3*9)/2.0f));
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 9;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 9;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(9, 15, 9, 15);
}

#pragma mark - getters

- (UILabel *)tipsLabel {
    if (!_tipsLabel) {
        _tipsLabel = [UILabel new];
        _tipsLabel.font = [UIFont systemFontOfSize:11];
        _tipsLabel.textColor = [SPD_HEXCOlOR(@"#FFFFFF") colorWithAlphaComponent:0.7];
        _tipsLabel.text = @"免费礼物将帮助主播提升人气和心动值".localized;
    }
    return _tipsLabel;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout new];
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor clearColor];
        [_collectionView registerNib:[UINib nibWithNibName:@"LiveFreeGiftCell" bundle:nil] forCellWithReuseIdentifier:@"LiveFreeGiftCell"];
    }
    return _collectionView;
}

@end
