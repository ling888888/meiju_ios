//
//  LY_LiveRankListViewController.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_BaseTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol LY_LiveRankListViewControllerDelegate <NSObject>

- (void)liveRankListViewControllerDidClick:(UIButton *)giftingBtn;

@end

@interface LY_LiveRankListViewController : LY_BaseTableViewController

@property (nonatomic, weak) id<LY_LiveRankListViewControllerDelegate> delegate;

@property(nonatomic, copy) NSString *type;

@property(nonatomic, copy) NSString *entrance;

@property(nonatomic, copy) NSString *liveId;

@end

NS_ASSUME_NONNULL_END
