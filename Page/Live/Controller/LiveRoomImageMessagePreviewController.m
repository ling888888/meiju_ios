//
//  LiveRoomImageMessagePreviewController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/4/2.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomImageMessagePreviewController.h"

@interface LiveRoomImageMessagePreviewController ()
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;

@end

@implementation LiveRoomImageMessagePreviewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:_url]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
