//
//  LiveRoomImageMessagePreviewController.h
//  SimpleDate
//
//  Created by ling Hou on 2020/4/2.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveRoomImageMessagePreviewController : BaseViewController

@property (nonatomic, copy)NSString * url;

@end

NS_ASSUME_NONNULL_END
