//
//  LiveRoomViewController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/3/20.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LiveRoomViewController.h"
#import "LiveRoomTitleView.h"
#import "LiveOnlineUserNumView.h"
#import "LiveAnchorOfflineView.h"
#import "RadioRoomTextMessageCell.h"
#import "LiveRoomAnchorBottomView.h"
#import "LiveRoomAudienceBottomView.h"
#import "RCDLiveInputBar.h"
#import "LiveModel.h"
#import "LiveRoomSystemMessage.h"
#import "LiveRoomSystemMessageCell.h"
#import "LiveRoomTextMessage.h"
#import "LiveJoinMessage.h"
#import "HomeModel.h"
#import "LiveRoomGiftView.h"
#import "LiveJoinMessageCell.h"
#import "ZegoKitManager.h"
#import "LiveModel.h"
#import "SPDVoiceLiveUserModel.h"
#import "LiveRoomUserInfoView.h"
#import "LiveGiftMessage.h"
#import "LiveRoomAnchorToolsView.h"
#import "LiveUserUpdateMessage.h"
#import "ZLPhotoPickerViewController.h"
#import "LiveImageMessage.h"
#import "LiveChangeManagerMessage.h"

#import "LiveRoomImageMessageCell.h"
#import "ZLPhotoAssets.h"
#import "LiveRoomGiftAnimationView.h"
#import "LiveGiftModel.h"
#import "LiveRoomBackgroundMusicView.h"
#import "LY_LiveNoSpeakAlertView.h"
#import "LiveEndedAnchorView.h"
#import "LiveUserModel.h"
#import "LiveFollowMessage.h"
#import "LY_LiveAudienceHalfVC.h"
#import "LiveRoomOpenMicView.h"
#import "LiveRoomGiftComboAnimationView.h"
#import "LiveRoomImageMessagePreviewController.h"
#import "LY_LiveWalletViewController.h"
#import "LY_LiveRecordViewController.h"
#import "LiveRoomGiftRecordButton.h"
#import "LiveRoomGiftRecordView.h"
#import "LiveShareView.h"
#import "LiveRoomAudioEffectView.h"
#import "LiveRoomAudioConsoleView.h"
#import "LY_LiveRechargeConchView.h"
#import "LiveShareTipsView.h"
#import "SPDKeywordMatcher.h"
#import "LY_HeartbeatView.h"
#import "JXPopoverView.h"
#import "AppDelegate.h"
#import "ChatroomGiftComboButton.h"
#import "LiveFreeGiftIconView.h"
#import "LiveFreeGiftTimerManager.h"
#import "LY_FansView.h"
#import "LY_HalfNavigationController.h"
#import "LY_FanGroupViewController.h"
#import "LY_FansListViewController.h"
#import "LY_FunsGroupCreatViewController.h"
#import "LY_LiveFansInviteMessageCell.h"
#import "LY_LiveFansJoinMessageView.h"
#import "LY_LiveFansLevelMessage.h"
#import "LY_LiveFollowedMessageCell.h"
#import "LiveFreeGiftViewController.h"
#import "SDCycleScrollView.h"
#import "BannerModel.h"
#import "LiveRecommendAnchorController.h"
#import "QuickMessageTopBar.h"
#import "LinkMicAnchorView.h"
#import "LinkMicAudienceDialingView.h"
#import "LiveStateMessage.h"
#import "LinkMicAudienceListButton.h"
#import "BarrageTopBarView.h"
#import "BarrageRenderer.h"
#import "LiveNormalBarrageMessage.h"
#import "LiveFansBarrageMessage.h"
#import "LiveRoomTextMessageCell.h"
#import "LiveUnreadMessageView.h"
#import "LiveGiftMessageCell.h"
#import "LiveWealthLevelMessage.h"
#import "UIButton+HXExtension.h"
#import "LY_HomePageInfo.h"
#import "ReportViewController.h"
#import "LiveRoomEnteringView.h"
#import "LiveWealthLevelMessageCell.h"
#import "LY_LivePlayMusicView.h"
#import "SPDCommonDefine.h"
#import "SPDVehicleEffectImageView.h"
#import "SPDChatroomNobleEnterView.h"
#import "PKInviteViewController.h"
#import "PKBeginAnimationView.h"
#import "LivePKInviteMessage.h"
#import "LivePKInviteRefuseMessage.h"
#import "LivePKInviteBusyMessage.h"
#import "LivePKBeginMessage.h"
#import "PKProcessAnimationView.h"
#import "LivePKTimeMessage.h"
#import "LivePKScoreMessage.h"
#import "PKCheckBeheadingMsg.h"
#import "PKResultMessage.h"
#import "PKReceiveInviteMessageView.h"
#import "PKScoreViewController.h"
#import "PKContributionListController.h"
#import "LiveWorldMessage.h"
#import "LiveWorldMessageView.h"
#import "ChatroomMicCell.h"
#import "LinkMicUserCell.h"
#import "LinkMicNewModel.h"
#import "LiveApplyMicMessage.h"
#import "LinkMicReceiveApplyMsgView.h"
#import "LiveLinkMicAgreeMessage.h"
#import "LiveLinkMicSilenceMsg.h"
#import "LinkMicSettingsView.h"
#import "RocketWaveView.h"
#import "LY_WebViewController.h"
#import "RocketWebViewController.h"
#import "RocketBoomView.h"
#import "LiveCancelInvitePk.h"
#import "LiveManagerListController.h"
#import "LiveAudiencePlayBottomView.h"
#import "LiveLuckyNumberMessage.h"
#import "LiveAnchorWalletController.h"
#import "LiveRedPackModel.h"
#import "LiveRedPackIconView.h"
#import "LiveRedPackMessage.h"

#define LinkMicCollectionHeight  (114.0f/375 * kScreenW  + (kScreenW - 2* 11)/4.0f * 2)

#define MessageCollectionBottom  (self.isAnchor ? (85+IphoneX_Bottom):(58+IphoneX_Bottom))

static CGFloat QuickMessageBarHeight = 40.0f;

@interface LiveRoomViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,LiveRoomAudienceBottomViewDelegate,RCTKInputBarControlDelegate,LiveRoomAnchorBottomViewDelegate,ZegoAudioRoomDelegate,LiveRoomTitleViewDelegate,LiveRoomAnchorToolsViewDelegate,LiveOnlineUserNumViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,LiveRoomUserInfoViewDelegate,LY_LiveNoSpeakAlertViewDelegate,LiveRoomTitleViewDelegate,LiveEndedAnchorViewDelegate,LiveRoomImageMessageCellDelegate,LY_LiveAudienceHalfVCDelegate,LiveRoomAudioEffectViewDelegate,ChatroomGiftComboButtonDelegate,LiveRoomGiftViewDelegate,SDCycleScrollViewDelegate,LiveRecommendAnchorControllerDelegate,QuickMessageTopBarDeleagate,  LY_FanGroupViewControllerDelegate, LY_LiveFansInviteMessageCellDelegate, LY_FunsGroupCreatViewControllerDelegate, BarrageTopBarViewDelegate, ZegoKitManagerDelegate,UIGestureRecognizerDelegate, LinkMicAnchorViewDelegate,PKInviteViewControllerDelegate,PKBeginAnimationViewDelegate,PKProcessAnimationViewDelegate,LiveAudiencePlayBottomViewDelegate>

@property (nonatomic, strong) UIImageView * backgroundImageView;// 背景图
@property (nonatomic, strong) UIButton * exitButton;
@property (nonatomic, strong) LiveRoomTitleView * titleView;
@property (nonatomic, strong) LiveOnlineUserNumView * onlineNumView;
@property (nonatomic, strong) LiveAnchorOfflineView *anchorOfflineView;
@property (nonatomic, strong) UICollectionView * messageCollectionView;
@property (nonatomic, strong) NSMutableArray<RCMessageContent *> *messageArr;
@property (nonatomic, strong) LiveRoomAnchorBottomView * anchorBottomView;
@property (nonatomic, strong) LiveRoomAudienceBottomView * audienceBottomView;
@property (nonatomic, strong) LiveRoomGiftAnimationView * giftAnimationView; // 礼物的svga动画
@property (nonatomic, strong) LiveRoomGiftComboAnimationView *giftComboAnimationView; // 礼物动画
@property (nonatomic, strong) RCDLiveInputBar *inputBar;
@property (nonatomic, strong) LiveRoomAnchorToolsView * toolsView;
@property (nonatomic, strong) HomeModel * currentUserModel; // 当前用户的model

@property (nonatomic, assign) NSTimeInterval joinTimeStamp;
@property (nonatomic, strong) NSMutableDictionary *onlineUserDic;
@property (nonatomic, assign) NSInteger onlineNumber;
@property (nonatomic, strong) LiveRoomBackgroundMusicView *backgroundMusicView;
@property (nonatomic, strong) LiveRoomGiftRecordButton *giftRecordButton;
@property (nonatomic, strong) NSTimer *durationTimer;
@property (nonatomic, assign) NSInteger durationTime;
@property (nonatomic, strong) LiveRoomAudioConsoleView *audioConsoleView;
@property (nonatomic, strong) LiveShareTipsView *shareTipsView;
@property (nonatomic, weak) LiveRoomEnteringView *enteringView;

@property(nonatomic, strong) LY_HeartbeatView *heartbeatView;
@property(nonatomic, strong) LY_FansView *fansView;
@property(nonatomic, strong) LY_LiveFansJoinMessageView *fansJoinMessageView;

@property (nonatomic, strong) NSTimer *fansTaskTimer;
@property (nonatomic, assign) NSInteger taskSecond;
@property (nonatomic, assign) BOOL isShowedInviteMessage;

@property (nonatomic,copy) NSDictionary *noticeUserInfo; // @ 消息所需
@property (nonatomic,strong) SPDKeywordMatcher * keyword_match;
@property (nonatomic,strong) KeywordMap * keymap;
@property (nonatomic,copy) AFNetworkReachabilityManager *networkReachabilityManager;
@property (nonatomic, strong) NSString *giftComboURL;
@property (nonatomic, strong) NSMutableDictionary *giftComboParams;

@property (nonatomic, strong) SDCycleScrollView *clanActivityScrollView; //家族活动
@property (nonatomic, strong) NSMutableArray *clanActivityArray;
@property (nonatomic, strong) QuickMessageTopBar * quickMessageBar;

@property (nonatomic, strong) BarrageTopBarView * barrageTopBarView;
@property (nonatomic, assign) BarrageType barrageType;
@property (nonatomic, strong) BarrageRenderer *barrageRenderer;
@property (nonatomic, copy) NSString * barrageBalanceString;
@property (nonatomic, assign) NSInteger unreadNewMsgCount;
@property (nonatomic, strong) LiveUnreadMessageView * unreadMessageView;
@property (nonatomic, assign) BOOL showSendMessageTips; // 直播间发送消息太快提示
@property (nonatomic, strong) SPDVehicleEffectImageView * vehicleEffectView;
@property (nonatomic, strong) SPDChatroomNobleEnterView * nobleEnterView;
@property (nonatomic, assign) LiveRoomAnchorPKStatus pkStatus; // pk的状态
@property (nonatomic, strong) PKBeginAnimationView * beginPKAnimationView;
@property (nonatomic, strong) PKProcessAnimationView * processPKAnimationView;
@property (nonatomic, strong) LivePKScoreMessage * scoreMsg;
@property (nonatomic, strong) KKPaddingLabel * pkScoreLabel; // pk积分
@property (nonatomic, strong) LiveWorldMessageView * worldMsgView;
@property (nonatomic, strong) UICollectionView *bigMicCollectionView;
@property (nonatomic, strong) NSMutableDictionary * linkMicUserInfos;
@property (nonatomic, assign) BOOL openChat; // 是否开启连麦
@property (nonatomic, strong) LinkMicSettingsView * linkMicSettingsView;
@property (nonatomic, strong) RocketWaveView * rocketWaveView;
@property (nonatomic, strong) NSTimer * rocketTimer;
@property (nonatomic, strong) NSTimer * timer;
@property (nonatomic, strong) PKReceiveInviteMessageView * pkInviteView; // 收到pk 邀请的view
@property (nonatomic, strong) PKInviteViewController * pkInviteController;
@property (nonatomic, assign) NSInteger postionWeight;// 位置权重 180 主播 100 管理员 20 普通观众
@property (nonatomic, strong) LiveRedPackIconView * redPackIconView; // 红包的view

@end

@implementation LiveRoomViewController

- (NSMutableDictionary *)linkMicUserInfos {
    if (!_linkMicUserInfos) {
        _linkMicUserInfos = [NSMutableDictionary new];
    }
    return _linkMicUserInfos;
}

- (LiveWorldMessageView *)worldMsgView {
    if (!_worldMsgView) {
        _worldMsgView = [[LiveWorldMessageView alloc]initWithFrame:CGRectMake(0, statusBarHeight + 37.5, kScreenW, 100)];
        _worldMsgView.hidden = YES;
        [self.view insertSubview:_worldMsgView belowSubview:self.exitButton];
    }
    return _worldMsgView;
}

- (PKBeginAnimationView *)beginPKAnimationView {
    if (!_beginPKAnimationView) {
        _beginPKAnimationView =  [[PKBeginAnimationView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.heartbeatView.frame)+ 13, kScreenW, kScreenW * 0.51)];
        _beginPKAnimationView.delegate = self;
    }
    return _beginPKAnimationView;
}

- (SDCycleScrollView *)clanActivityScrollView {
    if (!_clanActivityScrollView) {
        _clanActivityScrollView = [[SDCycleScrollView alloc] init];
        _clanActivityScrollView.backgroundColor = [UIColor clearColor];
        _clanActivityScrollView.delegate = self;
        _clanActivityScrollView.bannerImageViewContentMode = UIViewContentModeScaleAspectFit;
        _clanActivityScrollView.showPageControl = YES;
        _clanActivityScrollView.pageControlDotSize = CGSizeMake(3, 3);
        _clanActivityScrollView.pageControlBottomOffset = -18;
        _clanActivityScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleAnimated;
        _clanActivityScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    }
    return _clanActivityScrollView;
}

//LY_FansView
- (LY_FansView *)fansView {
    if (!_fansView) {
        _fansView = [[LY_FansView alloc] init];
        _fansView.backgroundColor = [[UIColor colorWithHexString:@"#FF6B93"] colorWithAlphaComponent:0.1];
        _fansView.layer.cornerRadius = 12.5;
        [_fansView addTarget:self action:@selector(popupFansView) forControlEvents:UIControlEventTouchUpInside];
    }
    return _fansView;
}

- (LY_LiveFansJoinMessageView *)fansJoinMessageView {
    if (!_fansJoinMessageView) {
        _fansJoinMessageView = [[LY_LiveFansJoinMessageView alloc] init];
    }
    return _fansJoinMessageView;
}

- (LY_HeartbeatView *)heartbeatView {
    if (!_heartbeatView) {
        _heartbeatView = [[LY_HeartbeatView alloc] init];
        _heartbeatView.backgroundColor = [[UIColor colorWithHexString:@"#FF6B93"] colorWithAlphaComponent:0.1];
        _heartbeatView.layer.cornerRadius = 12.5;
        [_heartbeatView addTarget:self action:@selector(heartbeatViewAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _heartbeatView;
}

- (LiveUnreadMessageView *)unreadMessageView {
    if (!_unreadMessageView) {
        _unreadMessageView = [[[NSBundle mainBundle]loadNibNamed:@"LiveUnreadMessageView" owner:self options:nil]lastObject];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleUnreadMessageView:)];
        [_unreadMessageView addGestureRecognizer:tap];
    }
    return _unreadMessageView;
}

- (void)setOpenChat:(BOOL)openChat {
    _openChat = openChat;
    if (_openChat) {
        [self setupMicInfo];
        [self.clanActivityScrollView mas_remakeConstraints:^(MASConstraintMaker *make) {
           make.width.mas_equalTo(83);
           make.height.mas_offset(115);
           make.trailing.mas_equalTo(-7);
            make.top.equalTo(self.bigMicCollectionView.mas_top).mas_offset(5); // 主播：需要空出礼物记录的位置
        }];
        [self.redPackIconView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.trailing.mas_equalTo(-15);
            make.size.mas_equalTo(CGSizeMake(46, 57));
            make.top.equalTo(self.messageCollectionView.mas_top).mas_offset(32);
        }];
    }else{
        [self.clanActivityScrollView mas_remakeConstraints:^(MASConstraintMaker *make) {
           make.width.mas_equalTo(83);
           make.height.mas_offset(115);
           make.trailing.mas_equalTo(-7);
           make.top.equalTo(self.messageCollectionView.mas_top).mas_offset(self.isAnchor ? 40 + 5:0); // 主播：需要空出礼物记录的位置
        }];
        [self.redPackIconView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.trailing.mas_equalTo(-15);
            make.size.mas_equalTo(CGSizeMake(46, 57));
            make.top.equalTo(self.clanActivityScrollView.mas_bottom).mas_offset(10);
        }];
    }
    self.bigMicCollectionView.hidden = !_openChat;
    [self resetMessageCollectionViewFrame];
}

- (void)resetMessageCollectionViewFrame {
    if (self.liveInfo.isBroadcast) {
        [UIView animateWithDuration:0.25 animations:^{
            [self.messageCollectionView mas_updateConstraints:^(MASConstraintMaker *make) {
                if (_openChat) {
                    make.top.mas_equalTo(LinkMicCollectionHeight + 5 +CGRectGetMaxY(self.heartbeatView.frame)+ 13);
                }else{
                    make.top.mas_equalTo(self.pkStatus == LiveRoomAnchorPKInProcessStatus ? (CGRectGetMaxY(self.heartbeatView.frame)+13 + 215 + 30):CGRectGetMaxY(self.heartbeatView.frame)+13);
                }
            }];
        }];
    }else{
        [UIView animateWithDuration:0.25 animations:^{
            [self.messageCollectionView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(_openChat ? CGRectGetMaxY(self.bigMicCollectionView.frame) + 13: CGRectGetMaxY(self.anchorOfflineView.frame) + 13);
            }];
        }];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.barrageType = BarrageType_Default;
    self.barrageBalanceString = [NSString stringWithFormat:@"粉丝团每周免费10条（剩余%@），超出100贝壳/条".localized,@"10"];
    self.joinTimeStamp = [[NSDate date] timeIntervalSince1970];
    [self registerNotification];
    [self initSubViews];
    [self loginRoom];
    [self requestUserInfo];
    [self requestLiveInfoInsertTip:YES];
    
    self.fansTaskTimer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(fansTaskTimerAction) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.fansTaskTimer forMode:NSRunLoopCommonModes];
    [self.fansTaskTimer fire];

    [self requestBanner];
    
//    if (!self.isAnchor) {
//        [self requestSysTime];
//    }
//
    [self requestQuickMessage];
    [self requestRocketSwitch];
    [self initTimer];
}

- (void)dealloc {
    NSLog(@"LiveRoomViewController dealloc");
    [self releaseTimer];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    [[IQKeyboardManager sharedManager]setEnable:NO];
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = NO;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
    [[IQKeyboardManager sharedManager]setEnable:YES];
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
}

#pragma mark - Notification

- (void)registerNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveChatroomMessage:) name:SPDLiveKitChatRoomMessageNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLiveRoomTapAvatar:) name:@"LiveRoomTapAvatar" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLongPressAvatar:) name:@"LiveLongPressAvatar" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleQuickMessageNotification:) name:@"QucikMessageNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(linkMicSwitchDidChange:) name:LinkMicSwitchDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(linkMicMineMicStatusChanged:) name:@"MeMicStatusDidChangeNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(popGift) name:@"LiveRoomShowGiftViewNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showToUserDetail:) name:@"RocketToUserDetailNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadLiveRoom:) name:@"JoinInOtherLiveRoomNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hanleMatchTimeOut:) name:@"MatchTimeAutoOutCancel" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSendMessage:) name:@"SendMessage" object:nil];
}

- (void)handleSendMessage:(NSNotification *)notify {
    NSString * message = notify.userInfo[@"msg"];
    [self onTouchSendMessageButton:message enableGlobalMessage:NO];
}

#pragma mark - private method

- (void)startNetworkMonitoring {
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    [manager startMonitoring];
    __weak __typeof(self) weakSelf = self;
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
            case AFNetworkReachabilityStatusNotReachable:
                strongSelf.titleView.showConnecting = YES;
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
            case AFNetworkReachabilityStatusReachableViaWiFi:
                strongSelf.titleView.showConnecting = NO;
                break;
            default:
                break;
        }
    }];
}

- (void)initSubViews {
    //背景图
    [self.view addSubview:self.backgroundImageView];
    [self.backgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    // title view
    [self.view addSubview:self.titleView];
    self.titleView.isAnchor = self.isAnchor;
    self.liveInfo.isFollow = YES; // 之后live.info 状态会更新
    self.titleView.liveModel = self.liveInfo;
    self.titleView.delegate = self;
    [self.titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(5+StatusBarHeight);
        make.leading.mas_equalTo(10);
        make.height.mas_equalTo(40);
    }];
    
    [self.view addSubview:self.exitButton];
    [self.exitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-10);
        make.width.and.height.mas_equalTo(20);
        make.centerY.mas_equalTo(self.titleView.mas_centerY);
    }];
    
    self.onlineNumView = [[LiveOnlineUserNumView alloc]init];
    [self.view addSubview:self.onlineNumView];
    self.onlineNumView.delegate = self;
    [self.onlineNumView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleView.mas_centerY).offset(0);
        make.trailing.equalTo(self.exitButton.mas_leading).offset(kScreenW < 375 ? -5: -28);
        make.height.mas_equalTo(26);
        make.width.mas_equalTo(100);
    }];
    
    [self.view addSubview:self.heartbeatView];
    [self.heartbeatView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.height.mas_equalTo(25);
        make.top.equalTo(self.titleView.mas_bottom).offset(10);
    }];
    
    [self.view addSubview:self.pkScoreLabel];
    [self.pkScoreLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.heartbeatView.mas_trailing).offset(10);
        make.height.mas_equalTo(25);
        make.centerY.equalTo(self.heartbeatView.mas_centerY);
    }];
    self.pkScoreLabel.hidden = !self.isAnchor;
    self.pkScoreLabel.layer.cornerRadius = 12.5;
    self.pkScoreLabel.clipsToBounds = YES;
    
    
    [self.view addSubview:self.anchorOfflineView];
    [self.anchorOfflineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.heartbeatView.mas_bottom).offset(11);
        make.leading.mas_equalTo(15);
        make.trailing.mas_equalTo(-15);
        make.height.mas_equalTo(126);
    }];
    self.anchorOfflineView.hidden = YES;
    

    [self.view layoutIfNeeded];
    
    [self.view addSubview:self.messageCollectionView];
    [self.messageCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_offset(0);
        make.trailing.mas_offset(0);
//        make.top.equalTo(.mas_bottom).offset(13);
        make.top.mas_equalTo(CGRectGetMaxY(self.heartbeatView.frame)+ 13);
        make.bottom.mas_equalTo(-MessageCollectionBottom);
    }];
    [self.messageCollectionView reloadData];
    
    [self.view addSubview:self.anchorBottomView];
    [self.anchorBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-IphoneX_Bottom);
        make.leading.and.trailing.mas_equalTo(0);
        make.height.mas_equalTo(85);
    }];
    
    [self.view addSubview:self.audienceBottomView];
    [self.audienceBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-IphoneX_Bottom);
        make.leading.and.trailing.mas_equalTo(0);
        make.height.mas_equalTo(58);
    }];
    self.anchorBottomView.hidden = !self.isAnchor;
    self.audienceBottomView.hidden = self.isAnchor;
    
    [self.view addSubview:self.clanActivityScrollView];
    [self.clanActivityScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
       make.width.mas_equalTo(83);
       make.height.mas_offset(115);
       make.trailing.mas_equalTo(-7);
       make.top.equalTo(self.messageCollectionView.mas_top).mas_offset(self.isAnchor ? 40 + 5:0); // 主播：需要空出礼物记录的位置
    }];
    
    [self.view addSubview:self.redPackIconView];
    [self.redPackIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-15);
        make.size.mas_equalTo(CGSizeMake(46, 57));
        make.top.equalTo(self.messageCollectionView.mas_top).mas_offset(32);
    }];
    
    self.giftRecordButton = [[NSBundle mainBundle] loadNibNamed:@"LiveRoomGiftRecordButton" owner:self options:nil].firstObject;
    [self.giftRecordButton addTarget:self action:@selector(clickGiftRecordButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.giftRecordButton];
    [self.giftRecordButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.heartbeatView.mas_top).offset(0);
        make.trailing.mas_equalTo(12.5);
        make.height.mas_equalTo(25);
        make.width.mas_equalTo(44.5);
    }];
    self.giftRecordButton.hidden = !self.isAnchor;
    
    
    self.shareTipsView = [[LiveShareTipsView alloc]init];
    self.shareTipsView.alpha = 0;
    self.shareTipsView.layer.cornerRadius = 14;
    self.shareTipsView.clipsToBounds = YES;
    [self.view addSubview:self.shareTipsView];
    UITapGestureRecognizer * tapShare = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleShare)];
    [self.shareTipsView addGestureRecognizer:tapShare];
    [self.shareTipsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-15);
        make.height.mas_equalTo(28);
        make.width.mas_equalTo(1);
        make.bottom.equalTo(self.audienceBottomView.mas_top).offset(-7);
    }];
    
    [self.view addSubview:self.fansJoinMessageView];
    [self.fansJoinMessageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.size.mas_equalTo(CGSizeMake(210, 26));
        make.top.equalTo(self.heartbeatView.mas_bottom).offset(10);
    }];
    
    
    [self.view layoutIfNeeded];
    
    
    if (self.isAnchor) {
        [self performSelector:@selector(handleShare) withObject:nil afterDelay:1];
    }else{
         [self performSelector:@selector(showShareView) withObject:nil afterDelay:30];
    }
    
    [self.view addSubview:self.quickMessageBar];
    [self.quickMessageBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(0);
        make.top.mas_equalTo(kScreenH);
        make.width.mas_equalTo(kScreenW);
        make.height.mas_equalTo(QuickMessageBarHeight);
    }];
    
    [self.view addSubview:self.barrageTopBarView];
    self.barrageTopBarView.alpha = 0;
    [self.barrageTopBarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(0);
        make.width.mas_equalTo(kScreenW);
        make.height.mas_equalTo(40);
        make.top.equalTo(self.quickMessageBar.mas_top).offset(QuickMessageBarHeight);
    }];
    
    [self.view addSubview:self.unreadMessageView];
    self.unreadMessageView.hidden = YES;
    [self.unreadMessageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.height.mas_equalTo(28);
        make.bottom.mas_equalTo(-MessageCollectionBottom - 10);
    }];
    self.unreadMessageView.layer.cornerRadius = 14;
    self.unreadMessageView.clipsToBounds = YES;
    
    [self.view addSubview:self.beginPKAnimationView];
    self.beginPKAnimationView.alpha = 0;
    
    [self.view insertSubview:self.bigMicCollectionView aboveSubview:self.backgroundImageView];
    [self.bigMicCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.pkScoreLabel.mas_bottom).offset(5);
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(kScreenW);
        make.height.mas_equalTo(LinkMicCollectionHeight);
    }];
    
    [self.view layoutSubviews];
    self.rocketWaveView = [[RocketWaveView alloc]initWithFrame:CGRectMake(self.isAnchor ?(kIsMirroredLayout? 20:  kScreenW - 20 - 46): (kScreenW - 15 - 46), self.isAnchor? (CGRectGetMinY(self.anchorBottomView.frame) - 20 -46): CGRectGetMinY(self.anchorBottomView.frame) - 20 - 46, 46, 46)];
    self.rocketWaveView.progress = 0.8;
    self.rocketWaveView.type = RocketWaveViewTypeLive;
    [self.view addSubview:self.rocketWaveView];
    self.rocketWaveView.userInteractionEnabled = YES;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleSelect:)];
    [self.rocketWaveView addGestureRecognizer:tap];
    
    [self.view addSubview:self.giftAnimationView];
    [self.giftAnimationView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    // 礼物连击动效
    self.giftComboAnimationView = [[LiveRoomGiftComboAnimationView alloc] initWithFrame:CGRectMake(0, CGRectGetMinY(self.messageCollectionView.frame)+ 29, kScreenW, 54 * 3 + 10 * 2)];
    [self.view addSubview:self.giftComboAnimationView];
    
    //座驾进场效果展示
    self.vehicleEffectView = [[SPDVehicleEffectImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH)];
    [self.view addSubview:self.vehicleEffectView];
    self.vehicleEffectView.hidden = YES;
    
    [self.view addSubview:self.nobleEnterView];
    
    [self.view bringSubviewToFront:self.exitButton];
}

- (void)handleSelect:(UITapGestureRecognizer *)tap {
    RocketWebViewController  * vc = [[RocketWebViewController alloc]init];
    vc.urlString = [NSString stringWithFormat:@"https://%@share.famy.ly/view/ar/luckyRocket.html?type=live&lang=%@",DEV?@"dev-":@"",[SPDCommonTool getFamyLanguage]];
    vc.type = @"live";
    LY_HalfNavigationController * v = [[LY_HalfNavigationController alloc]initWithRootViewController:vc controllerHeight:(kScreenH < 667 ?300: 550)];
    [self presentViewController:v animated:YES completion:^{
        
    }];
}

- (void)showShareView {
    self.shareTipsView.alpha = 1;
    CADisplayLink * link = [CADisplayLink displayLinkWithTarget:self selector:@selector(processShareTips:)];
    link.frameInterval = 0.05;
    [link addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
}

- (void)processShareTips:(CADisplayLink *)displayLink {
    [self.view layoutIfNeeded];
    CGFloat item = ceilf(self.shareTipsView.width/ 30);
    if (CGRectGetWidth(self.shareTipsView.frame) <= self.shareTipsView.width) {
        [self.shareTipsView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(CGRectGetWidth(self.shareTipsView.frame) + item);
        }];
    }else{
       [displayLink invalidate];
       displayLink = nil;
       [self performSelector:@selector(hideShare) withObject:nil afterDelay:10];
    }
}

- (void)hideShare {
    CADisplayLink * link = [CADisplayLink displayLinkWithTarget:self selector:@selector(hideShareTips:)];
    link.frameInterval = 0.05;
    [link addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
}

- (void)hideShareTips:(CADisplayLink *)displayLink {
    [self.view layoutIfNeeded];
    CGFloat item = ceilf(self.shareTipsView.width/ 30);
    if (CGRectGetWidth(self.shareTipsView.frame) >0 ) {
        [self.shareTipsView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(CGRectGetWidth(self.shareTipsView.frame) - item);
        }];
    }else{
       [displayLink invalidate];
       displayLink = nil;
    }
}

- (void)handleClearInputBar {
    [self.inputBar setInputBarStatus:RCDLiveBottomBarDefaultStatus];
}

- (void)handleShare {
    self.shareTipsView.alpha = 0;
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideShare) object:nil];
    LiveShareView * vc = [[[NSBundle mainBundle]loadNibNamed:@"LiveShareView" owner:self options:nil]lastObject];
    vc.frame = self.view.bounds;
    vc.liveInfo = self.liveInfo;
    [self.view addSubview:vc];
    [vc show];
}

- (void)popGift {
    LiveRoomGiftView * giftView = [[[NSBundle mainBundle]loadNibNamed:@"LiveRoomGiftView" owner:self options:nil]firstObject];
    giftView.frame = self.view.bounds;
    giftView.receiverUserId = self.liveInfo.userId;
    giftView.liveId = self.liveInfo.liveId;
    giftView.delegate = self;
    [self.view addSubview:giftView];
}

- (void)showToUserDetail:(NSNotification *)noti {
    NSString * userId = noti.userInfo[@"userId"];
    [self pushToDetailTableViewController:self userId:userId];
}

- (void)handleQuickMessageNotification:(NSNotification *)notify {
    NSString * message = notify.userInfo[@"message"];
    [self onTouchSendMessageButton:message enableGlobalMessage:NO];
}

- (void)handleUnreadMessageView:(UITapGestureRecognizer *)tap {
    [self scrollToBottomAnimated:YES];
}

- (void)linkMicSwitchDidChange:(NSNotification *)notify {
    self.bigMicCollectionView.hidden = !ZegoManager.linkMicSwitch;
    self.openChat = ZegoManager.linkMicSwitch;
}

- (void)linkMicMineMicStatusChanged:(NSNotification *)notify {
    [self setupMicInfo];
}

// 处理ui 逻辑 声音 
- (void)handlePKWithUserId:(NSString *)userId Status:(BOOL)isPK {
    if (isPK) {
        [ZegoManager.api startPlayStream:[NSString stringWithFormat:@"s-%@-Famy",userId]];
    }else{
        [ZegoManager.api stopPlayStream:[NSString stringWithFormat:@"s-%@-Famy",userId]];
    }
    [self resetMessageCollectionViewFrame];
}

#pragma mark - Reuqeust

- (void)requestUserInfo {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"my.info.detail" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        self.currentUserModel = [HomeModel initWithDictionary:suceess];
        self.currentUserModel.level = suceess[@"level"][@"user_level"];
    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)uploadImage:(UIImage *)image {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary * dict = [NSMutableDictionary new];
    [dict setValue:self.liveInfo.liveId forKey:@"liveId"];
    [RequestUtils PostMultipartFormDataUtils:dict bURL:URL_NewServer(@"sys/upload.file") constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSString *timeStampString = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]];
        [formData appendPartWithFileData:UIImageJPEGRepresentation(image, 0.5) name:@"file" fileName:[NSString stringWithFormat:@"%@.jpg", timeStampString] mimeType:@"image/jpg"];
    } success:^(id  _Nullable suceess) {
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        if (suceess[@"url"]) {
            LiveImageMessage * message = [[LiveImageMessage alloc]init];
             message.imageUrl = [NSString stringWithFormat:STATIC_DOMAIN_URL,suceess[@"url"]];
             message.senderUserInfo = [[RCIM sharedRCIM] currentUserInfo];
             message.age = [self.currentUserModel.age stringValue];
             message.gender = self.currentUserModel.gender;
             message.userLevel = [self.currentUserModel.level stringValue]?:@"";
             message.medal = self.currentUserModel.medal;
             message.specialNum = self.currentUserModel.specialNum;
            message.portrait = self.currentUserModel.portrait;
            
             message.isVip = self.currentUserModel.is_vip? @"true":@"false";
            message.noble = self.currentUserModel.noble;
            if (self.liveInfo.wealthLevel.notEmpty && ![self.liveInfo.wealthLevel isEqualToString:@"0"]) {
                message.wealthLevel = self.liveInfo.wealthLevel;
            }
            message.fansName = self.liveInfo.fansName;
            message.fansLevel = [NSString stringWithFormat:@"%@",@(self.liveInfo.fansLevel)];
            
            message.portrait = self.currentUserModel.portrait;
            message.customTag = self.currentUserModel.customTag;
            message.fansTag = self.currentUserModel.fansTag;
            message.medalUrl = self.currentUserModel.medal;
            message.userIdentity = LY_MessageSenderUserIdentityAnchor;
            [self sendMessage:message];
        }
        
    } failure:^(id  _Nullable failure) {
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [self showTips:failure[@"msg"]];
    }];
}

// insertTip 是否插入 系统提示
- (void)requestLiveInfoInsertTip:(BOOL)insertTip {
    if (insertTip) {
        self.enteringView.showFailed = NO;
    }
    [RequestUtils GET:URL_NewServer(@"live/live.info") parameters:@{@"liveId":self.liveInfo.liveId} success:^(id  _Nullable suceess) {
        if (!suceess) {
            self.enteringView.showFailed = YES;
            return;
        }
        
        
        
        [self.liveInfo setValuesForKeysWithDictionary:suceess];
        
        // 权重
        if ([[SPDApiUser currentUser].userId isEqualToString:self.liveInfo.userId]) {
            self.postionWeight = PositionWeightAnchor;
        }else{
            self.postionWeight = [suceess[@"isAdmin"] boolValue] ? PositionWeightManager : PositionWeightAudience;
        }
        self.barrageTopBarView.liveModel = self.liveInfo;
        
        // 插入系统提示
        if (suceess[@"systemTip"] && insertTip) {
            LiveRoomSystemMessage * sysMessage = [[LiveRoomSystemMessage alloc]init];
            sysMessage.content = suceess[@"systemTip"];
            [self insertMessage:sysMessage scrollToBottom:YES];
        }
        [self.titleView bindOnlineNum:suceess[@"onlineNum"]?:@"1" joinNum:suceess[@"joinNum"]?:@"1"];

        self.titleView.liveModel = self.liveInfo;
        if (self.liveInfo.liveBackground.notEmpty) {
            [self.backgroundImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:STATIC_DOMAIN_URL,self.liveInfo.liveBackground]] placeholderImage:self.backgroundImageView.image];
        }
        
        if (!self.isAnchor) {
            if (self.liveInfo.isBroadcast) {
                self.anchorOfflineView.hidden = YES;
            } else {
                self.anchorOfflineView.hidden = self.liveInfo.openChat ? YES : NO;
                self.anchorOfflineView.liveInfo = self.liveInfo;
                [self requestRecommendAnchor];
                [ZegoManager stopLinkMic];
            }
        }
        
        // 判断是否显示粉丝团
        if (self.isAnchor || self.liveInfo.haveFansGroup) {
            self.fansView.text = [NSString stringWithFormat:@"粉丝团 %ld人".localized, self.liveInfo.fansNum];
            if (self.isAnchor && !self.liveInfo.haveFansGroup) {
                self.fansView.text = @"创建粉丝团".localized;
            }
            
            [self.view insertSubview:self.fansView belowSubview:self.worldMsgView];
            [self.fansView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.leading.mas_equalTo(15);
                make.height.mas_equalTo(25);
                make.centerY.equalTo(self.heartbeatView);
            }];
            
            [self.heartbeatView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.leading.mas_equalTo(self.fansView.mas_trailing).offset(10);
                make.height.mas_equalTo(25);
                make.top.equalTo(self.titleView.mas_bottom).offset(10);
            }];
            
            BOOL isShowFansGroupTipsView = [[NSUserDefaults standardUserDefaults] boolForKey:@"isShowFansGroupTipsView"];
            if (!isShowFansGroupTipsView) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    JXPopoverView *popoverView = [JXPopoverView popoverView];
                    JXPopoverAction *action1 = [JXPopoverAction actionWithTitle:@"成为真爱粉，享受特权".localized handler:^(JXPopoverAction *action) {

                    }];
                    [popoverView showToView:self.fansView withActions:@[action1]];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [popoverView hide];
                    });
                });
                [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isShowFansGroupTipsView"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
        
        if (self.liveInfo.isFans) {
            [self requestBarrageBalance];
        }
        [self.enteringView removeFromSuperview];

        
        [self.heartbeatView setInitialValue:self.liveInfo.heartRange];
        self.anchorBottomView.hidden = !self.isAnchor;
        self.audienceBottomView.hidden = self.isAnchor;
        self.giftRecordButton.hidden = !self.isAnchor;
        
        // 只在页面初始化的时候请求一次
        if (insertTip) {
            self.pkStatus = LiveRoomAnchorPKDefalut;
            [self requestPKLiveInfo]; // pk状态查询
        }
        // 直播间红包
        if (suceess[@"redPackage"]) {
            self.redPackIconView.hidden = NO;
            self.redPackIconView.model =  [LiveRedPackModel initWithDictionary:suceess[@"redPackage"]];
        }

    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
        self.enteringView.showFailed = YES;
    }];
}

- (void)setPkStatus:(LiveRoomAnchorPKStatus)pkStatus {
    _pkStatus = pkStatus;
    self.anchorBottomView.pkStatus = _pkStatus;
    
    if (self.openChat && ZegoManager.linkMicUserInfoDic.allValues.count == 0) {
        self.openChat = NO; // 没有人连麦的状态下可以进行pk
    }else{
        [self resetMessageCollectionViewFrame];
    }
}

- (void)requestRecommendAnchor {
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"live.offline.recommend" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id  _Nullable suceess) {
        NSArray * array = suceess[@"list"];
        NSMutableArray * data = [NSMutableArray new];
        for (NSDictionary * dict in array) {
            LiveModel * model = [LiveModel initWithDictionary:dict];
            [data addObject:model];
        }
        //有推荐
        LiveRecommendAnchorController * vc = [LiveRecommendAnchorController new];
        vc.dataArray = data;
        vc.liveInfo = self.liveInfo;
        vc.delegate = self;
        LY_HalfNavigationController * nav = [[LY_HalfNavigationController alloc]initWithRootViewController:vc controllerHeight:array.count> 0 ? 520: 200];
        [self presentViewController:nav animated:YES completion:nil];
        
    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)requestBanner {
    NSDictionary *param = @{@"name": @"iOS", @"type": @"studio"};
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary dictionaryWithDictionary:param] bURL:@"banner" bAnimated:NO bAnimatedViewController:self bSuccessDoSomething:^(id _Nullable suceess) {
        [self.clanActivityArray removeAllObjects];
        
        NSMutableArray *imageUrlArray = [NSMutableArray array];
        NSArray *bannerArray = suceess[@"banner"];
        for (NSDictionary *dic in bannerArray) {
            BannerModel *model = [BannerModel initWithDictionary:dic];
            double begin = [model.begin_time doubleValue] / 1000;
            double end = [model.end_time doubleValue] / 1000;
            NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
            if (currentTime >= begin && currentTime <= end) {
                NSString *imgaeUrl = [NSString stringWithFormat:STATIC_DOMAIN_URL, model.image_url];
                [imageUrlArray addObject:imgaeUrl];
                [self.clanActivityArray addObject:model];
            }
        }
        self.clanActivityScrollView.imageURLStringsGroup = imageUrlArray;
    } bFailure:^(id  _Nullable failure) {
        
    }];
}

- (void)requestQuickMessage {
    [RequestUtils GET:URL_NewServer(@"live/live.message.rec") parameters:@{} success:^(id  _Nullable suceess) {
        NSArray * messages = suceess[@"messages"];
        self.quickMessageBar.dataArray = [messages mutableCopy];
        self.inputBar.quickMessageArray = messages;
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)requestPKLiveInfo {
    [RequestUtils GET:URL_NewServer(@"live/live.pk.info") parameters:@{@"liveId":self.liveInfo.liveId?:@""} success:^(id  _Nullable suceess) {
        NSDictionary * dict = suceess;
        if (dict.allValues.count > 0) {
            
            self.pkStatus = LiveRoomAnchorPKInProcessStatus;
            self.openChat = NO;
            
            LivePKBeginMessage * beginMsg = [[LivePKBeginMessage alloc]init];
            beginMsg.launchUserId = dict[@"opponentUserId"];
            beginMsg.acceptUserId = self.liveInfo.userId;
            beginMsg.launchLiveId = dict[@"opponentLiveId"];
            beginMsg.acceptLiveId = self.liveInfo.liveId;
            [self.beginPKAnimationView configMessage:beginMsg andMineAnchorUserId:self.liveInfo.userId];
            [self.beginPKAnimationView startAnimation];
            
            _scoreMsg = [LivePKScoreMessage new];
            _scoreMsg.opponentSupportIds = dict[@"opponentSupportIds"];
            _scoreMsg.opponentLiveId = dict[@"opponentLiveId"];
            _scoreMsg.opponentScore = [dict[@"opponentScore"] integerValue];
            _scoreMsg.myLiveId = self.liveInfo.liveId;
            _scoreMsg.mySupportIds = dict[@"mySupportIds"];
            _scoreMsg.myScore = [dict[@"myScore"] integerValue];
            
            [self handlePKWithUserId:[beginMsg.acceptUserId isEqualToString:self.liveInfo.userId]? beginMsg.launchUserId:beginMsg.acceptUserId Status:YES];
            
        }else{
            // 9 麦模式
            self.openChat = self.liveInfo.openChat;
            ZegoManager.linkMicSwitch = self.liveInfo.openChat;
            ZegoManager.linkMicFreeUpMicSwitch = self.liveInfo.openSwitch;
        }
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)requestSendBarrgeMessage:(NSString *)message {
    NSMutableDictionary * dict = [NSMutableDictionary new];
    [dict setValue:self.liveInfo.liveId forKey:@"liveId"];
    [dict setValue:self.liveInfo.userId forKey:@"anchorId"];
    [dict setValue:@(self.barrageType) forKey:@"type"];
    [dict setValue:message forKey:@"msg"];
    [RequestUtils  POST:URL_NewServer(@"/live/live.bullet.chat.send") parameters:dict success:^(id  _Nullable suceess) {
        if (self.liveInfo.isFans) {
            [self requestBarrageBalance];
        }
        if (self.liveInfo.canFinishTask) {
            NSDictionary *params = @{@"liveId": self.liveInfo.liveId,
                                     @"liveUserId": self.liveInfo.userId,
                                     @"code": @"liveSendMsg"
            };
            [LY_Network postRequestWithURL:LY_Api.live_fansGroup_finishTask parameters:params success:^(id  _Nullable response) {
            } failture:^(NSError * _Nullable error) {
            }];
        }
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)requestBarrageBalance {
    [RequestUtils GET:URL_NewServer(@"/live/live.bullet.balance") parameters:@{@"liveId":self.liveInfo.liveId,@"anchorId":self.liveInfo.userId} success:^(id  _Nullable suceess) {
        self.barrageBalanceString = [NSString stringWithFormat:@"粉丝团每周免费10条（剩余%@），超出100贝壳/条".localized,suceess[@"number"]];
        if (self.barrageTopBarView.currentBarrageType == BarrageType_Fans) {
            self.inputBar.chatSessionInputBarControl.inputTextView.placeholder = self.barrageBalanceString;
        }
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)requestPKInviteDeal:(NSDictionary *)params {
    [RequestUtils POST:URL_NewServer(@"live/pk.invite.deal") parameters:params success:^(id  _Nullable suceess) {
        NSLog(@"suceesssuceesssuceesssuceess");
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)setupMicInfo {
    [RequestUtils GET:URL_NewServer(@"live/live.chat.info") parameters:@{@"broadcaster":self.liveInfo.userId} success:^(id  _Nullable suceess) {
        [self.linkMicUserInfos removeAllObjects];
        [ZegoManager.linkMicUserInfoDic removeAllObjects];
        
        for (NSDictionary * dic in suceess[@"infos"]) {
            LinkMicNewModel * model = [LinkMicNewModel initWithDictionary:dic];
            [self.linkMicUserInfos setValue:model forKey:model.site];
            [ZegoManager.linkMicUserInfoDic setValue:model forKey:model.userId];
        }
        
        if (self.linkMicUserInfos.allKeys.count >0) {
            self.pkStatus = LiveRoomAnchorPKInProcessUnable;
        }
        [self.bigMicCollectionView reloadData];
        
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

#pragma mark - rocket

- (void)requestRocketSwitch {
    [RequestUtils GET:URL_Server(@"rocket.switch") parameters:@{@"type":@"conch"} success:^(id  _Nullable suceess) {
        NSLog(@"rocket.switch  在执行 %@",suceess);
        if ([suceess[@"isOpen"] isEqualToString:@"true"]) {
            self.rocketWaveView.hidden = false;
            CGFloat progress = ([suceess[@"num"] floatValue] * 1.0f)/[suceess[@"defaultNum"] floatValue];
            self.rocketWaveView.progress = progress;
            if (!_rocketTimer) {
                _rocketTimer = [NSTimer timerWithTimeInterval:5 target:self selector:@selector(requestRocketSwitch) userInfo:nil repeats:YES];
                [[NSRunLoop mainRunLoop] addTimer:self.rocketTimer forMode:NSRunLoopCommonModes];
                [_rocketTimer fire];
            }
        }else{
            self.rocketWaveView.hidden = true;
            if (self.rocketTimer) {
                [self rocketTimerInvalidate];
            }
        }
    } failure:^(id  _Nullable failure) {
        [self rocketTimerInvalidate];
    }];
}

- (void)rocketTimerInvalidate {
    [self.rocketTimer invalidate];
    self.rocketTimer = nil;
}

#pragma mark - Response Methods

- (void)handleExitButton:(UIButton *)sender {
    if (self.isAnchor && self.pkStatus == LiveRoomAnchorPKInProcessStatus) {
        [self showTips:@"PK中无法退出".localized];
        return;
    }
    LYPSTextAlertView *view = [[LYPSTextAlertView alloc] init];
    view.title = SPDLocalizedString(@"提示");
    if (self.isAnchor) {
        view.message = SPDLocalizedString(@"还有很多观众正在赶来");
        [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"再等等") style:LYPSystemAlertActionStyleCancel handler:^(LYPSystemAlertAction * _Nonnull action) {
            
        }]];
        [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"现在下播") style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
            LiveEndedAnchorView *view = [[NSBundle mainBundle] loadNibNamed:@"LiveEndedAnchorView" owner:self options:nil].firstObject;
            view.frame = self.view.bounds;
            view.delegate = self;
            [self.view addSubview:view];
        
            [ZegoManager leaveRoom];
            [self.durationTimer invalidate];
            self.durationTimer = nil;
        }]];
    } else {
        view.message = SPDLocalizedString(@"即将离开房间，是否确认退出？");
        [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"退出") style:LYPSystemAlertActionStyleCancel handler:^(LYPSystemAlertAction * _Nonnull action) {
            [self.inputBar setInputBarStatus:RCDLiveBottomBarKeyboardStatus];
            [[LiveFreeGiftTimerManager sharedInstance] stop];
            [self exit];
        }]];
        [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"最小化") style:LYPSystemAlertActionStyleDestructive handler:^(LYPSystemAlertAction * _Nonnull action) {
            [self keep];
        }]];
    }
    [view present];
}

- (void)clickGiftRecordButton:(UIButton *)sender {
    self.giftRecordButton.badgeValue = 0;

    LiveRoomGiftRecordView *view = [[NSBundle mainBundle] loadNibNamed:@"LiveRoomGiftRecordView" owner:self options:nil].firstObject;
    view.frame = self.view.bounds;
    [self.view addSubview:view];
    [view showWithLiveId:self.liveInfo.liveId];
}

- (void)clickRefreshButton:(UIButton *)sender {
    [self requestLiveInfoInsertTip:YES];
}

#pragma mark - Room logic

- (void)loginRoom {
    self.titleView.showConnecting = YES;
    
    ZegoManager.popIndex = self.navigationController.viewControllers.count;
    ZegoManager.sceneType = SceneTypeLiveRoom;
    ZegoManager.isBackground = NO;
    ZegoManager.clan_id = self.liveInfo.liveId;
    ZegoManager.delegate = self;
    [ZegoManager.api setUserStateUpdate:NO];
    [ZegoManager.api setAudioRoomDelegate:ZegoManager];
    [ZegoManager.api setAudioPublisherDelegate:ZegoManager];
    [ZegoAudioRoomApi setUserID:[SPDApiUser currentUser].userId userName:[SPDApiUser currentUser].nickName];
    [ZegoManager.api setManualPlay:YES]; // 直播的时候设置手动播放
    [ZegoManager.api loginRoom:self.liveInfo.liveId completionBlock:^(int errorCode) {
        if (errorCode == 0) {
            if (self.isAnchor) {
                [ZegoManager.api enableMic:YES];
//                [ZegoManager.api startPublish];
                [ZegoManager.api startPublishWithStreamID:[NSString stringWithFormat:@"s-%@-Famy",[SPDApiUser currentUser].userId]]; // 为了pk 可以拿到对方主播的流
                self.durationTimer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(durationTiming) userInfo:nil repeats:YES];
                [[NSRunLoop currentRunLoop] addTimer:self.durationTimer forMode:NSRunLoopCommonModes];
                [self.durationTimer fire];
            } else {
                [ZegoManager.api enableMic:NO];
            }
            self.titleView.showConnecting = NO;
            [self startNetworkMonitoring];
        } else {
            [self showToast:SPDStringWithKey(@"加入聊天室失败，请稍后重试", nil)];
            [self exit];
        }
    }];
}

- (void)keep {
    [self back];
    [self releaseAllTimer];
    ZegoManager.isBackground = YES;
}


- (void)exit {
    [self back];
    [self clearData];
}

- (void)clearData {
    [self releaseAllTimer];
    [ZegoManager leaveRoom];
}

- (void)releaseAllTimer {
    [self.fansTaskTimer invalidate];
    self.fansTaskTimer = nil;
    [self releaseTimer];
    [self rocketTimerInvalidate];
}

- (void)back {
    if (ZegoManager.popIndex > 0) {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        [self.navigationController popToViewController:self.navigationController.viewControllers[ZegoManager.popIndex - 2] animated:NO];
        ZegoManager.popIndex = 0;
    }
}

- (void)durationTiming {
    self.titleView.timerStr = [NSString stringWithFormat:@"%02ld:%02ld:%02ld", self.durationTime / 3600, self.durationTime / 60 % 60, self.durationTime % 60];
    self.durationTime++;
    
    if (self.durationTime % 30 == 0) {
        [RequestUtils GET:URL_NewServer(@"live/live.heartbeat") parameters:@{} success:^(id  _Nullable suceess) {
            
        } failure:^(id  _Nullable failure) {
            
        }];
    }
}

- (void)showUserInfoViewWithUserId:(NSString *)userId {
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    LiveRoomUserInfoView *view = [[NSBundle mainBundle] loadNibNamed:@"LiveRoomUserInfoView" owner:self options:nil].firstObject;
    view.frame = self.view.bounds;
    view.currentPositionWeight = self.postionWeight;
    view.delegate = self;
    [self.view addSubview:view];
    [view showWithUserId:userId liveInfo:self.liveInfo isAnchor:self.isAnchor];
}

- (void)requestLiveRemoveWithUser:(LiveUserModel *)userInfo {
    NSDictionary *params = @{@"liveId": self.liveInfo.liveId, @"userId": userInfo.userId};
    [RequestUtils POST:URL_NewServer(@"live/live.remove") parameters:params success:^(id  _Nullable suceess) {
        [self showTips:[NSString stringWithFormat:SPDLocalizedString(@"已将%@踢出房间"), userInfo.name]];
    } failure:^(id  _Nullable failure) {

    }];
}

- (void)requestLiveRemoveForbiddenWithUser:(LiveUserModel *)userInfo {
    NSDictionary *params = @{@"liveId": self.liveInfo.liveId, @"userId": userInfo.userId};
    [RequestUtils POST:URL_NewServer(@"live/live.remove.forbidden") parameters:params success:^(id  _Nullable suceess) {

    } failure:^(id  _Nullable failure) {

    }];
}

- (void)requestChangeAdmin:(LiveUserModel *)model isSetAdmin:(BOOL) isSetAdmin {
    [RequestUtils POST:URL_NewServer(isSetAdmin? @"live/set.admin":@"live/remove.admin") parameters:@{@"lUserId":model.userId} success:^(id  _Nullable suceess) {
        
        [self showTips:@"操作成功".localized];
        LiveChangeManagerMessage * msg = [LiveChangeManagerMessage new];
        msg.type = isSetAdmin ? @"set":@"remove";
        msg.userId = model.userId;
        [self sendMessage:msg];
        if (isSetAdmin) {
            [self onTouchSendMessageButton:[NSString stringWithFormat:@"%@成了管理员".localized,model.name] enableGlobalMessage:NO];
        }
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)requestFollowWithType:(NSString *)type {
    BOOL isFollow = [type isEqualToString:@"follow"];
    [self.titleView followStatusChangedTo:isFollow];

    NSString *url = [NSString stringWithFormat:@"live/live.%@", type];
    NSDictionary *params = @{@"userId": self.liveInfo.userId};
    [RequestUtils POST:URL_NewServer(url) parameters:params success:^(id  _Nullable suceess) {
        [self showTips:SPDLocalizedString(isFollow ? @"关注成功" : @"取消关注")];
        self.liveInfo.isFollow = isFollow;
        if (isFollow && !self.isShowedInviteMessage && !self.liveInfo.isFans && self.liveInfo.haveFansGroup) {
            [self sendFansInviteMessage];
        }
    } failure:^(id  _Nullable failure) {
        [self.titleView followStatusChangedTo:!isFollow];
    }];
}

- (void)sendFansInviteMessage {
    NSDictionary *params = @{@"user_id":self.liveInfo.userId};
    [LY_Network getRequestWithURL:LY_Api.user_detail parameters:params success:^(id  _Nullable response) {
        LY_HomePageInfo *userInfo = [LY_HomePageInfo yy_modelWithDictionary:response];

        self.isShowedInviteMessage = true;
        LY_LiveFansInviteMessage *fansInviteMessage = [[LY_LiveFansInviteMessage alloc] init];
        fansInviteMessage.content = @"邀请你加入粉丝团享受特权".localized;
        fansInviteMessage.type = @"invite";
        fansInviteMessage.userName = [SPDApiUser currentUser].nickName;
        fansInviteMessage.gender = userInfo.gender;
        fansInviteMessage.age = userInfo.age;
        fansInviteMessage.userLevel = [NSString stringWithFormat:@"%d", userInfo.level];
        fansInviteMessage.medalUrl = userInfo.medal;
        fansInviteMessage.noble = userInfo.noble;
        fansInviteMessage.userIdentity = LY_MessageSenderUserIdentityAnchor;
        fansInviteMessage.isVip = userInfo.isVip ? @"true" : @"false";
        fansInviteMessage.specialNum = userInfo.specialNum;
        fansInviteMessage.wealthLevel = userInfo.wealthLevel;
        fansInviteMessage.portrait = userInfo.portrait;
        fansInviteMessage.customTag = userInfo.customTag;
        fansInviteMessage.senderUserInfo = [[RCUserInfo alloc] initWithUserId:self.liveInfo.userId name:self.liveInfo.userName portrait:self.liveInfo.avatar];
        [self insertMessage:fansInviteMessage scrollToBottom:YES];
    } failture:^(NSError * _Nullable error) {
    }];
}

- (void)popupFansView {
    
    UIViewController *vc;
    CGFloat controllerHeight = 0;
    // 判断是否是主播
    if (self.isAnchor) {
        // 判断是否拥有粉丝团
        if (self.liveInfo.haveFansGroup) {
            // 有粉丝团，直接显示粉丝列表
            LY_FansListViewController *fansListVC = [[LY_FansListViewController alloc] init];
            fansListVC.anchorUserId = self.liveInfo.userId;
            fansListVC.isAnchorStyle = self.isAnchor;
            fansListVC.portraitUrl = self.liveInfo.avatar;
            fansListVC.nickname = self.liveInfo.userName;
            vc = fansListVC;
            controllerHeight = 460;
        } else {
            // 没有粉丝团，去创建
            LY_FunsGroupCreatViewController *fansGroupCreatVC = [[LY_FunsGroupCreatViewController alloc] init];
            fansGroupCreatVC.delegate = self;
            vc = fansGroupCreatVC;
            controllerHeight = 430;
        }
    } else {
        // 判断是否已经加入粉丝团
        // 用户查看粉丝团
        LY_FanGroupViewController *fansGroupVC = [[LY_FanGroupViewController alloc] initWithLiveId:self.liveInfo.liveId anchorUserId:self.liveInfo.userId];
        fansGroupVC.delegate = self;
        vc = fansGroupVC;
        controllerHeight = 420;
        if (self.liveInfo.isFans && self.liveInfo.isLock) {
            // 已经加入，但已冻结
            fansGroupVC.status = LY_FansGroupStatusInvalid;
        } else if (self.liveInfo.isFans && !self.liveInfo.isLock) {
            // 已经加入，去做任务
            fansGroupVC.status = LY_FansGroupStatusJoined;
        } else {
            // 未加入，去加入
            fansGroupVC.status = LY_FansGroupStatusNotJoined;
        }
    }
    
    LY_HalfNavigationController *nav = [[LY_HalfNavigationController alloc] initWithRootViewController:vc controllerHeight:controllerHeight];
    [self presentViewController:nav animated:true completion:nil];
}

- (void)heartbeatViewAction {
    JXPopoverView *popoverView = [JXPopoverView popoverView];
    JXPopoverAction *action1 = [JXPopoverAction actionWithTitle:@"赠送给主播礼物将会为主播增加心动值".localized handler:^(JXPopoverAction *action) {

    }];
    [popoverView showToView:self.heartbeatView withActions:@[action1]];
}

- (void)handlePKScoreAction:(UITapGestureRecognizer *)tap {
    LY_HalfNavigationController * nav = [[LY_HalfNavigationController alloc]initWithRootViewController:[PKScoreViewController new] controllerHeight:275+IphoneX_Bottom];
    [self presentViewController:nav animated:YES completion:^{
        
    }];
}

- (void)fansTaskTimerAction {
    self.taskSecond += 1;
    
    if (self.taskSecond == 300 || self.taskSecond == 600 || self.taskSecond == 900 || self.taskSecond == 1200) {
        if (self.liveInfo.canFinishTask) {
            NSDictionary *params = @{@"liveId": self.liveInfo.liveId,
                                     @"liveUserId": self.liveInfo.userId,
                                     @"code": @"liveListen"
            };
            [LY_Network postRequestWithURL:LY_Api.live_fansGroup_finishTask parameters:params success:^(id  _Nullable response) {
            } failture:^(NSError * _Nullable error) {
            }];
        }
    }
    if (self.taskSecond == 1200) {
        NSDictionary *params = @{@"liveId": self.liveInfo.liveId};
        [LY_Network postRequestWithURL:LY_Api.live_fansGroup_unlock parameters:params success:^(id  _Nullable response) {
        } failture:^(NSError * _Nullable error) {
        }];
    }
    if (self.taskSecond >= 120 && !self.isShowedInviteMessage && !self.liveInfo.isFans) {
        self.isShowedInviteMessage = true;
        if (!self.isShowedInviteMessage && !self.liveInfo.isFans && self.liveInfo.haveFansGroup) {
            [self sendFansInviteMessage];
        }
    }
}

#pragma mark - ZegoKitManagerDelegate

- (void)liveDidStart {
    [self requestLiveInfoInsertTip:NO];
}

- (void)didLeaveRoom {
    [self back];
}

#pragma mark - LY_LiveAudienceHalfVCDelegate

- (void)liveAudienceHalfVCDidClick:(UIButton *)giftingBtn {
    // 发送礼物
    if(self.isAnchor)return;
    [self popGift];
}

#pragma mark - LiveRoomUserInfoViewDelegate

- (void)userInfoView:(LiveRoomUserInfoView *)view didClickUserAvatar:(LiveUserModel *)userInfo {
    [self pushToDetailTableViewController:self userId:userInfo.userId];
}

- (void)userInfoView:(LiveRoomUserInfoView *)view didSelectAction:(NSString *)action toUser:(LiveUserModel *)userInfo {
    if ([action isEqualToString:@"chat"]) {
        [self pushToConversationViewControllerWithTargetId:userInfo.userId];
    } else if ([action isEqualToString:@"forbidden"]) {
        LY_LiveNoSpeakAlertView *view = [LY_LiveNoSpeakAlertView new];
        view.title = [NSString stringWithFormat:SPDLocalizedString(@"将“%@”禁言"), userInfo.name];
        view.delegate = self;
        view.userInfo = userInfo;
        [view addAction:[LYPSystemAlertAction actionWithTitle:@"取消".localized style:LYPSystemAlertActionStyleCancel handler:^(LYPSystemAlertAction * _Nonnull action) {

        }]];
        [view addAction:[LYPSystemAlertAction actionWithTitle:@"确定".localized style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {

        }]];
        [view present];
    } else if ([action isEqualToString:@"release"]) {
        [self requestLiveRemoveForbiddenWithUser:userInfo];
    } else if ([action isEqualToString:@"kickout"]) {
        LYPSTextAlertView *view = [LYPSTextAlertView new];
        view.title = [NSString stringWithFormat:SPDLocalizedString(@"将“%@”踢出房间"), userInfo.name];
        view.message = SPDLocalizedString(@"踢出后，对方将在24小时之内不能进入你的直播间");
        [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"取消") style:LYPSystemAlertActionStyleCancel handler:^(LYPSystemAlertAction * _Nonnull action) {
            
        }]];
        [view addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"确定") style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
            [self requestLiveRemoveWithUser:userInfo];
        }]];
        [view present];
    } else if ([action isEqualToString:@"contribution"]) {
        LY_LiveAudienceHalfVC *vc = [[LY_LiveAudienceHalfVC alloc] initWithControllerHeight:500];
        vc.liveId = self.liveInfo.liveId;
        vc.giftingBtnDelegate = self;
        vc.selectIndex = 1;
        [self presentViewController:vc animated:true completion:nil];
    } else if ([action isEqualToString:@"report"]) {
        ReportViewController *vc = [ReportViewController new];
        vc.userId = userInfo.userId;
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([action isEqualToString:@"fans"]) {
        [self popupFansView];
    }else if ([action isEqualToString:@"gift"]){
        LiveRoomGiftView * giftView = [[[NSBundle mainBundle]loadNibNamed:@"LiveRoomGiftView" owner:self options:nil]firstObject];
        giftView.frame = self.view.bounds;
        giftView.receiverUserId = userInfo.userId;
        giftView.liveId = self.liveInfo.liveId;
        giftView.delegate = self;
        [self.view addSubview:giftView];
    }else if([action isEqualToString:@"setAdmin"]){
        [self requestChangeAdmin:userInfo isSetAdmin:YES];
    }else if([action isEqualToString:@"removeAdmin"]){
        [self requestChangeAdmin:userInfo isSetAdmin:NO];
    }else {
        [self requestFollowWithType:action];
    }
}

#pragma mark - LY_LiveNoSpeakAlertViewDelegate

- (void)liveNoSpeakMinute:(int)minute toUser:(LiveUserModel *)userInfo {
    NSDictionary *params = @{@"liveId": self.liveInfo.liveId, @"userId": userInfo.userId, @"min": @(minute)};
    [RequestUtils POST:URL_NewServer(@"live/live.forbidden.words") parameters:params success:^(id  _Nullable suceess) {
        [self showTips:[NSString stringWithFormat:SPDLocalizedString(@"已将%@禁言%d分钟"), userInfo.name, minute]];
    } failure:^(id  _Nullable failure) {

    }];
}

#pragma mark - LiveRoomTitleViewDelegate

- (void)liveRoomTitleView:(LiveRoomTitleView *)titleView didClickedFollow:(UIButton *)sender {
    [self requestFollowWithType:@"follow"];
}

- (void)liveRoomTitleView:(LiveRoomTitleView *)titleView didClickedAvatar:(LiveModel *)model {
    [self showUserInfoViewWithUserId:model.userId];
}

#pragma mark - LiveEndedAnchorViewDelegate

- (void)didClickBackButtonInEndedAnchorView {
    [self back];
}

- (void)didClickWalletButtonInEndedAnchorView {
    LiveAnchorWalletController *walletVC = [[LiveAnchorWalletController alloc] init];
    walletVC.selectIndex = 0;
    [self.navigationController pushViewController:walletVC animated:true];
}

- (void)didClickRecordButtonInEndedAnchorView {
    LiveAnchorWalletController *walletVC = [[LiveAnchorWalletController alloc] init];
    walletVC.selectIndex = 1;
    [self.navigationController pushViewController:walletVC animated:true];
}

#pragma mark - LiveRoomAudioEffectViewDelegate

- (void)audioEffectViewDidHide {
    self.anchorBottomView.hidden = NO;
}

#pragma mark - Message methods

- (void)didReceiveChatroomMessage:(NSNotification *)notification {
    RCMessage *message = notification.object;
    if ([message.targetId isEqualToString:self.liveInfo.liveId]) {
        if ([message.content isKindOfClass:[LiveRoomTextMessage class]] || [message.content isKindOfClass:[LiveImageMessage class]] || [message.content isKindOfClass:[LY_LiveFansInviteMessage class]] || [message.content isKindOfClass:[LY_LiveFollowedMessage class]] || [message.content isKindOfClass:[LiveLuckyNumberMessage class]]) {
            [self insertMessage:message.content scrollToBottom:[self isAtTheBottomOfTableView]];
        }else if ([message.content isKindOfClass:[LiveUserUpdateMessage class]]){
            LiveUserUpdateMessage * updateMessage = (LiveUserUpdateMessage *)message.content;
            self.onlineNumView.message = updateMessage;
            [self.titleView bindOnlineNum:updateMessage.onlineNum joinNum:updateMessage.userNum];
        }else if ([message.content isKindOfClass:[LiveGiftMessage class]]){
            if (message.content.senderUserInfo && [[NSDate date] timeIntervalSince1970] - self.joinTimeStamp > 2) {
                [self insertMessage:message.content scrollToBottom:NO];
                LiveGiftMessage * giftMessage = (LiveGiftMessage *)message.content;
                if (giftMessage.giftAnimationUrl.notEmpty) {
                    [self.giftAnimationView setMessage:giftMessage];
                }else{
                    [self.giftComboAnimationView showAnimationWithMessage:giftMessage];
                }
                
                [self.heartbeatView addValue:giftMessage.heartRange];
            }
            self.giftRecordButton.badgeValue++;
        } else if ([message.content isKindOfClass:[LiveStateMessage class]]) {
            LiveStateMessage * msg = (LiveStateMessage *)message.content;
            [self requestLiveInfoInsertTip:NO];
            if (ZegoManager.liveInfo.openChat) {
                LinkMicUserCell * cell = (LinkMicUserCell*) [self.bigMicCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                cell.anchorStatus = msg.status == 0 ? @"0":@"1";
            }
        } else if ([message.content isKindOfClass:[LiveJoinMessage class]]) {
            LiveJoinMessage * msg = (LiveJoinMessage *)message.content;
            [self insertMessage:message.content scrollToBottom:YES];
            if (msg.noble.notEmpty) {
                self.nobleEnterView.message = msg;
            }
            if (msg.isFans && !msg.noble.notEmpty) {
                self.fansJoinMessageView.message = (LiveJoinMessage *)message.content;
            }
            if (msg.vehicleId.notEmpty) {
                self.vehicleEffectView.hidden = NO;
                self.vehicleEffectView.liveJoinMessage = msg;
            }
            
        } else if ([message.content isKindOfClass:[LY_LiveFansLevelMessage class]]) {
            LY_LiveFansLevelMessage * msg = (LY_LiveFansLevelMessage *)message.content;
            if ([msg.fansId isEqualToString:[SPDApiUser currentUser].userId]) {
                self.liveInfo.fansLevel = msg.fansLevel;
            }
        }else if ([message.content isKindOfClass:[LiveNormalBarrageMessage class]]){
            LiveNormalBarrageMessage * barrage = (LiveNormalBarrageMessage *)message.content;
            NSDictionary * dic = @{@"content":barrage.msg,@"headwearWebp":barrage.portrait.headwearUrl?:@"",@"avatar":barrage.senderUserInfo.portraitUri,@"user_id":barrage.senderUserInfo.userId,@"nick_name":barrage.senderUserInfo.name};
            [self.barrageRenderer receive:[self createBarrageDescriptorWithDic:dic delay:(3 + arc4random() % 4)]];
        } else if ([message.content isKindOfClass:[LiveFansBarrageMessage class]]){
            LiveFansBarrageMessage * barrage = (LiveFansBarrageMessage *)message.content;
            NSDictionary * dic = @{@"content":barrage.msg,@"headwearWebp":barrage.portrait.headwearUrl,@"avatar":barrage.senderUserInfo.portraitUri,@"user_id":barrage.senderUserInfo.userId,@"nick_name":barrage.senderUserInfo.name,@"isFansBarrage":@(1)};
            [self.barrageRenderer receive:[self createBarrageDescriptorWithDic:dic delay:(3 + arc4random() % 4)]];
        }else if ([message.content isKindOfClass:[LiveWealthLevelMessage class]]){
            LiveWealthLevelMessage * wealthMessage = (LiveWealthLevelMessage *)message.content;
            if ([wealthMessage.userId isEqualToString:[SPDApiUser currentUser].userId]) {
                self.liveInfo.wealthLevel = wealthMessage.userWealthLevel;
            }
            [self insertMessage:message.content scrollToBottom:NO];
            // 展示消息流
        }else if ([message.content isKindOfClass:[LivePKInviteMessage class]]){
            if ([[NSDate date] timeIntervalSince1970] - self.joinTimeStamp > 2.0) {
                if (self.openChat && ZegoManager.linkMicUserInfoDic.allValues.count > 0) {
                    return;
                }// 有人连麦 是不能pk的
                LivePKInviteMessage * inviteMsg = (LivePKInviteMessage *)message.content;
                if (self.isAnchor) {
                    // 弹出弹框
                    if ([ZegoKitManager sharedInstance].linkMicLinkingDic.count > 0) {
                        [self requestPKInviteDeal:@{@"pkId":@(inviteMsg.pkId),@"noDisturb":@(0),@"type":@(0),@"inChat":@(1)}];
                    }else{
                        self.pkInviteView = [[PKReceiveInviteMessageView alloc]init];
                        self.pkInviteView.inviteMsg = inviteMsg;
                        [self.pkInviteView present];
                    }
                    
                    if ([[SPDCommonTool getWindowTopViewController] isKindOfClass:[PKInviteViewController class]] && self.pkInviteController) {
                        [self.pkInviteController dismissViewControllerAnimated:YES completion:nil];
                    }
                }
            }
           
        }else if ([message.content isKindOfClass:[LivePKInviteRefuseMessage class]]){
            if ([[NSDate date] timeIntervalSince1970] - self.joinTimeStamp > 2.0) {
                LivePKInviteRefuseMessage * msg = (LivePKInviteRefuseMessage *)message.content;
                if (!self.isAnchor) {
                    return;
                }
                if (msg.inChat == 1) {
                    [self showTips:@"对方正在连麦".localized];
                }else{
                    [self showTips:@"对方拒绝PK".localized];
                }
                self.pkStatus = LiveRoomAnchorPKDefalut;
            }
        }else if ([message.content isKindOfClass:[LivePKInviteBusyMessage class]]){
            if (self.isAnchor) {
                [self showTips:@"对方正忙，请稍后再试".localized];
            }
        }else if ([message.content isKindOfClass:[LivePKBeginMessage class]]){
            if ([[NSDate date] timeIntervalSince1970] - self.joinTimeStamp > 2.0) {
                if (self.isAnchor) {
                    self.pkStatus = LiveRoomAnchorPKInProcessStatus;
                }else{
                    [self resetMessageCollectionViewFrame];
                }
                LivePKBeginMessage * beginMsg = (LivePKBeginMessage *)message.content;
                [self.beginPKAnimationView configMessage:beginMsg andMineAnchorUserId:self.liveInfo.userId];
                [self.beginPKAnimationView startAnimation];
                [self handlePKWithUserId:[beginMsg.acceptUserId isEqualToString:self.liveInfo.userId]? beginMsg.launchUserId:beginMsg.acceptUserId Status:YES];
            }
        }else if ([message.content isKindOfClass:[LivePKTimeMessage class]]){
            LivePKTimeMessage * timeMsg = (LivePKTimeMessage *)message.content;
            [self.processPKAnimationView updateTimeWith:timeMsg];
        }else if ([message.content isKindOfClass:[LivePKScoreMessage class]]){
            LivePKScoreMessage * scoreMsg = (LivePKScoreMessage *)message.content;
            [self.processPKAnimationView updateScore:scoreMsg];
        }else if ([message.content isKindOfClass:[PKCheckBeheadingMsg class]]){
            PKCheckBeheadingMsg * scoreMsg = (PKCheckBeheadingMsg *)message.content;
            [self.processPKAnimationView updateBehindMsg:scoreMsg];
        }else if ([message.content isKindOfClass:[PKResultMessage class]]){
            PKResultMessage * scoreMsg = (PKResultMessage *)message.content;
            [self.processPKAnimationView updateResultMsg:scoreMsg];
        }else if ([message.content isKindOfClass:[LiveCancelInvitePk class]]){
            if ([[UIApplication sharedApplication].keyWindow.subviews containsObject:self.pkInviteView] && self.isAnchor) {
                [self showTips:@"对方已取消匹配".localized];
                [self.pkInviteView dismiss];
            }
        }else if ([message.content isKindOfClass:[LiveApplyMicMessage class]]){
            if (self.isAnchor) {
                __weak typeof(self) ws = self;
                LinkMicReceiveApplyMsgView * alert = [LinkMicReceiveApplyMsgView new];
                alert.message = (LiveApplyMicMessage *)message.content;
                [alert addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"取消") style:LYPSystemAlertActionStyleCancel handler:^(LYPSystemAlertAction * _Nonnull action) {
                    [ws handleLinkMicApply:(LiveApplyMicMessage *)message.content type:@"refuse"];

                }]];
                [alert addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"确定") style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
                    [ws handleLinkMicApply:(LiveApplyMicMessage *)message.content type:@"agree"];
                }]];
                [alert present];
            }
        }else if ([message.content isKindOfClass:[LiveLinkMicAgreeMessage class]]){
            LiveLinkMicAgreeMessage * msg = (LiveLinkMicAgreeMessage *)message.content;
            if ([msg.userId isEqualToString:[SPDApiUser currentUser].userId]) {
                if ([msg.type isEqualToString:@"refuse"]) {
                    return;
                }
                [ZegoManager requestLiveUpMicChange:@{@"broadcaster":self.liveInfo.userId,@"number":msg.number,@"type":@"0",@"liveId":self.liveInfo.liveId}];
            }
        }else if ([message.content isKindOfClass:[LiveLinkMicSilenceMsg class]]){
            if (self.pkStatus != LiveRoomAnchorPKInProcessStatus) {
                [self setupMicInfo];
            }
        }else if ([message.content isKindOfClass:[LiveChangeManagerMessage class]]){
            LiveChangeManagerMessage * msg = (LiveChangeManagerMessage *)message.content;
            if ([msg.userId isEqualToString:[SPDApiUser currentUser].userId]) {
                self.postionWeight = [msg.type isEqualToString:@"set"] ? PositionWeightManager: PositionWeightAudience;
            }
        }else if ([message.content isKindOfClass:[LiveRedPackMessage class]]){
            LiveRedPackMessage * msg = (LiveRedPackMessage *)message.content;
            if ([msg.action isEqualToString:@"finish"]) {
                self.redPackIconView.hidden = YES;
            }else{
                LiveRedPackModel * model = [LiveRedPackModel new];
                model.userName = msg.senderUserInfo.name;
                model.avatar = msg.senderUserInfo.portraitUri;
                model._id = msg._id;
                model.type = msg.type;
                model.msg = msg.msg;
                self.redPackIconView.model =  model;
                self.redPackIconView.hidden = NO;
            }
        }
    }else if ([message.targetId isEqualToString:DEV? @"liveWorldRoom_dev" : @"liveWorldRoom"]){
        if ([message.content isKindOfClass:[LiveWorldMessage class]]  && [[NSDate date] timeIntervalSince1970] - self.joinTimeStamp > 4.0) {
            LiveWorldMessage * msg = (LiveWorldMessage *)message.content;
            self.worldMsgView.liveWorldMessage = msg;
            if ([msg.worldMsgType isEqualToString:@"rocket"]) {
                if ([msg.resource isEqualToString:@"liveRoom"]) {
                    self.rocketWaveView.progress = 1.0f;
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        self.rocketWaveView.progress = 0;
                    });
                }
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    if ([[SPDCommonTool getWindowTopViewController] isKindOfClass:[LiveRoomViewController class]]) {
                        RocketBoomView * boom = [[RocketBoomView alloc]initWithFrame:self.view.bounds];
                        boom.liveWorldMsg = msg;
                        [self.view addSubview:boom];
//                        [[UIApplication sharedApplication].keyWindow addSubview:boom];
                    }
                });
            }
        }
    }
}

- (void)handleLinkMicApply:(LiveApplyMicMessage *) message type:(NSString *)type{
    NSDictionary * dict = @{@"userId":message.userId,@"position":message.position,@"type":type};
    [RequestUtils POST:URL_Server(@"live.wheat.position.agree") parameters:dict success:^(id  _Nullable suceess) {
        
    } failure:^(id  _Nullable failure) {
        
    }];
}

- (PKProcessAnimationView *)processPKAnimationView{
    if (!_processPKAnimationView) {
        _processPKAnimationView = [[PKProcessAnimationView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_heartbeatView.frame) +13 , kScreenW, 180)];
        _processPKAnimationView.delegate = self;
    }
    return _processPKAnimationView;
}

#pragma mark - PKBeginAnimationViewDelegate
- (void)pkBeginAnimationViewdidFinishPKBeginAnimation:(PKBeginAnimationView *)animationView WithMessage:(LivePKBeginMessage *)message{
    NSLog(@"pkBeginAnimationViewdidFinishPKBeginAnimation--");
    if ([self.view.subviews containsObject:self.processPKAnimationView]) {
        [self.processPKAnimationView removeFromSuperview];
    }
    self.processPKAnimationView = nil;
    [self.processPKAnimationView configMessage:message andMineAnchorUserId:self.liveInfo.userId];
    [self.view insertSubview:self.processPKAnimationView belowSubview:self.beginPKAnimationView];
    if (_scoreMsg) { // pk 中途进入进行更新分数
        [self.processPKAnimationView updateScore:_scoreMsg];
        _scoreMsg = nil;
    }
}

#pragma mark - 长按头像@

- (void)handleLongPressAvatar:(NSNotification *)noti {
    [self.inputBar.chatSessionInputBarControl.inputTextView becomeFirstResponder];
    [self.inputBar.chatSessionInputBarControl.sendMessageButton setImage:[UIImage imageNamed:@"ic_msg_send"].adaptiveRtl  forState:UIControlStateNormal];

    if (self.noticeUserInfo && self.noticeUserInfo.count) {
        self.noticeUserInfo = noti.userInfo;
        self.inputBar.chatSessionInputBarControl.inputTextView.text = [NSString stringWithFormat:@"@%@ ", self.noticeUserInfo[@"nick_name"]];
    } else {
       self.noticeUserInfo = noti.userInfo;
       self.inputBar.chatSessionInputBarControl.inputTextView.text = [self.inputBar.chatSessionInputBarControl.inputTextView.text stringByAppendingFormat:@"@%@ ", _noticeUserInfo[@"nick_name"]];
    }
}

- (void)handleLiveRoomTapAvatar:(NSNotification *)notify {
    NSString * userId = notify.userInfo[@"userId"];
    [self showUserInfoViewWithUserId:userId];
}

- (void)sendMessage:(RCMessageContent *)message {
    message.senderUserInfo = [RCIM sharedRCIM].currentUserInfo;
    [[RCIMClient sharedRCIMClient] sendMessage:ConversationType_CHATROOM targetId:self.liveInfo.liveId content:message pushContent:nil pushData:nil success:^(long messageId) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([message isKindOfClass:[LiveRoomTextMessage class]] || [message isKindOfClass:[LiveImageMessage class]] || [message isKindOfClass:[LiveLuckyNumberMessage class]]) {
                [self insertMessage:message scrollToBottom:YES];
            }
            
            if (self.liveInfo.canFinishTask) {
                NSDictionary *params = @{@"liveId": self.liveInfo.liveId,
                                         @"liveUserId": self.liveInfo.userId,
                                         @"code": @"liveSendMsg"
                };
                [LY_Network postRequestWithURL:LY_Api.live_fansGroup_finishTask parameters:params success:^(id  _Nullable response) {
                } failture:^(NSError * _Nullable error) {
                }];
            }
        });
    } error:^(RCErrorCode nErrorCode, long messageId) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (nErrorCode == 23408) {
                [self showToast:SPDStringWithKey(@"您已经被禁言了哦!", nil)];
            } else if (nErrorCode == 23406) {
                [self showToast:@"Not In Chatroom"];
            }else{
                [self showToast:[NSString stringWithFormat:@"%@",@(nErrorCode)]];
            }
        });
    }];
}

- (void)reloadLiveRoom:(NSNotification *)noti {
    NSString * liveId = noti.userInfo[@"liveId"];
    if ([noti.object isKindOfClass:[PKProcessAnimationView class]] || [self.view.subviews containsObject:self.processPKAnimationView]) {
        [self.processPKAnimationView removeFromSuperview];
        self.processPKAnimationView = nil;
    }
    [self clearData];
    [self.messageArr removeAllObjects];
    self.joinTimeStamp = [[NSDate date] timeIntervalSince1970];
    [self.onlineUserDic removeAllObjects];
    self.onlineNumber = 0;
    self.unreadNewMsgCount = 0;
    self.pkStatus = LiveRoomAnchorPKDefalut;
    [self.linkMicUserInfos removeAllObjects];
    
    LiveModel * model = [LiveModel new];
    model.liveId = liveId;
    [ZegoManager joinLiveRoomWithliveInfo:model isAnchor:NO];

    // 重新 请求接口
    self.liveInfo.liveId = liveId;
    self.barrageType = BarrageType_Default;
    self.barrageBalanceString = [NSString stringWithFormat:@"粉丝团每周免费10条（剩余%@），超出100贝壳/条".localized,@"10"];
    [self loginRoom];
    [self requestUserInfo];
    self.fansTaskTimer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(fansTaskTimerAction) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.fansTaskTimer forMode:NSRunLoopCommonModes];
    [self.fansTaskTimer fire];
    if (!self.isAnchor) {
//        [self requestSysTime];
    }
    [self requestQuickMessage];
    [self requestRocketSwitch];
    [self.messageCollectionView reloadData];
    [self requestLiveInfoInsertTip:YES];
    
}

- (void)hanleMatchTimeOut:(NSNotification *)noti {
    [RequestUtils POST:URL_NewServer(@"live/cancel.pk.random") parameters:@{} success:^(id  _Nullable suceess) {
        self.pkStatus = LiveRoomAnchorPKDefalut;
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)insertMessage:(RCMessageContent *)message scrollToBottom:(BOOL)scroll{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:([message isKindOfClass:[LiveRoomSystemMessage class]] ? 0: self.messageArr.count)  inSection:0];
    [self.messageArr insertObject:message atIndex:([message isKindOfClass:[LiveRoomSystemMessage class]] ? 0 : self.messageArr.count)];
    [self.messageCollectionView insertItemsAtIndexPaths:@[indexPath]];
    if (scroll) {
        [self.messageCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionBottom animated:YES];
    }
    if (![self isAtTheBottomOfTableView]) {
        self.unreadNewMsgCount ++ ;
        [self updateUnreadMsgCountLabel];
    }
}

- (void)scrollToBottomAnimated:(BOOL)animated {
    if ([self.messageCollectionView numberOfSections] == 0) { return;}
    NSUInteger finalRow = MAX(0, [self.messageCollectionView numberOfItemsInSection:0] - 1);
    if (0 == finalRow) { return;}
    NSIndexPath *finalIndexPath =
    [NSIndexPath indexPathForItem:finalRow inSection:0];
    [self.messageCollectionView scrollToItemAtIndexPath:finalIndexPath
                                                   atScrollPosition:UICollectionViewScrollPositionTop
                                                           animated:animated];
}


#pragma mark - CollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.bigMicCollectionView == collectionView) {
        return section == 0 ? 1: 8;
    }else{
        return self.messageArr.count;
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.bigMicCollectionView == collectionView ?  2 : 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.bigMicCollectionView) {
        LinkMicUserCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LinkMicUserCell" forIndexPath:indexPath];
        cell.isAnchor = (indexPath.section == 0);
        if (indexPath.section == 0) {
            cell.anchorStatus = self.liveInfo.isBroadcast?@"1":@"0";
            LinkMicNewModel * model = [LinkMicNewModel new];
            model.userId = self.liveInfo.userId;
            model.userName = self.liveInfo.userName;
            model.portrait = self.liveInfo.portrait;
            cell.model = model;
        }else{
            NSString * index = [NSString stringWithFormat:@"%ld",indexPath.row +1];
            cell.model = self.linkMicUserInfos[index];
        }
        return cell;
        
    }else{
        RCMessageContent *message = self.messageArr[indexPath.row];
        if ([message isKindOfClass:[LiveRoomTextMessage class]] || [message isKindOfClass:[LiveLuckyNumberMessage class]]) {
            LiveRoomTextMessageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveRoomTextMessageCell" forIndexPath:indexPath];
            if([message isKindOfClass:[LiveRoomTextMessage class]]){
                LY_LiveBaseMessageContent *msg = (LY_LiveBaseMessageContent *)message;
                if ([message.senderUserInfo.userId isEqualToString:self.liveInfo.userId]) {
                    msg.userIdentity = LY_MessageSenderUserIdentityAnchor;
                } else {
                    msg.userIdentity = LY_MessageSenderUserIdentityAudience;
                }
                cell.messageContent = msg;
            }else{
                LiveLuckyNumberMessage *msg = (LiveLuckyNumberMessage *)message;
                cell.messageContent = msg;
            }
            return cell;
        }else if ([message isKindOfClass:[LiveRoomSystemMessage class]]){
            LiveRoomSystemMessageCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveRoomSystemMessageCell" forIndexPath:indexPath];
            cell.message = (LiveRoomSystemMessage *)message;
            return cell;
        }else if ([message isKindOfClass:[LiveJoinMessage class]]){
            LiveJoinMessageCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveJoinMessageCell" forIndexPath:indexPath];
            cell.message = (LiveJoinMessage *)message;
            return cell;
        }else if ([message isKindOfClass:[LiveGiftMessage class]]){
            LiveGiftMessageCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveGiftMessageCell" forIndexPath:indexPath];
            cell.messageContent = (LiveGiftMessage *)self.messageArr[indexPath.row];
            return cell;
        }else if ([message isKindOfClass:[LiveImageMessage class]]){
            LiveRoomImageMessageCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveRoomImageMessageCell" forIndexPath:indexPath];
            cell.delegate = self;
            cell.messageContent = (LiveImageMessage *)message;
            return cell;
        }else if ([message isKindOfClass:[LY_LiveFansInviteMessage class]]) {
            LY_LiveFansInviteMessageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LY_LiveFansInviteMessageCell" forIndexPath:indexPath];
            cell.delegate = self;
            cell.messageContent = (LY_LiveFansInviteMessage *)message;
            return cell;
        } else if ([message isKindOfClass:[LY_LiveFollowedMessage class]]) {
            LY_LiveFollowedMessageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LY_LiveFollowedMessageCell" forIndexPath:indexPath];
            cell.messageContent = (LY_LiveFollowedMessage *)message;
            return cell;
        } else if ([message isKindOfClass:[LiveWealthLevelMessage class]]) {
            LiveWealthLevelMessageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveWealthLevelMessageCell" forIndexPath:indexPath];
            cell.messageContent = (LiveWealthLevelMessage *)message;
            return cell;
        } else {
            return [UICollectionViewCell new];
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.bigMicCollectionView isEqual:collectionView] && indexPath.section == 1) {
        LinkMicNewModel * model = self.linkMicUserInfos[[NSString stringWithFormat:@"%ld",indexPath.row+1]];
        if (model) {
            [self showUserInfoViewWithUserId:model.userId];
        }else{
            if ([self.liveInfo.userId isEqualToString:[SPDApiUser currentUser].userId] && !self.isAnchor) {
                [self showTips:@"不可在自己直播间进行连麦".localized];
                return;
            }
            if (self.isAnchor) {
                LY_LiveAudienceHalfVC *audienceHalfVC = [[LY_LiveAudienceHalfVC alloc] initWithControllerHeight:500];
                audienceHalfVC.liveId = self.liveInfo.liveId;
                audienceHalfVC.giftingBtnDelegate = self;
                audienceHalfVC.selectIndex = 3;
                audienceHalfVC.showInviteOnlineLinkMic = YES;
                [self presentViewController:audienceHalfVC animated:true completion:nil];
            }else{
                if (ZegoManager.linkMicUserInfoDic[[SPDApiUser currentUser].userId]) {
                    [self showTips:@"你已在麦上".localized];
                    return;
                }
                if (self.postionWeight == PositionWeightManager) {
                    [ZegoManager requestLiveUpMicChange:@{@"broadcaster":self.liveInfo.userId,@"number":@(indexPath.row+1),@"type":@"0",@"liveId":self.liveInfo.liveId}];
                    return;
                }
                [RequestUtils GET:URL_Server(@"live.wheat.position") parameters:@{@"user_id":self.liveInfo.userId} success:^(id  _Nullable suceess) {
                    if ([suceess[@"status"] boolValue]) {
                        [ZegoManager requestLiveUpMicChange:@{@"broadcaster":self.liveInfo.userId,@"number":@(indexPath.row+1),@"type":@"0",@"liveId":self.liveInfo.liveId}];
                    }else{
                        if (self.liveInfo.isBroadcast) {
                            if (allowClicked == NO) {
                                return ;
                            }
                            allowClicked = NO;
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                allowClicked = YES;
                            });
                            NSDictionary * dict = @{
                                @"liveId": self.liveInfo.userId,
                                @"position": @(indexPath.row + 1)
                            };
                            [self requestApplyMic:dict];
                            
                        }else{
                            [self showTips:@"当前不可申请连麦".localized];
                        }
                    }
                } failure:^(id  _Nullable failure) {
                    [self showTips:failure[@"msg"]];
                }];
            }
        }
    }else{
        [self showUserInfoViewWithUserId:self.liveInfo.userId];
    }
}

static bool allowClicked = YES;

- (void)requestApplyMic:(NSDictionary *)dict {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSLog(@"ssssss点击了一下 而下三峡");
    [RequestUtils POST:URL_Server(@"live.wheat.position.apply") parameters:dict success:^(id  _Nullable suceess) {
//        allowClicked = YES;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showTips:@"已向主播发送连麦申请".localized];
    } failure:^(id  _Nullable failure) {
//        allowClicked = YES;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showTips:failure[@"msg"]];
    }];
}


#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.messageCollectionView == collectionView) {
        RCMessageContent * message = self.messageArr[indexPath.row];
        NSValue * siezeValue = [message valueForKey:@"cellSize"]; // 每个消息都要定义cellsize
        return siezeValue.CGSizeValue;
    }else{
        return indexPath.section == 0 ? CGSizeMake(114.0f/375*kScreenW, 114.0f/375*kScreenW) : CGSizeMake((kScreenW - 2* 11)/4.0f, (kScreenW - 2* 11)/4.0f);
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if ([collectionView isEqual:self.messageCollectionView]) {
        return UIEdgeInsetsZero;
    }else{
        return (section == 0) ? UIEdgeInsetsMake(0, (kScreenW - 114.0f/375*kScreenW)/2, 0, (kScreenW - 114.0f/375*kScreenW)/2) : UIEdgeInsetsMake(0, 11, 0, 11);
    }
}

#pragma mark - RCDLiveInputBarDelegate

- (void)onTouchSendMessageButton:(NSString *)text enableGlobalMessage:(BOOL)enableGlobalMessage {
    if (!text.notEmpty) {
        return;
    }
    // 2020。6.1
    if (self.showSendMessageTips && !enableGlobalMessage) {
//        [self showTips:@"发言太快别人会看不到哦".localized];
        [self.inputBar clearInputView];
        [self.inputBar.chatSessionInputBarControl resetSendMessageBtn];
        return;
    }
    // 弹幕
    if (enableGlobalMessage) { // 如果为no 的情况下 && barragetype == common
        if (self.barrageType == BarrageType_Common || self.barrageType == BarrageType_Fans) {
            if (text.length <= 60) {
                [self requestSendBarrgeMessage:text];
                [self.inputBar clearInputView];
                [self.inputBar.chatSessionInputBarControl resetSendMessageBtn];
            }else{
                [self showToast:SPDStringWithKey(@"输入字数太多了哦！", nil)];
            }
            return;
        }
    }
    
    // 普通消息
    if (text.length <= 100) {
        BOOL isBool = [self.keyword_match match:text withKeywordMap:self.keymap];
       
        LiveRoomTextMessage *textMessage  = [LiveRoomTextMessage new];
        textMessage.content = text;
        textMessage.gender = [SPDApiUser currentUser].gender;
        textMessage.age = [SPDApiUser currentUser].age.stringValue;
        textMessage.userLevel = [self.currentUserModel.level stringValue]?:@"1";
        textMessage.medal = self.currentUserModel.medal;
        textMessage.isVip = self.currentUserModel.is_vip ? @"true":@"false";
        textMessage.specialNum = self.currentUserModel.specialNum;
        textMessage.noble = self.currentUserModel.noble;
        textMessage.bubble = self.currentUserModel.packages[Bubble];
        
        
        textMessage.portrait = self.currentUserModel.portrait;
        textMessage.medalUrl = self.currentUserModel.medal;
        textMessage.customTag = self.currentUserModel.customTag;
        
        textMessage.userIdentity = LY_MessageSenderUserIdentityAudience;
        
        if (self.liveInfo.wealthLevel.notEmpty && ![self.liveInfo.wealthLevel isEqualToString:@"0"]) {
            textMessage.wealthLevel = self.liveInfo.wealthLevel;
        }
        if (self.liveInfo.isFans && !self.liveInfo.isLock) {
            textMessage.fans = @{
                @"fansName":self.liveInfo.fansName,
                @"fansLevel": [NSString stringWithFormat:@"%@",@(self.liveInfo.fansLevel)]
            };
            textMessage.fansTag = self.liveInfo.fansTag;
        }
        if (self.noticeUserInfo && self.noticeUserInfo.allKeys.count > 0) {
            textMessage.mentionedType = @"2";
            textMessage.mentionedList = @[self.noticeUserInfo];
            self.noticeUserInfo = nil;
        }
        if (self.postionWeight == 100) {
            textMessage.isAdmin = YES;
        }
        if (isBool) {
            textMessage.senderUserInfo = [RCIM sharedRCIM].currentUserInfo;
            [self insertMessage:textMessage scrollToBottom:YES];
        }else{
            [self sendMessage:textMessage];
        }
        [self.inputBar clearInputView];
        [self.inputBar.chatSessionInputBarControl resetSendMessageBtn];
        [self.inputBar setInputBarStatus:RCDLiveBottomBarDefaultStatus];
        
        if (!self.showSendMessageTips && !enableGlobalMessage) {
          self.showSendMessageTips = YES;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.showSendMessageTips = NO;
            });
        }
    } else {
        [self showToast:SPDStringWithKey(@"输入字数太多了哦！", nil)];
    }
}

#pragma mark - RCTKInputBarControlDelegate

- (void)onInputTextViewDidChange:(UITextView *)inputTextView {
    if (self.noticeUserInfo && self.noticeUserInfo.count) {
        if (![inputTextView.text containsString:[NSString stringWithFormat:@"@%@ ", _noticeUserInfo[@"nick_name"]]]) {
            self.noticeUserInfo = nil;
        }
    }
}

// 切换了switch
- (void)onTouchAutoSwitchMode:(BOOL)on {
    self.quickMessageBar.alpha = !on;
    if (on && self.inputBar.bottomBarStatus != RCDLiveBottomBarKeyboardStatus ) {
       [self.inputBar setInputBarStatus:RCDLiveBottomBarKeyboardStatus];
    }
    if (!on) {
        self.barrageType = BarrageType_Default;
        self.inputBar.chatSessionInputBarControl.inputTextView.placeholder = @"想说点...".localized;
    }else{
        self.barrageType = self.barrageTopBarView.currentBarrageType;
        self.inputBar.chatSessionInputBarControl.inputTextView.placeholder = (self.barrageType == BarrageType_Common)?@"发送直播间弹幕100贝壳/条".localized:self.barrageBalanceString;
    }
    [UIView animateWithDuration:0.25 animations:^{
        [self.barrageTopBarView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.quickMessageBar.mas_top).offset(on ? 0 : CGRectGetHeight(self.quickMessageBar.frame));
        }];
        [self.view layoutIfNeeded];
        self.barrageTopBarView.alpha = on;
    }completion:^(BOOL finished) {
        
    }];
}

- (void)onInputBarControlContentSizeChanged:(CGRect)frame withAnimationDuration:(CGFloat)duration andAnimationCurve:(UIViewAnimationCurve)curve{
    if (frame.origin.y < kScreenH) {
        [UIView animateWithDuration:duration animations:^{
            [self.messageCollectionView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(-frame.size.height-CGRectGetHeight(self.quickMessageBar.frame));
            }];
           
        } completion:^(BOOL finished) {
            [self scrollToBottomAnimated:YES];// 让消息滚动到最后一个
        }];
    }else{
        [UIView animateWithDuration:duration animations:^{
            [self.messageCollectionView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(-MessageCollectionBottom);
            }];
        }];
    }
    
    // 只要g输入框高度更新 就要更新 快速发言bar
    [UIView animateWithDuration:duration animations:^{
        CGFloat y = frame.origin.y >= kScreenH ?kScreenH :frame.origin.y - CGRectGetHeight(self.quickMessageBar.frame);
        [self.quickMessageBar mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(y);
        }];
    }];
}

#pragma mark - LiveOnlineUserNumViewDelegate

-(void)liveOnlineUserNumViewClickedOnlineUser:(LiveOnlineUserNumView *)onlineUserNumView {
    LY_LiveAudienceHalfVC *audienceHalfVC = [[LY_LiveAudienceHalfVC alloc] initWithControllerHeight:500];
    audienceHalfVC.liveId = self.liveInfo.liveId;
    audienceHalfVC.giftingBtnDelegate = self;
    if (self.isAnchor && self.pkStatus != LiveRoomAnchorPKInProcessStatus && self.openChat) {
        audienceHalfVC.showInviteOnlineLinkMic = YES;
    }
    [self presentViewController:audienceHalfVC animated:true completion:nil];
}

#pragma mark - LiveRoomAudienceBottomViewDelegate

- (void)liveRoomAudienceBottomView:(LiveRoomAudienceBottomView *)view didTapViewWithTag:(LiveRoomAudienceBottomViewType)type {
    switch (type) {
        case LiveRoomAudienceBottomViewType_Input:{
            [self.inputBar setInputBarStatus:RCDLiveBottomBarKeyboardStatus];
            break;
        }
        case LiveRoomAudienceBottomViewType_LinkMic: {
            if (ZegoManager.linkMicState == LinkMicStateNormal || ZegoManager.linkMicState == LinkMicStateIncoming) {
                if (ZegoManager.linkMicLinkingDic.count < 5) {
                    [ZegoManager sendCustomCommandToUser:self.liveInfo.userId content:@"apply"];
                    ZegoManager.linkMicState = LinkMicStateDialing;
                } else {
                    [self showTips:SPDLocalizedString(@"连麦人数已达到最大人数")];
                    return;
                }
            }
            LinkMicAudienceDialingView *dialingView = [[NSBundle mainBundle] loadNibNamed:@"LinkMicAudienceDialingView" owner:self options:nil].firstObject;
            dialingView.frame = self.view.bounds;
            [self.view addSubview:dialingView];
            [dialingView showWithLiveInfo:self.liveInfo];
            break;
        }
        case LiveRoomAudienceBottomViewType_QB: {
            [[LY_LiveRechargeConchView new] present];
            break;
        }
        case LiveRoomAudienceBottomViewType_Share:{
            [self handleShare];
            break;
        }
        case LiveRoomAudienceBottomViewType_Play: {
            LiveAudiencePlayBottomView * playView = [[LiveAudiencePlayBottomView alloc]init];
            playView.delegate = self;
            playView.wealthLevel = self.liveInfo.wealthLevel;
            [playView present];
            break;
        }
        default:{
            [self popGift];
            break;
        }
           
    }
}

- (void)liveAudiencePlayBottomView:(LiveAudiencePlayBottomView *)view didClickedLuckyNumber:(NSInteger)number {
    //
    LiveLuckyNumberMessage *textMessage  = [LiveLuckyNumberMessage new];
    textMessage.content = @"幸运数字".localized;
    textMessage.lucky_number = [NSString stringWithFormat:@"%@",@(number)];
    textMessage.gender = [SPDApiUser currentUser].gender;
    textMessage.age = [SPDApiUser currentUser].age.stringValue;
    textMessage.userLevel = [self.currentUserModel.level stringValue]?:@"1";
    textMessage.medal = self.currentUserModel.medal;
    textMessage.isVip = self.currentUserModel.is_vip ? @"true":@"false";
    textMessage.specialNum = self.currentUserModel.specialNum;
    textMessage.noble = self.currentUserModel.noble;
    textMessage.bubble = self.currentUserModel.packages[Bubble];
    textMessage.portrait = self.currentUserModel.portrait;
    textMessage.medalUrl = self.currentUserModel.medal;
    textMessage.customTag = self.currentUserModel.customTag;
    textMessage.userIdentity = LY_MessageSenderUserIdentityAudience;
    if (self.liveInfo.wealthLevel.notEmpty && ![self.liveInfo.wealthLevel isEqualToString:@"0"]) {
        textMessage.wealthLevel = self.liveInfo.wealthLevel;
    }
    if (self.liveInfo.isFans && !self.liveInfo.isLock) {
        textMessage.fans = @{
            @"fansName":self.liveInfo.fansName,
            @"fansLevel": [NSString stringWithFormat:@"%@",@(self.liveInfo.fansLevel)]
        };
        textMessage.fansTag = self.liveInfo.fansTag;
    }
    
    if (self.postionWeight == 100) {
        textMessage.isAdmin = YES;
    }
    [self sendMessage:textMessage];
}

#pragma mark - LinkMicAnchorViewDelegate

- (void)linkMicAnchorViewDidHide {
    self.anchorBottomView.showingAnchorView = NO;
}

- (void)anchorDidChangeLinkMicSwitch {
    [self onTouchSendMessageButton:SPDLocalizedString(ZegoManager.linkMicSwitch ? @"主播已开启连麦" : @"主播已关闭连麦") enableGlobalMessage:NO];
}

#pragma mark - LiveRoomAnchorBottomViewDelegate

- (void)liveRoomAnchorBottomView:(LiveRoomAnchorBottomView *)view didClickedWithType:(LiveRoomAnchorBottomViewClickedItemType)type {
    switch (type) {
        case LiveRoomAnchorBottomViewClickedType_SendMsg: {
//            view.showingAnchorView = YES;
//
//            LinkMicAnchorView *anchorView = [[NSBundle mainBundle] loadNibNamed:@"LinkMicAnchorView" owner:self options:nil].firstObject;
//            anchorView.frame = self.view.bounds;
//            anchorView.delegate = self;
//            [self.view addSubview:anchorView];
//            [anchorView show];
            [self.inputBar setInputBarStatus:RCDLiveBottomBarKeyboardStatus];
            break;
        }
        case LiveRoomAnchorBottomViewClickedType_Music: {
            [self.backgroundMusicView show];
            break;
        }
        case LiveRoomAnchorBottomViewClickedType_Tool: {
//            self.toolsView.type = LiveRoomAnchorToolsViewType_Tool;
            [self.toolsView present];
            break;
        }
        case LiveRoomAnchorBottomViewClickedType_SoundEffect: {
            self.anchorBottomView.hidden = YES;
            LiveRoomAudioEffectView *view = [[NSBundle mainBundle] loadNibNamed:@"LiveRoomAudioEffectView" owner:self options:nil].firstObject;
            view.frame = self.view.bounds;
            view.delegate = self;
            [self.view addSubview:view];
            [view show];
            break;
        }
        case LiveRoomAnchorBottomViewClickedType_PK:{
            if (view.pkStatus == LiveRoomAnchorPKDefalut) {
                LY_HalfNavigationController * nav = [[LY_HalfNavigationController alloc]initWithRootViewController:self.pkInviteController controllerHeight:450];
                [self presentViewController:nav animated:YES completion:nil];
            }else if (view.pkStatus == LiveRoomAnchorPKInvitingStatus || view.pkStatus == LiveRoomAnchorPKMatchingStatus){
                __weak typeof(self) weakSelf = self;
                LYPSTextAlertView * alert = [LYPSTextAlertView new];
                alert.title = @"取消PK".localized;
                alert.message = @"是否取消PK匹配/邀请？".localized;
                [alert addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"取消") style:LYPSystemAlertActionStyleCancel handler:^(LYPSystemAlertAction * _Nonnull action) {
                    
                }]];
                [alert addAction:[LYPSystemAlertAction actionWithTitle:SPDLocalizedString(@"确定") style:LYPSystemAlertActionStyleDefault handler:^(LYPSystemAlertAction * _Nonnull action) {
                    NSString * url = (view.pkStatus == LiveRoomAnchorPKInvitingStatus) ? @"live/cancel.pk.invite":@"live/cancel.pk.random";
                    [RequestUtils POST:URL_NewServer(url) parameters:@{} success:^(id  _Nullable suceess) {
                        
                        weakSelf.pkStatus = LiveRoomAnchorPKDefalut;
                        
                    } failure:^(id  _Nullable failure) {
                        [weakSelf showTips:failure[@"msg"]];
                    }];
                }]];
                [alert present];
            }else if (view.pkStatus == LiveRoomAnchorPKInProcessStatus){
                
            }
            break;
        }
        default: {
            break;
        }
    }
}

#pragma mark - LiveRoomAnchorToolsViewDelegate

- (void)liveRoomAnchorToolsView:(LiveRoomAnchorToolsView *)view didClickedItemWithType:(LiveRoomAnchorToolsViewItemType)type {
    switch (type) {
        case LiveRoomAnchorToolsViewItemType_SendMessage: {
            [self.inputBar setInputBarStatus:RCDLiveBottomBarKeyboardStatus];
            break;
        }
        case LiveRoomAnchorToolsViewItemType_SendImage: {
           UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
           imagePickerController.delegate = self;
           [imagePickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
           [self presentViewController:imagePickerController animated:YES completion:nil];
           break;
        }
        case LiveRoomAnchorToolsViewItemType_CloseMic: {
            LiveRoomOpenMicView *view = [[NSBundle mainBundle] loadNibNamed:@"LiveRoomOpenMicView" owner:self options:nil].firstObject;
            view.center = self.view.center;
            [self.view addSubview:view];
            break;
        }
        case LiveRoomAnchorToolsViewItemType_AudioConsole: {
            [self.audioConsoleView show];
            break;
        }
        case LiveRoomAnchorToolsViewItemType_LinkMicSettings: {
            if (self.pkStatus != LiveRoomAnchorPKDefalut && self.pkStatus != LiveRoomAnchorPKInProcessUnable ) {
                [self showTips:@"正在PK无法使用此功能".localized];
                return;
            }
            self.linkMicSettingsView.liveId = self.liveInfo.liveId;
            [self.linkMicSettingsView present];
            break;
        }
        case LiveRoomAnchorToolsViewItemType_LiveManager:{
//            LiveManagerListController *
            LY_HalfNavigationController * nav = [[LY_HalfNavigationController alloc]initWithRootViewController:[LiveManagerListController new] controllerHeight:415.0f/667 * kScreenH +IphoneX_Bottom];
            [self presentViewController:nav animated:YES completion:nil];
            break;
        }
        default:{
            [self handleShare];
            break;
        }
    }
}
#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey, id> *)info{
    [self dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    [self uploadImage:image];
}

#pragma mark - LiveRoomImageMessageCellDelegate

- (void)liveRoomImageMessageCell:(LiveImageMessage *)message {
    LiveRoomImageMessagePreviewController * vc = [[LiveRoomImageMessagePreviewController alloc]init];
    vc.url = message.imageUrl;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - LY_FanGroupViewControllerDelegate

- (void)fanGroupViewControllerDidClickJoinFansGroupWith:(LY_FanGroupViewController *)fanGroupViewController withFansGroupName:(NSString *)fansGroupName {
    self.liveInfo.isFans = true;
    self.liveInfo.canFinishTask = true;
    self.liveInfo.fansLevel = 1;
    self.liveInfo.fansName = fansGroupName;
}

- (void)fanGroupViewControllerDidClickHearLiveWith:(LY_FanGroupViewController *)fanGroupViewController {
    
}

- (void)fanGroupViewControllerDidClickSendGiftWith:(LY_FanGroupViewController *)fanGroupViewController {
    [self popGift];
}

- (void)fanGroupViewControllerDidClickSendMessageWith:(LY_FanGroupViewController *)fanGroupViewController {
    [self.inputBar setInputBarStatus:RCDLiveBottomBarKeyboardStatus];
}

- (void)fanGroupViewControllerDidClickShareLiveWith:(LY_FanGroupViewController *)fanGroupViewController {
    [self handleShare];
}

#pragma mark - LY_LiveFansInviteMessageCellDelegate

- (void)liveFansInviteMessageCell:(LY_LiveFansInviteMessageCell *)liveFansInviteMessageCell didClickJoinBtn:(UIButton *)joinBtn {
//    fan
    [self popupFansView];
}

- (void)liveFansInviteMessageCell:(LY_LiveFansInviteMessageCell *)liveFansInviteMessageCell didClickSeeBtn:(UIButton *)seeBtn {
    
    [self popupFansView];
}

#pragma mark - LY_FunsGroupCreatViewControllerDelegate

- (void)funsGroupCreatViewController:(LY_FunsGroupCreatViewController *)funsGroupCreatViewController checkStatus:(NSString *)checkStatus {
    // 审核状态，0待审核，1审核通过，2审核拒绝
    if ([checkStatus isEqualToString:@"1"]) {
        self.liveInfo.haveFansGroup = true;
        self.fansView.text = [NSString stringWithFormat:@"粉丝团 %ld人".localized, 0];
        [funsGroupCreatViewController.navigationController dismissViewControllerAnimated:false completion:nil];
        [self popupFansView];
    }
}
#pragma mark - LiveRoomGiftView

- (void)liveRoomGiftViewShowComboButton:(LiveRoomGiftView *)giftView prepareComboWithURL:(NSString *)URL params:(NSMutableDictionary *)params {
    self.giftComboURL = URL;
    self.giftComboParams = params;
    [self.giftComboParams setValue:@"1" forKey:@"num"];
    ChatroomGiftComboButton *button = [[NSBundle mainBundle] loadNibNamed:@"ChatroomGiftComboButton" owner:self options:nil].firstObject;
    button.frame = self.view.bounds;
    button.delegate = self;
    [self.view addSubview:button];
}

#pragma mark - ChatroomGiftComboButtonDelegate

- (void)didClickGiftComboButton {
    [RequestUtils POST:self.giftComboURL parameters:self.giftComboParams success:^(id  _Nullable suceess) {
        
    } failure:^(id  _Nullable failure) {
        
    }];
}

- (void)giftComboCountdownDidEnd {
    [self popGift];
}

#pragma mark - SDCycleScrollViewDelegate

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
     if (cycleScrollView == self.clanActivityScrollView) {
         BannerModel *model = self.clanActivityArray[index];
         [self loadWebViewController:self title:@"" url:model.action_url];
    }
}

#pragma mark - LiveRecommendAnchorControllerDelgate

- (void)liveRecommendAnchorController:(LiveRecommendAnchorController *)controller didClickedFollowButton:(UIButton *)sender {
    [self requestFollowWithType:@"follow"];
}

#pragma mark - QuickMessageTopBarDelegate

- (void)quickMessageTopBar:(QuickMessageTopBar *)topBar didClickedShowMoreButton:(UIButton *)sender {
    [self.inputBar setInputBarStatus:sender.selected ? RCDLiveBottomBarKeyboardStatus:RCDLiveBottomBarQuickMessageStatus];
}

#pragma mark - BarrageTopBarViewDelegate

- (void)barrageTopBarView:(BarrageTopBarView *)view didChangeBarrageType:(BarrageType)type {
    self.barrageType = type;
    self.inputBar.chatSessionInputBarControl.inputTextView.placeholder = (self.barrageType == BarrageType_Common)?@"发送直播间弹幕100贝壳/条".localized:self.barrageBalanceString;
}

#pragma mark - Barrage

- (BarrageDescriptor *)createBarrageDescriptorWithDic:(NSDictionary *)dic delay:(CGFloat)delay {
    BarrageDescriptor *descriptor = [BarrageDescriptor new];
    descriptor.spriteName = NSStringFromClass([BarrageWalkSprite class]);
    descriptor.params[@"viewClassName"] = @"BarrageView";
    descriptor.params[@"trackNumber"] = @(2);
    descriptor.params[@"direction"] = @(kIsMirroredLayout ? BarrageWalkDirectionL2R : BarrageWalkDirectionR2L);
    descriptor.params[@"delay"] = @(delay);
    descriptor.params[@"user_id"] = dic[@"user_id"];
    descriptor.params[@"isFansBarrage"] = dic[@"isFansBarrage"];
    descriptor.params[@"avatar"] = dic[@"avatar"];
    descriptor.params[@"headwearWebp"] = dic[@"headwearWebp"];
    descriptor.params[@"nick_name"] = dic[@"nick_name"];
    descriptor.params[@"content"] = dic[@"content"];
//    __weak typeof(self)weakSelf = self;
    [descriptor clickAction:^(NSDictionary *params) {
//        if ([weakSelf.delegate respondsToSelector:@selector(slideWaveCardView:didClickBarrageWithParams:)]) {
//            [weakSelf.delegate slideWaveCardView:weakSelf didClickBarrageWithParams:params];
//            //[MobClick event:@"clickSendBarrageTheUserButton"];
//        }
    }];
    return descriptor;
}


#pragma mark - unreadmessage
- (BOOL)isAtTheBottomOfTableView {
    if (self.messageCollectionView.contentSize.height <= self.messageCollectionView.frame.size.height) {
        return YES;
    }
    if(self.messageCollectionView.contentOffset.y +200 >= (self.messageCollectionView.contentSize.height - self.messageCollectionView.frame.size.height)) {
        return YES;
    }else{
        return NO;
    }
}

/**
 *  更新底部新消息提示显示状态
 */
- (void)updateUnreadMsgCountLabel{
    if (self.unreadNewMsgCount == 0) {
        self.unreadMessageView.hidden = YES;
    }
    else{
        self.unreadMessageView.hidden = NO;
        self.unreadMessageView.unreadCount = self.unreadNewMsgCount;
    }
}

/**
 *  检查是否更新新消息提醒
 */
- (void) checkVisiableCell{
    NSIndexPath *lastPath = [self getLastIndexPathForVisibleItems];
    if (lastPath.row >= self.messageArr.count - self.unreadNewMsgCount || lastPath == nil || [self isAtTheBottomOfTableView] ) {
        self.unreadNewMsgCount = 0;
        [self updateUnreadMsgCountLabel];
    }
}

/**
 *  获取显示的最后一条消息的indexPath
 *
 *  @return indexPath
 */
- (NSIndexPath *)getLastIndexPathForVisibleItems
{
    NSArray *visiblePaths = [self.messageCollectionView indexPathsForVisibleItems];
    if (visiblePaths.count == 0) {
        return nil;
    }else if(visiblePaths.count == 1) {
        return (NSIndexPath *)[visiblePaths firstObject];
    }
    NSArray *sortedIndexPaths = [visiblePaths sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSIndexPath *path1 = (NSIndexPath *)obj1;
        NSIndexPath *path2 = (NSIndexPath *)obj2;
        return [path1 compare:path2];
    }];
    return (NSIndexPath *)[sortedIndexPaths lastObject];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.unreadNewMsgCount != 0) {
        [self checkVisiableCell];
    }
}

#pragma mark - PK

- (void)didClickedPkMatch {
    [RequestUtils POST:URL_NewServer(@"live/pk.random") parameters:@{} success:^(id  _Nullable suceess) {
        self.pkStatus = LiveRoomAnchorPKMatchingStatus;
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

- (void)didClickedPkInviteWithUserId:(NSString * )userId{
    [RequestUtils POST:URL_NewServer(@"live/pk.invite") parameters:@{@"inviteUserId":userId} success:^(id  _Nullable suceess) {
        self.pkStatus = LiveRoomAnchorPKInvitingStatus;
    } failure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}

#pragma mark - PKProcessAnimationView

- (void)PKProcessAnimationViewDidEndPK:(PKProcessAnimationView *)processView otherAnchorID:(NSString *)otherAnchorID{
    self.pkStatus = LiveRoomAnchorPKDefalut;
    self.openChat = ZegoManager.linkMicSwitch;
    [self handlePKWithUserId:otherAnchorID Status:NO];
    if (self.isAnchor) {
        [self liveRoomAnchorBottomView:self.anchorBottomView didClickedWithType:LiveRoomAnchorBottomViewClickedType_PK];
    }
}

- (void)PKProcessAnimationViewDidTapUserAvatar:(NSString *)userId {
    [self showUserInfoViewWithUserId:userId];
}

- (void)PKProcessAnimationViewDidTapContributionList:(NSString * )pkId liveId:(NSString *)liveId andType:(NSString *)type {
    PKContributionListController * contributionVC = [PKContributionListController new];
    contributionVC.pkId = pkId;
    contributionVC.liveId = liveId;
    contributionVC.type = type;
    LY_HalfNavigationController * nav = [[LY_HalfNavigationController alloc]initWithRootViewController:contributionVC controllerHeight:417];
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (self.inputBar.bottomBarStatus != RCDLiveBottomBarDefaultStatus) {
        return YES;
    }else{
        return NO;
    }
}

#pragma mark - SoundLevel

- (void)initTimer {
    if (!self.timer) {
        self.timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(getSoundLevel) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
        [self.timer fire];
    }
}

- (void)releaseTimer {
    [self.timer invalidate];
    self.timer = nil;
}

- (void)getSoundLevel {
    for (ZegoAudioStream *stream in [ZegoManager.streams allValues]) {
        if ([stream.userID isEqualToString:[SPDApiUser currentUser].userId]) {
            if ([ZegoManager.api getCaptureSoundLevel] > 0.5) {
                [self updateMicStatus:YES userId:stream.userID];
            } else {
                [self updateMicStatus:NO userId:stream.userID];
            };
        } else {
            if ([ZegoManager.api getSoundLevelOfStream:stream.streamID] > 0.5) {
                [self updateMicStatus:YES userId:stream.userID];
            } else {
                [self updateMicStatus:NO userId:stream.userID];
            };
        }
    }
}

- (void)updateMicStatus:(BOOL) speaking userId:(NSString *)userId {
    NSString * index;
    if ([userId isEqualToString:self.liveInfo.userId] || [userId containsString:self.liveInfo.userId]) {
        index = @"0";
        [self.titleView setAnimating:speaking];
    }else{
        LinkMicNewModel * model = ZegoManager.linkMicUserInfoDic[userId];
        index = model.site;
    }
    if (index) {
        NSInteger section = [index isEqualToString:@"0"] ? 0:1;
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:section == 0 ? index.integerValue:index.integerValue -1 inSection:section];
        LinkMicUserCell *sCell = (LinkMicUserCell *)[_bigMicCollectionView cellForItemAtIndexPath:indexPath];
        sCell.animating = speaking;
    }
}


#pragma mark - Getters

- (UIImageView *)backgroundImageView{
    if (!_backgroundImageView) {
        _backgroundImageView = [[UIImageView alloc]init];
        _backgroundImageView.image = [UIImage imageNamed:@"img_beijing"];
    }
    return _backgroundImageView;
}

- (UIButton *)exitButton {
    if (!_exitButton) {
        _exitButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_exitButton setImage:[UIImage imageNamed:@"ic_chatroom_back_btn"] forState:UIControlStateNormal];
        [_exitButton addTarget:self action:@selector(handleExitButton:) forControlEvents:UIControlEventTouchUpInside];
        [_exitButton hx_setEnlargeEdgeWithTop:10 right:10 bottom:10 left:10];
    }
    return _exitButton;
}

- (LiveRoomTitleView *)titleView {
    if (!_titleView) {
        _titleView = [[[NSBundle mainBundle]loadNibNamed:@"LiveRoomTitleView" owner:self options:nil] firstObject];
        _titleView.delegate = self;
    }
    return _titleView;
}

- (LiveAnchorOfflineView *)anchorOfflineView {
    if (!_anchorOfflineView) {
        _anchorOfflineView = [[[NSBundle mainBundle]loadNibNamed:@"LiveAnchorOfflineView" owner:self options:nil]lastObject];
    }
    return _anchorOfflineView;
}

- (NSMutableArray<RCMessageContent *> *)messageArr {
    if (!_messageArr) {
        _messageArr = [NSMutableArray new];
    }
    return _messageArr;
}

- (UICollectionView *)messageCollectionView {
    if (!_messageCollectionView) {
        UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout new];
        _messageCollectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _messageCollectionView.delegate = self;
        _messageCollectionView.dataSource = self;
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleClearInputBar)];
        [_messageCollectionView addGestureRecognizer:tap];
        _messageCollectionView.backgroundColor = [UIColor clearColor];
//        [_messageCollectionView registerNib:[UINib nibWithNibName:@"LiveRoomTextMessageCell" bundle:nil] forCellWithReuseIdentifier:@"LiveRoomTextMessageCell"];
        [_messageCollectionView registerClass:[LiveRoomTextMessageCell class] forCellWithReuseIdentifier:@"LiveRoomTextMessageCell"];
        [_messageCollectionView registerNib:[UINib nibWithNibName:@"LiveRoomSystemMessageCell" bundle:nil] forCellWithReuseIdentifier:@"LiveRoomSystemMessageCell"];
//        [_messageCollectionView registerNib:[UINib nibWithNibName:@"LiveRoomImageMessageCell" bundle:nil] forCellWithReuseIdentifier:@"LiveRoomImageMessageCell"];
        [_messageCollectionView registerClass:[LiveRoomImageMessageCell class] forCellWithReuseIdentifier:@"LiveRoomImageMessageCell"];
        [_messageCollectionView registerClass:[LY_LiveFansInviteMessageCell class] forCellWithReuseIdentifier:@"LY_LiveFansInviteMessageCell"];
        [_messageCollectionView registerClass:[LY_LiveFollowedMessageCell class] forCellWithReuseIdentifier:@"LY_LiveFollowedMessageCell"];
        [_messageCollectionView registerClass:[LiveJoinMessageCell class] forCellWithReuseIdentifier:@"LiveJoinMessageCell"];
        [_messageCollectionView registerClass:[LiveGiftMessageCell class] forCellWithReuseIdentifier:@"LiveGiftMessageCell"];
        [_messageCollectionView registerClass:[LiveWealthLevelMessageCell class] forCellWithReuseIdentifier:@"LiveWealthLevelMessageCell"];
        _messageCollectionView.alwaysBounceVertical = YES;
        _messageCollectionView.showsVerticalScrollIndicator = NO;
        _messageCollectionView.userInteractionEnabled = true;
    }
    return _messageCollectionView;
}

- (LiveRoomAnchorBottomView *)anchorBottomView {
    if (!_anchorBottomView) {
        _anchorBottomView = [[LiveRoomAnchorBottomView alloc]init];
        _anchorBottomView.delegate = self;
    }
    return _anchorBottomView;
}

- (LiveRoomAudienceBottomView *)audienceBottomView {
    if (!_audienceBottomView) {
        _audienceBottomView = [[LiveRoomAudienceBottomView alloc]init];
        BOOL b = [[NSUserDefaults standardUserDefaults] boolForKey:@"HaveNewFreeGifts"];
//        [_audienceBottomView updateStatusIcon:b];
        _audienceBottomView.delegate = self;
       }
    return _audienceBottomView;
}

- (RCDLiveInputBar *)inputBar {
    if (!_inputBar) {
        _inputBar = [[RCDLiveInputBar alloc] initWithFrame:CGRectMake(0, kScreenH + CGRectGetHeight(self.quickMessageBar.frame), kScreenW, 58)];
        _inputBar.isLiveMode = YES;
        _inputBar.delegate = self;
        [self.view addSubview:_inputBar];
    }
    return _inputBar;
}

- (LiveRoomAnchorToolsView *)toolsView {
    if (!_toolsView) {
        _toolsView = [[LiveRoomAnchorToolsView alloc]initWithStyle:LY_AlertViewStyleActionSheet];
        _toolsView.delegate = self;
    }
    return _toolsView;
}

- (LiveRoomGiftAnimationView *)giftAnimationView {
    if (!_giftAnimationView) {
        _giftAnimationView = [[[NSBundle mainBundle]loadNibNamed:@"LiveRoomGiftAnimationView" owner:self options:nil]firstObject];
    }
    return _giftAnimationView;
}

- (LiveRoomBackgroundMusicView *)backgroundMusicView {
    if (!_backgroundMusicView) {
        _backgroundMusicView = [[NSBundle mainBundle] loadNibNamed:@"LiveRoomBackgroundMusicView" owner:self options:nil].firstObject;
        _backgroundMusicView.frame = self.view.bounds;
        [self.view addSubview:_backgroundMusicView];
    }
    return _backgroundMusicView;
}

- (LiveRoomAudioConsoleView *)audioConsoleView {
    if (!_audioConsoleView) {
        _audioConsoleView = [[NSBundle mainBundle] loadNibNamed:@"LiveRoomAudioConsoleView" owner:self options:nil].firstObject;
        _audioConsoleView.frame = self.view.bounds;
        [self.view addSubview:_audioConsoleView];
    }
    return _audioConsoleView;
}

- (SPDKeywordMatcher *)keyword_match {
    if (!_keyword_match) {
        _keyword_match = [[SPDKeywordMatcher alloc]init];
    }
    return _keyword_match;
}

- (KeywordMap *)keymap {
    if (!_keymap) {
        NSMutableDictionary * wordDict = [[NSMutableDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"word" ofType:@"plist"]];
        _keymap = [self.keyword_match convert:[wordDict allKeys]];
    }
    return _keymap;
}

- (NSMutableArray *)clanActivityArray{
    if (!_clanActivityArray) {
        _clanActivityArray = [[NSMutableArray alloc]init];
    }
    return _clanActivityArray;
}

- (QuickMessageTopBar *)quickMessageBar {
    if (!_quickMessageBar) {
        _quickMessageBar = [[QuickMessageTopBar alloc]initWithFrame:CGRectMake(0, kScreenH, kScreenW, QuickMessageBarHeight)];
        _quickMessageBar.delegate = self;
    }
    return _quickMessageBar;
}

- (BarrageTopBarView *)barrageTopBarView {
    if (!_barrageTopBarView) {
        _barrageTopBarView = [[[NSBundle mainBundle] loadNibNamed:@"BarrageTopBarView" owner:self options:nil]lastObject];
        _barrageTopBarView.delegate = self;
    }
    return _barrageTopBarView;
}

- (BarrageRenderer *)barrageRenderer {
    if (!_barrageRenderer) {
        _barrageRenderer = [BarrageRenderer new];
        _barrageRenderer.view.userInteractionEnabled = YES;
        _barrageRenderer.masked = NO;
        _barrageRenderer.speed = 1200;
        _barrageRenderer.canvasMargin = UIEdgeInsetsMake(300, 0, self.view.bounds.size.height - 79 - 23 * 6, 0);
        [self.view addSubview:_barrageRenderer.view];
        [_barrageRenderer start];
    }
    return _barrageRenderer;
}

- (LiveRoomEnteringView *)enteringView {
    if (!_enteringView) {
        _enteringView = [[NSBundle mainBundle] loadNibNamed:@"LiveRoomEnteringView" owner:self options:nil].firstObject;
        _enteringView.frame = self.view.bounds;
        [_enteringView.refreshButton addTarget:self action:@selector(clickRefreshButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_enteringView];
    }
    return _enteringView;
}

- (SPDChatroomNobleEnterView *)nobleEnterView {
    if (!_nobleEnterView) {
        _nobleEnterView = [[SPDChatroomNobleEnterView alloc] initWithFrame:CGRectMake(kScreenW + 10, CGRectGetMaxY(self.titleView.frame) + 30, 271, 24)];
    }
    return _nobleEnterView;
}

- (KKPaddingLabel *)pkScoreLabel {
    if (!_pkScoreLabel) {
        _pkScoreLabel = [[KKPaddingLabel alloc]init];
        _pkScoreLabel.text = @"PK积分 >".localized;
        _pkScoreLabel.font = [UIFont mediumFontOfSize:11];
        _pkScoreLabel.textColor = [UIColor whiteColor];
        _pkScoreLabel.backgroundColor = [SPD_HEXCOlOR(@"#FF6C94") colorWithAlphaComponent:0.1];
        _pkScoreLabel.userInteractionEnabled = YES;
        _pkScoreLabel.padding = UIEdgeInsetsMake(7, 11, 7, 11);
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handlePKScoreAction:)];
        [_pkScoreLabel addGestureRecognizer:tap];
    }
    return _pkScoreLabel;
}

- (UICollectionView *)bigMicCollectionView {
    if (!_bigMicCollectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.minimumLineSpacing = 0;
        flowLayout.minimumInteritemSpacing = 0;
        _bigMicCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _bigMicCollectionView.dataSource = self;
        _bigMicCollectionView.delegate = self;
        _bigMicCollectionView.hidden = YES;
        _bigMicCollectionView.backgroundColor = [UIColor clearColor];
        _bigMicCollectionView.showsHorizontalScrollIndicator = NO;
        [_bigMicCollectionView registerClass:[LinkMicUserCell class] forCellWithReuseIdentifier:@"LinkMicUserCell"];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleClearInputBar)];
        tap.delegate = self;
        [_bigMicCollectionView addGestureRecognizer:tap];
        _bigMicCollectionView.userInteractionEnabled = YES;
    }
    return _bigMicCollectionView;
}

- (LinkMicSettingsView *)linkMicSettingsView {
    if (!_linkMicSettingsView) {
        _linkMicSettingsView = [[LinkMicSettingsView alloc]initWithStyle:LY_AlertViewStyleActionSheet];
    }
    return _linkMicSettingsView;
}

- (PKInviteViewController *)pkInviteController {
    if (!_pkInviteController) {
       _pkInviteController = [PKInviteViewController new];
        _pkInviteController.delegate = self;
    }
    return _pkInviteController;
}

- (LiveRedPackIconView *)redPackIconView {
    if (!_redPackIconView) {
        _redPackIconView = [[LiveRedPackIconView alloc]init];
        _redPackIconView.hidden = YES;
    }
    return _redPackIconView;
}

@end
