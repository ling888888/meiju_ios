//
//  LY_FansListViewController.m
//  SimpleDate
//
//  Created by jianyue on 2020/4/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_FansListViewController.h"
#import "LY_FansTableViewCell.h"
#import "LY_FansGroupRuleViewController.h"
#import "LY_FansLevelView.h"
#import "LY_FansSuccessJoinAlertView.h"
#import "LY_FunsGroupEditViewController.h"

@interface LY_FansListViewController () <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) UIView *whiteBgView;

@property(nonatomic, strong) UIImageView *portraitView;

@property(nonatomic, strong) UILabel *nameLabel;

@property(nonatomic, strong) UILabel *memberNumLabel;

@property(nonatomic, strong) UILabel *intimacyLabel;

@property(nonatomic, strong) UITableView *tableView;

@property(nonatomic, strong) BottomSelfView *bottomSelfView;

@property(nonatomic, strong) UIButton *whyBtn;

@property(nonatomic, strong) UIButton *editNameBtn;

@property(nonatomic, strong) NSArray *dataList;

@property(nonatomic, strong) LY_Fans *myFansInfo;

@end

@implementation LY_FansListViewController

- (UIView *)whiteBgView {
    if (!_whiteBgView) {
        _whiteBgView = [[UIView alloc] init];
        _whiteBgView.backgroundColor = [UIColor whiteColor];
    }
    return _whiteBgView;
}

- (UIImageView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[UIImageView alloc] init];
        _portraitView.contentMode = UIViewContentModeScaleAspectFit;
        _portraitView.layer.cornerRadius = 35.5;
        _portraitView.layer.masksToBounds = true;
        _portraitView.backgroundColor = [UIColor whiteColor];
    }
    return _portraitView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _nameLabel.font = [UIFont boldSystemFontOfSize:17];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        _nameLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    }
    return _nameLabel;
}

- (UILabel *)memberNumLabel {
    if (!_memberNumLabel) {
        _memberNumLabel = [[UILabel alloc] init];
        _memberNumLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _memberNumLabel.font = [UIFont mediumFontOfSize:14];
        _memberNumLabel.text = [NSString stringWithFormat:@"成员%ld人".localized, 0];
    }
    return _memberNumLabel;
}

- (UILabel *)intimacyLabel {
    if (!_intimacyLabel) {
        _intimacyLabel = [[UILabel alloc] init];
        _intimacyLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _intimacyLabel.font = [UIFont mediumFontOfSize:14];
        _intimacyLabel.text = @"亲密度".localized;
    }
    return _intimacyLabel;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.rowHeight = 70;
        _tableView.separatorInset = UIEdgeInsetsMake(0, 102, 0, 0);
        _tableView.separatorColor = [UIColor colorWithHexString:@"#EFEFEF"];
        [_tableView registerClass:[LY_FansTableViewCell class] forCellReuseIdentifier:@"FansIdentifier"];
        _tableView.tableFooterView = [[UIView alloc] init];
    }
    return _tableView;
}

- (BottomSelfView *)bottomSelfView {
    if (!_bottomSelfView) {
        _bottomSelfView = [[BottomSelfView alloc] init];
        _bottomSelfView.backgroundColor = [UIColor whiteColor];
        _bottomSelfView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.08].CGColor;
        _bottomSelfView.layer.shadowOffset = CGSizeMake(0,3);
        _bottomSelfView.layer.shadowOpacity = 1;
        _bottomSelfView.layer.shadowRadius = 15;
        [_bottomSelfView setHidden:true];
    }
    return _bottomSelfView;
}

- (UIButton *)whyBtn {
    if (!_whyBtn) {
        _whyBtn = [[UIButton alloc] init];
        [_whyBtn setImage:[UIImage imageNamed:@"ic_zhubo_guize_hei"] forState:UIControlStateNormal];
        [_whyBtn addTarget:self action:@selector(whyBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _whyBtn;
}

- (UIButton *)editNameBtn {
    if (!_editNameBtn) {
        _editNameBtn = [[UIButton alloc] init];
        [_editNameBtn setImage:[UIImage imageNamed:@"ic_zhibo_zhubo_bianji"] forState:UIControlStateNormal];
        [_editNameBtn addTarget:self action:@selector(editNameBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _editNameBtn;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavigation];
    
    [self setupUI];

    [self setupUIFrame];
    
    [self setupData];
    
    self.tableView.tipImageName = @"img_zhibo_fensiquan_kongyemian";
    
    self.tableView.vOffset = -10;
    
    // 判断是否是主播
    if (self.isAnchorStyle) {
        // 是主播
        [self.portraitView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:self.portraitUrl]] placeholderImage:[UIImage imageNamed:@"img_portrait_placeholder"]];
        self.nameLabel.text = [NSString stringWithFormat:@"%@的粉丝团".localized, self.nickname];
        self.tableView.tipText = @"暂无团成员".localized;
    } else {
        self.tableView.tipText = @"暂无成员，快来成为首位团员吧".localized;
        self.tableView.tipButtonTitle = @"立即加入".localized;
        self.tableView.tipButton.backgroundColor = [UIColor colorWithHexString:@"#FF6B93"];
        [self.tableView.tipButton addTarget:self action:@selector(joinFansGroupBtnAction) forControlEvents:UIControlEventTouchUpInside];
        // 设置导航栏标题样式
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        style.lineBreakMode = NSLineBreakByTruncatingMiddle;
        NSDictionary *titleTextAttributes=@{
        NSParagraphStyleAttributeName:style
        };
        self.navigationController.navigationBar.titleTextAttributes = titleTextAttributes;
        self.title = [NSString stringWithFormat:@"%@的粉丝团".localized, self.nickname];
    }
    
    self.bottomSelfView.rank = 100;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    [self.whiteBgView addRounded:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:10];
}

- (void)setupNavigation {
    // 判断是否是主播查看
    if (self.isAnchorStyle) {
        // 主播查看
    } else {
        // 观众查看
    }
}

- (void)setupUI {
    [self.view addSubview:self.whiteBgView];
    [self.whiteBgView addSubview:self.memberNumLabel];
    [self.whiteBgView addSubview:self.intimacyLabel];
    [self.whiteBgView addSubview:self.tableView];
    
    // 判断是否是主播查看
    if (self.isAnchorStyle) {
        // 主播查看
        self.view.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.portraitView];
        [self.whiteBgView addSubview:self.nameLabel];
        [self.whiteBgView addSubview:self.whyBtn];
        [self.whiteBgView addSubview:self.editNameBtn];
        
//        self.portraitView.backgroundColor = [UIColor redColor];
//        self.nameLabel.text = @"主播的粉丝团";
    } else {
        // 观众查看
        [self.whiteBgView addSubview:self.bottomSelfView];
    }
}

- (void)setupUIFrame {
    [self.whiteBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.bottom.trailing.mas_equalTo(self.view);
        make.top.mas_equalTo(self.view);
    }];
    [self.memberNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(44 + 15);
        make.leading.mas_equalTo(15);
    }];
    [self.intimacyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.memberNumLabel);
        make.trailing.mas_equalTo(-15);
    }];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.memberNumLabel.mas_bottom).offset(15);
        make.leading.trailing.mas_equalTo(self.view);
        make.bottom.mas_equalTo(-70);
    }];
    
    // 判断是否是主播查看
    if (self.isAnchorStyle) {
        // 主播查看
        [self.whiteBgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(36);
        }];
        [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(0);
        }];
        [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(71);
            make.centerX.mas_equalTo(self.whiteBgView);
            make.centerY.mas_equalTo(self.whiteBgView.mas_top);
        }];
        [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.portraitView.mas_bottom).offset(15);
            make.centerX.mas_equalTo(0);
            make.height.mas_equalTo(20);
        }];
        [self.whyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(40);
            make.top.mas_equalTo(5);
            make.trailing.mas_equalTo(-5);
        }];
        [self.editNameBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(40);
            make.centerY.mas_equalTo(self.nameLabel);
            make.centerX.mas_equalTo(self.whyBtn);
        }];
        [self.memberNumLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(85);
        }];
    } else {
        // 观众查看
        [self.bottomSelfView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.bottom.trailing.mas_equalTo(self.view);
            make.height.mas_equalTo(70);
        }];
    }
}

- (void)setMyFansInfo:(LY_Fans *)myFansInfo {
    _myFansInfo = myFansInfo;
    
    // 判断是否是主播
    if (self.isAnchorStyle) {
        // 是主播
        
    } else {
        // 不是主播
        // 判断是否加入直播间
        if (myFansInfo) {
            // 已加入
            [self.bottomSelfView setHidden:false];
            self.bottomSelfView.fans = myFansInfo;
            [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(-70);
            }];
        } else {
            // 未加入
            [self.bottomSelfView setHidden:true];
            [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(0);
            }];
        }
    }
}

- (void)setupData {
    NSDictionary *params = @{@"anchor_id": self.anchorUserId};
    [LY_Network getRequestWithURL:LY_Api.live_fans_list parameters:params success:^(id  _Nullable response) {
        NSArray *dataArray = response[@"list"];
        self.myFansInfo = [LY_Fans yy_modelWithDictionary:response[@"mydata"]];
        
        self.memberNumLabel.text = [NSString stringWithFormat:@"成员%ld人".localized, [response[@"count"] intValue]];
        
        NSMutableArray *models = [NSMutableArray array];
        for (NSDictionary *dict in dataArray) {
            LY_Fans *fans = [LY_Fans yy_modelWithDictionary:dict];
            [models addObject:fans];
        }
        
        if (models.count == 0) {
            self.tableView.showEmptyTipView = true;
        } else {
            self.tableView.showEmptyTipView = false;
        }
        
        self.dataList = models;
        [self.tableView reloadData];
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

- (void)whyBtnAction {
    NSString *urlString = [NSString stringWithFormat:@"https://share.famy.ly/ar/fansRule.html?lang=%@", [SPDCommonTool getFamyLanguage]];
    LY_FansGroupRuleViewController *webVC = [[LY_FansGroupRuleViewController alloc] initWithURLString:urlString];
    webVC.title = @"粉丝团规则".localized;
    [self.navigationController pushViewController:webVC animated:true];
}

- (void)editNameBtnAction {
    LY_FunsGroupEditViewController *funsGroupEditVC = [[LY_FunsGroupEditViewController alloc] init];
    [self.navigationController pushViewController:funsGroupEditVC animated:true];
}

- (void)joinFansGroupBtnAction {
    NSLog(@"asdfeqwrqwe");
    
    NSDictionary *params = @{@"anchor_id":self.anchorUserId};
    [LY_Network postRequestWithURL:LY_Api.live_fansGroup_join parameters:params success:^(id  _Nullable response) {
        LY_FansSuccessJoinAlertView * successJoinAV = [[LY_FansSuccessJoinAlertView alloc] init];
        [successJoinAV present];
        __weak typeof(self) weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [successJoinAV dismiss];
            [weakSelf setupData];
        });
        if ([weakSelf.delegate respondsToSelector:@selector(fansListViewControllerDidJoinFansGroup:)]) {
            [weakSelf.delegate fansListViewControllerDidJoinFansGroup:self];
        }
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LY_FansTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FansIdentifier" forIndexPath:indexPath];
    
    cell.fans = self.dataList[indexPath.row];
    cell.rank = indexPath.row + 1;
    
    return cell;
}

@end

@interface BottomSelfView ()

@property(nonatomic, strong) UILabel *rankLabel;

@property(nonatomic, strong) UIImageView *portraitView;

@property(nonatomic, strong) UILabel *nameLabel;

@property(nonatomic, strong) LY_FansLevelView *leveView;

@property(nonatomic, strong) UILabel *intimacyLabel;

@property(nonatomic, strong) UIImageView *heartImgView;

@end

@implementation BottomSelfView

- (UILabel *)rankLabel {
    if (!_rankLabel) {
        _rankLabel = [[UILabel alloc] init];
        _rankLabel.textAlignment = NSTextAlignmentCenter;
        //设置中文倾斜
        //设置反射。倾斜角度。
        CGAffineTransform matrix =CGAffineTransformMake(1, 0, tanf(5 * (CGFloat)M_PI / 180), 1, 0, 0);
        //取得系统字符并设置反射。
        UIFontDescriptor *desc = [ UIFontDescriptor fontDescriptorWithName :[UIFont mediumFontOfSize:15].fontName matrix :matrix];
        _rankLabel.font = [UIFont fontWithDescriptor:desc size :15];
    }
    return _rankLabel;
}

- (UIImageView *)portraitView {
    if (!_portraitView) {
        _portraitView = [[UIImageView alloc] init];
        _portraitView.layer.cornerRadius = 24;
        _portraitView.layer.masksToBounds = true;
        _portraitView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _portraitView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = [UIColor colorWithHexString:@"#333333"];
        _nameLabel.font = [UIFont mediumFontOfSize:15];
    }
    return _nameLabel;
}

- (LY_FansLevelView *)leveView {
    if (!_leveView) {
        _leveView = [[LY_FansLevelView alloc] init];
    }
    return _leveView;
}

- (UILabel *)intimacyLabel {
    if (!_intimacyLabel) {
        _intimacyLabel = [[UILabel alloc] init];
        _intimacyLabel.textColor = [UIColor colorWithHexString:@"#6A2CF7"];
        _intimacyLabel.font = [UIFont mediumFontOfSize:14];
    }
    return _intimacyLabel;
}

- (UIImageView *)heartImgView {
    if (!_heartImgView) {
        _heartImgView = [[UIImageView alloc] init];
        _heartImgView.image = [UIImage imageNamed:@"ic_zhibo_fensituan_qinmidu"];
    }
    return _heartImgView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        
        [self setupUIFrame];
        
//        self.rankLabel.textColor = [UIColor colorWithHexString:@"#FFD249"];
//        self.portraitView.backgroundColor = [UIColor redColor];
//        self.nameLabel.text = @"嘻嘻呼呼";
//        self.intimacyLabel.text = @"66";
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.rankLabel];
    [self addSubview:self.portraitView];
    [self addSubview:self.nameLabel];
    [self addSubview:self.leveView];
    [self addSubview:self.intimacyLabel];
    [self addSubview:self.heartImgView];
}

- (void)setupUIFrame {
    [self.rankLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(5);
        make.width.mas_equalTo(35);
        make.centerY.mas_equalTo(0);
    }];
    [self.portraitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.rankLabel.mas_trailing);
        make.size.mas_equalTo(48);
        make.centerY.mas_equalTo(0);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.portraitView);
        make.leading.mas_equalTo(self.portraitView.mas_trailing).offset(8);
        make.trailing.mas_lessThanOrEqualTo(self.intimacyLabel.mas_leading).offset(-10);
    }];
    [self.leveView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.portraitView).offset(-3);
        make.leading.mas_equalTo(self.portraitView.mas_trailing).offset(8);
        make.height.mas_equalTo(15);
    }];
    [self.heartImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(15, 12.5));
        make.centerY.mas_equalTo(0);
        make.trailing.mas_equalTo(-15);
    }];
    [self.intimacyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.trailing.mas_equalTo(self.heartImgView.mas_leading).offset(-10);
    }];
    [self.nameLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];
    [self.intimacyLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
}

- (void)setRank:(NSInteger)rank {
    _rank = rank;
    
    if (rank == 1) {
        self.rankLabel.textColor = [UIColor colorWithHexString:@"#FFD249"];
    } else if (rank == 2) {
        self.rankLabel.textColor = [UIColor colorWithHexString:@"#C6C6C6"];
    } else if (rank == 3) {
        self.rankLabel.textColor = [UIColor colorWithHexString:@"#E1A571"];
    } else {
        self.rankLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
    }
    
    if (rank > 50) {
        self.rankLabel.text = @"50+";
    } else {
        self.rankLabel.text = [NSString stringWithFormat:@"%ld", rank];
    }
}

- (void)setFans:(LY_Fans *)fans {
    _fans = fans;
    
    [self setRank:fans.rank];
    
    self.nameLabel.text = fans.nick_name;
    
    [self.leveView setLevel:fans.fan_level andName:fans.fan_male];
    
    self.intimacyLabel.text = fans.intimacy;
    
    
    [self.portraitView sd_setImageWithURL:[NSURL URLWithString:[LY_Api getCompleteResourceUrl:fans.portrait.url]] placeholderImage:[UIImage imageNamed:@"img_portrait_placeholder"]];
}

@end
