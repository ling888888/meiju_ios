//
//  LY_LiveFollowViewController.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_BaseTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LY_LiveFollowViewController : LY_BaseTableViewController

@property (nonatomic, copy) NSString * userId;


@end

NS_ASSUME_NONNULL_END
