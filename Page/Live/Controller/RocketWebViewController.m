//
//  RocketWebViewController.m
//  SimpleDate
//
//  Created by ling Hou on 2020/9/3.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "RocketWebViewController.h"
#import "UIView+RGSize.h"

@interface RocketWebViewController ()<WKNavigationDelegate>

@property (nonatomic, strong) WKWebView * webView;

@end

@implementation RocketWebViewController

- (WKWebView *)webView {
    if (!_webView) {
        _webView = [[WKWebView alloc]initWithFrame:CGRectMake(0, 0, kScreenW, (kScreenH < 667 ? 300: 550))];
        _webView.navigationDelegate = self;
        _webView.scrollView.scrollEnabled = NO;
        _webView.hidden = YES;
        _webView.opaque = NO;
        if (@available(iOS 11.0, *)) {
            _webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        [self.view addSubview:_webView];
    }
    return _webView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];

}

- (void)setUrlString:(NSString *)urlString{
    _urlString = urlString;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.webView.hidden = YES;
    NSURL *webURL = [NSURL URLWithString:_urlString];
    NSURLRequest *webRequest = [NSURLRequest requestWithURL:webURL];
    [self.webView loadRequest:webRequest];
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    NSString *requestStr = navigationAction.request.URL.absoluteString;
    if ([requestStr containsString:@"jianyue://jump=goback"]) {
//        [self showTips:@"点到了点到了点到了"];
        if ([self.type isEqualToString:@"live"]) {
            self.urlString = [NSString stringWithFormat:@"https://%@share.famy.ly/view/ar/luckyRocket.html?type=live&lang=%@",DEV?@"dev-":@"",[SPDCommonTool getFamyLanguage]];
        }else{
            self.urlString = [NSString stringWithFormat:@"https://%@share.famy.ly/view/ar/luckyRocket.html?type=chat&lang=%@",DEV?@"dev-":@"",[SPDCommonTool getFamyLanguage]];
        }
        if (@available(iOS 11.0, *)) {
            _webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }else if ([requestStr containsString:@"liveLuckyRank"]){
        if (@available(iOS 11.0, *)) {
            _webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentAlways;
        }
    }else if ([requestStr containsString:@"jianyue://jump=openGiftBox"]){
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LiveRoomShowGiftViewNotification" object:nil];
    }else if ([requestStr containsString:@"jianyue://get_user_detail"]){
        [self dismissViewControllerAnimated:YES completion:^{
            NSRange range = [requestStr rangeOfString:@"="];
            NSString *user_id = [requestStr substringFromIndex:range.location+1];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RocketToUserDetailNotification" object:nil userInfo:@{@"userId":user_id}];
        }];
    }
    decisionHandler(WKNavigationActionPolicyAllow);
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (![keyPath isEqualToString:@"estimatedProgress"]) {
        return;
    } else {
        if (self.webView.estimatedProgress >= 1.0f && object == self.webView) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.webView.hidden = NO;
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
        }
    }
}

- (void)dealloc
{
    [self.webView removeObserver:self forKeyPath:@"estimatedProgress"];
}


@end
