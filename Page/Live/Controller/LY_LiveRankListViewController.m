//
//  LY_LiveRankListViewController.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_LiveRankListViewController.h"
#import "LY_LiveRankTableViewCell.h"
#import "LY_HomePageViewController.h"

@interface LY_LiveRankListViewController ()

@property(nonatomic, strong) UILabel *emptyTipTextLabel;

@property(nonatomic, strong) UIButton *emptyTipGiftingBtn;

@end

@implementation LY_LiveRankListViewController

- (UILabel *)emptyTipTextLabel {
    if (!_emptyTipTextLabel) {
        _emptyTipTextLabel = [[UILabel alloc] init];
        _emptyTipTextLabel.textColor = [[UIColor colorWithHexString:@"#1A1A1A"] colorWithAlphaComponent:0.6];
        _emptyTipTextLabel.font = [UIFont mediumFontOfSize:15];
        _emptyTipTextLabel.text = @"还没有人上榜~".localized;
    }
    return _emptyTipTextLabel;
}

- (UIButton *)emptyTipGiftingBtn {
    if (!_emptyTipGiftingBtn) {
        _emptyTipGiftingBtn = [[UIButton alloc] init];
        [_emptyTipGiftingBtn setTitle:@"我要上榜".localized forState:UIControlStateNormal];
        _emptyTipGiftingBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 15);
        [_emptyTipGiftingBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _emptyTipGiftingBtn.titleLabel.font = [UIFont mediumFontOfSize:15];
        _emptyTipGiftingBtn.backgroundColor = [UIColor colorWithHexString:@"#00D4A0"];
        _emptyTipGiftingBtn.layer.cornerRadius = 15.5;
        _emptyTipGiftingBtn.layer.masksToBounds = true;
        [_emptyTipGiftingBtn addTarget:self action:@selector(emptyTipGiftingBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _emptyTipGiftingBtn;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    
    [self setupUIFrame];
    
    [self loadNetDataWith:RequestNetDataTypeRefresh];
    
    //[MobClick event:@"FM12_3_4_2"];
}

- (void)setupUI {
    self.tableView.tipText = @"还没有人上榜~".localized;
//    self.tableView.showEmptyTipView =
    self.tableView.rowHeight = 70;
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 75, 0, 0);
    self.tableView.separatorColor = [UIColor colorWithHexString:@"#EFEFEF"];
    [self.tableView registerClass:[LY_LiveRankTableViewCell class] forCellReuseIdentifier:@"RankIdentifier"];
    self.tableView.mj_footer = nil;
    
//    [self.view addSubview:self.emptyTipTextLabel];
//    [self.view addSubview:self.emptyTipGiftingBtn];
//
//    self.emptyTipTextLabel.hidden = true;
//    self.emptyTipGiftingBtn.hidden = true;
}

- (void)setupUIFrame {
//    [self.emptyTipTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.mas_equalTo(self.view);
//        make.centerY.mas_equalTo(self.view).offset(-20);
//    }];
//
//    [self.emptyTipGiftingBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.mas_equalTo(self.view);
//        make.top.mas_equalTo(self.emptyTipTextLabel.mas_bottom).offset(20);
//        make.height.mas_equalTo(31);
//    }];
}

- (void)loadNetDataWith:(RequestNetDataType)type {
    NSDictionary *params =@{@"rankType": self.type,
                            @"liveId": self.liveId,
    };
    [LY_Network getRequestWithURL:LY_Api.live_rank parameters:params success:^(id  _Nullable response) {
        NSArray *dataArray = response[@"list"];
        NSMutableArray *models = [NSMutableArray array];
        for (NSDictionary *data in dataArray) {
            LY_LiveAudience *audience = [LY_LiveAudience yy_modelWithDictionary:data];
            [models addObject:audience];
        }
        self.dataList = models;
        
        if (self.dataList.count == 0) {
            if (![self.entrance isEqualToString:@"mine"]) {
                self.tableView.tipButton = self.emptyTipGiftingBtn;
            }
            self.tableView.showEmptyTipView = true;
        } else {
            self.tableView.showEmptyTipView = false;
        }
        [self reloadData];
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LY_LiveRankTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RankIdentifier" forIndexPath:indexPath];
    
    LY_LiveAudience *liveAudience = self.dataList[indexPath.row];
    liveAudience.ranking = (int)indexPath.row + 1;
    
    cell.liveAudience = liveAudience;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    [self dismissViewControllerAnimated:true completion:nil];
    LY_LiveAudience *liveAudience = self.dataList[indexPath.row];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LiveRoomTapAvatar" object:nil userInfo:@{@"userId":liveAudience.userId}];

//    LY_HomePageViewController * detailTableViewController = [[LY_HomePageViewController alloc] initWithUserId:liveAudience.userId];
//    [[LY_Macro currentNavigationController] pushViewController:detailTableViewController animated:true];
}

- (void)emptyTipGiftingBtnAction {
    if ([self.delegate respondsToSelector:@selector(liveRankListViewControllerDidClick:)]) {
        [self.delegate liveRankListViewControllerDidClick:self.emptyTipGiftingBtn];
    }
}

@end
