//
//  LY_LiveAudienceListViewController.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_LiveAudienceListViewController.h"
#import "LY_LiveAudienceTableViewCell.h"
#import "LY_HomePageViewController.h"
#import "ZegoKitManager.h"
#import "LinkMicModel.h"

@interface LY_LiveAudienceListViewController ()<LY_LiveAudienceTableViewCellDelegate>

@property(nonatomic, strong) UILabel *onlineNumberLabel;

@end

@implementation LY_LiveAudienceListViewController

- (UILabel *)onlineNumberLabel {
    if (!_onlineNumberLabel) {
        _onlineNumberLabel = [[UILabel alloc] init];
        _onlineNumberLabel.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _onlineNumberLabel.font = [UIFont mediumFontOfSize:15];
    }
    return _onlineNumberLabel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    
    [self setupUIFrame];
    
    [self loadNetDataWith:RequestNetDataTypeRefresh];
    
    self.onlineNumberLabel.text = [NSString stringWithFormat:@"在线：%ld".localized, 0];
    
    //[MobClick event:@"FM12_3_4_1"];
}

- (void)setupUI {
    self.tableView.tipText = @"暂时没有人在线".localized;
    self.tableView.mj_footer = nil;
    self.tableView.rowHeight = 70;
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 75, 0, 0);
    [self.tableView registerClass:[LY_LiveAudienceTableViewCell class] forCellReuseIdentifier:@"AudienceIdentifier"];
    
    [self.view addSubview:self.onlineNumberLabel];
}

- (void)setupUIFrame {
    [self.onlineNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(15);
        make.top.mas_equalTo(10);
        make.width.mas_equalTo(150);
        make.height.mas_equalTo(20);
    }];

    [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(40, 0, 0, 0));
    }];
}

- (void)loadNetDataWith:(RequestNetDataType)type {
    [super loadNetDataWith:type];
    NSDictionary *params =@{@"liveId": self.liveId,
    };
    [LY_Network getRequestWithURL:LY_Api.live_audienceList parameters:params success:^(id  _Nullable response) {
        NSInteger onlineNumber = [response[@"onlineNum"] integerValue];
        self.onlineNumberLabel.text = [NSString stringWithFormat:@"在线：%ld".localized, onlineNumber];
        NSArray *dataArray = response[@"list"];
        NSMutableArray *models = [NSMutableArray array];
        for (NSDictionary *data in dataArray) {
            LY_LiveAudience *audience = [LY_LiveAudience yy_modelWithDictionary:data];
            [models addObject:audience];
        }
        if (type == RequestNetDataTypeRefresh) {
            self.dataList = models;
        } else {
            if (models.count == 0) {
                // 没有更多数据了
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                [self.dataList addObjectsFromArray:models];
            }
        }
        
        self.tableView.showEmptyTipView = self.dataList.count == 0 ? true : false;
        
        [self reloadData];
    } failture:^(NSError * _Nullable error) {
        [LY_HUD showToastTips:error.localizedDescription];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LY_LiveAudienceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AudienceIdentifier" forIndexPath:indexPath];
    cell.liveAudience = self.dataList[indexPath.row];
    cell.delegate = self;
    cell.inviteLinkMicButton.hidden = !_showInviteOnlineLinkMic;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    [self dismissViewControllerAnimated:true completion:nil];
    LY_LiveAudience *liveAudience = self.dataList[indexPath.row];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LiveRoomTapAvatar" object:nil userInfo:@{@"userId":liveAudience.userId}];
}

- (void)LiveAudienceTableViewCell:(LY_LiveAudienceTableViewCell *)cell didClickedInvite:(LY_LiveAudience *)liveAudience {
    if (!ZegoManager.linkMicUserInfoDic[liveAudience.userId]) {
        if (ZegoManager.linkMicUserInfoDic.count < 8) {
            [ZegoManager sendCustomCommandToUser:liveAudience.userId content:@"invite"];
            LinkMicModel *model = [LinkMicModel new];
            model._id = liveAudience.userId;
            model.avatar = liveAudience.portrait.url;
            model.portrait = liveAudience.portrait;
            model.nick_name = liveAudience.nickname;
            [ZegoManager.linkMicInvitingDic setValue:model forKey:liveAudience.userId];
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            [self showTips:SPDLocalizedString(@"最多可同时连麦8人")];
        }
    }
}



@end
