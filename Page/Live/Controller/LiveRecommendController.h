//
//  LiveRecommedController.h
//  SimpleDate
//
//  Created by ling Hou on 2020/3/18.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "BaseViewController.h"
#import "JXPagerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveRecommendController : BaseViewController <JXPagerViewListViewDelegate>

@end

NS_ASSUME_NONNULL_END
