//
//  SPDLaunchImageView.m
//  SimpleDate
//
//  Created by 李楠 on 2017/12/1.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import "SPDLaunchImageView.h"
#import "AppDelegate.h"

@interface SPDLaunchImageView ()

@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) NSTimer *timer;

@end

@implementation SPDLaunchImageView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.userInteractionEnabled = YES;
        self.contentMode = UIViewContentModeScaleAspectFill;
        
        self.timeLabel = [[UILabel alloc] init];
        self.timeLabel.backgroundColor = [UIColor colorWithWhite:0 alpha:0.25];
        self.timeLabel.font = [UIFont systemFontOfSize:16];
        self.timeLabel.textColor = [UIColor whiteColor];
        self.timeLabel.textAlignment = NSTextAlignmentCenter;
        self.timeLabel.clipsToBounds = YES;
        self.timeLabel.layer.cornerRadius = 30 / 2;
        [self addSubview:_timeLabel];
        [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.trailing.mas_equalTo(-25);
            make.top.mas_equalTo(40);
            make.width.and.height.mas_equalTo(30);
        }];
    }
    return self;
}

- (void)setTime:(NSInteger)time {
    _time = time;
    if (!self.timer) {
        self.timeLabel.text = [NSString stringWithFormat:@"%lds", _time];
        self.timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(countdown) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
        [self.timer fire];
    }
}

- (void)countdown {
    self.timeLabel.text = [NSString stringWithFormat:@"%lds", _time];
    if (self.time > 0) {
        self.time--;
    } else {
        [self.timer invalidate];
        self.timer = nil;
        [UIView animateWithDuration:0.25 delay:0.1 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.alpha = 0;
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            appDelegate.isShowingLaunchImage = NO;
            [appDelegate showBindAlert];
        }];
    }
}

@end
