//
//  LaunchScreenVideoView.m
//  SimpleDate
//
//  Created by 李楠 on 2020/5/21.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LaunchScreenVideoView.h"
#import "AppDelegate.h"

@implementation LaunchScreenVideoView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerItemDidPlayToEndTime:) name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"launch_screen_video" ofType:@"mp4"];
        NSURL *URL = [NSURL fileURLWithPath:path];
        AVPlayer *player = [AVPlayer playerWithURL:URL];
        AVPlayerLayer *playerLayer = [AVPlayerLayer playerLayerWithPlayer:player];
        playerLayer.frame = CGRectMake(0, 0, kScreenW, kScreenH);
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        [self.layer addSublayer:playerLayer];
        [player play];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)playerItemDidPlayToEndTime:(NSNotification *)notification {
    [self removeFromSuperview];
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.isShowingLaunchImage = NO;
    [appDelegate showBindAlert];
}

@end
