//
//  AppDelegate.m
//  SimpleDate
//
//  Created by 程龙军 on 15/12/4.
//  Copyright © 2015年WYB. All rights reserved.
//

#import "AppDelegate.h"
#import "SPDApiUser.h"
#import <RongIMKit/RongIMKit.h>
#import "SPDConversationViewController.h"
#import "UIColor+XXWAddition.h"
#import "GiftListModel.h"
#import "SPDWeekSignInView.h"
#import "PresentAnimView.h"
#import "SPDRCIMDataSource.h"
#import "ZegoKitManager.h"
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCall.h>
#import "SPDSignInView.h"
#import "FDAlertView.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "SPDLaunchImageView.h"
#import "LiveRoomViewController.h"
#import <ElvaChatServiceSDK/ElvaChatServiceSDK.h>
#import "WXManager.h"
//#import <UMCommon/UMCommon.h>
#import "IAPManager.h"

#ifdef NSFoundationVersionNumber_iOS_9_x_Max

#import <UserNotifications/UserNotifications.h> // 这里是iOS10需要用到的框架

#endif

//static NSString const *kAppKey = @"APP_KEY";
//static NSString const *kChannel = @"CHANNEL";
//static NSString const *kAPSForProduction = @"APS_FOR_PRODUCTION";

@interface AppDelegate ()

@property (nonatomic, strong) CTCallCenter *callCenter;
@property (nonatomic, strong) FDAlertView *alert;


@end

@implementation AppDelegate

static BOOL isRelogin;// true 代表已经弹出了 重新登录的框框
static BOOL isMaintencance;// true 代表已经弹出了 系统维护中的框框
static BOOL isBanned;// true 代表已经弹出了 系统维护中的框框
static BOOL isLowVersionError;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.isShowingLaunchImage = YES;
   
    [self configNavigationBar];

    
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = true;
    manager.enableAutoToolbar = false;
    manager.shouldResignOnTouchOutside = true;

    [SPDCommonTool setupGiftEffectsFolder:@"launch"];

    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    
    //facebook
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    // wx
    [WXApi registerApp:WXAppId universalLink:@"https://www.famy.ly/"];
    
//    [UMConfigure initWithAppkey:UMENG_KEY channel:@"App Store"];
    
    //初始化通知
    [self initNotifacation];

    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    
    if ([[SPDApiUser currentUser] isLoginToLocal]) {
        self.window.rootViewController = self.tab;
        [self connectRongCloudToken];
    }else {
        UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        self.window.rootViewController = [loginStoryboard instantiateInitialViewController];
    }
    [self.window makeKeyAndVisible];
    

    [self showLaunchImage];
    
    self.callCenter = [[CTCallCenter alloc] init];
    self.callCenter.callEventHandler = ^(CTCall* call) {
        if([call.callState isEqualToString:CTCallStateIncoming]) {
            [ZegoManager pauseAudio];
        } else if ([call.callState isEqualToString:CTCallStateDisconnected]) {
            [ZegoManager resumeAudio];
        }
    };
    
    [ECServiceSdk init:@"JIANYUE_app_2939fcb915e444f7ae5620d2936717e5" Domain:@"jianyue@aihelp.net" AppId:@"jianyue_platform_8f543c39-a046-4dfd-8a43-108c175c8491"];
    
    [[RCIMClient sharedRCIMClient] setLogLevel:RC_Log_Level_Verbose];
    [self redirectNSlogToDocumentFolder];

    [kIAPManager startObserve];

    return YES;
}

- (void)redirectNSlogToDocumentFolder {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];

    NSDate *currentDate = [NSDate date];
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"MMddHHmmss"];
    NSString *formattedDate = [dateformatter stringFromDate:currentDate];

    NSString *fileName = [NSString stringWithFormat:@"rc%@.log", formattedDate];
    NSString *logFilePath = [documentDirectory stringByAppendingPathComponent:fileName];

    freopen([logFilePath cStringUsingEncoding:NSASCIIStringEncoding], "a+", stdout);
    freopen([logFilePath cStringUsingEncoding:NSASCIIStringEncoding], "a+", stderr);
}

- (void)configNavigationBar {
    UINavigationBar *navigationBar = [UINavigationBar appearance];
    UIImage *backButtonImage = [[UIImage imageNamed:@"ic_arrow_left"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [navigationBar setBackIndicatorImage:backButtonImage];
    [navigationBar setBackIndicatorTransitionMaskImage:backButtonImage];
}

- (void)connectRongCloudToken {
    SPDApiUser* user = [SPDApiUser currentUser];
     [user initWithDefaultsData];
     if ([SPDCommonTool isEmpty:user.lang]) {
         user.lang = @"ar";
         [user saveToDefault];
     }
     //设置上线
     [RequestUtils setUserStauesOnline:YES];
     [[SPDRCIMDataSource shareInstance] logIn];
}


- (void)logOut {
    
    isRelogin = NO;
    isMaintencance = NO;
    isBanned = NO;
    
    [ZegoManager leaveRoom];

    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kUserDataIdentifier];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:MYINVITECOUNT];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:ISVIP];
    [[NSUserDefaults standardUserDefaults] synchronize];

    self.tab = nil;
    UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    self.window.rootViewController = [loginStoryboard instantiateInitialViewController];
    
    [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"logout" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
        NSLog(@"登出成功");
    } bFailure:^(id  _Nullable failure) {
        [SPDCommonTool showWindowToast:@"failure"];
    }];
    
    [[RCIMClient sharedRCIMClient] quitChatRoom:GLOBALCHATROOM success:^{
        
    } error:^(RCErrorCode status) {
        
    }];
    [SPDRCIMDataSource shareInstance].lastGlobalMessage = nil;
    [[RCIM sharedRCIM] logout];
    
    [RequestUtils setUserStauesOnline:NO];
    
    [[LY_User currentUser] clear];
    
}

- (void)initNotifacation {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logOut) name:@"LOGOUT" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLoginInvalid) name:@"LoginInvalid" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLowVersionError) name:@"LOWVERSION_ERROR" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleServerMaintencance) name:@"SERVER_MAINTENANCE" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(exitApplication) name:@"EXIT" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dealBanned:) name:@"BANNED" object:nil];

}

- (void)handleLoginInvalid
{
    if (isRelogin) {
        return;
    }
    isRelogin = YES;
    [self presentMessageAlertWithTitle:SPDStringWithKey(@"账号未登录或登录已失效，请重新登录", nil) message:@"" actionTitle:SPDStringWithKey(@"好", nil) actionHandler:^(UIAlertAction * _Nonnull action) {
        [self logOut];
    }];
}

- (void)handleServerMaintencance {
    if (isMaintencance) {
        return;
    }
    isMaintencance = YES;
    //清空缓存
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [self showTips:@"系统维护中哦～请稍后重试！".localized];
}

- (void)exitApplication {
    //系统出错 退出
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        //清空缓存
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        [self presentMessageAlertWithTitle:SPDStringWithKey(@"系统错误，请重试", nil) message:@"" actionTitle:SPDStringWithKey(@"确定", nil) actionHandler:^(UIAlertAction * _Nonnull action) {
            
        }];
    });
}

//版本更新
- (void)handleLowVersionError {
    if (isLowVersionError) {
        return;
    }
    isLowVersionError = YES;
    [self presentMessageAlertWithTitle:SPDStringWithKey(@"客户端版本过低，请升级客户端后重试", nil) message:@"" actionTitle:SPDStringWithKey(@"确定", nil) actionHandler:^(UIAlertAction * _Nonnull action) {
        isLowVersionError = NO;
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://share.famy.ly/download.html"]];
    }];

}

- (void)dealBanned :(NSNotification *)notify {
    if (isBanned) {
        return;
    }
    isBanned = YES;
    NSString *msgStr = notify.userInfo[@"msg"];
    NSString * msg ;
    if ([msgStr containsString:@"-"]) {
        NSArray * array = [msgStr componentsSeparatedByString:@"-"];
        if (array.count > 1 ) {
            NSString * alter = array[0];
            NSString *time = array[1];
            NSString *str1=[SPDCommonTool getDateStringWithDate:[NSDate dateWithTimeIntervalSince1970:[time integerValue]/1000] DateFormat:@"yyyy-MM-dd HH:mm:ss"];
            msg = [NSString stringWithFormat:@"%@%@\n%@%@",SPDStringWithKey(@"封禁原因：", nil),alter,SPDStringWithKey(@"解禁时间：", nil),str1];
        }else{
            msg = SPDStringWithKey(@"您的账号被锁定", nil);
        }
    }else{
        msg = SPDStringWithKey(@"您的账号被锁定", nil);
    }
    
    [self presentMessageAlertWithTitle:SPDStringWithKey(@"尊敬的用户您的账号已被封禁", nil) message:SPDStringWithKey(@"", nil) actionTitle:SPDStringWithKey(@"", nil) actionHandler:^(UIAlertAction * _Nonnull action) {
        [self logOut];
    }];
    
}


- (void)loginSuccess
{
    // 解决登陆页面 导航栏还在后面的问题 
    self.window.rootViewController = nil;
    for (UIView * view in self.window.subviews) {
        [view removeFromSuperview];
    }
    self.window.rootViewController = self.tab;
    [self.window addSubview:self.tab.view];
    [self connectRongCloudToken];
    [self showBindAlert];

}


#pragma mark - sigin
//判断每天第一次登录 -sign in
- (void)judageEverdayFristLogin {
    if (!self.isShowingLaunchImage) {
        if (![[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@%@",EVERYDAY_FRIST_LOGIN,[SPDApiUser currentUser].userId]]) {
            [[NSUserDefaults standardUserDefaults]setObject:[SPDCommonTool getCurrentTimes] forKey:[NSString stringWithFormat:@"%@%@",EVERYDAY_FRIST_LOGIN,[SPDApiUser currentUser].userId]];
            [self cheakWeekSignin];
        }else{
            NSString * current_date = [SPDCommonTool getCurrentTimes];
            NSString * usrDefault_date = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@%@",EVERYDAY_FRIST_LOGIN,[SPDApiUser currentUser].userId]];
            if (![usrDefault_date isEqualToString:current_date]) {
                [[NSUserDefaults standardUserDefaults]setObject:[SPDCommonTool getCurrentTimes] forKey:[NSString stringWithFormat:@"%@%@",EVERYDAY_FRIST_LOGIN,[SPDApiUser currentUser].userId]];
                [self cheakWeekSignin];
            }else{
                //check level
                [[SPDCommonTool shareTool]checkExpToWhetherToUpgrage];
            }
        }
    }
}

// 检查周签到
- (void)cheakWeekSignin {
    NSMutableDictionary * dict = [NSMutableDictionary new];
    [dict setObject:[SPDApiUser currentUser].userId forKey:@"user_id"];
    [RequestUtils commonGetUrlRequestUtils:dict bURL:@"sign.in.week" RequestType:SPDRankUrl bAnimated:NO bAnimatedViewController:nil bSuccessDoSomething:^(id  _Nullable suceess) {
        NSString * check = suceess[@"check"];
        NSArray * list = suceess[@"list"];
        if ([check isEqualToString:@"false"] && list.count > 0 ) {
            SPDWeekSignInView * view = [[[NSBundle mainBundle] loadNibNamed:@"SPDWeekSignInView" owner:self options:nil]lastObject];
            view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
            view.dataDict = [suceess copy];
            [[UIApplication sharedApplication].keyWindow addSubview:view];
        }
    } bFailure:^(id  _Nullable failure) {
        [self showTips:failure[@"msg"]];
    }];
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    //设置未读消息数目
    int count =[RCIMClient sharedRCIMClient].getTotalUnreadCount;
    application.applicationIconBadgeNumber=count;
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
    //facebook -
    [FBSDKAppEvents activateApp];

    if ([[SPDApiUser currentUser] isLoginToLocal]) {
        //everyday sign in
        [self judageEverdayFristLogin];
    }
    
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
}

- (void)applicationWillTerminate:(UIApplication *)application{
    
    [[RCIMClient sharedRCIMClient] disconnect:YES];
    if (isVideoStatus) {
        NSUserDefaults *userDe=[NSUserDefaults standardUserDefaults];
        NSTimeInterval time=[[NSDate date] timeIntervalSince1970];
        NSString *timeStr=[NSString stringWithFormat:@"%f",time];
        [userDe setObject:timeStr forKey:@"errorTime"];
    }
    [ZegoManager leaveRoom];
}


#pragma mark JPush
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    //融云Token
    [[RCIMClient sharedRCIMClient]setStatisticServer:StatisticServer];

    NSString *rongyunDeviceToken =
        [[[[deviceToken description] stringByReplacingOccurrencesOfString:@"<"
                                                               withString:@""]
            stringByReplacingOccurrencesOfString:@">"
                                      withString:@""]
            stringByReplacingOccurrencesOfString:@" "
                                      withString:@""];
    NSLog(@"融云TOKEN=%@", rongyunDeviceToken);
    [[RCIMClient sharedRCIMClient] setDeviceToken:rongyunDeviceToken];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    
    //统计融云打开率
    [[RCIMClient sharedRCIMClient] recordRemoteNotificationEvent:userInfo];
    
    //跳转到融云聊天页面
    [self pushToChat:userInfo];
}



// 程序关闭后, 通过点击推送弹出的通知
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    
    NSDictionary * userInfo = response.notification.request.content.userInfo;
        //统计融云打开率
    [[RCIMClient sharedRCIMClient] recordRemoteNotificationEvent:userInfo];
    //跳转到融云聊天页面
    [self pushToChat:userInfo];

    completionHandler();  // 系统要求执行这个方法
}


- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
   

    //统计融云打开率
    [[RCIMClient sharedRCIMClient] recordLocalNotificationEvent:notification];


   //跳转到融云聊天页面
    [self pushToChat:notification.userInfo];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"did Fail To Register For Remote Notifications With Error: %@", error);
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    if ([sourceApplication containsString:@"wx"]) {
        return [WXApi handleOpenURL:url delegate:[WXManager sharedInstance]];
    }else{
        return  [[FBSDKApplicationDelegate sharedInstance] application:application
                                                               openURL:url
                                                     sourceApplication:sourceApplication
                                                            annotation:annotation
                 ];;
    }
}

//iOS 9 及以上
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url  options:(NSDictionary *)options {
    if ([url.absoluteString containsString:@"wx"]) {
        return [WXApi handleOpenURL:url delegate:[WXManager sharedInstance]];
    }else{
        BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:app openURL:url options:options];
        return handled;
    }
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}

- (LY_TabBarController *)tab {
    if (_tab == nil) {
        _tab = [[LY_TabBarController alloc] init];
    }
    return _tab;
}


//融云推送 跳转到响应聊天ID
- (void)pushToChat:(NSDictionary *)userInfo{
    NSDictionary *rcInfo = [userInfo valueForKey:@"rc"];
    if (rcInfo != nil || rcInfo != [NSNull class]) {
        NSString *pushUserId = [rcInfo valueForKey:@"fId"];
        if (pushUserId != nil) {
            SPDConversationViewController* chat = [[SPDConversationViewController alloc] initWithConversationType:ConversationType_PRIVATE targetId:pushUserId];
            chat.hidesBottomBarWhenPushed=YES;
            [self.tab.selectedViewController pushViewController:chat animated:YES];
        }
    }
}

- (void)showLaunchImage {
    UIImage *image = [SPDCommonTool getLanchImage];
    if (image) {
        SPDLaunchImageView *launchImageView = [[SPDLaunchImageView alloc] initWithFrame:self.window.bounds];
        launchImageView.image = image;
        launchImageView.time = 3;
        [self.window addSubview:launchImageView];
    } else {
        [self showBindAlert];
    }
    if ([[SPDApiUser currentUser] isLoginToLocal]) {
        [SPDCommonTool requestAnimationImagesWithType:@"launch"];
    }
}

- (void)showBindAlert {
    if ([[SPDApiUser currentUser] isLoginToLocal] && !self.isShowingActivity) {
        [RequestUtils commonGetRequestUtils:[NSMutableDictionary new] bURL:@"account.property" bAnimated:NO bAnimatedViewController:[BaseViewController new] bSuccessDoSomething:^(id  _Nullable suceess) {
            if ([suceess[@"is_visitors"] boolValue]) {
                NSString *message = SPDStringWithKey(@"为保障您的账户安全，请及时到账号安全绑定手机号／第三方账号", nil);
                [SPDCommonTool presentAutoController:self.tab title:@"" titleColor:@"" titleFontSize:0 messageString:message messageColor:nil messageFontSize:0 cancelTitle:SPDStringWithKey(@"再想想", nil) cancelAction:^{
                    self.isShowingLaunchImage = NO;
                    [self judageEverdayFristLogin];
                } confirmTitle:SPDStringWithKey(@"去绑定", nil) confirmAction:^{
                    UIViewController *securityViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"securityViewController"];
                    [self.tab.selectedViewController pushViewController:securityViewController animated:YES];
                    self.isShowingLaunchImage = NO;
                    [self judageEverdayFristLogin];
                }];
            } else {
                self.isShowingLaunchImage = NO;
                [self judageEverdayFristLogin];
            }
        } bFailure:^(id  _Nullable failure) {
            self.isShowingLaunchImage = NO;
            [self judageEverdayFristLogin];
        }];
    } else {
        self.isShowingLaunchImage = NO;
    }
}

- (BOOL)application:(UIApplication *)application shouldAllowExtensionPointIdentifier:(NSString *)extensionPointIdentifier {
    if([[SPDCommonTool getWindowTopViewController] isKindOfClass:[LiveRoomViewController class]]){
        return NO;
    }// 在直播页面会有点问题所以 有时间再改一下
    return YES;
}


@end
