//
//  main.m
//  SimpleDate
//
//  Created by 程龙军 on 15/12/4.
//  Copyright © 2015年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char *argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
