//
//  AppDelegate.h
//  SimpleDate
//
//  Created by 程龙军 on 15/12/4.
//  Copyright © 2015年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LY_TabBarController.h"

typedef void (^AlertAction)();

@interface AppDelegate : UIResponder <UIApplicationDelegate,JYCallSessionDelegate>

@property (strong, nonatomic) UIWindow *window;
//@property (nonatomic, strong) SPDTabBarController *tabBarController;
@property (nonatomic, strong) LY_TabBarController *tab;
@property (nonatomic, assign) BOOL isShowingLaunchImage;
@property (nonatomic, assign) BOOL isShowingActivity;
@property (nonatomic, assign) BOOL hideWIFI4GTips; // 在直播间里
- (void)judageEverdayFristLogin;
- (void)loginSuccess;
- (void)showBindAlert;

@end
