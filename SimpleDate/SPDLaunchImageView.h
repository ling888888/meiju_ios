//
//  SPDLaunchImageView.h
//  SimpleDate
//
//  Created by 李楠 on 2017/12/1.
//  Copyright © 2017年 WYB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPDLaunchImageView : UIImageView

@property (nonatomic, assign) NSInteger time;

@end
