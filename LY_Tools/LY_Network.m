//
//  LY_Network.m
//  SimpleDate
//
//  Created by jianyue on 2020/2/28.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_Network.h"
#import "LY_HTTP.h"
#import "UIDevice+LY.h"

@interface LY_Network ()


@property(nonatomic, strong)AFHTTPSessionManager *sessionManager;

@end

static LY_Network *manager = nil;

@implementation LY_Network

+ (instancetype)shareManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
    });
    return manager;
}

- (AFHTTPSessionManager *) sessionManager {
    if (!_sessionManager) {
        _sessionManager = [AFHTTPSessionManager manager];
        _sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"image/png", @"application/x-www-form-urlencoded", @"text/html", nil];
        _sessionManager.requestSerializer.HTTPMethodsEncodingParametersInURI = [NSSet setWithObjects:@"GET", @"HEAD", nil];
        _sessionManager.securityPolicy.allowInvalidCertificates = YES;
        _sessionManager.securityPolicy = [AFSecurityPolicy defaultPolicy];
        
        //deal code = 4 之后请求某个api 一直code = 4
        [_sessionManager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    }
    return _sessionManager;
}

+ (void)getRequestWithURL:(NSString * _Nonnull)URLString parameters:(NSDictionary<NSString *, id> * _Nullable)parameters success:(nullable void (^)(id _Nullable response))success failture:(nullable void (^)(NSError  * _Nullable error))failure {
    NSURLSessionDataTask *dataTask = [self dataTaskWithHTTPMethod:@"GET" URLString:URLString parameters:parameters headers:nil success:success failture:failure];
    [dataTask resume];
}

///设置post请求
+ (void)postRequestWithURL:(NSString * _Nonnull)URLString parameters:(NSDictionary<NSString *, id> * _Nullable)parameters success:(nullable void (^)(id _Nullable response))success failture:(nullable void (^)(NSError  * _Nullable error))failure {
    NSURLSessionDataTask *dataTask = [self dataTaskWithHTTPMethod:@"POST" URLString:URLString parameters:parameters headers:nil success:success failture:failure];
    [dataTask resume];
}

+ (void)postImageRequestWithURL:(NSString * _Nonnull)URLString parameters:(NSDictionary<NSString *, id> * _Nullable)parameters imageParametersName:(NSString *)imageParametersName images:(NSArray<UIImage *> * _Nullable)images success:(nullable void (^)(id _Nullable response))success failture:(nullable void (^)(NSError  * _Nullable error))failure {
    
    NSURLSessionDataTask *dataTask = [self POST:URLString parameters:parameters headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData> _Nonnull formData) {
        // 图片转为紧致文件并压缩
        for (int i = 0; i < images.count; i ++) {
            UIImage *value = images[i];
            if (value == nil) {
                return;
            }
            // 图片转二进制文件并压缩
            NSData *imgData = UIImageJPEGRepresentation(value, 0.5);
            NSString *str = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]];
            // 拼接文件名
            NSString *fileName = [NSString stringWithFormat:@"%@%d%@", str, i, @".png"];
            // 以文件流的格式上传
            [formData appendPartWithFileData:imgData name:imageParametersName fileName:fileName mimeType:@"image/png"];
        }
    } progress:nil success:success failture:failure];
                                      //POST:URLString parameters:parameters headers:nil constructingBodyWithBlock:constructingBody progress:nil success:success failture:failure];
    [dataTask resume];
}

///设置post请求，上传图片或视频资源
+ (void)postImageRequestWithURL:(NSString * _Nonnull)URLString
                     parameters:(NSDictionary<NSString *, id> * _Nullable)parameters
               constructingBody:(nullable void (^)(id <AFMultipartFormData> formData))constructingBody
                        success:(nullable void (^)(id _Nullable response))success
                       failture:(nullable void (^)(NSError  * _Nullable error))failure {
    
    NSURLSessionDataTask *dataTask = [self POST:URLString parameters:parameters headers:nil constructingBodyWithBlock:constructingBody progress:nil success:success failture:failure];
    [dataTask resume];
}

+ (NSURLSessionDataTask *)POST:(NSString *)URLString
                    parameters:(nullable id)parameters
                       headers:(nullable NSDictionary<NSString *,NSString *> *)headers
     constructingBodyWithBlock:(nullable void (^)(id<AFMultipartFormData> _Nonnull))block
                      progress:(nullable void (^)(NSProgress * _Nonnull))uploadProgress
                       success:(nullable void (^)(id _Nullable response))success
                       failture:(nullable void (^)(NSError  * _Nullable error))failure {
    // 拼接UDID到参数中
    NSMutableDictionary *newParams = [NSMutableDictionary dictionaryWithDictionary:parameters];
    [newParams setValue:[UIDevice currentDevice].identifier forKey:@"UDID"];
    
    AFHTTPSessionManager *sessionManager = [LY_Network shareManager].sessionManager;
    sessionManager.requestSerializer = [LY_HTTP getHeaderWith:parameters];
    
    __block NSURLSessionDataTask *dataTask = nil;
    dataTask = [sessionManager POST:URLString
              parameters:parameters
                 headers:headers constructingBodyWithBlock:block progress:uploadProgress success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse * response = (NSHTTPURLResponse *)task.response;
        NSString * set_cookies = response.allHeaderFields[@"Set-Cookie"];
        if (set_cookies && ![LY_Api isJavaServerWithUrl:URLString]) {
            [[NSUserDefaults standardUserDefaults] setObject:set_cookies forKey:@"Set-Cookie"];
        }
        
        ResponseResult *result = [LY_HTTP analyzingResponseFrom:responseObject];
        if (result.statusCode == 0) {
            // 操作正确
            success(result.value);
        } else {
            // 操作异常
            NSDictionary *userInfo = @{
                NSLocalizedDescriptionKey:result.message
            };
            
            NSError *error = [[NSError alloc] initWithDomain:result.message code:result.statusCode userInfo:userInfo];
            failure(error);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        failure(error);
    }];
    
    return dataTask;
}



+ (NSURLSessionDataTask *)dataTaskWithHTTPMethod:(NSString *)method
                                       URLString:(NSString *)URLString
                                      parameters:(nullable id)parameters
                                         headers:(nullable NSDictionary <NSString *, NSString *> *)headers
                                         success:(nullable void (^)(id _Nullable response))success
                                        failture:(nullable void (^)(NSError  * _Nullable error))failure {
    // 拼接UDID到参数中
    NSMutableDictionary *newParams = [NSMutableDictionary dictionaryWithDictionary:parameters];
    [newParams setValue:[UIDevice currentDevice].identifier forKey:@"UDID"];
    
    AFHTTPSessionManager *sessionManager = [LY_Network shareManager].sessionManager;
    sessionManager.requestSerializer = [LY_HTTP getHeaderWith:parameters];
    
    __block NSURLSessionDataTask *dataTask = nil;
    dataTask = [sessionManager dataTaskWithHTTPMethod:method
                                            URLString:URLString
                                           parameters:parameters
                                              headers:headers
                                       uploadProgress:nil
                                     downloadProgress:nil
                                              success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse * response = (NSHTTPURLResponse *)task.response;
        NSString * set_cookies = response.allHeaderFields[@"Set-Cookie"];
        if (set_cookies && ![LY_Api isJavaServerWithUrl:URLString]) {
            [[NSUserDefaults standardUserDefaults] setObject:set_cookies forKey:@"Set-Cookie"];
        }
        ResponseResult *result = [LY_HTTP analyzingResponseFrom:responseObject];
        if (result.statusCode == 0) {
            // 操作正确
            success(result.value);
        } else {
            // 操作异常
            NSDictionary *userInfo = @{
                NSLocalizedDescriptionKey:result.message
            };
            NSError *error = [[NSError alloc] initWithDomain:result.message code:result.statusCode userInfo:userInfo];
            failure(error);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        failure(error);
    }];
    
    return dataTask;
}

@end
