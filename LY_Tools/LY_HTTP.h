//
//  LY_HTTP.h
//  SimpleDate
//
//  Created by jianyue on 2020/2/28.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <Network/Network.h>

NS_ASSUME_NONNULL_BEGIN

@interface ResponseResult : NSObject

// 请求返回有效数据（["data"]的数据）
@property(nonatomic, strong) id value;

// 请求返回提示信息
@property(nonatomic, copy) NSString *message;

// 请求状态码
@property(nonatomic, assign) NSInteger statusCode;

@end

@interface LY_HTTP : NSObject

+ (AFHTTPRequestSerializer *)getHeaderWith:(NSDictionary *)params;
//responseObject

+ (ResponseResult *)analyzingResponseFrom:(id  _Nullable)responseObject;

//AFHTTPRequestSerializer

@end

NS_ASSUME_NONNULL_END
