//
//  LY_HUD.m
//  SimpleDate
//
//  Created by jianyue on 2020/3/10.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_HUD.h"

@implementation LY_HUD

+ (void)showToastTips:(NSString *)tips {
    [MBProgressHUD hideAllHUDsForView:[UIApplication sharedApplication].keyWindow animated:YES];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    hud.userInteractionEnabled = NO;
    hud.mode = MBProgressHUDModeText;
    hud.margin = 10.0f;
    hud.yOffset = 15.0f;
    hud.opacity = 0.5f;
    hud.removeFromSuperViewOnHide = YES;
    hud.detailsLabelText = tips;
    hud.detailsLabelFont = [UIFont systemFontOfSize:15];
    [hud hide:YES afterDelay:2.5];
}

+ (void)dismiss {
    [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:true];
}

+ (void)show {
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:true];
}
/*
 
 */

@end
