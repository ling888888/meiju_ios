//
//  LY_HTTP.m
//  SimpleDate
//
//  Created by jianyue on 2020/2/28.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_HTTP.h"

@implementation ResponseResult


@end

@implementation LY_HTTP

+ (AFHTTPRequestSerializer *)getHeaderWith:(NSDictionary *)params {
    
    AFHTTPRequestSerializer *requestSerializer = [[AFHTTPRequestSerializer alloc] init];
    //
    NSString *userAgent = [NSString stringWithFormat:@"Famy/%@ (%@; iOS %@)",LOCAL_VERSION_SHORT,DEVICE_NAME,DEVICE_VERSION_NAME];
    [requestSerializer setValue:userAgent forHTTPHeaderField:@"User-Agent"];
    
    if ([SPDCommonTool getFamyLanguage]) {
        NSString * str = [SPDCommonTool getFamyLanguage];
        NSArray * array = [str componentsSeparatedByString:@"-"];
        if (array.count >0 ) {
            [requestSerializer setValue:array[0] forHTTPHeaderField:@"Accept-Language"];
        }
    }
    
    
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"]) {
        NSString * cookie_strings =[[NSUserDefaults standardUserDefaults] objectForKey:@"Set-Cookie"];
        [requestSerializer setValue:cookie_strings forHTTPHeaderField:@"Cookie"];
    }
    
    return requestSerializer;
}

+ (ResponseResult *)analyzingResponseFrom:(id  _Nullable)responseObject {
    
    ResponseResult * result = [[ResponseResult alloc] init];
    result.statusCode = [responseObject[@"code"] integerValue];
    result.message = responseObject[@"msg"];
    result.value = responseObject[@"data"];
    
    // 容错处理
    if (result.message == nil) {
        result.message = @"";
    }
    
    [self examineStatusCode:result.statusCode];
    
    return result;
}

/// 检查请求结果状态是否特殊处理
/// @param statusCode 状态a码
+ (void)examineStatusCode:(NSInteger)statusCode {
    switch (statusCode) {
        case 2: {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginInvalid" object:nil];
            break;
        }
        case 3: {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LOWVERSION_ERROR" object:nil];
            break;
        }
        case 4: {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SERVER_MAINTENANCE" object:nil];
            break;
        }
        case 7:{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BANNED" object:nil userInfo:nil];
            break;
        }
        case 5:{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"EXIT" object:nil];
            break;
        }
        default:
            break;
    }
}




@end
