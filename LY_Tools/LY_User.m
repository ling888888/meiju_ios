//
//  LY_User.m
//  SimpleDate
//
//  Created by 杨宝龙 on 2020/5/20.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import "LY_User.h"

@interface LY_User () <NSCoding>

@end

static LY_User *shareInstance = nil;
static dispatch_once_t onceToken;

@implementation LY_User

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"userId":@"_id",
             @"portrait":@"avatar",
             @"headwearWebp":@[@"headwearWebp", @"packages.headwear.headwearWebp"],
             @"nickname":@"nick_name",
             @"personalDesc":@"personal_desc",
             @"ryToken":@"ry_token",
             };
}

- (void)encodeWithCoder:(nonnull NSCoder *)coder {
    [coder encodeObject:self.userId forKey:@"userId"];
    [coder encodeObject:self.nickname forKey:@"nickname"];
    [coder encodeObject:self.portrait forKey:@"portrait"];
    [coder encodeObject:self.headwearWebp forKey:@"headwearWebp"];
    [coder encodeInteger:self.age forKey:@"age"];
    [coder encodeInteger:self.level forKey:@"level"];
    [coder encodeObject:self.personalDesc forKey:@"personalDesc"];
    [coder encodeObject:self.gender forKey:@"gender"];
    [coder encodeObject:self.birthday forKey:@"birthday"];
    [coder encodeObject:self.lang forKey:@"lang"];
    [coder encodeObject:self.ryToken forKey:@"ryToken"];
}

- (nullable instancetype)initWithCoder:(nonnull NSCoder *)coder {
    self = [super init];
    if (self) {
        self.userId = [coder decodeObjectForKey:@"userId"];
        self.nickname = [coder decodeObjectForKey:@"nickname"];
        self.portrait = [coder decodeObjectForKey:@"portrait"];
        self.headwearWebp = [coder decodeObjectForKey:@"headwearWebp"];
        self.age = [coder decodeIntegerForKey:@"age"];
        self.level = [coder decodeIntegerForKey:@"level"];
        self.personalDesc = [coder decodeObjectForKey:@"personalDesc"];
        self.gender = [coder decodeObjectForKey:@"gender"];
        self.birthday = [coder decodeObjectForKey:@"birthday"];
        self.lang = [coder decodeObjectForKey:@"lang"];
        self.ryToken = [coder decodeObjectForKey:@"ryToken"];
    }
    return self;
}

+ (BOOL)isLogin {
    return [LY_User currentUser].userId.notEmpty;
}

+ (LY_User *)currentUser {
    dispatch_once(&onceToken, ^{
        NSData *archiveData = [[NSUserDefaults standardUserDefaults] objectForKey:@"userData"];
        shareInstance = (LY_User *)[NSKeyedUnarchiver unarchiveObjectWithData:archiveData];
    });
    return shareInstance;
}

- (void)save {
    //
    NSData *archiveData = [NSKeyedArchiver archivedDataWithRootObject:self];
    [[NSUserDefaults standardUserDefaults] setObject:archiveData forKey:@"userData"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)clear {
    // 移除本地缓存
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userData"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    // 清空当前单例对象
    shareInstance = nil;
    onceToken = 0;
}

@end
