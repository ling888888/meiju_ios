//
//  LY_HUD.h
//  SimpleDate
//
//  Created by jianyue on 2020/3/10.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_HUD : NSObject

+ (void)showToastTips:(NSString *)tips;

+ (void)dismiss;

+ (void)show;



@end

NS_ASSUME_NONNULL_END
