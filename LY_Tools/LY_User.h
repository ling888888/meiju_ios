//
//  LY_User.h
//  SimpleDate
//
//  Created by 杨宝龙 on 2020/5/20.
//  Copyright © 2020 简约互动科技（北京）有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_User : NSObject

/// 用户ID
@property(nonatomic, copy) NSString *userId;

/// 昵称
@property(nonatomic, copy) NSString *nickname;

/// 头像
@property(nonatomic, copy) NSString *portrait;

/// 头饰
@property(nonatomic, copy) NSString *headwearWebp;

/// 年龄
@property(nonatomic, assign) NSInteger age;

/// 等级
@property(nonatomic, assign) NSInteger level;

/// 个人简介
@property(nonatomic, copy) NSString *personalDesc;

/// 性别 - male、female
@property(nonatomic, copy) NSString *gender;

/// 生日 - 1990-01-01
@property(nonatomic, copy) NSString *birthday;

/// 语言区 - ar
@property(nonatomic, assign) NSString *lang;

/// 融云Token
@property(nonatomic, copy) NSString *ryToken;


@property(class, nonatomic, readonly) LY_User *currentUser;

@property(class, nonatomic, assign, readonly) BOOL isLogin;

/// 更新保存的用户信息
- (void)save;

/// 清除本地用户信息
- (void)clear;

@end

NS_ASSUME_NONNULL_END
