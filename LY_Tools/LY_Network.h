//
//  LY_Network.h
//  SimpleDate
//
//  Created by jianyue on 2020/2/28.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_Network : NSObject

///设置get请求
+ (void)getRequestWithURL:(NSString * _Nonnull)URLString parameters:(NSDictionary<NSString *, id> * _Nullable)parameters success:(nullable void (^)(id _Nullable response))success failture:(nullable void (^)(NSError  * _Nullable error))failure;

///设置post请求
+ (void)postRequestWithURL:(NSString * _Nonnull)URLString parameters:(NSDictionary<NSString *, id> * _Nullable)parameters success:(nullable void (^)(id _Nullable response))success failture:(nullable void (^)(NSError  * _Nullable error))failure;

///设置post请求，上传图片
+ (void)postImageRequestWithURL:(NSString * _Nonnull)URLString parameters:(NSDictionary<NSString *, id> * _Nullable)parameters imageParametersName:(NSString *)imageParametersName images:(NSArray<UIImage *> * _Nullable)images success:(nullable void (^)(id _Nullable response))success failture:(nullable void (^)(NSError  * _Nullable error))failure;

///设置post请求，上传图片或视频资源
+ (void)postImageRequestWithURL:(NSString * _Nonnull)URLString
                     parameters:(NSDictionary<NSString *, id> * _Nullable)parameters
               constructingBody:(nullable void (^)(id <AFMultipartFormData> formData))constructingBody
                        success:(nullable void (^)(id _Nullable response))success
                       failture:(nullable void (^)(NSError  * _Nullable error))failure;



@end

NS_ASSUME_NONNULL_END
