//
//  LY_Macro.m
//  SimpleDate
//
//  Created by jianyue on 2020/2/28.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_Macro.h"

@implementation LY_Macro

/// 当前应用名称
+ (NSString *)appName {
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
}

/// 是否是生产环境
+ (BOOL)isDisEnvironment {
    return DIS;
}

///
+ (UINavigationController *)currentNavigationController {
    UITabBarController *tabBarC = [UIApplication sharedApplication].keyWindow.rootViewController;
    return tabBarC.selectedViewController;
}
//
////public var rootViewController: UIViewController? {
////    return UIApplication.shared.keyWindow?.rootViewController
////}
//
//public var currentNavigationController: UINavigationController? {
//
//    if let tabBarController = UIApplication.shared.keyWindow?.rootViewController as? MainTabBarController {
//        if let navi = tabBarController.selectedViewController as? UINavigationController {
//            return navi
//        } else {
//            return nil
//        }
//    }
//
//    if let navi = UIApplication.shared.keyWindow?.rootViewController as? BaseNavigationController {
//        return navi
//    }
//
//    return nil
//}

@end
