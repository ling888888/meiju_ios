//
//  LY_Api.h
//  SimpleDate
//
//  Created by jianyue on 2020/2/28.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_Api : NSObject


/// 获取完整的网络资源Url
/// @param resourceId 网络资源id
+ (NSString *)getCompleteResourceUrl:(NSString *)resourceId;

/// 根据URL判断是否是Java开发的接口
/// @param url 判断URL
+ (BOOL)isJavaServerWithUrl:(NSString *)url;

/// 我的资料
+ (NSString *)mine_info;

/// 获取周签到列表
+ (NSString *)mine_signIn;

/// 签到
+ (NSString *)signIn;

/// 关注主播列表
+ (NSString *)followList;
+ (NSString *)followOtherList; // 查看其他人关注列表 在这里服务器可是写的非常垃圾 明明可以传递参数解决问题（获取他人关注列表） 非要重新写一个接口 我是很无语。。。。。 这种做法本人十分不赞同 无奈人微言轻 

/// 取消关注主播
+ (NSString *)unfollow;

/// 直播在线观众列表
+ (NSString *)live_audienceList;

/// 直播榜单
+ (NSString *)live_rank;

/// 直播钱包账单
+ (NSString *)live_wallet_billList;

/// 直播列表
+ (NSString *)live_list;

/// 直播开始
+ (NSString *)live_start;

/// 直播上传信息
+ (NSString *)live_updateInfo;

/// 关注用户中正在直播的
+ (NSString *)live_followerLiving;

/// 我的家族
+ (NSString *)clan_mine;

/// 首页tabBar顺序
+ (NSString *)home_tabBar_config;

/// 根据融云id判断是否正在直播
+ (NSString *)live_rc_user;

/// 创建粉丝团
+ (NSString *)live_fansGroup_creat;

/// 编辑粉丝团
+ (NSString *)live_fansGroup_update;

/// 加入粉丝团
+ (NSString *)live_fansGroup_join;

/// 粉丝团信息
+ (NSString *)live_fansGroup_info;

/// 粉丝列表
+ (NSString *)live_fans_list;

/// 粉丝团任务列表
+ (NSString *)live_fansGroup_taskList;

/// 粉丝团完成任务
+ (NSString *)live_fansGroup_finishTask;

/// 粉丝团创建状态
+ (NSString *)live_fansGroup_creatStatus;

/// 粉丝团解锁
+ (NSString *)live_fansGroup_unlock;

/// 聊天室榜单
+ (NSString *)clan_rank;

/// 聊天室Banner
+ (NSString *)clan_banner;

/// 用户详细信息
+ (NSString *)user_detail;

/// 粉丝的关注数
+ (NSString *)live_Anchor_fans_num;

/// 编辑自己信息
+ (NSString *)edit_mineInfo;

/// 编辑自己照片和视频
+ (NSString *)edit_mineInfoPhoto;

/// 关注主播
+ (NSString *)live_follow;

/// 取消关注主播
+ (NSString *)live_unfollow;

/// 好友
+ (NSString *)friend;

/// 添加黑名单
+ (NSString *)blackList_add;

/// 移除黑名单
+ (NSString *)blackList_remove;

+ (NSString *)live_pk_score_list;

+ (NSString *)petBag;

+ (NSString *)transformation;
+ (NSString *)decorateConfig;
+ (NSString *)dupgrade;
+ (NSString *)boxprice;
+ (NSString *)shuxing;
+ (NSString *)receivejiangli;
+ (NSString *)compoundwhole;
+ (NSString *)compounddebris;
@end

NS_ASSUME_NONNULL_END
