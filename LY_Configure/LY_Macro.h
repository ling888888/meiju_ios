//
//  LY_Macro.h
//  SimpleDate
//
//  Created by jianyue on 2020/2/28.
//  Copyright © 2020 WYB. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LY_Macro : NSObject

/// 当前应用名称
+ (NSString *)appName;

/// 是否是生产环境
+ (BOOL)isDisEnvironment;

/// 当前正在显示的导航栏
+ (UINavigationController *)currentNavigationController;
@end

NS_ASSUME_NONNULL_END
