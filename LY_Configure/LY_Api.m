//
//  LY_Api.m
//  SimpleDate
//
//  Created by jianyue on 2020/2/28.
//  Copyright © 2020 WYB. All rights reserved.
//

#import "LY_Api.h"
#import "LY_Macro.h"

@interface LY_Api ()


@end

static NSString *api_version = @"1.2.9";
static NSString *dis_serverAddress = @"http://server.halahalahala.com";
static NSString *dev_serverAddress = @"https://dev-server.famy.ly";
//static NSString *dev_serverAddress = @"http://192.168.10.67:3000";
static NSString *dis_cdnServerAddress = @"https://famy.17jianyue.cn";
static NSString *dev_cdnServerAddress = @"https://dev-famy.17jianyue.cn";

static NSString *dis_serverAddress_java = @"http://new_game_server.famy.ly/web";
static NSString *dev_serverAddress_java = @"http://161.117.239.63:8888/web";
//static NSString *dev_serverAddress_java = @"http://192.168.10.120:8888/web";

@implementation LY_Api

+ (NSString *)cdnServerAddress {
    // 判断当前运行环境
    if (LY_Macro.isDisEnvironment) {
        return dis_cdnServerAddress;
    } else {
        return dev_cdnServerAddress;
    }
}

+ (NSString *)getServerAddressWithIsJava:(BOOL)isJava {
    // 判断当前运行环境
    if (LY_Macro.isDisEnvironment) {
        if (isJava) {
            return dis_serverAddress_java;
        } else {
            return dis_serverAddress;
        }
    } else {
        if (isJava) {
            return dev_serverAddress_java;
        } else {
            return dev_serverAddress;
        }
    }
}

+ (BOOL)isJavaServerWithUrl:(NSString *)url {
    return [url containsString:dis_serverAddress_java] || [url containsString:dev_serverAddress_java];
}

// 拼接接口路径
+ (NSString *)url:(NSString *)subPath isJave:(BOOL)isJava {
    return [[NSString alloc] initWithFormat:@"%@%@?_v=%@", [self getServerAddressWithIsJava:isJava], subPath, api_version];
}

/// 获取完整的资源路径
+ (NSString *)getCompleteResourceUrl:(NSString *)resourceId {
    if ([resourceId containsString:@"http"]) {
        return resourceId;
    }
    if ([resourceId containsString:@".mp4"]) {
        return [NSString stringWithFormat:@"%@/video/%@", [self cdnServerAddress], resourceId];
    }
    return [NSString stringWithFormat:@"%@/image/%@", [self cdnServerAddress], resourceId];
}

/// 我的资料
+ (NSString *)mine_info {
    return [self url:@"/api/my.info.detail" isJave:false];
}


+ (NSString *)mine_signIn {
    return [self url:@"/rank/sign.in.week" isJave:false];
}

+ (NSString *)signIn {
    return [self url:@"/rank/signin.week" isJave:false];
}

+ (NSString *)followList {
    return [self url:@"/live/live.follow.list" isJave:true];
}

+ (NSString *)followOtherList {
    return [self url:@"/live/live.follow.list.other" isJave:true];
}

+ (NSString *)unfollow {
    return [self url:@"/live/live.unfollow" isJave:true];
}

+ (NSString *)live_audienceList {
    return [self url:@"/live/live.listeners.list" isJave:true];
}

+ (NSString *)live_rank {
    return [self url:@"/live/live.rank" isJave:true];
}

+ (NSString *)live_pk_score_list {
    return [self url:@"/live/pk.score.list" isJave:true];
}

+ (NSString *)live_wallet_billList {
    return [self url:@"/live/live.wallet.list" isJave:true];
}

+ (NSString *)live_list {
    return [self url:@"/live/live.log.list" isJave:true];
}

+ (NSString *)live_start {
    return [self url:@"/live/live.start.info" isJave:true];
}

+ (NSString *)live_updateInfo {
    return [self url:@"/live/live.start.info.update" isJave:true];
}

+ (NSString *)live_followerLiving {
    return [self url:@"/live/live.follow.avatar.list" isJave:true];
}

+ (NSString *)clan_mine {
    return [self url:@"/clan/list.special" isJave:false];
}

+ (NSString *)petBag {
    return [self url:@"/api/decorate/bag" isJave:false];
}

+ (NSString *)transformation {
    return [self url:@"/api/decorate/transformation" isJave:false];
}

+ (NSString *)decorateConfig {
    return [self url:@"/api/decorate/config" isJave:false];
}

+ (NSString *)dupgrade {
    return [self url:@"/api/decorate/upgrade" isJave:false];
}

+ (NSString *)boxprice {
    return [self url:@"/api/decorate/price" isJave:false];
}

+ (NSString *)shuxing {
    return [self url:@"/api/user.decorate.equip" isJave:false];
}

+ (NSString *)compoundwhole {
    return [self url:@"/api/decorate/compound.whole" isJave:false];
}

+ (NSString *)compounddebris {
    return [self url:@"/api/decorate/compound.debris" isJave:false];
}

+ (NSString *)receivejiangli {
    return [self url:@"/api/decorate.receive" isJave:false];
}
+ (NSString *)home_tabBar_config {
    return [self url:@"/sys/tab.config" isJave:true];
}

+ (NSString *)live_rc_user {
    return [self url:@"/live/live.broadcaster.user" isJave:true];
}

+ (NSString *)live_fansGroup_creat {
    return [self url:@"/live/live.fansicon.create" isJave:true];
}

+ (NSString *)live_fansGroup_update {
    return [self url:@"/live/live.fansicon.update" isJave:true];
}

+ (NSString *)live_fansGroup_join {
    return [self url:@"/api/live.group.add" isJave:false];
}

+ (NSString *)live_fansGroup_info {
    return [self url:@"/api/live.group.fans" isJave:false];
}

+ (NSString *)live_fans_list {
    return [self url:@"/api/live.group.list" isJave:false];
}

+ (NSString *)live_fansGroup_taskList {
    return [self url:@"/live/live.fans.task" isJave:true];
}

+ (NSString *)live_Anchor_fans_num {
    return [self url:@"/api/live.follow.users" isJave:false];
}

+ (NSString *)live_fansGroup_finishTask {
    return [self url:@"/live/live.finish.task" isJave:true];
}

+ (NSString *)live_fansGroup_creatStatus {
    return [self url:@"/live/live.fansicon.query" isJave:true];
}

+ (NSString *)live_fansGroup_unlock {
    return [self url:@"/live/live.fans.group.unlock" isJave:true];
}

+ (NSString *)clan_rank {
    return [self url:@"/api/rank.catalogue" isJave:false];
}

+ (NSString *)clan_banner {
    return [self url:@"/api/banner" isJave:false];
}

+ (NSString *)user_detail {
    return [self url:@"/api/user.detail" isJave:false];
}

+ (NSString *)edit_mineInfo {
    return [self url:@"/api/my.info.edit" isJave:false];
}

+ (NSString *)edit_mineInfoPhoto {
    return [self url:@"/api/feed.create.new" isJave:false];
}

+ (NSString *)live_follow {
    return [self url:@"/live/live.follow" isJave:true];
}

+ (NSString *)live_unfollow {
    return [self url:@"/live/live.unfollow" isJave:true];
}

+ (NSString *)friend {
    return [self url:@"/api/friend" isJave:false];
}

+ (NSString *)blackList_add {
    return [self url:@"/api/my.blacklist.add" isJave:false];
}

+ (NSString *)blackList_remove {
    return [self url:@"/api/my.blacklist.remove" isJave:false];
}

@end
